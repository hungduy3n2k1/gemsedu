$.extend($.fn.datebox.defaults,{
	formatter:function(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		if (y=='1899')
				return '';
		else
				return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
	},
	parser:function(s){
		if (!s) return new Date();
		var ss = s.split('/');
		var d = parseInt(ss[0],10);
		var m = parseInt(ss[1],10);
		var y = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
		} else {
				return new Date();
		}
	}
});

function formatzero(val,row){
		if (val==0)
				return '';
		else
    		return val;
}

//$(function(){
//    $('#fm input').keydown(function(e) {
//        if (e.keyCode == 13) {
//            login();
//        }
//    });
//});

$(document).on("keydown", function (e) {
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
        e.preventDefault();
    }
});
$(document).on("keyup", function (e) {
    if (e.which === 13 && ["toolbar","toolbar1","toolbar2","toolbar3"].includes(e.target.parentElement.parentElement.id)) {
        timkiem();
    }
});
// hien thi thong bao dang slide
function show_messager(_text) {
	$.messager.show({
		title : 'Thông báo',
		msg : _text,
		showType : 'show'
	});
}
function show_center(title,_text){
    $.messager.show({
        title:title,
        msg:_text,
        showType:'fade',
        style:{
            right:'',
            bottom:''
        }
    });
}

function format_date(val, row){
	if(val != null) {
		var date=val.split('-');
		return date[2]+'/'+date[1]+'/'+date[0];
	}
	else
		return '';
}

function convert_date(val){ //convert date tu dang d/m/Y sang dang Y-m-d
	if(val != null) {
		var date=val.split('/');
		return date[2]+'-'+date[1]+'-'+date[0];
	}
	else
		return '0000-00-00';
}


function tienmat(val, row){
	if(val != null)
		return CurrencyFormatted(Number(val));
	else
		return '';
}

function get_today() {
	// var today = new Date();
	// var dd = String(today.getDate()).padStart(2, '0');
	// var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
	// var yyyy = today.getFullYear();
	// var homnay = dd + '/' + mm + '/' + yyyy;
	// return homnay;
	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth()+1;
	var y = today.getFullYear();
	return (d < 10 ? '0' + d : d) + '/' + (m < 10 ? '0' + m : m) + '/' + y;
}

/*
 * reset_format_date(id) Tác Dụng: Cập nhật lại định dạng ngày/tháng/năm
 */
function reset_format_date(id){
    $(id).datebox({
        formatter : function(date){
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            var d = date.getDate();
            return (d < 10 ? '0' + d : d) + '/' + (m < 10 ? '0' + m : m) + '/' + y;
        },
        parser : function(s){
            if (s){
                var a = s.split('/');
                var d = new Number(a[0]);
                var m = new Number(a[1]);
                var y = new Number(a[2]);
                var dd = new Date(y, m - 1, d);
                return dd;
            }else{
                return new Date();
            }
        }
    });
}

// dinh dang keiu tien tef
function FormatToCurrency(val, row){
    if(val != null){
        var num = val;
        num = num.toString().replace(/\$|\,/g,'');
        if(isNaN(num))
        num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num*100+0.50000000001);
        cents = num%100;
        num = Math.floor(num/100).toString();
        if(cents < 10)
        cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+','+ num.substring(num.length-(4*i+3));
        return (((sign)?'':'-') + num);
    }else{
        var num = 0;
        num = num.toString().replace(/\$|\,/g,'');
        if(isNaN(num))
        num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num*100+0.50000000001);
        cents = num%100;
        num = Math.floor(num/100).toString();
        if(cents < 10)
        cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+','+ num.substring(num.length-(4*i+3));
        return (((sign)?'':'-') + num);
    }
}

function Comma(Num) { //function to add commas to textboxes
    Num += '';
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
    return x1 + x2;
}

function CurrencyFormatted(amount){
	return amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function define_combobox(id, data_url, requi, multi, edita, ronly, id_field, text_field){
    $(id).combobox({
        url: data_url,
        valueField: id_field,
        textField: text_field,
        required: requi,
        multiple: multi,
        editable:edita,
		readonly:ronly
    });
}

function define_combobox_mota(id, data_url, requi, multi, edita, id_field, text_field){
    $(id).combobox({
        url: data_url,
        valueField: id_field,
        textField: text_field,
        required: requi,
        multiple: multi,
        editable:edita,
        formatter: formatItem
    });
}

function formatItem(row){
    var s = '<span style="font-weight:bold">' + row.Name + '</span><br/>' +
            '<span style="color:#888">' + row.Mota + '</span>';
    return s;
}

// combogrid khach hang
function define_khachhang(id, data_url, require){
    $(id).combogrid({
        panelWidth : 450,fitColumns : true,idField : 'Id',textField : 'Tenkhachhang',
        editable : true,mode : 'remote',pagination:true,panelHeight : 520, pageSize:30,
        rownumbers: true,url : data_url, required:require,
		columns : [[
            {field : 'Id',title : 'Id',hidden : true},
            {field : 'Tenkhachhang',title : 'Tên khách hàng',width : 200,sortable : true},
            {field : 'Sodienthoai',title : 'Điện thoại',width : 80,sortable : true, align:'center'},
            {field : 'Ngaysinh',title : 'Ngày sinh',width : 80,sortable : true, align:'center'}
        ]]
    });
    $("input[name='mode']").change(function(){
        var mode = $(this).val();
            $(id).combogrid({
            mode: mode
        });
    });
}

// combogrid nhan vien
function define_nhanvien(id, data_url, require, multi){
    $(id).combogrid({
        panelWidth : 450,fitColumns : true,idField : 'Id',textField : 'Tennhanvien',
        editable : false,mode : 'remote',pagination:false,panelHeight : 420,required:require,
        rownumbers: true,url : data_url, multiple:multi,
		columns : [[
            {field : 'Id',title : 'Id',hidden : true},
            {field : 'Manhanvien',title : 'Mã nhân viên',width : 100,sortable : true},
            {field : 'Tennhanvien',title : 'Tên nhân viên',width : 150,sortable : true},
            {field : 'Diachi',title : 'Địa chỉ',width : 180,sortable : true}
        ]]
    });
}

// combogrid hoa don ban le
function define_hoadon(id, data_url, require){
    $(id).combogrid({
        panelWidth : 450,fitColumns : true,idField : 'Sohoadon',textField : 'Sohoadon',
        editable : true,mode : 'remote',pagination:true,panelHeight : 420,
        rownumbers: true,url : data_url, required:require, pageSize:50,
		columns : [[
            {field : 'Sohoadon',title : 'Số hóa đơn',width : 100,sortable : true},
            {field : 'Ngay',title : 'Ngày bán',width : 110,sortable : true, align:'center'},
            {field : 'Nguoimua',title : 'Tên khách hàng',width : 120,sortable : true, align:'center'}
        ]]
    });
    $("input[name='mode']").change(function(){
        var mode = $(this).val();
            $(id).combogrid({
            mode: mode
        });
    });
}

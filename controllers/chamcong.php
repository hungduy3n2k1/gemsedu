<?php

class chamcong extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('chamcong') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "CHẤM CÔNG";
        $this->view->phep = $this->model->getphep(date("m"), date("Y"), $_SESSION['user']['nhan_vien']);
        require HEADER;
        $this->view->funs = $this->model->getfun('chamcong');
        if (MOBILE)
            $this->view->render('chamcong/index_m');
        else
            $this->view->render('chamcong/index');
        require FOOTER;
    }

    function json()
    {
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $nhanvien = (isset($_REQUEST['nhanvien']) && ($_REQUEST['nhanvien'] != '')) ? $_REQUEST['nhanvien'] : $_SESSION['user']['nhan_vien'];
        $jsonObj = $this->model->getFetObj($nhanvien, $thang, $nam);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phep()  //doc so lieu nghi phep de nap vao bang jquery
    {
        $thang = $_REQUEST['thang'];
        $nam = $_REQUEST['nam'];
        $nhanvien = $_REQUEST['nhanvien'];
        $data = $this->model->getphep($thang, $nam, $nhanvien);
        if (isset($data['phep_luy_ke'])) {
            $jsonObj['msg'] = 'OK';
            $jsonObj['phep'] = ($data['phepsang'] + $data['phepchieu']) / 2;
            $jsonObj['khongluong'] = ($data['khongsang'] + $data['khongchieu']) / 2;
            $jsonObj['phat'] = ($data['phatsang'] + $data['phatchieu']) / 2;
            $jsonObj['tongphep'] = $data['phep_luy_ke'];
            $jsonObj['danghi'] = ($data['phepnamsang'] + $data['phepnamchieu']) / 2;
            $jsonObj['conlai'] = $jsonObj['tongphep'] - $jsonObj['danghi'];
            $jsonObj['nghibu'] = $data['phep_nam'];
            $jsonObj['danghi1'] = ($data['busang'] + $data['buchieu']) / 2;
            $jsonObj['conlai1'] = $jsonObj['nghibu'] - $jsonObj['danghi1'];
        } else
            $jsonObj['msg'] = 'Không đọc được dữ liệu nghỉ phép';
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function chamcong()
    {
        $ngay = functions::convertDate($_REQUEST['ngay']);
        $giovao = $_REQUEST['giovao'];
        $giora = $_REQUEST['giora'];
        $sang = $_REQUEST['sang'];
        $chieu = $_REQUEST['chieu'];
        $lydo = $_REQUEST['lydo'];
        $apdung = $_REQUEST['apdung'];
        $nhanvien = $_REQUEST['idnhanvien'];
        if (($sang == '') && ($chieu == '')) {
            $jsonObj['msg'] = "Bạn cần phải chấm công sáng hoặc chiều";
            $jsonObj['success'] = false;
        } else {
            $data = array(
                'ngay' => $ngay,
                'nhan_vien' => $nhanvien,
                'ghi_chu' => $lydo,
                'tinh_trang' => 0
            );
            if ($giovao > 0)
                $data['gio_vao'] = $giovao;
            if ($giora > 0)
                $data['gio_ra'] = $giora;
            if ($sang != '')
                $data['sang'] = $sang;
            if ($chieu != '')
                $data['chieu'] = $chieu;
            if ($this->model->chamcongtay($data, $apdung)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function baonghi()
    {
        $nhanvien = $_SESSION['user']['nhan_vien'];
        $ngay = functions::convertDate($_REQUEST['ngaynghi']);
        $sang = $_REQUEST['nghisang'];
        $chieu = $_REQUEST['nghichieu'];
        $lydo = $_REQUEST['noidung'];
        if (($sang == '') && ($chieu == '')) {
            $jsonObj['msg'] = "Bạn cần phải chọn hình thức nghỉ";
            $jsonObj['success'] = false;
        } else {
            if ($sang == 2) {
                if (!$this->model->checkphep($nhanvien, $ngay)) {
                    $jsonObj['msg'] = "Bạn không còn đủ phép, vui lòng chọn hình thức nghỉ khác";
                    $jsonObj['success'] = false;
                } elseif ($ngay == date('Y-m-d')) {
                    $jsonObj['msg'] = "Bạn cần xin nghỉ phép trước 1 ngày";
                    $jsonObj['success'] = false;
                }
            } elseif ($sang == 6) {
                if (!$this->model->checkbu($nhanvien, $ngay)) {
                    $jsonObj['msg'] = "Bạn không còn quỹ nghỉ bù, vui lòng chọn hình thức nghỉ khác";
                    $jsonObj['success'] = false;
                } elseif ($ngay == date('Y-m-d')) {
                    $jsonObj['msg'] = "Bạn cần xin nghỉ bù trước 1 ngày";
                    $jsonObj['success'] = false;
                }
            } elseif ($chieu == 2) {
                if (!$this->model->checkphep($nhanvien, $ngay)) {
                    $jsonObj['msg'] = "Bạn không còn đủ phép, vui lòng chọn hình thức nghỉ khác";
                    $jsonObj['success'] = false;
                } elseif ($ngay == date('Y-m-d')) {
                    $jsonObj['msg'] = "Bạn cần xin nghỉ phép trước 1 ngày";
                    $jsonObj['success'] = false;
                }
            } elseif ($chieu == 6) {
                if (!$this->model->checkbu($nhanvien, $ngay)) {
                    $jsonObj['msg'] = "Bạn không còn quỹ nghỉ bù, vui lòng chọn hình thức nghỉ khác";
                    $jsonObj['success'] = false;
                } elseif ($ngay == date('Y-m-d')) {
                    $jsonObj['msg'] = "Bạn cần xin nghỉ bù trước 1 ngày";
                    $jsonObj['success'] = false;
                }
            }
        }
        if (!isset($jsonObj)) {
            $data = array(
                'ngay' => $ngay,
                'nhan_vien' => $nhanvien,
                'ghi_chu' => $lydo,
                'tinh_trang' => 0
            );
            if ($sang > 0) $data['sang'] = $sang;
            if ($chieu > 0) $data['chieu'] = $chieu;
            if ($this->model->baonghi($data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function loaiphep()
    {// combobox khi chấm công tay
        $jsonObj = $this->model->loaiphep();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function loainghi()
    {  // combobox khi báo nghỉ
        $jsonObj = $this->model->loainghi();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function cong()
    {
        $this->view->jsonObj = '[{"id":"0.5","name":"0.5"},{"id":"1","name":"1.0"},{"id":"0","name":"0.0"}]';
        $this->view->render('common/json');
    }

    function checkout()
    {
        if ($this->model->checkout()) {
            $jsonObj['msg'] = "Đã checkout";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Checkout không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


}

?>

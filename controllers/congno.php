<?php

class congno extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('congno') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "BÁO CÁO DOANH THU";
        require HEADER;
        $this->view->funs = $this->model->getfun('congno');
        $this->view->tongcongno = $this->model->getTongCongNo('', date('m'), date('Y'), '');
        if (MOBILE) {
            $this->view->render('congno/index_m');
        } else
            $this->view->render('congno/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : '';
        $thang = isset($_REQUEST['thang']) && $_REQUEST['thang'] != "" ? $_REQUEST['thang'] : date('m');
        $nam = isset($_REQUEST['nam']) && $_REQUEST['nam'] != "" ? $_REQUEST['nam'] : date('Y');
        $loaidh = isset($_REQUEST['loaidh']) && $_REQUEST['loaidh'] != "" ? $_REQUEST['loaidh'] : 0;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $khachhang, $thang, $nam, $loaidh);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function jsonmobile()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $team = isset($_REQUEST['team']) ? $_REQUEST['team'] : 1;
        $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date("m");
        $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam'] : date("Y");
        $jsonObj = $this->model->getjson($sort, $order, $offset, $rows, $team, $thang, $nam);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function getTongCongNo()
    {
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : '';
        $thang = isset($_REQUEST['thang']) && $_REQUEST['thang'] != "" ? $_REQUEST['thang'] : date('m');
        $nam = isset($_REQUEST['nam']) && $_REQUEST['nam'] != "" ? $_REQUEST['nam'] : date('Y');
        $loaidh = isset($_REQUEST['loaidh']) && $_REQUEST['loaidh'] != "" ? $_REQUEST['loaidh'] : 0;
        $jsonObj = $this->model->getTongCongNo($khachhang, $thang, $nam, $loaidh);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


}

?>

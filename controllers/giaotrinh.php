<?php

class giaotrinh extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new model();
        $this->fun = $model->getfun('giaotrinh');
        if ($model->checkright('giaotrinh') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ GIÁO TRÌNH";
        require 'layouts/header.php';
        $this->view->funs = $this->fun;
        $this->view->render('giaotrinh/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 30;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'level ASC, id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phanloai()
    {
        $this->view->jsonObj = '[{"id":"1","text":"Hợp đồng"},{"id":"2","text":"Báo giá"},
          {"id":"3","text":"Đề nghị tạm ứng"},{"id":"4","text":"Đề nghị thanh toán"},{"id":"5","text":"Yêu cầu tuyển dụng"}]';
        $this->view->render('common/json');
    }

    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"0","text":"Chưa duyệt"},{"id":"1","text":"Đã duyệt"}]';
        $this->view->render('common/json');
    }

    function add()
    {
        $checkadd = false;
        foreach ($this->fun as $item) {
            if ($item['link'] == 'add()')
                $checkadd = true;
        }
        if ($checkadd == true) {
            $name = $_REQUEST['name'];
            $link = $_REQUEST['link'];
            $level = $_REQUEST['level'];
            $unit = $_REQUEST['unit'];
            $lesson = $_REQUEST['lesson'];
            $review = $_REQUEST['review'];
            $bonus = $_REQUEST['bonus'];
            $test = $_REQUEST['test'];
            $phanloai = 0;
            $data = array(
                'ngay_gio' => date('Y-m-d'),
                'name' => $name,
                'link' =>$link,
                'level' => $level,
                'unit' => $unit,
                'lesson' => $lesson,
                'review' => $review,
                'bonus' => $bonus,
                'test'=>$test,
                'phan_loai'=>$phanloai,
                'tinh_trang' => 1
            );
            if ($this->model->addObj($data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công" ;
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function update()
    {
        $checkfun = false;
        foreach ($this->fun as $item) {
            if ($item['link'] == 'edit()')
                $checkfun = true;
        }
        if ($checkfun == true) {
            $id = $_REQUEST['id'];
            $name = $_REQUEST['name'];
            $link = $_REQUEST['link'];
            $level = $_REQUEST['level'];
            $unit = $_REQUEST['unit'];
            $lesson = $_REQUEST['lesson'];
            $review = $_REQUEST['review'];
            $bonus = $_REQUEST['bonus'];
            $test = $_REQUEST['test'];
            $phanloai = 0;
            $data = array(
              //  'ngay_gio' => date('Y-m-d'),
                'name' => $name,
                'link' =>$link,
                'level' => $level,
                'unit' => $unit,
                'lesson' => $lesson,
                'review' => $review,
                'bonus' => $bonus,
                'test'=>$test
              //  'phan_loai'=>$phanloai,
              //  'tinh_trang' => 1
            );
            if ($this->model->updateObj($id, $data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công" ;
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function noidung()
    {
        $checkfun = false;
        foreach ($this->fun as $item) {
            if ($item['link'] == 'noidung()')
                $checkfun = true;
        }
        if ($checkfun == true) {
            $id = $_REQUEST['id'];
            $noidung = $_REQUEST['noi_dung'];
            $data = array('noi_dung' => $noidung);
            if ($this->model->updateObj($id, $data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function view()
    {
        require 'layouts/header.php';
        $id = $_REQUEST['id'];
        $this->view->content = $this->model->content($id);
        $this->view->render('giaotrinh/view');
        require 'layouts/footer.php';
    }

    function del()
    {
        $checkfun = false;
        foreach ($this->fun as $item) {
            if ($item['link'] == 'del()')
                $checkfun = true;
        }
        if ($checkfun == true) {
            $id = $_REQUEST['id'];
            $temp = $this->model->delObj($id);
            if ($temp) {
                $jsonObj['msg'] = "Xóa dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Xóa dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function import()
    {
        $funs = $this->fun;
        $import = false;
        foreach ($funs as $item) {
            if ($item['link'] == 'nhap()') {
                $import = true;
                break;
            }
        }
        if ($import) {
            require_once ROOT_DIR . '/libs/phpexcel/PHPExcel/IOFactory.php';
            try {
                $inputFileType = PHPExcel_IOFactory::identify($_FILES['file1']['tmp_name']);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($_FILES['file1']['tmp_name']);
                $objReader->setReadDataOnly(true);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $banghi = 0;
                for ($row = 3; $row <= $highestRow; $row++) {
                    $name = $objPHPExcel->getActiveSheet()->getCell("C$row")->getValue();
                    $link = $objPHPExcel->getActiveSheet()->getCell("E$row")->getValue();
                    if ($name != '' && $link!='') {
                        $level = $objPHPExcel->getActiveSheet()->getCell("B$row")->getValue();
                        $level = ($level!='')?$level:'Khác';
                        $phanloai = $objPHPExcel->getActiveSheet()->getCell("D$row")->getValue();
                        $unit = $objPHPExcel->getActiveSheet()->getCell("G$row")->getValue();
                        $lesson = $objPHPExcel->getActiveSheet()->getCell("H$row")->getValue();
                        $review = $objPHPExcel->getActiveSheet()->getCell("J$row")->getValue();
                        $bonus = $objPHPExcel->getActiveSheet()->getCell("K$row")->getValue();
                        $test = $objPHPExcel->getActiveSheet()->getCell("L$row")->getValue();
                        $date = date("Y-m-d");
                        $data = array(
                            'ngay_gio' => $date,
                            'name' => $name,
                            'link' =>$link,
                            'level' => $level,
                            'unit' => $unit,
                            'lesson' => $lesson,
                            'review' => $review,
                            'bonus' => $bonus,
                            'test'=>$test,
                            'phan_loai'=>$phanloai,
                            'tinh_trang' => 1
                        );
                        if ($this->model->addObj($data))
                            $banghi++;

                    }
                    if ($banghi > 0) {
                        $jsonObj['msg'] = "Cập nhật thành công $banghi giáo trình";
                        $jsonObj['success'] = true;
                    } else {
                        $jsonObj['msg'] = "Lỗi cập nhật database";
                        $jsonObj['success'] = false;
                    }
                }
            } catch (Exception $e) {
                $jsonObj['msg'] = "Import dữ liệu không thành công";
                $jsonObj['success'] = false;
            }

            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }


}

?>

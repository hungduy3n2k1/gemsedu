<?php

class bieumau extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new model();
        $this->fun = $model->getfun('bieumau');
        if ($model->checkright('bieumau') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ BIỂU MẪU";
        require 'layouts/header.php';
        $this->view->funs = $this->fun;
        $this->view->render('bieumau/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 30;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'phan_loai ASC, name';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $phanloai = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : 0;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $phanloai);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phanloai()
    {
        $this->view->jsonObj = '[{"id":"1","text":"Hợp đồng"},{"id":"2","text":"Báo giá"},
          {"id":"3","text":"Đề nghị tạm ứng"},{"id":"4","text":"Đề nghị thanh toán"},{"id":"5","text":"Yêu cầu tuyển dụng"}]';
        $this->view->render('common/json');
    }

    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"0","text":"Chưa duyệt"},{"id":"1","text":"Đã duyệt"}]';
        $this->view->render('common/json');
    }

    function add()
    {
        $checkadd = false;
        foreach ($this->fun as $item) {
            if ($item['link'] == 'add()')
                $checkadd = true;
        }
        if ($checkadd == true) {
            $name = $_REQUEST['name'];
            $fname = functions::convertname($name);
            $phanloai = $_REQUEST['phan_loai'];
            $date = date("Y-m-d");
            $data = array(
                'name' => $name,
                'phan_loai' => $phanloai,
                'nhan_vien' => $_SESSION['user']['id'],
                'ngay_gio'=>$date,
                'ngay_cap_nhat'=>$date,
                'tinh_trang' => 1
            );
            if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] != '')) {
                $dir = ROOT_DIR . '/uploads/bieumau/';
                $file = functions::uploadfile('file', $dir, $fname);
                $filepath = URL . '/uploads/bieumau/' . $file;
                $data['filename'] = $filepath;
            }
            if ($this->model->addObj($data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công" . $filepath;
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function update()
    {
        $checkfun = false;
        foreach ($this->fun as $item) {
            if ($item['link'] == 'edit()')
                $checkfun = true;
        }
        if($checkfun==true) {
            $id = $_REQUEST['id'];
            $name = $_REQUEST['name'];
            $fname = functions::convertname($name);
            $phanloai = $_REQUEST['phan_loai'];
            $date = date("Y-m-d");
            $data = array(
                'name' => $name,
                'nhan_vien' => $_SESSION['user']['id'],
                'ngay_cap_nhat'=>$date,
                'phan_loai' => $phanloai
            );
            if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] != '')) {
                $dir = ROOT_DIR . '/uploads/bieumau/';
                $file = functions::uploadfile('file', $dir, $fname);
                $filepath = URL . '/uploads/bieumau/' . $file;
                $data['filename'] = $filepath;
            }
            if ($this->model->updateObj($id, $data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function noidung()
    {
        $checkfun = false;
        foreach ($this->fun as $item) {
            if ($item['link'] == 'noidung()')
                $checkfun = true;
        }
        if($checkfun==true) {
            $id = $_REQUEST['id'];
            $noidung = $_REQUEST['noi_dung'];
            $data = array('noi_dung' => $noidung);
            if ($this->model->updateObj($id, $data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function view()
    {
        require 'layouts/header.php';
        $id = $_REQUEST['id'];
        $this->view->content = $this->model->content($id);
        $this->view->render('bieumau/view');
        require 'layouts/footer.php';
    }

    function del()
    {
         $checkfun = false;
        foreach ($this->fun as $item) {
            if ($item['link'] == 'del()')
                $checkfun = true;
        }
        if($checkfun==true) {
            $id = $_REQUEST['id'];
            $temp = $this->model->delObj($id);
            if ($temp) {
                $jsonObj['msg'] = "Xóa dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Xóa dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }


}

?>

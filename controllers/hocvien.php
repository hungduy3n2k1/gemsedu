<?php

class hocvien extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new Model();
        $this->fun = $model->getfun('hocvien');
        if ($model->checkright('hocvien') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ HỌC VIÊN";
        require HEADER;
        $this->view->funs = $this->fun;
        if (MOBILE)
            $this->view->render('hocvien/index_m');
        else
            $this->view->render('hocvien/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $phanloai = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : 0;
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : 0;
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa, $phanloai, $khachhang, $tinhtrang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lichhoc()
    {
        if (isset($_REQUEST['hocvien'])) {
            $hocvien = $_REQUEST['hocvien'];
            $tenhocvien = $_REQUEST['tenhocvien'];
            $module = "QUẢN LÝ LỊCH HỌC";
            require HEADER;
            $this->view->hocvien = $hocvien;
            $this->view->tenhocvien = $tenhocvien;
            $this->view->render('hocvien/lichhoc');
            require FOOTER;
        }
    }

    function jsonhocvien()
    {
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $hocvien = (isset($_REQUEST['hocvien']) && ($_REQUEST['hocvien'] != '')) ? $_REQUEST['hocvien'] : '';
        $jsonObj = $this->model->getLichhoc($hocvien, $thang, $nam);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function jsonhocvien1()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : '01';
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $hocvien = (isset($_REQUEST['hocvien']) && ($_REQUEST['hocvien'] != '')) ? $_REQUEST['hocvien'] : '';
        $jsonObj = $this->model->getLichhoc1($sort, $order, $offset, $rows, $hocvien, $thang, $nam);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function LopHocByHV()
    {

        $hocvien = (isset($_REQUEST['hocvien']) && ($_REQUEST['hocvien'] != '')) ? $_REQUEST['hocvien'] : '';
        $jsonObj = $this->model->getLopHocByHV($hocvien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"1","name":"Đang chờ"},{"id":"2","name":"Đang học"},{"id":"3","name":"Đã tốt nghiệp"}]';
        $this->view->render('common/json');
    }

    function add()
    {
//        $mahocvien = $_REQUEST['ma_hoc_vien'];
        $name = $_REQUEST['name'];
        $ename = $_REQUEST['e_name'];
        $gioitinh = $_REQUEST['gioi_tinh'];
        $ngaysinh = functions::convertDate($_REQUEST['ngaysinh']);
        $truonghoc = $_REQUEST['truong_hoc'];
        $lophoc = $_REQUEST['lop_hoc'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $diachi = $_REQUEST['dia_chi'];
        $email = $_REQUEST['email'];
        $facebook = $_REQUEST['facebook'];
        $zalo = $_REQUEST['zalo'];
        $khachhang = $_REQUEST['khach_hang'];
        $phanloai = $_REQUEST['phan_loai'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        $data = array(
            'name' => $name,
            'e_name' => $ename,
            'gioi_tinh' => $gioitinh,
            'ngay_sinh' => $ngaysinh,
            'truong_hoc' => $truonghoc,
            'lop_hoc' => $lophoc,
            'dien_thoai' => $dienthoai,
            'dia_chi' => $diachi,
            'email' => $email,
            'facebook' => $facebook,
            'zalo' => $zalo,
            'khach_hang' => $khachhang,
            'phan_loai' => $phanloai,
            'tinh_trang' => $tinhtrang
        );
        $hocvien = $this->model->addObj($data);
        if ($hocvien > 0) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $ename = $_REQUEST['e_name'];
        $gioitinh = $_REQUEST['gioi_tinh'];
        $ngaysinh = functions::convertDate($_REQUEST['ngaysinh']);
        $truonghoc = $_REQUEST['truong_hoc'];
        $lophoc = $_REQUEST['lop_hoc'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $diachi = $_REQUEST['dia_chi'];
        $email = $_REQUEST['email'];
        $facebook = $_REQUEST['facebook'];
        $zalo = $_REQUEST['zalo'];
        $khachhang = $_REQUEST['khach_hang'];
        $phanloai = $_REQUEST['phan_loai'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        $data = array(
            'name' => $name,
            'e_name' => $ename,
            'gioi_tinh' => $gioitinh,
            'ngay_sinh' => $ngaysinh,
            'truong_hoc' => $truonghoc,
            'lop_hoc' => $lophoc,
            'dien_thoai' => $dienthoai,
            'dia_chi' => $diachi,
            'email' => $email,
            'facebook' => $facebook,
            'zalo' => $zalo,
            'khach_hang' => $khachhang,
            'phan_loai' => $phanloai,
            'tinh_trang' => $tinhtrang
        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function import()
    {
        $funs = $this->fun;
        $import = false;
        foreach ($funs as $item) {
            if ($item['link'] == 'nhap()') {
                $import = true;
                break;
            }
        }
        if ($import) {
            require_once ROOT_DIR . '/libs/phpexcel/PHPExcel/IOFactory.php';
            try {
                $inputFileType = PHPExcel_IOFactory::identify($_FILES['file']['tmp_name']);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($_FILES['file']['tmp_name']);
                $objReader->setReadDataOnly(true);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $banghi = 0;
                $idlophoc = 0;
                for ($row = 2; $row <= $highestRow; $row++) {
                    $tenph = $objPHPExcel->getActiveSheet()->getCell("G$row")->getValue();
                    $dienthoai = $objPHPExcel->getActiveSheet()->getCell("H$row")->getValue();
                    $dataph = array(
                        'name' => $tenph,
                        'ngay' => date('Y-m-d'),
                        'dien_thoai' => $dienthoai,
                        'nhan_vien' => $_SESSION['user']['nhan_vien'],
                        'phu_trach' => $_SESSION['user']['nhan_vien'],
                        'tinh_trang' => 1
                    );
                    $idph = $this->model->themPH($dataph);
                    $ten = $objPHPExcel->getActiveSheet()->getCell("C$row")->getValue();
                    if ($ten != '') {
                        $ename = $objPHPExcel->getActiveSheet()->getCell("C$row")->getValue();
                        $data = array(
                            'name' => $ten,
                            'e_name' => $ename,
                            'khach_hang' => $idph,
                            'phan_loai' => 14,
                            'tinh_trang' => 1
                        );
                        $idhocvien = $this->model->addObj($data);
                        if ($idhocvien > 0)
                            $banghi++;
//                        $tenlop = $objPHPExcel->getActiveSheet()->getCell("D$row")->getValue();
//                        $checktenlop = $this->model->checkTenlop($tenlop);
//                        if ($checktenlop == 0) {
//                            $datalh = array(
//                                'name' => $tenlop,
//                                'phan_loai' => 3,
//                                'tinh_trang' => 1
//                            );
//                            $idlophoc = $this->model->themLop($datalh);
//                        } else
//                            $idlophoc = $checktenlop;
//                        $ngaybatdau = $objPHPExcel->getActiveSheet()->getCell("G$row")->getValue();
//                        if ($ngaybatdau != '') {
//                            if (strlen($ngaybatdau) == 5) {
//                                $ngaybatdau = ((string)$ngaybatdau - 25569) * 86400;
//                                $ngaybatdau = date("Y-m-d", $ngaybatdau);
//                            } else {
//                                $ngaybatdau = functions::convertDate($ngaybatdau);
//                            }
//                        } else
//                            $ngaybatdau = '0000-00-00';
//                        $datasl = array(
//                            'hoc_vien' => $idhocvien,
//                            'lop_hoc' => $idlophoc,
//                            'ngay_bat_dau' => $ngaybatdau,
//                            'ngay_ket_thuc' => '0000-00-00',
//                            'tinh_trang' => 1
//                        );
//                        $this->model->themSapLop($datasl);
                    }
                    if ($banghi > 0) {
                        $jsonObj['msg'] = "Cập nhật thành công $banghi data";
                        $jsonObj['success'] = true;
                    } else {
                        $jsonObj['msg'] = "Lỗi cập nhật database";
                        $jsonObj['success'] = false;
                    }
                }
            } catch (Exception $e) {
                $jsonObj['msg'] = "Import dữ liệu không thành công";
                $jsonObj['success'] = false;
            }

            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function importlich()
    {
        $funs = $this->fun;
        $import = false;
        foreach ($funs as $item) {
            if ($item['link'] == 'nhap()') {
                $import = true;
                break;
            }
        }
        if ($import) {
            $hocvien = $_REQUEST['hocvien'];
            $lop = $_REQUEST['chonlop'];
            if (intVal($lop) > 0) {
                $idlophoc = $lop;
            } else {
                $lophoc = [
                    'name' => $lop,
                    'phan_loai' => 1,
                    'tinh_trang' => 2
                ];
                $idlophoc = $this->model->themLop($lophoc);
            }
            $cotexcel = [
                ['B', '2020-08'],
                ['C', '2020-09'],
                ['D', '2020-10'],
                ['E', '2020-11'],
                ['F', '2020-12'],
                ['G', '2021-01'],
                ['H', '2021-02'],
                ['I', '2021-03'],
                ['J', '2021-04'],
                ['K', '2021-05'],
                ['L', '2021-06'],
                ['M', '2021-07'],
                ['N', '2021-08'],
                ['O', '2021-09'],
                ['P', '2021-10'],
                ['Q', '2021-11'],
                ['R', '2021-12']
            ];
            if ($idlophoc > 0) {
                require_once ROOT_DIR . '/libs/phpexcel/PHPExcel/IOFactory.php';
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($_FILES['file1']['tmp_name']);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($_FILES['file1']['tmp_name']);
                    $objReader->setReadDataOnly(true);
                    $objWorksheet = $objPHPExcel->getActiveSheet();
                    $highestRow = $objWorksheet->getHighestRow();
                    $highestColumn = $objWorksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $banghi = 0;
                    for ($row = 7; $row <= 37; $row++) {
                        foreach ($cotexcel as $thang) {
                            $ngay = ($row - 6);
                            if ($ngay < 10)
                                $ngay = '0' . $ngay;
                            $temp = $objPHPExcel->getActiveSheet()->getCell("$thang[0]$row")->getValue();
                            if ($temp != '') {
                                $thoiluong = 0;
                                if ($temp == '0,75' || $temp == '0.75')
                                    $thoiluong = 45;
                                if ($temp == '1')
                                    $thoiluong = 60;
                                if ($temp == '1,5' || $temp == '1.5')
                                    $thoiluong = 90;
                                if (in_array($temp, ['0,75', '1', '1,5', '0.75', '1', '1.5'])) {
                                    $lichhoc = [
                                        'ngay' => $thang[1] . '-' . $ngay,
                                        'gio' => '19:30',
                                        'lop_hoc' => $idlophoc,
                                        'giao_vien' => 0,
                                        'phong_hoc' => 5,
                                        'thoi_luong' => $thoiluong,
                                        'tinh_trang' => 3
                                    ];
                                    $idlichoc = $this->model->themLichhoc($lichhoc);
                                    if ($idlichoc > 0) {
                                        $saplop = [
                                            'hoc_vien' => $hocvien,
                                            'lich_hoc' => $idlichoc,
                                            'lop_hoc' => $idlophoc,
                                            'tinh_trang' => 2
                                        ];
                                        if ($this->model->themSapLop($saplop))
                                            $banghi++;
                                    }
                                } else {
                                    $buois = explode("-", $temp);
                                    if (count($buois > 1)) {
                                        $lichhoc = [
                                            'ngay' => $thang[1] . '-' . $ngay,
                                            'gio' => '19:30',
                                            'lop_hoc' => $idlophoc,
                                            'giao_vien' => 0,
                                            'phong_hoc' => 5,
                                            'thoi_luong' => 45,
                                            'tinh_trang' => 3
                                        ];
                                        $idlichoc = $this->model->themLichhoc($lichhoc);
                                        if ($idlichoc > 0) {
                                            $saplop = [
                                                'hoc_vien' => $hocvien,
                                                'lich_hoc' => $idlichoc,
                                                'lop_hoc' => $idlophoc,
                                                'tinh_trang' => 2
                                            ];
                                            if ($this->model->themSapLop($saplop))
                                                $banghi++;
                                        }
                                        $lichhoc = [
                                            'ngay' => $thang[1] . '-' . $ngay,
                                            'gio' => '20:15',
                                            'lop_hoc' => $idlophoc,
                                            'giao_vien' => 0,
                                            'phong_hoc' => 5,
                                            'thoi_luong' => 45,
                                            'tinh_trang' => 3
                                        ];
                                        $idlichoc = $this->model->themLichhoc($lichhoc);
                                        if ($idlichoc > 0) {
                                            $saplop = [
                                                'hoc_vien' => $hocvien,
                                                'lich_hoc' => $idlichoc,
                                                'lop_hoc' => $idlophoc,
                                                'tinh_trang' => 2
                                            ];
                                            if ($this->model->themSapLop($saplop))
                                                $banghi++;
                                        }
                                    }
                                }
                            }

                        }
                        if ($banghi > 0) {
                            $jsonObj['msg'] = "Cập nhật thành công $banghi data";
                            $jsonObj['success'] = true;
                        } else {
                            $jsonObj['msg'] = "Lỗi cập nhật database";
                            $jsonObj['success'] = false;
                        }
                    }
                } catch
                (Exception $e) {
                    $jsonObj['msg'] = "Import dữ liệu không thành công";
                    $jsonObj['success'] = false;
                }
            } else {
                $jsonObj['msg'] = "Thêm lớp không thành công!";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function saplop()
    {
        $hocvien = $_REQUEST['hocvien'];
        $listlh = $_REQUEST['data-saplop'];
        $lophoc = $_REQUEST['lop_hoc'];
        if ($listlh != '') {
            $listlh = rtrim($listlh, ',');
            if ($hocvien != '' && $lophoc != '')
                $this->model->xoasaplop($hocvien, $lophoc, $listlh);
            $temp = explode(",", $listlh);
            $banghi = 0;
            foreach ($temp as $lichhoc) {
                $check = $this->model->checkSaplop($hocvien, $lichhoc);
                if ($check == 0) {
                    $data = array(
                        'hoc_vien' => $hocvien,
                        'lich_hoc' => $lichhoc,
                        'lop_hoc' => $lophoc,
                        'tinh_trang' => 1
                    );
                    if ($this->model->themSapLop($data))
                        $banghi++;
                } else {
                    $banghi++;
                }
            }
            if ($banghi > 0) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công ";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        } else {
            $this->model->xoasaplop($hocvien, $lophoc, $listlh);
            $jsonObj['msg'] = "Bạn chưa chọn lịch học!";
            $jsonObj['success'] = true;
        }

        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function getLichHoc()
    {
        $lophoc = $_REQUEST['lophoc'];
        $hocvien = $_REQUEST['hocvien'];
//        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
//        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
//        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay';
//        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
//        $offset = ($page - 1) * $rows;
//        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : '01';
//        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
//        $hocvien = (isset($_REQUEST['hocvien']) && ($_REQUEST['hocvien'] != '')) ? $_REQUEST['hocvien'] : '';
        $jsonObj = $this->model->getLichhoc2($lophoc, $hocvien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lophoc()
    {
        $jsonObj = $this->model->lophoc();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

<?php
class ungvien extends controller
{
    function __construct()
    {
        parent::__construct();
        $model  = new Model();
        if ($model->checkright('ungvien')==false)
                     header ('Location: '.URL);
    }
    function index()
    {
        $module = "CHẤM CÔNG";
        require HEADER;
        $this->view->funs=$model->getfun('ungvien');
        if (MOBILE)
 			 		  $this->view->render('ungvien/index_m');
 			 else
            $this->view->render('ungvien/index');
        require FOOTER;
    }

    function json()
    {
        $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order               = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset              = ($page - 1) * $rows;
        $tukhoa              = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $name        = $_REQUEST['name'];
        $gioitinh    = $_REQUEST['gioi_tinh'];
        $fname = functions::convertname($name);
        $ngaysinh    = functions::convertDate($_REQUEST['ngaysinh']);
        $honnhan     = $_REQUEST['hon_nhan'];
        $diachi      = $_REQUEST['dia_chi'];
        $quequan     = $_REQUEST['que_quan'];
        $dienthoai   = $_REQUEST['dien_thoai'];
        $emailcanhan = $_REQUEST['email_canhan'];
        $trinhdo     = $_REQUEST['trinh_do'];
        $truonghoc   = $_REQUEST['truong_hoc'];
        $zalo        = $_REQUEST['zalo'];
        // $facebook    = $_REQUEST['facebook'];
        $ghichu = $_REQUEST['ghi_chu'];
        $data        = array(
            'name' => $name,
            'gioi_tinh' => $gioitinh,
            'ngay_sinh' => $ngaysinh,
            'hon_nhan' => $honnhan,
            'dia_chi' => $diachi,
            'que_quan' => $quequan,
            'dien_thoai' => $dienthoai,
            'email_canhan' => $emailcanhan,
            'trinh_do' => $trinhdo,
            'zalo' => $zalo,
            'truong_hoc'=>$truonghoc,
            'ghi_chu'=>$ghichu,
            'tinh_trang' => 1
        );
        if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] != '')) {
           $dir   = ROOT_DIR . '/uploads/hinhanh/';
           $file  = functions::uploadfile('file', $dir, $fname);
           $hinhanh  = URL . '/uploads/hinhanh/' . $file;
           $data['hinh_anh']=$hinhanh;
        }
        if (isset($_FILES['taicv']['name']) && ($_FILES['taicv']['name'] != '')) {
            $dir   = ROOT_DIR . '/uploads/cv/';
            $file  = functions::uploadfile('taicv', $dir, $fname);
            $cv  = URL . '/uploads/cv/' . $file;
            $data['cv']=$cv;
        } else {
            $data['cv']=$_REQUEST['cv'];
        }
        if ($this->model->addObj($data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name        = $_REQUEST['name'];
        $fname = functions::convertname($name);
        $gioitinh    = $_REQUEST['gioi_tinh'];
        $ngaysinh    = functions::convertDate($_REQUEST['ngaysinh']);
        $honnhan     = $_REQUEST['hon_nhan'];
        $diachi      = $_REQUEST['dia_chi'];
        $quequan     = $_REQUEST['que_quan'];
        $dienthoai   = $_REQUEST['dien_thoai'];
        $emailcanhan = $_REQUEST['email_canhan'];
        $trinhdo     = $_REQUEST['trinh_do'];
        $truonghoc   = $_REQUEST['truong_hoc'];
        $zalo        = $_REQUEST['zalo'];
        // $facebook    = $_REQUEST['facebook'];
        $ghichu = $_REQUEST['ghi_chu'];
        $data        = array(
            'name' => $name,
            'gioi_tinh' => $gioitinh,
            'ngay_sinh' => $ngaysinh,
            'hon_nhan' => $honnhan,
            'dia_chi' => $diachi,
            'que_quan' => $quequan,
            'dien_thoai' => $dienthoai,
            'email_canhan' => $emailcanhan,
            'trinh_do' => $trinhdo,
            'zalo' => $zalo,
            'truong_hoc'=>$truonghoc,
            'ghi_chu'=>$ghichu
        );
        if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] != '')) {
          $dir   = ROOT_DIR . '/uploads/hinhanh/';
          $file  = functions::uploadfile('file', $dir, $fname);
          $hinhanh  = URL . '/uploads/hinhanh/' . $file;
          $data['hinh_anh']=$hinhanh;
        }
        if (isset($_FILES['taicv']['name']) && ($_FILES['taicv']['name'] != '')) {
            $dir   = ROOT_DIR . '/uploads/cv/';
            $file  = functions::uploadfile('taicv', $dir, $fname);
            $cv  = URL . '/uploads/cv/' . $file;
            $data['cv']=$cv;
        } else {
            $data['cv']=$_REQUEST['cv'];
        }
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id   = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg']     = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // function hopdong()
    // {
    //     $id          = $_REQUEST['id'];
    //     $ngaydilam   = functions::convertDate($_REQUEST['ngaydilam']);
    //     $tinhtrang   = $_REQUEST['tinh_trang'];
    //     $phongban    = $_REQUEST['phong_ban'];
    //     $email       = $_REQUEST['email'];
    //     $luong       = $_REQUEST['luong'];
    //     $baohiem     = $_REQUEST['bao_hiem'];
    //     $anca        = $_REQUEST['an_ca'];
    //     $guixe       = $_REQUEST['gui_xe'];
    //     $masothue    = $_REQUEST['ma_so_thue'];
    //     $sobaohiem     = $_REQUEST['so_bao_hiem'];
    //     $sotk        = $_REQUEST['so_tk'];
    //     $nganhang    = $_REQUEST['ngan_hang'];
    //     $chutk       = $_REQUEST['chu_tk'];
    //     $ngayketthuc = functions::convertDate($_REQUEST['ngayketthuc']);
    //     $ghichu      = $_REQUEST['ghi_chu'];
    //
    //     $data        = array(
    //         'ngay_di_lam' => $ngaydilam,
    //         'phong_ban' => $phongban,
    //         'tinh_trang' => $tinhtrang,
    //         'email' => $email,
    //         'luong' => $luong,
    //         'bao_hiem' => $baohiem,
    //         'an_ca' => $anca,
    //         'gui_xe' => $guixe,
    //         'ma_so_thue' => $masothue,
    //         'so_bao_hiem' => $sobaohiem,
    //         'so_tk' => $sotk,
    //         'ngan_hang' => $nganhang,
    //         'chu_tk' => $chutk,
    //         'ngay_ket_thuc' => $ngayketthuc,
    //         'ghi_chu' => $ghichu
    //     );
    //     if ($this->model->updateObj($id, $data)) {
    //         $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
    //         $jsonObj['success'] = true;
    //     } else {
    //         $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
    //         $jsonObj['success'] = false;
    //     }
    //     $this->view->jsonObj = json_encode($jsonObj);
    //     $this->view->render('common/json');
    // }

}
?>

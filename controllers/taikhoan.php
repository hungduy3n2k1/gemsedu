<?php
class taikhoan extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('taikhoan')==false)
            header('Location: ' . URL);
    }

    function index()
    {
        require HEADER;
        $this->view->funs=$this->model->getfun('taikhoan');
        if (MOBILE)
           $this->view->render('taikhoan/index_m');
        else
           $this->view->render('taikhoan/index');
        require FOOTER;
    }

    function json()
    {
        $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order               = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset              = ($page - 1) * $rows;
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $name   = $_REQUEST['name'];
        $sotk   = $_REQUEST['so_tk'];
        $nganhang   = $_REQUEST['ngan_hang'];
        $sodu   = $_REQUEST['sodu'];
        $data   = array(
            'name' => $name,
            'so_tk' => $sotk,
            'ngan_hang' => $nganhang,
            'tinh_trang' => 1
        );
        if ($this->model->addObj($data,$sodu)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id        = $_REQUEST['id'];
        $name   = $_REQUEST['name'];
        $sotk   = $_REQUEST['so_tk'];
        $nganhang   = $_REQUEST['ngan_hang'];
        $data   = array(
            'name' => $name,
            'so_tk' => $sotk,
            'ngan_hang' => $nganhang
        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id   = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg']     = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

<?php
class ngaynghi extends Controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new model();
        if ($model->checkright('ngaynghi')==false)
           header('Location: ' . URL);
    }

    function index()
    {
        require 'layouts/header.php';
        $this->view->funs=$model->getfun('ngaynghi');
        $this->view->render('ngaynghi/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page                = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows                = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort                = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order               = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset              = ($page - 1) * $rows;
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phep()
 	  {

       $nhanvien = $_REQUEST['nhanvien'];
       $ngay = functions::convertDate($_REQUEST['ngay']);
 			 $chamcong   = $_REQUEST['loaihinh'];
 			 $ghichu   = $_REQUEST['ghichu'];
 			 $data     = array(
 							 'ngay' => $ngay,
               'ngay_tao' => date("Y-m-d"),
 							 'nhan_vien' => $nhanvien,
 							 'ghi_chu' => $ghichu,
 							 'loai_hinh' => $chamcong
 			 );
 			 if ($this->model->champhep($data)) {
 							 $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
 							 $jsonObj['success'] = true;
 			 } else {
 							 $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
 							 $jsonObj['success'] = false;
 			 }
 			 $this->view->jsonObj = json_encode($jsonObj);
 			 $this->view->render('common/json');
 	 }

    function duyet()
    {
        $id     = $_REQUEST['id'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        $ghichu = $_REQUEST['ghi_chu'];
        $loaihinh = $_REQUEST['loai_hinh'];
        $ngay = functions::convertDate($_REQUEST['ngaynghi']);
        $nhanvien = $_REQUEST['nhan_vien'];
        $data      = array(
            'ghi_chu' =>$ghichu,
            'tinh_trang' => $tinhtrang
        );
        if ($this->model->duyet($id, $data,$loaihinh,$ngay,$nhanvien)) {
                $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
        } else {
                $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function nghile()
    {
        $ngay = functions::convertDate($_REQUEST['ngayle']);
        $loaihinh = $_REQUEST['letet'];
        $ghichu = $_REQUEST['lydo'];
        if ($this->model->nghile($ngay, $loaihinh,$ghichu)) {
                $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
        } else {
                $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function loaiphep(){
        $jsonObj             = $this->model->loaiphep();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"0","name"	:"Chưa duyệt"},{"id":"1","name":"Đã duyệt"},{"id":"2","name":"Từ chối"}]';
        $this->view->render('common/json');
    }

    function letet(){
        $jsonObj             = $this->model->letet();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

}
?>

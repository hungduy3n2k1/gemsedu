<?php
class taituc extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('taituc')==false)
            header ('Location: '.URL);
    }

    function index()
    {
        $module = "BÁO CÁO TÁI TỤC";
        require HEADER;
        $this->view->funs = $this->model->getfun('taituc');
        $this->view->tong = $this->model->getTong();
        if(MOBILE)
            $this->view->render('taituc/index_m');
        else 
            $this->view->render('taituc/index');
        require FOOTER;
    }

    function json()
    {
        $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay_dang_ky';
        $order               = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset              = ($page - 1) * $rows;
        $ngaybd              = isset($_REQUEST['ngaybd']) ? functions::convertDate($_REQUEST['ngaybd']) : '';
        $ngaykt              = isset($_REQUEST['ngaykt']) ? functions::convertDate($_REQUEST['ngaykt']) : '';
        $phanloai              = isset($_REQUEST['phanloai']) && $_REQUEST['phanloai']!="" ? $_REQUEST['phanloai'] : 0;
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt, $phanloai);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>
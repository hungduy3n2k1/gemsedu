<?php

class baocaogiaovien extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new Model();
        $this->fun = $model->getfun('baocaogiaovien');
        if ($model->checkright('baocaogiaovien') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ HỌC VIÊN";
        require HEADER;
        $this->view->funs = $this->fun;
        if (MOBILE)
            $this->view->render('baocaogiaovien/index_m');
        else
            $this->view->render('baocaogiaovien/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 30;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $hocvien = isset($_REQUEST['hocvien']) ? $_REQUEST['hocvien'] : '';
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : '';
        $tungay = isset($_REQUEST['tungay']) ? $_REQUEST['tungay'] : "";
        $tungay = $tungay!=''?functions::convertDate($tungay):'';
        $denngay = isset($_REQUEST['denngay']) ? $_REQUEST['denngay'] : "";
        $denngay = $denngay!=''?functions::convertDate($denngay):'';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $hocvien, $khachhang, $tungay, $denngay);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

}

?>

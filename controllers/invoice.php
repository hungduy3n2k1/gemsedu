<?php

class invoice extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('invoice') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = 'QUẢN LÝ INVOICE';
        require HEADER;
        $this->view->funs = $model->getfun('invoice');
        if (MOBILE)
            $this->view->render('invoice/index_m');
        else
            $this->view->render('invoice/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'don_hang DESC ,dot_thanh_toan';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $tungay = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : '';
        $denngay = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : '';
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : 0;
        $nhanvien = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : 0;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $khachhang,$nhanvien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function detail()
    {
        $this->view->id = $_REQUEST['id'];
        $this->view->render('invoice/detail');
    }

    function jsondetail()
    {
        $baogia = $_REQUEST['id'];
        $jsonObj = $this->model->get_detail($baogia);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function noidung()
    {
        $id = $_REQUEST['id'];
        $jsonObj = $this->model->noidung($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"1","name":"Chưa gửi"},{"id":"2","name":"Đã gửi"},{"id":"3","name":"Tạm ứng"},
          {"id":"4","name":"Đã thanh toán"},{"id":"5","name":"Đã gia hạn"},{"id":"6","name":"Hủy"}]';
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $ngaytt = functions::convertDate($_REQUEST['ngay']);
        $sotien = $_REQUEST['so_tien'];
        $donhang = $_REQUEST['don_hang'];
        $tongtien = $_REQUEST['tongtien'];
        $tongtien = str_replace(",","",$tongtien);
        $temptien = $this->model->checktien($id,$donhang);
        $checktien = $tongtien-$temptien-$sotien;
        if($checktien>=0) {
            $ngay = functions::convertDate($_REQUEST['ngay']);
            $data = array('ngay' => $ngay, 'so_tien'=>$sotien);
            if ($this->model->updateObj($id, $data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        }
        else {
            $jsonObj['msg'] = "Tổng các đợt thanh toán lớn hơn tổng tiền đơn hàng! $checktien";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $khachhang = $_REQUEST['khachhang'];
        if ($this->model->delObj($id, $khachhang)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function email()
    {
        // lấy danh sách email theo id khách hàng
        $id = $_REQUEST['id'];
        $jsonObj = $this->model->email($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function sendmail()
    {
        $id = $_REQUEST['id'];
        $lan = $_REQUEST['lan'];
        $email = $_REQUEST['emailtosend'];
        $note = $_REQUEST['note'];
        if ($this->model->sendinvoice($id, $email, $note, $lan)) {
            $jsonObj['msg'] = "Đã gửi invoice tới khách hàng ";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Gửi invoice không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
    function testmail()
    {
      $this->model->testmail();
    }

    function addrow()
    {
        $invoice = $_REQUEST['invoice'];
        $sanpham = $_REQUEST['dich_vu'];
        $dongia = $_REQUEST['don_gia'];
        $donvi = $_REQUEST['don_vi'];
        $chietkhautm = $_REQUEST['chiet_khau_tm'];
        $chietkhaupt = $_REQUEST['chiet_khau_pt'];
        $soluong = $_REQUEST['so_luong'];
        $thang = $_REQUEST['tang_thang'];
        $ghichu = $_REQUEST['ghi_chu'];
        $thuevat = $_REQUEST['thue_suat_vat'];
        $ngaybd = functions::convertDate($_REQUEST['ngaybd']);
        $ngaykt = functions::convertDate($_REQUEST['ngaykt']);
        $data = array('dich_vu' => $sanpham, 'so_luong' => $soluong, 'don_gia' => $dongia, 'don_vi' => $donvi, 'ngay_bd' => $ngaybd,
            'chiet_khau_tm' => $chietkhautm, 'chiet_khau_pt' => $chietkhaupt, 'tang_thang' => $thang, 'ghi_chu' => $ghichu, 'ngay_kt' => $ngaykt,
            'tinh_trang' => 1, 'invoice' => $invoice, 'thue_suat_vat' => $thuevat);
        if ($this->model->addrow($data, $invoice)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function updaterow()
    {
        $id = $_REQUEST['id'];
        $invoice = $_REQUEST['invoice'];
        $sanpham = $_REQUEST['dich_vu'];
        $dongia = $_REQUEST['don_gia'];
        $donvi = $_REQUEST['don_vi'];
        $chietkhautm = $_REQUEST['chiet_khau_tm'];
        $chietkhaupt = $_REQUEST['chiet_khau_pt'];
        $soluong = $_REQUEST['so_luong'];
        $thang = $_REQUEST['tang_thang'];
        $thuevat = $_REQUEST['thue_suat_vat'];
        $ghichu = $_REQUEST['ghi_chu'];
        $tenmien = $_REQUEST['ten_mien'];
        $ngaybd = functions::convertDate($_REQUEST['ngaybd']);
        $ngaykt = functions::convertDate($_REQUEST['ngaykt']);
        $data = array('dich_vu' => $sanpham, 'so_luong' => $soluong, 'don_gia' => $dongia, 'don_vi' => $donvi,
            'ngay_bd' => $ngaybd, 'ngay_kt' => $ngaykt, 'tang_thang' => $thang, 'thue_suat_vat' => $thuevat,
            'chiet_khau_tm' => $chietkhautm, 'chiet_khau_pt' => $chietkhaupt, 'ghi_chu' => $ghichu,'ten_mien' => $tenmien);
        if ($this->model->updaterow($id, $data, $invoice)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function delrow()
    {
        $id = $_REQUEST['id'];
        $invoice = $_REQUEST['invoice'];
        $data = array('tinh_trang' => 0);
        if ($this->model->updaterow($id, $data, $invoice)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function invoice()
    {
        $tungay = functions::convertDate($_REQUEST['tungay']);
        $denngay = functions::convertDate($_REQUEST['denngay']);
        $jsonObj = $this->model->invoice($tungay, $denngay);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function gop()
    {
        $invoice = $_REQUEST['invoice'];
        if ($this->model->gop($invoice)) {
            $jsonObj['msg'] = "Đã gộp invoice";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Gộp invoice không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    //Thêm invoice mới
    function add()
    {
        // thêm báo giá
        $data = $_REQUEST['invoice1'];
        if (strlen($data) > 3) {
            $khachhang = $_REQUEST['khach_hang'];
            $nhanvien = $_SESSION['user']['nhan_vien'];
            $noidung = $_REQUEST['noidung'];

            if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
                $dir = ROOT_DIR . '/uploads/baogia/';
                $file = functions::uploadfile('file', $dir, 'inv_' . $_FILES['file']['name']);
                $dinhkem = URL . '/uploads/baogia/' . $file;
            } else {
                $dinhkem = $_REQUEST['dinhkem'];
            }
            $send = $_REQUEST['send'];
            if ($send == 1)
                $tinhtrang = 2;
            else
                $tinhtrang = 1;
            $invoice = [
                'ngay' => date("Y-m-d"),
                'khach_hang' => $khachhang,
                'loai' => 1,
                'noi_dung' => $noidung,
                'tinh_trang' => $tinhtrang,
                'nhan_vien' => $nhanvien,
                'dinh_kem' => $dinhkem];
            $id = $this->model->addObj($invoice, $data);
            if ($id > 0) {
                if ($send == 1) {
                    $lan = 'lan_1';
                    $email = $_REQUEST['email'];
                    $note = '';
                    if ($this->model->sendinvoice($id, $email, $note, $lan))
                        $jsonObj['msg'] = "Thêm invoice thành công và đã gửi email tới khách hàng ";
                    else
                        $jsonObj['msg'] = "Thêm invoice thành công";
                } else {
                    $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                }
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        } else {
            $jsonObj['msg'] = "Invoice chưa có nội dung";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function nhanvien()
    {
        $jsonObj             = $this->model->nhanvien();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
    function donhang()
    {
        $khachhang = $_REQUEST['khachhang'];
        $jsonObj = $this->model->donhang($khachhang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

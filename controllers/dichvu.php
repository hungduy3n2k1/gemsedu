<?php
class dichvu extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('dichvu') == false) {
            header('Location: ' . URL);
        }
    }

    function index()
    {
        $module = "SẢN PHẨM VÀ CÁC GÓI DỊCH VỤ";
        require HEADER;
        $this->view->funs = $this->model->getfun('dichvu');
        $this->view->render('dichvu/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'phan_loai';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
        $tukhoa              = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $loai,$tukhoa);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"1","text":"Active"},{"id":"0","text":"Inactive"}]';
        $this->view->render('common/json');
    }
    function add()
    {
        $name = $_REQUEST['name'];
        $loai = $_REQUEST['phan_loai'];
        $ghichu = $_REQUEST['ghi_chu'];
        $dongia = str_replace(',', '', $_REQUEST['don_gia']);
        $giavon = str_replace(',', '', $_REQUEST['gia_von']);
        //$donvitinh = $_REQUEST['don_vi_tinh'];
        $donvitinh=3;
        $vat = $_REQUEST['thue_suat_vat'];
        $tinhtrang = 1;
        //$giahan = $_REQUEST['gia_han'];
        $giahan=0;
        $khoahoc = $_REQUEST['khoa_hoc'];
        $data = ['name' => $name, 'phan_loai' => $loai, 'ghi_chu' => $ghichu, 'don_gia' => $dongia, 'gia_von' => $giavon,
          'don_vi_tinh' => $donvitinh, 'tinh_trang' => $tinhtrang, 'gia_han' => $giahan,'thue_suat_vat'=>$vat,'khoa_hoc'=>$khoahoc];
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $loai = $_REQUEST['phan_loai'];
        $ghichu = $_REQUEST['ghi_chu'];
        $dongia = str_replace(',', '', $_REQUEST['don_gia']);
        $giavon = str_replace(',', '', $_REQUEST['gia_von']);
        //$donvitinh = $_REQUEST['don_vi_tinh'];
        $donvitinh=3;
        $vat = $_REQUEST['thue_suat_vat'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        //$giahan = $_REQUEST['gia_han'];
        $giahan=0;
        $khoahoc = $_REQUEST['khoa_hoc'];
        $data = ['name' => $name, 'phan_loai' => $loai, 'ghi_chu' => $ghichu, 'don_gia' => $dongia, 'gia_von' => $giavon,
          'don_vi_tinh' => $donvitinh, 'tinh_trang' => $tinhtrang, 'gia_han' => $giahan,'thue_suat_vat'=>$vat,'khoa_hoc'=>$khoahoc];
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

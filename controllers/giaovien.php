<?php

class giaovien extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new model();
        $this->fun = $model->getfun('giaovien');
        if ($model->checkright('giaovien') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ GIẢNG VIÊN";
        require 'layouts/header.php';
        $this->view->funs = $this->fun;
        $this->view->render('giaovien/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tukhoa = isset($_REQUEST['tukhoa']) ? strval($_REQUEST['tukhoa']) : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows,$tukhoa);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $name = $_REQUEST['name'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $ngaysinh = functions::convertDate($_REQUEST['ngaysinh']);
        $taikhoan = $_REQUEST['tai_khoan'];
        $email = $_REQUEST['email'];
        $chuyennganh = $_REQUEST['chuyen_nganh'];
        $loai = $_REQUEST['phan_loai'];
        $luongkid = $_REQUEST['luong_kid'];
        $luonggiaotiep = $_REQUEST['luong_giao_tiep'];
        $data = array(
            'name' => $name,
            'ngay_sinh'=>$ngaysinh,
            'tai_khoan' => $taikhoan,
            'dien_thoai' => $dienthoai,
            'email' => $email,
            'luong_kid'=>$luongkid,
            'luong_giao_tiep'=>$luonggiaotiep,
            'chuyen_nganh' => $chuyennganh,
            'phan_loai' => $loai,
            'tinh_trang' => 1
        );
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $ngaysinh = functions::convertDate($_REQUEST['ngaysinh']);
        $dienthoai = $_REQUEST['dien_thoai'];
        $taikhoan = $_REQUEST['tai_khoan'];
        $email = $_REQUEST['email'];
        $chuyennganh = $_REQUEST['chuyen_nganh'];
        $loaihinh = $_REQUEST['loai_hinh'];
        $bangtienganh = $_REQUEST['bang_tieng_anh'];
        $bangdaihoc = $_REQUEST['bang_dai_hoc'];
        $loai = $_REQUEST['phan_loai'];
        $luongkid = $_REQUEST['luong_kid'];
        $luonggiaotiep = $_REQUEST['luong_giao_tiep'];
        $data = array(
            'name' => $name,
            'tai_khoan' => $taikhoan,
            'dien_thoai' => $dienthoai,
            'email' => $email,
            'ngay_sinh'=>$ngaysinh,
            'luong_kid'=>$luongkid,
            'luong_giao_tiep'=>$luonggiaotiep,
            'chuyen_nganh' => $chuyennganh,
            'loai_hinh'=>$loaihinh,
            'bang_tieng_anh'=>$bangtienganh,
            'bang_dai_hoc'=>$bangdaihoc,
            'phan_loai' => $loai
        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function import()
    {
        $funs = $this->fun;
        $checkfun = false;
        foreach ($funs as $item)
            if ($item['link'] == 'nhap()') {
                $checkfun = true;
                break;
            }
        if ($checkfun == true) {
            require_once ROOT_DIR . '/libs/phpexcel/PHPExcel/IOFactory.php';
            try {
                $inputFileType = PHPExcel_IOFactory::identify($_FILES['file']['tmp_name']);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($_FILES['file']['tmp_name']);
                $objReader->setReadDataOnly(true);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $banghi = 0;
                $loai = $_REQUEST['phan_loai'];
                for ($row = 2; $row <= $highestRow; $row++) {
                    $name = $objPHPExcel->getActiveSheet()->getCell("B$row")->getValue();
                    if ($name == '')
                        continue;
                    $ngaysinh = $objPHPExcel->getActiveSheet()->getCell("C$row")->getValue();
                    if ($ngaysinh != '') {
                        if (strlen($ngaysinh) == 5) {
                            $ngaysinh = ((string)$ngaysinh - 25569) * 86400;
                            $ngaysinh = date("Y-m-d", $ngaysinh);
                        } else
                            $ngaysinh = functions::convertDate($ngaysinh);
                    } else
                        $ngaysinh = '0000-00-00';
                    $taikhoan = $objPHPExcel->getActiveSheet()->getCell("D$row")->getValue();
                    $dienthoai = $objPHPExcel->getActiveSheet()->getCell("E$row")->getValue();
                    $email = $objPHPExcel->getActiveSheet()->getCell("F$row")->getValue();
                    $chuyennganh = $objPHPExcel->getActiveSheet()->getCell("G$row")->getValue();
                    $data = array(
                        'name' => $name,
                        'ngay_sinh'=>$ngaysinh,
                        'tai_khoan' => $taikhoan,
                        'dien_thoai' => $dienthoai,
                        'email' => $email,
                        'chuyen_nganh' => $chuyennganh,
                        'phan_loai' => $loai,
                        'tinh_trang' => 1);
                    if ($this->model->addObj($data))
                        $banghi++;
                }
                if ($banghi > 0) {
                    $jsonObj['msg'] = "Cập nhật thành công $banghi data";
                    $jsonObj['success'] = true;
                } else {
                    $jsonObj['msg'] = "Lỗi cập nhật database";
                    $jsonObj['success'] = false;
                }
            } catch (Exception $e) {
                $jsonObj['msg'] = "Import dữ liệu không thành công";
                $jsonObj['success'] = false;
            }

            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }
}

?>

<?php
class bangluong extends controller
{
    private $fun;
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        $this->fun = $model->getfun('bangluong');
 		    if ($model->checkright('bangluong')==false)
            header ('Location: '.URL);
    }

    function index()
    {
        require HEADER;
        $this->view->funs=$this->fun;
        if (MOBILE)
            $this->view->render('bangluong/index_m');
        else
            $this->view->render('bangluong/index');
        require FOOTER;
    }

    function json()
    {
        $funs = $this->fun;
        $thang = isset($_REQUEST['thang'])?$_REQUEST['thang']:date("m");
        $nam = isset($_REQUEST['nam'])?$_REQUEST['nam']:date("Y");
        $jsonObj = $this->model->getFetObj($thang,$nam,$funs);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // function checkduyet()
    // {
    //     $thang = isset($_REQUEST['thang'])?$_REQUEST['thang']:date("m");
    //     $nam = isset($_REQUEST['nam'])?$_REQUEST['nam']:date("Y");
    //     $result = $this->model->checkduyet($thang, $nam);
    //     echo $result;
    //     //     $jsonObj['msg']     = "Đã duyệt bảng lương tháng ".$thang.'/'.$nam;
    //     //     $jsonObj['success'] = true;
    //     // } else {
    //     //     $jsonObj['msg']     = "Duyệt bảng lương không thành công";
    //     //     $jsonObj['success'] = false;
    //     // }
    //     // $this->view->jsonObj = json_encode($jsonObj);
    //     // $this->view->render('common/json');
    // }

    function lapbang()
    {
        $thang = isset($_REQUEST['thang'])?$_REQUEST['thang']:date("m");
        $nam = isset($_REQUEST['nam'])?$_REQUEST['nam']:date("Y");
        if ($this->model->lapbangluong($thang, $nam)) {
            $jsonObj['msg']     = "Đã lập lại bảng lương tháng ".$thang.'/'.$nam;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Bảng lương tháng này đã tồn tại";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id        = $_REQUEST['id'];
        $thuongds  = $_REQUEST['thuong_ds'];
        $thuonglt  = $_REQUEST['thuong_lt'];
        $thuong  = $_REQUEST['thuong'];
        $tamung    = $_REQUEST['tam_ung'];
        $data      = array(
          'thuong_ds' => $thuongds,
          'thuong_lt' => $thuonglt,
          'thuong_khac' => $thuong,
          'tam_ung' => $tamung
        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function duyet()
    {
        $thang = isset($_REQUEST['thang'])?$_REQUEST['thang']:date("m");
        $nam = isset($_REQUEST['nam'])?$_REQUEST['nam']:date("Y");
        if ($this->model->duyet($thang, $nam)) {
            $jsonObj['msg']     = "Đã duyệt bảng lương tháng ".$thang.'/'.$nam;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Duyệt bảng lương không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function xuatfile()
    {
        $thang = isset($_REQUEST['thang'])?$_REQUEST['thang']:date("m");
        $nam = isset($_REQUEST['nam'])?$_REQUEST['nam']:date("Y");
        $this->view->data = $this->model->getFetObj($thang,$nam,true);
        $this->view->render('bangluong/xuatfile');
    }
}
?>

<?php
class menu extends Controller
{
    function __construct()
    {
        parent::__construct();
        if ($_SESSION['user']['id']!=1 && $_SESSION['user']['id']!=2 )
            header ('Location: '.URL);
    }

    function index()
    {
        require HEADER;
        $this->view->render('menu/index');
        require FOOTER;
    }

    function json()
    {
        $loai = isset($_REQUEST['loai'])?$_REQUEST['loai']:1;
        $jsonObj             = $this->model->getFetObj($loai);
        $this->view->jsonObj = str_replace('"_parentId":"",', '', json_encode($jsonObj));
        $this->view->render('common/json');
    }

    function add()
    {
        $name     = $_REQUEST['name'];
        $link     = $_REQUEST['link'];
        $icon     = $_REQUEST['icon'];
        $thutu    = $_REQUEST['thu_tu'];
        $parentid = $_REQUEST['parentid'];
        $active   = $_REQUEST['active'];
        $loai = $_REQUEST['loai'];
        $data     = array(
            'name' => $name,
            'link' => $link,
            'icon' => $icon,
            'parentid' => $parentid,
            'active' => $active,
            'loai'=>$loai,
            'thu_tu' => $thutu
        );
        $temp     = $this->model->addObj($data);
        if ($temp) {
            $jsonObj['msg']     = 'Cập nhật thành công';
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = 'Cập nhật không thành công';
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id       = $_REQUEST['id'];
        $name     = $_REQUEST['name'];
        $link     = $_REQUEST['link'];
        $icon     = $_REQUEST['icon'];
        $thutu    = $_REQUEST['thu_tu'];
        $parentid = $_REQUEST['parentid'];
        $active   = $_REQUEST['active'];
        $mobile   = $_REQUEST['mobile'];
        $loai = $_REQUEST['loai'];
        $data     = array(
            'name' => $name,
            'link' => $link,
            'icon' => $icon,
            'parentid' => $parentid,
            'active' => $active,
            'mobile' => $mobile,
            'loai'=>$loai,
            'thu_tu' => $thutu
        );
        $temp     = $this->model->updateObj($id, $data);
        if ($temp) {
            $jsonObj['msg']     = 'Cập nhật thành công';
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = 'Cập nhật không thành công';
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id   = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg']     = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

<?php

class lophoc extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new Model();
        $this->fun = $model->getfun('lophoc');
        if ($model->checkright('lophoc') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ LỚP HỌC";
        require HEADER;
        $this->view->funs = $this->fun;
        if (MOBILE)
            $this->view->render('lophoc/index_m');
        else
            $this->view->render('lophoc/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : '';
        $khoahoc = isset($_REQUEST['khoahoc']) ? $_REQUEST['khoahoc'] : '';
        $phanloai = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : '';
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa, $giaovien, $khoahoc, $phanloai, $tinhtrang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function donhang()
    {
        $hocvien = $_REQUEST['hoc_vien'];
        $jsonObj = $this->model->donhang($hocvien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    //View lịch học
    function lichhoc()
    {
        if (isset($_REQUEST['lophoc'])) {
            $lophoc = $_REQUEST['lophoc'];
            $tenlophoc = $_REQUEST['tenlophoc'];
            $module = "LỊCH HỌC LỚP $tenlophoc";
            require HEADER;
            $this->view->lophoc = $lophoc;
            $this->view->tenlophoc = $tenlophoc;
            $this->view->render('lophoc/lichhoc');
            require FOOTER;
        }
    }

    function jsonlichhoc()
    {
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $lophoc = (isset($_REQUEST['lophoc']) && ($_REQUEST['lophoc'] != '')) ? $_REQUEST['lophoc'] : '';
        $jsonObj = $this->model->getLichhoc($lophoc, $thang, $nam);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function jsonlophoc()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $phanloai = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : 0;
        $lophoc = isset($_REQUEST['lophoc']) ? $_REQUEST['lophoc'] : 0;
        $jsonObj = $this->model->getlophoc($sort, $order, $offset, $rows, $tukhoa, $phanloai, $lophoc);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"1","name":"Chờ"},{"id":"2","name":"Đang học"},{"id":"3","name":"Hoàn thành"},{"id":"4","name":"Hủy"}]';
        $this->view->render('common/json');
    }

    function phanloai1()
    {
//        $this->view->jsonObj = '[{"id":"1","name":"Online"},{"id":"2","name":"Offline"},
//        {"id":"3","name":"Demo Online"},{"id":"4","name":"Demo Offline"},{"id":"5","name":"Trải nghiệm offline"}]';
        $this->view->jsonObj = '[{"id":"1","name":"Online"},{"id":"2","name":"Mầm non (S)"},
        {"id":"3","name":"Tiểu học (F)"},{"id":"4","name":"Trung học (E)"},
        {"id":"5","name":"Demo 30"},{"id":"6","name":"Demo 45"},{"id":"7","name":"Demo 60"}]';
        $this->view->render('common/json');
    }

    function phanloai2()
    {
//        $this->view->jsonObj = '[{"id":"1","name":"Online"},{"id":"2","name":"Offline"},
//        {"id":"3","name":"Demo Online"},{"id":"4","name":"Demo Offline"},{"id":"5","name":"Trải nghiệm offline"}]';
        $phanloai1 = $_REQUEST['phanloai'];
        if ($phanloai1 == 1) {
            $this->view->jsonObj = '[{"id":"1","name":"Kid"},{"id":"2","name":"Giao tiếp"},{"id":"3","name":"Demo"}]';
        } else {
            $this->view->jsonObj = '[{"id":"1","name":"Main"},{"id":"2","name":"Grammar"},{"id":"3","name":"Club"}]';
        }
        $this->view->render('common/json');
    }

    function add()
    {
        $name = $_REQUEST['name'];
        $giaovien = $_REQUEST['giao_vien'];
        $phutrach = $_REQUEST['phu_trach'];
        $chinhanh = $_REQUEST['chi_nhanh'];
        $trogiang = $_REQUEST['tro_giang'];
        $giaotrinh = $_REQUEST['giao_trinh'];
        $khoahoc = $_REQUEST['khoa_hoc'];
        $phanloai = $_REQUEST['phan_loai'];
        $phanloai2 = $_REQUEST['phan_loai_2'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        $ghichu = $_REQUEST['ghi_chu'];
        $hocvien = $_REQUEST['hoc_vien'];
        $sobuoi = $_REQUEST['so_buoi'];
        $buoikm = intVal($_REQUEST['buoi_khuyen_mai']);
        $thoiluong = $_REQUEST['thoi_luong'];
        $donhang = $_REQUEST['don_hang'];
        $sotien = intVal($_REQUEST['so_tien']);
        $data = array(
            'name' => $name,
            'giao_vien' => $giaovien,
            'phu_trach' => $phutrach,
            'chi_nhanh' => $chinhanh,
            'giao_trinh' => $giaotrinh,
            'khoa_hoc' => $khoahoc,
            'so_buoi' => $sobuoi,
            'buoi_khuyen_mai' => $buoikm,
            'tro_giang' => $trogiang,
            'phan_loai' => $phanloai,
            'phan_loai_2' => $phanloai2,
            'ghi_chu' => $ghichu,
            'hoc_vien' => $hocvien,
            'don_hang' => $donhang,
            'so_tien'=> $sotien,
            'thoi_luong'=>$thoiluong,
            'tinh_trang' => $tinhtrang
        );
        $idlop = $this->model->addObj($data);
        if ($idlop > 0) {
            $homnay = date("Y-m-d");
            $ngaybatdau = functions::convertDate($_REQUEST['ngaybatdau']);
            $buoihoc = isset($_REQUEST['buoihoc']) ? $_REQUEST['buoihoc'] : 0;
            if ($buoihoc > 0 && $ngaybatdau != '') {
                $gio = $_REQUEST['giohoc'];
                $dembuoi = 0;
                $date = $ngaybatdau;
                do {
                    if (in_array(date('l', strtotime($date)), $buoihoc)) {
                        $giora = date_create($gio);
                        date_add($giora, date_interval_create_from_date_string("$thoiluong minutes"));
                        $giora = date_format($giora, "H:i:s");
                        $lichhoc = array(
                            'ngay' => $date,
                            'gio' => $gio,
                            'gio_ra' => $giora,
                            'thoi_luong' => $thoiluong,
                            'lop_hoc' => $idlop,
                            'giao_vien' => $giaovien,
                        );
                        if (strtotime($date) < strtotime($homnay))
                            $lichhoc['tinh_trang'] = 4;
                        else
                            $lichhoc['tinh_trang'] = 1;
                        if ($phanloai == 1)
                            $lichhoc['phong_hoc'] = 1;
                        $lichhocid = $this->model->ThemLichHoc($lichhoc);
                        if ($phanloai == 1 && $lichhocid > 0) {
                            $saplop = array(
                                'hoc_vien' => $hocvien,
                                'lich_hoc' => $lichhocid,
                                'lop_hoc' => $idlop,
                            );
                            if (strtotime($date) < strtotime($homnay))
                                $saplop['tinh_trang'] = 2;
                            else
                                $saplop['tinh_trang'] = 1;
                            $this->model->themSaplop($saplop);
                        }
                        $dembuoi++;
                    }
                    $date = date('Y-m-d', strtotime($date . " +1 days"));
                } while ($dembuoi < ($sobuoi + $buoikm));
            }
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $giaoviencu = $_REQUEST['giaoviencu'];
        $hocviencu = $_REQUEST['hocviencu'];
        $giaovien = $_REQUEST['giao_vien'];
        $phutrach = $_REQUEST['phu_trach'];
        $chinhanh = $_REQUEST['chi_nhanh'];
        $trogiang = $_REQUEST['tro_giang'];
        $giaotrinh = $_REQUEST['giao_trinh'];
        $khoahoc = $_REQUEST['khoa_hoc'];
        $phanloai = $_REQUEST['phan_loai'];
        $phanloai2 = $_REQUEST['phan_loai_2'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        $ghichu = $_REQUEST['ghi_chu'];
        $hocvien = $_REQUEST['hoc_vien'];
        $donhang = $_REQUEST['don_hang'];
        $sobuoi = $_REQUEST['so_buoi'];
        $buoikm = intVal($_REQUEST['buoi_khuyen_mai']);
        $thoiluong = $_REQUEST['thoi_luong'];
        $sotien = intVal($_REQUEST['so_tien']);
        $data = array(
            'name' => $name,
            'giao_vien' => $giaovien,
            'phu_trach' => $phutrach,
            'chi_nhanh' => $chinhanh,
            'giao_trinh' => $giaotrinh,
            'khoa_hoc' => $khoahoc,
            'so_buoi' => $sobuoi,
            'buoi_khuyen_mai' => $buoikm,
            'tro_giang' => $trogiang,
            'phan_loai' => $phanloai,
            'phan_loai_2' => $phanloai2,
            'ghi_chu' => $ghichu,
            'hoc_vien' => $hocvien,
            'don_hang' => $donhang,
            'thoi_luong'=>$thoiluong,
            'so_tien'=> $sotien,
            'tinh_trang' => $tinhtrang
        );
        if ($this->model->updateObj($id, $data)) {
            if ($hocvien != $hocviencu) {
                $data = ['hoc_vien' => $hocvien];
                $this->model->updateLichHoc($id, $data);
                $this->model->updateSapLop($id, $data);
            }
            if ($giaovien != $giaoviencu) {
                $data = ['giao_vien' => $giaovien];
                $this->model->updateLichHoc($id, $data);
            }
            $checklich = $this->model->checkLich($id);
            if ($checklich == 0) {
                $homnay = date("Y-m-d");
                $ngaybatdau = functions::convertDate($_REQUEST['ngaybatdau']);
                $buoihoc = isset($_REQUEST['buoihoc']) ? $_REQUEST['buoihoc'] : 0;
                if ($buoihoc > 0 && $ngaybatdau != '') {
                    $gio = $_REQUEST['giohoc'];
                    $dembuoi = 0;
                    $date = $ngaybatdau;
                    do {
                        if (in_array(date('l', strtotime($date)), $buoihoc)) {
                            $giora = date_create($gio);
                            date_add($giora, date_interval_create_from_date_string("$thoiluong minutes"));
                            $giora = date_format($giora, "H:i:s");
                            $lichhoc = array(
                                'ngay' => $date,
                                'gio' => $gio,
                                'gio_ra' => $giora,
                                'thoi_luong' => $thoiluong,
                                'lop_hoc' => $id,
                                'giao_vien' => $giaovien,
                            );
                            if (strtotime($date) < strtotime($homnay))
                                $lichhoc['tinh_trang'] = 4;
                            else
                                $lichhoc['tinh_trang'] = 1;
                            if ($phanloai == 1)
                                $lichhoc['phong_hoc'] = 1;
                            $lichhocid = $this->model->ThemLichHoc($lichhoc);
                            if ($phanloai == 1 && $lichhocid > 0) {
                                $saplop = array(
                                    'hoc_vien' => $hocvien,
                                    'lich_hoc' => $lichhocid,
                                    'lop_hoc' => $id,
                                );
                                if (strtotime($date) < strtotime($homnay))
                                    $saplop['tinh_trang'] = 2;
                                else
                                    $saplop['tinh_trang'] = 1;
                                $this->model->themSaplop($saplop);
                            }
                            $dembuoi++;
                        }
                        $date = date('Y-m-d', strtotime($date . " +1 days"));
                    } while ($dembuoi < ($sobuoi + $buoikm));
                }
            }
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function import()
    {
        $funs = $this->fun;
        $import = false;
        foreach ($funs as $item) {
            if ($item['link'] == 'nhap()') {
                $import = true;
                break;
            }
        }
        if ($import) {
            require_once ROOT_DIR . '/libs/phpexcel/PHPExcel/IOFactory.php';
            try {
                $inputFileType = PHPExcel_IOFactory::identify($_FILES['file']['tmp_name']);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($_FILES['file']['tmp_name']);
                $objReader->setReadDataOnly(true);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $banghi = 0;
                for ($row = 2; $row <= $highestRow; $row++) {
                    $tenph = $objPHPExcel->getActiveSheet()->getCell("E$row")->getValue();
                    $dataph = array(
                        'name' => $tenph,
                        'ngay' => date('Y-m-d'),
                        'nhan_vien' => $_SESSION['user']['nhan_vien'],
                        'phu_trach' => $_SESSION['user']['nhan_vien'],
                        'tinh_trang' => 1
                    );
                    $idph = $this->model->themPH($dataph);
                    $ten = $objPHPExcel->getActiveSheet()->getCell("C$row")->getValue();
                    if ($ten != '') {
                        $dienthoai = $objPHPExcel->getActiveSheet()->getCell("F$row")->getValue();
                        $email = $objPHPExcel->getActiveSheet()->getCell("G$row")->getValue();
                        $truonghoc = $objPHPExcel->getActiveSheet()->getCell("J$row")->getValue();
                        $data = array(
                            'name' => $ten,
                            'dien_thoai' => $dienthoai,
                            'email' => $email,
                            'truong_hoc' => $truonghoc,
                            'phu_huynh' => $idph,
                            'tinh_trang' => 1
                        );
                        if ($this->model->addObj($data))
                            $banghi++;
                    }
                    if ($banghi > 0) {
                        $jsonObj['msg'] = "Cập nhật thành công $banghi data";
                        $jsonObj['success'] = true;
                    } else {
                        $jsonObj['msg'] = "Lỗi cập nhật database";
                        $jsonObj['success'] = false;
                    }
                }
            } catch (Exception $e) {
                $jsonObj['msg'] = "Import dữ liệu không thành công";
                $jsonObj['success'] = false;
            }

            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }
}

?>

<?php
class duan extends controller
{
   function __construct()
   {
       parent::__construct();
       $model    = new Model();
       if ($model->checkright('duan')==false)
            header ('Location: '.URL);
   }

   function index()
   {
       require HEADER;
       $this->view->funs=$this->model->getfun('duan');
       if (MOBILE) {
          //$this->view->data = $this->model->getFetObj();
          $this->view->render('duan/index_m');
       }
       else
          $this->view->render('duan/index');
       require FOOTER;
   }

   function json(){
     		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
     		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
     		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
     		$order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
     		$offset = ($page-1)*$rows;
        $tinhtrang           = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : 1;
     		$jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tinhtrang);
     		$this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
 	 }

   // function load()
   // {
   //     $id = $_REQUEST['id'];
   //     $jsonObj = $this->model->loaddata($id);
   //     $this->view->jsonObj = json_encode($jsonObj);
   //     $this->view->render('common/json');
   // }

   function add()
   {
       $name     = $_REQUEST['name'];
       $nhanvien  = $_SESSION['user']['nhan_vien'];
       $ghichu      = $_REQUEST['ghi_chu'];
       $tinhtrang = isset($_REQUEST['tinh_trang'])?$_REQUEST['tinh_trang']:1;
       $data = array(
               'name' => $name,
               'nhan_vien' => $nhanvien,
               'ghi_chu' => $ghichu,
               'tinh_trang' => $tinhtrang
       );
       if ($this->model->addObj($data)) {
           $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
           $jsonObj['success'] = true;
       } else {
           $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
           $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

   function update()
   {
       $id        = $_REQUEST['id'];
       $name     = $_REQUEST['name'];
       $ghichu      = $_REQUEST['ghi_chu'];
       $tinhtrang = $_REQUEST['tinh_trang'];
       $data = array(
               'name' => $name,
               'ghi_chu' => $ghichu,
               'tinh_trang' => $tinhtrang
       );
       if ($this->model->updateObj($id, $data)) {
           $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
           $jsonObj['success'] = true;
       } else {
           $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
           $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

   function del()
   {
       $id   = $_REQUEST['id'];
       $temp = $this->model->delObj($id);
       if ($temp) {
           $jsonObj['msg']     = "Xóa dữ liệu thành công";
           $jsonObj['success'] = true;
       } else {
           $jsonObj['msg']     = "Xóa dữ liệu không thành công";
           $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

   function tinhtrang()
   {
       $this->view->jsonObj = '[{"id":"1","name":"Đang thực hiện"},{"id":"0","name":"Đã hoàn thành"}]';
       $this->view->render('common/json');
   }
}
?>

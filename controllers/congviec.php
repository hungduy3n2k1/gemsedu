<?php

class congviec extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('congviec') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ CÔNG VIỆC";
        require HEADER;
        $this->view->funs = $this->model->getfun('congviec');
        if (MOBILE)
            $this->view->render('congviec/index_m');
        else
            $this->view->render('congviec/index');
        require FOOTER;
    }

    function json()
    {
        $nhanvien = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : $_SESSION['user']['nhan_vien'];
        $jsonObj = $this->model->getFetObj($nhanvien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $congviec = $_REQUEST['viec'];
        $nhanvien = $_REQUEST['giaocho'];
        $deadline = functions::convertDate($_REQUEST['deadline']);
        $yeucau = $_REQUEST['yeucau'];
        $nhom = ($_REQUEST['nhom'] > 0) ? $_REQUEST['nhom'] : 1;
        $ngaygiao = date("Y-m-d");
        $nguoigiao = $_SESSION['user']['nhan_vien'];
        $tinhtrang = 1;
        if (strtotime($deadline) < strtotime($ngaygiao)) {
            $jsonObj['msg'] = "Deadline không được nhỏ hơn ngày hiện tại";
            $jsonObj['success'] = false;
        } else {
            $data = array(
                'name' => $congviec,
                'mo_ta' => $yeucau,
                'ngay_giao' => $ngaygiao,
                'deadline' => $deadline,
                'du_an' => $nhom,
                'nhan_vien' => $nhanvien,
                'nguoi_giao' => $nguoigiao,
                'tinh_trang' => $tinhtrang,
                'updated' => $ngaygiao
            );
            if ($this->model->addObj($data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function chitiet()
    {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $jsonObj = $this->model->chitiet($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        // $tt        = $_REQUEST['tt'];
        $name = $_REQUEST['name'];
        $mota = $_REQUEST['mo_ta'];
        $nguoigiao = $_REQUEST['nguoi_giao'];
        $nhanvien = $_REQUEST['nhan_vien'];
        $hancuoi = functions::convertDate($_REQUEST['hancuoi']);
        $hancuoicu = functions::convertDate($_REQUEST['hancuoicu']);
        $duan = $_REQUEST['du_an'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        $tiendo = $_REQUEST['tien_do'];
        $today = date("Y-m-d");
        $funs = $this->model->getfun('congviec');
        $data = array(
            'name' => $name,
            'du_an' => $duan,
            'mo_ta' => $mota,
            'tien_do' => $tiendo,
            'tinh_trang' => $tinhtrang,
            'updated' => $today
        );
        if ($nguoigiao == $_SESSION['user']['id'])
            $data['nhan_vien'] = $nhanvien;
        if ($tinhtrang == 4) {
            $data['tien_do'] = 100;
            $data['ngay_kt'] = $today;
        }
        if (isset($funs[128])) {
            if (strtotime($hancuoi) > strtotime($today) && $hancuoi != $hancuoicu) {
                $data['tinh_trang'] = 2;
                $data['deadline'] = $hancuoi;
            } elseif ($tinhtrang == 2)
                $data['deadline'] = $today;
        }
        if ($this->model->updateObj($id, $data, $nguoigiao)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function comment()
    {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $jsonObj = $this->model->comment($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function addcomment()
    {
        $id = $_REQUEST['id'];
        $noidung = $_REQUEST['noidung'];
        $hinhanh = $_REQUEST['hinhanh'];
        $data = array(
            'ngay_gio' => date("Y-m-d H:i:s"),
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'cong_viec' => $id,
            'noi_dung' => $noidung,
            'dinh_kem' => $hinhanh,
            'tinh_trang' => 1
        );
        if ($this->model->addcomment($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // MObile
    // function jsonmobile(){
    //       $nhanvien = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : $_SESSION['user']['nhan_vien'];
    //       $jsonObj = $this->model->getFetObj($nhanvien);
    //       $this->view->jsonObj = json_encode($jsonObj);
    //       $this->view->render('common/json');
    // }

    function giaoviec()
    {
        $id = $_REQUEST['nguoinhan'];
        $noidung = $_REQUEST['mota'];
        $deadline = functions::convertDate($_REQUEST['deadli']);
        $data = array(
            'ngay_giao' => date("Y-m-d"),
            'nguoi_giao' => $_SESSION['user']['nhan_vien'],
            'deadline' => $deadline,
            'name' => $noidung,
            'nhan_vien' => $id,
            'updated' => date('Y-m-d'),
            'du_an' => 1,
            'tinh_trang' => 1
        );
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function capnhat()
    {
        $id = $_REQUEST['viecid'];
        $name = $_REQUEST['tencongviec'];
        $nguoigiao = $_REQUEST['nguoigiao'];
        $tinhtrang = $_REQUEST['check'];
        $today = date("Y-m-d");
        $deadline = functions::convertDate($_REQUEST['thoihan']);
        $data = array(
            'name' => $name,
            'deadline' => $deadline,
            'tinh_trang' => $tinhtrang,
            'updated' => $today
        );
        if ($tinhtrang == 4) {
            $data['tien_do'] = 100;
            $data['ngay_kt'] = date('Y-m-d');
        }
        if (isset($funs[128])) {
            if ($hancuoi > $today) {
                $data['tinh_trang'] = 2;
                $data['deadline'] = $hancuoi;
            } elseif ($tinhtrang == 2)
                $data['deadline'] = $today;
        }
        if ($this->model->updateObj($id, $data, $nguoigiao)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // dung chung mobile va pc
    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"1","name":"Đang làm"},
              {"id":"2","name":"Gia hạn"},
              {"id":"3","name":"Trễ deadline","disabled":"true"},
              {"id":"4","name":"Đã xong"},
              {"id":"5","name":"Không đạt"},
              {"id":"6","name":"Hoàn thành"},
              {"id":"0","name":"Hủy"}]';
        $this->view->render('common/json');
    }

    function tinhtrang2()
    {
        $this->view->jsonObj = '[{"id":"1","name":"Đang làm","disabled":"true"},
              {"id":"2","name":"Gia hạn","disabled":"true"},
              {"id":"3","name":"Trễ deadline","disabled":"true"},
              {"id":"4","name":"Đã xong"}]';
        $this->view->render('common/json');
    }



    // function hoanthanh()
    // {
    //     $this->view->jsonObj = '[{"id":"5","name":"Không đạt"},{"id":"6","name":"Hoàn thành"}]';
    //     $this->view->render('common/json');
    // }
    //
    //
    //
    // function giahan()
    // {
    //     $id        = $_REQUEST['id'];
    //     $ngay  = ($_REQUEST['ngay']!='')?functions::convertDate($_REQUEST['ngay']):'';
    //     $tinhtrang = $_REQUEST['tinhtrang'];
    //     if ($this->model->giahan($id,$ngay,$tinhtrang)) {
    //         $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
    //         $jsonObj['success'] = true;
    //     } else {
    //         $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
    //         $jsonObj['success'] = false;
    //     }
    //     $this->view->jsonObj = json_encode($jsonObj);
    //     $this->view->render('common/json');
    // }
    //
    // function danhgia()
    // {
    //     $id      = $_REQUEST['id'];
    //     $danhgia = $_REQUEST['danhgia'];
    //     if ($this->model->danhgia($id,$danhgia)) {
    //         $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
    //         $jsonObj['success'] = true;
    //     } else {
    //         $jsonObj['msg']     = "Cập nhật dữ liệu không thành công" ;
    //         $jsonObj['success'] = false;
    //     }
    //     $this->view->jsonObj = json_encode($jsonObj);
    //     $this->view->render('common/json');
    // }
    //
    // function del()
    // {
    //     $id   = $_REQUEST['id'];
    //     $data      = array('tinh_trang' =>0, 'updated' => date("Y-m-d"));
    //     if ($this->model->updateObj($id,$data,0)) {
    //          $jsonObj['msg']     = "Xóa dữ liệu thành công";
    //         $jsonObj['success'] = true;
    //     } else {
    //         $jsonObj['msg']     = "Xóa dữ liệu không thành công";
    //         $jsonObj['success'] = false;
    //     }
    //     $this->view->jsonObj = json_encode($jsonObj);
    //     $this->view->render('common/json');
    // }


}

?>

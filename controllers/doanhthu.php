<?php
class doanhthu extends controller
{
   function __construct()
   {
       parent::__construct();
       $model    = new Model();
       if ($model->checkright('doanhthu')==false)
            header ('Location: '.URL);
   }

   function index()
   {
       $module = "BÁO CÁO DOANH THU";
       require HEADER;
       $this->view->funs=$this->model->getfun('doanhthu');
       $this->view->tongdoanhthu = $this->model->getTong();
       if (MOBILE) {
          $this->view->thang = isset($_REQUEST['thang'])?('Báo cáo doanh thu tháng '.$_REQUEST['taikhoan']):('Báo cáo doanh thu tháng '.date("m"));
          $this->view->render('doanhthu/index_m');
       }
       else
            $this->view->render('doanhthu/index');
       require FOOTER;
   }

   function json()
   {
       $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
       $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
       $offset              = ($page - 1) * $rows;
       $nam                 = isset($_REQUEST['nam']) ? $_REQUEST['nam'] : date("Y");
       $thang               = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date('m');
       $loaidh              = isset($_REQUEST['loaidh']) && $_REQUEST['loaidh']!="" ? $_REQUEST['loaidh'] : 0;
       $khachhang           = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : '';
       $jsonObj             = $this->model->getFetObj($offset, $rows, $thang, $nam, $loaidh, $khachhang);
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

   function jsonmobile()
   {
       $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
       $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
       $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
       $order               = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
       $offset              = ($page - 1) * $rows;
       $team = isset($_REQUEST['team']) ? $_REQUEST['team']:1;
       $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date("m");
       $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam']:date("Y");
       $jsonObj  = $this->model->getjson($sort, $order, $offset, $rows, $team, $thang, $nam);
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }


}
?>

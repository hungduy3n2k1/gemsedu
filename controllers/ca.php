<?php
// Module: quản lý chi nhánh địa điểm chấm công
class ca extends Controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new model();
        if ($model->checkright('ca')==false)
           header('Location: ' . URL);
    }

    function index()
    {
        require 'layouts/header.php';
        $this->view->funs=$this->model->getfun('ca');
        $this->view->render('ca/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page                = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows                = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort                = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order               = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset              = ($page - 1) * $rows;
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


    function add()
    {
        $name = $_REQUEST['ca'];
        $t2_in = $_REQUEST['t2_in'];
         $t2_out = $_REQUEST['t2_out'];
        $t3_in = $_REQUEST['t3_in'];
         $t3_out = $_REQUEST['t3_out'];
        $t4_in = $_REQUEST['t4_in'];
         $t4_out = $_REQUEST['t4_out'];
         $t5_in = $_REQUEST['t5_in'];
         $t5_out = $_REQUEST['t5_out'];
         $t6_in = $_REQUEST['t6_in'];
         $t6_out = $_REQUEST['t6_out'];
         $t7_in = $_REQUEST['t7_in'];
         $t7_out = $_REQUEST['t7_out'];
        $data      = array('ca' => $name,'t2_in' => $t2_in,'t2_out' => $t2_out,'t3_in' => $t3_in,'t3_out' => $t3_out,'t4_in' => $t4_in,'t4_out' => $t4_out,'t5_in' => $t5_in,'t5_out' => $t5_out,'t6_in' => $t6_in,'t6_out' => $t6_out,'t7_in' => $t7_in,'t7_out' => $t7_out,'tinh_trang'=>1);
        if ($this->model->addObj($data)) {
                $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
        } else {
                $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id     = $_REQUEST['id'];
        $name = $_REQUEST['ca'];
        $t2_in = $_REQUEST['t2_in'];
         $t2_out = $_REQUEST['t2_out'];
        $t3_in = $_REQUEST['t3_in'];
         $t3_out = $_REQUEST['t3_out'];
        $t4_in = $_REQUEST['t4_in'];
         $t4_out = $_REQUEST['t4_out'];
         $t5_in = $_REQUEST['t5_in'];
         $t5_out = $_REQUEST['t5_out'];
         $t6_in = $_REQUEST['t6_in'];
         $t6_out = $_REQUEST['t6_out'];
         $t7_in = $_REQUEST['t7_in'];
         $t7_out = $_REQUEST['t7_out'];
        $data      = array('ca' => $name,'t2_in' => $t2_in,'t2_out' => $t2_out,'t3_in' => $t3_in,'t3_out' => $t3_out,'t4_in' => $t4_in,'t4_out' => $t4_out,'t5_in' => $t5_in,'t5_out' => $t5_out,'t6_in' => $t6_in,'t6_out' => $t6_out,'t7_in' => $t7_in,'t7_out' => $t7_out);
        if ($this->model->updateObj($id, $data)) {
                $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
        } else {
                $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $data      = array('tinh_trang'=>0);
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg']     = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


}
?>

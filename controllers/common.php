<?php
class common extends controller
{
	 function __construct()
	 {
			 parent::__construct();
	 }

	 function index()
	 {
			 $this->view->thongbao = 'Chức năng này chưa mở';
			 $this->view->render('common/thongbao');
	 }

	 function thongbao()
	 {
			 $this->view->thongbao = 'Chức năng này chưa mở';
			 $this->view->render('common/thongbao');
	 }

	 function gioitinh()
	 {
			 $this->view->jsonObj = '[{"id":"0","name"	:"Nữ"},{"id":"1","name":"Nam"}]';
			 $this->view->render('common/json');
	 }

	 function thanhpho()
	 {
			 $jsonObj             = $this->model->thanhpho();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function menu()
	 {
			$jsonObj             = $this->model->menu();
			$this->view->jsonObj = json_encode($jsonObj['rows']);
			$this->view->render('common/json');
	  }

	 function honnhan()
	 {
			 $this->view->jsonObj = '[{"id":"0","name"	:"Chưa lập gia đình"},{"id":"1","name":"Đã lập gia đình"},{"id":"1","name":"Ly hôn"}]';
			 $this->view->render('common/json');
	 }

	 function phongban()
	 {
			 $jsonObj             = $this->model->phongban();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function chinhanh()
	 {
			 $jsonObj             = $this->model->chinhanh();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function donvitinh()
	 {
			 $jsonObj             = $this->model->donvitinh();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function ca()
	 {
			 $jsonObj             = $this->model->calamviec();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }
	function thu()
	{
		$this->view->jsonObj = '[
					{"id":"Monday","name":"Monday"},
					{"id":"Tuesday","name":"Tuesday"},
					{"id":"Wednesday","name":"Wednesday"},
					{"id":"Thursday","name":"Thursday"},
					{"id":"Friday","name":"Friday"},
					{"id":"Saturday","name":"Saturday"},
					{"id":"Sunday","name":"Sunday"}]';
		$this->view->render('common/json');
	}

	 function thang()
	 {
	 		$this->view->jsonObj = '[{"id":"01","name"	:"Tháng 1"},
					{"id":"02","name":"Tháng 2"},
					{"id":"03","name":"Tháng 3"},
					{"id":"04","name":"Tháng 4"},
					{"id":"05","name":"Tháng 5"},
					{"id":"06","name":"Tháng 6"},
					{"id":"07","name":"Tháng 7"},
					{"id":"08","name":"Tháng 8"},
					{"id":"09","name":"Tháng 9"},
					{"id":"10","name":"Tháng 10"},
					{"id":"11","name":"Tháng 11"},
					{"id":"12","name":"Tháng 12"}]';
	 		$this->view->render('common/json');
	 }

	 function nam()
	 {
	 	 $this->view->jsonObj = '[{"id":"2020","name"	:"2020"},
	 			 {"id":"2021","name":"2021"},
	 			 {"id":"2022","name":"2022"},
	 			 {"id":"2023","name":"2023"},
	 			 {"id":"2024","name":"2024"},
	 			 {"id":"2025","name":"2025"},
	 			 {"id":"2026","name":"2026"},
	 			 {"id":"2027","name":"2027"},
	 			 {"id":"2028","name":"2028"},
	 			 {"id":"2029","name":"2029"},
	 			 {"id":"2030","name":"2030"},
	 			 {"id":"2031","name":"2031"}]';
	 	 $this->view->render('common/json');
	 }

	 function trinhdo()
	 {
			 $jsonObj             = $this->model->trinhdo();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function dichvu()
	 {
			 $jsonObj             = $this->model->dichvu();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }
	 function loaidichvu()
	 {
			 $jsonObj             = $this->model->loaidichvu();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }
	 function khachhang()
	 {
			 $jsonObj             = $this->model->khachhang();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }
	 function nhacungcap()
	 {
	 		$jsonObj             = $this->model->nhacungcap();
	 		$this->view->jsonObj = json_encode($jsonObj);
	 		$this->view->render('common/json');
	 }
	 function taikhoan()
	 {
			 $jsonObj             = $this->model->taikhoan();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function loaiphieu()
	 {
			 $this->view->jsonObj = '[{"id":"0","name":"Phiếu thu"},{"id":"1","name":"Phiếu chi"}]';
			 $this->view->render('common/json');
	 }
	 function hachtoan()
	 {
			 $this->view->jsonObj = '[{"id":"1","name":"Doanh thu"},{"id":"2","name":"Chi phí"},{"id":"3","name":"Nội bộ"}]';
			 $this->view->render('common/json');
	 }
	 function hopdong()
	 {
			 $jsonObj             = $this->model->hopdong();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }
	 function chamcong()
	 {
	 		$this->view->jsonObj = '[{"id":"0","name":"Không phép"},{"id":"1","name":"Công"},{"id":"2","name":"Phép"},
					{"id":"3","name":"Đi muộn"},{"id":"4","name":"Phép nửa ngày"},{"id":"5","name":"nửa ngày"},
					{"id":"6","name":"Thêm giờ"},{"id":"7","name":"Thêm giờ nửa ngày"},{"id":"8","name":"Nghỉ lễ"},
					{"id":"9","name":"Online"}]';
	 		$this->view->render('common/json');
	 }

	 function tinhtrang()
	 {
			 $this->view->jsonObj = '[{"id":"1","name":"Bật"},{"id":"0","name":"Tắt"}]';
			 $this->view->render('common/json');
	 }
	 function sukien()
	 {
	 		$jsonObj             = $this->model->sukien();
	 		$this->view->jsonObj = json_encode($jsonObj);
	 		$this->view->render('common/json');
	 }
	 function linhvuc()
	 {
			 $jsonObj             = $this->model->linhvuc();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }
	 function nguondata()
	 {
			 $jsonObj             = $this->model->nguondata();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }
	 function lienhe()
	 {
			 $jsonObj             = $this->model->lienhe();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }
	 function contact()
	 {
			 $jsonObj             = $this->model->contact();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function duan()
   {
       $jsonObj = $this->model->duan();
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

	 function nhanvien()
	 {
			 $jsonObj             = $this->model->nhanvien();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }
	 function noiquy()
	 {
			 $jsonObj             = $this->model->noiquy();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }
	 function loaikhachhang()
	 {
			 $this->view->jsonObj = '[{"id":"-1","text":"Toàn bộ"},{"id":"1","text":"Khách hàng mới"},{"id":"2","text":"Tiềm năng"},
			 	{"id":"3","text":"Có nhu cầu"},{"id":"4","text":"Đang sử dụng DV"},{"id":"5","text":"Đã từng sử dụng DV"},
				{"id":"0","text":"Không tiềm năng"}]';
			 $this->view->render('common/json');
	 }
	 function doitac()
	 {
	 		$this->view->jsonObj = '[{"id":"1","text":"Khách hàng"},{"id":"2","text":"Nhà cung cấp"},
	 		 {"id":"3","text":"Khách hàng/nhà cung cấp"}]';
	 		$this->view->render('common/json');
	 }
	 function loaidonhang()
	 {
			 $this->view->jsonObj = '[{"id":"1","text":"Đang đàm phán"},{"id":"2","text":"Đang thực hiện"},
			 	{"id":"3","text":"Sắp hết hạn"},{"id":"4","text":"Đã gia hạn"},
				{"id":"5","text":"Đã quá hạn"},{"id":"6","text":"Hoàn thành"},{"id":"7","text":"Hủy"}]';
			 $this->view->render('common/json');
	 }
	 function phanloai()
	 {
			 $jsonObj             = $this->model->phanloai();
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	function loaikh()
	{
		$jsonObj             = $this->model->loaikh();
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}
	 function tiendo()
	 {
	 		$this->view->jsonObj = '[{"id":"1","text":"Đang làm"},{"id":"2","text":"Sắp làm"},{"id":"3","text":"Sẽ làm"},
				{"id":"4","text":"Xin gia hạn"},{"id":"5","text":"Hoàn thành"},{"id":"0","text":"Hủy"}]';
	 		$this->view->render('common/json');
	 }

	function folder()
	{
		$jsonObj = $this->model->folder();
		$this->view->jsonObj = json_encode($jsonObj['rows']);
		$this->view->render('common/json');
	}

	function folderphanquyen()
	{
		$jsonObj = $this->model->folderphanquyen();
		$this->view->jsonObj = json_encode($jsonObj['rows']);
		$this->view->render('common/json');
	}

	function thumuc()
	{
		$jsonObj = $this->model->folder();
		$this->view->jsonObj = json_encode($jsonObj['rows']);
		$this->view->render('common/json');
	}

	function bieumau()
	{
		$loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
		$jsonObj = $this->model->bieumau($loai);
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}
	function themes()
	{
		$this->view->jsonObj = '[{"id":"default","name"	:"Default"},
					{"id":"black","name":"Black"},
					{"id":"bootstrap","name":"Bootstrap"},
					{"id":"gray","name":"Gray"},
					{"id":"material","name":"Material"},
					{"id":"material-blue","name":"Material-blue"},
					{"id":"material-teal","name":"Material-teal"},
					{"id":"metro","name":"Metro"},
					{"id":"metro-blue","name":"Metro-blue"},
					{"id":"metro-gray","name":"Metro-gray"},
					{"id":"metro-green","name":"Metro-green"},
					{"id":"metro-orange","name":"Metro-orange"},
					{"id":"metro-red","name":"Metro-red"}]';
		$this->view->render('common/json');
	}

	function khoahoc()
	{
		$jsonObj = $this->model->khoahoc();
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}

	function giaotrinh()
	{
		$jsonObj = $this->model->giaotrinh();
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}

	function hocvien()
	{
		$jsonObj = $this->model->hocvien();
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}

	function khoahoctn()
	{
		$jsonObj = $this->model->khoahoctn();
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}

	function lophoctn()
	{
		$jsonObj = $this->model->lophoctn();
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}

	function lophoc()
	{
		$jsonObj = $this->model->lophoc();
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}

	function phonghoc()
	{
		$jsonObj = $this->model->phonghoc();
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}

	function giaovien()
	{
		$jsonObj = $this->model->giaovien();
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}

	function trogiang()
	{
		$jsonObj             = $this->model->trogiang();
		$this->view->jsonObj = json_encode($jsonObj);
		$this->view->render('common/json');
	}
}
?>

<?php

class diemdanh extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
//        if (!MOBILE)
//        if ($model->checkright('diemdanh') == false)
//            header('Location: ' . URL);
    }

    function index()
    {
        $module = "CHECK ATTENDANCE";
        require HEADER;
        $this->view->funs = $this->model->getfun('diemdanh');
        if (MOBILE)
            $this->view->render('diemdanh/index_m');
        else
            $this->view->render('diemdanh/index');
        require FOOTER;
    }

    function json()
    {
        $lichhoc = isset($_REQUEST['lichhoc'])?$_REQUEST['lichhoc']:0;
        $jsonObj = $this->model->getFetObj($lichhoc);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
    function lichhoc()
    {
        $giaovien = isset($_SESSION['user']['giao_vien'])?($_SESSION['user']['giao_vien']):(isset($_REQUEST['giao_vien'])?$_REQUEST['giao_vien']:0);
        $jsonObj = $this->model->lichhoc($giaovien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lichcheckin()
    {
        $giaovien = isset($_SESSION['user']['giao_vien'])?($_SESSION['user']['giao_vien']):(isset($_REQUEST['giao_vien'])?$_REQUEST['giao_vien']:0);
        $jsonObj = $this->model->lichcheckin($giaovien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lichcheckout()
    {
        $giaovien = isset($_SESSION['user']['giao_vien'])?($_SESSION['user']['giao_vien']):(isset($_REQUEST['giao_vien'])?$_REQUEST['giao_vien']:0);
        $jsonObj = $this->model->lichcheckout($giaovien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function diemdanh()
    {
        $listhv = $_REQUEST['data-hocvien'];
        $lichhoc = $_REQUEST['lichhoc'];
        $giovao = date("H:i").':00';
        if ($listhv != '') {
            $listlh = rtrim($listhv, ',');
            if ($lichhoc!='')
                $this->model->xoadiemdanh($lichhoc,$listhv);
            $temp = explode(",", $listlh);
            $banghi = 0;
            foreach ($temp as $hocvien) {
                $check = $this->model->checkdiemdanh($lichhoc, $hocvien);
                if ($check == 0) {
                    $data = array(
                        'hoc_vien' => $hocvien,
                        'lich_hoc' => $lichhoc,
                        'gio_vao'=>$giovao,
                        'tinh_trang' => 1
                    );
                    if ($this->model->themDiemDanh($data))
                        $banghi++;
                } else {
                    $banghi++;
                }
            }
            if ($banghi > 0) {
                $datalh =  $data = array(
                    'tinh_trang' => 3
                );
                $this->model->updateLichHoc($lichhoc,$datalh," tinh_trang IN (1,2) ");
                $jsonObj['msg'] = "You have successfully registered $banghi students ";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Something wrong, pls contact with admin!";
                $jsonObj['success'] = false;
            }
        } else {
            $this->model->xoadiemdanh($lichhoc,$listhv);
            $datalh =  $data = array(
                'tinh_trang' => 2
            );
            $this->model->updateLichHoc($lichhoc,$datalh," tinh_trang<4 ");
            $jsonObj['msg'] = "Pls choose a student!";
            $jsonObj['success'] = true;
        }

        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function checkin()
    {
        $lichhoc = $_REQUEST['lichhoc'];
        if ($this->model->checkin($lichhoc)) {
            $jsonObj['msg'] = "Goodbye and see you again!";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Something wrong, pls contact with admin!";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function checkout()
    {
        $lichhoc = $_REQUEST['lichhoc'];
        $link = isset($_REQUEST['link'])?$_REQUEST['link']:'';
        $link_danh_gia = isset($_REQUEST['link_danh_gia'])?$_REQUEST['link_danh_gia']:'';
        if ($this->model->checkout($lichhoc,$link,$link_danh_gia)) {
            $jsonObj['msg'] = "Goodbye and see you again!";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Something wrong, pls contact with admin!";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function huylich()
    {
        $lichhoc = $_REQUEST['lichhoc'];
        if ($this->model->huylich($lichhoc)) {
            $jsonObj['msg'] = "Success!";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Something wrong, pls contact with admin! $lichhoc";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

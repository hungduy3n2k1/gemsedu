<?php
class thanhtoanonline extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new model();
        if ($model->checkright('thanhtoanonline') == false) {
            header('Location: ' . URL);
        }
    }

    function index()
    {
        $module = "THANH TOÁN ONLINE";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('thanhtoanonline');
        $this->view->render('thanhtoanonline/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tungay = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : date("Y-m-d", strtotime("first day of this month"));
        $denngay = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : date("Y-m-d");
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : 0;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $tukhoa);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

}
?>

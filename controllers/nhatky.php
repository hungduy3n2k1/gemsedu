<?php
class nhatky extends controller
{
   function __construct()
   {
       parent::__construct();
       $model    = new Model();
       if ($model->checkright('nhatky')==false)
            header ('Location: '.URL);
   }

   function index()
   {
       require HEADER;
       $this->view->render('nhatky/index');
       require FOOTER;
   }

   function json()
   {
       $page                = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
       $rows                = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
       $sort                = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
       $order               = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
       $offset              = ($page - 1) * $rows;
       $tukhoa              = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
       $nhanvien            = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : 0;
       $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa,$nhanvien);
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

   function xuatfile()
   {
       // $this->view->nhatky = $this->model->get_hang_hoa();
       // $this->view->donvi   = $this->model->get_don_vi();
       // $this->view->render('nhatky/xuatfile');
   }

}


?>

<?php

class baocaohvkt extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new Model();
        $this->fun = $model->getfun('baocaohvkt');
        if ($model->checkright('baocaohvkt') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "HỌC VIÊN KẾT THÚC KHÓA HỌC";
        require HEADER;
        $this->view->funs = $this->fun;
        if (MOBILE)
            $this->view->render('baocaohvkt/index_m');
        else
            $this->view->render('baocaohvkt/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 30;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $hocvien = isset($_REQUEST['hocvien']) ? $_REQUEST['hocvien'] : '';
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : '';
        $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : '';
        $chuyenmon = isset($_REQUEST['chuyenmon']) ? $_REQUEST['chuyenmon'] : '';
        $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam'] : date("Y");
        $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date('m');
        $phanloaidh = isset($_REQUEST['phanloaidh']) ? $_REQUEST['phanloaidh'] : '';
        $nhanviensale = isset($_REQUEST['nhanviensale']) ? $_REQUEST['nhanviensale'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $hocvien, $khachhang, $giaovien, $chuyenmon, $thang, $nam,$phanloaidh, $nhanviensale);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function xuatfile()
    {
        // $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : '';
        // $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : '';
        // $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'ngayketthuc';
        // $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        // $offset = ($page - 1) * $rows;
        $hocvien = isset($_REQUEST['hocvien']) ? $_REQUEST['hocvien'] : '';
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : '';
        $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : '';
        $chuyenmon = isset($_REQUEST['chuyenmon']) ? $_REQUEST['chuyenmon'] : '';
        $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam'] : date("Y");
        $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date('m');
        $phanloaidh = isset($_REQUEST['phanloaidh']) ? $_REQUEST['phanloaidh'] : '';
        $nhanviensale = isset($_REQUEST['nhanviensale']) ? $_REQUEST['nhanviensale'] : '';
        $baocaohvkt = $this->model->getFetObjAll($hocvien, $khachhang, $giaovien, $chuyenmon, $thang, $nam,$phanloaidh, $nhanviensale);
        $this->view->baocaohvkt = $baocaohvkt;
        $this->view->render('baocaohvkt/xuatfile');
    }

    function phanloaisale()
    {
        $this->view->jsonObj = '[{"id":"2","name":"Tái tục trải nghiệm"},{"id":"3","name":"Tái tục new"}]';
        $this->view->render('common/json');
    }

}

?>

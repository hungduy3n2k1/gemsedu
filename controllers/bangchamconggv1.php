<?php

class bangchamconggv1 extends Controller
{
    function __construct()
    {
        parent::__construct();
        $model = new model();
//        if (!MOBILE)
//            if ($model->checkright('bangchamconggv1') == false)
//                header('Location: ' . URL);
    }

    function index()
    {
        $module = "BẢNG CHẤM CÔNG LỚP CHÍNH";
        require HEADER;
        $this->view->funs = $this->model->getfun('bangchamconggv1');
        if (MOBILE)
            $this->view->render('bangchamconggv1/index_m');
        else
            $this->view->render('bangchamconggv1/index');
        require FOOTER;
    }

    function demo()
    {
        $module = "BẢNG CHẤM CÔNG DEMO";
        require HEADER;
        if (MOBILE)
            $this->view->render('bangchamconggv1/demo_m');
        else
            $this->view->render('bangchamconggv1/demo');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $giaovien = isset($_SESSION['user']['giao_vien'])?($_SESSION['user']['giao_vien']):(isset($_REQUEST['giao_vien'])?$_REQUEST['giao_vien']:0);
        $jsonObj = $this->model->getFetObj($nam, $thang, $sort, $order, $offset, $rows,$giaovien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function chamcong()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $giaovien = isset($_SESSION['user']['giao_vien'])?($_SESSION['user']['giao_vien']):(isset($_REQUEST['giao_vien'])?$_REQUEST['giao_vien']:0);
        $jsonObj = $this->model->getFetAPI($nam, $thang, $sort, $order, $offset, $rows,$giaovien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function jsondemo()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $giaovien = isset($_SESSION['user']['giao_vien'])?($_SESSION['user']['giao_vien']):(isset($_REQUEST['giao_vien'])?$_REQUEST['giao_vien']:0);
        $jsonObj = $this->model->getCongDemo($nam, $thang, $sort, $order, $offset, $rows,$giaovien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $thang = $_REQUEST['thang'];
        $nam = $_REQUEST['nam'];
        if ($this->model->addObj($thang, $nam)) {
            $jsonObj['msg'] = "Tạo bảng chấm công thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Bảng chấm công đã tồn tại";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

<?php
class thuongphat extends controller
{
    function __construct()
    {
       parent::__construct();
       $model = new Model();
       $menuid=$model->getmodule('thuongphat');
       $role = $model->getmenu($_SESSION['user']['id']);
 		 	 if (!in_array($menuid,$role))
 		 	 	    header ('Location: '.URL);
    }

    function index()
    {
        require 'layouts/header.php';
        $this->view->module=$this->model->getmodule('thuongphat');
        $this->view->render('thuongphat/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay_gio';
        $order               = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset              = ($page - 1) * $rows;
        $ngaybd              = isset($_REQUEST['ngaybd']) ? functions::convertDate($_REQUEST['ngaybd']) : date("Y-m-d", strtotime('first day of this month'));
        $ngaykt              = isset($_REQUEST['ngaykt']) ? functions::convertDate($_REQUEST['ngaykt']) : date("Y-m-d");
        $tukhoa              = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt, $tukhoa);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $nhanvien  = $_REQUEST['nhan_vien'];
        $event     = $_REQUEST['event'];
        $ghichu    = $_REQUEST['ghi_chu'];
        $sotien    = $_REQUEST['sotien'];
        $data      = array(
            'nhan_vien' => $nhanvien,
            'ngay_gio' => date("Y-m-d H:i:s"),
            'event' => $event,
            'so_tien' => $sotien,
            'ghi_chu' => $ghichu,
            'tinh_trang' => 1
        );
        if ($this->model->addObj($data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id        = $_REQUEST['id'];
        $nhanvien  = $_REQUEST['nhan_vien'];
        $event     = $_REQUEST['event'];
        $sotien    = $_REQUEST['sotien'];
        $ghichu    = $_REQUEST['ghi_chu'];
        $ngaygio  = $_REQUEST['ngay_gio'];
        $data      = array(
          'nhan_vien' => $nhanvien,
          'ngay_gio' => $ngaygio,
          'event' => $event,
          'so_tien' => $sotien,
          'ghi_chu' => $ghichu
        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function xoa()
    {
        $id   = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg']     = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

<?php
class doanhthu extends controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new Model();
        if ($model->checkright('doanhthu')==false)
             header ('Location: '.URL);
    }

    function index()
    {
        require HEADER;
        $this->view->funs=$this->model->getfun('doanhthu');
        if(MOBILE) {
            $this->view->tungay=isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : date("Y-m-d", strtotime('first day of this month'));
            $this->view->denngay=isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : date("Y-m-d");
            $this->view->doanhthu = $this->model->doanhthu($this->view->tungay, $this->view->denngay);
            $this->view->chiphi = $this->model->chiphi($this->view->tungay, $this->view->denngay);
            $this->view->render('doanhthu/index_m');
        } else {
            $this->view->render('doanhthu/index');
        }
        require FOOTER;
    }
    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page-1)*$rows;
        $tungay              = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : date("Y-m-d", strtotime('first day of this month'));
        $denngay              = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : date("Y-m-d");
        $jsonObj = $this->model->getFetObj($tungay,$denngay);
    		$this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function jsondetail()
    {
        $id               = $_REQUEST['id'];
        $jsonObj             = $this->model->getDetail($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function chitietdoanhthu()
    {
        $tungay=isset($_REQUEST['tungay']) ? $_REQUEST['tungay'] : date("Y-m-d", strtotime('first day of this month'));
        $denngay=isset($_REQUEST['denngay']) ? $_REQUEST['denngay'] : date("Y-m-d");
        $jsonObj             = $this->model->chitietdoanhthu($tungay,$denngay);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
    function chitietchiphi()
    {
        $tungay=isset($_REQUEST['tungay']) ? $_REQUEST['tungay'] : date("Y-m-d", strtotime('first day of this month'));
        $denngay=isset($_REQUEST['denngay']) ? $_REQUEST['denngay'] : date("Y-m-d");
        $jsonObj             = $this->model->chitietchiphi($tungay,$denngay);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


}
?>

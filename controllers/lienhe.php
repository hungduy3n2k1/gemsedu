<?php
class lienhe extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('lienhe') == false) {
            header('Location: ' . URL);
        }
    }
    function index()
    {
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('lienhe');
        $this->view->render('lienhe/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'name';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // function detail()
    // {
    //     $this->view->id = $_REQUEST['id'];
    //     $this->view->render('lienhe/detail');
    // }
		//
    // function jsondetail()
    // {
    //     $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
    //     $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
    //     $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
    //     $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
    //     $offset = ($page - 1) * $rows;
    //     $lienhe = $_REQUEST['id'];
    //     $jsonObj = $this->model->get_detail($lienhe, $sort, $order, $offset, $rows);
    //     $this->view->jsonObj = json_encode($jsonObj);
    //     $this->view->render('common/json');
    // }

    function add()
    {
        $name = $_REQUEST['name'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $email = $_REQUEST['email'];
        $doitac = $_REQUEST['khach_hang'];
        $chucvu = $_REQUEST['chuc_vu'];
        $ghichu = $_REQUEST['ghi_chu'];
				$facebook = $_REQUEST['facebook'];
        $zalo = $_REQUEST['zalo'];
        $data = ['name' => $name, 'dien_thoai' => $dienthoai, 'email' => $email, 'khach_hang' => $doitac,
						'chuc_vu' => $chucvu, 'ghi_chu' => $ghichu, 'facebook'=>$facebook, 'zalo'=>$zalo, 'tinh_trang' => 1];
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $email = $_REQUEST['email'];
				$facebook = $_REQUEST['facebook'];
        $zalo = $_REQUEST['zalo'];
        $doitac = $_REQUEST['khach_hang'];
        $chucvu = $_REQUEST['chuc_vu'];
        $ghichu = $_REQUEST['ghi_chu'];
        $data = ['name' => $name, 'dien_thoai' => $dienthoai, 'email' => $email, 'khach_hang' => $doitac,
					'chuc_vu' => $chucvu, 'ghi_chu' => $ghichu, 'facebook'=>$facebook, 'zalo'=>$zalo];
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function xuatfile()
    {
        $this->view->data = $this->model->getdata();
        $this->view->render('lienhe/xuatfile');
    }
    function del()
    {
        $id = $_REQUEST['id'];
        if ($this->model->delObj($id)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

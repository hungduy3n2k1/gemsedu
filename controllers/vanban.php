<?php

class vanban extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new model();
        if ($model->checkright('vanban') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ VĂN BẢN";
        require HEADER;
        $this->view->funs = $this->model->getfun('vanban');
        $this->view->render('vanban/index');
        require FOOTER;
    }

    function funcheck()
    {
        //require 'layouts/header.php';
        $funs = $this->model->getfun('vanban');
        print_r($funs);
        //$this->view->fun = $model->checkfun('vanban');
        //$this->view->render('vanban/index');
        //require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 30;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $folder = isset($_REQUEST['folder']) ? $_REQUEST['folder'] : 1;
        $tungay = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : '';
        $denngay = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : '';
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $folder, $tungay, $denngay, $tukhoa);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // function phongban(){
    //       $this->view->jsonObj = '[{"id":"1","text":"Marketing"},{"id":"2","text":"Sales"},
    //             {"id":"3","text":"Kế Toán"},{"id":"4","text":"Nhân sự"}]';
    //           $this->view->render('common/json');
    // }

    // function tinhtrang(){
    //   $this->view->jsonObj = '[{"id":"0","text":"Chưa duyệt"},{"id":"1","text":"Đã duyệt"}]';
    // 	$this->view->render('common/json');
    // }


    function thumuc()
    {
        $jsonObj = $this->model->getthumuc();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function themphanloai()
    {
        $name = $_REQUEST['name'];
        $folder = str_replace('-', '', functions::convertname($name));
        $parid = ($_REQUEST['parentid'] != '') ? $_REQUEST['parentid'] : 0;
        $thutu = $_REQUEST['thu_tu'];
        $rootdir = ROOT_DIR . '/uploads/vanban/';
        $userid = $_SESSION['user']['id'];
        if ($parid == 0) {
            $rootdir .= $folder;
        } else {
            $parid1 = $parid;
            $thumuc = array();
            $i = 1;
            do {
                $f = $this->model->getFolder($parid1);
                $thumuc[$i] = $f[0]['folder'];
                $parid1 = $f[0]['parentid'];
                $i++;
            } while ($parid1 > 0);
            $temp = count($thumuc);
            for ($i = 0; $i < $temp; $i++)
                $rootdir .= $thumuc[$temp - $i] . '/';
        }
        $index = 0;
        do {
            if ($index > 0) {
                $folder1 = $folder . $index;
                if ($parid == 0)
                    $rootdir1 = $rootdir . $index;
                else
                    $rootdir1 = $rootdir . $folder1;
                $name1 = $name . $index;
            } else {
                $folder1 = $folder;
                if ($parid == 0)
                    $rootdir1 = $rootdir;
                else
                    $rootdir1 = $rootdir . $folder1;
                $name1 = $name;
            }
            $index++;
        } while (is_dir($rootdir1));
        $data = array(
            'name' => $name1,
            'folder' => $folder1,
            'parentid' => $parid,
            'thu_tu' => $thutu,
            'tinh_trang' => 1
        );
        if (mkdir($rootdir1 . '/', 0755, true)) {
            $fid = $this->model->themphanloai($data);
            if ($fid) {
                $jsonObj['fid'] = $parid;
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Lỗi cập nhật database";
                $jsonObj['success'] = false;
            }
        } else {
            $jsonObj['msg'] = "Lỗi khi tạo thư mục" . $rootdir1;
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function checkfolder()
    {
        $fid = $_REQUEST['id'];
        $uid = $_SESSION['user']['id'];
        if ($uid > 2)
            $check = $this->model->checkfolder($uid, $fid);
        else
            $check = 1;
        if ($check > 0)
            $jsonObj['success'] = true;
        else {
            $jsonObj['msg'] = "Bạn không có quyền làm việc tại thư mục này";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $name = $_REQUEST['name'];
        $folder = $_REQUEST['folder'];
        $i = 0;
        $fname = functions::convertname($name);
        $thumuc = array();
        if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] != '')) {
            $dir = ROOT_DIR . '/uploads/vanban/';
            $link = '';
            $parid1 = $folder;
            $i = 1;
            do {
                $f = $this->model->getFolder($parid1);
                $thumuc[$i] = $f[0]['folder'];
                $parid1 = $f[0]['parentid'];
                $i++;
            } while ($parid1 > 0);
            $temp = count($thumuc);
            for ($i = 0; $i < $temp; $i++) {
                $dir .= $thumuc[$temp - $i] . '/';
                $link .= $thumuc[$temp - $i] . '/';
            }
            $file = functions::uploadfile('file', $dir, $fname);
            $data = array(
                'ngay' => date("Y-m-d H:i:s"),
                'name' => $name,
                'folder' => $folder,
                'filename' => $file,
                'link' => $link,
                'nhan_vien' => $_SESSION['user']['id'],
                'tinh_trang' => 1);
            if ($this->model->addObj($data)) {
                $jsonObj['fid'] = $folder;
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function uploadm()
    {
        $folder = $_REQUEST['idfolder'];
        if (!empty($_FILES)) {
            $fname = $_FILES['file']['name'];
            $name = functions::convertname($fname);
            $dir = ROOT_DIR . '/uploads/vanban/';
            $link = '';
            $parid1 = $folder;
            $i = 1;
            do {
                $f = $this->model->getFolder($parid1);
                $thumuc[$i] = $f[0]['folder'];
                $parid1 = $f[0]['parentid'];
                $i++;
            } while ($parid1 > 0);
            $temp = count($thumuc);
            for ($i = 0; $i < $temp; $i++) {
                $dir .= $thumuc[$temp - $i] . '/';
                $link .= $thumuc[$temp - $i] . '/';
            }
            $data = array(
                'ngay' => date("Y-m-d H:i:s"),
                'name' => $name,
                'folder' => $folder,
                'link' => $link,
                'tinh_trang' => 1);
            if ($file = functions::uploadfile('file', $dir, $name)) {
                $data['filename'] = $file;
                if ($this->model->addObj($data))
                    echo header('refresh');
            }
        }
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $folder = $_REQUEST['folder'];
        $fname = functions::convertname($name);
        if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] != '')) {
            $dir = ROOT_DIR . '/uploads/vanban/';
            $link = '';
            $parid1 = $folder;
            $i = 1;
            do {
                $f = $this->model->getFolder($parid1);
                $thumuc[$i] = $f[0]['folder'];
                $parid1 = $f[0]['parentid'];
                $i++;
            } while ($parid1 > 0);
            $temp = count($thumuc);
            for ($i = 0; $i < $temp; $i++) {
                $dir .= $thumuc[$temp - $i] . '/';
                $link .= $thumuc[$temp - $i] . '/';
            }
            $file = functions::uploadfile('file', $dir, $fname);
            // $filepath  = URL . '/uploads/vanban/dungchung/'.$thumuc.'/' . $file;
            $data = array('name' => $name, 'folder' => $folder, 'nhan_vien' => $_SESSION['user']['id']);
            $data['filename'] = $file;
            if ($this->model->updateObj($id, $data)) {
                $jsonObj['fid'] = $folder;
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công" . $thumuc;
                $jsonObj['success'] = false;
            }
        } else {
            $data = array('name' => $name, 'nhan_vien' => $_SESSION['user']['id']);
            if ($this->model->updateObj($id, $data)) {
                $jsonObj['fid'] = $folder;
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        if ($this->model->delObj($id)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


    function duyet()
    {
        $id = $_REQUEST['id'];
        $data = array(
            'tinh_trang' => 1
        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


    function phanloai()
    {
        $jsonObj = $this->model->phanloai();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function suafolder()
    {
        $id = $_REQUEST['id'];
        $foldername = $_REQUEST['foldername'];
        $thutu = $_REQUEST['thu_tu1'];
        $data = array(
            'name' => $foldername,
            'thu_tu' => $thutu
        );
        if ($this->model->updateFolder($id, $data)) {
            $jsonObj['fid'] = $id;
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function xoafolder()
    {
        $id = $_REQUEST['id'];
        $data = array(
            'tinh_trang' => 0
        );
        if ($this->model->updateFolder($id, $data)) {
            $jsonObj['msg'] = "Đã xóa thư mục";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

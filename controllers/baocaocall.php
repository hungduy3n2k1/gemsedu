<?php
class baocaocall extends controller{
    private $fun;
    function __construct()
    {
        parent::__construct();
        $model    = new model();
        $this->fun=$model->getfun('baocaocall');
        if ($model->checkright('baocaocall')==false)
            header('Location: ' . URL);
    }

    function index()
    {
        require 'layouts/header.php';
        $this->view->funs=$this->fun;
        $this->view->render('baocaocall/index');
        require 'layouts/footer.php';
    }

    function json(){
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 30;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'phan_loai';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset = ($page-1)*$rows;
        $nhanvien = isset($_REQUEST['nhanvien'])?$_REQUEST['nhanvien']:0;
        $ngaybd = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : '';
        $ngaykt = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows,$nhanvien,$ngaybd,$ngaykt);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


}

?>

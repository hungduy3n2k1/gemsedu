<?php

class tongbuoihoc extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new Model();
        $this->fun = $model->getfun('tongbuoihoc');
        if ($model->checkright('tongbuoihoc') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "TỔNG SỐ BUỔI HỌC VIÊN";
        require HEADER;
        $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date('m');
        $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam'] : date('Y');
        $hocvien='';
        $this->view->tong = $this->model->getTong($nam, $thang,$hocvien);
        $this->view->funs = $this->fun;
        if (MOBILE)
            $this->view->render('tongbuoihoc/index_m');
        else
            $this->view->render('tongbuoihoc/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'hoc_vien';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $hocvien = isset($_REQUEST['hocvien']) ? $_REQUEST['hocvien'] : '';
        $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date('m');
        $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam'] : date('Y');
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $hocvien, $nam, $thang);
        // echo json_encode($jsonObj);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function getTong()
    {
        $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date('m');
        $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam'] : date('Y');
        $jsonObj = $this->model->getTong($nam, $thang);
    
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

}

?>

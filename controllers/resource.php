<?php
class resource extends controller
{
    function __construct()
    {
      parent::__construct();
			$model    = new Model();
			if ($model->checkright('resource')==false)
					 header ('Location: '.URL);
    }
    function index()
    {
        $module="TÀI NGUYÊN";
        require 'layouts/header.php';
        $this->view->funs=$this->model->getfun('resource');
        $this->view->render('resource/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'name';
        $order               = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset              = ($page - 1) * $rows;
        $tukhoa              = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
				$phanloai            = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : 0;
				$sohuu               = isset($_REQUEST['sohuu']) ? $_REQUEST['sohuu'] : 0;
                $nhacungcap               = isset($_REQUEST['nhacungcap']) ? $_REQUEST['nhacungcap'] : 0;
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa,$phanloai,$sohuu,$nhacungcap);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

		function copypass()
    {
        $id           = $_REQUEST['id'];
        $jsonObj = $this->model->getpass($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function updatepass()
    {
        $id         = $_REQUEST['id'];
				$username   = $_REQUEST['ten_dang_nhap'];
				$matkhaumoi = $_REQUEST['mat_khau_moi'];
        if ($matkhaumoi!='')
				    $data = array('ten_dang_nhap' => $username,'mat_khau' => $matkhaumoi);
        else
            $data = array('ten_dang_nhap' => $username);
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $name       = $_REQUEST['name'];
        $chusohuu   = $_REQUEST['chu_so_huu'];
        $nhacungcap = $_REQUEST['nha_cung_cap'];
        $link       = $_REQUEST['link'];
        $ghichu     = $_REQUEST['ghi_chu'];
        $phanloai   = $_REQUEST['phan_loai'];
        $username   = $_REQUEST['username'];
        $passwd   = $_REQUEST['passwd'];
        $data       = array(
                'name' => $name,
                'chu_so_huu' => $chusohuu,
                'nha_cung_cap' => $nhacungcap,
                'link' => $link,
                'ghi_chu' => $ghichu,
                'phan_loai' => $phanloai,
                'ten_dang_nhap' => $username,
                'mat_khau' => $passwd,
                'nguoi_tao' => $_SESSION['user']['nhan_vien'],
                'tinh_trang' => 1,
            );
        if ($this->model->addObj($data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id         = $_REQUEST['id'];
				$name       = $_REQUEST['name'];
				$chusohuu   = $_REQUEST['chu_so_huu'];
				$nhacungcap = $_REQUEST['nha_cung_cap'];
        $link       = $_REQUEST['link'];
        $ghichu     = $_REQUEST['ghi_chu'];
				$phanloai   = $_REQUEST['phan_loai'];
				$data       = array(
		            'name' => $name,
								'chu_so_huu' => $chusohuu,
								'nha_cung_cap' => $nhacungcap,
		            'link' => $link,
		            'ghi_chu' => $ghichu,
								'phan_loai' => $phanloai
		        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công".$nhanvien;
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id   = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg']     = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function chiase()
    {
        $id         = $_REQUEST['id'];
        $nguoitao   = $_REQUEST['nguoi_tao'];
		$nhanvien   = implode(",", $_REQUEST['nhanvien']);
        // $nhanvien .=  $nguoitaoBD;
		$data = array('nhan_vien' => $nhanvien,'nguoi_tao' => $nguoitao);
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


}
?>

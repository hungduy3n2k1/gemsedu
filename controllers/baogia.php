<?php
class baogia extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('baogia') == false) {
            header('Location: ' . URL);
        }
    }
    function index()
    {
        require HEADER;
        $this->view->funs = $model->getfun('baogia');
        if (MOBILE) {
            $this->view->render('baogia/index_m');
        } else {
            $this->view->render('baogia/index');
        }
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tungay = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : date("Y-m-d", strtotime('first day of this month'));
        $denngay = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : date("Y-m-d");
        $khachhang = isset($_REQUEST['doitac']) ? $_REQUEST['doitac'] : 0;
        $tinhtrang = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : 0;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $khachhang, $tinhtrang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function detail()
    {
        $this->view->id = $_REQUEST['id'];
        $this->view->render('baogia/detail');
    }

    function jsondetail()
    {
        $baogia = $_REQUEST['id'];
        $jsonObj = $this->model->get_detail($baogia);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"1","name":"Nháp"},{"id":"2","name":"Đã gửi"},{"id":"3","name":"Chốt đơn"},{"id":"4","name":"Hủy"}]';
        $this->view->render('common/json');
    }

    function add()
    {
        // thêm báo giá
        $data = $_REQUEST['baogia'];
        $khachhang = $_REQUEST['khachhang'];
        $nhanvien = $_SESSION['user']['nhan_vien'];
        $noidung = $_REQUEST['noidung'];
        $tinhtrang = $_REQUEST['tinhtrang'];
        if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
            $dir = ROOT_DIR . '/uploads/baogia/';
            $file = functions::uploadfile('file', $dir, 'bao_gia_' . $_FILES['file']['name']);
            $dinhkem = URL . '/uploads/baogia/' . $file;
        } else {
            $dinhkem = $_REQUEST['dinhkem'];
        }
        $baogia = ['ngay' => date("Y-m-d"), 'khach_hang' => $khachhang, 'noi_dung' => $noidung, 'tinh_trang' => $tinhtrang, 'nhan_vien' => $nhanvien, 'dinh_kem' => $dinhkem];
        if ($this->model->addObj($baogia, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        // xóa báo giá
        $id = $_REQUEST['id'];
        $data = ['tinh_trang' => 0];
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // Sửa báo giá
    function noidung()
    {
        $id = $_REQUEST['id'];
        $jsonObj = $this->model->noidung($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function delrow()
    {
        $id = $_REQUEST['id'];
        $data = ['tinh_trang' => 0];
        if ($this->model->updaterow($id, $data)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function saveitem()
    {
        $sanpham = $_REQUEST['dich_vu'];
        $dongia = $_REQUEST['don_gia'];
        $chietkhautm = $_REQUEST['chiet_khau_tm'];
        $chietkhaupt = $_REQUEST['chiet_khau_pt'];
        $soluong = $_REQUEST['so_luong'];
        $donvi = $_REQUEST['don_vi'];
        $thuesuat = $_REQUEST['thue_suat_vat'];
        $thang = $_REQUEST['tang_thang'];
        $tungay = functions::convertDate($_REQUEST['tungay']);
        $denngay = functions::convertDate($_REQUEST['denngay']);
        $ghichu = $_REQUEST['ghi_chu'];
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $data = [
                'dich_vu' => $sanpham,
                'don_gia' => $dongia,
                'so_luong' => $soluong,
                'don_vi' => $donvi,
                'thue_suat_vat' => $thuesuat,
                'chiet_khau_tm' => $chietkhautm,
                'chiet_khau_pt' => $chietkhaupt,
                'tang_thang' => $thang,
                'tu_ngay' => $tungay,
                'den_ngay' => $denngay,
                'ghi_chu' => $ghichu,
            ];
            $query = $this->model->updaterow($id, $data);
        } elseif (isset($_REQUEST['sophieu'])) {
            $sophieu = $_REQUEST['sophieu'];
            $data = [
                'dich_vu' => $sanpham,
                'don_gia' => $dongia,
                'so_luong' => $soluong,
                'don_vi' => $donvi,
                'thue_suat_vat' => $thuesuat,
                'chiet_khau_tm' => $chietkhautm,
                'chiet_khau_pt' => $chietkhaupt,
                'tinh_trang' => 1,
                'tang_thang' => $thang,
                'tu_ngay' => $tungay,
                'den_ngay' => $denngay,
                'ghi_chu' => $ghichu,
                'bao_gia' => $sophieu,
            ];
            $query = $this->model->insertrow($data);
        } else {
            $query = false;
        }
        if ($query) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        // update báo giá
        $id = $_REQUEST['id'];
        $noidung = $_REQUEST['noi_dung'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') {
            $dir = ROOT_DIR . '/uploads/baogia/';
            $file = functions::uploadfile('file', $dir, 'bao_gia_' . $id);
            $dinhkem = URL . '/uploads/baogia/' . $file;
        } else {
            $dinhkem = $_REQUEST['dinh_kem'];
        }
        $data = ['noi_dung' => $noidung, 'tinh_trang' => $tinhtrang, 'dinh_kem' => $dinhkem];
        if ($this->model->updateObj($id, $data)) {
            if($tinhtrang==3)
                $this->model->chot($id);
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // Gui email
    function email()
    {
        // lấy danh sách email theo id khách hàng
        $id = $_REQUEST['id'];
        $jsonObj = $this->model->email($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function sendmail()
    {
        $id = $_REQUEST['id'];
        $email = $_REQUEST['emailtosend'];
        $note = $_REQUEST['note'];
        if ($this->model->sendbaogia($id, $email, $note)) {
            $jsonObj['msg'] = "Đã gửi email báo giá tới khách hàng";

            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Gửi email không thành công".$email;
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // Chốt báo giá chuyển sang invoice
    function chot()
    {
        $id = $_REQUEST['id'];
        if ($this->model->chot($id)) {
            $jsonObj['msg'] = "Đã chốt đơn hàng";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Lỗi khi ghi dữ liệu";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

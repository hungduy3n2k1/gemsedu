<?php
class nhacungcap extends controller
{
	 function __construct()
	 {
			 parent::__construct();
			 $model    = new Model();
       if ($model->checkright('nhacungcap')==false)
            header ('Location: '.URL);
	 }
	 function index()
	 {
			 require HEADER;
			 $this->view->funs=$this->model->getfun('nhacungcap');
			 if (MOBILE)
			 		$this->view->render('nhacungcap/index_m');
			 else
			 		$this->view->render('nhacungcap/index');
			 require FOOTER;
	 }

	 function json()
	 {
			 $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
			 $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
			 $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'name';
			 $order               = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
			 $offset              = ($page - 1) * $rows;
			 $tukhoa              = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
			 $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa);
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function detail()
	 {
			 $this->view->id = $_REQUEST['id'];
			 $this->view->render('nhacungcap/detail');
	 }

	 function jsondetail()
	 {
			 $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
			 $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
			 $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay_kt';
			 $order               = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
			 $offset              = ($page - 1) * $rows;
			 $nhacungcap           = $_REQUEST['id'];
			 $jsonObj             = $this->model->get_detail($nhacungcap, $sort, $order, $offset, $rows);
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function add()
	 {
			 $maso      = $_REQUEST['ma_so'];
			 $name      = $_REQUEST['name'];
			 $fullname  = $_REQUEST['ten_day_du'];
			 $diachi    = $_REQUEST['dia_chi'];
			 $website   = $_REQUEST['website'];
			 $dienthoai   = $_REQUEST['dien_thoai'];
			 $email   	= $_REQUEST['email'];
			 $vanphong   = $_REQUEST['van_phong'];
			 $daidien   = $_REQUEST['dai_dien'];
			 $chucvu    = $_REQUEST['chuc_vu'];
			 $ghichu    = $_REQUEST['ghi_chu'];
			 $loai = $_REQUEST['loai'];
			 $data      = array(
					 'name' => $name,
					 'ma_so' => $maso,
					 'ten_day_du' => $fullname,
					 'dia_chi' => $diachi,
					 'website' => $website,
					 'dien_thoai' => $dienthoai,
					 'email' => $email,
					 'van_phong' => $vanphong,
					 'dai_dien' => $daidien,
					 'chuc_vu' => $chucvu,
					 'loai' => $loai,
					 'ghi_chu' => $ghichu,
					 'ngay' => date('Y-m-d'),
					 'phu_trach' => $_SESSION['user']['nhan_vien'],
					 'tinh_trang' => 1
			 );
			 $copy = isset($_REQUEST['copycts'])?true:false;
			 if ($this->model->addObj($data,$copy)) {
					 $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
					 $jsonObj['success'] = true;
			 } else {
					 $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
					 $jsonObj['success'] = false;
			 }
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function update()
	 {
			 $id        = $_REQUEST['id'];
			 $maso      = $_REQUEST['ma_so'];
			 $name      = $_REQUEST['name'];
			 $fullname  = $_REQUEST['ten_day_du'];
			 $diachi    = $_REQUEST['dia_chi'];
			 $website   = $_REQUEST['website'];
			 $dienthoai   = $_REQUEST['dien_thoai'];
			 $email   	= $_REQUEST['email'];
			 $vanphong   = $_REQUEST['van_phong'];
			 $daidien   = $_REQUEST['dai_dien'];
			 $chucvu    = $_REQUEST['chuc_vu'];
			 $ghichu    = $_REQUEST['ghi_chu'];
			 $phutrach = $_REQUEST['phu_trach'];
			 $loai = $_REQUEST['loai'];
			 $data      = array(
					 'name' => $name,
					 'ma_so' => $maso,
					 'ten_day_du' => $fullname,
					 'dia_chi' => $diachi,
					 'website' => $website,
					 'dien_thoai' => $dienthoai,
					 'email' => $email,
					 'van_phong' => $vanphong,
					 'dai_dien' => $daidien,
					 'chuc_vu' => $chucvu,
					 'ghi_chu' => $ghichu,
					 'phu_trach' => $phutrach,
					 'loai' => $loai
			 );
			 if ($this->model->updateObj($id,$data)) {
					 $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
					 $jsonObj['success'] = true;
			 } else {
					 $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
					 $jsonObj['success'] = false;
			 }
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
	 }

	 function del(){
			 $id = $_REQUEST['id'];
			 if($this->model->delObj($id)){
				 	$jsonObj['msg'] = "Xóa dữ liệu thành công";
					$jsonObj['success'] = true;
			 } else {
				 	$jsonObj['msg'] = "Xóa dữ liệu không thành công";
					$jsonObj['success'] = false;
			 }
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
   }

	 function lienhe()
	 {
			 $id = $_REQUEST['id'];
			 $lienhe = $this->model->lienhe($id);
			 $this->view->jsonObj = json_encode($lienhe);
			 $this->view->render('common/json');
	 }

	 function addcts()
	 {
	 		$nhacungcap = $_REQUEST['nhacungcap'];
	 		$name      = $_REQUEST['hoten'];
	 		$chucvu  = $_REQUEST['chucvu'];
	 		$dienthoai    = $_REQUEST['dienthoai'];
	 		$email   = $_REQUEST['emailcts'];
			$ghichu   = $_REQUEST['ghichu'];
	 		$data      = array(
	 				'name' => $name,
					'khach_hang' =>$nhacungcap,
	 				'chuc_vu' => $chucvu,
	 				'dien_thoai' => $dienthoai,
	 				'email' => $email,
	 				'ghi_chu' => $ghichu,
	 				'tinh_trang' => 1
	 		);
	 		if ($this->model->addCts($data)) {
	 				$jsonObj['msg']     = "Cập nhật dữ liệu thành công";
	 				$jsonObj['success'] = true;
	 		} else {
	 				$jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
	 				$jsonObj['success'] = false;
	 		}
	 		$this->view->jsonObj = json_encode($jsonObj);
	 		$this->view->render('common/json');
	 }

	 function savects()
	 {
		 	 $id = $_REQUEST['id'];
		 	 $name      = $_REQUEST['hoten'];
		 	 $chucvu  = $_REQUEST['chucvu'];
		 	 $dienthoai    = $_REQUEST['dienthoai'];
		 	 $email   = $_REQUEST['emailcts'];
		 	 $ghichu   = $_REQUEST['ghichu'];
		 	 $data      = array(
		 			 'name' => $name,
		 			 'chuc_vu' => $chucvu,
		 			 'dien_thoai' => $dienthoai,
		 			 'email' => $email,
		 			 'ghi_chu' => $ghichu
		 	 );
	 	 if ($this->model->saveCts($id,$data)) {
	 			 $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
	 			 $jsonObj['success'] = true;
	 	 } else {
	 			 $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
	 			 $jsonObj['success'] = false;
	 	 }
	 	 $this->view->jsonObj = json_encode($jsonObj);
	 	 $this->view->render('common/json');
	 }

	 function delcts(){
			 $id = $_REQUEST['id'];
			 $data = ['khach_hang' => 0];
			 if($this->model->saveCts($id,$data)){
				 	$jsonObj['msg'] = "Xóa dữ liệu thành công";
					$jsonObj['success'] = true;
			 } else {
				 	$jsonObj['msg'] = "Xóa dữ liệu không thành công";
					$jsonObj['success'] = false;
			 }
			 $this->view->jsonObj = json_encode($jsonObj);
			 $this->view->render('common/json');
   }


}
?>

<?php
class noiquy extends controller
{
   function __construct()
   {
       parent::__construct();
       $model = new Model();
       $menuid=$model->getmodule('noiquy');
       $role = $model->getmenu($_SESSION['user']['id']);
       if (!in_array($menuid,$role))
            header ('Location: '.URL);
   }

   function index()
   {
       require 'layouts/header.php';
       $this->view->render('noiquy/index');
       require 'layouts/footer.php';
   }

   function json()
   {
       $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
       $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
       $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'loai';
       $order               = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
       $offset              = ($page - 1) * $rows;
       $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows);
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

   function add()
   {
       $name      = $_REQUEST['name'];
       $loai      = $_REQUEST['loai'];
       $sotien    = str_replace(',', '', $_REQUEST['so_tien']);
       $toida     = $_REQUEST['toi_da'];
       $ghichu    = $_REQUEST['ghi_chu'];
       $tinhtrang = $_REQUEST['tinh_trang'];
       $data      = array(
           'name' => $name,
           'loai' => $loai,
           'so_tien' => $sotien,
           'toi_da' => $toida,
           'ghi_chu' => $ghichu,
           'tinh_trang' => $tinhtrang
       );
       if ($this->model->addObj($data)) {
           $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
           $jsonObj['success'] = true;
       } else {
           $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
           $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

   function update()
   {
       $id        = $_REQUEST['id'];
       $name      = $_REQUEST['name'];
       $loai      = $_REQUEST['loai'];
       $sotien    = str_replace(',', '', $_REQUEST['so_tien']);
       $toida     = $_REQUEST['toi_da'];
       $ghichu    = $_REQUEST['ghi_chu'];
       $tinhtrang = $_REQUEST['tinh_trang'];
       $data      = array(
           'name' => $name,
           'loai' => $loai,
           'so_tien' => $sotien,
           'toi_da' => $toida,
           'ghi_chu' => $ghichu,
           'tinh_trang' => $tinhtrang
       );
       if ($this->model->updateObj($id, $data)) {
           $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
           $jsonObj['success'] = true;
       } else {
           $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
           $jsonObj['success'] = false;
      }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

   function del()
   {
       $id   = $_REQUEST['id'];
       $temp = $this->model->delObj($id);
       if ($temp) {
           $jsonObj['msg']     = "Xóa dữ liệu thành công";
           $jsonObj['success'] = true;
       } else {
           $jsonObj['msg']     = "Xóa dữ liệu không thành công";
           $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }
}
?>

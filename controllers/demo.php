<?php

class demo extends Controller
{
    function __construct()
    {
        parent::__construct();
        $model = new model();
        if ($model->checkright('demo') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "LỚP HỌC DEMO";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('demo');
        $tinhtrang = array(
            1 => array('name' => 'Đang chờ', 'bg_color' => '', 'text_color' => ''),
            2 => array('name' => 'Đang học', 'bg_color' => 'blue', 'text_color' => 'white'),
            3 => array('name' => 'Đã học xong', 'bg_color' => 'red', 'text_color' => 'white')
        );
        $this->view->tinhtrang = json_encode($tinhtrang);
        $this->view->render('demo/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $phanloai = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : 0;
        // $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : 0;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $tungay = isset($_REQUEST['tungay']) && $_REQUEST['tungay'] != '' ? functions::convertDate($_REQUEST['tungay']) : '';
        $denngay = isset($_REQUEST['denngay']) && $_REQUEST['denngay'] != '' ? functions::convertDate($_REQUEST['denngay']) : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $phanloai, $tukhoa,$tungay,$denngay);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lichhoc()
    {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $jsonObj = $this->model->lichhoc($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lophoc()
    {
        $jsonObj = $this->model->lophoc();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $ngaysinh = functions::convertDate($_REQUEST['ngaysinh']);
        $dienthoai = $_REQUEST['dien_thoai'];
        $gioitinh = $_REQUEST['gioi_tinh'];
        $phanloai = $_REQUEST['phan_loai'];
        $nhanvien = $_REQUEST['nguoichamsoc'];
//        $tinhtrang = $_REQUEST['tinh_trang'];
        $ghichu = $_REQUEST['ghi_chu'];
        $data = [
            'name' => $name,
            'ngay_sinh' => $ngaysinh,
            'dien_thoai' => $dienthoai,
            'gioi_tinh' => $gioitinh,
            'phan_loai' => $phanloai,
            //  'nhan_vien' => $nhanvien,
            'ghi_chu' => $ghichu
//            'tinh_trang' => $tinhtrang
        ];
        if ($this->model->updateObj($id, $data)) {
            $datakh = [
                'phu_trach' => $nhanvien,
            ];
            $this->model->updateKH($id, $datakh);
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function getrow()
    {
        $id = $_REQUEST['id'];
        $temp = $this->model->getrow($id);
        if ($temp) {
            $jsonObj['data'] = $temp;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Không query được dữ liệu";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function sualich()
    {
        $id = $_REQUEST['idlich'];
        $ngay = functions::convertDate($_REQUEST['ngay']);
        $gio = $_REQUEST['gio'];
        $data = array('ngay' => $ngay, 'gio' => $gio);
        if ($this->model->sualich($id, $data)) {
            // $jsonObj['msg']     = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật không thành công" . $id;
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function checkphone()
    {
        $phone = $_REQUEST['phone'];
        $khachhang = $this->model->getkhachhang($phone);
        if ($khachhang != '') {
            $jsonObj['data'] = $khachhang;
            $jsonObj['msg'] = "Số điện khách hàng đã đăng ký";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Số điện thoại mới";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // function add()
    // {
    //     $khachhang = $_REQUEST['khachhang'];
    //     $sdt = $_REQUEST['sdt'];
    //     $hocvien = $_REQUEST['hocvien'];
    //     $ngaysinh = functions::convertDate($_REQUEST['bd']);
    //     $gioitinh = $_REQUEST['gioitinh'];
    //     $dienthoai = $_REQUEST['sdthv'];
    //     $phanloai = $_REQUEST['loai'];
    //     $ghichu = $_REQUEST['note'];
    //     $data = [
    //         'name' => $hocvien,
    //         'ngay_sinh' => $ngaysinh,
    //         'dien_thoai' => $dienthoai,
    //         'gioi_tinh' => $gioitinh,
    //         'phan_loai' => $phanloai,
    //         'nhan_vien' => $_SESSION['user']['nhan_vien'],
    //         'ghi_chu' => $ghichu,
    //         'tinh_trang' => 1];
    //     if ($this->model->addObj($data, $khachhang, $sdt)) {
    //         $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
    //         $jsonObj['success'] = true;
    //     } else {
    //         $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
    //         $jsonObj['success'] = false;
    //     }
    //     $this->view->jsonObj = json_encode($jsonObj);
    //     $this->view->render('common/json');
    // }

    function add()
    {
        // $iddata = $_REQUEST['khach_hang'];
        // $data = ['tinh_trang' => 7];
        // $this->model->updateData($iddata, $data);
        $ngaysinh = functions::convertDate($_REQUEST['ngay_sinh']);
        $dienthoai = $_REQUEST['dienthoai'];
        $tenhv = $_REQUEST['ten_hoc_vien'];
        $idph = $_REQUEST['khach_hang'];
        if($idph > 0){
            $phanloai = $_REQUEST['loailop'];
            $product = $_REQUEST['khoa_hoc'];
            $sobuoi = $_REQUEST['so_buoi'] != '' ? $_REQUEST['so_buoi'] : 1;
            $data = array(
                'ngay_tao' => date('Y-m-d'),
                'name' => $tenhv,
                'e_name' => $tenhv,
                'ngay_sinh' => $ngaysinh,
                'dien_thoai' => $dienthoai,
                'khach_hang' => $idph,
                'phan_loai' => $phanloai,
                'tinh_trang' => 1
            );
            $idhv = $this->model->themHocVien($data);
            if ($idhv > 0) {
                $donhang = array(
                    'khach_hang' => $idph,
                    'hoc_vien' => $idhv,
                    'so_buoi' => $sobuoi,
                    'product' => $product,
                    'so_tien' => 0,
                    'ngay_dang_ky' => date('Y-m-d'),
                    'khuyen_mai' => 0,
                    'bao_luu' => 0,
                    'da_hoc' => 0,
                    'tinh_trang' => 1,
                    'ghi_chu' => ''
                );
                $iddh = $this->model->themDonHang($donhang);
                if($iddh > 0) {
                    if ($phanloai == 15 || $phanloai == 16) {
                        $tenlop = $_REQUEST['tenlop'];
                        $giaovien = $_REQUEST['giao_vien'];
                        $giaotrinh = $_REQUEST['giao_trinh'];
                        $khoahoc = $_REQUEST['khoa_hoc'];
                        $ghichu = "Lớp học học demo từ data";
                        $sobuoi = $_REQUEST['so_buoi'];
                        $thoiluong = $_REQUEST['thoi_luong'];
                        $data = array(
                            'name' => $tenlop,
                            'giao_vien' => $giaovien,
                            'giao_trinh' => $giaotrinh,
                            'khoa_hoc' => $khoahoc,
                            'so_buoi' => $sobuoi,
                            'phan_loai' => 1,
                            'phan_loai_2' => 3,
                            'ghi_chu' => $ghichu,
                            'hoc_vien' => $idhv,
                            'don_hang' => $iddh,
                            'so_tien'=> 0,
                            'thoi_luong'=>$thoiluong,
                            'tinh_trang' => 1
                        );
                        $idlop = $this->model->ThemLopHoc($data);
                        if ($idlop > 0) {
                            $homnay = date("Y-m-d");
                            $ngaybatdau = functions::convertDate($_REQUEST['ngaybatdau']);
                            $buoihoc = isset($_REQUEST['buoihoc']) ? $_REQUEST['buoihoc'] : 0;
                            if ($buoihoc > 0 && $ngaybatdau != '') {
                                $gio = $_REQUEST['giohoc'];
                                $dembuoi = 0;
                                $date = $ngaybatdau;
                                do {
                                    if (in_array(date('l', strtotime($date)), $buoihoc)) {
                                        $giora = date_create($gio);
                                        date_add($giora, date_interval_create_from_date_string("$thoiluong minutes"));
                                        $giora = date_format($giora, "H:i:s");
                                        $lichhoc = array(
                                            'ngay' => $date,
                                            'gio' => $gio,
                                            'gio_ra' => $giora,
                                            'thoi_luong' => $thoiluong,
                                            'lop_hoc' => $idlop,
                                            'giao_vien' => $giaovien,
                                        );
                                        if (strtotime($date) < strtotime($homnay))
                                            $lichhoc['tinh_trang'] = 3;
                                        else
                                            $lichhoc['tinh_trang'] = 1;
                                        if ($phanloai == 1)
                                            $lichhoc['phong_hoc'] = 1;
                                        $lichhocid = $this->model->ThemLichHoc($lichhoc);
                                        if ($lichhocid > 0) {
                                            $saplop = array(
                                                'hoc_vien' => $idhv,
                                                'lich_hoc' => $lichhocid,
                                                'lop_hoc' => $idlop,
                                            );
                                            if (strtotime($date) < strtotime($homnay))
                                                $saplop['tinh_trang'] = 2;
                                            else
                                                $saplop['tinh_trang'] = 1;
                                            $this->model->themSaplop($saplop);
                                        }
                                        $dembuoi++;
                                    }
                                    $date = date('Y-m-d', strtotime($date . " +1 days"));
                                } while ($dembuoi < ($sobuoi));
                            }
                        }
                    }
                }

                $jsonObj['msg'] = "Đăng ký học thử thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function trainghiem()
    {
        $id = $_REQUEST['id'];
        $product = $_REQUEST['khoa_hoc'];
        $sobuoi = $_REQUEST['so_buoi'];
        $kh = $_REQUEST['kh'];
        $donhang = array(
            'khach_hang' => $kh,
            'hoc_vien' => $id,
            'so_buoi' => $sobuoi,
            'product' => $product,
            'so_tien' => 0,
            'ngay_dang_ky' => date('Y-m-d'),
            'khuyen_mai' => 0,
            'bao_luu' => 0,
            'da_hoc' => 0,
            'tinh_trang' => 1,
            'ghi_chu' => ''
        );
        $this->model->updateDonHang1($id, $kh);
        if ($this->model->themDonHang($donhang)) {
            $data = ['phan_loai' => 20];
            $this->model->updateObj($id, $data);
            $jsonObj['msg'] = "Đăng ký học thử thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function dangky()
    {
        $id = $_REQUEST['id'];
        $kh = $_REQUEST['kh'];
        $sobuoi = $_REQUEST['buoi_hoc'];
        $buoikm = $_REQUEST['buoi_hoc_km'];
        $product = $_REQUEST['product'];
        $sotien = $_REQUEST['so_tien'];
        $sotien = str_replace(",", "", $sotien);
        $ngaydangky = functions::convertDate($_REQUEST['ngay_dang_ky']);
        $dotthanhtoan = $_REQUEST['dot_thanh_toan'];
        $phanloai = $_REQUEST['loailop'];
        $donhang = array(
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'khach_hang' => $kh,
            'hoc_vien' => $id,
            'so_buoi' => $sobuoi,
            'product' => $product,
            'so_tien' => $sotien,
            'ngay_dang_ky' => $ngaydangky,
            'khuyen_mai' => $buoikm,
            'bao_luu' => 0,
            'da_hoc' => 0,
            'dot_thanh_toan' => $dotthanhtoan,
            'tinh_trang' => 1,
            'ghi_chu' => ''
        );
        $donhangid = $this->model->themDonHang($donhang);
        if ($donhangid > 0) {
            $data = ['phan_loai' => $phanloai];
            $this->model->updateObj($id, $data);
            $dot = 0;
            for ($i = 1; $i < 7; $i++) {
                $ngay = $_REQUEST['ngaydot' . $i];
                $tiendot = $_REQUEST['sotiendot' . $i];
                if ($ngay != '' && $tiendot != '') {
                    $ngay = functions::convertDate($ngay);
                    $tiendot = str_replace(",", "", $tiendot);
                    $invoice = array(
                        'ngay' => $ngay,
                        'don_hang' => $donhangid,
                        'so_tien' => $tiendot,
                        'du_no' => $tiendot,
                        'lan_1' => '0000-00-00 00:00:00',
                        'lan_2' => '0000-00-00 00:00:00',
                        'lan_3' => '0000-00-00 00:00:00',
                        'dot_thanh_toan' => $i,
                        'tinh_trang' => 1
                    );
                    $this->model->themInvoice($invoice);
                    $dot++;
                }
                $this->model->updateDonhang($donhangid, ['dot_thanh_toan' => $dot]);
            }
            $buoichot = $_REQUEST['buoi_chot'];
            $tinhtrangchot = $_REQUEST['tinh_trang_chot'];
            $databh = ['tinh_trang'=>$tinhtrangchot];
            $this->model->updateLichHoc($buoichot,$databh);
            $jsonObj['msg'] = "Đăng ký khóa học thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function chiadot()
    {
        $ngaydangky = functions::convertDate($_REQUEST['ngaydangky']);
        $sotien = str_replace(",", "", $_REQUEST['sotien']);
        $sodot = $_REQUEST['sodot'];
        $ngaydot = $ngaydangky;
        if ($sotien != '' && $ngaydangky != '') {
            for ($i = 1; $i <= $sodot; $i++) {
                if ($i > 1) {
                    $ngaydot = date('Y-m-d', strtotime("$ngaydot +1 month"));
                }
                $data[$i] = ['ngaydot' => date('d/m/Y', strtotime($ngaydot)),
                    'sotiendot' => ROUND($sotien / $sodot)];
            }
            $jsonObj['data'] = $data;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Bạn cần nhập số tiền và ngày đăng ký";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function chot()
    {
        $id = $_REQUEST['hocvien'];
        $buoichot = $_REQUEST['buoi_chot'];
        $tinhtrangchot = $_REQUEST['tinh_trang_chot'];
        $databh = ['tinh_trang'=>$tinhtrangchot];
        $this->model->updateLichHoc($buoichot,$databh);
        $loaihv = $_REQUEST['loai_hv'];
        $datahv = ['phan_loai'=>$loaihv];
        $temp = $this->model->updateObj($id,$datahv);
        if ($temp) {
            $jsonObj['msg'] = "Buổi học đã được chốt";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Lỗi khi cập nhật dữ liệu";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del(){
        $id = $_REQUEST['id'];
        $temp = $this->model->delDemo($id);
        if ($temp) {
            $jsonObj['msg'] = "Xóa thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa bản ghi không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phanloaidemo()
    {
        $jsonObj = $this->model->phanloaidemo();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phanloai()
    {
        $jsonObj = $this->model->phanloai();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function buoichot()
    {
        $hocvien = $_REQUEST['hocvien'];
        $jsonObj = $this->model->buoichot($hocvien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function tinhtrangchot(){
        $this->view->jsonObj = '[{"id":"4","name":"Hoàn thành"},
            {"id":"5","name":"Đã chốt"},
            {"id":"6","name":"Cọc ngay"}]';
        $this->view->render('common/json');
    }

    function loaihocvien(){
        $this->view->jsonObj = '[{"id":"14","name":"IG.00.KC"},
            {"id":"17","name":"IG.00.AC"}]';
        $this->view->render('common/json');
    }

}

?>

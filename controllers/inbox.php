<?php
class inbox extends controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new Model();
//        if ($model->checkright('inbox')==false)
//            header ('Location: '.URL);
    }

    function index()
    {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $data = ['tinh_trang' => 2];
            $this->model->updateThongBao($data, $id);
        }
        $module = "INBOX";
        require HEADER;
        $this->view->render('inbox/index');
        require FOOTER;
    }

    function json()
    {
        $page                = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows                = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort                = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'ngay_gio';
        $order               = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        $offset              = ($page - 1) * $rows;
        $tukhoa              = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $nhanvien            = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : '';
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa,$nhanvien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function updateDaXem()
    {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $data = ['tinh_trang' => 3];
            $this->model->updateDaXem($data, $id);
            echo 1;
        }else
            echo 0;
    }
    function updateDaDoc()
    {
        $this->model->updateDaDoc();
        echo 1;
    }

}


?>

<?php
class doanhthukh extends controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new Model();
        if ($model->checkright('doanhthukh')==false)
            header ('Location: '.URL);
    }

    function index()
    {
        $module = "BÁO CÁO DOANH THU";
        require HEADER;
        $this->view->funs=$this->model->getfun('doanhthukh');
        if (MOBILE) {
            $this->view->thang = isset($_REQUEST['thang'])?('Báo cáo doanh thu tháng '.$_REQUEST['taikhoan']):('Báo cáo doanh thu tháng '.date("m"));
            $this->view->render('doanhthukh/index_m');
        }
        else
            $this->view->render('doanhthukh/index');
        require FOOTER;
    }

    function json()
    {
        $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order               = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset              = ($page - 1) * $rows;
        $ngaybd              = isset($_REQUEST['ngaybd']) ? functions::convertDate($_REQUEST['ngaybd']) : date("Y-m-d", strtotime('first day of this month'));
        $ngaykt              = isset($_REQUEST['ngaykt']) ? functions::convertDate($_REQUEST['ngaykt']) : date("Y-m-d");
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function jsonmobile()
    {
        $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order               = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset              = ($page - 1) * $rows;
        $team = isset($_REQUEST['team']) ? $_REQUEST['team']:1;
        $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date("m");
        $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam']:date("Y");
        $jsonObj  = $this->model->getjson($sort, $order, $offset, $rows, $team, $thang, $nam);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


}
?>

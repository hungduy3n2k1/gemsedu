<?php

class khachhang extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('khachhang') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "KHÁCH HÀNG";
        require HEADER;
        $this->view->funs = $this->model->getfun('khachhang');
        if (MOBILE)
            $this->view->render('khachhang/index_m');
        else
            $this->view->render('khachhang/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $nhanvien = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : 0;
        $phutrach = isset($_REQUEST['phutrach']) ? $_REQUEST['phutrach'] : 0;
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa, $nhanvien, $phutrach, $loai);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function xuatfile()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 2000;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'name';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : 4;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $nhanvien = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : 0;
        $phutrach = isset($_REQUEST['phutrach']) ? $_REQUEST['phutrach'] : 0;
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : '';
        $khachhang = $this->model->getFetObj($sort, $order, $offset, $rows, $tinhtrang, $tukhoa, $nhanvien, $phutrach, $loai);
        $this->view->khachhang = $khachhang;
        $this->view->render('khachhang/xuatfile');
    }

    function loai()
    {
        $this->view->jsonObj = '[{"id":"1","name"	:"Online"},{"id":"2","name":"Offline"}]';
        $this->view->render('common/json');
    }

    function add()
    {
        $name = $_REQUEST['name'];
        $diachi = $_REQUEST['dia_chi'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $email = $_REQUEST['email'];
        $ghichu = $_REQUEST['ghi_chu'];
        $phutrach = $_REQUEST['phu_trach'];
        $phanloai = $_REQUEST['loai'];
        $data = array(
            'name' => $name,
            'dia_chi' => $diachi,
            'dien_thoai' => $dienthoai,
            'email' => $email,
            'ghi_chu' => $ghichu,
            'ngay' => date('Y-m-d'),
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'phu_trach' => $phutrach,
            'loai'=>$phanloai,
            'tinh_trang' => 1
        );
        // // $copy = isset($_REQUEST['copycts'])?true:false;
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $diachi = $_REQUEST['dia_chi'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $email = $_REQUEST['email'];
        $ghichu = $_REQUEST['ghi_chu'];
        $phutrach = $_REQUEST['phu_trach'];
        $phanloai = $_REQUEST['loai'];
        $data = array(
            'name' => $name,
            'dia_chi' => $diachi,
            'dien_thoai' => $dienthoai,
            'email' => $email,
            'ghi_chu' => $ghichu,
            'loai'=>$phanloai,
            'phu_trach' => $phutrach
        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        if ($this->model->delObj($id)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function contact() // lấy danh sách contact theo khách hàng để đưa vào table liên hệ
    {
        $id = $_REQUEST['id'];
        $contact = $this->model->contact($id);
        $this->view->jsonObj = json_encode($contact);
        $this->view->render('common/json');
    }

    function addcts()
    {
        $khachhang = $_REQUEST['khachhang'];
        $chucvu = $_REQUEST['chucvu'];
        $dienthoai = $_REQUEST['dienthoai'];
        $email = $_REQUEST['emailcts'];
        $ghichu = $_REQUEST['ghichu'];
        $contact = $_REQUEST['contact'];
        if (is_numeric($_REQUEST['contact'])) {
            $data = ['khach_hang' => $khachhang, 'chuc_vu' => $chucvu, 'dien_thoai' => $dienthoai, 'email' => $email, 'ghi_chu' => $ghichu];
            $ok = $this->model->saveCts($contact, $data);
        } else {
            $data = ['name' => $contact, 'khach_hang' => $khachhang, 'chuc_vu' => $chucvu, 'dien_thoai' => $dienthoai, 'email' => $email, 'ghi_chu' => $ghichu, 'tinh_trang' => 1];
            $ok = $this->model->addCts($data);
        }
        if ($ok) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function savects()
    {
        $id = $_REQUEST['id'];
        $contact = $_REQUEST['contact'];
        $chucvu = $_REQUEST['chucvu'];
        $dienthoai = $_REQUEST['dienthoai'];
        $email = $_REQUEST['emailcts'];
        $ghichu = $_REQUEST['ghichu'];
        $data = array(
            'name' => $contact,
            'chuc_vu' => $chucvu,
            'dien_thoai' => $dienthoai,
            'email' => $email,
            'ghi_chu' => $ghichu
        );
        if ($this->model->saveCts($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function delcts()
    {
        $id = $_REQUEST['id'];
        $data = ['khach_hang' => 0];
        if ($this->model->saveCts($id, $data)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function import()
    {
        require_once ROOT_DIR . '/libs/phpexcel/PHPExcel/IOFactory.php';
        try {
            $inputFileType = PHPExcel_IOFactory::identify($_FILES['file']['tmp_name']);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($_FILES['file']['tmp_name']);
            $objReader->setReadDataOnly(true);
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            $banghi = 0;
            for ($row = 6; $row <= $highestRow; $row++) {
                $name = $objPHPExcel->getActiveSheet()->getCell("B$row")->getValue();
                if ($name != '') {
                    $diachi = $objPHPExcel->getActiveSheet()->getCell("C$row")->getValue();
                    $dienthoai = $objPHPExcel->getActiveSheet()->getCell("D$row")->getValue();
                    $website = $objPHPExcel->getActiveSheet()->getCell("E$row")->getValue();
                    $data = array(
                        'name' => $name,
                        'ten_day_du' => $name,
                        'dia_chi' => $diachi,
                        'van_phong'=>$diachi,
                        'website' => $website,
                        'dien_thoai' => $dienthoai,
                        'ngay' => date('Y-m-d'),
                        'nhan_vien' => $_SESSION['user']['nhan_vien'],
                        'phu_trach' => $_SESSION['user']['nhan_vien'],
                        'tinh_trang' => 4
                    );
                    if ($this->model->addObj($data)) {
                        $banghi++;
                    }
                }
                if ($banghi > 0) {
                    $jsonObj['msg'] = "Cập nhật thành công $banghi data";
                    $jsonObj['success'] = true;
                } else {
                    $jsonObj['msg'] = "Lỗi cập nhật database";
                    $jsonObj['success'] = false;
                }
            }
        } catch (Exception $e) {
            $jsonObj['msg'] = "Import dữ liệu không thành công";
            $jsonObj['success'] = false;
        }

        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


}

?>

<?php
class kpi extends controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new Model();
        if ($model->checkright('kpi')==false)
             header ('Location: '.URL);
    }

    function index()
    {
        $module = "BÁO CÁO KPI";
        require HEADER;
        $this->view->funs=$this->model->getfun('kpi');
        if (MOBILE)
           $this->view->render('kpi/index_m');
        else
           $this->view->render('kpi/index');
        require FOOTER;
    }

    function json()
    {
        $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'nhan_vien';
        $order               = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset              = ($page - 1) * $rows;
        $ngaybd              = isset($_REQUEST['ngaybd']) ? functions::convertDate($_REQUEST['ngaybd']) : date("Y-m-d", strtotime('first day of this month'));
        $ngaykt              = isset($_REQUEST['ngaykt']) ? functions::convertDate($_REQUEST['ngaykt']) : date("Y-m-d");
        // $nhanvien            = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : $_SESSION['user']['nhan_vien'];
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function detail(){
        $this->view->index = $_REQUEST['id'];
        $this->view->ngaybd = $_REQUEST['ngaybd'];
        $this->view->ngaykt = $_REQUEST['ngaykt'];
        $this->view->render('kpi/detail');
    }

    function jsondetail(){
      $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
      $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
      $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'tinh_trang';
      $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
      $offset = ($page-1)*$rows;
      $nhanvien = $_REQUEST['nhanvien'];
      $ngaybd = isset($_REQUEST['ngaybd']) ? functions::convertDate($_REQUEST['ngaybd']) : date("Y-m-01");
      $ngaykt = isset($_REQUEST['ngaykt']) ? functions::convertDate($_REQUEST['ngaykt']) : date("Y-m-d");
      $jsonObj = $this->model->get_detail($sort, $order, $offset, $rows,$ngaybd,$ngaykt,$nhanvien);
      $this->view->jsonObj = json_encode($jsonObj);
      $this->view->render('common/json');

    }
}
?>

<?php
class phaithu extends controller{
  function __construct()
  {
      parent::__construct();
      $model    = new model();
      if ($model->checkright('phaithu')==false)
         header('Location: ' . URL);
  }

  function index()
  {
      require 'layouts/header.php';
      $this->view->funs=$model->getfun('phaithu');
      $this->view->render('phaithu/index');
      require 'layouts/footer.php';
  }

    function json(){
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
    		$rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 30;
    		$sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
    		$order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
    		$offset = ($page-1)*$rows;
    		$jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows);
    	  $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

	// function phanloai(){
  //   $this->view->jsonObj = '[{"id":"1","text":"Hợp đồng"},{"id":"2","text":"Báo giá"},
  //         {"id":"3","text":"Đề nghị tạm ứng"},{"id":"4","text":"Đề nghị thanh toán"},{"id":"5","text":"Yêu cầu tuyển dụng"}]';
	// 	$this->view->render('common/json');
	// }
  //
  // function tinhtrang(){
  //   $this->view->jsonObj = '[{"id":"0","text":"Chưa duyệt"},{"id":"1","text":"Đã duyệt"}]';
	// 	$this->view->render('common/json');
	// }

  function chitiet()
  {
      $khachhang = $_REQUEST['id'];
      $jsonObj = $this->model->getdetail($khachhang);
      $this->view->jsonObj = json_encode($jsonObj);
      $this->view->render('common/json');
  }

  function jsondetail()
  {
      $page                = isset($_POST['page']) ? intval($_POST['page']) : 1;
      $rows                = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
      $sort                = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
      $order               = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
      $offset              = ($page - 1) * $rows;
      $invoice           = $_REQUEST['id'];

  }


  function add()
  {
      $name = $_REQUEST['name'];
      $fname = functions::convertname($name);
      $phanloai = $_REQUEST['phan_loai'];
      $data      = array(
          'name' => $name,
          'phan_loai' => $phanloai,
          'tinh_trang' => 1
      );
      if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] != '')) {
             $dir   = ROOT_DIR . '/uploads/phaithu/';
             $file  = functions::uploadfile('file', $dir, $fname);
             $filepath  = URL . '/uploads/phaithu/' . $file;
             $data['filename']=$filepath;
      }
      if ($this->model->addObj($data)) {
              $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
              $jsonObj['success'] = true;
      } else {
              $jsonObj['msg']     = "Cập nhật dữ liệu không thành công".$filepath;
              $jsonObj['success'] = false;
      }
      $this->view->jsonObj = json_encode($jsonObj);
      $this->view->render('common/json');
  }

  function update()
  {
      $id = $_REQUEST['id'];
      $name = $_REQUEST['name'];
      $fname = functions::convertname($name);
      $phanloai = $_REQUEST['phan_loai'];
      $data      = array(
          'name' => $name,
          'phan_loai' => $phanloai
      );
      if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] != '')) {
             $dir   = ROOT_DIR . '/uploads/phaithu/';
             $file  = functions::uploadfile('file', $dir, $fname);
             $filepath  = URL . '/uploads/phaithu/' . $file;
             $data['filename']=$filepath;
      }
      if ($this->model->updateObj($id,$data)) {
              $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
              $jsonObj['success'] = true;
      } else {
              $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
              $jsonObj['success'] = false;
      }
      $this->view->jsonObj = json_encode($jsonObj);
      $this->view->render('common/json');
  }

  function noidung()
  {
      $id = $_REQUEST['id'];
      $noidung = $_REQUEST['noi_dung'];
      $data      = array('noi_dung' => $noidung);
      if ($this->model->updateObj($id,$data)) {
              $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
              $jsonObj['success'] = true;
      } else {
              $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
              $jsonObj['success'] = false;
      }
      $this->view->jsonObj = json_encode($jsonObj);
      $this->view->render('common/json');
  }

  function view()
  {
      require 'layouts/header.php';
      $id = $_REQUEST['id'];
      $this->view->content=$this->model->content($id);
      $this->view->render('phaithu/view');
      require 'layouts/footer.php';
  }


}

?>

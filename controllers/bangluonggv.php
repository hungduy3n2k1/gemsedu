<?php

class bangluonggv extends Controller
{
    function __construct()
    {
        parent::__construct();
        $model = new model();
        if (!MOBILE)
            if ($model->checkright('bangluonggv') == false)
                header('Location: ' . URL);
    }

    function index()
    {
        $module = "BẢNG LƯƠNG GIÁO VIÊN";
        require HEADER;
        $this->view->funs = $this->model->getfun('bangluonggv');
        if (MOBILE)
            $this->view->render('bangluonggv/index_m');
        else
            $this->view->render('bangluonggv/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $giaovien = isset($_REQUEST['giaovien'])?$_REQUEST['giaovien']:'';
        $jsonObj = $this->model->getFetObj($nam, $thang, $sort, $order, $offset, $rows,$giaovien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lapbang()
    {
        $thang = isset($_REQUEST['thang'])?$_REQUEST['thang']:date("m");
        $nam = isset($_REQUEST['nam'])?$_REQUEST['nam']:date("Y");
        if ($this->model->lapbangGioday($thang, $nam)) {
            $jsonObj['msg']     = "Đã lập lại bảng giờ dạy giáo viên tháng ".$thang.'/'.$nam;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Bảng lương tháng này đã tồn tại";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

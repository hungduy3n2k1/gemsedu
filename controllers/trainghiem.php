<?php
class trainghiem extends Controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new model();
        if ($model->checkright('trainghiem')==false)
           header('Location: ' . URL);
    }

    function index()
    {
        $module = "LỚP HỌC TRẢI NGHIỆP";
        require 'layouts/header.php';
        $this->view->funs=$this->model->getfun('trainghiem');
        $tinhtrang = array(
            1=>array('name'=>'Đang chờ','bg_color'=>'','text_color'=>''),
            2=>array('name'=>'Đang học','bg_color'=>'blue','text_color'=>'white'),
            3=>array('name'=>'Đã học xong','bg_color'=>'red','text_color'=>'white')
        );
        $this->view->tinhtrang = json_encode($tinhtrang);
        $this->view->render('trainghiem/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        // $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : 0;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows,$tukhoa);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lichhoc()
    {
        $id= isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $jsonObj = $this->model->lichhoc($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lophoc()
  	{
  		$jsonObj = $this->model->lophoc();
  		$this->view->jsonObj = json_encode($jsonObj);
  		$this->view->render('common/json');
  	}

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $ngaysinh = functions::convertDate($_REQUEST['ngaysinh']);
        $dienthoai = $_REQUEST['dien_thoai'];
        $gioitinh = $_REQUEST['gioi_tinh'];
        $nhanvien = $_REQUEST['nhan_vien'];
        //$tinhtrang = $_REQUEST['tinh_trang'];
        $ghichu = $_REQUEST['ghi_chu'];
        $data = [
            'name' => $name,
            'ngay_sinh' => $ngaysinh,
            'dien_thoai' => $dienthoai,
            'gioi_tinh' => $gioitinh,
           // 'nhan_vien' => $nhanvien,
            'ghi_chu' => $ghichu
           // 'tinh_trang' => $tinhtrang
        ];
        if ($this->model->updateObj($id, $data)) {
            $datakh = [
                'phu_trach' => $nhanvien,
            ];
            $this->model->updateKH($id,$datakh);
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công" ;
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function getrow(){
        $id = $_REQUEST['id'];
        $temp = $this->model->getrow($id);
        if($temp){
            $jsonObj['data'] = $temp;
            $jsonObj['success'] = true;
        }else {
            $jsonObj['msg'] = "Không query được dữ liệu";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function sualich()
    {
        $id = $_REQUEST['idlich'];
        $ngay = functions::convertDate($_REQUEST['ngay']);
        $gio = $_REQUEST['gio'];
        $data      = array('ngay'=>$ngay, 'gio'=>$gio);
        if ($this->model->sualich($id,$data)) {
            // $jsonObj['msg']     = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật không thành công".$id;
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function checkphone(){
        $phone = $_REQUEST['phone'];
        $khachhang = $this->model->getkhachhang($phone);
        if($khachhang!=''){
            $jsonObj['data'] = $khachhang;
            $jsonObj['msg'] = "Số điện khách hàng đã đăng ký";
            $jsonObj['success'] = true;
        }else {
            $jsonObj['msg'] = "Số điện thoại mới";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $khachhang = $_REQUEST['khachhang'];
        $sdt = $_REQUEST['sdt'];
        $hocvien = $_REQUEST['hocvien'];
        $ngaysinh = functions::convertDate($_REQUEST['bd']);
        $gioitinh = $_REQUEST['gioitinh'];
        $dienthoai = $_REQUEST['sdthv'];
        $phanloai = 5;
        $ghichu = $_REQUEST['note'];
        $lophoc = $_REQUEST['lop_hoc'];
        $data = [
            'name' => $hocvien,
            'ngay_sinh' => $ngaysinh,
            'dien_thoai' => $dienthoai,
            'gioi_tinh' => $gioitinh,
            'phan_loai' => $phanloai,
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'ghi_chu' => $ghichu,
            'tinh_trang' => 1];
        if ($this->model->addObj($data,$khachhang,$sdt,$lophoc)) {
                $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
        } else {
                $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function dangky()
    {
        $id = $_REQUEST['id'];
        $kh = $_REQUEST['kh'];
        $sobuoi = $_REQUEST['buoi_hoc'];
        $buoikm = $_REQUEST['buoi_hoc_km'];
        $product = $_REQUEST['product'];
        $sotien = $_REQUEST['so_tien'];
        $sotien = str_replace(",","",$sotien);
        $ngaydangky = functions::convertDate($_REQUEST['ngay_dang_ky']);
        $dotthanhtoan = $_REQUEST['dot_thanh_toan'];
        $phanloai = $_REQUEST['loailop'];
        $donhang = array(
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'khach_hang' => $kh,
            'hoc_vien' => $id,
            'so_buoi' => $sobuoi,
            'product' => $product,
            'so_tien' => $sotien,
            'ngay_dang_ky' => $ngaydangky,
            'khuyen_mai' => $buoikm,
            'bao_luu' => 0,
            'da_hoc' => 0,
            'dot_thanh_toan'=>$dotthanhtoan,
            'tinh_trang' => 1,
            'ghi_chu' => ''
        );
        $donhangid=$this->model->themDonHang($donhang);
        if ($donhangid>0) {
            $data = ['phan_loai' => $phanloai];
            $this->model->updateObj($id, $data);
            $dot = 0;
            for ($i = 1; $i < 7; $i++) {
                $ngay = $_REQUEST['ngaydot' . $i];
                $tiendot = $_REQUEST['sotiendot' . $i];
                if ($ngay != '' && $tiendot != '') {
                    $ngay = functions::convertDate($ngay);
                    $tiendot = str_replace(",", "", $tiendot);
                    $invoice = array(
                        'ngay' => $ngay,
                        'don_hang' => $donhangid,
                        'so_tien' => $tiendot,
                        'du_no' => $tiendot,
                        'lan_1' => '0000-00-00 00:00:00',
                        'lan_2' => '0000-00-00 00:00:00',
                        'lan_3' => '0000-00-00 00:00:00',
                        'dot_thanh_toan' => $i,
                        'tinh_trang' => 1
                    );
                    $this->model->themInvoice($invoice);
                    $dot++;
                }
                $this->model->updateDonhang($donhangid, ['dot_thanh_toan' => $dot]);
            }
            $jsonObj['msg'] = "Đăng ký khóa học thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function chiadot()
    {
        $ngaydangky = functions::convertDate($_REQUEST['ngaydangky']);
        $sotien = str_replace(",", "", $_REQUEST['sotien']);
        $sodot = $_REQUEST['sodot'];
        $ngaydot = $ngaydangky;
        if ($sotien != '' && $ngaydangky != '') {
            for ($i = 1; $i <= $sodot; $i++) {
                if ($i > 1) {
                    $ngaydot = date('Y-m-d', strtotime("$ngaydot +1 month"));
                }
                $data[$i] = ['ngaydot' => date('d/m/Y', strtotime($ngaydot)),
                    'sotiendot' => ROUND($sotien / $sodot)];
            }
            $jsonObj['data'] = $data;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Bạn cần nhập số tiền và ngày đăng ký";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phanloai()
    {
        $jsonObj = $this->model->phanloai();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phanloaidemo()
    {
        $jsonObj = $this->model->phanloaidemo();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

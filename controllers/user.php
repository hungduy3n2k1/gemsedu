<?php

class user extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new model();
        if ($model->checkright('user') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ NGƯỜI DÙNG";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('user');
        $this->view->render('user/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
        $nhanvien = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : '';
        $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $email, $nhanvien, $giaovien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $username = $_REQUEST['email'];
        if ($this->model->dupliObj($username, 0)) {
            $jsonObj['msg'] = "Tên đăng nhập không khả dụng";
            $jsonObj['success'] = false;
        } else {
            $nhanvien = $_REQUEST['nhan_vien'];
            $giaovien = $_REQUEST['giao_vien'];
            $nhom = $_REQUEST['nhom'];
            $matkhau = (isset($_REQUEST['matkhau']) AND ($_REQUEST['matkhau'] != '')) ? $_REQUEST['matkhau'] : 'vdata123';
            $data = array(
                'email' => $username,
                'nhan_vien' => $nhanvien,
                'giao_vien' => $giaovien,
                'nhom' => $nhom,
                'mat_khau' => md5(md5($matkhau)),
                'tinh_trang' => 1,
            );
            if ($this->model->addObj($data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $username = $_REQUEST['email'];
        if ($this->model->dupliObj($username, $id)) {
            $jsonObj['msg'] = "Tên đăng nhập không khả dụng";
            $jsonObj['success'] = false;
        } else {
            $nhanvien = $_REQUEST['nhan_vien'];
            $nhom = $_REQUEST['nhom'];
            $matkhau = $_REQUEST['matkhau'];
            $giaovien = $_REQUEST['giao_vien'];
            $data = array(
                'email' => $username,
                'nhan_vien' => $nhanvien,
                'giao_vien' => $giaovien,
                'nhom' => $nhom
            );
            if ($matkhau != '')
                $data['mat_khau'] = md5(md5($matkhau));
            if ($this->model->updateObj($id, $data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        if ($this->model->delObj($id)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function saverole()
    {
        $id = $_REQUEST['id'];
        $menu = $_REQUEST['role'];
        $data = array('menu' => $menu);
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function saverolef()
    {
        $id = $_REQUEST['id'];
        $thumuc = $_REQUEST['rolef'];
        $data = array('thu_muc' => $thumuc);
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

<?php
// Module: quản lý chi nhánh địa điểm chấm công
class chinhanh extends Controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new model();
        if ($model->checkright('chinhanh')==false)
           header('Location: ' . URL);
    }

    function index()
    {
        require 'layouts/header.php';
        $this->view->funs=$this->model->getfun('chinhanh');
        $this->view->render('chinhanh/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page                = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows                = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort                = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order               = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset              = ($page - 1) * $rows;
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


    function add()
    {
        $name = $_REQUEST['name'];
        $diachi = $_REQUEST['dia_chi'];
        $ip = $_REQUEST['ip'];
        $data      = array('name' => $name,'dia_chi' => $diachi,'ip' => $ip,'tinh_trang'=>1);
        if ($this->model->addObj($data)) {
                $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
        } else {
                $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id     = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $diachi = $_REQUEST['dia_chi'];
        $ip = $_REQUEST['ip'];
        $data      = array('name' => $name,'dia_chi' => $diachi,'ip' => $ip);
        if ($this->model->updateObj($id, $data)) {
                $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
        } else {
                $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $data      = array('tinh_trang'=>0);
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg']     = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


}
?>

<?php

class index extends controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        require(HEADER);
        if ($_SESSION['user']['nhom'] != 16) {
            if ($this->model->chamcong()) {
                echo '<script>show_messager("Chào mừng bạn đến với VPDT công ty GEMS TECH!")</script>';
                // $this->view->thongbao = 'Chào mừng bạn đến với VPDT công ty VDATA!';
                // $this->view->render('common/thongbao');
            }
            $this->model->deadline();
            // $this->model->updatePhep();
            $this->model->ketthuclop();
            $thang = isset($_REQUEST['thang']) && $_REQUEST['thang'] != '' ? $_REQUEST['thang'] : date('m');
            $nam = isset($_REQUEST['nam']) && $_REQUEST['nam'] != '' ? $_REQUEST['nam'] : date('Y');
            $this->view->doanhthu = $this->doanhthu($thang, $nam);
            $this->view->thang = $thang;
            $this->view->nam = $nam;
            if (MOBILE)
                $this->view->render('index/index_m');
            else
                $this->view->render('index/index');
        } else {
            if (MOBILE)
                $this->view->render('index/index_gv_m');
            else
                $this->view->render('index/index_gv');
        }
        require(FOOTER);
    }

    function updatePhep()
    {
        $this->model->updatePhep();
    }

    function matkhau()
    {
        require(HEADER);
        if (MOBILE)
            $this->view->render('index/index_m');
        else
            $this->view->render('index/matkhau');
        require(FOOTER);
    }

    function deadline()
    {
        $this->model->deadline();
    }

    function changepass()
    {
        $matkhaucu = md5(md5($_REQUEST['matkhaucu']));
        $matkhaumoi = md5(md5($_REQUEST['matkhaumoi']));
        $retype = md5(md5($_REQUEST['retype']));
        $userid = isset($_REQUEST['userid']) ? $_REQUEST['userid'] : $_SESSION['user']['id'];
        if ($this->model->checkpass($userid, $matkhaucu)) {
            if ($matkhaumoi == $retype) {
                if ($this->model->changepass($userid, $matkhaumoi)) {
                    $jsonObj['msg'] = "Đổi mật khẩu thành công";
                    $jsonObj['success'] = true;
                } else {
                    $jsonObj['msg'] = "Đổi mật khẩu không thành công";
                    $jsonObj['success'] = false;

                }
            } else {
                $jsonObj['msg'] = "Xác nhận mật khẩu không đúng";
                $jsonObj['success'] = false;
            }
        } else {
            $jsonObj['msg'] = "Mật khẩu cũ không đúng";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function checkToken()
    {
        $checktoken = false;
        if (isset($_REQUEST['token']) && $_REQUEST['token'] != '') {
            $checktoken = $this->model->checkToken($_REQUEST['token']);
        }
        if ($checktoken == true) {
            $jsonObj['msg'] = "Token đã tồn tại";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Token không tồn tại";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function logout()
    {
        // $this->model->logout();
        $token = $_REQUEST['token'];
        session_destroy();
        $this->model->unsetToken($token);
        header('Location: ' . URL);
    }

// DASHBOARD
    function congviec()
    {
        $jsonObj = $this->model->congviec();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function thongbao()
    {
        $jsonObj = $this->model->thongbao();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function getthongbao()
    {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $jsonObj = $this->model->getthongbao($id);
        $jsonObj = json_encode($jsonObj);
        echo $jsonObj;
    }

    function invoice()
    {
        $jsonObj = $this->model->getInvoice();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function doanhthu($thang, $nam)
    {
        $tongtien = 0;
        $tongthucte = 0;
        $doanhthu = $this->model->doanhthu($thang, $nam);
        $dtnhanvien = '';
        foreach ($doanhthu as $item) {
            $muctieu = $item['mt_doanh_thu'] / 1000000;
            $thucte = $item['thucte'] / 1000000;
            $kpi = round($thucte / $muctieu * 100, 2);
            $nhanvien = explode(' ', $item['nhanvien']);
            $tennv = $nhanvien[count($nhanvien) - 1];
            $dtnhanvien .= '{name: "' . $tennv . '",y: ' . $kpi . ',value:' . $thucte . ',target:' . $muctieu . '},';
            $tongtien += $muctieu;
            $tongthucte += $thucte;
        }
        $dtnhanvien = rtrim($dtnhanvien, ',');
        if ($tongtien > 0)
            $tongkpi = round($tongthucte / $tongtien * 100, 2);
        else
            $tongkpi = 0;
        $tongdoanhthu = '{name: "All",y: ' . $tongkpi . ',value:' . $tongthucte . ',target:' . $tongtien . '}';
        return $tongdoanhthu . ',' . $dtnhanvien;
    }

    function ketthuclop()
    {
        $this->model->ketthuclop();
    }


// Notification
    function events()
    {
        $jsonObj = $this->model->events();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function eventstop()
    {
        $id = $_REQUEST['id'];
        $this->model->eventstop($id);
    }

    function ketthuc()
    {
        $jsonObj = $this->model->getHVKT();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function login()
    {
        if (isset($_REQUEST['username'])) {
            $username = str_replace("'", "", $_REQUEST['username']);
            $password = md5(md5($_REQUEST['password']));
            $jsonObj['success'] = true;
            $data = $this->model->login($username, $password);
            if (count($data)) {
                if ($data['giao_vien'] == 0) {
                    $jsonObj['success'] = false;
                    $jsonObj['msg'] = 'Bạn không phải giáo viên';
                    $this->logout();
                } else {
                    $jsonObj['success'] = true;
                    $jsonObj['data'] = $data;
                }
            } else {
                $jsonObj['success'] = false;
                $jsonObj['msg'] = 'Bạn nhập sai username hoặc mật khẩu';
            }
        } else {
            $jsonObj['success'] = false;
            $jsonObj['msg'] = 'Bạn nhập sai username hoặc mật khẩu';
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lichdaythay()
    {
        $jsonObj = $this->model->getlichdaythay();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

<?php

class hoadon extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('hoadon') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "BẢNG KÊ HÓA ĐƠN";
        require HEADER;
        $this->view->funs = $this->model->getfun('hoadon');
        if (MOBILE)
            $this->view->render('hoadon/index_m');
        else
            $this->view->render('hoadon/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 30;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'ngay';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tungay = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : date("Y-m-d", strtotime('first day of this month'));
        $denngay = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : date("Y-m-d");
        $loai = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : 0;
        $doitac = isset($_REQUEST['doitac']) ? $_REQUEST['doitac'] : 0;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $loai, $doitac,$tukhoa);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function loaihoadon()
    {
        $this->view->jsonObj = '[{"id":"0","name"	:"Mua vào"},{"id":"1","name":"Bán ra"}]';
        $this->view->render('common/json');
    }

    function doitac()

    {

        $jsonObj = $this->model->doitac();

        $this->view->jsonObj = json_encode($jsonObj);

        $this->view->render('common/json');

    }

    function khachhang()

    {

        $jsonObj = $this->model->khachhang();

        $this->view->jsonObj = json_encode($jsonObj);

        $this->view->render('common/json');

    }

    function detail()
    {
        $this->view->id = $_REQUEST['id'];
        $this->view->render('hoadon/detail');
    }

    function jsondetail()
    {
        $hoadon = $_REQUEST['id'];
        $jsonObj = $this->model->get_detail($hoadon);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $ngay = functions::convertDate($_REQUEST['ngaygio']);
        $sohoadon = $_REQUEST['name'];
        $loai = $_REQUEST['phan_loai'];
        $doitac = $_REQUEST['doi_tac'];
        $diachi = $_REQUEST['dia_chi'];
        $masothue = $_REQUEST['ma_so_thue'];
        $sotien = $_REQUEST['so_tien'];
        $thuevat = $_REQUEST['thue_vat'];
        $ghichu = $_REQUEST['ghi_chu'];
        $dinhkem = $_REQUEST['dinh_kem'];
        $noidung = $_REQUEST['noi_dung'];
        if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] != '')) {
            $dir = ROOT_DIR . '/uploads/hoadon/';
            $file = functions::uploadfile('file', $dir, $sohoadon);
            $dinhkem = URL . '/uploads/hoadon/' . $file;
        }
        $data = array(
            'ngay' => $ngay,
            'name' => $sohoadon,
            'phan_loai' => $loai,
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'doi_tac' => $doitac,
            'dia_chi' => $diachi,
            'ma_so_thue' => $masothue,
            'so_tien' => $sotien,
            'thue_vat' => $thuevat,
            'dinh_kem' => $dinhkem,
            'ghi_chu' => $ghichu,
            'noi_dung'=>$noidung,
            'tinh_trang' => 1
        );
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $ngay = functions::convertDate($_REQUEST['ngaygio']);
        $sohoadon = $_REQUEST['name'];
        $loai = $_REQUEST['phan_loai'];
        $doitac = $_REQUEST['doi_tac'];
        $diachi = $_REQUEST['dia_chi'];
        $masothue = $_REQUEST['ma_so_thue'];
        $sotien = $_REQUEST['so_tien'];
        $thuevat = $_REQUEST['thue_vat'];
        $ghichu = $_REQUEST['ghi_chu'];
        $dinhkem = $_REQUEST['dinh_kem'];
        $noidung = $_REQUEST['noi_dung'];
        if (isset($_FILES['file']['name']) && ($_FILES['file']['name'] != '')) {
            $dir = ROOT_DIR . '/uploads/hoadon/';
            $file = functions::uploadfile('file', $dir, $sohoadon);
            $dinhkem = URL . '/uploads/hoadon/' . $file;
        }
        $data = array(
            'ngay' => $ngay,
            'name' => $sohoadon,
            'phan_loai' => $loai,
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'doi_tac' => $doitac,
            'dia_chi' => $diachi,
            'ma_so_thue' => $masothue,
            'so_tien' => $sotien,
            'thue_vat' => $thuevat,
            'dinh_kem' => $dinhkem,
            'noi_dung'=>$noidung,
            'ghi_chu' => $ghichu
        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $data['tinh_trang'] = 0;
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function xuatfile()
    {
        $nhanvien = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : 0;
        $nhacungcap = isset($_REQUEST['nhacungcap']) ? $_REQUEST['nhacungcap'] : 0;
        $hinhthuc = isset($_REQUEST['hinhthuc']) ? $_REQUEST['hinhthuc'] : 0;
        $tungay = isset($_REQUEST['tungay']) ? date("Y-m-d", strtotime($_REQUEST['tungay'])) : date("Y-m-d");
        $denngay = isset($_REQUEST['denngay']) ? date("Y-m-d", strtotime($_REQUEST['denngay'])) : date("Y-m-d");
        $this->view->phieunhap = $this->model->getFetObj('id', 'DESC', 0, 1000000, $tungay, $denngay, $nhacungcap, $nhanvien, $hinhthuc);
        $this->view->tungay = date("d/m/Y", strtotime($tungay));
        $this->view->denngay = date("d/m/Y", strtotime($denngay));
        $this->view->render('hoadon/xuatfile');
    }

}

?>

<?php

class giaotrinhgv extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $module = "QUẢN LÝ GIÁO TRÌNH";
        require 'layouts/header.php';
        $this->view->funs = $this->fun;
        $this->view->render('giaotrinhgv/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 30;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'level ASC, id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $giaovien = isset($_SESSION['user']['giao_vien'])?($_SESSION['user']['giao_vien']):(isset($_REQUEST['giao_vien'])?$_REQUEST['giao_vien']:0);
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa,$giaovien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>
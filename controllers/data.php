<?php

class data extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new model();
        $this->fun = $model->getfun('data');
        if ($model->checkright('data') == false) {
            header('Location: ' . URL);
        }
    }

    function index()
    {
        $module = "DATA KHÁCH HÀNG";
        require 'layouts/header.php';
        $funs = $this->view->funs = $this->fun;
        $chia = 0;
        foreach ($funs as $item) {
            if ($item['link'] == 'chia()') {
                $chia = 1;
                break;
            }
        }
        $this->view->chiadata = $chia;
        $tinhtrang = $this->model->gettinhtrang();
        $this->view->tinhtrang = json_encode($tinhtrang);
        $this->view->render('data/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
//        if (isset($_REQUEST['nhanvien']))
        $nhanvien = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : 0;
//        else
//            $nhanvien = $_SESSION['user']['nhan_vien'];
        $nguoinhap = isset($_REQUEST['nguoinhap']) ? $_REQUEST['nguoinhap'] : 0;
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : 0;
        $tungay = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : '0000-00-00';
        $denngay = (isset($_REQUEST['denngay']) && ($_REQUEST['denngay'] != '')) ? functions::convertDate($_REQUEST['denngay']) : date('Y-m-d');
        $loctrung = isset($_REQUEST['loctrung']) ? $_REQUEST['loctrung'] : 0;
        $phanloai = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : 0;
        $kieungay = isset($_REQUEST['kieungay']) ? $_REQUEST['kieungay'] : 1;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tinhtrang, $nhanvien, $tungay, $denngay, $loctrung, $nguoinhap, $kieungay, $tukhoa, $phanloai);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

// function tinhtrang()
// {
//     $jsonObj = $this->model->gettinhtrang($sort
//     $this->view->jsonObj = '[{"id":"1","name":"Mới"},
//        {"id":"2","name":"Đã chia"},
//        {"id":"3","name":"Thuê bao"},
//        {"id":"4","name":"Không nghe máy"},
//        {"id":"5","name":"Không quan tâm"},
//        {"id":"6","name":"Chăm sóc tiếp"},
//        {"id":"7","name":"Đã chốt"},
//        {"id":"8","name":"Bỏ qua"}]';
//     $this->view->render('common/json');
// }

    function phuhuynh()
    {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $jsonObj = $this->model->phuhuynh($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lichsu()
    {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
        $jsonObj = $this->model->lichsu($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function addph()
    {
        $hocvien = $_REQUEST['hocvien'];
        $dienthoai = $_REQUEST['dienthoaiphuhuynh'];
        $hoten = $_REQUEST['hotenphuhuynh'];
        $data = ['hoc_vien' => $hocvien,
            'ho_ten' => $hoten,
            'dien_thoai' => $dienthoai,
            'tinh_trang' => 1];
        if ($this->model->addph($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function addnhatky()
    {
        $hocvien = $_REQUEST['idhocvien'];
        $nhatky = $_REQUEST['nhatky'];
        $data = ['hoc_vien' => $hocvien,
            'ngay_gio' => date('Y-m-d H:i:s'),
            'ghi_chu' => $nhatky,
            'tinh_trang' => 1];
        if ($this->model->addnhatky($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function updateData()
    {
        $id = $_REQUEST['id'];
        $tenkh = $_REQUEST['ho_ten'];
//        $ngayhoc = functions::convertDate($_REQUEST['ngayhoc']);
//        $giohoc = $_REQUEST['gio_hoc'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $ghichu = $_REQUEST['ghi_chu'];
        $email = $_REQUEST['email'];
        $nhanvien = $_REQUEST['nhan_vien'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        $tenhocvien = $_REQUEST['hoc_vien'];
        $loaikh = $_REQUEST['phan_loai'];
        $nguondata = $_REQUEST['nguon_data'] != '' ? $_REQUEST['nguon_data'] : 0;
        $data = [
            'ho_ten' => $tenkh,
//            'ngay_hoc' => $ngayhoc,
//            'gio_hoc' => $giohoc,
            'dien_thoai' => $dienthoai,
            'email' => $email,
            'ghi_chu' => $ghichu,
            'nhan_vien' => $nhanvien,
            'hoc_vien' => $tenhocvien,
            'phan_loai' => $loaikh,
            'nguon_data' => $nguondata,
            'tinh_trang' => $tinhtrang];
        if ($this->model->updateData($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $tenkh = $_REQUEST['ho_ten'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $ghichu = $_REQUEST['ghi_chu'];
        $email = $_REQUEST['email'];
        $tenhocvien = $_REQUEST['hoc_vien'];
        $loaikh = $_REQUEST['phan_loai'];
        $nguondata = $_REQUEST['nguon_data'] != '' ? $_REQUEST['nguon_data'] : 0;
        $data = [
            'ngay_nhap' => date('Y-m-d'),
            'ho_ten' => $tenkh,
            'dien_thoai' => $dienthoai,
            'email' => $email,
            'ghi_chu' => $ghichu,
            'hoc_vien' => $tenhocvien,
            'phan_loai' => $loaikh,
            'nguon_data' => $nguondata,
            'nguoi_nhap' => $_SESSION['user']['nhan_vien'],
            'tinh_trang' => 1];
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


    function import()
    {
        $funs = $this->fun;
        $import = false;
        foreach ($funs as $item) {
            if ($item['link'] == 'nhap()') {
                $import = true;
                break;
            }
        }
        if ($import) {
//            $phanloai = $_REQUEST['phanloai'];
            require_once ROOT_DIR . '/libs/phpexcel/PHPExcel/IOFactory.php';
            try {
                $inputFileType = PHPExcel_IOFactory::identify($_FILES['file']['tmp_name']);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($_FILES['file']['tmp_name']);
                $objReader->setReadDataOnly(true);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $banghi = 0;
                for ($row = 3; $row <= $highestRow; $row++) {
                    $ten = $objPHPExcel->getActiveSheet()->getCell("B$row")->getValue();
                    if ($ten != '') {
                        $dienthoai = $objPHPExcel->getActiveSheet()->getCell("C$row")->getValue();
                        $tenhocvien = $objPHPExcel->getActiveSheet()->getCell("D$row")->getValue();
                        $gioitinh = $objPHPExcel->getActiveSheet()->getCell("E$row")->getValue();
                        $gioitinh = ($gioitinh == 'Nam') ? 1 : 0;
                        $ngaysinh = $objPHPExcel->getActiveSheet()->getCell("F$row")->getValue();
                        if ($ngaysinh != '')
                            if (strlen($ngaysinh) == 5) {
                                $ngaysinh = ((string)$ngaysinh - 25569) * 86400;
                                $ngaysinh = date("Y-m-d", $ngaysinh);
                            } else {
                                if (count(explode("/", $ngaysinh)) == 3)
                                    $ngaysinh = functions::convertDate($ngaysinh);
                            }
                        else
                            $ngaysinh = '0000-00-00';
                        $email = $objPHPExcel->getActiveSheet()->getCell("G$row")->getValue();
                        $diachi = $objPHPExcel->getActiveSheet()->getCell("H$row")->getValue();
                        $truonghoc = $objPHPExcel->getActiveSheet()->getCell("I$row")->getValue();
                        $dienthoaihv = $objPHPExcel->getActiveSheet()->getCell("J$row")->getValue();
                        $phanloai = $objPHPExcel->getActiveSheet()->getCell("K$row")->getValue();
                        $phanloaiid = $this->model->getPhanLoai($phanloai);
                        $data = array(
                            'ho_ten' => $ten,
                            'hoc_vien' => $tenhocvien,
                            'ngay_sinh' => $ngaysinh,
                            'nguoi_nhap' => $_SESSION['user']['nhan_vien'],
                            'ngay_sinh' => $ngaysinh,
                            'gioi_tinh' => $gioitinh,
                            'dien_thoai' => $dienthoai,
                            'dia_chi' => $diachi,
                            'email' => $email,
                            'ngay_nhap' => date('Y-m-d'),
                            'truong_hoc' => $truonghoc,
                            'dien_thoai_hv' => $dienthoaihv,
                            'phan_loai' => $phanloaiid,
                            'tinh_trang' => 1
                        );
                        $hocvienid = $this->model->addObj($data);
                        if ($hocvienid > 0) {
//                            $hotenph = $objPHPExcel->getActiveSheet()->getCell("F$row")->getValue();
////                            if ($hotenph != '') {
////                                $dienthoaiph = $objPHPExcel->getActiveSheet()->getCell("G$row")->getValue();
////                                $emailph = $objPHPExcel->getActiveSheet()->getCell("H$row")->getValue();
////                                $dataph = array(
////                                    'ho_ten' => $hotenph,
////                                    'dien_thoai' => $dienthoaiph,
////                                    'email' => $emailph,
////                                    'hoc_vien' => $hocvienid,
////                                    'tinh_trang' => 1
////                                );
////                                $this->model->addph($dataph);
////                            }
                            $banghi++;
                        }
                    }
                    if ($banghi > 0) {
                        $jsonObj['msg'] = "Cập nhật thành công $banghi data";
                        $jsonObj['success'] = true;
                    } else {
                        $jsonObj['msg'] = "Lỗi cập nhật database";
                        $jsonObj['success'] = false;
                    }
                }
            } catch (Exception $e) {
                $jsonObj['msg'] = "Import dữ liệu không thành công";
                $jsonObj['success'] = false;
            }

            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function import1()
    {
        $funs = $this->fun;
        $import = false;
        foreach ($funs as $item) {
            if ($item['link'] == 'nhap()') {
                $import = true;
                break;
            }
        }
        if ($import) {
//            $phanloai = $_REQUEST['phanloai'];
            require_once ROOT_DIR . '/libs/phpexcel/PHPExcel/IOFactory.php';
            try {
                $inputFileType = PHPExcel_IOFactory::identify($_FILES['file']['tmp_name']);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($_FILES['file']['tmp_name']);
                $objReader->setReadDataOnly(true);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $banghi = 0;
                $ngaygio = '0000-00-00';
                for ($row = 1; $row <= $highestRow; $row++) {
                    $temp = $objPHPExcel->getActiveSheet()->getCell("B$row")->getValue();
                    if (strlen($temp) == 5) {
                        $ngaygio = ((string)$temp - 25569) * 86400;
                        $ngaygio = date("Y-m-d", $ngaygio);
                    } else {
                        $dienthoai = '0' . $temp;
                        $ten = $objPHPExcel->getActiveSheet()->getCell("C$row")->getValue();
                        $tenhv = $objPHPExcel->getActiveSheet()->getCell("D$row")->getValue();
                        $ghichu1 = $objPHPExcel->getActiveSheet()->getCell("F$row")->getValue();
                        $ghichu2 = $objPHPExcel->getActiveSheet()->getCell("G$row")->getValue();
                        $ghichu = $ghichu1 . ',' . $ghichu2;
                        $ghichu = ltrim($ghichu, ',');
                        $ghichu = rtrim($ghichu, ',');
//                        $truonghoc = $objPHPExcel->getActiveSheet()->getCell("H$row")->getValue();
                        $data = array(
                            'ho_ten' => $ten,
                            'nhan_vien' => 0,
                            'nguoi_nhap' => $_SESSION['user']['nhan_vien'],
                            'hoc_vien' => $tenhv,
//                                'ngay_sinh' => $ngaysinh,
//                                'gioi_tinh' => $gioitinh,
                            'dien_thoai' => $dienthoai,
//                                'dia_chi' => $diachilh,
//                                'email' => $email,
                            'ngay_nhap' => $ngaygio,
//                                'truong_hoc' => $truonghoc,
                            'ghi_chu' => $ghichu,
                            'tinh_trang' => 1
                        );
                        if ($ten != '')
                            $hocvienid = $this->model->addObj($data);
                        else
                            $hocvienid = 0;
                        if ($hocvienid > 0)
                            $banghi++;
                    }
                }
                if ($banghi > 0) {
                    $jsonObj['msg'] = "Cập nhật thành công $banghi data";
                    $jsonObj['success'] = true;
                } else {
                    $jsonObj['msg'] = "Lỗi cập nhật database";
                    $jsonObj['success'] = false;
                }
            } catch (Exception $e) {
                $jsonObj['msg'] = "Import dữ liệu không thành công";
                $jsonObj['success'] = false;
            }

            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function chia()
    {
        $funs = $this->fun;
        $chia = false;
        foreach ($funs as $item) {
            if ($item['link'] == 'chia()') {
                $chia = true;
                break;
            }
        }
        if ($chia) {
            $nhanvien = $_REQUEST['nguoinhan'];
            $data = $_REQUEST['data'];
            if ($this->model->updateObj($nhanvien, $data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function del()
    {
        $funs = $this->fun;
        $del = false;
        foreach ($funs as $item) {
            if ($item['link'] == 'del()') {
                $del = true;
                break;
            }
        }
        if ($del) {
            $data = $_REQUEST['data'];
            if ($this->model->delObj($data)) {
                $jsonObj['msg'] = "Xóa dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Xóa dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }


    function themungvien()
    {
        $dienthoai = $_REQUEST['dien_thoai1'];
        $phanloai = $_REQUEST['loaiungvien'];
        $id = $_REQUEST['id'];
        if ($this->model->checkso($dienthoai, 0)) {
            $jsonObj['msg'] = "Số điện thoại ứng viên đã có trong hệ thống";
            $jsonObj['success'] = false;
        } else {
            $hoten = $_REQUEST['ho_ten1'];
            $fname = functions::convertname($hoten);
            $ngaysinh = functions::convertDate($_REQUEST['ngaysinh1']);
            $gioitinh = $_REQUEST['gioi_tinh'];
            $ngoaihinh = $_REQUEST['ngoai_hinh'];
            $thanhpho = $_REQUEST['thanh_pho'];
            $quanhuyen = $_REQUEST['quan_huyen'];
            $vitri = $_REQUEST['danhmuc'];
            $kinhnghiem = $_REQUEST['kinh_nghiem'];
            $trinhdo = $_REQUEST['trinh_do'];
            $ngoaingu = $_REQUEST['ngoai_ngu'];
            $mucluong = $_REQUEST['muc_luong'];
            $email = $_REQUEST['email1'];
            $facebook = $_REQUEST['facebook'];
            $nguon = $_REQUEST['nguon'];
            $tennguon = $_REQUEST['ten_nguon'];
            $chuyenmon = $_REQUEST['chuyen_mon1'];
            $tinhtrang = ($_REQUEST['tinh_trang1'] > 0) ? $_REQUEST['tinh_trang1'] : 1;
            $data = array(
                'ho_ten' => $hoten,
                'ngay_sinh' => $ngaysinh,
                'gioi_tinh' => $gioitinh,
                'ngoai_hinh' => $ngoaihinh,
                'thanh_pho' => $thanhpho,
                'quan_huyen' => $quanhuyen,
                'vi_tri' => $vitri,
                'kinh_nghiem' => $kinhnghiem,
                'trinh_do' => $trinhdo,
                'ngoai_ngu' => $ngoaingu,
                'muc_luong' => $mucluong,
                'email' => $email,
                'facebook' => $facebook,
                'dien_thoai' => $dienthoai,
                'nguon' => $nguon,
                'ten_nguon' => $tennguon,
                'chuyen_mon' => $chuyenmon,
                'phan_loai' => $phanloai,
                'thoi_gian' => date("Y-m-d"),
                'nhan_vien' => $_SESSION['user']['nhan_vien'],
                'tinh_trang' => $tinhtrang
            );
            if (isset($_FILES['file1']['name']) && ($_FILES['file1']['name'] != '')) {
                $dir = ROOT_DIR . '/uploads/hinhanh/';
                $file = functions::uploadfile('file1', $dir, $fname);
                $hinhanh = URL . '/uploads/hinhanh/' . $file;
                $data['hinh_anh'] = $hinhanh;
            }
            if (isset($_FILES['cv']['name']) && ($_FILES['cv']['name'] != '')) {
                $dir = ROOT_DIR . '/uploads/cv/';
                $file = functions::uploadfile('cv', $dir, $fname);
                $cv = URL . '/uploads/cv/' . $file;
                $data['cv'] = $cv;
            }
            if ($this->model->themungvien($data)) {
                $data1 = array('tinh_trang' => 7);
                $this->model->goidata($id, $data1);
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phanloaikh()
    {
        $this->view->jsonObj = '[
        {"id":"1","name":"Học sinh"},
        {"id":"2","name":"Phụ huynh"},
        {"id":"3","name":"Đăng ký dùng thử"},
        {"id":"4","name":"Facebook"}]';
        $this->view->render('common/json');
    }

    function hocdemo()
    {
        $iddata = $_REQUEST['idkh'];
        $data = ['tinh_trang' => 7];
        $this->model->updateData($iddata, $data);
        $ngaysinh = functions::convertDate($_REQUEST['ngay_sinh']);
        $dienthoai = $_REQUEST['dienthoai'];
        $tenhv = $_REQUEST['ten_hoc_vien'];
        $idph = $this->model->themKH($iddata);
        if($idph > 0){
            $phanloai = $_REQUEST['loailop'];
            $product = $_REQUEST['khoa_hoc'];
            $sobuoi = $_REQUEST['so_buoi'] != '' ? $_REQUEST['so_buoi'] : 1;
            $data = array(
                'ngay_tao' => date('Y-m-d'),
                'name' => $tenhv,
                'e_name' => $tenhv,
                'ngay_sinh' => $ngaysinh,
                'dien_thoai' => $dienthoai,
                'khach_hang' => $idph,
                'phan_loai' => $phanloai,
                'tinh_trang' => 1
            );
            $idhv = $this->model->themHocVien($data);
            if ($idhv > 0) {
                $donhang = array(
                    'khach_hang' => $idph,
                    'hoc_vien' => $idhv,
                    'so_buoi' => $sobuoi,
                    'product' => $product,
                    'so_tien' => 0,
                    'ngay_dang_ky' => date('Y-m-d'),
                    'khuyen_mai' => 0,
                    'bao_luu' => 0,
                    'da_hoc' => 0,
                    'tinh_trang' => 1,
                    'ghi_chu' => ''
                );
                $this->model->themDonHang($donhang);
                if ($phanloai == 15 || $phanloai == 16) {
                    $tenlop = $_REQUEST['tenlop'];
                    $giaovien = $_REQUEST['giao_vien'];
                    $giaotrinh = $_REQUEST['giao_trinh'];
                    $khoahoc = $_REQUEST['khoa_hoc'];
                    $ghichu = "Lớp học học demo từ data";
                    $sobuoi = $_REQUEST['so_buoi'];
                    $thoiluong = $_REQUEST['thoi_luong'];
                    $data = array(
                        'name' => $tenlop,
                        'giao_vien' => $giaovien,
                        'giao_trinh' => $giaotrinh,
                        'khoa_hoc' => $khoahoc,
                        'so_buoi' => $sobuoi,
                        'phan_loai' => 1,
                        'phan_loai_2' => 3,
                        'ghi_chu' => $ghichu,
                        'hoc_vien' => $idhv,
                        'so_tien'=>0,
                        'thoi_luong'=>$thoiluong,
                        'tinh_trang' => 1
                    );
                    $idlop = $this->model->ThemLopHoc($data);
                    if ($idlop > 0) {
                        $homnay = date("Y-m-d");
                        $ngaybatdau = functions::convertDate($_REQUEST['ngaybatdau']);
                        $buoihoc = isset($_REQUEST['buoihoc']) ? $_REQUEST['buoihoc'] : 0;
                        if ($buoihoc > 0 && $ngaybatdau != '') {
                            $gio = $_REQUEST['giohoc'];
                            $dembuoi = 0;
                            $date = $ngaybatdau;
                            do {
                                if (in_array(date('l', strtotime($date)), $buoihoc)) {
                                    $giora = date_create($gio);
                                    date_add($giora, date_interval_create_from_date_string("$thoiluong minutes"));
                                    $giora = date_format($giora, "H:i:s");
                                    $lichhoc = array(
                                        'ngay' => $date,
                                        'gio' => $gio,
                                        'gio_ra' => $giora,
                                        'thoi_luong' => $thoiluong,
                                        'lop_hoc' => $idlop,
                                        'giao_vien' => $giaovien,
                                    );
                                    if (strtotime($date) < strtotime($homnay))
                                        $lichhoc['tinh_trang'] = 3;
                                    else
                                        $lichhoc['tinh_trang'] = 1;
                                    if ($phanloai == 1)
                                        $lichhoc['phong_hoc'] = 1;
                                    $lichhocid = $this->model->ThemLichHoc($lichhoc);
                                    if ($lichhocid > 0) {
                                        $saplop = array(
                                            'hoc_vien' => $idhv,
                                            'lich_hoc' => $lichhocid,
                                            'lop_hoc' => $idlop,
                                        );
                                        if (strtotime($date) < strtotime($homnay))
                                            $saplop['tinh_trang'] = 2;
                                        else
                                            $saplop['tinh_trang'] = 1;
                                        $this->model->themSaplop($saplop);
                                    }
                                    $dembuoi++;
                                }
                                $date = date('Y-m-d', strtotime($date . " +1 days"));
                            } while ($dembuoi < ($sobuoi));
                        }
                    }
                }
                $jsonObj['msg'] = "Đăng ký học thử thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function trainghiem()
    {
        $iddata = $_REQUEST['idkh'];
        $data1 = ['tinh_trang' => 7];
        $this->model->updateData($iddata, $data1);
        $ngaysinh = functions::convertDate($_REQUEST['ngay_sinh']);
        $dienthoai = $_REQUEST['dienthoai'];
        $tenhv = $_REQUEST['ten_hoc_vien'];
        $idph = $this->model->themKH($iddata);
        $product = $_REQUEST['khoa_hoc'];
        $sobuoi = $_REQUEST['so_buoi'];
        $data = array(
            'name' => $tenhv,
            'e_name' => $tenhv,
            'ngay_sinh' => $ngaysinh,
            'dien_thoai' => $dienthoai,
            'khach_hang' => $idph,
            'phan_loai' => 20,
            'tinh_trang' => 1
        );
        $idhv = $this->model->themHocVien($data);
        if ($idhv > 0) {
            $donhang = array(
                'khach_hang' => $idph,
                'hoc_vien' => $idhv,
                'so_buoi' => $sobuoi,
                'product' => $product,
                'so_tien' => 0,
                'ngay_dang_ky' => date('Y-m-d'),
                'khuyen_mai' => 0,
                'bao_luu' => 0,
                'da_hoc' => 0,
                'tinh_trang' => 1,
                'ghi_chu' => ''
            );
            $this->model->themDonHang($donhang);
            $jsonObj['msg'] = "Đăng ký học thử thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function dangky()
    {
        $iddata = $_REQUEST['idkh'];
        $data1 = ['tinh_trang' => 9];
        $this->model->updateData($iddata, $data1);
        $tenhv = $_REQUEST['ten_hoc_vien'];
        $idph = $this->model->themKH($iddata);
        $phanloai = $_REQUEST['loailop'];
        $data = array(
            'name' => $tenhv,
            'e_name' => $tenhv,
            'khach_hang' => $idph,
            'phan_loai' => $phanloai,
            'tinh_trang' => 1
        );
        $idhv = $this->model->themHocVien($data);
        if ($idhv) {
            $sobuoi = $_REQUEST['buoi_hoc'];
            $buoikm = $_REQUEST['buoi_hoc_km'];
            $product = $_REQUEST['product'];
            $sotien = $_REQUEST['so_tien'];
            $sotien = str_replace(",", "", $sotien);
            $ngaydangky = functions::convertDate($_REQUEST['ngay_dang_ky']);
            $dotthanhtoan = $_REQUEST['dot_thanh_toan'];
            $donhang = array(
                'nhan_vien' => $_SESSION['user']['nhan_vien'],
                'khach_hang' => $idph,
                'hoc_vien' => $idhv,
                'so_buoi' => $sobuoi,
                'product' => $product,
                'so_tien' => $sotien,
                'ngay_dang_ky' => $ngaydangky,
                'khuyen_mai' => $buoikm,
                'bao_luu' => 0,
                'da_hoc' => 0,
                'dot_thanh_toan' => $dotthanhtoan,
                'tinh_trang' => 1,
                'ghi_chu' => ''
            );
            $donhangid = $this->model->themDonHang($donhang);
            if ($donhangid > 0) {
                $dot = 0;
                for ($i = 1; $i < 7; $i++) {
                    $ngay = $_REQUEST['ngaydot' . $i];
                    $tiendot = $_REQUEST['sotiendot' . $i];
                    if ($ngay != '' && $tiendot != '') {
                        $ngay = functions::convertDate($ngay);
                        $tiendot = str_replace(",", "", $tiendot);
                        $invoice = array(
                            'ngay' => $ngay,
                            'don_hang' => $donhangid,
                            'so_tien' => $tiendot,
                            'du_no' => $tiendot,
                            'lan_1' => '0000-00-00 00:00:00',
                            'lan_2' => '0000-00-00 00:00:00',
                            'lan_3' => '0000-00-00 00:00:00',
                            'dot_thanh_toan' => $i,
                            'tinh_trang' => 1
                        );
                        $this->model->themInvoice($invoice);
                        $dot++;
                    }
                }
                $this->model->updateDonhang($donhangid, ['dot_thanh_toan' => $dot]);
            }
            $jsonObj['msg'] = "Đăng ký khóa học thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }

        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function dangky1()
    {
//        $iddata = $_REQUEST['idkh'];
//        $data1 = ['tinh_trang' => 9];
//        $this->model->updateData($iddata, $data1);
//        $tenhv = $_REQUEST['ten_hoc_vien'];
//        $idph = $this->model->themKH($iddata);
//        $phanloai = $_REQUEST['loailop'];
//        $data = array(
//            'name' => $tenhv,
//            'e_name' => $tenhv,
//            'khach_hang' => $idph,
//            'phan_loai' => $phanloai,
//            'tinh_trang' => 1
//        );
//        $idhv = $this->model->themHocVien($data);
//        if ($idhv) {
//            $sobuoi = $_REQUEST['buoi_hoc'];
//            $buoikm = $_REQUEST['buoi_hoc_km'];
//            $product = $_REQUEST['product'];
//            $sotien = $_REQUEST['so_tien'];
//            $sotien = str_replace(",", "", $sotien);
//            $ngaydangky = functions::convertDate($_REQUEST['ngay_dang_ky']);
//            $dotthanhtoan = $_REQUEST['dot_thanh_toan'];
//            $donhang = array(
//                'nhan_vien' => $_SESSION['user']['nhan_vien'],
//                'khach_hang' => $idph,
//                'hoc_vien' => $idhv,
//                'so_buoi' => $sobuoi,
//                'product' => $product,
//                'so_tien' => $sotien,
//                'ngay_dang_ky' => $ngaydangky,
//                'khuyen_mai' => $buoikm,
//                'bao_luu' => 0,
//                'da_hoc' => 0,
//                'dot_thanh_toan' => $dotthanhtoan,
//                'tinh_trang' => 1,
//                'ghi_chu' => ''
//            );
//            $donhangid = $this->model->themDonHang($donhang);
//            $dot = 0;
//            if ($donhangid > 0) {
        $test = 0;
        for ($i = 1; $i < 7; $i++) {
            $ngay = $_REQUEST['ngaydot' . $i];
            $tiendot = $_REQUEST['sotiendot' . $i];
            if ($ngay != '' && $tiendot != '') {
                $ngay = functions::convertDate($ngay);
                $tiendot = str_replace(",", "", $tiendot);
                $test .= $tiendot . " ";
//                        $this->model->themInvoice($invoice);
//                        $dot++;
            }

//                $this->model->updateDonhang($donhangid,['dot_thanh_toan' => $dot]);

            $jsonObj['msg'] = "Đăng ký khóa học thành công $test";
            $jsonObj['success'] = false;
        }

        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phanloaidemo()
    {
        $jsonObj = $this->model->phanloaidemo();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function phanloai()
    {
        $jsonObj = $this->model->phanloai();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function tinhtrang()
    {
        $jsonObj = $this->model->tinhtrang();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function chiadot()
    {
        $ngaydangky = functions::convertDate($_REQUEST['ngaydangky']);
        $sotien = str_replace(",", "", $_REQUEST['sotien']);
        $sodot = $_REQUEST['sodot'];
        $ngaydot = $ngaydangky;
        if ($sotien != '' && $ngaydangky != '') {
            for ($i = 1; $i <= $sodot; $i++) {
                if ($i > 1) {
                    $ngaydot = date('Y-m-d', strtotime("$ngaydot +1 month"));
                }
                $data[$i] = ['ngaydot' => date('d/m/Y', strtotime($ngaydot)),
                    'sotiendot' => ROUND($sotien / $sodot)];
            }
            $jsonObj['data'] = $data;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Bạn cần nhập số tiền và ngày đăng ký";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

}

?>

<?php
class thuengoai extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('thuengoai') == false) {
            header('Location: ' . URL);
        }
    }

    function index()
    {
        $module="THUÊ NGOÀI";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('thuengoai');
        $this->view->render('thuengoai/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay_kt';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : 1;
        $dichvu = isset($_REQUEST['dichvu']) ? $_REQUEST['dichvu'] : 0;
        $ncc = isset($_REQUEST['ncc']) ? $_REQUEST['ncc'] : 0;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tinhtrang, $tukhoa, $dichvu, $ncc);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function xuatfile()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 2000;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay_kt';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : 1;
        $dichvu = isset($_REQUEST['dichvu']) ? $_REQUEST['dichvu'] : 0;
        $ncc = isset($_REQUEST['ncc']) ? $_REQUEST['ncc'] : 0;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tinhtrang, $tukhoa, $dichvu, $ncc);
        $this->view->data = $jsonObj;
        $this->view->render('thuengoai/xuatfile');
    }

    function add()
    {
        $name = $_REQUEST['name'];
        $product = $_REQUEST['product'];
        $khachhang = $_REQUEST['khach_hang'];
        $ngaybd = functions::convertDate($_REQUEST['ngaybd']);
        $ngaykt = functions::convertDate($_REQUEST['ngaykt']);
        $sotien = $_REQUEST['so_tien'];
        $link = $_REQUEST['link'];
        $tinhtrang = 1;
        $mota = $_REQUEST['mo_ta'];
        $ghichu = $_REQUEST['ghi_chu'];
        // $tainguyen = $_REQUEST['hoa_don'];
        $data = [
            'name' => $name,
            'khach_hang' => $khachhang,
            'ngay_bd' => $ngaybd,
            'ngay_kt' => $ngaykt,
            'so_tien' => $sotien,
            'link' => $link,
            'tinh_trang' => $tinhtrang,
            'mo_ta' => $mota,
            'ghi_chu' => $ghichu,
            'product' => $product,
        ];
        // if ($tainguyen==1) {
        //     $data1       = array(
        //         'name' => $name,
        //         'nha_cung_cap' => $khachhang,
        //         'chu_so_huu'=>1,
        //         'link' => $link,
        //         'ghi_chu' => $ghichu,
        //         'phan_loai' => $product,
        //         'nguoi_tao' => $_SESSION['user']['nhan_vien'],
        //         'phan_loai' =>$product,
        //         'tinh_trang' => 1
        //     );
        //     $ok=$this->model->addObj1($data1);
        // }
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $product = $_REQUEST['product'];
        $khachhang = $_REQUEST['khach_hang'];
        $ngaybd = functions::convertDate($_REQUEST['ngaybd']);
        $ngaykt = functions::convertDate($_REQUEST['ngaykt']);
        $sotien = $_REQUEST['so_tien'];
        $link = $_REQUEST['link'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        $mota = $_REQUEST['mo_ta'];
        $ghichu = $_REQUEST['ghi_chu'];
        $data = [
            'name' => $name,
            'khach_hang' => $khachhang,
            'ngay_bd' => $ngaybd,
            'ngay_kt' => $ngaykt,
            'so_tien' => $sotien,
            'link' => $link,
            'tinh_trang' => $tinhtrang,
            'mo_ta' => $mota,
            'ghi_chu' => $ghichu,
            'product' => $product
        ];
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function giahan()
    {
        $id = $_REQUEST['id'];
        $khachhang = $_REQUEST['khachhang'];
        $ngaygiahan = functions::convertDate($_REQUEST['ngaygiahan']);
        $sotien = $_REQUEST['sotien'];
        $taikhoan = $_REQUEST['taikhoan'];
        $hoadon = $_REQUEST['hoadon'];
        if ($this->model->giahan($id,$ngaygiahan,$sotien,$taikhoan,$hoadon,$khachhang)) {
            $jsonObj['msg'] = "Đã gia hạn dịch vụ";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

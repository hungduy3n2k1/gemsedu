<?php
class phieuchi extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new model();
        if ($model->checkright('phieuchi') == false) {
            header('Location: ' . URL);
        }
    }

    function index()
    {
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('phieuchi');
        $this->view->render('phieuchi/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tungay = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : date("Y-m-d", strtotime("first day of this month"));
        $denngay = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : date("Y-m-d");
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : 0;
        $taikhoan = isset($_REQUEST['taikhoan']) ? $_REQUEST['taikhoan'] : 0;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $khachhang, $taikhoan);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $khachhang = $_REQUEST['khach_hang'];
        $taikhoan = $_REQUEST['tai_khoan'];
        $ngaygio = functions::convertDate($_REQUEST['ngaygio']) . ' ' . date('H:i:s');
        $sotien = $_REQUEST['so_tien'];
        // $invoice = $_REQUEST['invoice'];
        $noidung = $_REQUEST['dien_giai'];
        $ghichu = $_REQUEST['ghi_chu'];
        $name = $_REQUEST['name'];
        $data = [
            'ngay_gio' => $ngaygio,
						'name' => $name,
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'khach_hang' => $khachhang,
            'so_tien' => $sotien,
            'dien_giai' => $noidung,
            'tai_khoan' => $taikhoan,
            'ghi_chu' => $ghichu,
            'loai' => 1,
            'tinh_trang' => 1,
        ];
				if ($this->model->addObj($data)) {
						$jsonObj['msg'] = "Cập nhật dữ liệu thành công";
						$jsonObj['success'] = true;
				} else {
						$jsonObj['msg'] = "Cập nhật dữ liệu không thành công!";
						$jsonObj['success'] = false;
				}
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

		function update()
		{
				$id = $_REQUEST['id'];
				$khachhang = $_REQUEST['khach_hang'];
				$ngaygio = functions::convertDate($_REQUEST['ngaygio']) . ' ' . date('H:i:s');
				$taikhoan = $_REQUEST['tai_khoan'];
				$sotien = $_REQUEST['so_tien'];
				$name = ($_REQUEST['name']!='')?$_REQUEST['name']:'PC-'.$id;
				$noidung = $_REQUEST['dien_giai'];
				$ghichu = $_REQUEST['ghi_chu'];
				$data = [
						'ngay_gio' => $ngaygio,
						'nhan_vien' => $_SESSION['user']['nhan_vien'],
						'khach_hang' => $khachhang,
						'so_tien' => $sotien,
						'name' => $name,
						'dien_giai' => $noidung,
						'tai_khoan' => $taikhoan,
						'ghi_chu' => $ghichu,
				];
				if ($this->model->updateObj($id, $data)) {
						$jsonObj['msg'] = "Cập nhật dữ liệu thành công";
						$jsonObj['success'] = true;
				} else {
						$jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
						$jsonObj['success'] = false;
				}
				$this->view->jsonObj = json_encode($jsonObj);
				$this->view->render('common/json');
		}

		function del()
    {
        $id = $_REQUEST['id'];
        if ($this->model->delObj($id)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

//--------------



    function invoice()
    {
        $id = $_REQUEST['khachhang'];
        $jsonObj = $this->model->invoice($id);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


    function danhsach()
    {
        $khachhang = $_REQUEST['kh'];
        $jsonObj = $this->model->danhsach($khachhang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
    function xuatfile()
    {
        $tungay = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : date("Y-m-d", strtotime("first day of this month"));
        $denngay = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : date("Y-m-d");
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : 0;
        $taikhoan = isset($_REQUEST['taikhoan']) ? $_REQUEST['taikhoan'] : '';
        $jsonObj = $this->model->getFetObj('id', 'DESC', 0, 10000000, $tungay, $denngay, $khachhang, $taikhoan);
        $this->view->tungay = date("d/m/Y", strtotime($tungay));
        $this->view->denngay = date("d/m/Y", strtotime($denngay));
        $this->view->jsonObj = $jsonObj;
        $this->view->render('phieuchi/xuatfile');
    }
    function inphieu()
    {
        $id = $_REQUEST['id'];
        $this->view->phieu = $this->model->phieuchi($id);
        $this->view->render('phieuchi/inphieu');
    }
}
?>

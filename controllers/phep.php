<?php
class phep extends Controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new model();
        if ($model->checkright('phep')==false)
           header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ PHÉP";
        require HEADER;
        $this->view->funs=$model->getfun('phep');
        $this->view->render('phep/index');
        require FOOTER;
    }

    function json()
    {
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang']!=''))?$_REQUEST['thang']:date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam']!=''))?$_REQUEST['nam']:date("Y");
        $jsonObj             = $this->model->getFetObj($thang, $nam);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id     = $_REQUEST['id'];
        $phepnam = $_REQUEST['phep_nam'];
        $phepluyke = $_REQUEST['phep_luy_ke'];
        $data      = array(
            'phep_nam' => $phepnam,
            'phep_luy_ke' => $phepluyke,
        );
        if ($this->model->updateObj($id, $data)) {
                $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
        } else {
                $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
        }

        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

<?php

class donhang extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new Model();
        $this->fun = $model->getfun('donhang');
        if ($model->checkright('donhang') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "HỢP ĐỒNG ĐÀO TẠO";
        require HEADER;
        $this->view->funs = $this->fun;
        if (MOBILE)
            $this->view->render('donhang/index_m');
        else
            $this->view->render('donhang/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : '';
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : '';
        $hocvien = isset($_REQUEST['hocvien']) ? $_REQUEST['hocvien'] : '';
        $loaidh = isset($_REQUEST['loaidh']) ? $_REQUEST['loaidh'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tinhtrang, $khachhang,$hocvien,$loaidh);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function xuatfile()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tukhoa = isset($_REQUEST['tenmien']) ? $_REQUEST['tenmien'] : '';
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : 1;
        $khachhang = isset($_REQUEST['sohuu']) ? $_REQUEST['sohuu'] : 0;
        $dichvu = isset($_REQUEST['dichvu']) ? $_REQUEST['dichvu'] : 0;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa, $tinhtrang, $sohuu, $dichvu);
        $this->view->donhang = $jsonObj;
        $this->view->render('donhang/xuatfile');

    }

    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"1","name":"Mới"},{"id":"2","name":"Đang thực hiện"},{"id":"3","name":"Đã hoàn thành"},{"id":"4","name":"Tạm dừng"},{"id":"5","name":"Hủy"}]';
        $this->view->render('common/json');
    }

    function phanloaisale()
    {
        $this->view->jsonObj = '[{"id":"1","name":"New sale: 2%"},{"id":"2","name":"Tái tục trải nghiệm: 2%"},{"id":"3","name":"Tái tục new: 3%"}]';
        $this->view->render('common/json');
    }

    function hocvien()
    {
        $khachhang = $_REQUEST['khach_hang'];
        $jsonObj = $this->model->gethocvien($khachhang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function sobuoi()
    {
        $khoahoc = $_REQUEST['product'];
        $jsonObj = $this->model->getsobuoi($khoahoc);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function dotthanhtoan()
    {
        $donhang = $_REQUEST['donhang'];
        $jsonObj = $this->model->getDotTT($donhang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $product = $_REQUEST['product'];
        $khachhang = $_REQUEST['khach_hang'];
        $sdt = $_REQUEST['sdt'];
        $hocvien = $_REQUEST['hoc_vien'];
        $sobuoi = $_REQUEST['so_buoi'];
        $sotien = $_REQUEST['so_tien'];
        $khuyenmai = $_REQUEST['khuyen_mai'];
//        $baoluu = $_REQUEST['bao_luu'];
//        $dahoc = $_REQUEST['da_hoc'];
        $ghichu = $_REQUEST['ghi_chu'];
        $loai = 1;
        $giaovien = $_REQUEST['giao_vien'];
        $nhanviensale = $_REQUEST['nhan_vien_sale'];
        $phanloaisale = $_REQUEST['phan_loai_sale'];
        $loaidh = 1;
        if (!is_numeric($khachhang))
            $khachhang = $this->model->addkhachhang($khachhang, $sdt, $loai, date('Y-m-d'));
        if (!is_numeric($hocvien))
            $hocvien = $this->model->addhocvien($khachhang, $hocvien, $loai, 1);
        $dotthanhtoan = $_REQUEST['dot_thanh_toan'];
        $data = array(
            'ngay_dang_ky' => date('Y-m-d'),
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'product' => $product,
            'khach_hang' => $khachhang,
            'hoc_vien' => $hocvien,
            'so_buoi' => $sobuoi,
            'so_tien' => $sotien,
            'khuyen_mai' => $khuyenmai,
//            'bao_luu' => $baoluu,
//            'da_hoc' => $dahoc,
            'dot_thanh_toan' => $dotthanhtoan,
            'tinh_trang' => 1,
            'ghi_chu' => $ghichu,
            'giao_vien' => $giaovien,
            'nhan_vien_sale' => $nhanviensale,
            'phan_loai_sale' => $phanloaisale,
            'loaidh' => $loaidh
        );
        $donhangid = $this->model->addObj($data);
        if ($donhangid > 0) {
            $dot = 0;
            for ($i = 1; $i < 7; $i++) {
                $ngay = $_REQUEST['ngaydot' . $i];
                $tiendot = $_REQUEST['sotiendot' . $i];
                if ($ngay != '' && $tiendot != '') {
                    $ngay = functions::convertDate($ngay);
                    $tiendot = str_replace(",", "", $tiendot);
                    $invoice = array(
                        'ngay' => $ngay,
                        'don_hang' => $donhangid,
                        'so_tien' => $tiendot,
                        'du_no' => $tiendot,
                        'lan_1' => '0000-00-00 00:00:00',
                        'lan_2' => '0000-00-00 00:00:00',
                        'lan_3' => '0000-00-00 00:00:00',
                        'dot_thanh_toan' => $i,
                        'tinh_trang' => 1
                    );
                    $this->model->themInvoice($invoice);
                    $dot++;
                }
            }
            $this->model->updateObj($donhangid, ['dot_thanh_toan' => $dot]);
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function updateST()
    {
        $id     = $_REQUEST['id'];
        $sotien = $_REQUEST['so_tien'];
        $data   = array('so_tien' => $sotien);
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $product = $_REQUEST['product'];
        $khachhang = $_REQUEST['khach_hang'];
        $hocvien = $_REQUEST['hoc_vien'];
        $sobuoi = $_REQUEST['so_buoi'];
        $sotien = $_REQUEST['so_tien'];
        $khuyenmai = $_REQUEST['khuyen_mai'];
        $baoluu = $_REQUEST['bao_luu'] ?? '';
        $dahoc = $_REQUEST['da_hoc'] ?? '';
        $ghichu = $_REQUEST['ghi_chu'];
        $dotthanhtoan = $_REQUEST['dot_thanh_toan'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        $giaovien = $_REQUEST['giao_vien'];
        $nhanviensale = $_REQUEST['nhan_vien_sale'];
        $phanloaisale = $_REQUEST['phan_loai_sale'];
        $loaidh = 1;
        $ngaydangky = functions::convertDate($_REQUEST['ngay_dang_ky']);
        $data = array(
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'product' => $product,
            'khach_hang' => $khachhang,
            'hoc_vien' => $hocvien,
            'so_buoi' => $sobuoi,
            'so_tien' => $sotien,
            'khuyen_mai' => $khuyenmai,
            'bao_luu' => $baoluu,
            'da_hoc' => $dahoc,
            'dot_thanh_toan' => $dotthanhtoan,
            'tinh_trang' => $tinhtrang,
            'ghi_chu' => $ghichu,
            'giao_vien' => $giaovien,
            'nhan_vien_sale' => $nhanviensale,
            'phan_loai_sale' => $phanloaisale,
            'ngay_dang_ky' => $ngaydangky,
            'loaidh' => $loaidh
        );
        if ($this->model->updateObj($id, $data)) {
            $datainv = array('tinh_trang' => 0);
            $this->model->delInvoice($id, $datainv);
            $dot = 0;
            for ($i = 1; $i < 7; $i++) {
                $ngay = $_REQUEST['ngaydot' . $i];
                $tiendot = $_REQUEST['sotiendot' . $i];
                if ($ngay != '' && $tiendot != '') {
                    $ngay = functions::convertDate($ngay);
                    $tiendot = str_replace(",", "", $tiendot);
                    $invoice = array(
                        'ngay' => $ngay,
                        'don_hang' => $id,
                        'so_tien' => $tiendot,
                        'du_no' => $tiendot,
                        'lan_1' => '0000-00-00 00:00:00',
                        'lan_2' => '0000-00-00 00:00:00',
                        'lan_3' => '0000-00-00 00:00:00',
                        'dot_thanh_toan' => $i,
                        'tinh_trang' => 1
                    );
                    $this->model->themInvoice($invoice);
                    $dot++;
                }
            }
            $this->model->updateObj($id, ['dot_thanh_toan' => $dot]);
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công $dotthanhtoan";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function detail()
    {
        $this->view->khachhang = $this->model->getkhachhang($_REQUEST['id']);
        $this->view->render('donhang/detail');
    }

    function import()
    {
        $funs = $this->fun;
        $import = false;
        foreach ($funs as $item) {
            if ($item['link'] == 'nhap()') {
                $import = true;
                break;
            }
        }
        if ($import) {
            $loai = $_REQUEST['loaikh'];
            require_once ROOT_DIR . '/libs/phpexcel/PHPExcel/IOFactory.php';
            try {
                $inputFileType = PHPExcel_IOFactory::identify($_FILES['file']['tmp_name']);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($_FILES['file']['tmp_name']);
                $objReader->setReadDataOnly(true);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $banghi = 0;
                for ($row = 10; $row <= $highestRow; $row++) {
                    $khachhangid = $objPHPExcel->getActiveSheet()->getCell("A$row")->getValue();
                    $hocvienid = $objPHPExcel->getActiveSheet()->getCell("B$row")->getValue();
                    $product = $objPHPExcel->getActiveSheet()->getCell("C$row")->getValue();
                    $hocvien = $objPHPExcel->getActiveSheet()->getCell("E$row")->getValue();
                    $khachhang = $objPHPExcel->getActiveSheet()->getCell("F$row")->getValue();
                    $ngaydangky = $objPHPExcel->getActiveSheet()->getCell("K$row")->getValue();
                    if ($ngaydangky != '')
                        if (strlen($ngaydangky) == 5) {
                            $ngaydangky = ((string)$ngaydangky - 25569) * 86400;
                            $ngaydangky = date("Y-m-d", $ngaydangky);
                        } else
                            $ngaydangky = functions::convertDate($ngaydangky);
                    else
                        $ngaydangky = '0000-00-00';
                    if (!is_numeric($khachhangid)) {
                        $sdt = $objPHPExcel->getActiveSheet()->getCell("G$row")->getValue();
                        if ($khachhang != '')
                            $khachhangid = $this->model->addkhachhang($khachhang, $sdt, $loai, $ngaydangky);
                        else
                            $khachhangid = 0;
                    }
                    if ($khachhangid > 0) {
                        if (!is_numeric($hocvienid)) {
                            if ($hocvien != '')
                                $hocvienid = $this->model->addhocvien($khachhangid, $hocvien, $loai, 2);
                            else
                                $hocvienid = 0;
                        }
                        if ($hocvienid > 0) {
                            $sobuoi = $objPHPExcel->getActiveSheet()->getCell("I$row")->getValue();
                            $khuyenmai = $objPHPExcel->getActiveSheet()->getCell("J$row")->getValue();
                            $hocphi = $objPHPExcel->getActiveSheet()->getCell("M$row")->getValue();
                            $data = array(
                                'product' => $product,
                                'khach_hang' => $khachhangid,
                                'hoc_vien' => $hocvienid,
                                'so_buoi' => $sobuoi,
                                'so_tien' => $hocphi,
                                'khuyen_mai' => $khuyenmai,
                                'bao_luu' => 0,
                                'da_hoc' => 0,
                                'ngay_dang_ky' => $ngaydangky,
                                'tinh_trang' => 2,
                                'ghi_chu' => ''
                            );
                            $dotthanhtoan = 0;
                            $donhangid = $this->model->addObj($data);
                            if ($donhangid > 0) {
                                $banghi++;
                                for ($i = 1; $i < 7; $i++) {
                                    if ($i == 1) {
                                        $sotien = $objPHPExcel->getActiveSheet()->getCell("N$row")->getValue();
                                        $dot = $objPHPExcel->getActiveSheet()->getCell("O$row")->getValue();
                                    } elseif ($i == 2) {
                                        $sotien = $objPHPExcel->getActiveSheet()->getCell("P$row")->getValue();
                                        $dot = $objPHPExcel->getActiveSheet()->getCell("Q$row")->getValue();
                                    } elseif ($i == 3) {
                                        $sotien = $objPHPExcel->getActiveSheet()->getCell("R$row")->getValue();
                                        $dot = $objPHPExcel->getActiveSheet()->getCell("S$row")->getValue();
                                    } elseif ($i == 4) {
                                        $sotien = $objPHPExcel->getActiveSheet()->getCell("T$row")->getValue();
                                        $dot = $objPHPExcel->getActiveSheet()->getCell("U$row")->getValue();
                                    } elseif ($i == 5) {
                                        $sotien = $objPHPExcel->getActiveSheet()->getCell("V$row")->getValue();
                                        $dot = $objPHPExcel->getActiveSheet()->getCell("W$row")->getValue();
                                    } else {
                                        $sotien = $objPHPExcel->getActiveSheet()->getCell("X$row")->getValue();
                                        $dot = $objPHPExcel->getActiveSheet()->getCell("Y$row")->getValue();
                                    }
                                    if ($sotien != '' && $dot != '') {
                                        if (strlen($dot) == 5) {
                                            $dot = (intVal($dot) - 25569) * 86400;
                                            $dot = date("Y-m-d", $dot);
                                        } else
                                            $dot = functions::convertDate($dot);
                                        $datad1 = [
                                            'ngay' => $dot,
                                            'don_hang' => $donhangid,
                                            'so_tien' => $sotien,
                                            'du_no' => $sotien,
                                            'dot_thanh_toan' => $i,
                                            'tinh_trang' => 1
                                        ];
                                        $this->model->ThemInvoice($datad1);
                                        $dotthanhtoan = $i;
                                    } else {
                                        break;
                                    }
                                }
                                $this->model->updateObj($donhangid, ['dot_thanh_toan' => $dotthanhtoan]);
                                $tenlop = $objPHPExcel->getActiveSheet()->getCell("D$row")->getValue();
                                $tengv = $objPHPExcel->getActiveSheet()->getCell("H$row")->getValue();
                                $giaovien = $this->model->getGiaoVien($tengv);
                                $khoahoc = $product;
                                $tengt = $objPHPExcel->getActiveSheet()->getCell("AB$row")->getValue();
                                $giaotrinh = $this->model->getTenGT($tengt);
                                $datalop = array(
                                    'name' => $tenlop,
                                    'giao_vien' => $giaovien,
                                    'giao_trinh' => $giaotrinh,
                                    'khoa_hoc' => $khoahoc,
                                    'so_buoi' => $sobuoi,
                                    'buoi_khuyen_mai' => $khuyenmai,
                                    'phan_loai' => $loai,
                                    'phan_loai_2' => 0,
                                    'ghi_chu' => '',
                                    'so_tien'=>$hocphi,
                                    'tinh_trang' => 2
                                );
                                if ($loai == 1)
                                    $datalop['hoc_vien'] = $hocvienid;
                                if ($loai == 2) {
                                    $checklop = $this->model->checkLop($tenlop);
                                } else {
                                    $checklop = true;
                                }
                                if ($checklop == true)
                                    $this->model->ThemLop($datalop);
                            }
                        }
                    }
                }
                if ($banghi > 0) {
                    $jsonObj['msg'] = "Cập nhật thành công $banghi data";
                    $jsonObj['success'] = true;
                } else {
                    $jsonObj['msg'] = "Lỗi cập nhật database";
                    $jsonObj['success'] = false;
                }
            } catch (Exception $e) {
                $jsonObj['msg'] = "Import dữ liệu không thành công";
                $jsonObj['success'] = false;
            }

            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function chiadot()
    {
        $ngaydangky = functions::convertDate($_REQUEST['ngaydangky']);
        $sotien = str_replace(",", "", $_REQUEST['sotien']);
        $sodot = $_REQUEST['sodot'];
        $ngaydot = $ngaydangky;
        if ($sotien != '' && $ngaydangky != '') {
            for ($i = 1; $i <= $sodot; $i++) {
                if ($i > 1) {
                    $ngaydot = date('Y-m-d', strtotime("$ngaydot +1 month"));
                }
                $data[$i] = ['ngaydot' => date('d/m/Y', strtotime($ngaydot)),
                    'sotiendot' => ROUND($sotien / $sodot)];
            }
            $jsonObj['data'] = $data;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Bạn cần nhập số tiền và ngày đăng ký";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }


}

?>

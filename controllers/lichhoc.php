<?php

class lichhoc extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new model();
        if ($model->checkright('lichhoc') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "QUẢN LÝ LỊCH HỌC";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('lichhoc');
        $this->view->render('lichhoc/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $ngay = (isset($_REQUEST['ngay'])) ? $_REQUEST['ngay'] : '';
        $thang = (isset($_REQUEST['thang'])) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
        $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : 0;
        $phonghoc = isset($_REQUEST['phonghoc']) ? $_REQUEST['phonghoc'] : 0;
        $giohoc = isset($_REQUEST['gio']) ? $_REQUEST['gio'] : '';
        $tenlop = isset($_REQUEST['tenlop']) ? $_REQUEST['tenlop'] : '';
        $jsonObj = $this->model->getFetObj($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function getrow()
    {
        $id = $_REQUEST['id'];
        $temp = $this->model->getrow($id);
        if ($temp) {
            $jsonObj['data'] = $temp;
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Không query được dữ liệu";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $ngaybatdau = functions::convertDate($_REQUEST['ngay']);
        $gio = $_REQUEST['gio'];
        $lophoc = $_REQUEST['lop_hoc'];
        $phonghoc = $_REQUEST['phong_hoc'];
        $thoiluong = $_REQUEST['thoi_luong'];
        $giaovien = $_REQUEST['giao_vien'];
        $buoihoc = isset($_REQUEST['buoihoc']) ? $_REQUEST['buoihoc'] : 0;
        $phanloai = $_REQUEST['phanloai'];
        $sobuoi = $_REQUEST['sobuoi'];
        $buoikm = $_REQUEST['buoikm'];
        $hocvien = $_REQUEST['hocvien'];
        $ok = $this->model->checkSoBuoi($lophoc);
        if($ok == 1){
            $dembuoi = 0;
            if ($buoihoc > 0 && $ngaybatdau != '') {
                $date = $ngaybatdau;
                $homnay = date("Y-m-d");
                $buoidahoc = $this->model->BuoiDaHoc($lophoc);
                $buoiconlai = ($sobuoi + $buoikm) - $buoidahoc;
                do {
                    if (in_array(date('l', strtotime($date)), $buoihoc)) {
                        $giora = date_create($gio);
                        date_add($giora, date_interval_create_from_date_string("$thoiluong minutes"));
                        $giora = date_format($giora, "H:i:s");
                        $lichhoc = array(
                            'ngay' => $date,
                            'gio' => $gio,
                            'gio_ra' => $giora,
                            'thoi_luong' => $thoiluong,
                            'lop_hoc' => $lophoc,
                            'giao_vien' => $giaovien,
                            'phong_hoc' => $phonghoc,
                        );
                        if (strtotime($date) < strtotime($homnay))
                            $lichhoc['tinh_trang'] = 4;
                        else
                            $lichhoc['tinh_trang'] = 1;
                        $lichhocid = $this->model->addObj($lichhoc);
                        if ($phanloai == 1 && $lichhoc > 0) {
                            $saplop = array(
                                'hoc_vien' => $hocvien,
                                'lich_hoc' => $lichhocid,
                                'lop_hoc' => $lophoc
                            );
                            if (strtotime($date) < strtotime($homnay))
                                $saplop['tinh_trang'] = 2;
                            else
                                $saplop['tinh_trang'] = 1;
                            $this->model->themSaplop($saplop);
                        }
                        $dembuoi++;
                    }
                    $date = date('Y-m-d', strtotime($date . " +1 days"));
                } while ($dembuoi < $buoiconlai);
            }
            if ($dembuoi > 0) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        } else {
            $jsonObj['msg'] = "Đã tạo đủ số lịch học!";
            $jsonObj['success'] = false;
        }
        
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $ngay = functions::convertDate($_REQUEST['ngay']);
        $gio = $_REQUEST['gio'];
        $phonghoc = $_REQUEST['phong_hoc'];
        $giaovien = $_REQUEST['giao_vien'];
        $tinhtrang = $_REQUEST['tinh_trang'];
        $thoiluong = $_REQUEST['thoi_luong'];
        $link = $_REQUEST['link'];
        $data = array(
            'ngay' => $ngay,
            'gio' => $gio,
            'giao_vien' => $giaovien,
            'phong_hoc' => $phonghoc,
            'thoi_luong' => $thoiluong,
            'link' => $link,
            'tinh_trang' => $tinhtrang);
        if ($this->model->updateObj($id, $data)) {
            $lophoc = $_REQUEST['lop_hoc'];
            $thucu = $_REQUEST['thu'];
            $editmulti = isset($_REQUEST['editmulti']) ? 1 : 0;
            if ($editmulti == 1) {
                $listlich = $this->model->getMultiLich($lophoc, $ngay);
                if ($listlich) {
                    foreach ($listlich as $item) {
                        $thu = date('l', strtotime($item['ngay']));
                        $thu = substr($thu, 0, 3);
                        $idmoi = $item['id'];
                        if ($thu == $thucu) {
                            $giora = date_create($gio);
                            date_add($giora, date_interval_create_from_date_string("$thoiluong minutes"));
                            $giora = date_format($giora, "H:i:s");
                            $lichmoi = array(
                                'gio' => $gio,
                                'gio_ra' => $giora,
                                'giao_vien' => $giaovien,
                                'phong_hoc' => $phonghoc,
                                'link' => $link,
                                'thoi_luong' => $thoiluong
                            );
                            $this->model->updateObj($idmoi, $lichmoi);
                        }
                    }
                }
            }
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function duyetlich()
    {
        $duyet = $_REQUEST['duyet'];
        $id = $_REQUEST['id'];
        $thoiluong = $_REQUEST['thoi_luong'];
        $thoiluongmoi = $_REQUEST['thoi_luong_moi'];
        if ($duyet == 1) {
            $thoiluong = $thoiluongmoi;
            $thoiluongmoi = 0;
        } else {
            $thoiluongmoi = $thoiluong;
        }
        $data = array('thoi_luong' => $thoiluong, 'thoi_luong_moi' => $thoiluongmoi);
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function import()
    {
//        $funs = $this->fun;
        $import = true;
//        foreach ($funs as $item) {
//            if ($item['link'] == 'nhap()') {
//                $import = true;
//                break;
//            }
//        }
        if ($import) {
            $cotexcel = [
                ['B', '2020-06'],
                ['C', '2020-07'],
                ['D', '2020-08'],
                ['E', '2020-09'],
                ['F', '2020-10'],
                ['G', '2020-11'],
                ['H', '2020-12'],
                ['I', '2021-01'],
                ['J', '2021-02'],
                ['K', '2021-03'],
                ['L', '2021-04'],
                ['M', '2021-05'],
                ['N', '2021-06'],
                ['O', '2021-07'],
                ['P', '2021-08'],
                ['Q', '2021-09'],
                ['R', '2021-10'],
                ['S', '2021-11'],
                ['T', '2021-12']
            ];
            require_once ROOT_DIR . '/libs/phpexcel/PHPExcel/IOFactory.php';
            try {
                $inputFileType = PHPExcel_IOFactory::identify($_FILES['file']['tmp_name']);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($_FILES['file']['tmp_name']);
                $objReader->setReadDataOnly(true);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $banghi = 0;
                $idlophoc = $objPHPExcel->getActiveSheet()->getCell("C3")->getValue();
                $checklich = $this->model->BuoiDaHoc($idlophoc);
                $checkcot = 0;
                if ($checklich == 0) {
                    $infolop = $this->model->getInfoLop($idlophoc);
                    if (count($infolop) > 0) {
                        $hocvien = $infolop[0]['hoc_vien'];
                        $ngaybatdau = '';
                        for ($row = 6; $row <= 36; $row++) {
                            foreach ($cotexcel as $k => $thang) {
                                $ngay = ($row - 5);
                                if ($ngay < 10)
                                    $ngay = '0' . $ngay;
                                $temp = $objPHPExcel->getActiveSheet()->getCell("$thang[0]$row")->getValue();
                                if ($temp != '') {
                                    $temp = str_replace(" ", "", $temp);
                                    $ngayhoc = $thang[1] . '-' . $ngay;
                                    if ($k >= $checkcot) {
                                        $ngaybatdau = $ngayhoc;
                                        $checkcot = $k;
                                    }
                                    $thoiluong = 0;
                                    if ($temp == '0,5' || $temp == '0.5')
                                        $thoiluong = 30;
                                    else if ($temp == '0,75' || $temp == '0.75')
                                        $thoiluong = 45;
                                    elseif ($temp == '1')
                                        $thoiluong = 60;
                                    elseif ($temp == '1,5' || $temp == '1.5')
                                        $thoiluong = 90;
                                    elseif ($temp == '0.75-0.75' || $temp == '0,75-0,75')
                                        $thoiluong = -1;
                                    if ($thoiluong > 0) {
                                        $lichhoc = [
                                            'ngay' => $ngayhoc,
                                            'gio' => '19:30',
                                            'lop_hoc' => $idlophoc,
                                            'giao_vien' => $infolop[0]['giao_vien'],
                                            'phong_hoc' => 1,
                                            'thoi_luong' => $thoiluong,
                                            'tinh_trang' => 4
                                        ];
                                        $idlichoc = $this->model->addObj($lichhoc);
                                        if ($idlichoc > 0) {
                                            $saplop = [
                                                'hoc_vien' => $hocvien,
                                                'lich_hoc' => $idlichoc,
                                                'lop_hoc' => $idlophoc,
                                                'tinh_trang' => 2
                                            ];
                                            if ($this->model->themSapLop($saplop))
                                                $banghi++;
                                        }
                                    } else if ($thoiluong == -1) {
                                        $buois = explode("-", $temp);
                                        if (count($buois > 1)) {
                                            $lichhoc = [
                                                'ngay' => $ngayhoc,
                                                'gio' => '19:30',
                                                'lop_hoc' => $idlophoc,
                                                'giao_vien' => $infolop[0]['giao_vien'],
                                                'phong_hoc' => 1,
                                                'thoi_luong' => 45,
                                                'tinh_trang' => 4
                                            ];
                                            $idlichoc = $this->model->addObj($lichhoc);
                                            if ($idlichoc > 0) {
                                                $saplop = [
                                                    'hoc_vien' => $hocvien,
                                                    'lich_hoc' => $idlichoc,
                                                    'lop_hoc' => $idlophoc,
                                                    'tinh_trang' => 2
                                                ];
                                                if ($this->model->themSapLop($saplop))
                                                    $banghi++;
                                            }
                                            $lichhoc = [
                                                'ngay' => $ngayhoc,
                                                'gio' => '20:30',
                                                'lop_hoc' => $idlophoc,
                                                'giao_vien' => $infolop[0]['giao_vien'],
                                                'phong_hoc' => 1,
                                                'thoi_luong' => 45,
                                                'tinh_trang' => 4
                                            ];
                                            $idlichoc = $this->model->addObj($lichhoc);
                                            if ($idlichoc > 0) {
                                                $saplop = [
                                                    'hoc_vien' => $hocvien,
                                                    'lich_hoc' => $idlichoc,
                                                    'lop_hoc' => $idlophoc,
                                                    'tinh_trang' => 2
                                                ];
                                                if ($this->model->themSapLop($saplop))
                                                    $banghi++;
                                            }
                                        }
                                    }
                                }

                            }
                            if ($banghi > 0) {
                                $jsonObj['msg'] = "Cập nhật thành công $banghi data";
                                $jsonObj['success'] = true;
                            } else {
                                $jsonObj['msg'] = "Lỗi cập nhật database";
                                $jsonObj['success'] = false;
                            }
                        }
                        if ($ngaybatdau == '')
                            $ngaybatdau = date("Y-m-d");
                        else
                            $ngaybatdau = date("Y-m-d", strtotime("$ngaybatdau +1 days"));
                        $thoiluong = $objPHPExcel->getActiveSheet()->getCell("D3")->getValue();
                        $thoiluong = ($thoiluong!='')?$thoiluong:'45';
                        $buoihoc = $objPHPExcel->getActiveSheet()->getCell("E3")->getValue();
                        if ($buoihoc != '') {
                            $buoihoc = explode(",", $buoihoc);
                            $date = $ngaybatdau;
                            $homnay = date("Y-m-d");
                            $sobuoi = $infolop[0]['so_buoi'];
                            $buoikm = $infolop[0]['buoi_khuyen_mai'];
                            $buoidahoc = $this->model->BuoiDaHoc($idlophoc);
                            $buoiconlai = ($sobuoi + $buoikm) - $buoidahoc;
                            $dembuoi = 0;
                            do {
                                $gio = '';
                                foreach ($buoihoc as $bh) {
                                    $tempbh = explode("-", $bh);
                                    $ckdate = date('N', strtotime($date)) + 1;
                                    if ($ckdate == $tempbh[0]) {
                                        $gio = $tempbh[1];
                                        break;
                                    } else
                                        $gio = '';
                                }
                                if ($gio != '') {
                                    $giora = date_create($gio);
                                    date_add($giora, date_interval_create_from_date_string("$thoiluong minutes"));
                                    $giora = date_format($giora, "H:i:s");
                                    $lichhoc = array(
                                        'ngay' => $date,
                                        'gio' => $gio,
                                        'gio_ra' => $giora,
                                        'thoi_luong' => $thoiluong,
                                        'lop_hoc' => $idlophoc,
                                        'giao_vien' => $infolop[0]['giao_vien'],
                                        'phong_hoc' => 1
                                    );
                                    if (strtotime($date) < strtotime($homnay))
                                        $lichhoc['tinh_trang'] = 4;
                                    else
                                        $lichhoc['tinh_trang'] = 1;
                                    $lichhocid = $this->model->addObj($lichhoc);
                                    if ($lichhocid > 0) {
                                        $saplop = array(
                                            'hoc_vien' => $hocvien,
                                            'lich_hoc' => $lichhocid,
                                            'lop_hoc' => $idlophoc
                                        );
                                        if (strtotime($date) < strtotime($homnay))
                                            $saplop['tinh_trang'] = 2;
                                        else
                                            $saplop['tinh_trang'] = 1;
                                        $this->model->themSaplop($saplop);
                                    }
                                    $dembuoi++;
                                }
                                $date = date('Y-m-d', strtotime($date . " +1 days"));
                            } while ($dembuoi < $buoiconlai);
                        }
                    } else {
                        $jsonObj['msg'] = "Lớp chưa có học viên";
                        $jsonObj['success'] = false;
                    }
                } else {
                    $jsonObj['msg'] = "Lớp đã có lịch học";
                    $jsonObj['success'] = false;
                }
            } catch (Exception $e) {
                $jsonObj['msg'] = "Import dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function delall()
    {
        $lophoc = $_REQUEST['lophoc'];
        $temp = $this->model->delAllObj($lophoc);
        if ($temp) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function tinhtrang()
    {
        $this->view->jsonObj = '[{"id":"1","name":"Mới tạo"},
            {"id":"2","name":"Đã setup"},
            {"id":"3","name":"Đang học"},
            {"id":"4","name":"Hoàn thành"},
            {"id":"5","name":"Đã chốt"},
            {"id":"6","name":"Cọc ngay"},
            {"id":"7","name":"Hủy"},
            {"id":"0","name":"Xóa"}]';
        $this->view->render('common/json');
    }

// Second view

    function view()
    {
        $module = "QUẢN LÝ LỊCH HỌC";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('lichhoc');
        $this->view->render('lichhoc/view');
        require 'layouts/footer.php';
    }

    function jsonview()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $offset = ($page - 1) * $rows;
        $ngay = (isset($_REQUEST['ngay'])) ? $_REQUEST['ngay'] : '';
        $thang = (isset($_REQUEST['thang'])) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam'])) ? $_REQUEST['nam'] : date("Y");
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
        $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : 0;
        $phonghoc = isset($_REQUEST['phonghoc']) ? $_REQUEST['phonghoc'] : 0;
        $giohoc = isset($_REQUEST['gio']) ? $_REQUEST['gio'] : '';
        $tenlop = isset($_REQUEST['tenlop']) ? $_REQUEST['tenlop'] : '';
        $jsonObj = $this->model->getview($offset,$rows,$ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function dayview()
    {
        $module = "QUẢN LÝ LỊCH HỌC";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('lichhoc');
        $this->view->render('lichhoc/day');
        require 'layouts/footer.php';
    }

    function jsonday()
    {
        $ngay = (isset($_REQUEST['ngay'])) ? $_REQUEST['ngay'] : '';
        $thang = (isset($_REQUEST['thang'])) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam'])) ? $_REQUEST['nam'] : date("Y");
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
        $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : 0;
        $phonghoc = isset($_REQUEST['phonghoc']) ? $_REQUEST['phonghoc'] : 0;
        $giohoc = isset($_REQUEST['gio']) ? $_REQUEST['gio'] : '';
        $tenlop = isset($_REQUEST['tenlop']) ? $_REQUEST['tenlop'] : '';
        $jsonObj = $this->model->getday($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function timeview()
    {
        $module = "QUẢN LÝ LỊCH HỌC";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('lichhoc');
        $this->view->render('lichhoc/time');
        require 'layouts/footer.php';
    }

    function jsontime()
    {
        $ngay = (isset($_REQUEST['ngay'])) ? $_REQUEST['ngay'] : '';
        $thang = (isset($_REQUEST['thang'])) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam'])) ? $_REQUEST['nam'] : date("Y");
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
        $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : 0;
        $phonghoc = isset($_REQUEST['phonghoc']) ? $_REQUEST['phonghoc'] : 0;
        $giohoc = isset($_REQUEST['gio']) ? $_REQUEST['gio'] : '';
        $tenlop = isset($_REQUEST['tenlop']) ? $_REQUEST['tenlop'] : '';
        $jsonObj = $this->model->gettime($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function duyetview()
    {
        $module = "DUYỆT LỊCH HỌC";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('lichhoc');
        $this->view->render('lichhoc/duyet');
        require 'layouts/footer.php';
    }

    function jsonduyet()
    {
        $ngay = (isset($_REQUEST['ngay'])) ? $_REQUEST['ngay'] : '';
        $thang = (isset($_REQUEST['thang'])) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam'])) ? $_REQUEST['nam'] : date("Y");
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
        $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : 0;
        $phonghoc = isset($_REQUEST['phonghoc']) ? $_REQUEST['phonghoc'] : 0;
        $giohoc = isset($_REQUEST['gio']) ? $_REQUEST['gio'] : '';
        $tenlop = isset($_REQUEST['tenlop']) ? $_REQUEST['tenlop'] : '';
        $jsonObj = $this->model->getduyet($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lophuy(){
        $jsonObj = $this->model->getlophuy();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function huylich()
    {
        $lophoc = $_REQUEST['lophoc'];
        if ($this->model->huylich($lophoc)) {
            $jsonObj['msg'] = "Success!";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Something wrong, pls contact with admin!";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

}

?>

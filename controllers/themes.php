<?php

class themes extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new Model();
        $this->fun = $model->getfun('themes');
        if ($model->checkright('themes') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        require HEADER;
        $this->view->funs = $this->fun;
        if (MOBILE)
            $this->view->render('themes/index_m');
        else
            $this->view->render('themes/index');
        require FOOTER;
    }

    function changethemes()
    {
        $_SESSION['themes'] = $_REQUEST['themes'];
        echo '1';
    }
}

?>

<?php

class congdoan extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('congdoan') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "CÔNG ĐOÀN";
        require HEADER;
        $this->view->funs = $this->model->getfun('congdoan');
        if (MOBILE) {
            $this->view->taikhoan = isset($_REQUEST['taikhoan']) ? $_REQUEST['taikhoan'] : 1;
            $this->view->name = isset($_REQUEST['name']) ? $_REQUEST['name'] : 'Tiền mặt';
            $this->view->render('congdoan/index_m');
        } else
            $this->view->render('congdoan/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam'] : date("Y");
        $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date("m");
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $nhanvien = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : '';
        $loaicongdoan = isset($_REQUEST['loaicongdoan']) ? $_REQUEST['loaicongdoan'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $thang, $nam, $tukhoa, $loaicongdoan, $nhanvien);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $loaiphieu = $_REQUEST['loai'];
        $nhanvien = $_SESSION['user']['nhan_vien'];
        $ngaygio = date('Y-m-d H:i:s');
//        $taikhoan = $_REQUEST['tai_khoan'];
        $sotien = $_REQUEST['so_tien'];
//        $hachtoan = $_REQUEST['hach_toan'];
        $noidung = $_REQUEST['dien_giai'];
//        $ghichu = $_REQUEST['ghi_chu'];
        $data = ['ngay_gio' => $ngaygio,
            'ngay_cap_nhat' => $ngaygio,
            'nhan_vien' => $nhanvien,
            'nguou_lap_phieu' => $nhanvien,
            'so_tien' => $sotien,
            'dien_giai' => $noidung,
//            'tai_khoan' => $taikhoan,
//            'hach_toan' => $hachtoan,
//            'ghi_chu' => $ghichu,
            'loai' => $loaiphieu,
            'tinh_trang' => 1,
        ];
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công!";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $nhanvien = $_SESSION['user']['nhan_vien'];
        $sotien = $_REQUEST['so_tien'];
//        $hachtoan = $_REQUEST['hach_toan'];
        $noidung = $_REQUEST['dien_giai'];
//        $ghichu = $_REQUEST['ghi_chu'];
        $data = [
            'nhan_vien' => $nhanvien,
            'so_tien' => $sotien,
            'dien_giai' => $noidung,
            //'ngay_cap_nhat' => date('Y-m-d'),
//            'hach_toan' => $hachtoan,
//            'ghi_chu' => $ghichu,
        ];
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công!";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        if ($this->model->delObj($id)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function sodu()
    {
        $ngay = functions::convertDate($_REQUEST['ngay']);
        $temp = $this->model->chotsodu($ngay);
        if ($temp) {
            $jsonObj['msg'] = "Cập nhật thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function capnhat()
    {
        $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam'] : date("Y");
        $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date("m");
        $listnv = $this->model->listNhanvien($thang, $nam);
        $ngaygio = date('Y-m-d');
        $nhanvien = $_SESSION['user']['nhan_vien'];
        $check = 0;
        $datacd = array();
        foreach ($listnv as $nv) {
            $idnv = $nv['id'];
            $congdoan = $this->model->getCongdoan($thang, $nam, $idnv);
            foreach ($congdoan as $cong) {
                $sang = $cong['sang'];
                $chieu = $cong['chieu'];
                $congid = $cong['id'];
                $dimuon = strtotime($cong['gio_vao']) - strtotime($cong['ca_vao']);
                $dimuon = floor($dimuon / 60);
                $ngaycapnhat = $cong['ngay'];
                $checkout = $this->model->checkcongdoan($idnv, $ngaycapnhat, $congid);
                if ($checkout == 0) {
                    if ($sang == 11) {
                        if ($dimuon > 0) {
                            $sotien = $dimuon * 1000;
                            $noidung = 'Đi muộn';
                            $data1 = [
                                'ngay_cap_nhat' => $ngaycapnhat,
                                'ngay_gio' => $ngaygio,
                                'loai_id' => $congid,
                                'nhan_vien' => $idnv,
                                'nguou_lap_phieu' => $nhanvien,
                                'so_tien' => $sotien,
                                'dien_giai' => $noidung,
                                'loai' => 0,
                                'phan_loai' => 1,
                                'tinh_trang' => 1,
                            ];
//                            if ($this->model->addObj($data1))
//                                $checkcong++;
                            array_push($datacd, $data1);
                        }
                    }
                    if ($chieu == 12) {
                        $data2 = [
                            'ngay_cap_nhat' => $ngaycapnhat,
                            'ngay_gio' => $ngaygio,
                            'loai_id' => $congid,
                            'nhan_vien' => $idnv,
                            'nguou_lap_phieu' => $nhanvien,
                            'so_tien' => '20000',
                            'dien_giai' => 'Quên checkout',
                            'loai' => 0,
                            'phan_loai' => 1,
                            'tinh_trang' => 1,
                        ];
//                        if ($this->model->addObj($data2))
//                            $checkcong++;
                        array_push($datacd, $data2);
                    }
                }
            }
            $congviec = $this->model->getCongViec($thang, $nam, $idnv);
            foreach ($congviec as $cv) {
                $viecid = $cv['id'];
                $ngaycapnhat = $cv['updated'];
                $checkviec = $this->model->checkcongviec($idnv, $ngaycapnhat, $viecid);
                if ($checkviec == 0) {
                    if ($cv['tinh_trang'] == '3') {
                        $data1 = [
                            'ngay_cap_nhat' => $ngaycapnhat,
                            'ngay_gio' => $ngaygio,
                            'loai_id' => $viecid,
                            'nhan_vien' => $idnv,
                            'nguou_lap_phieu' => $nhanvien,
                            'so_tien' => '20000',
                            'dien_giai' => 'Trễ deadline',
                            'loai' => 0,
                            'phan_loai' => 2,
                            'tinh_trang' => 1,
                        ];
//                        if ($this->model->addObj($data1))
//                            $checkcv++;
                        array_push($datacd, $data1);
                    }
                    if ($cv['tinh_trang'] == '5') {
                        $data2 = [
                            'ngay_cap_nhat' => $ngaycapnhat,
                            'ngay_gio' => $ngaygio,
                            'loai_id' => $viecid,
                            'nhan_vien' => $idnv,
                            'nguou_lap_phieu' => $nhanvien,
                            'so_tien' => '30000',
                            'dien_giai' => 'Không đạt',
                            'loai' => 0,
                            'phan_loai' => 2,
                            'tinh_trang' => 1,
                        ];
//                        if ($this->model->addObj($data2))
//                            $checkcv++;
                        array_push($datacd, $data2);
                    }
                }
            }

        }
        asort($datacd);
//         print_r($datacd);
        foreach ($datacd as $congdoan) {
//            print_r($congdoan);
//            echo '<br>';
            if ($this->model->addObj($congdoan))
                $check++;
        }
        if ($check > 0) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công!";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

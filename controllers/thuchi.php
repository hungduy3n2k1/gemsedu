<?php

class thuchi extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('thuchi') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "NHẬT KÝ THU CHI";
        require HEADER;
        $this->view->funs = $this->model->getfun('thuchi');
        if (MOBILE) {
            $this->view->taikhoan = isset($_REQUEST['taikhoan']) ? $_REQUEST['taikhoan'] : 1;
            $this->view->name = isset($_REQUEST['name']) ? $_REQUEST['name'] : 'Tiền mặt';
            $this->view->render('thuchi/index_m');
        } else
            $this->view->render('thuchi/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $ngaybd = isset($_REQUEST['ngaybd']) ? functions::convertDate($_REQUEST['ngaybd']) : date("Y-m-d", strtotime('first day of this month'));
        $ngaykt = isset($_REQUEST['ngaykt']) ? functions::convertDate($_REQUEST['ngaykt']) : date("Y-m-d");
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $taikhoan = isset($_REQUEST['taikhoan']) ? $_REQUEST['taikhoan'] : 1;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt, $tukhoa, $taikhoan);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $loaiphieu = $_REQUEST['loai'];
        $khachhang = $_REQUEST['khach_hang'];
        $ngaygio = date('Y-m-d H:i:s');
        $taikhoan = $_REQUEST['tai_khoan'];
        $sotien = $_REQUEST['so_tien'];
        $hachtoan = $_REQUEST['hach_toan'];
        $noidung = $_REQUEST['dien_giai'];
        $ghichu = $_REQUEST['ghi_chu'];
        $invoice = $_REQUEST['invoice'];
        $nhanviensale = $_REQUEST['nhan_vien_sale'];
        $tinhtranginv = isset($_REQUEST['tinhtranginv']) ? $_REQUEST['tinhtranginv'] : 0;
        $data = ['ngay_gio' => $ngaygio,
            'nhan_vien' => $_SESSION['user']['nhan_vien'],
            'invoice' => $invoice,
            'khach_hang' => $khachhang,
            'so_tien' => $sotien,
            'dien_giai' => $noidung,
            'tai_khoan' => $taikhoan,
            'hach_toan' => $hachtoan,
            'ghi_chu' => $ghichu,
            'loai' => $loaiphieu,
            'tinh_trang' => 1,
        ];
        if ($hachtoan == 1)
            $data['nhan_vien_sale'] = $nhanviensale;
        if ($this->model->addObj($data)) {
            if ($invoice > 0) {
                $invoicesubs = $this->model->getInvoicesub($invoice);
                $thongthanhtoan = 0;
                $check = 0;
                foreach ($invoicesubs as $invsub) {
                    $giahan = $invsub['giahan'];
                    if ($giahan == 1) {
                        $checkdv = $this->model->checkdichvu($invsub['id'], $invsub['invoice'], $invsub['dich_vu']);
                        if ($checkdv == 0) {
                            $data1 = array(
                                'invoice' => $invsub['invoice'],
                                'invoice_sub' => $invsub['id'],
                                'dich_vu' => $invsub['loaidichvu'],
                                'product' => $invsub['dich_vu'],
                                'khach_hang' => $khachhang,
                                'ten_mien' => $invsub['ten_mien'],
                                'ngay_bd' => $invsub['ngay_bd'],
                                'ngay_kt' => $invsub['ngay_kt'],
                                'so_luong' => $invsub['so_luong'],
                                'don_vi_tinh' => $invsub['don_vi'],
                                'ghi_chu' => $invsub['ghi_chu'],
                                'tinh_trang' => 1,
                                'lap_inv' => 1
                            );
                            if ($this->model->themdichvu($data1))
                                $check++;
                        }
                    }
                }
                if ($tinhtranginv > 0)
                    $this->model->updateInvoice($invoice, $tinhtranginv);
                if ($check > 0) {
                    $jsonObj['msg'] = "Đã thêm dịch vụ tương ứng";
                    $jsonObj['success'] = true;
                } else {
                    $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                    $jsonObj['success'] = true;
                }
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            }
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công!";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $khachhang = $_REQUEST['khach_hang'];
        $sotien = $_REQUEST['so_tien'];
        $hachtoan = $_REQUEST['hach_toan'];
        $noidung = $_REQUEST['dien_giai'];
        $ghichu = $_REQUEST['ghi_chu'];
        $invoice = $_REQUEST['invoice'];
        $nhanviensale = $_REQUEST['nhan_vien_sale'];
        $tinhtranginv = isset($_REQUEST['tinhtranginv']) ? $_REQUEST['tinhtranginv'] : 0;
        $data = [
            'khach_hang' => $khachhang,
            'so_tien' => $sotien,
            'dien_giai' => $noidung,
            'hach_toan' => $hachtoan,
            'ghi_chu' => $ghichu,
            'invoice' => $invoice
        ];
        if ($hachtoan == 1)
            $data['nhan_vien_sale'] = $nhanviensale;
        if ($this->model->updateObj($id, $data)) {
            if ($invoice > 0) {
                $invoicesubs = $this->model->getInvoicesub($invoice);
                $check = 0;
                foreach ($invoicesubs as $invsub) {
                    $giahan = $invsub['giahan'];
                    if ($giahan == 1) {
                        $checkdv = $this->model->checkdichvu($invsub['id'], $invsub['invoice'], $invsub['dich_vu']);
                        if ($checkdv == 0) {
                            $data1 = array(
                                'invoice' => $invsub['invoice'],
                                'invoice_sub' => $invsub['id'],
                                'dich_vu' => $invsub['loaidichvu'],
                                'product' => $invsub['dich_vu'],
                                'khach_hang' => $khachhang,
                                'ten_mien' => $invsub['ten_mien'],
                                'ngay_bd' => $invsub['ngay_bd'],
                                'ngay_kt' => $invsub['ngay_kt'],
                                'so_luong' => $invsub['so_luong'],
                                'don_vi_tinh' => $invsub['don_vi'],
                                'ghi_chu' => $invsub['ghi_chu'],
                                'tinh_trang' => 1,
                                'lap_inv' => 1
                            );
                            if ($this->model->themdichvu($data1))
                                $check++;
                        }
                    }
                }
                if ($tinhtranginv > 0)
                    $this->model->updateInvoice($invoice, $tinhtranginv);
                if ($check > 0) {
                    $jsonObj['msg'] = "Đã thêm dịch vụ tương ứng";
                    $jsonObj['success'] = true;
                } else {
                    $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                    $jsonObj['success'] = true;
                }
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            }
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công!";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        if ($this->model->delObj($id)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function sodu()
    {
        $ngay = functions::convertDate($_REQUEST['ngay']);
        $taikhoan = $_REQUEST['taikhoan'];
        $temp = $this->model->chotsodu($ngay, $taikhoan);
        if ($temp) {
            $jsonObj['msg'] = "Cập nhật thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function invoice()
    {
        $khachhang = $_REQUEST['khachhang'];
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : 0;
        $jsonObj = $this->model->invoice($khachhang, $tinhtrang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

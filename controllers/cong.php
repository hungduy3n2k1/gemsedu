<?php
// Định nghĩa chấm công
class cong extends controller{
    function __construct()
    {
      parent::__construct();
      $model    = new Model();
      if ($model->checkright('cong')==false)
           header ('Location: '.URL);
    }
    function index()
    {
       require HEADER;
       $this->view->funs=$this->model->getfun('cong');
       if (MOBILE)
          $this->view->render('cong/index_m');
       else
          $this->view->render('cong/index');
       require FOOTER;
    }
  	function json(){
  		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
  		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
  		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay';
  		$order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
  		$offset = ($page-1)*$rows;
  		$jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows);
  		$this->view->jsonObj = json_encode($jsonObj);
      $this->view->render('common/json');
  	}

    function nhom()
   {
       $this->view->jsonObj = '[{"id":"1","name"  :"Ngày công"},{"id":"2","name":"Lễ tết"},{"id":"3","name":"Nghỉ phép"},{"id":"4","name":"Công tác"},{"id":"5","name":"Nghỉ bù"},{"id":"6","name":"Đi muộn về sớm"},{"id":"7","name":"Khác"}]';
       $this->view->render('common/json');
   }

    function add(){
        $name = $_REQUEST['name'];
        $kyhieu = $_REQUEST['ky_hieu'];
        $nhom = $_REQUEST['nhom'];
		    // $ghichu = $_REQUEST['ghi_chu'];
       	$data = array('name' => $name,'ky_hieu'=>$kyhieu,'nhom' => $nhom,'tinh_trang' => 1);
      	if ($this->model->addObj($data)) {
    				$jsonObj['msg'] = "Cập nhật dữ liệu thành công";
    				$jsonObj['success'] = true;
    		} else {
    				$jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
    				$jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update(){
		    $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $kyhieu = $_REQUEST['ky_hieu'];
        $nhom = $_REQUEST['nhom'];
		    //$ghichu = $_REQUEST['ghi_chu'];
       	$data = array('name' => $name,'ky_hieu'=>$kyhieu,'nhom' => $nhom);
       	if ($this->model->updateObj($id, $data)) {
    				$jsonObj['msg'] = "Cập nhật dữ liệu thành công";
    				$jsonObj['success'] = true;
    		} else {
    				$jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
    				$jsonObj['success'] = false;
       	}
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del(){
    		$id = $_REQUEST['id'];
    		if($this->model->delObj($id)){
    			   $jsonObj['msg'] = "Xóa dữ liệu thành công";
             $jsonObj['success'] = true;
    		} else {
    			   $jsonObj['msg'] = "Xóa dữ liệu không thành công";
             $jsonObj['success'] = false;
    		}
    		$this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

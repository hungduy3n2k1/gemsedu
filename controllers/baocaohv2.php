<?php

class baocaohv2 extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new Model();
        $this->fun = $model->getfun('baocaohv2');
        if ($model->checkright('baocaohv2') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "BÁO CÁO HỌC VIÊN ĐANG HỌC";
        require HEADER;
        $this->view->funs = $this->fun;
        if (MOBILE)
            $this->view->render('baocaohv2/index_m');
        else
            $this->view->render('baocaohv2/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 30;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $hocvien = isset($_REQUEST['hocvien']) ? $_REQUEST['hocvien'] : '';
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : '';
        // $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : '';
        // $chuyenmon = isset($_REQUEST['chuyenmon']) ? $_REQUEST['chuyenmon'] : '';
        // $tungay = isset($_REQUEST['tungay']) ? $_REQUEST['tungay'] : "01/".date("m/Y");
        // $tungay = $tungay!=''?functions::convertDate($tungay):'';
        // $denngay = isset($_REQUEST['denngay']) ? $_REQUEST['denngay'] : date('d/m/Y');
        // $denngay = $denngay!=''?functions::convertDate($denngay):'';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $hocvien, $khachhang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

}

?>

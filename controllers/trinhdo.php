<?php
class trinhdo extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('trinhdo') == false) {
            header('Location: ' . URL);
        }
    }

    function index()
    {
        $module = "TÌNH TRẠNG DATA";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('trinhdo');
        $this->view->render('trinhdo/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $name = $_REQUEST['name'];
        $ghichu = $_REQUEST['ghi_chu'];
        $bg = $_REQUEST['bg_color'];
        $text = $_REQUEST['text_color'];
        $data = ['name' => $name, 'ghi_chu' => $ghichu,'bg_color'=>$bg, 'text_color'=>$text, 'tinh_trang' => 1];
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $ghichu = $_REQUEST['ghi_chu'];
        $bg = $_REQUEST['bg_color'];
        $text = $_REQUEST['text_color'];
        $data = ['name' => $name, 'ghi_chu' => $ghichu,'bg_color'=>$bg, 'text_color'=>$text];
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}
?>

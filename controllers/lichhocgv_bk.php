<?php

class lichhocgv extends controller
{
    function __construct()
    {
        parent::__construct();
        //  $model = new Model();
//        if ($model->checkright('lichhocgv') == false)
//            header('Location: ' . URL);
    }

    function index()
    {
        $module = "Schedule";
        require HEADER;
        $this->view->funs = $this->model->getfun('lichhocgv');
        if (MOBILE)
            $this->view->render('lichhocgv/index_m');
        else
            $this->view->render('lichhocgv/index');
        require FOOTER;
    }

    function json()
    {
        $tungay = (isset($_REQUEST['tungay']) && $_REQUEST['tungay'] != '') ? functions::convertDate($_REQUEST['tungay']) : '';
        $denngay = (isset($_REQUEST['denngay']) && $_REQUEST['denngay'] != '') ? functions::convertDate($_REQUEST['denngay']) : '';
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
        $giaovien = $_SESSION['user']['giao_vien'];
        $phonghoc = isset($_REQUEST['phonghoc']) ? $_REQUEST['phonghoc'] : 0;
        $giohoc = isset($_REQUEST['gio']) ? $_REQUEST['gio'] : '';
        $tenlop = isset($_REQUEST['tenlop']) ? $_REQUEST['tenlop'] : '';
        $jsonObj = $this->model->getFetobj($tungay, $denngay, $loai, $giaovien, $phonghoc, $giohoc, $tenlop);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lichhoc()
    {
        $jsonObj = $this->model->lichhoc();
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function dayview()
    {
        $module = "Schedule";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('lichhoc');
        $this->view->render('lichhoc/day');
        require 'layouts/footer.php';
    }

    function jsonday()
    {
        $ngay = (isset($_REQUEST['ngay']) && ($_REQUEST['ngay'] != '')) ? $_REQUEST['ngay'] : '';
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
        $giaovien = $_SESSION['user']['giao_vien'];
        $phonghoc = isset($_REQUEST['phonghoc']) ? $_REQUEST['phonghoc'] : 0;
        $giohoc = isset($_REQUEST['gio']) ? $_REQUEST['gio'] : '';
        $tenlop = isset($_REQUEST['tenlop']) ? $_REQUEST['tenlop'] : '';
        $jsonObj = $this->model->getday($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function timeview()
    {
        $module = "Schedule";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('lichhoc');
        $this->view->render('lichhoc/time');
        require 'layouts/footer.php';
    }

    function jsontime()
    {
        $ngay = (isset($_REQUEST['ngay']) && ($_REQUEST['ngay'] != '')) ? $_REQUEST['ngay'] : '';
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang'] != '')) ? $_REQUEST['thang'] : date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
        $giaovien = $_SESSION['user']['giao_vien'];
        $phonghoc = isset($_REQUEST['phonghoc']) ? $_REQUEST['phonghoc'] : 0;
        $giohoc = isset($_REQUEST['gio']) ? $_REQUEST['gio'] : '';
        $tenlop = isset($_REQUEST['tenlop']) ? $_REQUEST['tenlop'] : '';
        $jsonObj = $this->model->gettime($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $ngaycu = $_REQUEST['ngaycu'];
        $ngay = functions::convertDate($_REQUEST['ngaygio']);
        $gio = $_REQUEST['gio'];
        $data = array(
            'ngay' => $ngay,
            'gio' => $gio);
        if ($this->model->updateObj($id, $data)) {
            $lophoc = $_REQUEST['lop_hoc'];
            $thucu = $_REQUEST['thu'];
            $applyall = isset($_REQUEST['applyall']) ? 1 : 0;
            $applyallweekday = isset($_REQUEST['applyallweekday']) ? 1 : 0;
            $applyalltime = isset($_REQUEST['applyalltime']) ? 1 : 0;
            if ($applyall == 1 || $applyallweekday == 1 || $applyalltime == 1) {
                $listlich = $this->model->getMultiLich($lophoc, $ngaycu);
                $checkngay = (strtotime($ngay) - strtotime($ngaycu)) / 86400;
                if ($listlich) {
                    foreach ($listlich as $item) {
                        $idmoi = $item['id'];
                        $ngaytiep = $item['ngay'];
                        $thu = date('l', strtotime($item['ngay']));
                        if ($thu == $thucu) {
                            $lichmoi = [];
                            if ($checkngay != 0 && $applyall == 1) {
                                $ngaymoi = date("Y-m-d", strtotime("$ngaytiep $checkngay days"));
                                $lichmoi['ngay'] = $ngaymoi;
                            }
                            if ($applyallweekday == 1 || $applyalltime == 1)
                                $lichmoi['gio'] = $gio;
                            if (isset($lichmoi))
                                $this->model->updateObj($idmoi, $lichmoi);
                        } elseif ($applyalltime == 1) {
                            $lichmoi = [];
                            $lichmoi['gio'] = $gio;
                            $this->model->updateObj($idmoi, $lichmoi);
                        }
                    }
                }
            }
            $jsonObj['msg'] = "Success";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Failed";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function suathoiluong()
    {
        $id = $_REQUEST['id'];
        $thoiluongmoi = $_REQUEST['thoi_luong_moi'];
        $thoiluong = $_REQUEST['thoi_luong'];
        if ($thoiluong == $thoiluongmoi)
            $thoiluongmoi = 0;
        $data = array('thoi_luong_moi' => $thoiluongmoi);
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Success";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Failed";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $ngaybatdau = functions::convertDate($_REQUEST['ngaygio']);
        if(strtotime($ngaybatdau)>=strtotime(date('Y-m-d'))) {
            $gio = $_REQUEST['gio'];
            $lophoc = $_REQUEST['lop_hoc'];
            $thoiluong = $_REQUEST['thoi_luong'];
            $giaovien = $_SESSION['user']['giao_vien'];
            $buoihoc = isset($_REQUEST['buoihoc']) ? $_REQUEST['buoihoc'] : 0;
            $sobuoi = $_REQUEST['sobuoi'];
            $buoikm = $_REQUEST['buoikm'];
            $hocvien = $_REQUEST['hocvien'];
            $dembuoi = 0;
            if ($buoihoc > 0 && $ngaybatdau != '') {
                $date = $ngaybatdau;
                $homnay = date("Y-m-d");
                $buoidahoc = $this->model->BuoiDaHoc($lophoc);
                $buoiconlai = ($sobuoi + $buoikm) - $buoidahoc;
                do {
                    $giora = date_create($gio);
                    date_add($giora, date_interval_create_from_date_string("$thoiluong minutes"));
                    $giora = date_format($giora, "H:i:s");
                    if (in_array(date('l', strtotime($date)), $buoihoc)) {
                        $lichhoc = array(
                            'ngay' => $date,
                            'gio' => $gio,
                            'gio_ra' => $giora,
                            'thoi_luong' => $thoiluong,
                            'lop_hoc' => $lophoc,
                            'giao_vien' => $giaovien,
                            'phong_hoc' => 1
                        );
                        if (strtotime($date) < strtotime($homnay))
                            $lichhoc['tinh_trang'] = 4;
                        else
                            $lichhoc['tinh_trang'] = 1;
                        $lichhocid = $this->model->addObj($lichhoc);
                        if ($lichhoc > 0) {
                            $saplop = array(
                                'hoc_vien' => $hocvien,
                                'lich_hoc' => $lichhocid,
                                'lop_hoc' => $lophoc
                            );
                            if (strtotime($date) < strtotime($homnay))
                                $saplop['tinh_trang'] = 3;
                            else
                                $saplop['tinh_trang'] = 1;
                            $this->model->themSaplop($saplop);
                        }
                        $dembuoi++;
                    }
                    $date = date('Y-m-d', strtotime($date . " +1 days"));
                } while ($dembuoi < $buoiconlai);
            }
            if ($dembuoi > 0) {
                $jsonObj['msg'] = "Success";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Failed";
                $jsonObj['success'] = false;
            }
        }else{
            $jsonObj['msg'] = "Failed";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function change()
    {
        $ngaybatdau = functions::convertDate($_REQUEST['ngaygio']);
        if(strtotime($ngaybatdau)>=strtotime(date('Y-m-d'))) {
            $gio = $_REQUEST['gio'];
            $lophoc = $_REQUEST['lop_hoc'];
            $this->model->deleteForChange($lophoc);
            $thoiluong = $_REQUEST['thoi_luong'];
            $giaovien = $_SESSION['user']['giao_vien'];
            $buoihoc = isset($_REQUEST['buoihoc']) ? $_REQUEST['buoihoc'] : 0;
            $sobuoi = $_REQUEST['sobuoi'];
            $buoikm = $_REQUEST['buoikm'];
            $hocvien = $_REQUEST['hocvien'];
            $dembuoi = 0;
            if ($buoihoc > 0 && $ngaybatdau != '') {
                $date = $ngaybatdau;
                $homnay = date("Y-m-d");
                $buoidahoc = $this->model->BuoiDaHoc($lophoc);
                $buoiconlai = ($sobuoi + $buoikm) - $buoidahoc;
                do {
                    $giora = date_create($gio);
                    date_add($giora, date_interval_create_from_date_string("$thoiluong minutes"));
                    $giora = date_format($giora, "H:i:s");
                    if (in_array(date('l', strtotime($date)), $buoihoc)) {
                        $lichhoc = array(
                            'ngay' => $date,
                            'gio' => $gio,
                            'gio_ra' => $giora,
                            'thoi_luong' => $thoiluong,
                            'lop_hoc' => $lophoc,
                            'giao_vien' => $giaovien,
                            'phong_hoc' => 1
                        );
                        if (strtotime($date) < strtotime($homnay))
                            $lichhoc['tinh_trang'] = 4;
                        else
                            $lichhoc['tinh_trang'] = 1;
                        $lichhocid = $this->model->addObj($lichhoc);
                        if ($lichhoc > 0) {
                            $saplop = array(
                                'hoc_vien' => $hocvien,
                                'lich_hoc' => $lichhocid,
                                'lop_hoc' => $lophoc
                            );
                            if (strtotime($date) < strtotime($homnay))
                                $saplop['tinh_trang'] = 3;
                            else
                                $saplop['tinh_trang'] = 1;
                            $this->model->themSaplop($saplop);
                        }
                        $dembuoi++;
                    }
                    $date = date('Y-m-d', strtotime($date . " +1 days"));
                } while ($dembuoi < $buoiconlai);
            }
            if ($dembuoi > 0) {
                $jsonObj['msg'] = "Success";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Failed";
                $jsonObj['success'] = false;
            }
        }else{
            $jsonObj['msg'] = "Failed";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lophoc()
    {
        $giaovien = $_SESSION['user']['giao_vien'];
        $phanloai = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : 0;
        $jsonObj = $this->model->lophoc($giaovien, $phanloai);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function lophoc1()
    {
        $giaovien = $_SESSION['user']['giao_vien'];
        $phanloai = isset($_REQUEST['phanloai']) ? $_REQUEST['phanloai'] : 0;
        $jsonObj = $this->model->lophoc1($giaovien, $phanloai);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function dellich()
    {
        $lophoc = $_REQUEST['lophoc'];
        if ($this->model->DelLichhoc($lophoc)) {
            $jsonObj['msg'] = "Success";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Failed $lophoc";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function huylich()
    {
        $lichhoc = $_REQUEST['lichhoc'];
        if ($this->model->huylich($lichhoc)) {
            $jsonObj['msg'] = "Success!";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Something wrong, pls contact with admin! $lichhoc";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

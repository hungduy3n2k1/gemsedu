<?php
class bangchamcong extends Controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new model();
        if ($model->checkright('bangchamcong')==false)
           header('Location: ' . URL);
    }

    function index()
    {
        require HEADER;
        $this->view->funs=$this->model->getfun('bangchamcong');
        if (MOBILE)
           $this->view->render('bangchamcong/index_m');
        else
           $this->view->render('bangchamcong/index');
        require FOOTER;
    }

    function json()
    {
        $page                = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows                = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort                = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order               = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
        $offset              = ($page - 1) * $rows;
        $thang = (isset($_REQUEST['thang']) && ($_REQUEST['thang']!=''))?$_REQUEST['thang']: date("m");
        $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam']!=''))?$_REQUEST['nam']: date("Y");
        $jsonObj             = $this->model->getFetObj($nam,$thang,$sort, $order, $offset, $rows);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $thang = $_REQUEST['thang'];
        $nam = $_REQUEST['nam'];
        if ($this->model->addObj($thang,$nam)) {
                $jsonObj['msg']     = "Tạo bảng chấm công thành công";
                $jsonObj['success'] = true;
        } else {
                $jsonObj['msg']     = "Bảng chấm công đã tồn tại";
                $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    // function chamcong()
    // {
    //     $thang = $_REQUEST['thang'];
    //     $nam = $_REQUEST['nam'];
    //     if ($this->model->chamcong($thang, $nam)) {
    //             $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
    //             $jsonObj['success'] = true;
    //     } else {
    //             $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
    //             $jsonObj['success'] = false;
    //     }
    //     $this->view->jsonObj = json_encode($jsonObj);
    //     $this->view->render('common/json');
    // }
    //
    // function del()
    // {
    //     $id = $_REQUEST['id'];
    //     // $data      = array('tinh_trang'=>0);
    //     if ($this->model->del($id)) {
    //         $jsonObj['msg']     = "Xóa dữ liệu thành công";
    //         $jsonObj['success'] = true;
    //     } else {
    //         $jsonObj['msg']     = "Xóa dữ liệu không thành công";
    //         $jsonObj['success'] = false;
    //     }
    //     $this->view->jsonObj = json_encode($jsonObj);
    //     $this->view->render('common/json');
    // }
}
?>

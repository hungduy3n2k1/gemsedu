<?php

class denghi extends controller
{
    private $fun;

    function __construct()
    {
        parent::__construct();
        $model = new Model();
        $this->fun = $model->getfun('denghi');
        if ($model->checkright('denghi') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module = "ĐỀ NGHỊ TẠM ỨNG/THANH TOÁN";
        require HEADER;
        $this->view->funs = $this->fun;
        if (MOBILE) {
            $this->view->render('denghi/index_m');
        } else
            $this->view->render('denghi/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'ngay';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $ngaybd = isset($_REQUEST['ngaybd']) ? functions::convertDate($_REQUEST['ngaybd']) : date("Y-m-d", strtotime('first day of this month'));
        $ngaykt = isset($_REQUEST['ngaykt']) ? functions::convertDate($_REQUEST['ngaykt']) : date("Y-m-d");
        $duyet = false;
        foreach ($this->fun as $item)
            if ($item['link'] == 'duyet()')
                $duyet = true;
        if ($duyet)
            $nhanvien = isset($_REQUEST['nhanvien']) ? $_REQUEST['nhanvien'] : 0;
        else
            $nhanvien = $_SESSION['user']['id'];
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : '';
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt, $nhanvien, $tinhtrang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $duyet = false;
        foreach ($this->fun as $item)
            if ($item['link'] == 'duyet()')
                $duyet = true;
        if ($duyet) {
            $ngaygio = functions::convertDate($_REQUEST['ngaygio']);
            $nhanvien = $_REQUEST['nhan_vien'];
        } else {
            $nhanvien = $_SESSION['user']['id'];
            $ngaygio = date('Y-m-d');
        }
        $sotien = $_REQUEST['so_tien'];
        $noidung = $_REQUEST['noi_dung'];
        $data = [
            'ngay' => $ngaygio,
            'so_tien' => $sotien,
            'noi_dung' => $noidung,
            'nhan_vien' => $nhanvien,
            'tinh_trang' => 1,
        ];
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Đã thêm phiếu tạm ứng/thanh toán";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }

        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $duyet = false;
        foreach ($this->fun as $item)
            if ($item['link'] == 'duyet()')
                $duyet = true;
        $id = $_REQUEST['id'];
        $sotien = $_REQUEST['so_tien'];
        $noidung = $_REQUEST['noi_dung'];
        $data = [
            'so_tien' => $sotien,
            'noi_dung' => $noidung
        ];
        if ($duyet) {
            $ngaygio = functions::convertDate($_REQUEST['ngaygio']);
            $data['ngay'] = $ngaygio;
        }
        if ($this->model->updateObj($id, $data, $duyet)) {
            $jsonObj['msg'] = "Đã cập nhật phiếu tạm ứng/thanh toán";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function duyet()
    {
        $duyet = false;
        foreach ($this->fun as $item)
            if ($item['link'] == 'duyet()')
                $duyet = true;
        if ($duyet) {
            $id = $_REQUEST['id'];
            $tinhtrang = $_REQUEST['tinh_trang'];
            $data = [
                'tinh_trang' => $tinhtrang
            ];
            if ($this->model->duyetphieu($id, $data)) {
                $jsonObj['msg'] = "Đã cập nhật phiếu tạm ứng/thanh toán";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
            $this->view->jsonObj = json_encode($jsonObj);
            $this->view->render('common/json');
        }
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $duyet = false;
        foreach ($this->fun as $item)
            if ($item['link'] == 'duyet()')
                $duyet = true;
        if ($this->model->delObj($id, $duyet)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

?>

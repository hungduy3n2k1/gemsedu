<?php
class lichdaythay extends controller
{
    function __construct()
    {
        parent::__construct();
        $model    = new model();
        if ($model->checkright('lichdaythay') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        require 'layouts/header.php';
        $this->view->render('lichdaythay/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        // $ngay = (isset($_REQUEST['ngay'])) ? $_REQUEST['ngay'] : '';
        // $thang = (isset($_REQUEST['thang'])) ? $_REQUEST['thang'] : date("m");
        // $nam = (isset($_REQUEST['nam']) && ($_REQUEST['nam'] != '')) ? $_REQUEST['nam'] : date("Y");
        // $loai = isset($_REQUEST['loai']) ? $_REQUEST['loai'] : 0;
        $giaovien = isset($_REQUEST['giaovien']) ? $_REQUEST['giaovien'] : 0;
        // $phonghoc = isset($_REQUEST['phonghoc']) ? $_REQUEST['phonghoc'] : 0;
        // $giohoc = isset($_REQUEST['gio']) ? $_REQUEST['gio'] : '';
        $tenlop = isset($_REQUEST['tenlop']) ? $_REQUEST['tenlop'] : '';
        $page                = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows                = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
        $sort                = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
        $order               = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'DESC';
        $offset              = ($page - 1) * $rows;
        $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows, $giaovien, $tenlop);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function duyet()
    {
        $data = $_REQUEST['data'];
        if ($this->model->updateObj($data)) {
            $this->model->updateLichHoc($data);
            $jsonObj['msg'] = "Duyệt dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Duyệt dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
}

<?php

class nhanvien extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new Model();
        if ($model->checkright('nhanvien') == false)
            header('Location: ' . URL);
    }

    function index()
    {
        $module="NHÂN VIÊN";
        require HEADER;
        $this->view->funs = $model->getfun('nhanvien');
        if (MOBILE)
            $this->view->render('nhanvien/index_m');
        else
            $this->view->render('nhanvien/index');
        require FOOTER;
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC';
        $offset = ($page - 1) * $rows;
        $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
        $thang = isset($_REQUEST['thang']) ? $_REQUEST['thang'] : date('m');
        $nam = isset($_REQUEST['nam']) ? $_REQUEST['nam'] :  date('Y');
        $tinhtrang = isset($_REQUEST['tinhtrang']) ? $_REQUEST['tinhtrang'] : 0;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tukhoa, $thang, $nam,$tinhtrang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function tinhtrang()
     {
             $this->view->jsonObj = '[{"id":"1","name":"fresher"},{"id":"2","name":"Thử việc"},{"id":"3","name":"Chính thức"},{"id":"4","name":"freelancer"},{"id":"5","name":"Tạm ngừng"},{"id":"6","name":"Thôi việc"}]';
             $this->view->render('common/json');
     }

    function add()
    {
        $name = $_REQUEST['name'];
        $gioitinh = $_REQUEST['gioi_tinh'];
        $ngaysinh = functions::convertDate($_REQUEST['ngaysinh']);
        $honnhan = $_REQUEST['hon_nhan'];
        $diachi = $_REQUEST['dia_chi'];
        $quequan = $_REQUEST['que_quan'];
        $cmnd = $_REQUEST['cmnd'];
        $noicap = $_REQUEST['noi_cap'];
        $ngaycap = functions::convertDate($_REQUEST['ngaycap']);
        $thuongtru = $_REQUEST['thuong_tru'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $emailcanhan = $_REQUEST['email_canhan'];
//        $trinhdo = $_REQUEST['trinh_do'];
//        $truonghoc = $_REQUEST['truong_hoc'];
        $zalo = $_REQUEST['zalo'];
        $facebook = $_REQUEST['facebook'];
        $data = array(
            'name' => $name,
            'gioi_tinh' => $gioitinh,
            'ngay_sinh' => $ngaysinh,
            'hon_nhan' => $honnhan,
            'dia_chi' => $diachi,
            'que_quan' => $quequan,
            'cmnd' => $cmnd,
            'noi_cap' => $noicap,
            'ngay_cap' => $ngaycap,
            'thuong_tru' => $thuongtru,
            'dien_thoai' => $dienthoai,
            'email_canhan' => $emailcanhan,
//            'trinh_do' => $trinhdo,
//            'truong_hoc' => $truonghoc,
            'facebook' => $facebook,
            'zalo' => $zalo,
            'email_canhan' => $emailcanhan,
            'tinh_trang' => 1
        );
        if ($this->model->addObj($data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $gioitinh = $_REQUEST['gioi_tinh'];
        $ngaysinh = ($_REQUEST['ngaycap']!='')?functions::convertDate($_REQUEST['ngaysinh']):'0000-00-00';
        $honnhan = $_REQUEST['hon_nhan'];
        $diachi = $_REQUEST['dia_chi'];
        $quequan = $_REQUEST['que_quan'];
        $cmnd = $_REQUEST['cmnd'];
        $noicap = $_REQUEST['noi_cap'];
        $ngaycap = ($_REQUEST['ngaycap']!='')?functions::convertDate($_REQUEST['ngaycap']):'0000-00-00';
        $thuongtru = $_REQUEST['thuong_tru'];
        $dienthoai = $_REQUEST['dien_thoai'];
        $emailcanhan = $_REQUEST['email_canhan'];
//        $trinhdo = $_REQUEST['trinh_do'];
//        $truonghoc = $_REQUEST['truong_hoc'];
        $zalo = $_REQUEST['zalo'];
        $facebook = $_REQUEST['facebook'];
        $data = array(
            'name' => $name,
            'gioi_tinh' => $gioitinh,
            'ngay_sinh' => $ngaysinh,
            'hon_nhan' => $honnhan,
            'dia_chi' => $diachi,
            'que_quan' => $quequan,
            'cmnd' => $cmnd,
            'noi_cap' => $noicap,
            'ngay_cap' => $ngaycap,
            'thuong_tru' => $thuongtru,
            'dien_thoai' => $dienthoai,
            'email_canhan' => $emailcanhan,
//            'trinh_do' => $trinhdo,
//            'truong_hoc' => $truonghoc,
            'facebook' => $facebook,
            'zalo' => $zalo,
            'email_canhan' => $emailcanhan
        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công".$ngaycap;
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        $temp = $this->model->delObj($id);
        if ($temp) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function hopdong()
    {
        $id = $_REQUEST['id'];
        $ngaydilam = ($_REQUEST['ngaydilam']!='')?functions::convertDate($_REQUEST['ngaydilam']):'0000-00-00';
        $tinhtrang = $_REQUEST['tinh_trang'];
        $phongban = $_REQUEST['phong_ban'];
        $email = $_REQUEST['email'];
        $luong = $_REQUEST['luong'];
        $baohiem = $_REQUEST['bao_hiem'];
        $anca = $_REQUEST['an_ca'];
        $guixe = $_REQUEST['gui_xe'];
        $masothue = $_REQUEST['ma_so_thue'];
        $sobaohiem = $_REQUEST['so_bao_hiem'];
        $sotk = $_REQUEST['so_tk'];
        $nganhang = $_REQUEST['ngan_hang'];
        $chutk = $_REQUEST['chu_tk'];
        $ngayketthuc = ($_REQUEST['ngayketthuc']!='')?functions::convertDate($_REQUEST['ngayketthuc']):'0000-00-00';
        $ghichu = $_REQUEST['ghi_chu'];
        $vanphong = $_REQUEST['van_phong'];
        $ca = $_REQUEST['ca'];
        $data = array(
            'ngay_di_lam' => $ngaydilam,
            'phong_ban' => $phongban,
            'tinh_trang' => $tinhtrang,
            'email' => $email,
            'luong' => $luong,
            'bao_hiem' => $baohiem,
            'an_ca' => $anca,
            'gui_xe' => $guixe,
            'ma_so_thue' => $masothue,
            'so_bao_hiem' => $sobaohiem,
            'so_tk' => $sotk,
            'ngan_hang' => $nganhang,
            'chu_tk' => $chutk,
            'ngay_ket_thuc' => $ngayketthuc,
            'ghi_chu' => $ghichu,
            'van_phong' => $vanphong,
            'ca' => $ca
        );
        if ($this->model->updateObj($id, $data)) {
            $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

}

?>

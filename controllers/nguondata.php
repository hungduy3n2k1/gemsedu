<?php
class nguondata extends controller
{
   function __construct()
   {
       parent::__construct();
       $model    = new model();
       if ($model->checkright('nguondata')==false)
          header('Location: ' . URL);
   }

   function index()
   {
       require 'layouts/header.php';
       $this->view->render('nguondata/index');
       require 'layouts/footer.php';
   }

   function json()
   {
       $page                = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
       $rows                = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 50;
       $sort                = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : 'id';
       $order               = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'ASC';
       $offset              = ($page - 1) * $rows;
       $jsonObj             = $this->model->getFetObj($sort, $order, $offset, $rows);
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }


   function add()
   {
       $name = $_REQUEST['name'];
       $data      = array('name' => $name,'tinh_trang'=>1);
       if ($this->model->addObj($data)) {
               $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
               $jsonObj['success'] = true;
           } else {
               $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
               $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

   function update()
   {
       $id     = $_REQUEST['id'];
       $name = $_REQUEST['name'];
       $data      = array('name' => $name);
       if ($this->model->updateObj($id, $data)) {
               $jsonObj['msg']     = "Cập nhật dữ liệu thành công";
               $jsonObj['success'] = true;
           } else {
               $jsonObj['msg']     = "Cập nhật dữ liệu không thành công";
               $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

   function del()
   {
       $id = $_REQUEST['id'];
       if ($this->model->delObj($id)) {
           $jsonObj['msg']     = "Xóa dữ liệu thành công";
           $jsonObj['success'] = true;
       } else {
           $jsonObj['msg']     = "Xóa dữ liệu không thành công";
           $jsonObj['success'] = false;
       }
       $this->view->jsonObj = json_encode($jsonObj);
       $this->view->render('common/json');
   }

}
?>

<?php

class phieuthu extends controller
{
    function __construct()
    {
        parent::__construct();
        $model = new model();
        if ($model->checkright('phieuthu') == false) {
            header('Location: ' . URL);
        }
    }

    function index()
    {
        $module = "PHIẾU THU";
        require 'layouts/header.php';
        $this->view->funs = $this->model->getfun('phieuthu');
        $this->view->render('phieuthu/index');
        require 'layouts/footer.php';
    }

    function json()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 50;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';
        $offset = ($page - 1) * $rows;
        $tungay = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : date("Y-m-d", strtotime("first day of this month"));
        $denngay = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : date("Y-m-d");
        $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : 0;
        $taikhoan = isset($_REQUEST['taikhoan']) ? $_REQUEST['taikhoan'] : 0;
        $jsonObj = $this->model->getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $khachhang, $taikhoan);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function add()
    {
        $donhang = $_REQUEST['donhang'];
        $khachhang = $_REQUEST['khach_hang'];
        $taikhoan = $_REQUEST['tai_khoan'];
        $ngaygio = functions::convertDate($_REQUEST['ngaygio']) . ' ' . date('H:i:s');
        $sotien = $_REQUEST['so_tien'];
        $sotien = str_replace(",", "", $sotien);
        $noidung = $_REQUEST['dien_giai'];
        $ghichu = $_REQUEST['ghi_chu'];
        $invoice = $_REQUEST['invoice'];
        $duno = $_REQUEST['duno'];
        if($sotien <= $duno){
            $data = [
                'ngay_gio' => $ngaygio,
                'nhan_vien' => $_SESSION['user']['nhan_vien'],
                'khach_hang' => $khachhang,
                'so_tien' => $sotien,
                'dien_giai' => $noidung,
                'tai_khoan' => $taikhoan,
                'ghi_chu' => $ghichu,
                'loai' => 0,
                'invoice' => $invoice,
                'tinh_trang' => 1,
            ];
            if ($this->model->addObj($data)) {
                $datadh = ['tinh_trang' => 2];
                $dunomoi = $duno - $sotien;
                if($dunomoi > 0){
                    $data = [
                        'du_no' => $dunomoi,
                        'tinh_trang' => 2,
                    ];
                    $this->model->updateInvoice($invoice,$data);
                } else if($dunomoi == 0) {
                    $data = [
                        'du_no' => $dunomoi,
                        'tinh_trang' => 3,
                    ];
                    $this->model->updateInvoice($invoice,$data);
                }
                
                $this->model->updateDonHang($donhang,$datadh);
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công!";
                $jsonObj['success'] = false;
            }
        } else {
            $jsonObj['msg'] = "Số tiền bạn nhập vượt quá dư nợ của đợt thanh toán!";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function update()
    {
        $id = $_REQUEST['id'];
        $invoice = $_REQUEST['invoice'];
        $noidung = $_REQUEST['dien_giai'];
        $ghichu = $_REQUEST['ghi_chu'];
        $sotien = $_REQUEST['so_tien'];
        $sotiencu = $_REQUEST['so_tien_cu'];
        $duno = $_REQUEST['duno'];
        $ngaygio = $_REQUEST['ngaygio'] != '' ? functions::convertDate($_REQUEST['ngaygio']):date('Y-m-d');
        $data = ['dien_giai' => $noidung, 'ghi_chu' => $ghichu, 'ngay_gio' => $ngaygio, 'so_tien' => $sotien];
        $dunomoi = $sotiencu - $sotien + $duno;
        if($dunomoi >= 0){
            if($dunomoi > 0){
                $data1 = [
                    'du_no' => $dunomoi,
                    'tinh_trang' => 2,
                ];
                $this->model->updateInvoice($invoice,$data1);
            } else if($dunomoi == 0) {
                $data1 = [
                    'du_no' => $dunomoi,
                    'tinh_trang' => 3,
                ];
                $this->model->updateInvoice($invoice,$data1);
            }
            if ($this->model->updateObj($id, $data)) {
                $jsonObj['msg'] = "Cập nhật dữ liệu thành công";
                $jsonObj['success'] = true;
            } else {
                $jsonObj['msg'] = "Cập nhật dữ liệu không thành công";
                $jsonObj['success'] = false;
            }
        } else {
            $jsonObj['msg'] = "Số tiền bạn nhập vượt quá dư nợ của đợt thanh toán!";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function del()
    {
        $id = $_REQUEST['id'];
        if ($this->model->delObj($id)) {
            $jsonObj['msg'] = "Xóa dữ liệu thành công";
            $jsonObj['success'] = true;
        } else {
            $jsonObj['msg'] = "Xóa dữ liệu không thành công";
            $jsonObj['success'] = false;
        }
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function inphieu()
    {
        $id = $_REQUEST['id'];
        $this->view->phieu = $this->model->phieuthu($id);
        $this->view->render('phieuthu/inphieu');
    }

    function donhang()
    {
        $khachhang = $_REQUEST['khachhang'];
        $jsonObj = $this->model->donhang($khachhang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function khachhang()
    {
        $khachhang = $_REQUEST['khachhang'];
        $jsonObj = $this->model->khachhang($khachhang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }

    function invoice()
    {
        $donhang = $_REQUEST['donhang'];
        $jsonObj = $this->model->invoice($donhang);
        $this->view->jsonObj = json_encode($jsonObj);
        $this->view->render('common/json');
    }
    
    // function xuatfile()
    // {
    //     $tungay = isset($_REQUEST['tungay']) ? functions::convertDate($_REQUEST['tungay']) : date("Y-m-d", strtotime("first day of this month"));
    //     $denngay = isset($_REQUEST['denngay']) ? functions::convertDate($_REQUEST['denngay']) : date("Y-m-d");
    //     $khachhang = isset($_REQUEST['khachhang']) ? $_REQUEST['khachhang'] : 0;
    //     $tukhoa = isset($_REQUEST['tukhoa']) ? $_REQUEST['tukhoa'] : '';
    //     $jsonObj = $this->model->getFetObj('id', 'DESC', 0, 10000000, $tungay, $denngay, $khachhang, $tukhoa);
    //     $this->view->tungay = date("d/m/Y", strtotime($tungay));
    //     $this->view->denngay = date("d/m/Y", strtotime($denngay));
    //     $this->view->jsonObj = $jsonObj;
    //     $this->view->render('phieuthu/xuatfile');
    // }

}

?>

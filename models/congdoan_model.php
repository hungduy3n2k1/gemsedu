<?php

class congdoan_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $thang, $nam, $tukhoa, $loaicongdoan, $nhanvien)
    {
        $result = array();

        $dieukien = " WHERE tinh_trang=1 ";
        $result['rows'] = array();
        if ($thang != '' && $nam != '') {
            $thangnam = $nam . '-' . $thang;
            $dieukien .= " AND ngay_cap_nhat LIKE '$thangnam%'  ";
            $thangtruoc = $thang - 1;
            if ($thangtruoc == '0') {
                $thangtruoc = 12;
                $nam1 = $nam - 1;
            } else {
                $nam1 = $nam;
            }
            $thangnam1 = $nam1 . '-' . $thangtruoc;
            $dieukien1 = " WHERE tinh_trang=1 AND ngay_cap_nhat LIKE '$thangnam1%'  ";
            $query = $this->db->query("SELECT *,
           IF(ngay_gio='0000-00-00','',DATE_FORMAT(ngay_gio, '%d/%m/%Y')) AS ngay,
            IF(phan_loai=1,DATE_FORMAT(ngay_cap_nhat, '%d/%m/%Y'),IF(phan_loai=2,
            (SELECT name FROM congviecsub WHERE id=a.loai_id),'')) AS ghichu,
           (SELECT name FROM nhanvien WHERE id=a.nhan_vien) as nhanvien,
           IF(loai=0,so_tien,'') AS thu, IF(loai=1,so_tien,'') AS chi
           FROM congdoan a $dieukien1 ORDER BY id DESC LIMIT 1");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            if($temp) {
                $temp[0]['ghichu']='';
                $temp[0]['ngay']='01/'.$thang.'/'.$nam;
                $temp[0]['thu']='';
                $temp[0]['chi']='';
                $temp[0]['dien_giai']=' Số dư đầu tháng ';
                if ($nhanvien == '')
                array_push($result['rows'], $temp[0]);
            }
        }
//        if ($loaicongdoan != '')
//            $dieukien .= " AND loai=$loaicongdoan ";
//        if ($tukhoa != '')
//            $dieukien .= " AND (name LIKE '%" . $tukhoa . "%' OR dien_giai LIKE '%" . $tukhoa . "%' ) ";
        if ($nhanvien != '')
            $dieukien .= " AND nhan_vien=$nhanvien ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM congdoan $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *,
           IF(ngay_gio='0000-00-00','',DATE_FORMAT(ngay_gio, '%d/%m/%Y')) AS ngay,
            IF(phan_loai=1,DATE_FORMAT(ngay_cap_nhat, '%d/%m/%Y'),IF(phan_loai=2,
            (SELECT name FROM congviecsub WHERE id=a.loai_id),'')) AS ghichu,
           (SELECT name FROM nhanvien WHERE id=a.nhan_vien) as nhanvien,
           IF(loai=0,so_tien,'') AS thu, IF(loai=1,so_tien,'') AS chi
           FROM congdoan a $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongthu = isset($result['rows'][0]['thu'])?$result['rows'][0]['thu']:0;
        $tongchi = isset($result['rows'][0]['chi'])?$result['rows'][0]['chi']:0;
        foreach ($temp as $item) {
            array_push($result['rows'], $item);
            $tongthu = $tongthu + $item['thu'];
            $tongchi = $tongchi + $item['chi'];
        }
        $result['footer'] = array(0 => array('dien_giai' => 'Tổng cộng', 'thu' => $tongthu, 'chi' => $tongchi, 'so_du' => 0));
        return $result;
    }

    function addObj($data)
    {

        $query = $this->db->query("SELECT so_du FROM congdoan WHERE tinh_trang=1 ORDER BY id DESC LIMIT 1");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($data['loai'] == 0)
            $data['so_du'] = (isset($temp[0]['so_du']) ? $temp[0]['so_du'] : 0) + $data['so_tien'];
        else
            $data['so_du'] = (isset($temp[0]['so_du']) ? $temp[0]['so_du'] : 0) - $data['so_tien'];
        $query = $this->insert("congdoan", $data);
        return $query;
    }

    function updateObj($id, $data)
    {
        $query = $this->db->query("SELECT * FROM congdoan WHERE id = $id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $dulieu = json_encode($temp[0]);
        $sotien = $temp[0]['so_tien'];
        $loai = $temp[0]['loai'];
        if ($data['so_tien'] == $sotien) {
            $query = $this->update("congdoan", $data, " id= $id ");
        } else {
            $saiso = $data['so_tien'] - $sotien;
            if ($loai == 0)
                $sodudau = $temp[0]['so_du'] + $saiso;
            else
                $sodudau = $temp[0]['so_du'] - $saiso;
            $data['so_du'] = $sodudau;
            $query = $this->update("congdoan", $data, " id= $id ");
            if ($query) {
                // Cong don so du
                $dieukien = " WHERE tinh_trang=1 AND id>$id ";
                $query = $this->db->query("SELECT id,loai,so_tien,so_du FROM congdoan $dieukien ORDER BY ngay_gio ");
                if ($query) {
                    $x = $query->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($x AS $item) {
                        if ($item['loai'] == 0)
                            $sodu = $sodudau + $item['so_tien'];
                        else
                            $sodu = $sodudau - $item['so_tien'];
                        $phieuid = $item['id'];
                        $query = $this->db->query("UPDATE congdoan SET so_du=$sodu WHERE id=$phieuid ");
                        if (!$query) break;
                        $sodudau = $sodu;
                    }
                }
                //Update và cộng dồn công đoàn
            }
        }
        $nhatky = array(
            'ngay_gio' => date("Y-m-d H:i:s"),
            'user' => $_SESSION['user']['nhan_vien'],
            'doi_tuong' => 'Thu chi công đoàn',
            'action' => 'Sửa phiếu công đoàn:' . $dulieu
        );
        $query = $this->insert("nhatky", $nhatky);
        return $query;
    }

    function delObj($id)
    {
        $query = $this->db->query("SELECT * FROM congdoan WHERE id = $id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $dulieu = json_encode($temp[0]);
        $sotien = $temp[0]['so_tien'];
        $loai = $temp[0]['loai'];
        $data = ['tinh_trang' => 0];
        $query = $this->update("congdoan", $data, "id = $id ");
        if ($query) {
            // cong don so du
            if ($loai == 0)
                $sodudau = $temp[0]['so_du'] - $sotien;
            else
                $sodudau = $temp[0]['so_du'] + $sotien;
            $dieukien = " WHERE tinh_trang=1 AND id>$id ";
            $query = $this->db->query("SELECT id,loai,so_tien,so_du FROM congdoan $dieukien ORDER BY id ");
            if ($query) {
                $x = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($x AS $item) {
                    if ($item['loai'] == 0)
                        $sodu = $sodudau + $item['so_tien'];
                    else
                        $sodu = $sodudau - $item['so_tien'];
                    $phieuid = $item['id'];
                    $query = $this->db->query("UPDATE congdoan SET so_du=$sodu WHERE id=$phieuid ");
                    if (!$query) break;
                    $sodudau = $sodu;
                }
            }
            //Update và cộng dồn công nợ

        }
        $nhatky = array(
            'ngay_gio' => date("Y-m-d H:i:s"),
            'user' => $_SESSION['user']['nhan_vien'],
            'doi_tuong' => 'Thu chi',
            'action' => 'Xóa phiếu:' . $id
        );
        $query = $this->insert("nhatky", $nhatky);
        return $query;
    }

    function chotsodu($ngay)
    {
        $dieukien = " WHERE tinh_trang=1 AND ngay_gio>='$ngay' ";
        $query = $this->db->query("SELECT id,loai,so_tien,so_du FROM congdoan $dieukien ORDER BY id ");
        $sodudau = 0;
        if ($query) {
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $i = 0;
            foreach ($temp AS $item) {
                if ($i == 0) {
                    $sodudau = $item['so_du'];
                } else {
                    if ($item['loai'] == 0)
                        $sodu = $sodudau + $item['so_tien'];
                    else
                        $sodu = $sodudau - $item['so_tien'];
                    $id = $item['id'];
                    $query = $this->db->query("UPDATE congdoan SET so_du=$sodu WHERE id=$id ");
                    if (!$query)
                        break;
                    $sodudau = $sodu;
                }
                $i++;
            }
        }
        return $query;
    }

    function listNhanvien($thang, $nam)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 ";
        // $dieukien .= " AND tinh_trang<5  ";
//       if($tungay!=''){
//           $tungay= functions::convertDate($tungay);
//       }
        if ($nam != '' && $thang != '') {
            $thangnam = strtotime('last day of this month', strtotime($nam . '-' . $thang . '-01'));
            $thangnam = date('Y-m-d', $thangnam);
            $dieukien .= " AND ngay_di_lam <= '$thangnam' AND ngay_di_lam!='0000-00-00' ";
            $dieukien .= " AND (ngay_ket_thuc>= '$thangnam' OR ngay_ket_thuc='0000-00-00') ";
        }
        $sql = "SELECT id FROM nhanvien  $dieukien";
        $query = $this->db->query($sql);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getCongdoan($thang, $nam, $nv)
    {
        $dieukien = " WHERE tinh_trang=1 AND nhan_vien=$nv";
        // $dieukien .= " AND tinh_trang<5  ";
//       if($tungay!=''){
//           $tungay= functions::convertDate($tungay);
//       }
        if ($nam != '' && $thang != '') {
            $thangnam = $nam . '-' . $thang;
            $dieukien .= " AND ngay LIKE '$thangnam%' ";
        }
        $sql = "SELECT id,gio_vao,gio_ra,ca_vao,ca_ra,sang,chieu,ngay FROM chamcong $dieukien";
        $query = $this->db->query($sql);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function checkcongdoan($idnv,$ngaycapnhat,$congid)
    {
        $check = 0;
        if ($congid != '' && $ngaycapnhat != '' && $idnv != '') {
            $dieukien = " WHERE tinh_trang=1 AND nhan_vien=$idnv ";
            $dieukien .= " AND loai_id=$congid AND phan_loai=1 AND ngay_cap_nhat LIKE '$ngaycapnhat' ";
            $sql = "SELECT count(1) as total FROM congdoan $dieukien";
            $query = $this->db->query($sql);
            $row = $query->fetchAll(PDO::FETCH_ASSOC);
            $check = $row[0]['total'];
        }
        return $check;

    }

    function getCongViec($thang, $nam, $nv)
    {
        $dieukien = " WHERE nhan_vien=$nv ";
        // $dieukien .= " AND tinh_trang<5  ";
//       if($tungay!=''){
//           $tungay= functions::convertDate($tungay);
//       }
        if ($nam != '' && $thang != '') {
            $thangnam = $nam . '-' . $thang;
            $dieukien .= " AND updated LIKE '$thangnam%' AND tinh_trang IN (3,5) ";
        }
        $sql = "SELECT id,tinh_trang,updated FROM congviecsub $dieukien";
        $query = $this->db->query($sql);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function checkcongviec($idnv, $ngaycapnhat , $viecid)
    {
        $check = 0;
        if ($viecid != '' && $ngaycapnhat != '' && $idnv != '') {
            $dieukien = " WHERE tinh_trang=1 AND nhan_vien=$idnv ";
            $dieukien .= " AND loai_id=$viecid AND phan_loai=2 AND ngay_cap_nhat LIKE '$ngaycapnhat' ";
            $sql = "SELECT count(1) as total FROM congdoan $dieukien";
            $query = $this->db->query($sql);
            $row = $query->fetchAll(PDO::FETCH_ASSOC);
            $check = $row[0]['total'];
        }
        return $check;

    }

}

?>

<?php

class invoice_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $khachhang, $nhanvien)
    {
        $result = [];
        $dieukien = " WHERE tinh_trang>0 ";
        if ($tungay != '')
            $dieukien .= " AND ngay>='$tungay' ";
        if ($denngay != '')
            $dieukien .= " AND ngay<='$denngay' ";
        if ($khachhang > 0) {
            $dieukien .= " AND (SELECT khach_hang FROM donhang WHERE id=a.don_hang)=$khachhang ";
        }
        if ($nhanvien > 0) {
            $dieukien .= " AND nhan_vien=$nhanvien ";
        }
        $query = $this->db->query("SELECT COUNT(*) AS total FROM invoice a $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            (SELECT khach_hang FROM donhang WHERE id=a.don_hang) idkh,
           (SELECT name FROM khachhang WHERE id=(SELECT khach_hang FROM donhang WHERE id=a.don_hang)) AS khachhang,
           (SELECT name FROM nhanvien WHERE id=(SELECT nhan_vien FROM donhang WHERE id=a.don_hang)) AS nhanvien,
           (SELECT so_tien FROM donhang WHERE id=a.don_hang) as tongtien,
           (so_tien-du_no) as dathanhtoan,
            (SELECT so_buoi FROM khoahoc WHERE id=(SELECT product FROM donhang WHERE id=a.don_hang)) as sobuoi,
           CONCAT('INV-',id) AS soinv
           FROM invoice a $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $tong = 0;
        $sotien = 0;
        $dathanhtoan = 0;
        $duno = 0;
        foreach ($result['rows'] as $item) {
            $tong +=$item['tongtien'];
            $sotien += $item['so_tien'];
            $dathanhtoan +=$item['dathanhtoan'];
            $duno+=$item['du_no'];
        }
        $result['footer'] = [0 => ['khachhang' => 'Tổng cộng:', 'so_tien' => $sotien,
            'tongtien' => $tong,'dathanhtoan'=>$dathanhtoan,'du_no'=>$duno]];
        return $result;
    }

    function get_detail($id)
    {
        $dieukien = " WHERE invoice=$id AND tinh_trang=1 ";
        $query = $this->db->query("SELECT *,
           CONCAT((SELECT name FROM dichvu WHERE id=dich_vu),'(',ten_mien,')') AS hanghoa,
           (SELECT name FROM donvitinh WHERE id=don_vi) AS donvi,
           DATE_FORMAT(ngay_bd,'%d/%m/%Y') AS ngaybd,
           DATE_FORMAT(ngay_kt,'%d/%m/%Y') AS ngaykt,
           ROUND(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(1+thue_suat_vat/100)) AS thanhtien
           FROM invoicesub $dieukien ");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function invoice($tungay, $denngay)
    {
        $query = $this->db->query("SELECT id, CONCAT('INV-',id,': ',(SELECT name FROM khachhang WHERE id=khach_hang)) AS soinv
            FROM invoice WHERE tinh_trang=1 AND ngay>='$tungay' AND ngay<='$denngay' ");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function checktien($id, $donhang)
    {
        $query = $this->db->query("SELECT sum(so_tien) as total
            FROM invoice WHERE tinh_trang>0 AND don_hang=$donhang AND id!=$id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp)
            return $temp[0]['total'];
        else
            return 0;
    }

    function gop($invoice)
    {
        $i = 1;
        $query = false;
        foreach ($invoice as $id) {
            if ($i == 1) {
                $invoicegoc = $id;
            } else {
                $query = $this->db->query("UPDATE IGNORE invoicesub SET invoice=$invoicegoc
                    WHERE invoice = $id and tinh_trang=1 ");
                if ($query) {
                    $this->db->query("UPDATE IGNORE invoice SET tinh_trang=0 WHERE id = $id ");
                }
            }
            $i++;
        }
        return $query;
    }

    function noidung($id)
    {
        $dieukien = " WHERE invoice=$id AND tinh_trang=1 ";
        $query = $this->db->query("SELECT *,
           (SELECT name FROM donvitinh WHERE id=don_vi) AS donvi,
            CONCAT((SELECT name FROM dichvu WHERE id=dich_vu),'(',ten_mien,')') AS product,
           DATE_FORMAT(ngay_bd,'%d/%m/%Y') AS ngaybd, DATE_FORMAT(ngay_kt,'%d/%m/%Y') AS ngaykt,
           ROUND(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)) AS thanhtien,
           ROUND(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(thue_suat_vat/100)) AS thuevat
           FROM invoicesub $dieukien ORDER BY ngay_bd ");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $sum = 0;
        $vat = 0;
        foreach ($result['rows'] as $row) {
            $sum += $row['thanhtien'];
            $vat += $row['thuevat'];
        }
        $result['footer'] = [0 => ['product' => 'Tổng tiền', 'don_gia' => 0, 'chiet_khau_tm' => 0, 'thanhtien' => $sum, 'thuevat' => $vat]];
        return $result;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("invoice", $data, "id = $id");
//        if ($query) {
//            if ($data['tinh_trang'] == 2 and $tinhtrangcu == 1) {
//                $query = $this->db->query("SELECT SUM(so_luong*(don_gia-chiet_khau_tm)*(1-ROUND(chiet_khau_pt/100,0))) AS sotien
//                      FROM invoicesub WHERE invoice=$id AND tinh_trang=1 ");
//                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
//                $sotien = $temp[0]['sotien'];
//                $query = $this->db->query("SELECT * FROM phaithu WHERE khach_hang=$khachhang ORDER BY ngay_gio DESC LIMIT 1 ");
//                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
//                $duno = isset($temp[0]['du_no']) ? $sotien + $temp[0]['du_no'] : $sotien;
//                $data = ['ngay_gio' => date("Y-m-d H:i:s"), 'khach_hang' => $khachhang, 'so_tien' => $sotien, 'du_no' => $duno, 'noi_dung' => $data['noi_dung'], 'tinh_trang' => 1, 'loai_phieu' => 1, 'so_phieu' => $id];
//                $query = $this->insert("phaithu", $data);
//            }
//        }
        return $query;
    }

    function delObj($id, $khachhang)
    {
        $query = $this->db->query("UPDATE IGNORE invoice SET tinh_trang=0 WHERE id=$id");
        if ($query) {
            $query = $this->db->query("UPDATE IGNORE invoicesub SET tinh_trang=0 WHERE invoice=$id");
            $query = $this->db->query("SELECT ngay_gio,khach_hang FROM phaithu WHERE tinh_trang=1 AND loai_phieu=1 AND so_phieu=$id ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($temp) {
                $ngaygio = $temp[0]['ngay_gio'];
                $khachhang = $temp[0]['khach_hang'];
                $query = $this->db->query("SELECT du_no FROM phaithu WHERE khach_hang=$khachhang AND tinh_trang=1 AND ngay_gio<'$ngaygio'
                ORDER BY ngay_gio DESC LIMIT 1 ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                $sodu = isset($temp[0]['du_no']) ? $temp[0]['du_no'] : 0;
                $query = $this->db->query("UPDATE IGNORE phaithu SET tinh_trang=0 WHERE tinh_trang=1 AND loai_phieu=1 AND so_phieu=$id ");
                $query = $this->db->query("SELECT * FROM phaithu WHERE tinh_trang=1 AND khach_hang=$khachhang AND ngay_gio>'$ngaygio' ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($temp as $item) {
                    $sodu = $item['loai_phieu'] == 1 ? $sodu + $item['so_tien'] : $sodu - $item['so_tien'];
                    $id = $item['id'];
                    $query = $this->db->query("UPDATE IGNORE phaithu SET du_no=$sodu WHERE id=$id ");
                }
            }
        }
        return $query;
    }

    function addrow($data, $invoice)
    {
        $query = $this->insert("invoicesub", $data);
        if ($query) {
            $query = $this->db->query("SELECT
                SUM(ROUND(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(1+thue_suat_vat/100))) AS thanhtien
                FROM invoicesub WHERE invoice=$invoice AND tinh_trang=1");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $sotien = $temp[0]['thanhtien'];
            $query = $this->congdon($invoice, $sotien);
        }
        return $query;
    }

    function updaterow($id, $data, $invoice)
    {
        $query = $this->update("invoicesub", $data, "id = $id");
        if ($query) {
            $query = $this->db->query("SELECT
             SUM(ROUND(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(1+thue_suat_vat/100))) AS thanhtien
             FROM invoicesub WHERE tinh_trang=1 AND invoice=$invoice ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $sotien = isset($temp[0]['thanhtien']) ? $temp[0]['thanhtien'] : 0;
            $query = $this->congdon($invoice, $sotien);
        }
        return $query;
    }

    function congdon($invoice, $sotien)
    {
        $query = $this->db->query("SELECT id, ngay_gio, khach_hang, so_tien, du_no FROM phaithu WHERE tinh_trang=1 AND loai_phieu=1 AND so_phieu=$invoice ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($temp[0]['so_tien'])) {
            $sotiencu = $temp[0]['so_tien'];
            if ($sotien == $sotiencu) {
                return true;
            } else {
                $khachhang = $temp[0]['khach_hang'];
                $sodu = $temp[0]['du_no'] + ($sotien - $sotiencu);
                $ngaygio = $temp[0]['ngay_gio'];
                $id = $temp[0]['id'];
                $query = $this->db->query("UPDATE IGNORE phaithu SET so_tien=$sotien, du_no=$sodu WHERE id=$id ");
                $query = $this->db->query("SELECT * FROM phaithu WHERE tinh_trang=1 AND khach_hang=$khachhang AND ngay_gio>'$ngaygio' ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($temp as $item) {
                    $sodu = $item['loai_phieu'] == 1 ? $sodu + $item['so_tien'] : $sodu - $item['so_tien'];
                    $id = $item['id'];
                    $query = $this->db->query("UPDATE IGNORE phaithu SET du_no=$sodu WHERE id=$id ");
                }
                return $query;
            }
        } else {
            return true;
        }
    }

    function email($id)
    {
        //lấy danh sách email khách hàng để chọn
        $dieukien = " WHERE khach_hang=$id AND tinh_trang=1 AND email!='' ";
        $query = $this->db->query("SELECT email FROM contact $dieukien ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $query = $this->db->query("SELECT email FROM khachhang WHERE id=$id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($temp[0]['email']) && $temp[0]['email'] != '') {
            $result[count($result)]['email'] = $temp[0]['email'];
        }
        return $result;
    }

    function sendinvoice($id, $email, $note, $lan)
    {
        $query = $this->db->query("SELECT *,(SELECT ten_day_du FROM khachhang WHERE id=khach_hang) AS khachhang FROM invoice WHERE id=$id ");
        $invoice = $query->fetchAll(PDO::FETCH_ASSOC);
        $name = $invoice[0]['khachhang'];
        $file = $invoice[0]['dinh_kem'];
        $loai = $invoice[0]['loai'];
        $query = $this->db->query("SELECT *,
             (SELECT name FROM dichvu WHERE id=dich_vu) AS product,
             (SELECT name FROM donvitinh WHERE id=don_vi) AS donvi,
             ROUND(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)) AS thanhtien,
             ROUND(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(thue_suat_vat/100)) AS thuevat
             FROM invoicesub WHERE invoice=$id AND tinh_trang=1 ORDER BY ngay_bd");
        $sub = $query->fetchAll(PDO::FETCH_ASSOC);
        $deadline = $sub[0]['ngay_bd'];
        $tenmien = false;
        $cktm = false;
        $ckpt = false;
        $free = false;
        foreach ($sub as $item) {
            if ($item['chiet_khau_tm'] > 0) {
                $cktm = true;
            }
            if ($item['chiet_khau_pt'] > 0) {
                $ckpt = true;
            }
            if ($item['tang_thang'] > 0) {
                $free = true;
            }
            if ($item['ten_mien'] != '') {
                $tenmien = true;
            }
        }
        $noidung = $note;
        $query = $this->db->query("SELECT * FROM system ");
        $thongtin = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($loai == 1) {
            $noidung =
                '
              <br>
              <img src="https://thuonghieuweb.com/oldversion/img/homepage/logo-thuonghieu.png" />
              <h2 class="title">THÔNG BÁO THU TIỀN</h2>
              <h3>Kính gửi quý khách: ' . $name . ' </h3>
              <p><span style="font-weight: bold;color: #0E2D5F;">' . $thongtin[0]['value'] . '</span> trân trọng gửi tới quý khách hàng các khoản phí dịch vụ mà quý khách đăng ký/sử dụng tại công ty chúng tôi, chi tiết như sau:</p>
              <br>
              <table>
                <tr>
                  <th>STT</th>
                  <th>Dịch vụ</th>
                  <th>Từ ngày</th>
                  <th>Đến ngày</th>
                  <th>Đơn giá</th>
                  <th>Số lượng</th>
                  <th>ĐVT</th>';
            if ($cktm) {
                $noidung .= '<th>Chiết khấu TM</th>';
            }
            if ($ckpt) {
                $noidung .= '<th>Chiết khấu %</th>';
            }
            $noidung .= '<th>VAT %</th><th>Thành tiền</th>';
            if ($free) {
                $noidung .= '<th>Tặng tháng</th>';
            }
            if ($tenmien) {
                $noidung .= '<th>Tên miền</th>';
            }
            $noidung .= '</tr>';
            $stt = 1;
            $tong = 0;
            $thuevat = 0;
            foreach ($sub as $row) {
                $noidung .= '<tr><td>' .
                    $stt . '</td><td>' .
                    $row['product'] . '</td><td>' .
                    date("d/m/Y", strtotime($row['ngay_bd'])) . '</td><td>' .
                    date("d/m/Y", strtotime($row['ngay_kt'])) . '</td><td style="text-align:right">' .
                    number_format($row['don_gia']) . '</td><td style="text-align:center">' .
                    $row['so_luong'] . '</td><td style="text-align:center">' .
                    $row['don_vi'] . '</td>';
                if ($cktm) {
                    $noidung .= '<td>' . number_format($row['chiet_khau_tm']) . '</td>';
                }
                if ($ckpt) {
                    $noidung .= '<td>' . $row['chiet_khau_pt'] . '</td>';
                }
                $noidung .= '<td style="text-align:center">' . ($row['thue_suat_vat']) . '</td>';
                $noidung .= '<td style="text-align:right">' . number_format($row['thanhtien'] + $row['thuevat']) . '</td>';
                //$noidung .= '<td style="text-align:right">' . number_format($row['thuevat']) . '</td>';
                if ($free) {
                    $noidung .= '<td>' . $row['tang_thang'] . '</td>';
                }
                if ($tenmien) {
                    $noidung .= '<td>' . $row['ten_mien'] . '</td>';
                }
                $noidung .= '</tr>';
                $stt++;
                $tong += $row['thanhtien'];
                $thuevat += $row['thuevat'];
            }
            $noidung .=
                '
                <tr>
                  <th></th>
                  <th>Tổng cộng</th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>';
            if ($cktm) {
                $noidung .= '<th></th>';
            }
            if ($ckpt) {
                $noidung .= '<th></th>';
            }
            $noidung .= ' <th></th><th></th>
                  <th style="text-align:right">' . number_format($tong + $thuevat) . '</th>
                 
                </tr>
                </table>
                <br>
                Quý khách vui lòng <span style="color:red">thanh toán trong vòng 05 ngày </span>kể từ ngày nhận được email này để dịch vụ được khởi tạo đúng thời gian mong muốn của quý khách.
                <br><br>
                Số tài khoản nhận thanh toán: <span style="font-weight: bold; color: #006400;">19034821045031</span><br>
                Chủ tài khoản: <span style="font-weight: bold; color: #006400;">Nguyễn Thị Nụ</span><br>
                Mở tại: <span style="font-weight: bold; color: #006400;">Ngân hàng Techcombank</span><br>
                ';
        } else {
            $noidung =
                '
              <br>
              <img src="https://thuonghieuweb.com/oldversion/img/homepage/logo-thuonghieu.png" />
              <h2 class="title">THÔNG BÁO THU TIỀN GIA HẠN DỊCH VỤ</h2>
              <h3>Kính gửi quý khách: ' . $name . ' </h3>
              <p><span style="font-weight: bold;color: #0E2D5F;">' . $thongtin[0]['value'] . '</span> trân trọng gửi tới quý khách hàng các khoản phí gia han dịch vụ mà quý khách đang sử dụng tại công ty chúng tôi, chi tiết như sau:</p>
              <br>
              <table>
                <tr>
                  <th>STT</th>
                  <th>Dịch vụ</th>
                  <th>Từ ngày</th>
                  <th>Đến ngày</th>
                  <th>Đơn giá</th>
                  <th>Số lượng</th>
                  <th>ĐVT</th>';
            if ($cktm) {
                $noidung .= '<th>Chiết khấu TM</th>';
            }
            if ($ckpt) {
                $noidung .= '<th>Chiết khấu %</th>';
            }
            $noidung .= '<th>VAT %</th><th>Thành tiền</th>';
            if ($free) {
                $noidung .= '<th>Tặng tháng</th>';
            }
            if ($tenmien) {
                $noidung .= '<th>Tên miền</th>';
            }
            $noidung .= '</tr>';
            $stt = 1;
            $tong = 0;
            $thuevat = 0;
            foreach ($sub as $row) {
                $noidung .= '<tr><td>' .
                    $stt . '</td><td>' .
                    $row['product'] . '</td><td>' .
                    date("d/m/Y", strtotime($row['ngay_bd'])) . '</td><td>' .
                    date("d/m/Y", strtotime($row['ngay_kt'])) . '</td><td style="text-align:right">' .
                    number_format($row['don_gia']) . '</td><td style="text-align:center">' .
                    $row['so_luong'] . '</td><td style="text-align:center">' .
                    $row['don_vi'] . '</td>';
                if ($cktm) {
                    $noidung .= '<td>' . number_format($row['chiet_khau_tm']) . '</td>';
                }
                if ($ckpt) {
                    $noidung .= '<td>' . $row['chiet_khau_pt'] . '</td>';
                }
                $noidung .= '<td style="text-align:center">' . ($row['thue_suat_vat']) . '</td>';
                $noidung .= '<td style="text-align:right">' . number_format($row['thanhtien'] + $row['thuevat']) . '</td>';
                //$noidung .= '<td style="text-align:right">' . number_format($row['thuevat']) . '</td>';
                if ($free) {
                    $noidung .= '<td>' . $row['tang_thang'] . '</td>';
                }
                if ($tenmien) {
                    $noidung .= '<td>' . $row['ten_mien'] . '</td>';
                }
                $noidung .= '</tr>';
                $stt++;
                $tong += $row['thanhtien'];
                $thuevat += $row['thuevat'];
            }
            $noidung .=
                '
                <tr>
                  <th></th>
                  <th>Tổng cộng</th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>';
            if ($cktm) {
                $noidung .= '<th></th>';
            }
            if ($ckpt) {
                $noidung .= '<th></th>';
            }
            if ($tenmien) {
                $noidung .= '<th></th>';
            }
            $noidung .= ' <th></th>
                  <th style="text-align:right">' . number_format($tong + $thuevat) . '</th>
                 
                </tr>
                </table>
                <br>
                Quý khách vui lòng <span style="color:red">thanh toán trong vòng 05 ngày </span>kể từ ngày nhận được email này để dịch vụ được gia hạn đúng thời gian mong muốn của quý khách.
                <br><br>
                Số tài khoản nhận thanh toán: <span style="font-weight: bold; color: #006400;">19034821045031</span><br>
                Chủ tài khoản: <span style="font-weight: bold; color: #006400;">Nguyễn Thị Nụ</span><br>
                Mở tại: <span style="font-weight: bold; color: #006400;">Ngân hàng Techcombank</span><br>
                ';
        }
        if ($file != '') {
            $noidung .= 'Tải file chi tiết đính kèm <a href="' . $file . '" target="_blanl">tại đây</a>';
        }
        $noidung .= '
                <br><br>
                Trân trọng! <br>
                --------------------------<br>
                Phòng kinh doanh - GEMS Tech <br>
                Hotline: ' . $thongtin[4]['value'] . ' <br>
                Email: sale@gemstech.com.vn
                </body>
                </html>
          ';
        $from = 'Phòng kinh doanh GEMS TECH - Thương Hiệu Web';
        $subject = 'Thông báo thu tiền dịch vụ';
//        $cc1 = 'info@gemstech.com.vn';
//        $cc2 = 'ketoan@gemstech.com.vn';
//        $cc3 = 'sale@gemstech.com.vn';
        $cc = array('info@gemstech.com.vn' => '', 'sale@gemstech.com.vn' => '', 'ketoan@gemstech.com.vn' => '');
        //$cc = array('info@gemstech.com.vn'=>'x1');
        $date = date('Y-m-d H:i:s');
        $this->db->query("UPDATE IGNORE invoice SET $lan='$date' WHERE id=$id");
        $query = $this->sendmail($from, $email, $cc, $subject, $noidung);
        $query = 1;
        return $query;
    }

    function addObj($invoice, $data)
    {
        $query = $this->insert("invoice", $invoice);
        $id = 0;
        if ($query) {
            $id = $this->db->lastInsertId();
            $hanghoa = json_decode($data, true);
            foreach ($hanghoa as $item) {
                $tungay = functions::convertDate($item['tungay']);
                $denngay = functions::convertDate($item['denngay']);
                $sub = [
                    'invoice' => $id,
                    'dich_vu' => $item['hanghoaid'],
                    'don_gia' => $item['dongia'],
                    'don_vi' => $item['donviid'],
                    'so_luong' => $item['soluong'],
                    'chiet_khau_tm' => $item['chietkhau'],
                    'chiet_khau_pt' => $item['chietkhaupt'],
                    'tang_thang' => $item['tangthang'],
                    'ngay_bd' => $tungay,
                    'ngay_kt' => $denngay,
                    'ghi_chu' => $item['ghichu'],
                    'thue_suat_vat' => $item['thuevat'],
                    'tinh_trang' => 1,
                ];
                $query = $this->insert("invoicesub", $sub);
            }
        }
        return $id;
    }

    function nhanvien()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name, dien_thoai FROM nhanvien 
        WHERE tinh_trang IN (1,2,3,4) AND phong_ban IN (3,4)");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        // array_push($temp,array('id'=>0,'name'=>'Tất cả','dien_thoai'=>''));
        return $temp;
    }

    function donhang($khachhang)
    {
        $query = $this->db->query("SELECT id,hoc_vien,product,khach_hang,so_tien,so_buoi,
        (SELECT name FROM hocvien WHERE id=hoc_vien) as hocvien
        FROM donhang WHERE khach_hang = $khachhang AND tinh_trang>0");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

}

?>

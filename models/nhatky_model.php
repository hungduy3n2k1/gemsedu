<?php
class nhatky_model extends model
{
   function __construct()
   {
       parent::__construct();

   }

   function getFetObj($sort, $order, $offset, $rows, $tukhoa,$nhanvien)
   {
       $result   = array();
       $dieukien = " WHERE 1 ";
       if ($tukhoa != '')
           $dieukien .= " AND (action LIKE '%$tukhoa%' OR doi_tuong LIKE '%$tukhoa%')  ";
       if ($nhanvien>0)
           $dieukien .= " AND user=(SELECT id FROM users WHERE nhan_vien=$nhanvien LIMIT 1 ) ";
       $query           = $this->db->query("SELECT COUNT(*) AS total FROM nhatky $dieukien ");
       $row             = $query->fetchAll(PDO::FETCH_ASSOC);
       $result['total'] = $row[0]['total'];
       $query           = $this->db->query("SELECT *, DATE_FORMAT(ngay_gio,'%d/%m/%Y %H:%i:%s') as ngaygio,
          (SELECT name FROM nhanvien WHERE id=(SELECT nhan_vien FROM users WHERE id=user )) AS nhanvien
          FROM nhatky  $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
       $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
       return $result;
   }
}
?>

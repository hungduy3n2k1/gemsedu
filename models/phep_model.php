<?php
class phep_model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($thang, $nam)
    {
        $result   = array();
        $dieukien = "WHERE tinh_trang=1 AND nam='$nam' AND thang='$thang' ";
        $query           = $this->db->query("SELECT *,
            (SELECT COUNT(1) FROM chamcong $dieukien AND chamcong.nhan_vien=phep.nhan_vien AND sang=2 AND ngay LIKE '$nam%') AS sang,
            (SELECT COUNT(1) FROM chamcong $dieukien AND chamcong.nhan_vien=phep.nhan_vien AND chieu=2 AND ngay LIKE '$nam%') AS chieu,
            (SELECT COUNT(1) FROM chamcong $dieukien AND chamcong.nhan_vien=phep.nhan_vien AND sang=6 AND ngay LIKE '$nam%') AS sangbu,
            (SELECT COUNT(1) FROM chamcong $dieukien AND chamcong.nhan_vien=phep.nhan_vien AND chieu=6 AND ngay LIKE '$nam%') AS chieubu,
            (SELECT name FROM nhanvien WHERE id=nhan_vien) AS name
            FROM phep $dieukien");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
//        if(empty($result)) {
//            $query = $this->db->query("SELECT id FROM nhanvien WHERE tinh_trang IN (1,2,3) ");
//            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
//            foreach ($temp AS $item) {
//                $data = ['thang'=>$thang, 'nam'=>$nam, 'nhan_vien'=>$item['id'], 'tinh_trang'=>1];
//                $ok = $this->insert("phep", $data);
//            }
//            $query           = $this->db->query("SELECT *,
//                (SELECT COUNT(1) FROM chamcong $dieukien AND chamcong.nhan_vien=phep.nhan_vien AND sang=2 AND ngay LIKE '$nam%') AS sang,
//                (SELECT COUNT(1) FROM chamcong $dieukien AND chamcong.nhan_vien=phep.nhan_vien AND chieu=2 AND ngay LIKE '$nam%') AS chieu,
//                (SELECT COUNT(1) FROM chamcong $dieukien AND chamcong.nhan_vien=phep.nhan_vien AND sang=6 AND ngay LIKE '$nam%') AS sangbu,
//                (SELECT COUNT(1) FROM chamcong $dieukien AND chamcong.nhan_vien=phep.nhan_vien AND chieu=6 AND ngay LIKE '$nam%') AS chieubu,
//                (SELECT name FROM nhanvien WHERE id=nhan_vien) AS name
//                FROM phep $dieukien");
//            $result  = $query->fetchAll(PDO::FETCH_ASSOC);
//        }
        foreach ($result AS $key=>$value) {
            $phep = ($value['sang']+$value['chieu'])/2;
            $bu = ($value['sangbu']+$value['chieubu'])/2;
            $conlai = $value['phep_luy_ke']-$phep;
            $conlaibu = $value['phep_nam']-$bu;
            $result[$key]['phep']=$phep;
            $result[$key]['conlai']=$conlai;
            $result[$key]['bu']=$bu;
            $result[$key]['conlaibu']=$conlaibu;
        }
        return $result;
    }

    function updateObj($id, $data)
    {
        $query=$this->update("phep", $data, "id = $id");
        return $query;
    }
}
?>

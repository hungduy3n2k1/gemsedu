<?php
class bangluong_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($thang,$nam,$funs)
    {
        $result   = array();
        //update bang luong
        $dieukien = " WHERE thang='$thang' AND nam='$nam' ";
        $query = $this->db->query("SELECT nhan_vien FROM bangluong $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp AS $item) {
            $nhanvien = $item['nhan_vien'];
            $cong = $this->demcong($thang,$nam,$nhanvien);
            $anca = $this->anca($thang,$nam,$nhanvien);
            $data = array('cham_cong'=>$cong, 'ngay_an_ca'=>$anca);
            $this->update("bangluong",$data,"nhan_vien=$nhanvien");
        }
        if(!isset($funs['152']))
            $dieukien.= " AND nhan_vien = ".$_SESSION['user']['nhan_vien'];
        $query = $this->db->query("SELECT *, ROUND(luong*cham_cong/ngay_cong_chuan) AS luongtg, ngay_an_ca*an_ca AS anca,
            (ROUND(luong*cham_cong/ngay_cong_chuan)+ngay_an_ca*an_ca+gui_xe+thuong_ds+thuong_lt+thuong_khac) AS tong,
            (ROUND(luong*cham_cong/ngay_cong_chuan)+ngay_an_ca*an_ca+gui_xe+thuong_ds+thuong_lt+thuong_khac)-(phat+bao_hiem+tam_ung) AS thuclinh,
            (SELECT name FROM nhanvien WHERE id=nhan_vien) AS nhanvien
            FROM bangluong $dieukien ");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongluong = 0;
        foreach ($result['rows'] AS $row)
            $tongluong = $tongluong+$row['thuclinh'];
            $result['footer'] = array(0=>array('nhanvien'=>'Tổng cộng','luong'=>0,'cham_cong'=>0, 'luongtg'=>0,'anca'=>0,
            'gui_xe'=>0,'thuong_ds'=>0,'thuong_lt'=>0,'thuong_khac'=>0,'tong'=>0,'phat'=>0,'bao_hiem'=>0,'tam_ung'=>0,'thuclinh'=>$tongluong));
        return $result;
    }

    function lapbangluong($thang,$nam)
    {
        $ok   = false;
        $dieukien = " WHERE tinh_trang IN (1,2,3) AND van_phong>0 AND ca>0 ";
        $query = $this->db->query("SELECT id,luong,an_ca,gui_xe,bao_hiem FROM nhanvien $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp AS $nhanvien) {
            $id = $nhanvien['id'];
            $dieukien = " WHERE thang = '$thang' AND nam = '$nam' AND nhan_vien=$id ";
            $query = $this->db->query("SELECT COUNT(1) AS total FROM bangluong $dieukien ");
            $row = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($row[0]['total']==0) {
                $congchuan = $this->workingday($thang,$nam,$id);
                $data = array('nhan_vien'=>$id,'thang'=>$thang,'nam'=>$nam, 'luong'=>$nhanvien['luong'],
                  'ngay_cong_chuan'=>$congchuan, 'an_ca'=>$nhanvien['an_ca'],
                  'gui_xe'=>$nhanvien['gui_xe'], 'bao_hiem'=>$nhanvien['bao_hiem'], 'tinh_trang'=>0);
                $ok = $this->insert("bangluong", $data);
            }
        }
        return $ok;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("bangluong", $data, "id=$id");
        return $query;
    }

    function duyet($thang,$nam)
    {
        $query  = $this->db->query("UPDATE bangluong SET tinh_trang=0 WHERE nam='$nam' AND thang='$thang' ");
        return $query;
    }

    function checkduyet($thang,$nam)
    {
        $query  = $this->db->query("SELECT tinh_trang FROM bangluong WHERE nam='$nam' AND thang='$thang' LIMIT 1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($temp[0]['tinh_trang']))
            return $temp[0]['tinh_trang'];
        else
            return 1;
    }

    function demcong($thang,$nam,$nhanvien)
    {
        $thangnam = $nam.'-'.$thang;
        $dieukien = " WHERE tinh_trang=1 AND nhan_vien=$nhanvien AND ngay LIKE '$thangnam%' AND sang NOT IN (0,8,9,10) ";
        $query  = $this->db->query("SELECT COUNT(1) AS total FROM chamcong $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $cong = $temp[0]['total'];
        $dieukien = " WHERE tinh_trang=1 AND nhan_vien=$nhanvien AND ngay LIKE '$thangnam%' AND chieu NOT IN (0,8,9,10) ";
        $query  = $this->db->query("SELECT COUNT(1) AS total FROM chamcong $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $cong = $cong + $temp[0]['total'];
        $cong = $cong/2;
        return $cong;
    }

    function anca($thang,$nam,$nhanvien)
    {
        $thangnam = $nam.'-'.$thang;
        $dieukien = " WHERE tinh_trang=1 AND nhan_vien=$nhanvien AND ngay LIKE '$thangnam%' AND sang NOT IN (0,2,4,5,6,7,8,9,10) AND chieu NOT IN (0,2,4,5,6,7,8,9,10) ";
        $query  = $this->db->query("SELECT COUNT(1) AS total FROM chamcong $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $anca = $temp[0]['total'];
        return $anca;
    }

    // $query = false;
    // $dieukien = " WHERE thang = '$thang' AND nam = '$nam' ";
    // $query = $this->db->query("SELECT COUNT(1) AS total FROM bangluong $dieukien ");
    // $row = $query->fetchAll(PDO::FETCH_ASSOC);
    // if ($row[0]['total']==0) {
    // $query = $this->db->query("SELECT nhan_vien, cong_chuan, ngay_cong,
    //    (SELECT luong FROM nhanvien WHERE id=nhan_vien) AS luong,
    //    (SELECT an_ca FROM nhanvien WHERE id=nhan_vien) AS anca,
    //    (SELECT gui_xe FROM nhanvien WHERE id=nhan_vien) AS guixe,
    //    (SELECT bao_hiem FROM nhanvien WHERE id=nhan_vien) AS baohiem,
    //    (IF(ngay_01=1,1,0)+IF(ngay_02=1,1,0)+IF(ngay_03=1,1,0)+IF(ngay_04=1,1,0)+IF(ngay_05=1,1,0)+IF(ngay_06=1,1,0)+
    //    IF(ngay_07=1,1,0)+ IF(ngay_08=1,1,0)+IF(ngay_09=1,1,0)+IF(ngay_10=1,1,0)+IF(ngay_11=1,1,0)+IF(ngay_12=1,1,0)+
    //    IF(ngay_13=1,1,0)+ IF(ngay_14=1,1,0)+IF(ngay_15=1,1,0)+IF(ngay_16=1,1,0)+IF(ngay_17=1,1,0)+IF(ngay_18=1,1,0)+
    //    IF(ngay_19=1,1,0)+ IF(ngay_20=1,1,0)+IF(ngay_21=1,1,0)+IF(ngay_22=1,1,0)+IF(ngay_23=1,1,0)+IF(ngay_24=1,1,0)+
    //    IF(ngay_25=1,1,0)+ IF(ngay_26=1,1,0)+IF(ngay_27=1,1,0)+IF(ngay_28=1,1,0)+IF(ngay_29=1,1,0)+IF(ngay_30=1,1,0)+
    //    IF(ngay_31=1,1,0)) AS ngayanca
    //    FROM bangchamcong WHERE thang='$thang' AND nam='$nam' ");
    // $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    // foreach ($temp as $row) {
    //     $nhanvien = $row['nhan_vien'];
    //     $luong = $row['luong'];
    //     $luongtg = $luong/$row['cong_chuan']*$row['ngay_cong'];
    //     $anca = $row['anca']*$row['ngayanca'];
    //     $guixe = $row['guixe'];
    //     $baohiem = $row['baohiem'];
    //     $query  = $this->db->query("SELECT id FROM bangluong WHERE nhan_vien=$nhanvien AND nam='$nam' AND thang='$thang' LIMIT 1 ");
    //     $x = $query->fetchAll(PDO::FETCH_ASSOC);
    //     if (isset($x[0]['id'])) {
    //         $id = $x[0]['id'];
    //         $data = array('luong'=>$luong, 'luong_tg'=>$luongtg, 'an_ca'=>$anca, 'gui_xe'=>$guixe, 'bao_hiem'=>$baohiem);
    //         $query = $this->update("bangluong", $data, "id=$id");
    //     } else {
    //         $data = array('nhan_vien'=>$nhanvien, 'nam'=>$nam, 'thang'=>$thang, 'luong'=>$luong,
    //           'luong_tg'=>$luongtg, 'an_ca'=>$anca, 'gui_xe'=>$guixe, 'bao_hiem'=>$baohiem, 'tinh_trang'=>1);
    //         $query = $this->insert("bangluong", $data);
    //     }
    //     $query = true;
    // }
    // return $query;

}
?>

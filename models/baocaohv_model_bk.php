<?php

class baocaohv_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $hocvien, $khachhang, $giaovien, $chuyenmon)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc) IN (1,2) ";
        if ($hocvien != '')
            $dieukien .= " AND hoc_vien IN (SELECT id FROM hocvien WHERE name LIKE '%$hocvien%') ";
        if ($khachhang != '')
            $dieukien .= " AND hoc_vien IN (SELECT id FROM hocvien WHERE khach_hang IN (SELECT id FROM khachhang WHERE name LIKE '%$khachhang%')) ";
        if ($giaovien != '')
            $dieukien .= " AND (SELECT giao_vien FROM lichhoc WHERE id=lich_hoc) = $giaovien ";
        if ($chuyenmon != '')
            $dieukien .= " AND (SELECT phu_trach FROM lophoc WHERE id=lop_hoc ) = $chuyenmon ";
        // if ($tungay != '')
        //     $dieukien .= " AND (SELECT ngay FROM lichhoc WHERE id=lich_hoc)>='$tungay' ";
        // if ($denngay != '')
        //     $dieukien .= " AND (SELECT ngay FROM lichhoc WHERE id=lich_hoc)<='$denngay' ";
        $listhv = '';
        $query = $this->db->query("SELECT hoc_vien,
        IFNULL((SELECT SUM(so_tien) FROM donhang WHERE tinh_trang IN (1,2) 
        AND khach_hang = (SELECT khach_hang FROM hocvien WHERE id=saplop.hoc_vien)),0) as sotien
        FROM saplop $dieukien GROUP BY lop_hoc HAVING sotien>0 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $total = 0;
        foreach ($temp as $item) {
            $listhv .= $item['hoc_vien'] . ",";
            $total++;
        }
        $listhv = rtrim($listhv, ",");
        if ($listhv != '') {
            $result['total'] = $total;
            $sql = "SELECT *,
            IF(ngay_sinh!='0000-00-00',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
            (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
            CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id) as mahocvien,
            /*(SELECT DATE_FORMAT(ngay_bat_dau,'%d/%m/%Y') FROM saplop 
            WHERE hoc_vien=a.id AND tinh_trang=1 ORDER BY ngay_bat_dau ASC LIMIT 1) as ngaybatdau,*/
            (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh,
            (SELECT email FROM khachhang WHERE id=a.khach_hang) as emailkh,
            IFNULL((SELECT SUM(so_tien) FROM donhang WHERE tinh_trang IN (1,2) 
            AND khach_hang=a.khach_hang),0) as sotien
           FROM hocvien a WHERE id IN ($listhv) HAVING sotien>0 ORDER BY $sort $order LIMIT $offset, $rows";
            $query = $this->db->query($sql);
            $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result['rows'] as $key => $item) {
                $tongbuoi = 0;
                $dahoc = 0;
                $conlai = 0;
                $listlh = '';
                $idhocvien = $item['id'];
                $query1 = $this->db->query("SELECT id,tinh_trang,lop_hoc,
                (SELECT (so_buoi+buoi_khuyen_mai) FROM lophoc WHERE id=a.lop_hoc) as tongbuoi 
                FROM saplop a WHERE tinh_trang>0 AND hoc_vien=$idhocvien GROUP BY lop_hoc ");
                $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                $tongbuoi=$temp[0]['tongbuoi'];
                foreach ($temp as $saplop) {
                    $listlh .= $saplop["lop_hoc"] . ",";
                    if ($saplop['tinh_trang'] > 1)
                        $dahoc++;
                }
                $listlh = rtrim($listlh, ",");
                $result['rows'][$key]['tongbuoi'] = $tongbuoi;
                $result['rows'][$key]['dahoc'] = $dahoc;
                $result['rows'][$key]['conlai'] = $tongbuoi-$dahoc;
                $query1 = $this->db->query("SELECT MAX(ngay) as ngayketthuc, MIN(ngay) AS ngaybatdau,giao_vien,lop_hoc
           FROM lichhoc a WHERE tinh_trang>0 AND lop_hoc IN ($listlh) GROUP BY lop_hoc");
                $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                if ($temp) {
                    $result['rows'][$key]['ngaybatdau'] = date("d/m/Y", strtotime($temp[0]['ngaybatdau']));
                    $result['rows'][$key]['ngayketthuc'] = date("d/m/Y", strtotime($temp[0]['ngayketthuc']));
                } else {
                    $result['rows'][$key]['ngaybatdau'] = '';
                    $result['rows'][$key]['ngayketthuc'] = '';
                }
                $giaovien = '';
                $chuyenmon = '';
                foreach ($temp as $lichhoc) {
                    $idlop = $lichhoc['lop_hoc'];
                    $query1 = $this->db->query("SELECT (SELECT name FROM giaovien WHERE id=giao_vien) as giaovien,
                (SELECT name FROM nhanvien WHERE id=phu_trach) as chuyenmon
                 FROM lophoc WHERE tinh_trang>0 AND id=$idlop");
                    $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                    if (isset($temp[0]['giaovien']) && $temp[0]['giaovien'] != '')
                        $giaovien .= $temp[0]['giaovien'] . "<br>";
                    if (isset($temp[0]['chuyenmon']) && $temp[0]['chuyenmon'] != '')
                        $chuyenmon .= $temp[0]['chuyenmon'] . "<br>";
                }
                $result['rows'][$key]['giaovien'] = $giaovien;
                $result['rows'][$key]['chuyenmon'] = $chuyenmon;
            }
        }
        return $result;
    }

}

?>

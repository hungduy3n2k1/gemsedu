<?php

class index_model extends model
{
    function __construction()
    {
        parent::__construction();
    }

    function updatePhep()
    {
        $result = array();
        $thang = date('m');
        $nam = date('Y');
        $thangtruoc = date('m', strtotime("01-$thang-$nam -1 month"));
        $dieukien = "WHERE tinh_trang=1 AND nam='$nam' AND thang='$thang' ";
        $query = $this->db->query("SELECT count(1) as total FROM phep $dieukien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp[0]['total'] == 0) {
            if ($thang == '01') {
                $namcu = $nam - 1;
                $dieukien1 = "WHERE tinh_trang=1 AND nam='$namcu' AND thang='12' ";
                $query = $this->db->query("SELECT id FROM nhanvien WHERE tinh_trang IN (1,2,3) ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($temp AS $item) {
                    $nhanvien = $item['id'];
                    $query = $this->db->query("SELECT phep_luy_ke,
                    (SELECT COUNT(1) FROM chamcong WHERE tinh_trang=1 AND nhan_vien=$nhanvien AND sang=2 AND ngay LIKE '$namcu%') AS sang,
                    (SELECT COUNT(1) FROM chamcong WHERE tinh_trang=1 AND nhan_vien=$nhanvien AND chieu=2 AND ngay LIKE '$namcu%') AS chieu
                    FROM phep $dieukien1 AND nhan_vien=$nhanvien");
                    $item = $query->fetchAll(PDO::FETCH_ASSOC);
                    $phepluyke = 0.5;
                    if (isset($item[0]['sang']) && isset($item[0]['chieu'])) {
                        $phep = ($item[0]['sang'] + $item[0]['chieu']) / 2;
                        $phepbu = $item[0]['phep_luy_ke'] - $phep;
                    } else {
                        $phepbu = 0;
                    }
                    $data = ['thang' => $thang, 'nam' => $nam, 'nhan_vien' => $nhanvien, 'tinh_trang' => 1, 'phep_luy_ke' => $phepluyke, 'phep_nam' => $phepbu];
                    $this->insert("phep", $data);
                }
            } else {
                $dieukien1 = "WHERE tinh_trang=1 AND nam='$nam' AND thang='$thangtruoc' ";
                $query = $this->db->query("SELECT id FROM nhanvien WHERE tinh_trang IN (1,2,3) ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($temp AS $item) {
                    $nhanvien = $item['id'];
                    $query = $this->db->query("SELECT phep_luy_ke,phep_nam FROM phep $dieukien1 AND nhan_vien=$nhanvien");
                    $item = $query->fetchAll(PDO::FETCH_ASSOC);
                    if (isset($item)) {
                        $phepluyke = $item[0]['phep_luy_ke'] + 0.50;
                        $phepnam = $item[0]['phep_nam'];
                    } else {
                        $phepluyke = 0.5;
                        $phepnam = $item[0]['phep_nam'];
                    }
                    $data = ['thang' => $thang, 'nam' => $nam, 'nhan_vien' => $nhanvien, 'tinh_trang' => 1, 'phep_luy_ke' => $phepluyke, 'phep_nam' => $phepnam];
                    $this->insert("phep", $data);
                }
            }
        }
        return $result;
    }

    function checkpass($id, $password)
    {
        $query = $this->db->query("SELECT count(*) as total FROM users WHERE id=$id AND mat_khau='$password'");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp[0]['total'] == 0 ? false : true;
    }

    function changepass($id, $password)
    {
        $query = $this->db->query("UPDATE users SET mat_khau='$password' WHERE id=$id ");
        return $query;
    }

    function deadline()
    {
        //update tình trạng trễ Deadline
        $i = 0;
        $today = date("Y-m-d");
        $dieukien = " WHERE tinh_trang IN (1,2) AND (ngay_kt='' OR ngay_kt='0000-00-00') AND deadline<'$today'";
        $query = $this->db->query("SELECT * FROM congviec $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp AS $item) {
            $data = ['nguoi_giao' => $item['nguoi_giao'], 'name' => $item['name'], 'congviec_id' => $item['id'], 'tinh_trang' => 3, 'nhan_vien' => $item['nhan_vien'], 'updated' => $today];
            if ($this->insert("congviecsub", $data))
                $i++;
        }
        $this->db->query("UPDATE IGNORE congviec SET tinh_trang=3,updated='$today' $dieukien  ");
        //echo $i;
    }

    function congviec()
    {
        $result = array();
        // $today = date("Y-m-d");
        // $this->db->query("UPDATE congviec SET tinh_trang=3 WHERE deadline<'$today' AND ngay_kt='0000-00-00' ");
        $nhanvien = $_SESSION['user']['nhan_vien'];
        $dieukien = " WHERE tinh_trang IN (1,2,3) AND nhan_vien=$nhanvien ";
        $query = $this->db->query("SELECT  id, CONCAT(name,' (<span style=\"color:blue\">',DATE_FORMAT(deadline,'%d/%m/%Y'),'</span>)') AS `text`
            FROM congviec $dieukien ORDER BY deadline  ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function thongbao()
    {
        $result = array();
        $date = date("Y-m-d H:i:s");
        $dieukien = " WHERE tinh_trang=1 AND ngay_gio<'$date' AND ket_thuc>'$date' ";
        $query = $this->db->query("SELECT
            id, CONCAT('<a onclick=\"opentb(',id,')\">',name,' (<span style=\"color:blue\">',DATE_FORMAT(ngay_gio,'%d/%m/%Y %H:%i:%s'),'</span>)</a>') AS `text`
         FROM thongbao $dieukien ORDER BY id DESC  ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getthongbao($id)
    {
        $result = array();
        $dieukien = " WHERE id=$id ";
        $query = $this->db->query("SELECT *,
            DATE_FORMAT(ngay_gio,'%d/%m/%Y %H:%i:%s') AS ngaygio
         FROM thongbao $dieukien ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result[0];
    }


    function getInvoice()
    {
        $result = [];
        $dieukien = " WHERE tinh_trang IN (1,2) AND du_no>0 ";
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT id,don_hang,so_tien,du_no,dot_thanh_toan,tinh_trang, DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            (SELECT khach_hang FROM donhang WHERE id=a.don_hang) idkh,
           (SELECT name FROM khachhang WHERE id=(SELECT khach_hang FROM donhang WHERE id=a.don_hang)) AS khachhang,
           (SELECT name FROM nhanvien WHERE id=(SELECT nhan_vien FROM donhang WHERE id=a.don_hang)) AS nhanvien,
           (SELECT so_tien FROM donhang WHERE id=a.don_hang) as tongtien,
           (so_tien-du_no) as dathanhtoan,
            (SELECT so_buoi FROM khoahoc WHERE id=(SELECT product FROM donhang WHERE id=a.don_hang)) as sobuoi,
           CONCAT('INV-',id) AS soinv,
           DATEDIFF(ngay,'$today') as ngaytt
           FROM invoice a $dieukien  HAVING ngaytt<=3 ORDER BY ngay ASC");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
//        $tong = 0;
//        $sotien = 0;
//        $dathanhtoan = 0;
//        $duno = 0;
//        foreach ($result['rows'] as $item) {
//            $tong +=$item['tongtien'];
//            $sotien += $item['so_tien'];
//            $dathanhtoan +=$item['dathanhtoan'];
//            $duno+=$item['du_no'];
//        }
//        $result['footer'] = [0 => ['khachhang' => 'Tổng cộng:', 'so_tien' => $sotien,
//            'tongtien' => $tong,'dathanhtoan'=>$dathanhtoan,'du_no'=>$duno]];
        return $result;
    }


    function eventstop($id)
    {
        $query = $this->db->query("UPDATE events SET tinh_trang=1 WHERE id=$id ");
        return true;
    }

    function doanhthu($thang, $nam)
    {
        $dieukien = " WHERE tinh_trang=1 AND thang=$thang AND nam=$nam ";
        $thangnam = $nam . '-' . $thang;
        $query = $this->db->query("SELECT *,
        IF((SELECT sum(so_tien) FROM socai b WHERE tinh_trang>0 AND loai=0 AND hach_toan=1 AND ngay_gio LIKE '$thangnam%'
        AND nhan_vien_sale = a.nhan_vien),
        (SELECT sum(so_tien) FROM socai b WHERE tinh_trang>0 AND loai=0 AND hach_toan=1 AND ngay_gio LIKE '$thangnam%'
        AND nhan_vien_sale = a.nhan_vien),0) as thucte,
        (SELECT name FROM nhanvien WHERE id=a.nhan_vien) as nhanvien
        FROM muctieu a $dieukien");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function ketthuclop()
    {
        $query = $this->db->query("SELECT lop_hoc,
        (SELECT so_buoi FROM lophoc WHERE id=lop_hoc) as sobuoi,
        (SELECT buoi_khuyen_mai FROM lophoc WHERE id=lop_hoc) as buoikm
        FROM lichhoc a WHERE tinh_trang>0 AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc) IN (1,2) GROUP BY lop_hoc");
        $lophoc = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($lophoc as $item) {
            $tongbuoi = $item['sobuoi'] + $item['buoikm'];
            $idlop = $item['lop_hoc'];
            $query = $this->db->query("SELECT id FROM lichhoc WHERE tinh_trang IN (2,3,4,5,6) AND lop_hoc=$idlop");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($temp)
                $dahoc = count($temp);
            else
                $dahoc = 0;
            echo $idlop . "-" . ($tongbuoi - $dahoc) . '<br>';
            if ($dahoc >= $tongbuoi)
                $this->update("lophoc", ["tinh_trang" => 3], "id=$idlop");
        }
    }

    function getHVKT()
    {
        $homnay = date("Y-m-d");
        $dieukien = " WHERE tinh_trang > 0 ";
        $query = $this->db->query("SELECT id, DATEDIFF(MAX(ngay),'$homnay') as datedif
                            FROM lichhoc $dieukien GROUP BY lop_hoc HAVING (datedif >= 0 AND datedif <= 10) ORDER by MAX(ngay)");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function unsetToken($token)
    {
        $this->db->query("UPDATE IGNORE users SET token='' WHERE token='$token'");
    }

    function login($username, $password)
    {
        $query = $this->db->query("SELECT id,menu,nhan_vien,giao_vien,nhom,
                    (SELECT ip FROM chinhanh WHERE chinhanh.id=(SELECT van_phong FROM nhanvien WHERE nhanvien.id=users.nhan_vien)) AS ip,
                    (SELECT name FROM nhanvien WHERE id=nhan_vien) as nhanvien,
                    (SELECT name FROM giaovien WHERE id=giao_vien) as giaovien
                    FROM users WHERE tinh_trang=1 AND email = '$username' AND mat_khau = '$password'");
        if ($query)
            $userdata = $query->fetchAll(PDO::FETCH_ASSOC);
        else
            $userdata = array();
        if (empty($userdata)) {
            return false;
        } else {
            $data = [];
            $userid = $userdata[0]['id'];
            $token = md5(time() . $userid);
            $this->db->query("UPDATE IGNORE users SET token='$token' WHERE id=$userid");
            $userdata[0]['token']=$token;
            $data = $userdata[0];
            // $model = new model();
            // $model->chamcong();
            return $data;
        }
    }

    function getlichdaythay()
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang > 0 ";
        $query           = $this->db->query("SELECT ngay_gio as ngay,
        (SELECT name FROM giaovien WHERE id = a.giao_vien) AS giaovien,
        (SELECT name FROM giaovien WHERE id = a.gv_daythay) AS gvdaythay,
        (SELECT name FROM lophoc WHERE id = (SELECT lop_hoc FROM lichhoc WHERE id = a.lich_hoc)) AS lophoc
        FROM daythay a $dieukien ");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // function khachhang($table1, $table2)
    // {
    //     $query = $this->db->query("SELECT * FROM $table1 ");
    //     $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //     foreach ($temp as $item) {
    //         $data = [
    //             'name' => $item['name'],
    //             'ten_day_du' => $item['name'],
    //             'ma_so' => $item['ma_so'],
    //             'dia_chi' => $item['dia_chi'],
    //             'website' => $item['website'],
    //             'lien_he' => $item['lien_he'],
    //             'ghi_chu' => $item['ghi_chu'],
    //             'loai' => 1,
    //             'tinh_trang' => 2,
    //         ];
    //         $query = $this->insert($table2, $data);
    //     }
    //     return $query;
    // }
    //
    // function lienhe($table1, $table2)
    // {
    //     $query = $this->db->query("SELECT * FROM $table1 ");
    //     $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //     foreach ($temp as $item) {
    //         $data = ['name' => $item['name'], 'doi_tac' => 1, 'dien_thoai' => $item['dien_thoai'], 'email' => $item['email'], 'chuc_vu' => $item['chuc_vu'], 'tinh_trang' => 1];
    //         $query = $this->insert($table2, $data);
    //     }
    //     return $query;
    // }
    //
    // function dichvu($table1, $table2)
    // {
    //     $query = $this->db->query("SELECT * FROM $table1 ");
    //     $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //     foreach ($temp as $item) {
    //         $data = ['name' => $item['name'], 'phan_loai' => 1, 'don_vi_tinh' => $item['don_vi_tinh'], 'don_gia' => $item['don_gia'], 'gia_von' => $item['gia_von'], 'ghi_chu' => $item['ghi_chu'], 'tinh_trang' => 1];
    //         $query = $this->insert($table2, $data);
    //     }
    //     return $query;
    // }
    //
    // function donhang($table1, $table2)
    // {
    //     $query = $this->db->query("SELECT * FROM $table1 ");
    //     $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //     foreach ($temp as $item) {
    //         $data = [
    //             'name' => $item['name'],
    //             'product' => 1,
    //             'khach_hang' => $item['khach_hang'],
    //             'link' => $item['ghi_chu'],
    //             'ngay_bd' => $item['ngay'],
    //             'ngay_kt' => $item['thoi_han'],
    //             'so_tien' => $item['so_tien'],
    //             'ngay_thanh_toan' => $item['thanh_toan'],
    //             'hoa_don' => $item['hoa_don'],
    //             'loai' => 1,
    //             'tinh_trang' => 1,
    //         ];
    //         $query = $this->insert($table2, $data);
    //     }
    //     return $query;
    // }
    //
    // function thuengoai($table1, $table2)
    // {
    //     $query = $this->db->query("SELECT * FROM $table1 WHERE tinh_trang=1 and khach_hang=73 ");
    //     $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //     foreach ($temp as $item) {
    //         $data = [
    //             'name' => $item['name'],
    //             'product' => $item['product'],
    //             'khach_hang' => $item['nha_cung_cap'],
    //             'link' => $item['link'],
    //             'ngay_bd' => $item['ngay_bd'],
    //             'ngay_kt' => $item['ngay_kt'],
    //             'mo_ta' => $item['mo_ta'],
    //             'so_tien' => $item['so_tien'],
    //             'ngay_thanh_toan' => $item['ngay_thanh_toan'],
    //             'hoa_don' => $item['hoa_don'],
    //             'ghi_chu' => $item['ghi_chu'],
    //             'tinh_trang' => 1,
    //         ];
    //         $query = $this->insert($table2, $data);
    //     }
    //     return $query;
    // }
}

?>

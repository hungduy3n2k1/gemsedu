<?php

class bangchamconggv_model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($nam, $thang, $sort, $order, $offset, $rows, $giaovien)
    {
        $result = array();
        $namthang = $nam . "-" . $thang;
        $dieukien = " WHERE tinh_trang IN (4,5,6) AND ngay LIKE '$namthang%' ";
        $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc)=1  ";
        $dieukien .= " AND (SELECT phan_loai_2 FROM lophoc WHERE id=a.lop_hoc)!=3 ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien=$giaovien ";
        else
            $dieukien .= " AND giao_vien>0 ";
        $query = $this->db->query("SELECT id,giao_vien,lop_hoc,thoi_luong,ngay,
            (SELECT name FROM giaovien WHERE id=a.giao_vien) as giaovien,
            IFNULL((SELECT min(gio_vao) FROM diemdanh WHERE lich_hoc=a.id),gio) as gio,
            IFNULL((SELECT max(gio_ra) FROM diemdanh WHERE lich_hoc=a.id),gio_ra) as gio_ra,
            (SELECT name FROM lophoc WHERE id=a.lop_hoc) as tenlop
            FROM lichhoc a $dieukien ORDER BY lop_hoc ASC,giao_vien ASC");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $key = -1;
        $lophoc = 0;
        $giaovien = 0;
        $tongthoiluong = 0;
        foreach ($temp as $item) {
            if ($lophoc != $item['lop_hoc']) {
                $key++;
                $tongthoiluong = 0;
                $lophoc = $item['lop_hoc'];
                $giaovien = $item['giao_vien'];
                $result['rows'][$key]["giao_vien"] = $item['giao_vien'];
                $result['rows'][$key]["lop_hoc"] = $item['lop_hoc'];
                $result['rows'][$key]["giaovien"] = $item['giaovien'];
                $result['rows'][$key]["tenlop"] = $item['tenlop'];
            }
            if ($giaovien != $item['giao_vien']) {
                $key++;
                $tongthoiluong = 0;
                $giaovien = $item['giao_vien'];
                $lophoc = $item['lop_hoc'];
                $result['rows'][$key]["giao_vien"] = $item['giao_vien'];
                $result['rows'][$key]["lop_hoc"] = $item['lop_hoc'];
                $result['rows'][$key]["giaovien"] = $item['giaovien'];
                $result['rows'][$key]["tenlop"] = $item['tenlop'];
            }
            $tempngay = explode("-", $item['ngay']);
            $ngay = $tempngay[2];
            $time = $item['thoi_luong'];
            $templuong = ROUND((strtotime($item['gio_ra']) - strtotime($item['gio'])) / 60, 2);
            if ($templuong <= 0 || $templuong > $time) {
                $templuong = ROUND($time, 2);
            }
            $tongthoiluong += $templuong;
            if (isset($result['rows'][$key]["ngay_$ngay"]))
                $result['rows'][$key]["ngay_$ngay"] += $templuong;
            else
                $result['rows'][$key]["ngay_$ngay"] = $templuong;
            $result['rows'][$key]["tonggio"] = ROUND($tongthoiluong / 60, 2);
        }
        return $result;
    }

    function getCongDemo($nam, $thang, $sort, $order, $offset, $rows, $giaovien)
    {
        $result = array();
        $namthang = $nam . "-" . $thang;
        $dieukien = " WHERE tinh_trang IN (4,5,6) AND ngay LIKE '$namthang%' ";
        $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc)=1  ";
        $dieukien .= " AND (SELECT phan_loai_2 FROM lophoc WHERE id=a.lop_hoc)=3 ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien=$giaovien ";
        else
            $dieukien .= " AND giao_vien>0 ";
        $query = $this->db->query("SELECT id,giao_vien,
            (SELECT name FROM giaovien WHERE id=a.giao_vien) as giaovien
            FROM lichhoc a $dieukien GROUP BY giao_vien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $key = 0;
        foreach ($temp as $item) {
            $idgv = $item['giao_vien'];
            $query = $this->db->query("SELECT lop_hoc,
            (SELECT name FROM lophoc WHERE id=lop_hoc) as tenlop
            FROM lichhoc a WHERE tinh_trang IN (4,5,6) AND ngay LIKE '$namthang%' AND giao_vien=$idgv 
            AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc)=1 AND (SELECT phan_loai_2 FROM lophoc WHERE id=a.lop_hoc)=3
            GROUP BY lop_hoc");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($temp AS $lop) {
                $tongthoiluong = 0;
                $result['rows'][$key]["giaovien"] = $item['giaovien'];
                $lopid = $lop['lop_hoc'];
                $result['rows'][$key]["tenlop"] = $lop['tenlop'];
                for ($i = 1; $i < 32; $i++) {
                    if ($i < 10) {
                        $tempdate = $namthang . '-0' . $i;
                        $qr = $this->db->query("SELECT COUNT(id) AS total
                        FROM lichhoc a WHERE tinh_trang IN (2,4,5,6)  AND ngay = '$tempdate' AND giao_vien=$idgv AND lop_hoc=$lopid");
                        $t = $qr->fetchAll(PDO::FETCH_ASSOC);
                        if ($t) {
                            $thoiluong = $t[0]['total'];
                            $result['rows'][$key]["ngay_0$i"] = $thoiluong;
                            $tongthoiluong += $thoiluong;
                        } else
                            $result['rows'][$key]["ngay_0$i"] = 0;
                    } else {
                        $tempdate = $namthang . '-' . $i;
                        $qr = $this->db->query("SELECT COUNT(id) AS total
                        FROM lichhoc a WHERE tinh_trang IN (2,4,5,6) AND ngay = '$tempdate' AND giao_vien=$idgv AND lop_hoc=$lopid");
                        $t = $qr->fetchAll(PDO::FETCH_ASSOC);
                        if ($t) {
                            $thoiluong = $t[0]['total'];
                            $result['rows'][$key]["ngay_$i"] = $thoiluong;
                            $tongthoiluong += $thoiluong;
                        } else
                            $result['rows'][$key]["ngay_$i"] = 0;
                    }
                }
                $result['rows'][$key]["tonggio"] = $tongthoiluong;
                $key++;
            }
        }

        return $result;
    }

    function addObj($thang, $nam) // tạo bảng chấm công hoặc thêm nhân viên mới
    {
        $ok = false;
        $dieukien = " WHERE tinh_trang IN (1,2,3) AND van_phong>0 AND ca>0 ";
        $query = $this->db->query("SELECT id FROM nhanvien $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp AS $nhanvien) {
            $id = $nhanvien['id'];
            $dieukien = " WHERE thang = '$thang' AND nam = '$nam' AND nhan_vien=$id ";
            $query = $this->db->query("SELECT COUNT(1) AS total FROM bangchamcong $dieukien ");
            $row = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($row[0]['total'] == 0) {
                $data = array('nhan_vien' => $id, 'thang' => $thang, 'nam' => $nam);
                $ok = $this->insert("bangchamcong", $data);
            }
        }
        return $ok;
    }

// function chamcong($thang,$nam)
// {


//
// }
// $congchuan = $this->workingday($thang,$nam,$nhanvien);
// // Cập nhật bảng công đoàn (tính đi muộn về sớm)
// if (isset($chamcong) && (($chamcong==17) || ($chamcong==19))) { // quen cong
//     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//     if (isset($temp[0]['id'])) {
//           $xid = $temp[0]['id'];
//           $ok = $this->db->query("UPDATE congdoan SET so_lan_qc=so_lan_qc+1 WHERE id=$xid");
//     } else {
//           $data = array('nhan_vien'=>$nhanvien,'so_lan_qc'=>1,'thang'=>$thang,'nam'=>$nam);
//           $ok = $this->insert("congdoan", $data);
//     }
// } elseif (isset($chamcong) && ($chamcong==15)) { // di muon
//     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//     $sophut = ROUND((strtotime($giovao)-strtotime($cavao))/60);
//     if (isset($temp[0]['id'])) {
//           $xid = $temp[0]['id'];
//           $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
//     } else {
//           $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
//           $ok = $this->insert("congdoan", $data);
//     }
// } elseif (isset($chamcong) && ($chamcong==16)) { //ve som
//     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//     $sophut = ROUND((strtotime($cara)-strtotime($giora))/60);
//     if (isset($temp[0]['id'])) {
//           $xid = $temp[0]['id'];
//           $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
//     } else {
//           $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
//           $ok = $this->insert("congdoan", $data);
//     }
// } elseif (isset($chamcong) && ($chamcong==18)) { //di muon va ve som
//     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//     $sophut = ROUND((strtotime($cara)-strtotime($giora))/60)+ROUND((strtotime($giovao)-strtotime($cavao))/60);
//     if (isset($temp[0]['id'])) {
//           $xid = $temp[0]['id'];
//           $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
//     } else {
//           $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
//           $ok = $this->insert("congdoan", $data);
//     }
// }


// function chamcong()
// {
//               $data = array('tinh_trang'=>1,'cham_cong'=>$chamcong);

//           // Cập nhật bảng công đoàn (tính đi muộn về sớm)
//           if (isset($chamcong) && (($chamcong==17) || ($chamcong==19))) { // quen cong
//               $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//               $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//               if (isset($temp[0]['id'])) {
//                     $xid = $temp[0]['id'];
//                     $ok = $this->db->query("UPDATE congdoan SET so_lan_qc=so_lan_qc+1 WHERE id=$xid");
//               } else {
//                     $data = array('nhan_vien'=>$nhanvien,'so_lan_qc'=>1,'thang'=>$thang,'nam'=>$nam);
//                     $ok = $this->insert("congdoan", $data);
//               }
//           } elseif (isset($chamcong) && ($chamcong==15)) { // di muon
//               $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//               $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//               $sophut = ROUND((strtotime($giovao)-strtotime($cavao))/60);
//               if (isset($temp[0]['id'])) {
//                     $xid = $temp[0]['id'];
//                     $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
//               } else {
//                     $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
//                     $ok = $this->insert("congdoan", $data);
//               }
//       return $ok;
// }
}

?>

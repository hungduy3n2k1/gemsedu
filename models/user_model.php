<?php
class user_model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $email, $nhanvien, $giaovien)
    {
        $result = array();
        $dieukien        = " WHERE tinh_trang = 1 AND id > 1 ";
        if ($email != '')
            $dieukien .= " AND email LIKE '%$email%' ";
        if ($nhanvien > 0)
            $dieukien .= " AND nhan_vien = $nhanvien ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien = $giaovien ";
        $query           = $this->db->query("SELECT COUNT(id) AS total FROM users $dieukien");
        $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *,
           (SELECT name FROM nhanvien WHERE id = nhan_vien AND tinh_trang>0) AS nhanvien,
           (SELECT name FROM giaovien WHERE id = giao_vien AND tinh_trang>0) AS giaovien,
           (SELECT dia_chi FROM nhanvien WHERE id = nhan_vien AND tinh_trang>0) AS diachi,
           (SELECT dien_thoai FROM nhanvien WHERE id = nhan_vien AND tinh_trang>0) AS dienthoai,
           (SELECT name FROM phongban WHERE tinh_trang=1 AND id=nhom) AS phongban,
           (SELECT email FROM nhanvien WHERE id = nhan_vien) AS email_nv
           FROM users $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

   function dupliObj($username,$id)
   {
       $ok = false;
       $query = $this->db->query("SELECT COUNT(*) AS total FROM users WHERE tinh_trang = 1 AND  email = '$username' AND id!=$id ");
       $row = $query->fetchAll(PDO::FETCH_ASSOC);
       if ($row[0]['total']>0)
          $ok = true ;
       return $ok;
   }

   function addObj($data)
   {
       $query = $this->insert("users", $data);
       return $query;
   }

   function updateObj($id, $data)
   {
       $query = $this->update("users", $data, "id = $id");
       return true;
   }

   function delObj($id)
   {
       $data=array('tinh_trang' => 0);
       $query = $this->update("users", $data, "id = $id ");
       return $query;
   }

   function user($id) // lay thong tin nhan vien khi phan quyen
   {
       $query = $this->db->query("SELECT id, name, menu FROM users WHERE id = $id");
       return $query->fetchAll(PDO::FETCH_ASSOC);
   }

}
?>

<?php

class diemdanh_model extends model
{
    function __construct()
    {
        parent::__construct();
    }


    function getFetObj($lichhoc)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 AND id IN (SELECT hoc_vien FROM saplop WHERE lich_hoc=$lichhoc AND tinh_trang>0)";
        $sql = "SELECT id,name,e_name,
        (SELECT COUNT(id) FROM diemdanh WHERE lich_hoc=$lichhoc AND hoc_vien=a.id AND tinh_trang>0) AS checkhv
        FROM hocvien a $dieukien ";
        $query = $this->db->query($sql);
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function lichcheckin($giaovien)
    {
        $temp = array();
        $date = date("Y-m-d");
        $hour = date("H:i") . ':00';
        $hour1 = date("H:i", strtotime("-10 minutes")) . ':00';
        $hour2 = date("H:i", strtotime("+10 minutes")) . ':00';
        $dieukien = " WHERE tinh_trang = 1 AND ngay='$date' AND gio>='$hour1' AND gio<='$hour2' 
        AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc)<3 AND tinh_trang<7 ";
        if ($giaovien > 1)
            $dieukien .= " AND giao_vien=$giaovien ";
        $query = $this->db->query("SELECT id,gio,
        (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc) as phanloai,
        TIME_TO_SEC(TIMEDIFF('$date $hour', CONCAT(ngay,' ',gio))) as giohuy,
        IF(tinh_trang IN (2,3) AND TIME_TO_SEC(TIMEDIFF('$date $hour', CONCAT(ngay,' ',gio)))<=1800,1,0) as tinhtranghuy,
        CONCAT((SELECT name FROM lophoc WHERE id=a.lop_hoc),' (',DATE_FORMAT(ngay,'%d/%m/%Y'),' ',DATE_FORMAT(gio,'%H:%i'),')') as name
        FROM lichhoc a $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function lichcheckout($giaovien)
    {
        $temp = array();
        $date = date("Y-m-d");
        $date1 = date('Y-m-d', strtotime("-1 days"));
        $hour = date("H:i") . ':00';
        $dieukien = " WHERE (( ngay='$date1' AND tinh_trang>2 AND gio>='$hour' ) OR ( tinh_trang > 1 AND ngay='$date' )) 
        AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc)<3 AND tinh_trang<7 ";
        if ($giaovien > 1)
            $dieukien .= " AND giao_vien=$giaovien ";
        $query = $this->db->query("SELECT id,gio,
        (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc) as phanloai,
        TIME_TO_SEC(TIMEDIFF('$date $hour', CONCAT(ngay,' ',gio))) as giohuy,
        IF(tinh_trang IN (2,3) AND TIME_TO_SEC(TIMEDIFF('$date $hour', CONCAT(ngay,' ',gio)))<=1800,1,0) as tinhtranghuy,
        CONCAT((SELECT name FROM lophoc WHERE id=a.lop_hoc),' (',DATE_FORMAT(ngay,'%d/%m/%Y'),' ',DATE_FORMAT(gio,'%H:%i'),')') as name
        FROM lichhoc a $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function lichhoc($giaovien)
    {
        $temp = array();
        $date = date("Y-m-d");
        $date1 = date('Y-m-d', strtotime("-1 days"));
        $hour = date("H:i") . ':00';
        $hour1 = date("H:i", strtotime("-10 minutes")) . ':00';
        $hour2 = date("H:i", strtotime("+10 minutes")) . ':00';
        $dieukien = " WHERE (( tinh_trang = 1 AND ngay='$date' AND gio>='$hour1' AND gio<='$hour2' ) 
        OR ( ngay='$date1' AND tinh_trang>2 AND gio>='$hour' )
        OR ( tinh_trang > 1 AND ngay='$date' )) AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc)<3 AND tinh_trang<7 ";
        if ($giaovien > 1)
            $dieukien .= " AND giao_vien=$giaovien ";
        $query = $this->db->query("SELECT id,gio,
        (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc) as phanloai,
        TIME_TO_SEC(TIMEDIFF('$date $hour', CONCAT(ngay,' ',gio))) as giohuy,
        IF(tinh_trang IN (2,3) AND TIME_TO_SEC(TIMEDIFF('$date $hour', CONCAT(ngay,' ',gio)))<=1800,1,0) as tinhtranghuy,
        CONCAT((SELECT name FROM lophoc WHERE id=a.lop_hoc),' (',DATE_FORMAT(ngay,'%d/%m/%Y'),' ',DATE_FORMAT(gio,'%H:%i'),')') as name
        FROM lichhoc a $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function xoadiemdanh($lichhoc, $listhv)
    {
        if ($listhv != '') {
            return $query = $this->delete("diemdanh", "lich_hoc=$lichhoc AND hoc_vien NOT IN ($listhv)");
            $this->update("saplop", array('tinh_trang' => 1), "lich_hoc=$lichhoc AND hoc_vien NOT IN ($listhv)");
        } else {
            return $query = $this->delete("diemdanh", "lich_hoc=$lichhoc");
            $this->update("saplop", array('tinh_trang' => 1), "lich_hoc=$lichhoc");
        }
    }

    function checkdiemdanh($lichhoc, $hocvien)
    {
        $temp = array();
        $query = $this->db->query("SELECT COUNT(1) as total FROM diemdanh WHERE tinh_trang>0 AND lich_hoc='$lichhoc' AND hoc_vien=$hocvien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp)
            return $temp[0]['total'];
        else
            return 0;
    }

    function themDiemDanh($data)
    {
        $query = $this->insert("diemdanh", $data);
        if ($query) {
            $lichhoc = $data['lich_hoc'];
            $hocvien = $data['hoc_vien'];
            $this->update("saplop", array('tinh_trang' => 2), "lich_hoc=$lichhoc AND hoc_vien=$hocvien AND tinh_trang=1");
        }
        return $query;
    }

    function updateLichHoc($id, $data, $tinhtrang)
    {
        return $query = $this->update("lichhoc", $data, "id=$id AND $tinhtrang");
    }

    function checkin($lichhoc)
    {
        $ok = false;
        $query = $this->db->query("SELECT lop_hoc FROM lichhoc WHERE id = $lichhoc ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $lophocid = $temp[0]['lop_hoc'];
        $query = $this->db->query("SELECT don_hang FROM lophoc WHERE id = $lophocid ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $donhangid = $temp[0]['don_hang'];
        $data = ['tinh_trang' => 2]; 
        $this->update("donhang", $data, "id=$lophocid AND tinh_trang=1");
        $this->update("lophoc", $data, "id=$lophocid AND tinh_trang=1");
        $this->update("lichhoc", $data, "id=$lichhoc AND tinh_trang=1");
        return $ok;
    }

    function checkout($lichhoc, $link, $link_danh_gia)
    {
        $ok = false;
        $today = date("Y-m-d");
        $where = " lich_hoc = $lichhoc AND tinh_trang>0 ";
        $data = ['gio_ra' => date("H:i") . ':00', 'tinh_trang' => 2];
        $ok = $this->update("diemdanh", $data, $where);
        $this->update("lichhoc", ['link' => $link, 'link_danh_gia' => $link_danh_gia], "id=$lichhoc AND tinh_trang=4");
        if ($ok) {
            $ok = $this->update("lichhoc", ['tinh_trang' => 4, 'link' => $link, 'link_danh_gia' => $link_danh_gia, 'gio_ra' => date('H:i') . ':00'], "id=$lichhoc AND tinh_trang<4");
            $this->update("lophoc", ['tinh_trang' => 2], "id=(SELECT lop_hoc FROM lichhoc WHERE id=$lichhoc) AND tinh_trang=1");
            $query = $this->db->query("SELECT hoc_vien FROM diemdanh WHERE tinh_trang>0 AND lich_hoc='$lichhoc' ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($temp) {
                $listhv = '';
                foreach ($temp as $item)
                    $listhv .= $item['hoc_vien'] . ",";
                $listhv = rtrim($listhv, ",");
                $this->update("saplop", ['tinh_trang' => 3], " lich_hoc = $lichhoc AND hoc_vien IN ($listhv) AND tinh_trang=2 ");
            }
        }
        return $ok;
    }

    function huylich($lichhoc)
    {
        $ok = false;
        $query = $this->db->query("SELECT *,
        (SELECT hoc_vien FROM saplop WHERE lich_hoc=$lichhoc LIMIT 1) as hocvien,
        (SELECT MAX(ngay) FROM lichhoc WHERE lop_hoc=a.lop_hoc) as ngaycuoi
        FROM lichhoc a WHERE tinh_trang>0 AND id='$lichhoc' ");
        $lichcu = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($lichcu) {
            $ok = $this->update("lichhoc", ["tinh_trang" => 7], "id=$lichhoc");
            $this->update("saplop", ["tinh_trang" => 3], "lich_hoc=$lichhoc");
            $this->update("diemdanh", ["tinh_trang" => 3], "lich_hoc=$lichhoc");
            $ngaycu = $lichcu[0]['ngay'];
            $ngaycuoi = $lichcu[0]['ngaycuoi'];
            $tongngay = (strtotime($ngaycuoi) - strtotime($ngaycu)) / 86400;
            $ngaychenh = 7 - ($tongngay % 7);
            $ngaymoi = date("Y-m-d", strtotime("$ngaycuoi +$ngaychenh days"));
            $data = array(
                'ngay' => $ngaymoi,
                'gio' => $lichcu[0]['gio'],
                'gio_ra' => $lichcu[0]['gio_ra'],
                'lop_hoc' => $lichcu[0]['lop_hoc'],
                'giao_vien' => $lichcu[0]['giao_vien'],
                'phong_hoc' => $lichcu[0]['phong_hoc'],
                'thoi_luong' => $lichcu[0]['thoi_luong'],
                'tinh_trang' => 1
            );
            if ($this->insert("lichhoc", $data)) {
                $lichhocid = $this->db->lastInsertId();
                $saplop = array(
                    'hoc_vien' => $lichcu[0]['hocvien'],
                    'lich_hoc' => $lichhocid,
                    'lop_hoc' => $lichcu[0]['lop_hoc'],
                    'tinh_trang' => 1
                );
                $ok = $this->insert("saplop", $saplop);
            }
            return $ok;
        } else
            return false;
    }
}

?>

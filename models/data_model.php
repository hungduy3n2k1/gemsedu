<?php

class data_model extends model
{
    function __construct()
    {
        parent::__construct();

    }

    function getFetObj($sort, $order, $offset, $rows, $tinhtrang, $nhanvien, $tungay, $denngay, $loctrung, $nguoinhap, $kieungay, $tukhoa,$phanloai)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 ";
        if ($loctrung == 1) {
            $dieukien .= " AND dien_thoai IN (SELECT dien_thoai FROM data WHERE tinh_trang>0 AND dien_thoai!='' GROUP BY dien_thoai HAVING COUNT(1)>1 ) ";
            $query = $this->db->query("SELECT *,
               DATE_FORMAT(ngay_nhap,'%d/%m/%Y') as ngaynhap,
               DATE_FORMAT(ngay_hoc,'%d/%m/%Y') as ngayhoc,
               IF(ngay_chia!='',DATE_FORMAT(ngay_chia,'%d/%m/%Y'),'') as ngaychia,
               IF(ngay_sinh!='',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
               (SELECT name FROM nhanvien WHERE id = nguoi_nhap) as nguoinhap,
               (SELECT name FROM loaikh WHERE id=phan_loai) as loaikh, 
               (SELECT name FROM nhanvien WHERE id = nhan_vien) as nhanvien
               FROM data $dieukien ORDER BY dien_thoai ");
        } else {
            if ($kieungay == 1)
                $dieukien .= " AND ngay_chia>='$tungay' AND ngay_chia<='$denngay 23:59:59'  ";
            else
                $dieukien .= " AND ngay_nhap>='$tungay' AND ngay_nhap<='$denngay'  ";
            if ($tukhoa != '')
                $dieukien .= " AND (dien_thoai LIKE '%$tukhoa%' OR ho_ten LIKE '%$tukhoa%' )  ";
            if ($tinhtrang > 0)
                $dieukien .= " AND tinh_trang = $tinhtrang ";
            if ($nhanvien > 1)
                $dieukien .= " AND nhan_vien = $nhanvien ";
            if ($nguoinhap > 0)
                $dieukien .= " AND nguoi_nhap = $nguoinhap ";
             if ($phanloai > 0)
                 $dieukien .= " AND phan_loai = $phanloai ";
            $query = $this->db->query("SELECT COUNT(*) AS total FROM data $dieukien ");
            $row = $query->fetchAll(PDO::FETCH_ASSOC);
            $result['total'] = $row[0]['total'];
            $query = $this->db->query("SELECT *,
               DATE_FORMAT(ngay_nhap,'%d/%m/%Y') as ngaynhap,
                DATE_FORMAT(ngay_hoc,'%d/%m/%Y') as ngayhoc,
               IF(ngay_chia!='',DATE_FORMAT(ngay_chia,'%d/%m/%Y'),'') as ngaychia,
               IF(ngay_sinh!='',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
               (SELECT name FROM nhanvien WHERE id = nguoi_nhap) as nguoinhap,
               (SELECT name FROM loaikh WHERE id=phan_loai) as loaikh, 
               (SELECT name FROM nhanvien WHERE id = nhan_vien) as nhanvien
               FROM data $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        }
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function phuhuynh($id)
    {
        $result = [];
        $query = $this->db->query("SELECT CONCAT(ho_ten,' (',dien_thoai,')') AS hoten FROM phuhuynh WHERE tinh_trang=1 AND hoc_vien=$id ORDER BY id DESC");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function lichsu($id)
    {
        $result = [];
        $query = $this->db->query("SELECT CONCAT(ngay_gio,': ',ghi_chu) AS noidung FROM lichsu WHERE tinh_trang=1 AND hoc_vien=$id ORDER BY id DESC");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function gettinhtrang()
    {
        $return = array();
        $query = $this->db->query("SELECT * FROM trinhdo WHERE tinh_trang=1  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp AS $item)
            $return[$item['id']] = $item;
        return $return;
    }

    function addph($data)
    {
        $ok = $this->insert('phuhuynh', $data);
        return $ok;
    }

    function addnhatky($data)
    {
        $ok = $this->insert('lichsu', $data);
        return $ok;
    }

    function updateData($id, $data)
    {
        $ok = $this->update('data', $data, "id=$id");
        return $ok;
    }


    function addObj($data)
    {
        $ok = $this->insert('data', $data);
        if ($ok)
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function updateObj($nhanvien, $data)
    {
        $ok = false;
        $rows = json_decode($data, true);
        foreach ($rows as $row) {
            $id = $row['id'];
            $update = ['tinh_trang' => 2, 'nhan_vien' => $nhanvien, 'ngay_chia' => date('Y-m-d')];
            $ok = $this->update("data", $update, "id=$id");
        }
        return $ok;
    }

    function delObj($data)
    {
        $ok = false;
        $rows = json_decode($data, true);
        foreach ($rows as $row) {
            $id = $row['id'];
            $update = ['tinh_trang' => 0];
            $ok = $this->update("data", $update, "id=$id");
        }
        return $ok;
    }



    // function baocao($tungay, $denngay)
    // {
    //     $return = [];
    //     $dieukien = "  AND ngay_chia>='$tungay' AND ngay_chia<='$denngay' ";
    //     $query = $this->db->query("SELECT nhan_vien,
    //         (SELECT name FROM user WHERE id = nhan_vien) as telesale
    //         FROM data WHERE tinh_trang>1 $dieukien GROUP BY nhan_vien ");
    //     $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //     $i = 0;
    //     foreach ($temp as $item) {
    //         $nhanvien = $item['nhan_vien'];
    //         if ($nhanvien > 0) {
    //             $return[$i]['telesale'] = $item['telesale'];
    //             $query = $this->db->query("SELECT COUNT(1) AS tongdata
    //                 FROM data WHERE tinh_trang>1 $dieukien AND nhan_vien = $nhanvien ");
    //             $x = $query->fetchAll(PDO::FETCH_ASSOC);
    //             $return[$i]['tongdata'] = $x[0]['tongdata'];
    //             $query = $this->db->query("SELECT COUNT(1) AS calls
    //                 FROM data WHERE tinh_trang>2 $dieukien AND nhan_vien = $nhanvien ");
    //             $x = $query->fetchAll(PDO::FETCH_ASSOC);
    //             $return[$i]['calls'] = $x[0]['calls'];
    //             $query = $this->db->query("SELECT COUNT(1) AS fails
    //                 FROM data WHERE (tinh_trang=3 OR tinh_trang=4) $dieukien AND nhan_vien = $nhanvien ");
    //             $x = $query->fetchAll(PDO::FETCH_ASSOC);
    //             $return[$i]['fails'] = $x[0]['fails'];
    //             $query = $this->db->query("SELECT COUNT(1) AS doncare
    //                 FROM data WHERE tinh_trang=5 $dieukien AND nhan_vien = $nhanvien ");
    //             $x = $query->fetchAll(PDO::FETCH_ASSOC);
    //             $return[$i]['doncare'] = $x[0]['doncare'];
    //             $query = $this->db->query("SELECT COUNT(1) AS chamsoc
    //                 FROM data WHERE tinh_trang=6 $dieukien AND nhan_vien = $nhanvien ");
    //             $x = $query->fetchAll(PDO::FETCH_ASSOC);
    //             $return[$i]['chamsoc'] = $x[0]['chamsoc'];
    //             $query = $this->db->query("SELECT COUNT(1) AS chot
    //                 FROM data WHERE tinh_trang=7 $dieukien AND nhan_vien = $nhanvien ");
    //             $x = $query->fetchAll(PDO::FETCH_ASSOC);
    //             $return[$i]['chot'] = $x[0]['chot'];
    //             $i++;
    //         }
    //     }
    //     return $return;
    // }

    function checkso($dienthoai, $id)
    {
        if ($dienthoai == '')
            return false;
        else {
            $query = $this->db->query("SELECT count(id) AS total FROM ungvien WHERE tinh_trang=1 AND dien_thoai='$dienthoai' AND id!=$id");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($temp[0]['total'] > 0)
                return true;
            else
                return false;
        }
    }

    function checksodata($dienthoai)
    {
        $query = $this->db->query("SELECT count(id) AS total FROM data WHERE tinh_trang>0 AND dien_thoai='$dienthoai' ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp[0]['total'] > 0)
            return true;
        else
            return false;
    }

    function themungvien($data)
    {
        $ok = $this->insert('ungvien', $data);
        return $ok;
    }

    function themKH($iddata)
    {
        $query = $this->db->query("SELECT * FROM `data` WHERE tinh_trang>0 AND id=$iddata ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp) {
            $data = array(
                'name' => $temp[0]['ho_ten'],
                'dien_thoai' => $temp[0]['dien_thoai'],
                'dia_chi' => $temp[0]['dia_chi'],
                'nguon_data' => $temp[0]['nguon_data'],
                'ghi_chu' => $temp[0]['ghi_chu'],
                'ngay' => date('Y-m-d'),
                'nhan_vien' => $_SESSION['user']['nhan_vien'],
                'phu_trach' => $_SESSION['user']['nhan_vien'],
                'tinh_trang' => 1
            );
            if ($this->insert('khachhang', $data))
                return $this->db->lastInsertId();
            else
                return 0;
        } else
            return 0;
    }

    function themHocVien($data)
    {
        $ok = $this->insert('hocvien', $data);
        if($ok)
            return $this->db->lastInsertId();
        else return 0;
    }

    function lastIdLop(){
        $query = $this->db->query("SELECT id FROM `lophoc` WHERE tinh_trang>0 ORDER BY id DESC LIMIT 1");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if($temp)
            return $temp[0]['id'];
        else
            return '';
    }
    function getPhanLoai($phanloai){
        $query = $this->db->query("SELECT id FROM `loaikh` WHERE tinh_trang>0 AND name='$phanloai' ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if($temp)
            return $temp[0]['id'];
        else
            return 0;
    }

    function ThemLopHoc($data)
    {
        $ok = $this->insert('lophoc', $data);
        if ($ok)
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function ThemLichHoc($data)
    {
        $query = $this->insert("lichhoc", $data);
        if ($query)
            return $this->db->lastInsertId();
        else return 0;
    }

    function themSapLop($data)
    {
        $ok = $this->insert('saplop', $data);
        return $ok;
    }

    function themDonHang($data)
    {
        $ok = $this->insert('donhang', $data);
        if($ok)
            return $this->db->lastInsertId();
        else return 0;
    }

    function updateDonhang($id,$data)
    {
        $ok = $this->update('donhang', $data,"id=$id");
        return $ok;
    }

    function themInvoice($data){
        $ok = $this->insert('invoice', $data);
        if($ok)
            return $this->db->lastInsertId();
        else return 0;
    }

    function phanloaidemo()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM loaidichvu WHERE tinh_trang=1 AND id IN (15,16,18,19) ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function phanloai()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM loaidichvu WHERE tinh_trang=1 AND id NOT IN (15,16,18,19,20) ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function tinhtrang()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM trinhdo WHERE tinh_trang=1 AND id<7 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

}

?>

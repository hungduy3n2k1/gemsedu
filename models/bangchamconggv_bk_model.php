<?php

class bangchamconggv_bk_model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($nam, $thang, $sort, $order, $offset, $rows, $giaovien)
    {
        $result = array();
        $namthang = $nam . "-" . $thang;
        $dieukien = " WHERE tinh_trang IN (4,5,6) AND ngay LIKE '$namthang%' ";
        $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc)=1  ";
        $dieukien .= " AND (SELECT phan_loai_2 FROM lophoc WHERE id=a.lop_hoc)!=3 ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien=$giaovien ";
        else
            $dieukien .= " AND giao_vien>0 ";
        $query = $this->db->query("SELECT id,giao_vien,
            (SELECT name FROM giaovien WHERE id=a.giao_vien) as giaovien
            FROM lichhoc a $dieukien GROUP BY giao_vien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $key = 0;
        foreach ($temp as $item) {
            $idgv = $item['giao_vien'];
            $query = $this->db->query("SELECT lop_hoc,
            (SELECT name FROM lophoc WHERE id=a.lop_hoc) as tenlop
            FROM lichhoc a WHERE tinh_trang IN (4,5,6) AND ngay LIKE '$namthang%' AND giao_vien=$idgv 
            AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc)=1 AND (SELECT phan_loai_2 FROM lophoc WHERE id=a.lop_hoc)!=3
            GROUP BY lop_hoc");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($temp AS $lop) {
                $tongthoiluong = 0;
                $result['rows'][$key]["giaovien"] = $item['giaovien'];
                $lopid = $lop['lop_hoc'];
                $result['rows'][$key]["tenlop"] = $lop['tenlop'];
                for ($i = 1; $i < 32; $i++) {
                    if ($i < 10) {
                        $tempdate = $namthang . '-0' . $i;
                        $qr = $this->db->query("SELECT id,thoi_luong,
                        IFNULL((SELECT min(gio_vao) FROM diemdanh WHERE lich_hoc=a.id),gio) as gio,
                        IFNULL((SELECT max(gio_ra) FROM diemdanh WHERE lich_hoc=a.id),gio_ra) as gio_ra
                        FROM lichhoc a WHERE tinh_trang IN (4,5,6)  AND ngay = '$tempdate' AND giao_vien=$idgv AND lop_hoc=$lopid");
                        $t = $qr->fetchAll(PDO::FETCH_ASSOC);
                        if ($t) {
                            $thoiluong = 0;
                            foreach ($t as $lh) {
                                $time = $lh['thoi_luong'];
                                $templuong = ROUND((strtotime($lh['gio_ra']) - strtotime($lh['gio'])) / 60, 2);
                                if ($templuong < 0 || $templuong > $time) {
                                    $templuong = ROUND($time, 2);
                                }
                                $thoiluong += $templuong;
                                // $congngay .= "'$thoiluong' as ngay_0$i,";
                            }
                            $tongthoiluong += $thoiluong;
                            $result['rows'][$key]["ngay_0$i"] = $thoiluong;

                            //$congngay .= "'0' as ngay_0$i,";
                        } else
                            $result['rows'][$key]["ngay_0$i"] = 0;
                        // $congngay .= "'0' as ngay_0$i,";
                    } else {
                        $tempdate = $namthang . '-' . $i;
                        $qr = $this->db->query("SELECT id,thoi_luong,
                         IFNULL((SELECT min(gio_vao) FROM diemdanh WHERE lich_hoc=a.id),gio) as gio,
                        IFNULL((SELECT max(gio_ra) FROM diemdanh WHERE lich_hoc=a.id),gio_ra) as gio_ra
                        FROM lichhoc a WHERE tinh_trang IN (4,5,6) AND ngay = '$tempdate' AND giao_vien=$idgv AND lop_hoc=$lopid");
                        $t = $qr->fetchAll(PDO::FETCH_ASSOC);
                        if ($t) {
                            $thoiluong = 0;
                            foreach ($t as $lh) {
                                $time = $lh['thoi_luong'];
                                $templuong = ROUND((strtotime($lh['gio_ra']) - strtotime($lh['gio'])) / 60, 2);
                                if ($templuong < 0 || $templuong > $time) {
                                    $templuong = ROUND($time, 2);
                                }
                                $thoiluong += $templuong;
                                // $congngay .= "'$thoiluong' as ngay_0$i,";
                            }
                            $tongthoiluong += $thoiluong;
                            $result['rows'][$key]["ngay_$i"] = $thoiluong;

                            //$congngay .= "'0' as ngay_0$i,";
                        } else
                            $result['rows'][$key]["ngay_$i"] = 0;
                    }
                }
                $result['rows'][$key]["tonggio"] = ROUND($tongthoiluong / 60, 2);
                $key++;
            }
//            $query = $this->db->query("SELECT
//            $congngay
//            '2' as ngaycong,
//            $tongthoiluong as tong
//            '$giaovien' as giaovien FROM 1 ");
//            $result += $query->fetchAll(PDO::FETCH_ASSOC);
        }

        return $result;
    }

    function getCongDemo($nam, $thang, $sort, $order, $offset, $rows, $giaovien)
    {
        $result = array();
        $namthang = $nam . "-" . $thang;
        $dieukien = " WHERE tinh_trang IN (4,5,6) AND ngay LIKE '$namthang%' ";
        $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc)=1  ";
        $dieukien .= " AND (SELECT phan_loai_2 FROM lophoc WHERE id=a.lop_hoc)=3 ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien=$giaovien ";
        else
            $dieukien .= " AND giao_vien>0 ";
        $query = $this->db->query("SELECT id,giao_vien,
            (SELECT name FROM giaovien WHERE id=a.giao_vien) as giaovien
            FROM lichhoc a $dieukien GROUP BY giao_vien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $key = 0;
        foreach ($temp as $item) {
            $idgv = $item['giao_vien'];
            $query = $this->db->query("SELECT lop_hoc,
            (SELECT name FROM lophoc WHERE id=lop_hoc) as tenlop
            FROM lichhoc a WHERE tinh_trang IN (4,5,6) AND ngay LIKE '$namthang%' AND giao_vien=$idgv 
            AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc)=1 AND (SELECT phan_loai_2 FROM lophoc WHERE id=a.lop_hoc)=3
            GROUP BY lop_hoc");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($temp AS $lop) {
                $tongthoiluong = 0;
                $result['rows'][$key]["giaovien"] = $item['giaovien'];
                $lopid = $lop['lop_hoc'];
                $result['rows'][$key]["tenlop"] = $lop['tenlop'];
                for ($i = 1; $i < 32; $i++) {
                    if ($i < 10) {
                        $tempdate = $namthang . '-0' . $i;
                        $qr = $this->db->query("SELECT COUNT(id) AS total
                        FROM lichhoc a WHERE tinh_trang IN (2,4,5,6)  AND ngay = '$tempdate' AND giao_vien=$idgv AND lop_hoc=$lopid");
                        $t = $qr->fetchAll(PDO::FETCH_ASSOC);
                        if ($t) {
                            $thoiluong = $t[0]['total'];
                            $result['rows'][$key]["ngay_0$i"] = $thoiluong;
                            $tongthoiluong += $thoiluong;
                        } else
                            $result['rows'][$key]["ngay_0$i"] = 0;
                    } else {
                        $tempdate = $namthang . '-' . $i;
                        $qr = $this->db->query("SELECT COUNT(id) AS total
                        FROM lichhoc a WHERE tinh_trang IN (2,4,5,6) AND ngay = '$tempdate' AND giao_vien=$idgv AND lop_hoc=$lopid");
                        $t = $qr->fetchAll(PDO::FETCH_ASSOC);
                        if ($t) {
                            $thoiluong = $t[0]['total'];
                            $result['rows'][$key]["ngay_$i"] = $thoiluong;
                            $tongthoiluong += $thoiluong;
                        } else
                            $result['rows'][$key]["ngay_$i"] = 0;
                    }
                }
                $result['rows'][$key]["tonggio"] = ROUND($tongthoiluong / 60, 2);
                $key++;
            }
        }

        return $result;
    }

    function addObj($thang, $nam) // tạo bảng chấm công hoặc thêm nhân viên mới
    {
        $ok = false;
        $dieukien = " WHERE tinh_trang IN (1,2,3) AND van_phong>0 AND ca>0 ";
        $query = $this->db->query("SELECT id FROM nhanvien $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp AS $nhanvien) {
            $id = $nhanvien['id'];
            $dieukien = " WHERE thang = '$thang' AND nam = '$nam' AND nhan_vien=$id ";
            $query = $this->db->query("SELECT COUNT(1) AS total FROM bangchamcong $dieukien ");
            $row = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($row[0]['total'] == 0) {
                $data = array('nhan_vien' => $id, 'thang' => $thang, 'nam' => $nam);
                $ok = $this->insert("bangchamcong", $data);
            }
        }
        return $ok;
    }

// function chamcong($thang,$nam)
// {


//
// }
// $congchuan = $this->workingday($thang,$nam,$nhanvien);
// // Cập nhật bảng công đoàn (tính đi muộn về sớm)
// if (isset($chamcong) && (($chamcong==17) || ($chamcong==19))) { // quen cong
//     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//     if (isset($temp[0]['id'])) {
//           $xid = $temp[0]['id'];
//           $ok = $this->db->query("UPDATE congdoan SET so_lan_qc=so_lan_qc+1 WHERE id=$xid");
//     } else {
//           $data = array('nhan_vien'=>$nhanvien,'so_lan_qc'=>1,'thang'=>$thang,'nam'=>$nam);
//           $ok = $this->insert("congdoan", $data);
//     }
// } elseif (isset($chamcong) && ($chamcong==15)) { // di muon
//     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//     $sophut = ROUND((strtotime($giovao)-strtotime($cavao))/60);
//     if (isset($temp[0]['id'])) {
//           $xid = $temp[0]['id'];
//           $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
//     } else {
//           $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
//           $ok = $this->insert("congdoan", $data);
//     }
// } elseif (isset($chamcong) && ($chamcong==16)) { //ve som
//     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//     $sophut = ROUND((strtotime($cara)-strtotime($giora))/60);
//     if (isset($temp[0]['id'])) {
//           $xid = $temp[0]['id'];
//           $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
//     } else {
//           $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
//           $ok = $this->insert("congdoan", $data);
//     }
// } elseif (isset($chamcong) && ($chamcong==18)) { //di muon va ve som
//     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//     $sophut = ROUND((strtotime($cara)-strtotime($giora))/60)+ROUND((strtotime($giovao)-strtotime($cavao))/60);
//     if (isset($temp[0]['id'])) {
//           $xid = $temp[0]['id'];
//           $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
//     } else {
//           $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
//           $ok = $this->insert("congdoan", $data);
//     }
// }


// function chamcong()
// {
//               $data = array('tinh_trang'=>1,'cham_cong'=>$chamcong);

//           // Cập nhật bảng công đoàn (tính đi muộn về sớm)
//           if (isset($chamcong) && (($chamcong==17) || ($chamcong==19))) { // quen cong
//               $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//               $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//               if (isset($temp[0]['id'])) {
//                     $xid = $temp[0]['id'];
//                     $ok = $this->db->query("UPDATE congdoan SET so_lan_qc=so_lan_qc+1 WHERE id=$xid");
//               } else {
//                     $data = array('nhan_vien'=>$nhanvien,'so_lan_qc'=>1,'thang'=>$thang,'nam'=>$nam);
//                     $ok = $this->insert("congdoan", $data);
//               }
//           } elseif (isset($chamcong) && ($chamcong==15)) { // di muon
//               $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
//               $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
//               $sophut = ROUND((strtotime($giovao)-strtotime($cavao))/60);
//               if (isset($temp[0]['id'])) {
//                     $xid = $temp[0]['id'];
//                     $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
//               } else {
//                     $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
//                     $ok = $this->insert("congdoan", $data);
//               }
//       return $ok;
// }
}

?>

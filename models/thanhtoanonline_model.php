<?php
class thanhtoanonline_Model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $tukhoa)
    {
        $result = [];
        $dieukien = " WHERE  tinh_trang=1 AND ngay_gio>='$tungay' AND ngay_gio<='$denngay 23:59:59' ";
        if ($tukhoa != '')
            $dieukien .= " AND (noi_dung LIKE '%$tukhoa%' OR don_hang LIKE '%$tukhoa%') ";
        $query = $this->db->query("SELECT COUNT(id) AS total FROM giaodich $dieukien");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay_gio,'%d/%m/%Y %H:%i:%s') AS ngaygio
            FROM giaodich $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $doanhthu = 0;
        foreach ($result['rows'] as $item) {
            $doanhthu = $doanhthu + $item['so_tien'];
        }
        $result['footer'] = [0 => ['ngaygio' => 'Tổng cộng', 'so_tien' => $doanhthu]];
        return $result;
    }



}
?>

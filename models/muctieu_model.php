<?php
class muctieu_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang=1  ";
        $query = $this->db->query("SELECT COUNT(1) AS total FROM loaikh $dieukien");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT * FROM loaikh $dieukien ");
        $result['rows'] =  $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function delObj($id)
    {
        $data = array('tinh_trang' => 0);
        $query = $this->update("loaikh", $data, "id = $id");
        return $query;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("loaikh", $data, "id = $id");
        return $query;
    }

    function addObj($data)
    {
        $query = $this->insert("loaikh", $data);
        return $query;
    }
}
?>

<?php

class baocaohv2_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $hocvien, $khachhang)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang > 0 ";
        if ($hocvien != '')
            $dieukien .= " AND name LIKE '%$hocvien%' ";
        if ($khachhang != '')
            $dieukien .= " AND khach_hang IN (SELECT id FROM khachhang WHERE name LIKE '%$khachhang%') ";

        $query = $this->db->query("SELECT count(id) as total
        FROM hocvien a $dieukien 
        AND (SELECT SUM(so_tien) FROM donhang WHERE tinh_trang IN (1,2) AND hoc_vien = a.id) > 0 
        AND (SELECT SUM(so_buoi+buoi_khuyen_mai) FROM lophoc WHERE hoc_vien=a.id) > 0
        AND (SELECT phan_loai_2 FROM lophoc WHERE hoc_vien=a.id LIMIT 1) != 3");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $temp[0]['total'];
        $sql = "SELECT *,
        IF(ngay_sinh!='0000-00-00',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
        (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
        CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id) as mahocvien,
        (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh,
        (SELECT email FROM khachhang WHERE id=a.khach_hang) as emailkh,
        IFNULL((SELECT SUM(so_tien) FROM donhang WHERE tinh_trang IN (1,2) 
        AND hoc_vien = a.id),0) as sotien,
        (SELECT SUM(so_buoi+buoi_khuyen_mai) FROM lophoc WHERE hoc_vien=a.id) AS tongbuoi,
        (SELECT phan_loai_2 FROM lophoc WHERE hoc_vien=a.id LIMIT 1) AS phan_loai_2
        FROM hocvien a $dieukien HAVING sotien > 0 AND tongbuoi > 0 AND phan_loai_2 != 3 ORDER BY $sort $order LIMIT $offset, $rows";
        $query = $this->db->query($sql);
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result['rows'] as $key => $item) {
            $result['rows'][$key]['tongbuoi'] = $item['tongbuoi'];

            $idhocvien = $item['id'];
            $query1 = $this->db->query("SELECT lop_hoc
            FROM saplop a WHERE tinh_trang>0 AND hoc_vien = $idhocvien AND (SELECT phan_loai_2 FROM lophoc WHERE id = a.lop_hoc) != 3
            GROUP BY lop_hoc");
            $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
            $listgt = '';
            $listlh = '';
            foreach ($temp as $saplop) {
                $lophocid = $saplop["lop_hoc"];
                $query = $this->db->query("SELECT (SELECT name FROM giaotrinh WHERE id = a.giao_trinh) as giaotrinh
                FROM lophoc a WHERE tinh_trang > 0 AND id = $lophocid");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                $listgt .= $temp[0]['giaotrinh'].', ';
                $listlh .= $lophocid . ", ";
            }
            $listgt = rtrim($listgt,", ");
            $result['rows'][$key]['giaotrinh'] = $listgt;
            $listlh = rtrim($listlh, ", ");
            
            if($listlh != ''){
                $query1 = $this->db->query("SELECT MAX(ngay) as ngayketthuc, MIN(ngay) AS ngaybatdau
                FROM lichhoc a WHERE tinh_trang > 0 AND tinh_trang != 4 AND lop_hoc IN ($listlh) ");
                $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                $result['rows'][$key]['ngayketthuc'] = $temp[0]['ngayketthuc'];
                $result['rows'][$key]['ngaybatdau'] = $temp[0]['ngaybatdau'];

                $query1 = $this->db->query("SELECT count(id) as dahoc
                FROM lichhoc WHERE tinh_trang IN (2,3,4,5,6) AND lop_hoc IN ($listlh) ");
                $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                $result['rows'][$key]['dahoc'] = $temp[0]['dahoc'];
                $result['rows'][$key]['conlai'] = $result['rows'][$key]['tongbuoi']-$temp[0]['dahoc'];
            } else {
                $result['rows'][$key]['dahoc'] = 0;
                $result['rows'][$key]['conlai'] = $result['rows'][$key]['tongbuoi']-$result['rows'][$key]['dahoc'] = 0;
            }
            
        }
        // }
        return $result;
    }

}

?>

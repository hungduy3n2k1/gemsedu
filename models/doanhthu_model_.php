<?php
class doanhthu_model extends model
{
   function __construct()
   {
       parent::__construct();
   }

   function getFetObj($tungay, $denngay)
   {
       $return = array();
       $dieukien = " WHERE tinh_trang>0 AND ngay>='$tungay' AND ngay<='$denngay' ";
       $query    = $this->db->query("SELECT COUNT(1) as total FROM invoice $dieukien ");
       $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
       $return['total']=$temp[0]['total'];
       $query    = $this->db->query("SELECT *, CONCAT('INV-',id) AS invoice, DATE_FORMAT(ngay,'%d/%m/%Y') AS ngaygio,
          (SELECT name FROM khachhang WHERE id=khach_hang) AS khachhang,
          (SELECT name FROM nhanvien WHERE id=nhan_vien) AS nhanvien,
          (SELECT SUM(so_luong*(don_gia-chiet_khau_tm)*(1-ROUND(chiet_khau_pt/100))) FROM invoicesub WHERE tinh_trang=1 AND invoice=a.id) AS sotien
          FROM invoice a $dieukien ");
       $return['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
       $tong = 0;
       foreach ($return['rows'] as $row)
          $tong = $tong+$row['sotien'];
       $return['footer'] = array(0=>array('noi_dung'=>'Tổng doanh thu','sotien'=>$tong));
       return $return;
   }

   function getDetail($id)
   {
        $return = array();
        $dieukien        = " WHERE tinh_trang=1 AND invoice=$id ";
        // $query           = $this->db->query("SELECT COUNT(*) AS total FROM invoicesub $dieukien");
        // $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        // $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *,
            (SELECT name FROM dichvu WHERE id=dich_vu) AS dichvu,
            so_luong*(don_gia-chiet_khau_tm)*(1-ROUND(chiet_khau_pt/100)) AS thanhtien
            FROM invoicesub $dieukien ");
        $return = $query->fetchAll(PDO::FETCH_ASSOC);
        return $return;
   }

   function doanhthu($tungay, $denngay)
   {
       $dieukien = " WHERE tinh_trang=1 AND hach_toan=1 AND ngay_gio>'$tungay' AND ngay_gio<='$denngay 23:59:59' ";
       $query    = $this->db->query("SELECT SUM(so_tien) as total FROM socai $dieukien ");
       $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
       return $temp[0]['total'];
   }

   function chitietdoanhthu($tungay, $denngay)
   {
       $dieukien = " WHERE tinh_trang=1 AND hach_toan=1 AND ngay_gio>'$tungay' AND ngay_gio<='$denngay 23:59:59' ";
       $query    = $this->db->query("SELECT dien_giai, so_tien,DATE_FORMAT(ngay_gio, '%d/%m/%Y') AS ngay,
          (SELECT name FROM khachhang WHERE id=khach_hang) AS khachhang
          FROM socai $dieukien ");
       $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
       return $temp;
   }

   function chiphi($tungay, $denngay)
   {
       $dieukien = " WHERE tinh_trang=1 AND hach_toan=2 AND ngay_gio>'$tungay' AND ngay_gio<='$denngay 23:59:59' ";
       $query    = $this->db->query("SELECT SUM(so_tien) as total FROM socai $dieukien ");
       $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
       return $temp[0]['total'];
   }

   function chitietchiphi($tungay, $denngay)
   {
       $dieukien = " WHERE tinh_trang=1 AND hach_toan=2 AND ngay_gio>'$tungay' AND ngay_gio<='$denngay 23:59:59' ";
       $query    = $this->db->query("SELECT dien_giai,so_tien,DATE_FORMAT(ngay_gio, '%d/%m/%Y') AS ngay,
          (SELECT name FROM khachhang WHERE id=khach_hang) AS khachhang
          FROM socai $dieukien ");
       $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
       return $temp;
   }
}
?>

<?php

class lichhoc_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop)
    {
        $data = array();
        $thangnam = $nam . '-' . $thang;
//        if($ngay!='')
//            $thangnam = $ngay.'-'.$nam . '-' . $thang;
        $firstday = date("N", strtotime('first day of this month', strtotime($thangnam)));
        $lastday = date("N", strtotime('last day of this month', strtotime($thangnam)));
        $endmonth = date("d", strtotime('last day of this month', strtotime($thangnam)));
        $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' ";
        if ($phonghoc > 0)
            $dieukien .= " AND phong_hoc=$phonghoc ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien=$giaovien ";
        if ($loai == 1)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc)=1 ";
        else if ($loai == 2)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (2,3,4) ";
        else if ($loai == 3)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (5,6,7) ";
        if ($giohoc != '')
            $dieukien .= " AND gio='$giohoc:00' ";
        if ($tenlop != '')
            $dieukien .= " AND (SELECT name FROM lophoc WHERE id=lop_hoc) LIKE '%$tenlop%' ";
        $query = $this->db->query("SELECT *, (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc
            FROM lichhoc $dieukien  ORDER BY ngay ASC, gio ASC");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        $lastrow = '';
        $index = 1;
        $check = array();
        for ($i = 1; $i <= $endmonth; $i++) {
            $check[$i] = '';
            $temp[$i] = '<div style="font-weight:bold;">' . $i . '</div>';
            foreach ($rows as $key => $row) {
                $ngay = (int)substr($row['ngay'], -2);
                $gio = substr($row['gio'], 0, 5) . ' ';
                if ($ngay == $i) {
                    if ($index <= 5) {
                        $temp[$i] .= '
                          <div onclick="edit(' . $row['id'] . ')" style="color:#1a75ba; font-weight:bold; padding:2px; cursor: pointer;">' . $gio . '
                          <span style="color:red;font-weight:bold;">' . $row['lophoc'] . '</span>' . $row['phonghoc'] . '</div>';
                        $index++;
                    } else {
                        $check[$i] = $row['ngay'];
                    }
                } else {
                    $index = 1;
                }
            }
            if ($check[$i] != '')
                $temp[$i] .= '<br><div onclick="xemthem(\'' . $check[$i] . '\')" style="color:blue; font-weight:bold; padding:2px; cursor: pointer;float: right;">
                          Xem thêm +</div>';
        }
        $bangchamcong = array();
        for ($i = 0; $i < 6; $i++) {
            if ($i == 0)
                for ($j = $firstday; $j < 8; $j++)
                    $bangchamcong[$i][$j] = isset($temp[$j - $firstday + 1]) ? $temp[$j - $firstday + 1] : '';
            elseif ($i < 5)
                for ($j = 1; $j < 8; $j++)
                    $bangchamcong[$i][$j] = isset($temp[$j + 7 * $i - $firstday + 1]) ? $temp[$j + 7 * $i - $firstday + 1] : '';
            else
                for ($j = 1; $j <= $lastday; $j++)
                    $bangchamcong[$i][$j] = isset($temp[$j + 7 * $i - $firstday + 1]) ? $temp[$j + 7 * $i - $firstday + 1] : '';
        }
        $data['rows'] = $bangchamcong;
        return $data;
    }

    function getrow($id)
    {
        $result = array();
        $query = $this->db->query("SELECT *,DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,DATE_FORMAT(ngay,'%a') AS thu  FROM lichhoc WHERE id=$id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $result = $temp[0];
        return $result;
    }

    function delObj($id)
    {
        $data = array('tinh_trang' => 0);
        $query = $this->update("lichhoc", $data, "id = $id");
        $this->update("saplop", $data, "lich_hoc = $id");
        return $query;
    }

    function delAllObj($lophoc)
    {
        $data = array('tinh_trang' => -1);
        $query = $this->update("diemdanh", $data, "lich_hoc IN (SELECT id FROM lichhoc WHERE lop_hoc = $lophoc)");
        $query = $this->update("lichhoc", $data, "lop_hoc = $lophoc");
        $query = $this->update("saplop", $data, "lop_hoc = $lophoc");
        return $query;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("lichhoc", $data, "id = $id");
        return $query;
    }

    function addObj($data)
    {
        $query = $this->insert("lichhoc", $data);
        if ($query)
            return $this->db->lastInsertId();
        else return 0;
    }

    function checkSoBuoi($lophoc)
    {
        $query = $this->db->query("SELECT (so_buoi+buoi_khuyen_mai) AS sobuoi
        FROM lophoc WHERE id=$lophoc ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $sobuoi = $temp[0]['sobuoi'];
        $query = $this->db->query("SELECT COUNT(id) AS total
        FROM lichhoc WHERE lop_hoc=$lophoc AND tinh_trang IN (1,2,3,4,5,6)");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $solichhoc = $temp[0]['total'];
        if($sobuoi > $solichhoc) 
            return 1;
        else 
            return 0;
    }

    function themSaplop($data)
    {
        $query = $this->insert("saplop", $data);
        if ($query)
            return $this->db->lastInsertId();
        else return 0;
    }

    function getview($offset, $rows, $ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop)
    {
        $data = array();
        if ($ngay != '') {
            $thangnam = $nam . '-' . $thang . '-' . $ngay;
            $dieukien = " WHERE tinh_trang>0 AND ngay='$thangnam' ";
        } elseif ($thang != '') {
            $thangnam = $nam . '-' . $thang;
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' ";
        } elseif ($nam != '') {
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$nam%' ";
        } else {
            $dieukien = " WHERE tinh_trang>0 ";
        }
        if ($phonghoc > 0)
            $dieukien .= " AND phong_hoc=$phonghoc ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien=$giaovien ";
        if ($loai == 1)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc)=1 ";
        else if ($loai == 2)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (2,3,4) ";
        else if ($loai == 3)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (5,6,7) ";
        if ($giohoc != '')
            $dieukien .= " AND gio='$giohoc:00' ";
        if ($tenlop != '')
            $dieukien .= " AND (SELECT name FROM lophoc WHERE id=lop_hoc) LIKE '%$tenlop%' ";
        $query = $this->db->query("SELECT id
            FROM lichhoc $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $data['total'] = count($temp);
        $query = $this->db->query("SELECT *, (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            DATE_FORMAT(ngay,'%a') AS thu,
            ngay as ngay_hoc,
            (SELECT name FROM giaovien WHERE id=giao_vien) AS giaovien,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc,
            (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) AS loailop,
            (SELECT name FROM hocvien WHERE id=(SELECT hoc_vien FROM saplop WHERE lich_hoc=lichhoc.id AND tinh_trang>0 LIMIT 1)) AS hocvien
            FROM lichhoc $dieukien  ORDER BY ngay_hoc, gio,tinh_trang LIMIT $offset, $rows");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        $data['rows'] = $rows;
        return $data;
    }

    function getday($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop)
    {
        $data = array();
        if ($ngay != '') {
            $thangnam = $nam . '-' . $thang . '-' . $ngay;
            $dieukien = " WHERE tinh_trang>0 AND ngay='$thangnam' ";
        } elseif ($thang != '') {
            $thangnam = $nam . '-' . $thang;
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' ";
        } elseif ($nam != '') {
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$nam%' ";
        } else {
            $dieukien = " WHERE tinh_trang>0 ";
        }
        if ($phonghoc > 0)
            $dieukien .= " AND phong_hoc=$phonghoc ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien=$giaovien ";
        if ($loai == 1)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc)=1 ";
        else if ($loai == 2)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (2,3,4) ";
        else if ($loai == 3)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (5,6,7) ";
        if ($giohoc != '')
            $dieukien .= " AND gio='$giohoc:00' ";
        if ($tenlop != '')
            $dieukien .= " AND (SELECT name FROM lophoc WHERE id=lop_hoc) LIKE '%$tenlop%' ";
        $query = $this->db->query("SELECT *, (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            DATE_FORMAT(ngay,'%a') AS thu,
            ngay as ngay_hoc,
            (SELECT name FROM giaovien WHERE id=giao_vien) AS giaovien,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc
            FROM lichhoc $dieukien  ORDER BY ngay_hoc, gio");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        $data['rows'] = $rows;
        return $data;
    }

    function gettime($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop)
    {
        $data = array();
        if ($ngay != '') {
            $thangnam = $nam . '-' . $thang . '-' . $ngay;
            $dieukien = " WHERE tinh_trang>0 AND ngay='$thangnam' ";
        } elseif ($thang != '') {
            $thangnam = $nam . '-' . $thang;
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' ";
        } elseif ($nam != '') {
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$nam%' ";
        } else {
            $dieukien = " WHERE tinh_trang>0 ";
        }
        if ($phonghoc > 0)
            $dieukien .= " AND phong_hoc=$phonghoc ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien=$giaovien ";
        if ($loai == 1)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc)=1 ";
        else if ($loai == 2)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (2,3,4) ";
        else if ($loai == 3)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (5,6,7) ";
        if ($giohoc != '')
            $dieukien .= " AND gio='$giohoc:00' ";
        if ($tenlop != '')
            $dieukien .= " AND (SELECT name FROM lophoc WHERE id=lop_hoc) LIKE '%$tenlop%' ";
        $query = $this->db->query("SELECT *, (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            DATE_FORMAT(ngay,'%a') AS thu,
            ngay as ngay_hoc,
            (SELECT name FROM giaovien WHERE id=giao_vien) AS giaovien,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc
            FROM lichhoc $dieukien  ORDER BY ngay_hoc,gio ");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        $data['rows'] = $rows;
        return $data;
    }

    function getduyet($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop)
    {
        $data = array();
        if ($ngay != '') {
            $thangnam = $nam . '-' . $thang . '-' . $ngay;
            $dieukien = " WHERE tinh_trang>0 AND ngay='$thangnam' ";
        } elseif ($thang != '') {
            $thangnam = $nam . '-' . $thang;
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' ";
        } elseif ($nam != '') {
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$nam%' ";
        } else {
            $dieukien = " WHERE tinh_trang > 0 ";
        }
        $dieukien .= " AND thoi_luong_moi > 0 ";
        if ($phonghoc > 0)
            $dieukien .= " AND phong_hoc=$phonghoc ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien=$giaovien ";
        if ($loai == 1)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc)=1 ";
        else if ($loai == 2)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (2,3,4) ";
        else if ($loai == 3)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (5,6,7) ";
        if ($giohoc != '')
            $dieukien .= " AND gio='$giohoc:00' ";
        if ($tenlop != '')
            $dieukien .= " AND (SELECT name FROM lophoc WHERE id=lop_hoc) LIKE '%$tenlop%' ";
        $query = $this->db->query("SELECT *, (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            DATE_FORMAT(ngay,'%a') AS thu,
            ngay as ngay_hoc,
            (SELECT name FROM giaovien WHERE id=giao_vien) AS giaovien,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc
            FROM lichhoc $dieukien  ORDER BY ngay_hoc, gio");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        $data['rows'] = $rows;
        return $data;
    }

    function getMultiLich($lophoc, $ngay)
    {
        $data = array();
        $dieukien = " WHERE tinh_trang IN (1,2,3,4,5,6) AND ngay >= '$ngay' AND lop_hoc=$lophoc";
        $query = $this->db->query("SELECT id,ngay
            FROM lichhoc $dieukien  ORDER BY ngay, gio");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    function getInfoLop($id)
    {
        $data = array();
        $dieukien = " WHERE tinh_trang >0 AND id=$id ";
        $query = $this->db->query("SELECT *
            FROM lophoc $dieukien ");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($rows)
            return $rows;
        else
            return [];
    }

    function BuoiDaHoc($lophoc)
    {
        $dieukien = " WHERE tinh_trang IN (1,2,3,4,5,6)  AND lop_hoc=$lophoc";
        $query = $this->db->query("SELECT COUNT(id) as total 
            FROM lichhoc $dieukien");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($rows)
            return $rows[0]['total'];
        else
            return 0;
    }

    function getlophuy()
    {
        $date = date("Y-m-d");
        $query = $this->db->query("SELECT lop_hoc as id,
       (SELECT name FROM lophoc WHERE id=a.lop_hoc) as name
        FROM lichhoc a WHERE tinh_trang=1 AND ngay<'$date' 
        AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc) IN (1,2,3,4) 
        AND (SELECT phan_loai_2 FROM lophoc WHERE id=a.lop_hoc) IN (1,2)
        AND (SELECT tinh_trang FROM lophoc WHERE id=a.lop_hoc) IN (1,2) GROUP BY lop_hoc");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function huylich($lophoc)
    {
        $ok = false;
        $date = date("Y-m-d");
        $query = $this->db->query("SELECT id
        FROM lichhoc a WHERE tinh_trang=1 AND ngay<'$date' 
        AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc) IN (1,2,3,4) 
        AND (SELECT phan_loai_2 FROM lophoc WHERE id=a.lop_hoc) IN (1,2)
        AND (SELECT tinh_trang FROM lophoc WHERE id=a.lop_hoc) IN (1,2)");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($rows) {
            foreach ($rows as $item) {
                $lichhoc = $item['id'];
                $query = $this->db->query("SELECT *,
                (SELECT hoc_vien FROM saplop WHERE lich_hoc=$lichhoc LIMIT 1) as hocvien,
                (SELECT MAX(ngay) FROM lichhoc WHERE lop_hoc=a.lop_hoc) as ngaycuoi
                FROM lichhoc a WHERE tinh_trang=1 AND id='$lichhoc' ");
                $lichcu = $query->fetchAll(PDO::FETCH_ASSOC);
                if ($lichcu) {
                    $ok = $this->update("lichhoc", ["tinh_trang" => 7], "id=$lichhoc");
                    $this->update("saplop", ["tinh_trang" => 3], "lich_hoc=$lichhoc");
                    $this->update("diemdanh", ["tinh_trang" => 3], "lich_hoc=$lichhoc");
                    $ngaycu = $lichcu[0]['ngay'];
                    $ngaycuoi = $lichcu[0]['ngaycuoi'];
                    $tongngay = (strtotime($ngaycuoi) - strtotime($ngaycu)) / 86400;
                    $ngaychenh = 7 - ($tongngay % 7);
                    $ngaymoi = date("Y-m-d", strtotime("$ngaycuoi +$ngaychenh days"));
                    $data = array(
                        'ngay' => $ngaymoi,
                        'gio' => $lichcu[0]['gio'],
                        'gio_ra' => $lichcu[0]['gio_ra'],
                        'lop_hoc' => $lichcu[0]['lop_hoc'],
                        'giao_vien' => $lichcu[0]['giao_vien'],
                        'phong_hoc' => $lichcu[0]['phong_hoc'],
                        'thoi_luong' => $lichcu[0]['thoi_luong'],
                        'tinh_trang' => 1
                    );
                    if ($this->insert("lichhoc", $data)) {
                        $lichhocid = $this->db->lastInsertId();
                        $saplop = array(
                            'hoc_vien' => $lichcu[0]['hocvien'],
                            'lich_hoc' => $lichhocid,
                            'lop_hoc' => $lichcu[0]['lop_hoc'],
                            'tinh_trang' => 1
                        );
                        if ($this->insert("saplop", $saplop))
                            $ok = true;
                    }
                }
            }
            return $ok;
        } else
            return false;
    }

}

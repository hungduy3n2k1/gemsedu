<?php
class dichvu_model extends model{
    function __construct(){
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows,$loai,$tukhoa){
        $result = array();
        $dieukien = " WHERE tinh_trang=1 ";
        if ($tukhoa != '')
           $dieukien.= " AND name LIKE '%$tukhoa%' ";
        if($loai != 0)
           $dieukien .= " AND phan_loai=$loai ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM dichvu $dieukien ");
        $row = $query->fetchAll();
		    $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *,
             (SELECT name FROM donvitinh WHERE id=don_vi_tinh) AS donvitinh,
             (SELECT name FROM khoahoc WHERE id=khoa_hoc) as khoahoc,
			       (SELECT name FROM loaidichvu WHERE id=phan_loai) as loai
			       FROM dichvu $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

	function delObj($id){
		$data=array('tinh_trang'=>0);
        $query = $this->update("dichvu", $data, "id = $id");
        return $query;
    }

    function updateObj($id, $data){
        $query = $this->update("dichvu", $data, "id = $id");
        return $query;
    }

    function addObj($data){
        $query = $this->insert("dichvu", $data);
        return $query;
    }

}
?>

<?php

class chamcong_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getphep($thang, $nam, $nhanvien)
    {
        $namthang = $nam . '-' . $thang;
        $dieukien = "WHERE nhan_vien=$nhanvien ";
        $query = $this->db->query("SELECT *,
           (SELECT COUNT(1) FROM chamcong $dieukien AND sang=2 AND ngay LIKE '$namthang%') AS phepsang,
           (SELECT COUNT(1) FROM chamcong $dieukien AND chieu=2 AND ngay LIKE '$namthang%') AS phepchieu,
           (SELECT COUNT(1) FROM chamcong $dieukien AND sang=8 AND ngay LIKE '$namthang%') AS khongsang,
           (SELECT COUNT(1) FROM chamcong $dieukien AND chieu=8 AND ngay LIKE '$namthang%') AS khongchieu,
           (SELECT COUNT(1) FROM chamcong $dieukien AND sang=10 AND ngay LIKE '$namthang%') AS phatsang,
           (SELECT COUNT(1) FROM chamcong $dieukien AND chieu=10 AND ngay LIKE '$namthang%') AS phatchieu,
           (SELECT COUNT(1) FROM chamcong $dieukien AND sang=2 AND ngay LIKE '$nam%') AS phepnamsang,
           (SELECT COUNT(1) FROM chamcong $dieukien AND chieu=2 AND ngay LIKE '$nam%') AS phepnamchieu,
           (SELECT COUNT(1) FROM chamcong $dieukien AND sang=6 AND ngay LIKE '$nam%') AS busang,
           (SELECT COUNT(1) FROM chamcong $dieukien AND chieu=6 AND ngay LIKE '$nam%') AS buchieu
           FROM phep WHERE tinh_trang=1 AND nam='$nam' AND thang='$thang' AND nhan_vien=$nhanvien  ");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        return isset($rows[0]) ? $rows[0] : array();
    }

    function getFetObj($nhanvien, $thang, $nam)
    {
        $data = array();
        $query = $this->db->query("SELECT COUNT(1) AS total,ca FROM nhanvien WHERE id=$nhanvien AND van_phong>0 AND ca>0");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp[0]['total'] == 1) {
            if ($temp[0]['ca'] == 4) {
                define('GIOVAO', '13:30:00');
                define('MUONSANG', date("H:i:s", strtotime(GIOVAO) + 3600));
                define('NGHITRUA', '18:00:00');
                define('SOMSANG', date("H:i:s", strtotime(NGHITRUA) - 3600));
                define('GIOCHIEU', '18:30:00');
                define('MUONCHIEU', date("H:i:s", strtotime(GIOCHIEU) + 3600));
                define('GIORA', '21:15:00');
                define('SOMCHIEU', date("H:i:s", strtotime(GIORA) - 3600));
            }
            $thangnam = $nam . '-' . $thang;
            $today = date('Y-m-d');
            $dieukien = " WHERE tinh_trang=0 AND nhan_vien=$nhanvien AND ngay<'$today' AND ngay LIKE '$thangnam%' AND ca_vao>0 AND ca_ra>0 ";
            $query = $this->db->query("SELECT * FROM chamcong $dieukien  ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($temp as $item) {
                $id = $item['id'];
                $sang = $item['sang'];
                $chieu = $item['chieu'];
                $giovao = $item['gio_vao'];
                $giora = $item['gio_ra'];
                $cavao = $item['ca_vao'];
                $cara = $item['ca_ra'];
                //echo GIORA;
                if (($cavao == GIOVAO) && ($cara == NGHITRUA)) // Chấm công ca sáng
                    if ($sang == 0)
                        if (($giovao < GIOVAO) && ($giora > NGHITRUA))
                            $sang = 1;
                        elseif (($giovao > MUONSANG) || ($giora < SOMSANG))
                            $sang = 10; //nghỉ không báo
                        else
                            $sang = 11; //đi muộn hoặc về sớm
                if (($cavao == GIOCHIEU) && ($cara == GIORA)) // Chấm công ca chiều
                    if ($chieu == 0)
                        if (($giovao < GIOCHIEU) && ($giora > GIORA))
                            $chieu = 1;
                        elseif (($giovao > MUONCHIEU) || ($giora < SOMCHIEU))
                            $chieu = 10; //nghỉ không báo
                        else
                            $chieu = 11; //đi muộn hoặc về sớm
                if (($cavao == GIOVAO) && ($cara == GIORA)) {// Chấm công $fulltime
                    if ($sang == 0)
                        if (($giovao < GIOVAO) && ($giora > NGHITRUA))
                            $sang = 1;
                        elseif (($giovao > MUONSANG) || ($giora < SOMSANG))
                            $sang = 10; //nghỉ không báo
                        else
                            $sang = 11; //đi muộn hoặc về sớm
                    if ($chieu == 0)
                        if (($giovao < GIOCHIEU) && ($giora > GIORA))
                            $chieu = 1;
                        elseif (($giovao > MUONCHIEU) || ($giora < SOMCHIEU))
                            $chieu = 10; //nghỉ không báo
                        else
                            $chieu = 11; //đi muộn hoặc về sớm
                }
                $data = array('tinh_trang' => 1, 'sang' => $sang, 'chieu' => $chieu);
                $ok = $this->update("chamcong", $data, " id=$id ");
            }
            $firstday = date("N", strtotime('first day of this month', strtotime($thangnam)));
            $lastday = date("N", strtotime('last day of this month', strtotime($thangnam)));
            $endmonth = date("d", strtotime('last day of this month', strtotime($thangnam)));
            $dieukien = " WHERE nhan_vien=$nhanvien AND ngay LIKE '" . $thangnam . "%' ";
            $query = $this->db->query("SELECT *, (SELECT ky_hieu FROM cong WHERE id=sang) AS sang,
                (SELECT ky_hieu FROM cong WHERE id=chieu) AS chieu
                FROM chamcong $dieukien ORDER BY ngay");
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            for ($i = 1; $i <= $endmonth; $i++) {
                $temp[$i] = '<div style="font-weight:bold;">' . $i . '</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div>';
                foreach ($rows AS $key => $row) {
                    $ngay = (int)substr($row['ngay'], -2);
                    $giovao = ($row['gio_vao'] == '00:00:00') ? ' ' : $row['gio_vao'] . ' vào';
                    $giora = ($row['gio_ra'] == '00:00:00') ? ' ' : $row['gio_ra'] . ' ra';
                    // $duyet = ($row['tinh_trang']==1)?'&#9733;':'&#9734;';
                    $duyet = ($row['tinh_trang'] == 1) ? '' : '(*)';
                    if ($ngay == $i)
                        $temp[$i] = '<div style="font-weight:bold;">' . $i . '</div>
                          <div style="color:#1a75ba; font-weight:bold; padding:2px">' . $giovao . '</div>
                          <div style="color:#1a75ba; font-weight:bold; padding:2px">' . $giora . '</div>
                          <div style="color:red;font-weight:bold;">' . $row['sang'] . $row['chieu'] . ' ' . $duyet . '</div>';
                }
            }
            $bangchamcong = array();
            for ($i = 0; $i < 6; $i++) {
                if ($i == 0)
                    for ($j = $firstday; $j < 8; $j++)
                        $bangchamcong[$i][$j] = isset($temp[$j - $firstday + 1]) ? $temp[$j - $firstday + 1] : '';
                elseif ($i < 5)
                    for ($j = 1; $j < 8; $j++)
                        $bangchamcong[$i][$j] = isset($temp[$j + 7 * $i - $firstday + 1]) ? $temp[$j + 7 * $i - $firstday + 1] : '';
                else
                    for ($j = 1; $j <= $lastday; $j++)
                        $bangchamcong[$i][$j] = isset($temp[$j + 7 * $i - $firstday + 1]) ? $temp[$j + 7 * $i - $firstday + 1] : '';
            }
            $data['rows'] = $bangchamcong;
            $data['footer'] = array(0 => array('1' => 'X: đủ công, P: phép', '2' => 'C: công tác, L: nghỉ lễ', '3' => 'T: tết, B: nghỉ bù', '4' => 'V: nghỉ có lương', '5' => 'K: nghỉ không lương', '6' => 'O: ốm, -: không báo', '7' => 'M: muộn sớm, Q: quên chấm'));
        }
        return $data;
    }

    function chamcongtay($data, $apdung)
    {
        $query = false;
        $ngay = $data['ngay'];
        if ($apdung == 1) {
            $nhanvien = $data['nhan_vien'];
            $ca = $this->ca($nhanvien, $ngay);
            $data['ca_vao'] = $ca['vao'];
            $data['ca_ra'] = $ca['ra'];
            $query = $this->db->query("SELECT id FROM chamcong WHERE nhan_vien=$nhanvien AND ngay='$ngay' ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            if (isset($temp[0]['id'])) {
                $id = $temp[0]['id'];
                $query = $this->update("chamcong", $data, "id = $id");
            } else {
                $query = $this->insert("chamcong", $data);
            }
        }
        if ($apdung == 2) {
            $dieukien = " WHERE tinh_trang IN (1,2,3) AND van_phong>0 AND ca>0 ";
            $query = $this->db->query("SELECT id FROM nhanvien $dieukien ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($temp AS $item) {
                $nhanvien = $item['id'];
                $data['nhan_vien'] = $nhanvien;
                $ca = $this->ca($nhanvien, $ngay);
                $data['ca_vao'] = $ca['vao'];
                $data['ca_ra'] = $ca['ra'];
                $query = $this->db->query("SELECT id FROM chamcong WHERE nhan_vien=$nhanvien AND ngay='$ngay' ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                if (isset($temp[0]['id'])) {
                    $id = $temp[0]['id'];
                    $query = $this->update("chamcong", $data, "id = $id");
                } else {
                    $query = $this->insert("chamcong", $data);
                }
            }
        }
        return $query;
    }

    function baonghi($data)
    {
        $query = false;
        $ngay = $data['ngay'];
        $nhanvien = $data['nhan_vien'];
        $ca = $this->ca($nhanvien, $ngay);
        $data['ca_vao'] = $ca['vao'];
        $data['ca_ra'] = $ca['ra'];
        $query = $this->db->query("SELECT id FROM chamcong WHERE nhan_vien=$nhanvien AND ngay='$ngay' ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($temp[0]['id'])) {
            $id = $temp[0]['id'];
            $query = $this->update("chamcong", $data, "id = $id");
        } else {
            $query = $this->insert("chamcong", $data);
        }
        return $query;
    }


    function loaiphep()
    {
        $query = $this->db->query("SELECT id,name FROM cong WHERE tinh_trang=1  ");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    function loainghi()
    {
        $query = $this->db->query("SELECT id,name FROM cong WHERE tinh_trang=1 AND id IN (2,3,6,8,9)  ");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    function checkout()
    {
        $ok = false;
        $nhanvien = $_SESSION['user']['nhan_vien'];
        $ipvanphong = $_SESSION['user']['ip'];
        $iplogin = $_SERVER["REMOTE_ADDR"];
        if ($ipvanphong == $iplogin) {
            $today = date("Y-m-d");
            $where = " nhan_vien = $nhanvien AND ngay = '$today' ";
            $data = ['gio_ra' => date("H:i:s")];
            $ok = $this->update("chamcong", $data, $where);
        }
        return $ok;
    }

    function checkphep($nhanvien, $ngay)
    {
        $ok = false;
        $nam = date('Y', strtotime($ngay));
        $thang = date('m', strtotime($ngay));
        $dieukien = "WHERE nhan_vien=$nhanvien AND ngay LIKE '$nam%'  ";
        $query = $this->db->query("SELECT phep_luy_ke,
             (SELECT COUNT(1) FROM chamcong $dieukien AND sang=2 ) AS phepsang,
             (SELECT COUNT(1) FROM chamcong $dieukien AND chieu=2 ) AS phepchieu
             FROM phep WHERE tinh_trang=1 AND nam='$nam' AND thang='$thang' AND nhan_vien=$nhanvien  ");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($rows[0]['phep_luy_ke']))
            $ok = (($rows[0]['phep_luy_ke'] - ($rows[0]['phepsang'] + $rows[0]['phepchieu']) / 2) >= 0.5);
        return $ok;
    }

    function checkbu($nhanvien, $ngay)
    {
        $ok = false;
        $nam = date('Y', strtotime($ngay));
        $thang = date('m', strtotime($ngay));
        $dieukien = "WHERE nhan_vien=$nhanvien AND ngay LIKE '$nam%'  ";
        $query = $this->db->query("SELECT phep_nam,
             (SELECT COUNT(1) FROM chamcong $dieukien AND sang=6 ) AS phepsang,
             (SELECT COUNT(1) FROM chamcong $dieukien AND chieu=6 ) AS phepchieu
             FROM phep WHERE tinh_trang=1 AND nam='$nam' AND thang='$thang' AND nhan_vien=$nhanvien  ");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($rows[0]['phep_nam']))
            $ok = (($rows[0]['phep_nam'] - ($rows[0]['phepsang'] + $rows[0]['phepchieu']) / 2) >= 0.5);
        return $ok;
    }
}

?>

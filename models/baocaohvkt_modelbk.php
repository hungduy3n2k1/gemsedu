<?php

class baocaohvkt_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj_bk($sort, $order, $offset, $rows, $hocvien, $khachhang, $thang, $nam, $phanloaidh)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc) IN (1,2,3) ";
        if ($hocvien != '')
            $dieukien .= " AND hoc_vien IN (SELECT id FROM hocvien WHERE name LIKE '%$hocvien%') ";
        if ($khachhang != '')
            $dieukien .= " AND hoc_vien IN (SELECT id FROM hocvien WHERE khach_hang IN (SELECT id FROM khachhang WHERE name LIKE '%$khachhang%')) ";
        if ($phanloaidh > 0) {
            $dieukien .= " AND IFNULL((SELECT GROUP_CONCAT(phan_loai_sale) FROM donhang 
                WHERE hoc_vien=saplop.hoc_vien AND tinh_trang > 0 GROUP BY hoc_vien),0) LIKE '%$phanloaidh%' ";
        }
        if ($nam != "" && $thang != "") {
            $ngay = "$nam-$thang";
            $dieukien .= " AND (SELECT MAX(ngay) FROM lichhoc WHERE lop_hoc=saplop.lop_hoc AND tinh_trang IN (1,2,3,4,5,6)) LIKE '$ngay%' ";
            $listhv = '';
            $query = $this->db->query("SELECT hoc_vien,
            IFNULL((SELECT SUM(so_tien) FROM donhang WHERE tinh_trang IN (1,2) 
            AND khach_hang = (SELECT khach_hang FROM hocvien WHERE id=saplop.hoc_vien)),0) as sotien
            FROM saplop $dieukien GROUP BY hoc_vien HAVING sotien>0");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $total = 0;
            foreach ($temp as $item) {
                $listhv .= $item['hoc_vien'] . ",";
                $total++;
            }
            $listhv = rtrim($listhv, ",");
            if ($listhv != '') {
                $result['total'] = $total;
                $sql = "SELECT id,name,
                IF(ngay_sinh!='0000-00-00',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
                (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
                CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id) as mahocvien,
                /*(SELECT DATE_FORMAT(ngay_bat_dau,'%d/%m/%Y') FROM saplop 
                WHERE hoc_vien=a.id AND tinh_trang=1 ORDER BY ngay_bat_dau ASC LIMIT 1) as ngaybatdau,*/
                (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh,
                (SELECT email FROM khachhang WHERE id=a.khach_hang) as emailkh,
                IFNULL((SELECT GROUP_CONCAT(phan_loai_sale) FROM donhang WHERE hoc_vien=a.id AND tinh_trang > 0 GROUP BY hoc_vien),0) as phanloaisale
                FROM hocvien a WHERE id IN ($listhv) ORDER BY $sort $order LIMIT $offset, $rows";
                $query = $this->db->query($sql);
                $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($result['rows'] as $key => $item) {
                    $listlh = '';
                    $idhocvien = $item['id'];
                    $query1 = $this->db->query("SELECT lop_hoc
                    FROM saplop a WHERE tinh_trang>0 AND hoc_vien=$idhocvien");
                    $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($temp as $saplop) {
                        $listlh .= $saplop["lop_hoc"] . ",";
                    }
                    $listlh = rtrim($listlh, ",");
                    $query1 = $this->db->query("SELECT MAX(ngay) as ngayketthuc, MIN(ngay) AS ngaybatdau,giao_vien,lop_hoc
                    FROM lichhoc a WHERE tinh_trang>0 AND lop_hoc IN ($listlh) GROUP BY lop_hoc");
                    $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                    if ($temp) {
                        $result['rows'][$key]['ngaybatdau'] = date("d/m/Y", strtotime($temp[0]['ngaybatdau']));
                        $result['rows'][$key]['ngayketthuc'] = date("d/m/Y", strtotime($temp[0]['ngayketthuc']));
                    } else {
                        $result['rows'][$key]['ngaybatdau'] = '';
                        $result['rows'][$key]['ngayketthuc'] = '';
                    }
                    $giaovien = '';
                    $chuyenmon = '';
                    foreach ($temp as $lichhoc) {
                        $idlop = $lichhoc['lop_hoc'];
                        $query1 = $this->db->query("SELECT (SELECT name FROM giaovien WHERE id=giao_vien) as giaovien,
                        (SELECT name FROM nhanvien WHERE id=phu_trach) as chuyenmon
                         FROM lophoc WHERE tinh_trang>0 AND id=$idlop");
                        $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                        if (isset($temp[0]['giaovien']) && $temp[0]['giaovien'] != '')
                            $giaovien .= $temp[0]['giaovien'] . "<br>";
                        if (isset($temp[0]['chuyenmon']) && $temp[0]['chuyenmon'] != '')
                            $chuyenmon .= $temp[0]['chuyenmon'] . "<br>";
                    }
                    $result['rows'][$key]['giaovien'] = $giaovien;
                    $result['rows'][$key]['chuyenmon'] = $chuyenmon;
                }
            }
        }
        return $result;
    }

    function getFetObj($sort, $order, $offset, $rows, $tenhocvien, $khachhang, $giaovien, $chuyenmon, $thang, $nam, $phanloaidh, $nhanviensale)
    {
        $result = array();
        $result['rows'] = array();
        if ($nam != "" && $thang != "") {
            $ngay = "$nam-$thang";
            $dieukienlich = " WHERE tinh_trang IN (1,2,3,4,5,6) ";
            if ($phanloaidh == 2)
                $dieukienlich .= " AND (SELECT so_buoi FROM lophoc WHERE id=a.lop_hoc) < 18 ";
            if ($phanloaidh == 3) 
                $dieukienlich .= " AND (SELECT so_buoi FROM lophoc WHERE id=a.lop_hoc) >= 18 ";
            if ($giaovien != '') 
                $dieukienlich .= " AND giao_vien = $giaovien ";
            if ($chuyenmon != '')
                $dieukienlich .= " AND (SELECT phu_trach FROM lophoc WHERE id=a.lop_hoc) = $chuyenmon ";
            if ($tenhocvien != '')
                $dieukienlich .= " AND (SELECT name FROM hocvien WHERE id=(SELECT hoc_vien FROM lophoc WHERE id=a.lop_hoc)) LIKE '%$tenhocvien%' ";
            if ($khachhang != '')
                $dieukienlich .= " AND (SELECT name FROM khachhang WHERE id=(SELECT khach_hang FROM hocvien WHERE id=(SELECT hoc_vien FROM lophoc WHERE id=a.lop_hoc))) LIKE '%$khachhang%' ";
            if ($nhanviensale > 0)
                $dieukienlich .= " AND (SELECT nhan_vien_sale FROM donhang WHERE id=(SELECT don_hang FROM lophoc WHERE id=a.lop_hoc)) = $nhanviensale ";
            $query = $this->db->query("SELECT MAX(ngay) AS ngayketthuc,
                (SELECT phan_loai_2 FROM lophoc WHERE id=lop_hoc) AS phan_loai_2,
                IFNULL((SELECT hoc_vien FROM saplop where lich_hoc=a.id),0) as hocvien
                FROM lichhoc a $dieukienlich GROUP BY lop_hoc HAVING MAX(ngay) LIKE '$ngay%' AND phan_loai_2 != 3 AND hocvien>0");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $result['total'] = COUNT($temp);
                
            $query = $this->db->query("SELECT lop_hoc,MAX(ngay) AS ngayketthuc,MIN(ngay) AS ngaybatdau,
            (SELECT so_buoi FROM lophoc WHERE id=lop_hoc) AS sobuoi,
            IFNULL((SELECT hoc_vien FROM saplop where lich_hoc=a.id),0) as hocvien,
            (SELECT phan_loai_2 FROM lophoc WHERE id=lop_hoc) AS phan_loai_2
            FROM lichhoc a $dieukienlich GROUP BY lop_hoc HAVING MAX(ngay) LIKE '$ngay%' AND phan_loai_2 != 3 AND hocvien>0 ORDER BY $sort $order LIMIT $offset, $rows ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $key = 0;
            foreach ($temp as $key => $item) {
                $idlop = $item['lop_hoc'];
                $dieukien = " WHERE tinh_trang>0 AND id=(SELECT hoc_vien
                    FROM lophoc WHERE id=$idlop) ";
                // if ($tenhocvien != '')
                //     $dieukien .= " AND name LIKE '%$tenhocvien%' ";
                // if ($khachhang != '')
                //     $dieukien .= " AND khach_hang IN (SELECT id FROM khachhang WHERE name LIKE '%$khachhang%') ";
                // if ($nhanviensale > 0)
                //     $dieukien .= " AND id IN (SELECT hoc_vien FROM donhang WHERE nhan_vien_sale = $nhanviensale) ";
                $sql = "SELECT id,name,
                IF(ngay_sinh!='0000-00-00',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
                (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
                CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id) as mahocvien,
                /*(SELECT DATE_FORMAT(ngay_bat_dau,'%d/%m/%Y') FROM saplop 
                WHERE hoc_vien=a.id AND tinh_trang=1 ORDER BY ngay_bat_dau ASC LIMIT 1) as ngaybatdau,*/
                (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh,
                (SELECT email FROM khachhang WHERE id=a.khach_hang) as emailkh
                FROM hocvien a $dieukien";
                $query = $this->db->query($sql);
                $hocvien = $query->fetchAll(PDO::FETCH_ASSOC);
                if ($hocvien) {
                    $result['rows'][$key] = $hocvien[0];
                    if ($item['sobuoi'] < 18) {
                        $result['rows'][$key]['phanloaisale'] = "Tái tục trải nghiệm";
                    } else {
                        $result['rows'][$key]['phanloaisale'] = "Tái tục new";
                    }
                    $result['rows'][$key]['ngaybatdau'] = date("d/m/Y", strtotime($item['ngaybatdau']));
                    $result['rows'][$key]['ngayketthuc'] = date("d/m/Y", strtotime($item['ngayketthuc']));
                    $query1 = $this->db->query("SELECT (SELECT name FROM giaovien WHERE id=giao_vien) as giaovien,
                        (SELECT name FROM nhanvien WHERE id=phu_trach) as chuyenmon
                         FROM lophoc WHERE tinh_trang>0 AND id=$idlop");
                    $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                    if (isset($temp[0]['giaovien']) && $temp[0]['giaovien'] != '')
                        $result['rows'][$key]['giaovien'] = $temp[0]['giaovien'];
                    if (isset($temp[0]['chuyenmon']) && $temp[0]['chuyenmon'] != '')
                        $result['rows'][$key]['chuyenmon'] = $temp[0]['chuyenmon'];
                   
                } else {
                    echo $idlop;
                }
            }
        }
        return $result;
    }

    function getFetObjAll($tenhocvien, $khachhang, $giaovien, $chuyenmon, $thang, $nam, $phanloaidh, $nhanviensale)
    {
        $result = array();
        $result['rows'] = array();
        if ($nam != "" && $thang != "") {
            $ngay = "$nam-$thang";
            $dieukienlich = " WHERE tinh_trang IN (1,2,3,4,5,6) ";
            if ($phanloaidh == 2)
                $dieukienlich .= " AND (SELECT so_buoi FROM lophoc WHERE id=a.lop_hoc) < 18 ";
            if ($phanloaidh == 3) 
                $dieukienlich .= " AND (SELECT so_buoi FROM lophoc WHERE id=a.lop_hoc) >= 18 ";
            if ($giaovien != '')
                $dieukienlich .= " AND giao_vien = $giaovien ";
            if ($chuyenmon != '')
                $dieukienlich .= " AND (SELECT phu_trach FROM lophoc WHERE id=a.lop_hoc) = $chuyenmon ";
            if ($tenhocvien != '')
                $dieukienlich .= " AND (SELECT name FROM hocvien WHERE id=(SELECT hoc_vien FROM lophoc WHERE id=a.lop_hoc)) LIKE '%$tenhocvien%' ";
            if ($khachhang != '')
                $dieukienlich .= " AND (SELECT name FROM khachhang WHERE id=(SELECT khach_hang FROM hocvien WHERE id=(SELECT hoc_vien FROM lophoc WHERE id=a.lop_hoc))) LIKE '%$khachhang%' ";
            if ($nhanviensale > 0)
                $dieukienlich .= " AND (SELECT nhan_vien_sale FROM donhang WHERE id=(SELECT don_hang FROM lophoc WHERE id=a.lop_hoc)) = $nhanviensale ";
            $query = $this->db->query("SELECT MAX(ngay) AS ngayketthuc,
                (SELECT phan_loai_2 FROM lophoc WHERE id=lop_hoc) AS phan_loai_2,
                IFNULL((SELECT hoc_vien FROM saplop where lich_hoc=a.id),0) as hocvien
                FROM lichhoc a $dieukienlich GROUP BY lop_hoc HAVING MAX(ngay) LIKE '$ngay%' AND phan_loai_2 != 3 AND hocvien>0");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $result['total'] = COUNT($temp);
                
            $query = $this->db->query("SELECT lop_hoc,MAX(ngay) AS ngayketthuc,MIN(ngay) AS ngaybatdau,
            (SELECT so_buoi FROM lophoc WHERE id=lop_hoc) AS sobuoi,
            IFNULL((SELECT hoc_vien FROM saplop where lich_hoc=a.id),0) as hocvien,
            (SELECT phan_loai_2 FROM lophoc WHERE id=lop_hoc) AS phan_loai_2
            FROM lichhoc a $dieukienlich GROUP BY lop_hoc HAVING MAX(ngay) LIKE '$ngay%' AND phan_loai_2 != 3 AND hocvien>0 ORDER BY ngayketthuc DESC");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $key = 0;
            foreach ($temp as $key => $item) {
                $idlop = $item['lop_hoc'];
                $dieukien = " WHERE tinh_trang>0 AND id=(SELECT hoc_vien
                    FROM lophoc WHERE id=$idlop) ";
                // if ($tenhocvien != '')
                //     $dieukien .= " AND name LIKE '%$tenhocvien%' ";
                // if ($khachhang != '')
                //     $dieukien .= " AND khach_hang IN (SELECT id FROM khachhang WHERE name LIKE '%$khachhang%') ";
                // if ($nhanviensale > 0)
                //     $dieukien .= " AND id IN (SELECT hoc_vien FROM donhang WHERE nhan_vien_sale = $nhanviensale) ";
                $sql = "SELECT id,name,
                IF(ngay_sinh!='0000-00-00',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
                (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
                CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id) as mahocvien,
                /*(SELECT DATE_FORMAT(ngay_bat_dau,'%d/%m/%Y') FROM saplop 
                WHERE hoc_vien=a.id AND tinh_trang=1 ORDER BY ngay_bat_dau ASC LIMIT 1) as ngaybatdau,*/
                (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh,
                (SELECT email FROM khachhang WHERE id=a.khach_hang) as emailkh
                FROM hocvien a $dieukien";
                $query = $this->db->query($sql);
                $hocvien = $query->fetchAll(PDO::FETCH_ASSOC);
                if ($hocvien) {
                    $result['rows'][$key] = $hocvien[0];
                    if ($item['sobuoi'] < 18) {
                        $result['rows'][$key]['phanloaisale'] = "Tái tục trải nghiệm";
                    } else {
                        $result['rows'][$key]['phanloaisale'] = "Tái tục new";
                    }
                    $result['rows'][$key]['ngaybatdau'] = date("d/m/Y", strtotime($item['ngaybatdau']));
                    $result['rows'][$key]['ngayketthuc'] = date("d/m/Y", strtotime($item['ngayketthuc']));
                    $query1 = $this->db->query("SELECT (SELECT name FROM giaovien WHERE id=giao_vien) as giaovien,
                        (SELECT name FROM nhanvien WHERE id=phu_trach) as chuyenmon
                         FROM lophoc WHERE tinh_trang>0 AND id=$idlop");
                    $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                    if (isset($temp[0]['giaovien']) && $temp[0]['giaovien'] != '')
                        $result['rows'][$key]['giaovien'] = $temp[0]['giaovien'];
                    if (isset($temp[0]['chuyenmon']) && $temp[0]['chuyenmon'] != '')
                        $result['rows'][$key]['chuyenmon'] = $temp[0]['chuyenmon'];
                   
                } else {
                    echo $idlop;
                }
            }
        }
        return $result;
    }

}

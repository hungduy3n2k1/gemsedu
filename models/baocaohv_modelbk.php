<?php

class baocaohv_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $hocvien, $khachhang, $giaovien, $chuyenmon)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc) IN (1,2) ";
        if ($hocvien != '')
            $dieukien .= " AND hoc_vien IN (SELECT id FROM hocvien WHERE name LIKE '%$hocvien%') ";
        if ($khachhang != '')
            $dieukien .= " AND hoc_vien IN (SELECT id FROM hocvien WHERE khach_hang IN (SELECT id FROM khachhang WHERE name LIKE '%$khachhang%')) ";
        if ($giaovien != '')
            $dieukien .= " AND (SELECT giao_vien FROM lichhoc WHERE id=lich_hoc) = $giaovien ";
        if ($chuyenmon != '')
            $dieukien .= " AND (SELECT phu_trach FROM lophoc WHERE id=lop_hoc ) = $chuyenmon ";
        // if ($tungay != '')
        //     $dieukien .= " AND (SELECT ngay FROM lichhoc WHERE id=lich_hoc)>='$tungay' ";
        // if ($denngay != '')
        //     $dieukien .= " AND (SELECT ngay FROM lichhoc WHERE id=lich_hoc)<='$denngay' ";
        $listlh = '';
        $query = $this->db->query("SELECT lop_hoc,hoc_vien,lich_hoc, 
        IFNULL((SELECT SUM(so_tien) FROM donhang WHERE tinh_trang IN (1,2) 
        AND khach_hang = (SELECT khach_hang FROM hocvien WHERE id=saplop.hoc_vien)),0) as sotien
        -- IFNULL((SELECT so_tien FROM donhang WHERE tinh_trang IN (1,2) 
        -- AND id=(SELECT don_hang FROM lophoc WHERE id=saplop.lop_hoc)),0) as sotien
        FROM saplop $dieukien GROUP BY lop_hoc HAVING sotien>0 ");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $total = 0;
        foreach ($result['rows'] as $key => $item) {
            $idlh = $item['lop_hoc'];
            $idhv = $item['hoc_vien'];
            $query1 = $this->db->query("SELECT (so_buoi+buoi_khuyen_mai) as tongbuoi,
            (SELECT name FROM giaovien WHERE id=giao_vien) as giaovien,
            (SELECT name FROM nhanvien WHERE id=phu_trach) as chuyenmon
            FROM lophoc WHERE id = $idlh ORDER BY $sort $order LIMIT $offset, $rows  ");
            $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
            $result['rows'][$key]['tongbuoi'] = $temp[0]['tongbuoi'];
            $result['rows'][$key]['giaovien'] = $temp[0]['giaovien'];
            $result['rows'][$key]['chuyenmon'] = $temp[0]['chuyenmon'];
            $query1 = $this->db->query("SELECT MAX(ngay) as ngayketthuc, MIN(ngay) AS ngaybatdau
            FROM lichhoc WHERE lop_hoc = $idlh AND tinh_trang IN (1,2,3,4,5,6) ");
            $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
            $result['rows'][$key]['ngayketthuc'] = $temp[0]['ngayketthuc'];
            $result['rows'][$key]['ngaybatdau'] = $temp[0]['ngaybatdau'];

            $query1 = $this->db->query("SELECT MAX(ngay) as ngayketthuc, MIN(ngay) AS ngaybatdau, count(id) as dahoc
            FROM lichhoc WHERE lop_hoc = $idlh AND tinh_trang IN (2,3,4,5,6) ");
            $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
            $result['rows'][$key]['dahoc'] = $temp[0]['dahoc'];
            $result['rows'][$key]['conlai'] = $result['rows'][$key]['tongbuoi'] - $result['rows'][$key]['dahoc'] = $temp[0]['dahoc'];
        
            $query1 = $this->db->query("SELECT name,
            IF(ngay_sinh!='0000-00-00',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
            (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
            CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id) as mahocvien,
            (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh,
            (SELECT email FROM khachhang WHERE id=a.khach_hang) as emailkh
            FROM hocvien a WHERE id = $idhv ");
            $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
            $result['rows'][$key]['name'] = $temp[0]['name'];
            $result['rows'][$key]['ngaysinh'] = $temp[0]['ngaysinh'];
            $result['rows'][$key]['khachhang'] = $temp[0]['khachhang'];
            $result['rows'][$key]['mahocvien'] = $temp[0]['mahocvien'];
            $result['rows'][$key]['dienthoaikh'] = $temp[0]['dienthoaikh'];
            $result['rows'][$key]['emailkh'] = $temp[0]['emailkh'];
            $total++;
        }
        $result['total'] = $total;
        
        return $result;
    }

}

?>

<?php

class baocaocall_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $nhanvien, $ngaybd, $ngaykt)
    {
        $result = array();
        $dieukien = "";
        $result['rows'] = array();
        if ($nhanvien > 0)
            $dieukien .= " AND nhan_vien=$nhanvien ";
        if ($ngaybd != "")
            $dieukien .= " AND ngay_chia>='$ngaybd' ";
        if ($ngaykt != "")
            $dieukien .= " AND ngay_chia<='$ngaykt 23:59:59' ";
        $query = $this->db->query("SELECT COUNT(1) AS total FROM 
        (SELECT id FROM `data`  WHERE tinh_trang>0 AND tinh_trang!=8 $dieukien GROUP BY  nhan_vien) a");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT nhan_vien,
        (SELECT name FROM nhanvien WHERE id=a.nhan_vien) AS nguoigoi
        FROM `data` a WHERE tinh_trang>0 AND tinh_trang!=8 $dieukien  GROUP BY nhan_vien LIMIT $offset, $rows");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $tong1 = 0;
        $dagoi1 = 0;
        $thuebao1 = 0;
        $khongnghemay1 = 0;
        $khongquantam1 = 0;
        $chamsoctiep1 = 0;
        $dachot1 = 0;
        foreach ($temp as $key => $item) {
            $nhanvien = $item['nhan_vien'];
            $nguoigoi = $item['nguoigoi'];
            $query = $this->db->query("SELECT tinh_trang FROM `data` WHERE tinh_trang>0 AND tinh_trang!=8 AND  nhan_vien=$nhanvien $dieukien");
            $cuocgoi = $query->fetchAll(PDO::FETCH_ASSOC);
            $tong = 0;
            $dagoi = 0;
            $thuebao = 0;
            $khongnghemay = 0;
            $khongquantam = 0;
            $chamsoctiep = 0;
            $dachot = 0;
            foreach ($cuocgoi as $t) {
                $tong++;
               
                if($t['tinh_trang']>2)
                    $dagoi++;
                    
                if($t['tinh_trang']==3)
                    $thuebao++;
                    
                if($t['tinh_trang']==4)
                    $khongnghemay++;
                    
                if($t['tinh_trang']==5)
                    $khongquantam++;
                    
                if($t['tinh_trang']==6)
                    $chamsoctiep++;
                    
                if($t['tinh_trang']==7)
                    $dachot++;
                    
            }
            $tong1+=$tong;
            $dagoi1+=$dagoi;
            $thuebao1+=$thuebao;
            $khongnghemay1+=$khongnghemay;
            $khongquantam1+=$khongquantam;
            $chamsoctiep1+=$chamsoctiep;
            $dachot1+=$dachot;
            
            $result['rows'][$key]['nguoigoi']=$nguoigoi;
            $result['rows'][$key]['tong']=$tong;
            $result['rows'][$key]['dagoi']=$dagoi;
            $result['rows'][$key]['thuebao']=$thuebao;
            $result['rows'][$key]['khongnghemay']=$khongnghemay;
            $result['rows'][$key]['khongquantam']=$khongquantam;
            $result['rows'][$key]['chamsoctiep']=$chamsoctiep;
            $result['rows'][$key]['dachot']=$dachot;
        }
            
        $result['footer'] = array(0 => array('nguoigoi' => 'Tổng cộng', 'tong' => $tong1, 'dagoi' => $dagoi1, 'thuebao' => $thuebao1, 'khongnghemay' => $khongnghemay1, 'khongquantam' => $khongquantam1, 'chamsoctiep' => $chamsoctiep1, 'dachot' => $dachot1));
        return $result;
    }
}

?>

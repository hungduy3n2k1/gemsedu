<?php
class lichdaythay_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $giaovien, $tenlop)
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang > 0 ";
        if ($giaovien > 0)
            $dieukien .= " AND giao_vien = $giaovien ";
        if ($tenlop != '')
            $dieukien .= " AND lich_hoc IN (SELECT id FROM lichhoc WHERE lop_hoc IN (SELECT id FROM lophoc WHERE name LIKE '%$tenlop%')) ";
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM daythay $dieukien ");
        $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *,
        (SELECT name FROM giaovien WHERE id = a.giao_vien) AS giaovien,
        (SELECT name FROM giaovien WHERE id = a.gv_daythay) AS gvdaythay,
        (SELECT name FROM lophoc WHERE id = (SELECT lop_hoc FROM lichhoc WHERE id = a.lich_hoc)) AS lophoc,
        (SELECT DATE_FORMAT(ngay,'%d/%m/%Y') FROM lichhoc WHERE id = a.lich_hoc) AS ngay,
        (SELECT DATE_FORMAT(gio,'%H:%i') FROM lichhoc WHERE id = a.lich_hoc) AS gio,
        (SELECT name FROM hocvien WHERE id = (SELECT hoc_vien FROM saplop WHERE id = a.lich_hoc)) AS hocvien
        FROM daythay a $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function updateObj($data)
    {
        $ok = false;
        $rows = json_decode($data, true);
        foreach ($rows as $row) {
            $id = $row['id'];
            $update = ['tinh_trang' => 2];
            $ok = $this->update("daythay", $update, "id=$id");
        }
        return $ok;
    }

    function updateLichHoc($data)
    {
        $ok = false;
        $rows = json_decode($data, true);
        foreach ($rows as $row) {
            $id = $row['lich_hoc'];
            $gvdaythay = $row['gv_daythay'];
            $update = ['giao_vien' => $gvdaythay];
            $ok = $this->update("lichhoc", $update, "id=$id");
        }
        return $ok;
    }

}

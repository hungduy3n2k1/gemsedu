<?php
class thongbao_model extends model{
    function __construct(){
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows){
        $result = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM thongbao $dieukien ");
        $row = $query->fetchAll();
		    $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *,
            DATE_FORMAT(ngay_gio,'%d/%m/%Y %H:%i:%s') AS ngaygio,
            DATE_FORMAT(ket_thuc,'%d/%m/%Y %H:%i:%s') AS ketthuc
         FROM thongbao $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

	function delObj($id){
		$data=array('tinh_trang'=>0);
        $query = $this->update("thongbao", $data, "id = $id");
        return $query;
    }

    function updateObj($id, $data){
        $query = $this->update("thongbao", $data, "id = $id");
        return $query;
    }

    function addObj($data){
        $query = $this->insert("thongbao", $data);
        return $query;
    }

}
?>

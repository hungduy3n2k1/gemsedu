<?php

class common_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function thanhpho()
    {
        $query = $this->db->query("SELECT * FROM thanhpho WHERE tinh_trang=1 ");
        if ($query)
            return $query->fetchAll(PDO::FETCH_ASSOC);
        else
            return array();
    }

    function hopdong()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM hopdong WHERE tinh_trang=1  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function donvitinh()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM donvitinh WHERE tinh_trang=1  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function phongban()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM phongban WHERE tinh_trang=1  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function chinhanh()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM chinhanh WHERE tinh_trang=1  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function calamviec()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, ca FROM ca WHERE tinh_trang=1  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function trinhdo()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM trinhdo WHERE tinh_trang=1  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function dichvu()
    {
        $temp = array();
        $query = $this->db->query("SELECT *, (SELECT name FROM donvitinh WHERE id=don_vi_tinh) AS donvitinh
            FROM dichvu WHERE tinh_trang=1 ORDER BY phan_loai  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function loaidichvu()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM  loaidichvu WHERE tinh_trang=1 ORDER BY name ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function taikhoan()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM taikhoan WHERE tinh_trang=1  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function khachhang()
    {
        $temp = array();
        $dieukien = " WHERE tinh_trang>0 AND tinh_trang<5 ";
        $query = $this->db->query("SELECT id, CONCAT(name,' (',dien_thoai,')') AS name,dien_thoai FROM khachhang $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function nhacungcap()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM nhacungcap WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function linhvuc()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM linhvuc WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function nguondata()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM nguondata WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function lienhe()  //cho đối tác, nhà cung cấp
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM lienhe WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function contact() // cho khách hàng
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM contact WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function sukien()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name, loai, so_tien FROM noiquy WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }


    function menu()
    {
        $result = array();
        $query = $this->db->query("SELECT id,name,name as text,parentid FROM menu WHERE active=1 ORDER BY thu_tu ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['rows'] = $this->buildTree($temp, 0);
        return $result;
    }

    function folder()
    {
        $result = array();
        $userid = $_SESSION['user']['id'];
        if ($_SESSION['user']['id'] == 1) {
            $dieukien = " WHERE tinh_trang=1 ";
            $query = $this->db->query("SELECT id,name,name as text,parentid,folder,thu_tu,
            (SELECT folder FROM folders WHERE id=a.parentid) AS thumuccha
            FROM folders a $dieukien ORDER BY parentid,thu_tu ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $result['rows'] = $this->buildTree($temp, 0);
        } else {
            $qr = $this->db->query("SELECT thu_muc FROM users WHERE id=$userid");
            $t = $qr->fetchAll(PDO::FETCH_ASSOC);
            $f = $t[0]['thu_muc'];
            if ($f != '') {
                $f2 = $f;
                $f1 = $f;
                do {
                    $qr1 = $this->db->query(" SELECT GROUP_CONCAT(DISTINCT parentid SEPARATOR ',') as parid FROM folders
            WHERE id IN ($f1) AND
            parentid NOT IN ($f1) ");
                    $t1 = $qr1->fetchAll(PDO::FETCH_ASSOC);
                    $f2 = $t1[0]['parid'];
                    if ($f2 != '')
                        $f1 .= ',' . $f2;
                } while ($f2 != '');
                $dieukien = " WHERE tinh_trang=1 AND id IN ($f1)";
                $query = $this->db->query("SELECT id,name,name as text,parentid,folder,thu_tu,
            (SELECT folder FROM folders WHERE id=a.parentid) AS thumuccha
            FROM folders a $dieukien ORDER BY parentid,thu_tu ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                $result['rows'] = $this->buildTree($temp, 0);
            }
        }
        return $result;
    }

    function folderphanquyen()
    {
        $result = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query = $this->db->query("SELECT id,name,name as text,parentid,folder,
        (SELECT folder FROM folders WHERE id=a.parentid) AS thumuccha
        FROM folders a $dieukien ORDER BY thu_tu ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['rows'] = $this->buildTree($temp, 0);
        return $result;
    }

    function buildTree(array $elements, $parentId = 0)
    {
        $branch = array();
        foreach ($elements as $element) {
            if ($element['parentid'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

    function nhanvien()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name, dien_thoai FROM nhanvien WHERE tinh_trang IN (1,2,3,4) ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        // array_push($temp,array('id'=>0,'name'=>'Tất cả','dien_thoai'=>''));
        return $temp;
    }

    function noiquy()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM noiquy WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function phanloai()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM phanloai WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function loaikh()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM loaikh WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function loaidonhang()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM loaidonhang WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function duan()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM duan WHERE tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function bieumau($loai)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang=1 ";
        if ($loai > 0)
            $dieukien .= " AND phan_loai=$loai ";
        $query = $this->db->query("SELECT * FROM bieumau $dieukien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;

    }

    function khoahoc()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM khoahoc WHERE tinh_trang=1  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function giaotrinh()
    {
        $query = $this->db->query("SELECT id,name FROM giaotrinh WHERE tinh_trang=1  ");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    function hocvien()
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 ";
            $dieukien .= " AND phan_loai NOT IN (15,16,18,19,20) ";
        $query = $this->db->query("SELECT *
            FROM hocvien $dieukien ORDER BY id DESC");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function khoahoctn()
    {
        $temp = array();
        $query = $this->db->query("SELECT id,name FROM khoahoc WHERE tinh_trang=1 AND don_gia=0 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function lophoctn()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM lophoc WHERE tinh_trang IN (1,2) AND phan_loai IN (2,3,4) ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function lophoc()
    {
        $temp = array();
        $query = $this->db->query("SELECT * FROM lophoc WHERE tinh_trang>0 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function phonghoc()
    {
        $temp = array();
        $query = $this->db->query("SELECT id,name FROM phonghoc WHERE tinh_trang>0 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function giaovien()
    {
        $query = $this->db->query("SELECT id,CONCAT(name,'-',id) as name FROM giaovien WHERE tinh_trang=1  ");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    function trogiang()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name, dien_thoai FROM nhanvien WHERE tinh_trang >0 AND phong_ban=14 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        // array_push($temp,array('id'=>0,'name'=>'Tất cả','dien_thoai'=>''));
        return $temp;
    }

}

?>

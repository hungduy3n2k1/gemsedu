<?php

class donhang_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tinhtrang, $khachhang,$hocvien,$loaidh)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang > 0  ";
        if ($tinhtrang > 0)
            $dieukien .= " AND tinh_trang=$tinhtrang  ";
        if ($khachhang > 0)
            $dieukien .= " AND khach_hang=$khachhang ";
        if ($loaidh > 0)
            $dieukien .= " AND loaidh=$loaidh ";
        if ($hocvien > 0)
            $dieukien .= " AND khach_hang=(SELECT khach_hang FROM hocvien WHERE tinh_trang>0 AND id=$hocvien ) ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM donhang $dieukien");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = isset($row[0]['total']) ? $row[0]['total'] : 0;
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay_dang_ky,'%d/%m/%Y') AS ngay,
           (so_buoi+khuyen_mai+bao_luu-da_hoc) AS conlai,
           (SELECT name FROM khachhang WHERE id=khach_hang) AS khachhang,
           (SELECT loai FROM khachhang WHERE id=khach_hang) AS loaikh,
           (SELECT name FROM hocvien WHERE id=hoc_vien) AS hocvien,
           (SELECT count(id) FROM invoice WHERE don_hang=donhang.id AND tinh_trang=3) as dathanhtoan,
           (SELECT name FROM khoahoc WHERE id=product) AS sanpham
           FROM donhang $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        // $query           = $this->db->query("SELECT ROUND(SUM(don_gia/thoi_han),0) AS total FROM donhang $dieukien ");
        // $temp             = $query->fetchAll(PDO::FETCH_ASSOC);
        // $doanhthu = isset($temp[0]['total'])?$temp[0]['total']:0;
        // $result['footer'] =array(0=>array("link"=>"Tổng cộng:","so_tien"=>0,"doanhthu"=>$doanhthu));
        return $result;
    }

    function delObj($id)
    {
        $data = array('tinh_trang' => 0);
        $query = $this->update("donhang", $data, "id = $id");
        $query = $this->update("invoice", $data, "don_hang   = $id");
        return $query;
    }

    function gethocvien($khachhang)
    {
        $query = $this->db->query("SELECT id, name FROM hocvien WHERE khach_hang=$khachhang ");
        if ($query)
            return $query->fetchAll(PDO::FETCH_ASSOC);
        else
            return [];
    }

    function getsobuoi($khoahoc)
    {
        $arr = array('success' => false);
        $query = $this->db->query("SELECT so_buoi,don_gia FROM khoahoc WHERE id=$khoahoc ");
        if ($query) {
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $arr = array('success' => true, 'sobuoi' => $temp[0]['so_buoi'], 'sotien' => $temp[0]['don_gia']);
        }
        return $arr;
    }

    function addkhachhang($khachhang, $sdt, $loai, $ngaydangky)
    {
        if ($sdt != '') {
            $query = $this->db->query("SELECT id FROM khachhang WHERE dien_thoai='$sdt' ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            if (isset($temp[0]['id']))
                $id = $temp[0]['id'];
            else
                $id = 0;
        } else {
            $id = 0;
        }
        if ($id == 0) {
            $data = [
                'ngay' => $ngaydangky,
                'name' => $khachhang,
                'dia_chi' => '',
                'dien_thoai' => $sdt,
                'loai' => $loai,
                'tinh_trang' => 1
            ];
            $query = $this->insert("khachhang", $data);
            if ($query)
                $id = $this->db->lastInsertId();
            else
                $id = 0;
        }
        return $id;
    }

    function addhocvien($khachhang, $hocvien, $loai, $tinhtrang)
    {
        if ($loai == 1)
            $loaihv = 14;
        else
            $loaihv = 13;
        $data = [
            'name' => $hocvien,
            'e_name' => $hocvien,
            'khach_hang' => $khachhang,
            'phan_loai' => $loaihv,
            'tinh_trang' => $tinhtrang
        ];
        $query = $this->insert("hocvien", $data);
        if ($query)
            $id = $this->db->lastInsertId();
        else
            $id = 0;
        return $id;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("donhang", $data, "id = $id");
        return $query;
    }

    function addObj($data)
    {
        $query = $this->insert("donhang", $data);
        if ($query)
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function ThemInvoice($data)
    {
        $query = $this->insert("invoice", $data);
        if ($query)
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function delInvoice($id)
    {
        $data = array('tinh_trang' => 0);
        $query = $this->update("invoice", $data, "don_hang = $id");
        return $query;
    }

    function ThemLop($data)
    {
        $query = $this->insert("lophoc", $data);
        if ($query)
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function getkhachhang($id)
    {
        $dieukien = " WHERE id=$id ";
        $query = $this->db->query("SELECT name, ma_so, dien_thoai, email, van_phong
           FROM khachhang $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp[0];
    }

    function checkLop($name)
    {
        $dieukien = " WHERE tinh_trang>0 AND name='$name' ";
        $query = $this->db->query("SELECT COUNT(1) AS total FROM lophoc  $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp && $temp[0]['total'] > 0)
            return false;
        else
            return true;
    }

    function getGiaoVien($name)
    {
        $dieukien = " WHERE tinh_trang=1 AND name='$name' ";
        $query = $this->db->query("SELECT id FROM giaovien $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp)
            return $temp[0]['id'];
        else
            return 0;
    }

    function getTenGT($name)
    {
        $dieukien = " WHERE tinh_trang=1 AND name='$name' ";
        $query = $this->db->query("SELECT id FROM giaotrinh $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp)
            return $temp[0]['id'];
        else
            return 0;
    }

    function getDotTT($donhangid)
    {
        $dieukien = " WHERE don_hang=$donhangid AND tinh_trang > 0 ";
        $query = $this->db->query("SELECT so_tien, DATE_FORMAT(ngay,'%d/%m/%Y') as ngaythanhtoan
           FROM invoice $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }


}

?>

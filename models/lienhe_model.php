<?php
class lienhe_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tukhoa)
    {
        $result = [];
        $dieukien = " WHERE tinh_trang>0 ";
        if ($tukhoa!='')
            $dieukien.= " AND (name LIKE '%".$tukhoa."%' OR dien_thoai LIKE '%".$tukhoa."%' OR email LIKE '%".$tukhoa."%' ) ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM lienhe $dieukien ");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *,
      			(SELECT name FROM nhacungcap WHERE id=khach_hang) AS doitac
      			FROM lienhe $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // function get_detail($lienhe, $sort, $order, $offset, $rows)
    // {
    //     $result = [];
    //     $dieukien = " WHERE khach_hang=$lienhe ";
    //     $query = $this->db->query("SELECT COUNT(*) AS total FROM donhang $dieukien");
    //     $row = $query->fetchAll();
    //     $result['total'] = $row[0]['total'];
    //     $query = $this->db->query("SELECT *
		// 	         FROM donhang $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
    //     $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }
    function updateObj($id, $data)
    {
        $query = $this->update("lienhe", $data, "id = $id");
        return $query;
    }

    function delObj($id)
    {
        $data = ['tinh_trang' => 0];
        $query = $this->update("lienhe", $data, "id = $id ");
        if ($query) {
           $data = array(
               'ngay_gio'=>date("Y-m-d H:i:s"),
               'user' => $_SESSION['user']['nhan_vien'],
               'doi_tuong' => 'Contacts',
               'action' => 'Xóa contact có id='.$id
           );
           $this->insert("nhatky", $data);
        }
        return $query;
    }

    function addObj($data)
    {
        $query = $this->insert("lienhe", $data);
        return $query;
    }

    function getdata()
    {
        $query = $this->db->query("SELECT *,
            (SELECT name FROM khachhang WHERE id=khach_hang) AS doitac
            FROM lienhe WHERE tinh_trang = 1");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}

?>

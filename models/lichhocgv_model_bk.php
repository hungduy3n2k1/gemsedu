<?php

class lichhocgv_model extends model
{
    function __construct()
    {
        parent::__construct();
    }


    function delObj($id)
    {
        $data = array('tinh_trang' => 0);
        $query = $this->update("lichhoc", $data, "id = $id");
        $this->update("saplop", $data, "lich_hoc = $id");
        return $query;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("lichhoc", $data, "id = $id");
        return $query;
    }

    function getFetobj($tungay, $denngay, $loai, $giaovien, $phonghoc, $giohoc, $tenlop)
    {
        $data = array();
        $dieukien = " WHERE tinh_trang>0 AND giao_vien=$giaovien AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc)<3 ";
        if ($tungay != '')
            $dieukien .= "  AND ngay>='$tungay' ";
        if ($denngay != '')
            $dieukien .= "  AND ngay<='$denngay' ";
        if ($phonghoc > 0)
            $dieukien .= " AND phong_hoc=$phonghoc ";
        if ($loai == 1)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc)=1 ";
        else if ($loai == 2)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (2,3,4) ";
        else if ($loai == 3)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (5,6,7) ";
        if ($giohoc != '')
            $dieukien .= " AND gio='$giohoc:00' ";
        if ($tenlop != '')
            $dieukien .= " AND (SELECT name FROM lophoc WHERE id=lop_hoc) LIKE '%$tenlop%' ";
        $query = $this->db->query("SELECT *, (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            DATE_FORMAT(ngay,'%d/%m/%Y') AS ngaygio,
            DATE_FORMAT(gio,'%H:%i') AS giohoc,
            IF(tinh_trang=1,'',gio_ra) as giora,
            (SELECT name FROM giaovien WHERE id=giao_vien) AS giaovien,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc,
            (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) AS loailop,
            (SELECT name FROM hocvien WHERE id=(SELECT hoc_vien FROM saplop WHERE lich_hoc=lichhoc.id AND tinh_trang>0 LIMIT 1)) AS hocvien
            FROM lichhoc $dieukien  ORDER BY ngay,gio");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        $data['rows'] = $rows;
        foreach($data['rows'] as $key=>$item){
            $thu = date('l', strtotime($item['ngay']));
            $data['rows'][$key]['thu']=$thu;
        }
        return $data;
    }

    function getday($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop)
    {
        $data = array();
        if ($ngay != '') {
            $thangnam = $nam . '-' . $thang . '-' . $ngay;
            $dieukien = " WHERE tinh_trang>0 AND ngay='$thangnam' AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc)<3 ";
        } else {
            $thangnam = $nam . '-' . $thang;
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc)<3 ";
        }
        if ($phonghoc > 0)
            $dieukien .= " AND phong_hoc=$phonghoc ";
        $dieukien .= " AND giao_vien=$giaovien ";
        if ($loai == 1)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc)=1 ";
        else if ($loai == 2)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (2,3,4) ";
        else if ($loai == 3)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (5,6,7) ";
        if ($giohoc != '')
            $dieukien .= " AND gio='$giohoc:00' ";
        if ($tenlop != '')
            $dieukien .= " AND (SELECT name FROM lophoc WHERE id=lop_hoc) LIKE '%$tenlop%' ";
        $query = $this->db->query("SELECT *, (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            DATE_FORMAT(ngay,'%a') AS thu,
            IF(tinh_trang=1,'',gio_ra) as giora,
            (SELECT name FROM giaovien WHERE id=giao_vien) AS giaovien,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc
            FROM lichhoc $dieukien  ORDER BY ngay, gio");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        $data['rows'] = $rows;
        return $data;
    }

    function gettime($ngay, $thang, $nam, $loai, $giaovien, $phonghoc, $giohoc, $tenlop)
    {
        $data = array();
        if ($ngay != '') {
            $thangnam = $nam . '-' . $thang . '-' . $ngay;
            $dieukien = " WHERE tinh_trang>0 AND ngay='$thangnam' AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc)<3 ";
        } else {
            $thangnam = $nam . '-' . $thang;
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc)<3 ";
        }
        $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' ";
        if ($phonghoc > 0)
            $dieukien .= " AND phong_hoc=$phonghoc ";
        $dieukien .= " AND giao_vien=$giaovien ";
        if ($loai == 1)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc)=1 ";
        else if ($loai == 2)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (2,3,4) ";
        else if ($loai == 3)
            $dieukien .= " AND (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) IN (5,6,7) ";
        if ($giohoc != '')
            $dieukien .= " AND gio='$giohoc:00' ";
        if ($tenlop != '')
            $dieukien .= " AND (SELECT name FROM lophoc WHERE id=lop_hoc) LIKE '%$tenlop%' ";
        $query = $this->db->query("SELECT *, (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            DATE_FORMAT(ngay,'%a') AS thu,
            IF(tinh_trang=1,'',gio_ra) as giora,
            (SELECT name FROM giaovien WHERE id=giao_vien) AS giaovien,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc
            FROM lichhoc $dieukien  ORDER BY gio, ngay");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        $data['rows'] = $rows;
        return $data;
    }

    function addObj($data)
    {
        $query = $this->insert("lichhoc", $data);
        if ($query)
            return $this->db->lastInsertId();
        else return 0;
    }

    function themSaplop($data)
    {
        $query = $this->insert("saplop", $data);
        if ($query)
            return $this->db->lastInsertId();
        else return 0;
    }

    function getMultiLich($lophoc, $ngay)
    {
        $data = array();
        $dieukien = " WHERE tinh_trang IN (1,2,3,4,5,6) AND ngay >= '$ngay' AND lop_hoc=$lophoc AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc)<3 ";
        $query = $this->db->query("SELECT id,ngay
            FROM lichhoc $dieukien  ORDER BY ngay, gio");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    function lophoc($giaovien, $phanloai)
    {
        $temp = array();
        $dieukien = " WHERE tinh_trang IN (1,2) AND giao_vien=$giaovien ";
        if ($phanloai > 0)
            $dieukien .= " AND phan_loai=$phanloai ";
        $query = $this->db->query("SELECT * FROM lophoc 
        $dieukien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function lophoc1($giaovien, $phanloai)
    {
        $temp = array();
        $dieukien = "WHERE tinh_trang IN (1,2) AND giao_vien=$giaovien 
        AND (SELECT count(id) FROM lichhoc WHERE lop_hoc=lophoc.id AND tinh_trang>1)=0";
        if ($phanloai > 0)
            $dieukien .= " AND phan_loai=$phanloai ";
        $query = $this->db->query("SELECT * FROM lophoc 
        $dieukien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function BuoiDaHoc($lophoc)
    {
        $data = array();
        $dieukien = " WHERE tinh_trang IN (1,2,3,4,5,6)  AND lop_hoc=$lophoc AND (SELECT tinh_trang FROM lophoc WHERE id=lop_hoc)<3 ";
        $query = $this->db->query("SELECT COUNT(id) as total 
            FROM lichhoc $dieukien");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($rows)
            return $rows[0]['total'];
        else
            return 0;
    }

    function DelLichhoc($lophoc){
        $data = ['tinh_trang'=>0];
        $ok = $this->update("lichhoc",$data,"lop_hoc=$lophoc");
        $ok = $this->update("saplop",$data,"lop_hoc=$lophoc");
        return $ok;
    }

    function deleteForChange($lophoc){
        $ok = $this->delete("lichhoc","lop_hoc=$lophoc AND tinh_trang=1");
        $ok = $this->delete("saplop","lop_hoc=$lophoc AND tinh_trang=1");
        return $ok;
    }

    function huylich($lichhoc)
    {
        $ok = false;
        $query = $this->db->query("SELECT *,
        (SELECT hoc_vien FROM saplop WHERE lich_hoc=$lichhoc LIMIT 1) as hocvien,
        (SELECT MAX(ngay) FROM lichhoc WHERE lop_hoc=a.lop_hoc) as ngaycuoi
        FROM lichhoc a WHERE tinh_trang>0 AND id='$lichhoc' ");
        $lichcu = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($lichcu) {
            $ok = $this->update("lichhoc",["tinh_trang"=>7],"id=$lichhoc");
            $this->update("saplop", ["tinh_trang" => 3], "lich_hoc=$lichhoc");
            $this->update("diemdanh", ["tinh_trang" => 3], "lich_hoc=$lichhoc");
            $ngaycu = $lichcu[0]['ngay'];
            $ngaycuoi = $lichcu[0]['ngaycuoi'];
            $tongngay = (strtotime($ngaycuoi) - strtotime($ngaycu)) / 86400;
            $ngaychenh = 7 - ($tongngay % 7);
            $ngaymoi = date("Y-m-d", strtotime("$ngaycuoi +$ngaychenh days"));
            $data = array(
                'ngay' => $ngaymoi,
                'gio' => $lichcu[0]['gio'],
                'gio_ra' => $lichcu[0]['gio_ra'],
                'lop_hoc' => $lichcu[0]['lop_hoc'],
                'giao_vien' => $lichcu[0]['giao_vien'],
                'phong_hoc' => $lichcu[0]['phong_hoc'],
                'thoi_luong' => $lichcu[0]['thoi_luong'],
                'tinh_trang' => 1
            );
            if($this->insert("lichhoc",$data)) {
                $lichhocid = $this->db->lastInsertId();
                $saplop = array(
                    'hoc_vien' => $lichcu[0]['hocvien'],
                    'lich_hoc' => $lichhocid,
                    'lop_hoc' => $lichcu[0]['lop_hoc'],
                    'tinh_trang' => 1
                );
                $ok = $this->insert("saplop",$saplop);
            }
            return $ok;
        } else
            return false;
    }

}

?>

<?php

class denghi_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt, $nhanvien, $tinhtrang)
    {
        $result = array();
        if ($tinhtrang == '')
            $dieukien = " WHERE tinh_trang>0  ";
        else
            $dieukien = " WHERE tinh_trang=$tinhtrang  ";
        if ($ngaybd != '')
            $dieukien .= " AND ngay>='$ngaybd' ";
        if ($ngaykt != '')
            $dieukien .= " AND ngay<='$ngaykt 23:59:59'  ";
        if ($nhanvien >0)
            $dieukien .= " AND nhan_vien=$nhanvien ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM denghi $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *,
           IF(ngay='0000-00-00','',DATE_FORMAT(ngay, '%d/%m/%Y')) AS ngaygio,
           IF(tinh_trang=1,'Tạo mới',IF(tinh_trang=2,'Đã duyệt','Từ chối')) as tinhtrang,
           (SELECT name FROM nhanvien WHERE id=nhan_vien) as nhanvien
           FROM denghi $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongthu = 0;
        $tongchi = 0;
//        foreach ($result['rows'] as $item) {
//            $tongthu = $tongthu +($item['thu']!='')?$item['thu']:0;
//            $tongchi = $tongchi + ($item['chi']!='')?$item['chi']:0;
//        }
//        $result['footer'] = array(0 => array('dien_giai' => 'Tổng cộng', 'thu' => $tongthu, 'chi' => $tongchi, 'so_du' => 0));
        return $result;
    }

    function addObj($data)
    {
        $query = $this->insert("denghi", $data);
        return $query;
    }

    function updateObj($id, $data, $duyet)
    {
        $nhanvien = $_SESSION['user']['nhan_vien'];
        if ($duyet)
            $query = $this->update("denghi", $data, " id=$id AND tinh_trang=1 ");
        else
            $query = $this->update("denghi", $data, " id=$id AND tinh_trang=1 AND nhan_vien=$nhanvien ");
        $nhatky = array(
            'ngay_gio' => date("Y-m-d H:i:s"),
            'user' => $nhanvien,
            'doi_tuong' => 'Tạm ứng/TT',
            'action' => 'Sửa phiếu tạm ứng id:' . $id
        );
        $query = $this->insert("nhatky", $nhatky);
        return $query;
    }

    function duyetphieu($id, $data)
    {
        $query = $this->update("denghi", $data, " id=$id AND tinh_trang=1 ");
        $nhatky = array(
            'ngay_gio' => date("Y-m-d H:i:s"),
            'user' => $_SESSION['user']['nhan_vien'],
            'doi_tuong' => 'Tạm ứng/TT',
            'action' => 'Duyệt phiếu tạm ứng id:' . $id
        );
        $query = $this->insert("nhatky", $nhatky);
        return $query;
    }

    function delObj($id, $duyet)
    {
        $nhanvien = $_SESSION['user']['nhan_vien'];
        $data = ['tinh_trang' => 0];
        if ($duyet)
            $query = $this->update("denghi", $data, "id = $id AND tinh_trang=1");
        else
            $query = $this->update("denghi", $data, "id = $id AND tinh_trang=1 AND nhan_vien=$nhanvien");
        $nhatky = array(
            'ngay_gio' => date("Y-m-d H:i:s"),
            'user' => $nhanvien,
            'doi_tuong' => 'Tạm ứng',
            'action' => 'Xóa phiếu tạm ứng:' . $id
        );
        $query = $this->insert("nhatky", $nhatky);
        return $query;
    }

}

?>

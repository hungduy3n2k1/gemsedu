<?php
class baogia_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $khachhang, $tinhtrang)
    {
        $result = [];
        $dieukien = " WHERE tinh_trang>0 AND ngay>='$tungay' AND ngay<='$denngay' ";
        if ($tinhtrang > 0) {
            $dieukien .= " AND tinh_trang=$tinhtrang ";
        }
        if ($khachhang > 0) {
            $dieukien .= " AND khach_hang=$khachhang ";
        }
        $query = $this->db->query("SELECT COUNT(*) AS total FROM baogia $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
           CONCAT('BG-',id) AS sobaogia,
           (SELECT name FROM khachhang WHERE id=khach_hang) AS khachhang,
           (SELECT IF(so_luong,ROUND(sum(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(1+thue_suat_vat/100))),0) FROM baogiasub WHERE bao_gia=a.id AND tinh_trang=1) AS sotien
           FROM baogia a $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function get_detail($id)
    {
        $dieukien = " WHERE bao_gia=$id AND tinh_trang=1 ";
        $query = $this->db->query("SELECT *,
           (SELECT name FROM dichvu WHERE id=dich_vu) AS hanghoa,
           (SELECT name FROM donvitinh WHERE id=don_vi) AS donvi,
           ROUND(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(1+thue_suat_vat/100)) AS thanhtien
           FROM baogiasub $dieukien ");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function addObj($baogia, $data)
    {
        $query = $this->insert("baogia", $baogia);
        if ($query) {
            $id = $this->db->lastInsertId();
            $hanghoa = json_decode($data, true);
            foreach ($hanghoa as $item) {
                $tungay = functions::convertDate($item['tungay']);
                $denngay = functions::convertDate($item['denngay']);
                $sub = [
                    'bao_gia' => $id,
                    'dich_vu' => $item['hanghoaid'],
                    'don_gia' => $item['dongia'],
                    'don_vi' => $item['donviid'],
                    'so_luong' => $item['soluong'],
                    'chiet_khau_tm' => $item['chietkhau'],
                    'chiet_khau_pt' => $item['chietkhaupt'],
                    'tang_thang' => $item['tangthang'],
                    'tu_ngay' => $tungay,
                    'den_ngay' => $denngay,
                    'ghi_chu' => $item['ghichu'],
                    'thue_suat_vat' => $item['thuevat'],
                    'tinh_trang' => 1,
                ];
                $query = $this->insert("baogiasub", $sub);
            }
        }
        return $query;
    }

    function noidung($id)
    {
        $dieukien = " WHERE bao_gia=$id AND tinh_trang=1 ";
        $query = $this->db->query("SELECT *,
           (SELECT name FROM dichvu WHERE id=dich_vu) AS hanghoa,
           (SELECT name FROM donvitinh WHERE id=don_vi) AS donvi,
           DATE_FORMAT(tu_ngay,'%d/%m/%Y') AS tungay,
           DATE_FORMAT(den_ngay,'%d/%m/%Y') AS denngay,
           ROUND(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(1+thue_suat_vat/100)) AS thanhtien
           FROM baogiasub $dieukien ");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $sum = 0;
        foreach ($result['rows'] as $row) {
            $sum += $row['thanhtien'];
        }
        $result['footer'] = [0 => ['hanghoa' => 'Tổng tiền', 'don_gia' => 0, 'chiet_khau_tm' => 0, 'thanhtien' => $sum]];
        return $result;
    }

    function updaterow($id, $data)
    {
        $query = $this->update("baogiasub", $data, "id = $id");
        return $query;
    }

    function insertrow($data)
    {
        $query = $this->insert("baogiasub", $data);
        return $query;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("baogia", $data, "id = $id");
        return $query;
    }

    function addrow($data)
    {
        $query = $this->insert("baogiasub", $data);
        return $query;
    }

    //--------send mail
    function email($id)
    {
        //lấy danh sách email khách hàng để chọn
        $dieukien = " WHERE khach_hang=$id AND tinh_trang=1 AND email!='' ";
        $query = $this->db->query("SELECT email FROM contact $dieukien ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $query = $this->db->query("SELECT email FROM khachhang WHERE id=$id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($temp[0]['email']) && $temp[0]['email'] != '') {
            $result[count($result)]['email'] = $temp[0]['email'];
        }
        return $result;
    }

    function sendbaogia($id, $email, $note)
    {
        $query = $this->db->query("SELECT *,
          (SELECT ten_day_du FROM khachhang WHERE id=khach_hang) AS khachhang
          FROM baogia WHERE id=$id ");
        $baogia = $query->fetchAll(PDO::FETCH_ASSOC);
        $name = $baogia[0]['khachhang'];
        $file = $baogia[0]['dinh_kem'];
        $query = $this->db->query("SELECT *,
                   (SELECT name FROM dichvu WHERE id=dich_vu) AS product,
                   (SELECT name FROM donvitinh WHERE id=don_vi) AS donvi,
                   ROUND(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(1+thue_suat_vat/100)) AS thanhtien
                   FROM baogiasub WHERE bao_gia=$id AND tinh_trang=1");
        $sub = $query->fetchAll(PDO::FETCH_ASSOC);
        $cktm = false;
        $ckpt = false;
        $free = false;
        foreach ($sub as $item) {
            if ($item['chiet_khau_tm'] > 0) {
                $cktm = true;
            }
            if ($item['chiet_khau_pt'] > 0) {
                $ckpt = true;
            }
            if ($item['tang_thang'] > 0) {
                $free = true;
            }
        }
        $noidung = $note;
        $query1 = $this->db->query("SELECT * FROM system ");
        $thongtin = $query1->fetchAll(PDO::FETCH_ASSOC);
        $noidung .=
            '
              <br></br>
              <img src="https://thuonghieuweb.com/oldversion/img/homepage/logo-thuonghieu.png" />
              <h2 class="title">BÁO GIÁ DỊCH VỤ</h2>
              <h3>Kính gửi quý khách : ' .$name .' </h3>
              <p><span style="font-weight: bold;color: #0E2D5F;">'.$thongtin[0]['value'].'</span> trân trọng gửi tới quý khách hàng báo giá các dịch vụ mà quý khách yêu cầu, chi tiết như sau:</p>
              <br>
              <table>
                <tr>
                  <th>STT</th>
                  <th>Sản phẩm / dịch vụ</th>
                  <th>Đơn giá</th>
                  <th>ĐVT</th>
                  <th>Số lượng</th>';
        if ($cktm) {
            $noidung .= '<th>Chiết khấu TM</th>';
        }
        if ($ckpt) {
            $noidung .= '<th>Chiết khấu %</th>';
        }
        $noidung .= '<th>Thuế VAT</th><th>Thành tiền</th>';
        if ($free) {
            $noidung .= '<th>Tặng tháng</th>';
        }
        $noidung .= '</tr>';
        $stt = 1;
        $tong = 0;
        foreach ($sub as $row) {
            $noidung .=
                '<tr>
                <td>' .
                $stt .
                '</td>
                <td>' .
                $row['product'] .
                '</td>
                <td style="text-align:right">' .
                number_format($row['don_gia']) .
                '</td>
                <td>' .
                $row['donvi'] .
                '</td>
                <td>' .
                $row['so_luong'] .
                '</td>';
            if ($cktm) {
                $noidung .= '<td>' . number_format($row['chiet_khau_tm']) . '</td>';
            }
            if ($ckpt) {
                $noidung .= '<td>' . $row['chiet_khau_pt'] . '</td>';
            }
            $noidung .= '<td style="text-align:right">' . number_format($row['thue_suat_vat']) . '</td>';
            $noidung .= '<td style="text-align:right">' . number_format($row['thanhtien']) . '</td>';
            if ($free) {
                $noidung .= '<td>' . $row['tang_thang'] . '</td>';
            }
            $noidung .= '</tr>';
            $stt++;
            $tong += $row['thanhtien'];
        }
        $noidung .= '<tr><th></th><th>Tổng cộng</th><th></th><th></th><th></th>';
        if ($cktm) {
            $noidung .= '<th></th>';
        }
        if ($ckpt) {
            $noidung .= '<th></th>';
        }
        $noidung .= '<th></th><th style="text-align:right">' . number_format($tong) . '</th>';
        if ($free) {
            $noidung .= '<th></th>';
        }
        $noidung .= '</tr></table><br>
        <br>';
        if ($file != '') {
            $noidung .= 'Tải file chi tiết đính kèm <a href="' . $file . '" target="_blanl">tại đây</a>';
        }
        $noidung .= 'Chúc quý khách một ngày làm việc hiệu quả!<br><br>
                Trân trọng! <br>
                --------------------------<br>
                Phòng kinh doanh - GEMS TECH <br>
                Hotline: '.$thongtin[4]['value'].' <br>
                Email: sale@gemstech.com.vn
                </body>
                </html>';
        $from = 'Phòng kinh doanh GEMS TECH - Thương Hiệu Web';
        $subject = 'Báo giá dịch vụ';
        $cc = '';
        $query = $this->sendmail($from, $email, $cc, $subject, $noidung);
        if ($query)
            $query = $this->db->query("UPDATE baogia SET tinh_trang=2 WHERE id=$id ");
        return $query;
    }

    function chot($id)
    {
        $query = $this->db->query("UPDATE baogia SET tinh_trang=3 WHERE id=$id ");
        $query = $this->db->query("SELECT * FROM baogia WHERE id=$id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $khachhang = $temp[0]['khach_hang'];
        $noidung = $temp[0]['noi_dung'];
        $data = [
            'ngay' => date("Y-m-d"),
            'khach_hang' => $khachhang,
            'bao_gia' => $id,
            'noi_dung' => $temp[0]['noi_dung'],
            'nhan_vien' => $temp[0]['nhan_vien'],
            'dinh_kem' => $temp[0]['dinh_kem'],
            'tinh_trang' => 2,
            'loai' =>1,
            'lan_1' => date("Y-m-d H:i:s"),
        ];
        if ($this->insert("invoice", $data)) {
            $invoice = $this->db->lastInsertId();
            $query = $this->db->query("SELECT * FROM baogiasub WHERE bao_gia=$id AND tinh_trang=1 ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $tong = 0;
            foreach ($temp as $row) {
                $data = [
                    'invoice' => $invoice,
                    'dich_vu' => $row['dich_vu'],
                    'don_gia' => $row['don_gia'],
                    'don_vi' => $row['don_vi'],
                    'so_luong' => $row['so_luong'],
                    'chiet_khau_tm' => $row['chiet_khau_tm'],
                    'chiet_khau_pt' => $row['chiet_khau_pt'],
                    'tang_thang' => $row['tang_thang'],
                    'thue_suat_vat' => $row['thue_suat_vat'],
                    'ngay_bd' => $row['tu_ngay'],
                    'ngay_kt' => $row['den_ngay'],
                    'ghi_chu' => $row['ghi_chu'],
                    'tinh_trang' => 1,
                ];
                $query = $this->insert("invoicesub", $data);
                $tong = $tong + $row['so_luong'] * ($row['don_gia'] - $row['chiet_khau_tm']) * (1 - $row['chiet_khau_pt'] / 100) * (1 + $row['thue_suat_vat'] / 100);
            }
            if ($query) {
                $query = $this->db->query("SELECT * FROM phaithu WHERE khach_hang=$khachhang ORDER BY ngay_gio DESC LIMIT 1 ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                $duno = isset($temp[0]['du_no']) ? $tong + $temp[0]['du_no'] : $tong;
                $data = ['ngay_gio' => date("Y-m-d H:i:s"), 'khach_hang' => $khachhang, 'so_tien' => $tong, 'du_no' => $duno, 'noi_dung' => $noidung, 'tinh_trang' => 1, 'loai_phieu' => 1, 'so_phieu' => $invoice];
                $this->insert("phaithu", $data);
            }
        }
        return $query;
    }
}

?>

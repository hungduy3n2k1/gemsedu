<?php
class khachhang_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tukhoa,$nhanvien,$phutrach,$loai)
    {
        $result   = array();
            $dieukien = " WHERE tinh_trang>0 ";
        if ($phutrach>0)
            $dieukien.= " AND phu_trach=$phutrach ";
        if ($tukhoa != '')
            $dieukien.= " AND (name LIKE '%$tukhoa%' OR dien_thoai LIKE '%$tukhoa%' ) ";
        if ($nhanvien>0)
            $dieukien.= " AND nhan_vien=$nhanvien";
        if($loai!='')
            $dieukien.= " AND loai=$loai";
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM khachhang $dieukien ");
        $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *, DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            (SELECT name FROM loaikh WHERE id=a.loai) as phanloai,
            (SELECT name FROM nhanvien WHERE id=a.phu_trach) as phutrach
            FROM khachhang a $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function get_detail($khachhang)
    {
        $result          = array();
        $dieukien        = " WHERE khach_hang=$khachhang AND tinh_trang>0 AND tinh_trang<5";
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM donhang $dieukien");
        $row             = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *,
            IF(ngay_bd='0000-00-00','',DATE_FORMAT(ngay_bd, '%d/%m/%Y')) AS ngaybd,
            IF(ngay_kt='0000-00-00','',DATE_FORMAT(ngay_kt, '%d/%m/%Y')) AS ngaykt,
            (SELECT name FROM product WHERE id=product) AS sanpham
            FROM donhang $dieukien ORDER BY ngay_kt");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("khachhang", $data, "id = $id");
        return $query;
    }

    function addObj($data)
    {
        $query = $this->insert("khachhang", $data);
        // if ($copy) {
        //     $id=$this->db->lastInsertId();
        //     $contact      = array(
        //         'name' => $data['dai_dien'],
        //         'khach_hang' =>$id,
        //         'chuc_vu' => $data['chuc_vu'],
        //         'dien_thoai' => $data['dien_thoai'],
        //         'email' => $data['email'],
        //         'ghi_chu' => 'Người đại diện',
        //         'tinh_trang' => 1
        //     );
        //     $query = $this->insert("contact", $contact);
        // }
        return $query;
    }

    function contact($id)
    {
        $result          = array();
        $dieukien        = " WHERE khach_hang=$id AND tinh_trang=1";
        $query           = $this->db->query("SELECT id, name AS hoten,
            dien_thoai AS dienthoai, email AS emailcts, chuc_vu AS chucvu, ghi_chu AS ghichu
            FROM contact $dieukien ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function addCts($data)
    {
        $query = $this->insert("contact", $data);
        return $query;
    }

    function saveCts($id, $data)
    {
        $query = $this->update("contact", $data, "id = $id");
        return $query;
    }

    function delObj($id)
    {
        $data = array('tinh_trang'=>-1);
        $query = $this->update("khachhang", $data, "id = $id");
        return $query;
    }

    function delCts($id)
    {
        $data = array('tinh_trang'=>0);
        $query = $this->update("contact", $data, "id = $id");
        return $query;
    }

}

?>

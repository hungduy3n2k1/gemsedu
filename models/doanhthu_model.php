<?php

class doanhthu_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($offset, $rows, $thang, $nam, $loaidh, $khachhang)
    {
        $result = array();
        if ($nam != "" && $thang != "") {
            $ngay = "$nam-$thang";
            $dieukien = " WHERE tinh_trang > 0 AND ngay_gio LIKE '$ngay%' AND (SELECT COUNT(id) FROM invoice WHERE id = a.invoice AND tinh_trang > 0) > 0 ";
            if($loaidh > 0 )
                $dieukien .= " AND (SELECT phan_loai_sale FROM donhang WHERE id = (SELECT don_hang FROM invoice WHERE id = a.invoice )) = $loaidh ";
            if($khachhang != '')
                $dieukien .= " AND (SELECT name FROM khachhang WHERE id = (SELECT khach_hang FROM donhang WHERE id = (SELECT don_hang FROM invoice WHERE id = a.invoice ))) LIKE '%$khachhang%' ";
            $query = $this->db->query("SELECT id FROM socai a $dieukien ");
            $row = $query->fetchAll();
            $result['total'] = count($row);
            $query = $this->db->query("SELECT ngay_gio AS ngaythanhtoan, SUM(so_tien) AS doanhthu,
            IFNULL((SELECT don_hang FROM invoice WHERE id = a.invoice),0) AS donhangid
            FROM socai a $dieukien GROUP BY invoice ORDER BY ngay_gio DESC LIMIT $offset, $rows");
            $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
            $tonghocphi = 0;
            $tongdadong = 0;
            $tongdoanhthu = 0;
            foreach ($result['rows'] as $key => $item) {
                $donhangid = $item['donhangid'];
                $query = $this->db->query("SELECT SUM(so_tien) AS dadong,
                    (SELECT name FROM khachhang WHERE id = (SELECT khach_hang FROM donhang WHERE id = $donhangid)) AS khachhang,
                    (SELECT name FROM hocvien WHERE id = (SELECT hoc_vien FROM donhang WHERE id = $donhangid)) AS hocvien,
                    (SELECT phan_loai_sale FROM donhang WHERE id = $donhangid) AS phanloaisale,
                    (SELECT so_tien FROM donhang WHERE id = $donhangid) AS tonghocphi
                    FROM socai WHERE invoice IN (SELECT id FROM invoice WHERE don_hang = $donhangid AND tinh_trang > 0) AND tinh_trang > 0 ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
               
                $result['rows'][$key]['hocvien'] = $temp[0]['hocvien'];
                $result['rows'][$key]['khachhang'] = $temp[0]['khachhang'];
                $result['rows'][$key]['phanloaisale'] = $temp[0]['phanloaisale'];
                $result['rows'][$key]['tonghocphi'] = $temp[0]['tonghocphi'];
                $result['rows'][$key]['dadong'] = $temp[0]['dadong'];
                $result['rows'][$key]['conlai'] = $temp[0]['tonghocphi'] - $temp[0]['dadong'];
                $tonghocphi += $temp[0]['tonghocphi'];
                $tongdadong += $temp[0]['dadong'];
                $tongdoanhthu += $item['doanhthu'];
            }
            $result['footer'] = array(0 => array('khachhang' => 'Tổng cộng', 'dadong' => $tongdadong, 'tonghocphi' => $tonghocphi, 'conlai' => $tonghocphi - $tongdadong, 'doanhthu' => $tongdoanhthu));
        }
        return $result;
    }

    function getjson($sort, $order, $offset, $rows, $team, $thang, $nam)
    {
        $result = array();
        $thangnam = $nam . '-' . $thang;
        $dieukien = " WHERE tinh_trang=1 AND loai=0 AND hach_toan=1 AND ngay_gio LIKE '$thangnam%' ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM socai $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay_gio, '%d/%m/%Y') AS ngay,
           IF((SELECT name FROM khachhang WHERE id=khach_hang),'Khác') AS khachhang
           FROM socai $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongthu = 0;
        foreach ($result['rows'] as $item)
            $tongthu = $tongthu + $item['so_tien'];
        $result['footer'] = array(0 => array('dien_giai' => 'Tổng cộng', 'so_tien' => $tongthu));
        return $result;
    }

    function getTong()
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 ";
        $query = $this->db->query("SELECT SUM(so_tien) as tonghocphi, SUM(du_no) AS conlai
           FROM invoice $dieukien ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}

?>

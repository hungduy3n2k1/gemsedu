<?php
class Menu_Model extends Model{
    function __construct(){
        parent::__construct();
    }

    function getFetObj($loai){
        $result = array();
        $dieukien = " WHERE loai=$loai ";
        $query = $this->db->query("SELECT *, parentid as cha, IF(parentid = 0, '', parentid) AS _parentId
            FROM menu $dieukien ORDER BY thu_tu");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['rows'] = functions::dequy($temp,0,1);
        return $result;
    }

    function addObj($data){
        $query = $this->insert("menu", $data);
        return $query;
    }

    function updateObj($id, $data){
        $query = $this->update("menu", $data, "id = $id");
        return $query;
    }

    function delObj($id){
        $query = $this->delete('menu', "id = $id");
        return $query;
    }
}
?>

<?php
class kpi_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt)
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang IN (1,2,3,4) ";
        $dieukien1 = " WHERE nhan_vien=nhanvien.id AND ngay_giao>'$ngaybd' AND ngay_giao<'$ngaykt 23:59:59' ";
        $dieukien2 = " WHERE nhan_vien=nhanvien.id AND updated>'$ngaybd' AND updated<'$ngaykt 23:59:59' ";
        $query = $this->db->query("SELECT id, name,
            (SELECT COUNT(1) FROM congviec $dieukien1 AND tinh_trang>0) AS tong,
            (SELECT COUNT(1) FROM congviec $dieukien2 AND tinh_trang IN (4,5)) AS hoanthanh,
            (SELECT COUNT(1) FROM congviec WHERE nhan_vien=nhanvien.id AND tinh_trang IN (1,2,3)) AS tondong,
            (SELECT COUNT(1) FROM congviecsub $dieukien2 AND tinh_trang=3) AS tre,
            (SELECT COUNT(1) FROM congviecsub $dieukien2 AND tinh_trang=5) AS khongdat
            FROM nhanvien $dieukien");
        $temp             = $query->fetchAll(PDO::FETCH_ASSOC);
        $result  = $temp;
        return $result;
    }

    function get_detail($sort, $order, $offset, $rows,$ngaybd,$ngaykt,$nhanvien){
        $result = array();
        $dieukien = " WHERE nhan_vien=$nhanvien AND updated>'$ngaybd' AND updated<'$ngaykt 23:59:59' ";
        $query = $this->db->query("SELECT *, IF(tinh_trang=3,'Trễ deadline','Không đạt') AS tinhtrang,
            (SELECT DATE_FORMAT(ngay_giao,'%d/%m/%Y') FROM congviec WHERE id=congviecsub.congviec_id) AS ngaygiao,
            (SELECT name FROM nhanvien WHERE id=congviecsub.nguoi_giao) AS nguoigiao,
            DATE_FORMAT(updated,'%d/%m/%Y') AS ngaycapnhat
            FROM congviecsub $dieukien ORDER BY $sort $order, updated LIMIT $offset, $rows  ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}
?>

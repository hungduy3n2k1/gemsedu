<?php

class baocaohv_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $hocvien, $khachhang, $giaovien, $chuyenmon)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang IN (1,2) ";
        if ($hocvien != '')
            $dieukien .= " AND hoc_vien IN (SELECT id FROM hocvien WHERE name LIKE '%$hocvien%') ";
        if ($khachhang != '')
            $dieukien .= " AND hoc_vien IN (SELECT id FROM hocvien WHERE khach_hang IN (SELECT id FROM khachhang WHERE name LIKE '%$khachhang%')) ";
        if ($giaovien != '')
            $dieukien .= " AND giao_vien = $giaovien ";
        if ($chuyenmon != '')
            $dieukien .= " AND phu_trach = $chuyenmon ";
        // if ($tungay != '')
        //     $dieukien .= " AND (SELECT ngay FROM lichhoc WHERE id=lich_hoc)>='$tungay' ";
        // if ($denngay != '')
        //     $dieukien .= " AND (SELECT ngay FROM lichhoc WHERE id=lich_hoc)<='$denngay' ";

        $query = $this->db->query("SELECT hoc_vien,(so_buoi+buoi_khuyen_mai) as tongbuoi,
        (SELECT COUNT(id) FROM lichhoc WHERE lop_hoc=a.id AND tinh_trang IN (2,3,4,5,6)) AS dahoc,
        IFNULL((SELECT so_tien FROM donhang WHERE tinh_trang IN (1,2) 
        AND id=a.don_hang),0) as sotien
        FROM lophoc a $dieukien HAVING sotien>0 AND tongbuoi>dahoc");

        // $query = $this->db->query("SELECT 
        // (SELECT SUM(so_buoi+buoi_khuyen_mai) FROM lophoc WHERE id = a.lop_hoc) AS tongbuoi,
        // (SELECT COUNT(id) FROM lichhoc WHERE lop_hoc = a.lop_hoc AND tinh_trang IN (2,3,4,5,6)) AS dahoc,
        // IFNULL((SELECT so_tien FROM donhang WHERE tinh_trang IN (1,2) 
        // AND id=(SELECT don_hang FROM lophoc WHERE id=a.lop_hoc)),0) as sotien
        // FROM saplop a $dieukien GROUP BY lop_hoc HAVING sotien>0 AND tongbuoi>dahoc");
        $result['total'] = COUNT($query->fetchAll(PDO::FETCH_ASSOC));

        $query = $this->db->query("SELECT id,hoc_vien,(so_buoi+buoi_khuyen_mai) as tongbuoi,
        (SELECT COUNT(id) FROM lichhoc WHERE lop_hoc=a.id AND tinh_trang IN (2,3,4,5,6)) AS dahoc,
        IFNULL((SELECT name FROM giaovien WHERE id=giao_vien),'') as giaovien,
        IFNULL((SELECT name FROM nhanvien WHERE id=phu_trach),'') as chuyenmon,
        IFNULL((SELECT so_tien FROM donhang WHERE tinh_trang IN (1,2) 
        AND id=a.don_hang),0) as sotien
        FROM lophoc a $dieukien HAVING sotien>0 AND tongbuoi>dahoc ORDER BY $sort $order LIMIT $offset, $rows ");
        if($query) {
            $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach($result['rows'] as $key => $item) {
                $result['rows'][$key]['tongbuoi'] = $item['tongbuoi'];
                $result['rows'][$key]['dahoc'] = $item['dahoc'];
                $result['rows'][$key]['conlai'] = $item['tongbuoi'] - $item['dahoc'];
                $result['rows'][$key]['giaovien'] = $item['giaovien'];
                $result['rows'][$key]['chuyenmon'] = $item['chuyenmon'];
                $idlh = $item['id'];
                $query = $this->db->query("SELECT MAX(ngay) as ngayketthuc, MIN(ngay) AS ngaybatdau
                FROM lichhoc WHERE lop_hoc = $idlh AND tinh_trang IN (1,2,3,4,5,6) ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                $result['rows'][$key]['ngayketthuc'] = $temp[0]['ngayketthuc'];
                $result['rows'][$key]['ngaybatdau'] = $temp[0]['ngaybatdau'];
                $idhv = $item['hoc_vien'];
                $query = $this->db->query("SELECT IFNULL(name,'') AS name,
                IF(ngay_sinh!='0000-00-00',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
                IFNULL((SELECT name FROM khachhang WHERE id=a.khach_hang),'') as khachhang,
                IFNULL(CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id),'') as mahocvien,
                IFNULL((SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang),'') as dienthoaikh,
                IFNULL((SELECT email FROM khachhang WHERE id=a.khach_hang),'') as emailkh
                FROM hocvien a WHERE id = $idhv ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                $result['rows'][$key]['name'] = $temp[0]['name'];
                $result['rows'][$key]['ngaysinh'] = $temp[0]['ngaysinh'];
                $result['rows'][$key]['khachhang'] = $temp[0]['khachhang'];
                $result['rows'][$key]['mahocvien'] = $temp[0]['mahocvien'];
                $result['rows'][$key]['dienthoaikh'] = $temp[0]['dienthoaikh'];
                $result['rows'][$key]['emailkh'] = $temp[0]['emailkh'];
            }
        }
        
        return $result;
    }

}

?>





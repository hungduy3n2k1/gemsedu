<?php
class ungvien_model extends model
{
   function __construct()
   {
       parent::__construct();
   }

   function getFetObj($sort, $order, $offset, $rows, $tukhoa)
   {
       $result   = array();
       $dieukien = " WHERE tinh_trang=1";
       if ($tukhoa != '')
           $dieukien.= " AND (name LIKE '%".$tukhoa."%' OR dien_thoai LIKE '%".$tukhoa."%' ) ";
       $query           = $this->db->query("SELECT COUNT(*) AS total FROM ungvien $dieukien ");
       $row             = $query->fetchAll(PDO::FETCH_ASSOC);
       $result['total'] = $row[0]['total'];
       $query           = $this->db->query("SELECT *,
          IF(ngay_sinh='0000-00-00 00:00:00','',DATE_FORMAT(ngay_sinh, '%d/%m/%Y')) AS ngaysinh
           FROM ungvien $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
       $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
       return $result;
   }

   // function get_detail($nhanvien, $sort, $order, $offset, $rows)
   // {
   //     $result          = array();
   //     $dieukien        = " WHERE khach_hang=$nhanvien ";
   //     $query           = $this->db->query("SELECT COUNT(*) AS total FROM donhang $dieukien");
   //     $row             = $query->fetchAll();
   //     $result['total'] = $row[0]['total'];
   //     $query           = $this->db->query("SELECT *
   //         FROM donhang $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
   //     $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
   //     return $result;
   // }

   function updateObj($id, $data)
   {
       $query = $this->update("ungvien", $data, "id = $id");
       return $query;
   }

   function addObj($data)
   {
       $query = $this->insert("ungvien", $data);
       return $query;
   }

   function delObj($id)
   {
       $data = array('tinh_trang'=>0);
       $query = $this->update("ungvien", $data, "id = $id");
       return $query;
   }

}
?>

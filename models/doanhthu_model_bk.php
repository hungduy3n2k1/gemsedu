<?php

class doanhthu_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt, $loaidh, $khachhang)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 ";
        if ($ngaybd != "")
            $dieukien .= " AND ngay >= '$ngaybd' ";
        if ($ngaykt != "")
            $dieukien .= " AND ngay <= '$ngaykt 23:59:59' ";
        if($loaidh > 0 )
         $dieukien .= " AND (SELECT phan_loai_sale FROM donhang WHERE id=don_hang)=$loaidh ";
        if($khachhang != '')
            $dieukien .= " AND (SELECT name FROM khachhang WHERE id=(SELECT khach_hang FROM donhang WHERE id=don_hang)) LIKE '%$khachhang%' ";
        $query = $this->db->query("SELECT id, SUM(du_no) AS conlai FROM invoice $dieukien AND don_hang IN (SELECT id FROM donhang WHERE id=don_hang) GROUP BY don_hang");
        $row = $query->fetchAll();
        $result['total'] = count($row);
        $query = $this->db->query("SELECT SUM(so_tien) as tonghocphi, SUM(du_no) AS conlai, DATE_FORMAT(ngay,'%d/%m/%Y') AS ngaythanhtoan,
           (SELECT name FROM khachhang WHERE id=(SELECT khach_hang FROM donhang WHERE id=don_hang)) AS khachhang,
           (SELECT name FROM hocvien WHERE id=(SELECT hoc_vien FROM donhang WHERE id=don_hang)) AS hocvien,
           (SELECT phan_loai_sale FROM donhang WHERE id=(SELECT id FROM donhang WHERE id=don_hang)) AS phanloaisale
           FROM invoice $dieukien GROUP BY don_hang ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $tonghocphi = 0;
        $tongconlai = 0;
        foreach ($result['rows'] as $key => $item) {
            $result['rows'][$key]['dadong'] = $item['tonghocphi'] - $item['conlai'];
            $tonghocphi += $item['tonghocphi'];
            $tongconlai += $item['conlai'];
        }
        $result['footer'] = array(0 => array('khachhang' => 'Tổng cộng', 'dadong' => $tonghocphi - $tongconlai, 'tonghocphi' => $tonghocphi, 'conlai' => $tongconlai));
        return $result;
    }

    function getjson($sort, $order, $offset, $rows, $team, $thang, $nam)
    {
        $result = array();
        $thangnam = $nam . '-' . $thang;
        $dieukien = " WHERE tinh_trang=1 AND loai=0 AND hach_toan=1 AND ngay_gio LIKE '$thangnam%' ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM socai $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay_gio, '%d/%m/%Y') AS ngay,
           IF((SELECT name FROM khachhang WHERE id=khach_hang),'Khác') AS khachhang
           FROM socai $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongthu = 0;
        foreach ($result['rows'] as $item)
            $tongthu = $tongthu + $item['so_tien'];
        $result['footer'] = array(0 => array('dien_giai' => 'Tổng cộng', 'so_tien' => $tongthu));
        return $result;
    }

    function getTong()
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 ";
        $query = $this->db->query("SELECT SUM(so_tien) as tonghocphi, SUM(du_no) AS conlai
           FROM invoice $dieukien ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}

?>

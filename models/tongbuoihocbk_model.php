<?php

class tongbuoihoc_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tukhoa, $nam, $thang)
    {
        $result = array();

        if ($thang > 0)
            $thangnam = $nam . '-' . $thang;
        else
            $thangnam = $nam;
        if ($thangnam != '')
            $dieukien = " WHERE tinh_trang>0 AND (SELECT ngay FROM lichhoc WHERE id=lich_hoc) LIKE '$thangnam%' ";
        else
            $dieukien = " WHERE tinh_trang>0";
        if ($tukhoa != '')
            $dieukien .= " AND (SELECT name FROM hocvien WHERE id=hoc_vien) LIKE '%$tukhoa%' ";
        $query = $this->db->query("SELECT hoc_vien,
            IFNULL((SELECT SUM(so_tien) FROM donhang WHERE tinh_trang IN (1,2) 
            AND khach_hang=(SELECT khach_hang FROM hocvien WHERE id=saplop.hoc_vien)),0) as sotien
            FROM saplop $dieukien GROUP BY lop_hoc HAVING sotien>0");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp) {
            $result['total'] = count($temp);
            $query = $this->db->query("SELECT lop_hoc,hoc_vien,
            (SELECT name FROM hocvien WHERE id=hoc_vien) AS hocvien,
            IFNULL((SELECT so_tien FROM donhang WHERE tinh_trang IN (1,2) 
            AND id=(SELECT don_hang FROM lophoc WHERE id=saplop.lop_hoc)),0) as sotien
            -- IFNULL((SELECT SUM(so_tien) FROM donhang WHERE tinh_trang IN (1,2)
            -- AND khach_hang=(SELECT khach_hang FROM hocvien WHERE id=saplop.hoc_vien)),0) as sotien
            FROM saplop $dieukien GROUP BY lop_hoc HAVING sotien>0 ORDER BY $sort $order LIMIT $offset,$rows");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $doanhthutong = 0;
            $tongtienkhoa = 0;
            foreach ($temp as $key => $hocvien) {
                $tongbuoi = 0;
                $dahoc = 0;
                $hocvienid = $hocvien['hoc_vien'];
                $lophoc = $hocvien['lop_hoc'];
                $result['rows'][$key]['hocvien'] = $hocvien['hocvien'];
                $result['rows'][$key]['tongtienkhoa'] = $hocvien['sotien'];
                $qr = $this->db->query("SELECT `name`,khoa_hoc,IF(thoi_luong>0,thoi_luong,'45') as thoi_luong,
                    (SELECT SUM(thoi_luong) FROM lichhoc WHERE lop_hoc=$lophoc AND tinh_trang IN (1,2,3,4,5,6)) AS tongthoiluong,
                    (SELECT IF(giao_vien=1,'Nước ngoài','Việt Nam') FROM khoahoc WHERE id=khoa_hoc AND tinh_trang=1) AS phanloai
                    FROM lophoc WHERE id=$lophoc");
                $tp = $qr->fetchAll(PDO::FETCH_ASSOC);
                $query = $this->db->query("SELECT id,
                    (SELECT thoi_luong FROM lichhoc WHERE id=lich_hoc) as thoi_luong,
                    (SELECT tinh_trang FROM lichhoc WHERE id=lich_hoc) as tinh_trang
                    FROM saplop WHERE tinh_trang>0 
                    AND (SELECT ngay FROM lichhoc WHERE id=lich_hoc) LIKE '$thangnam%' 
                    AND lop_hoc=$lophoc");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($temp as $buoi) {
                    $tongbuoi+=ROUND($buoi['thoi_luong']/$tp[0]['thoi_luong']);
                    if ($buoi['tinh_trang'] > 1)
                        $dahoc+=ROUND($buoi['thoi_luong']/$tp[0]['thoi_luong']);
                }

                if ($tp && $tp[0]['tongthoiluong'] > 0) {
                    $result['rows'][$key]['phanloai'] = $tp[0]['phanloai'];
                    $result['rows'][$key]['tongbuoicuakhoa'] =  ROUND($tp[0]['tongthoiluong']/$tp[0]['thoi_luong']);
                    $result['rows'][$key]['doanhthu'] = ROUND(($result['rows'][$key]['tongtienkhoa'] / $tp[0]['tongthoiluong']) * $tp[0]['thoi_luong'] * $dahoc);
                } else {
                    $result['rows'][$key]['phanloai'] = '';
                    $result['rows'][$key]['tongbuoicuakhoa'] = 0;
                    $result['rows'][$key]['doanhthu'] = 0;
                }
                $doanhthutong += $result['rows'][$key]['doanhthu'];
                $tongtienkhoa += $result['rows'][$key]['tongtienkhoa'];
                $result['rows'][$key]['lophoc'] = $tp[0]['name'];
                $result['rows'][$key]['tongbuoi'] = $tongbuoi;
                $result['rows'][$key]['dahoc'] = $dahoc;
                $result['rows'][$key]['conlai'] = $tongbuoi - $dahoc;

            }
        }
        $result['footer'] = [['doanhthu' => $doanhthutong, 'tongtienkhoa' => $tongtienkhoa]];
        return $result;
    }

    function getTong($nam, $thang, $tukhoa)
    {
        if ($thang > 0)
            $thangnam = $nam . '-' . $thang;
        else
            $thangnam = $nam;
        if ($thangnam != '')
            $dieukien = " WHERE tinh_trang>0 AND (SELECT ngay FROM lichhoc WHERE id=lich_hoc) LIKE '$thangnam%' ";
        else
            $dieukien = " WHERE tinh_trang>0";
        if ($tukhoa != '')
            $dieukien .= " AND (SELECT name FROM hocvien WHERE id=hoc_vien) LIKE '%$tukhoa%' ";
        $query = $this->db->query("SELECT hoc_vien,
            IFNULL((SELECT SUM(so_tien) FROM donhang WHERE tinh_trang IN (1,2) 
            AND khach_hang = (SELECT khach_hang FROM hocvien WHERE id=saplop.hoc_vien)),0) as sotien
            FROM saplop $dieukien GROUP BY hoc_vien HAVING sotien>0");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp) {
            $query = $this->db->query("SELECT lop_hoc,hoc_vien,
            IFNULL((SELECT SUM(so_tien) FROM donhang WHERE tinh_trang IN (1,2) 
            AND khach_hang = (SELECT khach_hang FROM hocvien WHERE id=saplop.hoc_vien)),0) as sotien
            FROM saplop $dieukien GROUP BY hoc_vien HAVING sotien>0");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $doanhthutong = 0;
            $tongbuoihoc = 0;
            foreach ($temp as $key => $hocvien) {
                $tongtienkhoa = $hocvien['sotien'];
                $tongbuoi = 0;
                $dahoc = 0;
                $hocvienid = $hocvien['hoc_vien'];
                $query = $this->db->query("SELECT id,
                    (SELECT tinh_trang FROM lichhoc WHERE id=lich_hoc) as tinh_trang
                    FROM saplop WHERE tinh_trang>0 
                    AND (SELECT ngay FROM lichhoc WHERE id=lich_hoc) LIKE '$thangnam%' 
                    AND hoc_vien=$hocvienid");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($temp as $buoi) {
                    $tongbuoi++;
                    if ($buoi['tinh_trang'] > 1)
                        $dahoc++;
                }

                $lophoc = $hocvien['lop_hoc'];
                $qr = $this->db->query("SELECT (so_buoi+buoi_khuyen_mai) as tongbuoi,`name`,khoa_hoc,
                    (SELECT IF(giao_vien=1,'Nước ngoài','Việt Nam') FROM khoahoc WHERE id=khoa_hoc AND tinh_trang=1) AS phanloai
                    FROM lophoc WHERE id=$lophoc");
                $tp = $qr->fetchAll(PDO::FETCH_ASSOC);

                if ($tp && $tp[0]['tongbuoi'] > 0) {
                    $doanhthu = ROUND(($tongtienkhoa / $tp[0]['tongbuoi']) * $dahoc);
                } else {
                    $doanhthu = 0;
                }
                $doanhthutong += $doanhthu;
                $tongbuoihoc += $tongbuoi;
            }

        }
        $result = ['tongdoanhthu' => $doanhthutong, 'tongbuoihoc' => $tongbuoihoc];
        return $result;
    }

}

?>

<?php
class hoadon_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows,$tungay,$denngay,$loai,$doitac,$tukhoa)
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 AND ngay>='$tungay' AND ngay<='$denngay' AND phan_loai=$loai ";
        if ($doitac > 0)
          $dieukien.= " AND doi_tac=$doitac ";
        if($tukhoa!='')
            $dieukien .= " AND name LIKE '%$tukhoa%' ";
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM hoadon $dieukien ");
        $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        if ($loai)
            $query = $this->db->query("SELECT *, (so_tien+thue_vat) AS tong, DATE_FORMAT(ngay, '%d/%m/%Y') AS ngaygio,
            (SELECT name FROM khachhang WHERE id=doi_tac) AS khachhang
            FROM hoadon $dieukien ORDER BY $sort $order LIMIT $offset, $rows ");
        else
            $query = $this->db->query("SELECT *, (so_tien+thue_vat) AS tong, DATE_FORMAT(ngay, '%d/%m/%Y') AS ngaygio,
            (SELECT name FROM nhacungcap WHERE id=doi_tac) AS khachhang
            FROM hoadon $dieukien ORDER BY $sort $order LIMIT $offset, $rows ");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongtien = 0;
        $tongvat=0;
        $tongsotien=0;
        foreach($result['rows'] AS $item){
          $tongsotien = $tongsotien + $item['so_tien'];
          $tongvat = $tongvat + $item['thue_vat'];
          $tongtien = $tongtien + $item['tong'];
        }
        $result['footer']  = array(0=>array('dia_chi'=>'Tổng ','so_tien'=>$tongsotien,'thue_vat'=>$tongvat,'tong'=>$tongtien,'dinh_kem'=>''));
        return $result;
    }

    function get_detail($hoadon)
    {
        $result          = array();
        $dieukien        = " WHERE hoa_don=$hoadon AND tinh_trang=1";
        $query           = $this->db->query("SELECT *, (so_luong*don_gia*thue_suat/100) AS thanhtien FROM chitiethoadon $dieukien ");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function doitac()

    {

        $temp  = array();

        $query = $this->db->query("SELECT id,name,ma_so,dia_chi FROM nhacungcap WHERE tinh_trang=1  ");

        $temp  = $query->fetchAll(PDO::FETCH_ASSOC);

        return $temp;

    }
    function khachhang()

    {

        $temp  = array();

        $query = $this->db->query("SELECT id,name,ma_so,dia_chi FROM khachhang WHERE tinh_trang>1  ");

        $temp  = $query->fetchAll(PDO::FETCH_ASSOC);

        return $temp;

    }

    function updateObj($id, $data)
    {
        $query = $this->update("hoadon", $data, "id=$id");
        return $query;
    }

    function addObj($data)
    {
        $query = $this->insert("hoadon", $data);
        return $query;
    }
}
?>

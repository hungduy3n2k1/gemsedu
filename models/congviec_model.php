<?php
class congviec_model extends model{
    function __construct(){
        parent::__construct();
    }

    function getFetObj($nhanvien){
        $result = array();
        $dieukien = " WHERE tinh_trang IN (1,2,3,4) AND nhan_vien=$nhanvien";
        $query = $this->db->query("SELECT  id, name, CONCAT(name,' (<span style=\"color:blue\">',DATE_FORMAT(deadline,'%d/%m/%Y'),'</span>)') AS `text`,
            IF(tinh_trang=1,'Đang làm',IF(tinh_trang=2,'Gia hạn',IF(tinh_trang=3,'Trễ deadline',IF(tinh_trang=4,'Đã xong',IF(tinh_trang=5,'Không đạt','Hoàn thành'))))) AS `group`,
            DATE_FORMAT(deadline,'%d/%m/%Y') AS thoihan, nguoi_giao, tinh_trang
            FROM congviec $dieukien ORDER BY tinh_trang, updated DESC  ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // function getjson($nhanvien){ //mobile
    //     $result = array();
    //     $dieukien = " WHERE tinh_trang IN (1,2,3,4) AND nhan_vien=$nhanvien";
    //     $query = $this->db->query("SELECT  id, name, CONCAT(name,' (<span style=\"color:blue\">',DATE_FORMAT(deadline,'%d/%m/%Y'),'</span>)') AS `text`,
    //         IF(tinh_trang=1,'Đang làm',IF(tinh_trang=2,'Gia hạn',IF(tinh_trang=3,'Trễ deadline',IF(tinh_trang=4,'Đã xong',IF(tinh_trang=5,'Không đạt','Hoàn thành'))))) AS `group`,
    //         DATE_FORMAT(deadline,'%d/%m/%Y') AS thoihan, nguoi_giao, tinh_trang
    //         FROM congviec $dieukien ORDER BY tinh_trang, updated DESC  ");
    //     $result = $query->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    function addObj($data){
        $query = false;
        if($this->insert("congviec", $data)) {
            $event = array('name'=>'Công việc mới: '.$data['name'],
                        'nguoi_gui'=>$data['nguoi_giao'], 'nguoi_nhan'=>$data['nhan_vien'],
                        'ngay_gio'=>date("Y-m-d H:i:s"), 'tinh_trang'=>0);
            $query = $this->insert("events", $event);
        }
        return $query;
    }

    function updateObj($id, $data, $nguoigiao){
        $query = $this->update("congviec", $data, "id = $id");
        if($query) {
            if($data['tinh_trang']==5) {
                $data =['nguoi_giao'=>$nguoigiao,'name'=>$data['name'],'congviec_id'=>$id,'tinh_trang'=>5,'nhan_vien'=>$data['nhan_vien'],'updated'=>$data['updated']];
                $this->insert("congviecsub", $data);
                $event = array('name'=>'Công việc không đạt: '.$data['name'],
                            'nguoi_gui'=>$_SESSION['user']['nhan_vien'], 'nguoi_nhan'=>$data['nhan_vien'],
                            'ngay_gio'=>date("Y-m-d H:i:s"), 'tinh_trang'=>0);
                $query = $this->insert("events", $event);
            }
            if($data['tinh_trang']==4) {
                $event = array('name'=>'Hoàn thành công việc: '.$data['name'],
                            'nguoi_gui'=>$_SESSION['user']['nhan_vien'], 'nguoi_nhan'=>$nguoigiao,
                            'ngay_gio'=>date("Y-m-d H:i:s"), 'tinh_trang'=>0);
                $query = $this->insert("events", $event);
            }
        }
        return $query;
    }

    function chitiet($id)
    {
        $result   = array();
        if ($id>0) {
            $query           = $this->db->query("SELECT *, DATE_FORMAT(ngay_giao,'%d/%m/%Y') AS ngaygiao,
               DATE_FORMAT(deadline,'%d/%m/%Y') AS hancuoi,
               DATE_FORMAT(ngay_bd,'%d/%m/%Y') AS ngaybd,
               DATE_FORMAT(ngay_kt,'%d/%m/%Y') AS ngaykt,
               DATE_FORMAT(updated,'%d/%m/%Y') AS ngaycapnhat
               FROM congviec  WHERE id=$id ");
            $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
            $result = $temp[0];
        }
        return $result;
    }

    function comment($id)
    {
        $result   = array();
        $query   = $this->db->query("SELECT
            CONCAT(DATE_FORMAT(ngay_gio,'%d/%m/%Y %H:%i:%s'),' (<span style=\"color:blue\">',(SELECT name FROM nhanvien WHERE id=nhan_vien),'</span>) ',noi_dung,' <a style=\"color:blue\" href=\"',dinh_kem,'\" target=\"_blank\">',dinh_kem,'</a>') AS `text`
            FROM comment  WHERE cong_viec=$id ORDER BY ngay_gio DESC ");
        $result  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function addcomment($data)
    {
        $query = $this->insert("comment", $data);
        if($query) {
            $congviec =$data['cong_viec'];
            $query = $this->db->query("SELECT nguoi_giao, nhan_vien FROM congviec WHERE id=$congviec ");
            $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
            $nguoinhan = $temp[0]['nguoi_giao'].','.$temp[0]['nhan_vien'];
            $event = array('name'=>'Góp ý công việc: '.$data['noi_dung'],
                        'nguoi_gui'=>$_SESSION['user']['nhan_vien'], 'nguoi_nhan'=>$nguoinhan,
                        'ngay_gio'=>date("Y-m-d H:i:s"), 'tinh_trang'=>0);
            $query = $this->insert("events", $event);
        }
        return $query;
    }





    // function giahan($id, $ngay, $tinhtrang){
    //     $query = false;
    //     $today = date("Y-m-d");
    //     if ($tinhtrang==3) {
    //         $query = $this->db->query("SELECT * FROM congviec WHERE id=$id");
    //         $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //         $data      = array(
    //             'name' => $temp[0]['name'].'(Gia hạn)',
    //             'mo_ta' => $temp[0]['mo_ta'],
    //             'ngay_giao' => $temp[0]['ngay_giao'],
    //             'deadline' => $ngay,
    //             'ngay_bd' => $today,
    //             'du_an' => $temp[0]['du_an'],
    //             'nhan_vien' => $temp[0]['nhan_vien'],
    //             'nguoi_giao' => $temp[0]['nguoi_giao'],
    //             'tinh_trang' => 1,
    //             'updated' => $today
    //         );
    //         if($this->insert("congviec", $data)) {
    //             $event = array('name'=>'Gia hạn công việc: '.$data['name'],
    //                         'nguoi_gui'=>$data['nguoi_giao'], 'nguoi_nhan'=>$data['nhan_vien'],
    //                         'ngay_gio'=>date("Y-m-d H:i:s"), 'tinh_trang'=>0);
    //             $query = $this->insert("events", $event);
    //         }
    //     } else {
    //         $data      = array('deadline' => $ngay,'updated' => $today);
    //         $query = $this->update("congviec", $data, "id = $id");
    //     }
    //     return $query;
    // }
    //
    // function danhgia($id, $danhgia){
    //     $query = false;
    //     $today = date("Y-m-d");
    //     if ($danhgia==6) {
    //         $data      = array('tinh_trang' => 6,'updated' => $today);
    //         $query = $this->update("congviec", $data, "id = $id");
    //     } else {
    //         $query = $this->db->query("SELECT * FROM congviec WHERE id=$id");
    //         $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //         $data      = array(
    //             'name' => $temp[0]['name'].'(Làm lại)',
    //             'mo_ta' => $temp[0]['mo_ta'],
    //             'ngay_giao' => $temp[0]['ngay_giao'],
    //             'du_an' => $temp[0]['du_an'],
    //             'nhan_vien' => $temp[0]['nhan_vien'],
    //             'nguoi_giao' => $temp[0]['nguoi_giao'],
    //             'tinh_trang' => 2,
    //             'updated' => $today
    //         );
    //         if($this->insert("congviec", $data)) {
    //             $event = array('name'=>'Tạo lại công việc chưa hoàn thành: '.$data['name'],
    //                         'nguoi_gui'=>$data['nguoi_giao'], 'nguoi_nhan'=>$data['nhan_vien'],
    //                         'ngay_gio'=>$today, 'tinh_trang'=>0);
    //             $query = $this->insert("events", $event);
    //             $data      = array('tinh_trang' => 5,'updated' => $today);
    //             $query = $this->update("congviec", $data, "id = $id");
    //         }
    //     }
    //     return $query;
    // }

  	// function delObj($id){
    //     $query = $this->delete("congviec", "id = $id");
    //     return $query;
    // }


}
?>

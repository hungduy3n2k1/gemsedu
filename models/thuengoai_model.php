<?php
class thuengoai_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tinhtrang, $tukhoa, $dichvu, $ncc)
    {
        $result = [];
        $dieukien = " WHERE tinh_trang=$tinhtrang  ";
        if ($dichvu > 0)
            $dieukien .= " AND product=$dichvu  ";
        if ($ncc > 0)
            $dieukien .= " AND khach_hang=$ncc  ";
        if ($tukhoa != '') {
            $dieukien .= " AND (name LIKE '%" . $tukhoa . "%' OR link LIKE '%" . $tukhoa . "%' OR ghi_chu LIKE '%" . $tukhoa . "%' ) ";
        }
        // if ($tukhoa != '') {
        //     $query = $this->db->query("SELECT id FROM khachhang WHERE name LIKE '%" . $tukhoa . "%' ");
        //     $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        //     $khachhang = '';
        //     foreach ($temp as $item) {
        //         $khachhang .= $item['id'] . ',';
        //     }
        //     $khachhang = rtrim($khachhang, ',');
        //     $dieukien .= " AND (name LIKE '%" . $tukhoa . "%' OR khach_hang IN (" . $khachhang . ")) ";
        // }
        $query = $this->db->query("SELECT COUNT(*) AS total FROM thuengoai $dieukien ");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $sort = $sort == 'ngaybd' ? 'ngay_bd' : $sort;
        $sort = $sort == 'ngaykt' ? 'ngay_kt' : $sort;
        $query = $this->db->query("SELECT *,
      			IF(ngay_bd='0000-00-00','',DATE_FORMAT(ngay_bd, '%d/%m/%Y')) AS ngaybd,
      			IF(ngay_kt='0000-00-00','',DATE_FORMAT(ngay_kt, '%d/%m/%Y')) AS ngaykt,
      			(SELECT name FROM loaidichvu WHERE id=product) AS dichvu,
      			IF(ngay_thanh_toan='0000-00-00','',DATE_FORMAT(ngay_thanh_toan, '%d/%m/%Y')) AS ngaytt,
      			(SELECT name FROM nhacungcap WHERE id=khach_hang) AS khachhang
      			FROM thuengoai $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $query1 = $this->db->query("SELECT so_tien
                FROM thuengoai $dieukien ");
        $a = $query1->fetchAll(PDO::FETCH_ASSOC);
        $sotien = 0;
       foreach($a as $item) {
          $sotien = $sotien + $item['so_tien'];
       }
       $result['footer'] = array(0=>array('dichvu'=>'Tổng cộng:','so_tien'=>$sotien));
        return $result;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("thuengoai", $data, "id = $id");
        return $query;
    }

    function addObj($data)
    {
        $query = $this->insert("thuengoai", $data);
        return $query;
    }

     function addObj1($data1)
    {
        $query = $this->insert("resource", $data1);
        return $query;
    }

    function giahan($id,$ngaygiahan,$sotien,$taikhoan,$hoadon,$khachhang)
    {
        $data = ['ngay_kt' => $ngaygiahan];
        $query = $this->update("thuengoai", $data, "id = $id");
        if ($query) {
            $query = $this->db->query("SELECT so_du FROM socai WHERE tinh_trang=1 AND tai_khoan=$taikhoan ORDER BY id DESC ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $sodu=$temp[0]['so_du']-$sotien;
            $data = ['ngay_gio' => date("Y-m-d H:i:s"), 'dien_giai'=>"Gia hạn dịch vụ số: ".$id,
                    'khach_hang'=>$khachhang, 'nhan_vien'=>$_SESSION['user']['nhan_vien'], 'tai_khoan'=>$taikhoan,
                    'loai'=>1, 'hach_toan'=>2, 'so_tien'=>$sotien, 'so_du'=>$sodu,'tinh_trang'=>1];
            $query = $this->insert("socai", $data);
            $phieuid=$this->db->lastInsertId();
            if ($query) {
                $query = $this->db->query("SELECT du_no FROM phaitra WHERE tinh_trang=1 AND khach_hang=$khachhang ORDER BY ngay_gio DESC LIMIT 1 ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                $duno = isset($temp[0]['du_no']) ? $temp[0]['du_no'] + $sotien: $sotien;
                $congno = ['ngay_gio' => $data['ngay_gio'], 'khach_hang' => $khachhang , 'so_tien' => $sotien,
                    'du_no' => $duno, 'noi_dung' => $data['dien_giai'], 'tinh_trang' => 1, 'loai_phieu' => 2, 'so_phieu' => $phieuid];
                $query = $this->insert("phaitra", $congno);
            }
        }
        return $query;
    }

}
?>

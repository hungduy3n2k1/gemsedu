<?php
class inbox_model extends model
{
    function __construct()
    {
        parent::__construct();

    }

    function getFetObj($sort, $order, $offset, $rows, $tukhoa,$nhanvien)
    {
        $result   = array();
        $nguoinhan=$_SESSION['user']['nhan_vien'];
        $dieukien = " WHERE nguoi_nhan=$nguoinhan";
        if ($tukhoa != '')
            $dieukien .= " AND (name LIKE '%$tukhoa%')  ";
        if ($nhanvien>0)
            $dieukien .= " AND nguoi_gui=$nhanvien ";
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM events $dieukien ");
        $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *, 
            DATE_FORMAT(ngay_gio,'%d/%m/%Y %H:%i:%s') as ngaygio,
          (SELECT name FROM nhanvien WHERE id=a.nguoi_gui) AS nguoigui
          FROM events a $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    function updateThongBao($data,$id){
        $this->update('events',$data,"id=$id AND tinh_trang=1");
    }
    function updateDaXem($data,$id){
        $this->update('events',$data,"id=$id");
    }
}
?>

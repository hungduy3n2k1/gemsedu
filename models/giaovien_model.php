<?php

class giaovien_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tukhoa)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang=1 ";
        if ($tukhoa != '')
            $dieukien .= " AND name LIKE '%$tukhoa%' ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM giaovien $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, 
            DATE_FORMAT(ngay_sinh,'%d/%m/%Y') as ngaysinh,
            IF(phan_loai=1,'GVNN',IF(phan_loai=2,'GVVN','Trợ giảng')) AS phanloai,
            IF(loai_hinh=1,'Cũ',IF(loai_hinh=2,'Mới','')) AS loaihinh,
            IF(bang_tieng_anh=1,'B2',IF(bang_tieng_anh=2,'C1','')) AS bangtienganh,
            IF(bang_dai_hoc=1,'Có','Không') AS bangdaihoc
            FROM giaovien $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function delObj($id)
    {
        $data = array('tinh_trang' => 0);
        $query = $this->update("giaovien", $data, "id = $id");
        return $query;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("giaovien", $data, "id = $id");
        return $query;
    }

    function addObj($data)
    {
        $query = $this->insert("giaovien", $data);
        return $query;
    }

}

?>

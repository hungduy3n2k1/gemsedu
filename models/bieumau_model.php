<?php
class bieumau_model extends model
{
   function __construct()
   {
       parent::__construct();
   }

   function getFetObj($sort, $order, $offset, $rows,$phanloai)
   {
       $result   = array();
       $dieukien = " WHERE tinh_trang>0 ";
       $query           = $this->db->query("SELECT COUNT(*) AS total FROM bieumau $dieukien ");
       $row             = $query->fetchAll(PDO::FETCH_ASSOC);
       $result['total'] = $row[0]['total'];
       $query           = $this->db->query("SELECT *,
        IF(ngay_cap_nhat='0000-00-00 00:00:00','',DATE_FORMAT(ngay_cap_nhat, '%d/%m/%Y')) AS ngaycapnhat,
        (SELECT name FROM nhanvien WHERE id=bieumau.nhan_vien) AS nguoinhap
        FROM bieumau $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
       $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
       return $result;
   }

   function addObj($data)
   {
       $ok=$this->insert("bieumau", $data);
       return $ok;
   }

   function updateObj($id, $data)
   {
       $query=$this->update("bieumau", $data, "id = $id");
       return $query;
   }

   function content($id)
   {
       $result   = '';
       $query           = $this->db->query("SELECT name, noi_dung FROM bieumau WHERE id=$id ");
       $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
       $result =$temp[0];
       return $result;
   }
    function delObj($id){
        $data=array('tinh_trang'=>0);
        $query = $this->update("bieumau", $data, "id = $id");
        return $query;
    }


}
?>

<?php
class taituc_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt, $phanloai)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang > 0 AND phan_loai_sale IN (2,3) ";
        if ($ngaybd != "")
            $dieukien .= " AND ngay_dang_ky>='$ngaybd' ";
        if ($ngaykt != "")
            $dieukien .= " AND ngay_dang_ky<='$ngaykt 23:59:59' ";
        if($phanloai > 0 )
            $dieukien .= " AND phan_loai_sale = $phanloai ";
        $query = $this->db->query("SELECT id FROM donhang $dieukien");
        $result['total'] = COUNT($query->fetchAll(PDO::FETCH_ASSOC));
        $tonghocphi = 0;
        $tongdathu = 0;
        $tongcanthu = 0;
        $query = $this->db->query("SELECT so_tien AS tonghocphi, phan_loai_sale AS phanloaisale, DATE_FORMAT(ngay_dang_ky,'%d/%m/%Y') AS ngaydangky,
           (SELECT name FROM khachhang WHERE id = khach_hang) AS khachhang,
           (SELECT name FROM hocvien WHERE id = hoc_vien) AS hocvien,
           (SELECT name FROM khoahoc WHERE id = product) AS khoahoc,
           (SELECT name FROM nhanvien WHERE id = nhan_vien) AS nhanvien,
           IFNULL((SELECT SUM(du_no) FROM invoice WHERE don_hang = donhang.id AND tinh_trang > 0),0) AS canthu
           FROM donhang $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result['rows'] as $key => $item){
            $result['rows'][$key]['dathu'] = $item['tonghocphi'] - $item['canthu'];
            $tonghocphi += $item['tonghocphi'];
            $tongcanthu += $item['canthu'];
        }
        $result['footer'] = array(0 => array('khachhang' => 'Tổng cộng', 'tonghocphi' => $tonghocphi, 'dathu' => $tonghocphi - $tongcanthu, 'canthu' => $tongcanthu));
        return $result;
    }

    function getTong()
    {
        $dieukien = " WHERE tinh_trang > 0 ";
        $query = $this->db->query("SELECT SUM(so_tien) as tonghocphi FROM donhang $dieukien AND phan_loai_sale IN (2,3) ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $tonghocphi = $temp[0]['tonghocphi'] ?? 0;
        $query = $this->db->query("SELECT COUNT(hoc_vien) as tttrainghiem FROM donhang $dieukien AND phan_loai_sale = 2 ORDER BY hoc_vien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $tttrainghiem = $temp[0]['tttrainghiem'];
        $query = $this->db->query("SELECT COUNT(hoc_vien) as ttnew FROM donhang $dieukien AND phan_loai_sale = 3 ORDER BY hoc_vien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $ttnew = $temp[0]['ttnew'];
        return $result = array('tonghocphi' => $tonghocphi, 'tttrainghiem' => $tttrainghiem, 'ttnew' => $ttnew);
    }
}

?>
<?php

class thuchi_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt, $tukhoa, $taikhoan)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang=1  ";
        if ($ngaybd != '')
            $dieukien .= " AND ngay_gio>'$ngaybd' ";
        if ($ngaykt != '')
            $dieukien .= " AND ngay_gio<='$ngaykt 23:59:59'  ";
        if ($taikhoan > 0)
            $dieukien .= " AND tai_khoan=$taikhoan ";
        if ($tukhoa != '')
            $dieukien .= " AND (name LIKE '%" . $tukhoa . "%' OR dien_giai LIKE '%" . $tukhoa . "%' ) ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM socai $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *,
           IF(ngay_gio='0000-00-00 00:00:00','',DATE_FORMAT(ngay_gio, '%d/%m/%Y')) AS ngaygio,
           IF(ngay_gio='0000-00-00 00:00:00','',DATE_FORMAT(ngay_gio, '%d/%m/%Y')) AS ngay,
           IF(loai=0,so_tien,'') AS thu, IF(loai=1,so_tien,'') AS chi,
           IF(hach_toan=1,'Doanh thu',IF(hach_toan=2,'Chi phí','Nội bộ')) AS hachtoan,
           (SELECT name FROM nhanvien WHERE id=nhan_vien_sale) as sale,
           (SELECT tinh_trang FROM invoice WHERE id=socai.invoice) as tinhtranginv,
           IF(loai=0,(SELECT name FROM khachhang WHERE id=khach_hang),(SELECT name FROM nhacungcap WHERE id=khach_hang)) AS khachhang
           FROM socai $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongthu = 0;
        $tongchi = 0;
        foreach ($result['rows'] as $item) {
            if($item['thu']>0)
             $tongthu +=$item['thu'];
            if($item['chi']>0)
            $tongchi += $item['chi'];
        }
        $result['footer'] = array(0 => array('dien_giai' => 'Tổng cộng', 'thu' => $tongthu, 'chi' => $tongchi, 'so_du' => 0));
        return $result;
    }

    function addObj($data)
    {
        $taikhoan = $data['tai_khoan'];
        $query = $this->db->query("SELECT so_du FROM socai WHERE tinh_trang=1 AND tai_khoan=$taikhoan ORDER BY id DESC ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($data['loai'] == 0)
            $data['so_du'] = $temp[0]['so_du'] + $data['so_tien'];
        else
            $data['so_du'] = $temp[0]['so_du'] - $data['so_tien'];
        $query = $this->insert("socai", $data);
        $id = $this->db->lastInsertId();
        if ($query) {
            if ($data['khach_hang'] > 1) {
                $khachhang = $data['khach_hang'];
                $sotien = $data['so_tien'];
                if ($data['loai'] == 0) {
                    $query = $this->db->query("SELECT du_no FROM phaithu WHERE tinh_trang=1 AND khach_hang=$khachhang ORDER BY ngay_gio DESC LIMIT 1 ");
                    $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                    $duno = isset($temp[0]['du_no']) ? $temp[0]['du_no'] - $sotien : -$sotien;
                    $congno = ['ngay_gio' => $data['ngay_gio'], 'khach_hang' => $khachhang, 'so_tien' => $sotien,
                        'du_no' => $duno, 'noi_dung' => $data['dien_giai'], 'tinh_trang' => 1, 'loai_phieu' => 2, 'so_phieu' => $id];
                    $query = $this->insert("phaithu", $congno);
                } else {
                    $query = $this->db->query("SELECT du_no FROM phaitra WHERE tinh_trang=1 AND khach_hang=$khachhang ORDER BY ngay_gio DESC LIMIT 1 ");
                    $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                    $duno = isset($temp[0]['du_no']) ? $temp[0]['du_no'] + $sotien : +$sotien;
                    $congno = ['ngay_gio' => $data['ngay_gio'], 'khach_hang' => $khachhang, 'so_tien' => $sotien,
                        'du_no' => $duno, 'noi_dung' => $data['dien_giai'], 'tinh_trang' => 1, 'loai_phieu' => 2, 'so_phieu' => $id];
                    $query = $this->insert("phaitra", $congno);
                }
            }
        }
        return $query;
    }

    function updateObj($id, $data)
    {
        $query = $this->db->query("SELECT * FROM socai WHERE id = $id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $dulieu = json_encode($temp[0]);
        $sotien = $temp[0]['so_tien'];
        $khachhang = $temp[0]['khach_hang'];
        $loai = $temp[0]['loai'];
        if ($data['so_tien'] == $sotien) {
            $query = $this->update("socai", $data, " id= $id ");
        } else {
            $saiso = $data['so_tien'] - $sotien;
            if ($loai == 0)
                $sodudau = $temp[0]['so_du'] + $saiso;
            else
                $sodudau = $temp[0]['so_du'] - $saiso;
            $data['so_du'] = $sodudau;
            $query = $this->update("socai", $data, " id= $id ");
            if ($query) {
                // Cong don so du
                $taikhoan = $temp[0]['tai_khoan'];
                $dieukien = " WHERE tinh_trang=1 AND tai_khoan=$taikhoan AND id>$id ";
                $query = $this->db->query("SELECT id,loai,so_tien,so_du FROM socai $dieukien ORDER BY ngay_gio ");
                if ($query) {
                    $x = $query->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($x AS $item) {
                        if ($item['loai'] == 0)
                            $sodu = $sodudau + $item['so_tien'];
                        else
                            $sodu = $sodudau - $item['so_tien'];
                        $phieuid = $item['id'];
                        $query = $this->db->query("UPDATE socai SET so_du=$sodu WHERE id=$phieuid ");
                        if (!$query) break;
                        $sodudau = $sodu;
                    }
                }
                //Update và cộng dồn công nợ
                if ($khachhang > 1) {
                    if ($loai == 0) {
                        $query = $this->db->query("SELECT * FROM phaithu WHERE tinh_trang=1 AND khach_hang=$khachhang AND loai_phieu=2 AND so_phieu=$id ");
                        $x = $query->fetchAll(PDO::FETCH_ASSOC);
                        $dunocuoi = $x[0]['du_no'] - $saiso;
                        $phaithuid = $x[0]['id'];
                        $phaithu = ['so_tien' => $data['so_tien'], 'du_no' => $dunocuoi];
                        $query = $this->update("phaithu", $phaithu, "id=$phaithuid");
                        $dieukien = " WHERE tinh_trang=1 AND khach_hang=$khachhang AND id>$phaithuid ";
                        $query = $this->db->query("SELECT id,so_tien,loai_phieu FROM phaithu $dieukien ORDER BY id ");
                        if ($query) {
                            $x = $query->fetchAll(PDO::FETCH_ASSOC);
                            foreach ($x AS $item) {
                                if ($item['loai_phieu'] == 1)
                                    $sodu = $dunocuoi + $item['so_tien'];
                                else
                                    $sodu = $dunocuoi - $item['so_tien'];
                                $phaithuid = $item['id'];
                                $query = $this->db->query("UPDATE phaithu SET du_no=$sodu WHERE id=$phaithuid ");
                                if (!$query)
                                    break;
                                $dunocuoi = $sodu;
                            }
                        }
                    } else {
                        $query = $this->db->query("SELECT * FROM phaitra WHERE tinh_trang=1 AND khach_hang=$khachhang AND loai_phieu=2 AND so_phieu=$id ");
                        $x = $query->fetchAll(PDO::FETCH_ASSOC);
                        $dunocuoi = $x[0]['du_no'] - $saiso;
                        $phaitraid = $x[0]['id'];
                        $phaitra = ['so_tien' => $data['so_tien'], 'du_no' => $dunocuoi];
                        $query = $this->update("phaitra", $phaitra, "id=$phaitraid");
                        $dieukien = " WHERE tinh_trang=1 AND khach_hang=$khachhang AND id>$phaitraid ";
                        $query = $this->db->query("SELECT id,so_tien,loai_phieu FROM phaitra $dieukien ORDER BY id ");
                        if ($query) {
                            $x = $query->fetchAll(PDO::FETCH_ASSOC);
                            foreach ($x AS $item) {
                                if ($item['loai_phieu'] == 1)
                                    $sodu = $dunocuoi + $item['so_tien'];
                                else
                                    $sodu = $dunocuoi - $item['so_tien'];
                                $phaitraid = $item['id'];
                                $query = $this->db->query("UPDATE phaitra SET du_no=$sodu WHERE id=$phaitraid ");
                                if (!$query)
                                    break;
                                $dunocuoi = $sodu;
                            }
                        }
                    }
                }
            }
        }
        $nhatky = array(
            'ngay_gio' => date("Y-m-d H:i:s"),
            'user' => $_SESSION['user']['nhan_vien'],
            'doi_tuong' => 'Thu chi',
            'action' => 'Sửa phiếu:' . $dulieu
        );
        $query = $this->insert("nhatky", $nhatky);
        return $query;
    }

    function delObj($id)
    {
        $query = $this->db->query("SELECT * FROM socai WHERE id = $id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $dulieu = json_encode($temp[0]);
        $sotien = $temp[0]['so_tien'];
        $khachhang = $temp[0]['khach_hang'];
        $loai = $temp[0]['loai'];
        $data = ['tinh_trang' => 0];
        $query = $this->update("socai", $data, "id = $id ");
        if ($query) {
            // cong don so du
            if ($loai == 0)
                $sodudau = $temp[0]['so_du'] - $sotien;
            else
                $sodudau = $temp[0]['so_du'] + $sotien;
            $taikhoan = $temp[0]['tai_khoan'];
            $dieukien = " WHERE tinh_trang=1 AND tai_khoan=$taikhoan AND id>$id ";
            $query = $this->db->query("SELECT id,loai,so_tien,so_du FROM socai $dieukien ORDER BY id ");
            if ($query) {
                $x = $query->fetchAll(PDO::FETCH_ASSOC);
                foreach ($x AS $item) {
                    if ($item['loai'] == 0)
                        $sodu = $sodudau + $item['so_tien'];
                    else
                        $sodu = $sodudau - $item['so_tien'];
                    $phieuid = $item['id'];
                    $query = $this->db->query("UPDATE socai SET so_du=$sodu WHERE id=$phieuid ");
                    if (!$query) break;
                    $sodudau = $sodu;
                }
            }
            //Update và cộng dồn công nợ
            if ($khachhang > 1) {
                if ($loai == 0) {
                    $query = $this->db->query("SELECT * FROM phaithu WHERE tinh_trang=1 AND khach_hang=$khachhang AND loai_phieu=2 AND so_phieu=$id ");
                    $x = $query->fetchAll(PDO::FETCH_ASSOC);
                    $dunocuoi = $x[0]['du_no'] - $sotien;
                    $phaithuid = $x[0]['id'];
                    $phaithu = ['tinh_trang' => 0];
                    $query = $this->update("phaithu", $phaithu, "id=$phaithuid");
                    $dieukien = " WHERE tinh_trang=1 AND khach_hang=$khachhang AND id>$phaithuid ";
                    $query = $this->db->query("SELECT id,so_tien,loai_phieu FROM phaithu $dieukien ORDER BY id ");
                    if ($query) {
                        $x = $query->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($x AS $item) {
                            if ($item['loai_phieu'] == 1)
                                $sodu = $dunocuoi + $item['so_tien'];
                            else
                                $sodu = $dunocuoi - $item['so_tien'];
                            $phaithuid = $item['id'];
                            $query = $this->db->query("UPDATE phaithu SET du_no=$sodu WHERE id=$phaithuid ");
                            if (!$query)
                                break;
                            $dunocuoi = $sodu;
                        }
                    }
                } else {
                    $query = $this->db->query("SELECT * FROM phaitra WHERE tinh_trang=1 AND khach_hang=$khachhang AND loai_phieu=2 AND so_phieu=$id ");
                    $x = $query->fetchAll(PDO::FETCH_ASSOC);
                    $dunocuoi = $x[0]['du_no'] + $sotien;
                    $phaitraid = $x[0]['id'];
                    $phaitra = ['tinh_trang' => 0];
                    $query = $this->update("phaitra", $phaitra, "id=$phaitraid");
                    $dieukien = " WHERE tinh_trang=1 AND khach_hang=$khachhang AND id>$phaitraid ";
                    $query = $this->db->query("SELECT id,so_tien,loai_phieu FROM phaitra $dieukien ORDER BY id ");
                    if ($query) {
                        $x = $query->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($x AS $item) {
                            if ($item['loai_phieu'] == 1)
                                $sodu = $dunocuoi + $item['so_tien'];
                            else
                                $sodu = $dunocuoi - $item['so_tien'];
                            $phaitraid = $item['id'];
                            $query = $this->db->query("UPDATE phaitra SET du_no=$sodu WHERE id=$phaitraid ");
                            if (!$query)
                                break;
                            $dunocuoi = $sodu;
                        }
                    }
                }
            }

        }
        $nhatky = array(
            'ngay_gio' => date("Y-m-d H:i:s"),
            'user' => $_SESSION['user']['nhan_vien'],
            'doi_tuong' => 'Thu chi',
            'action' => 'Xóa phiếu:' . $id
        );
        $query = $this->insert("nhatky", $nhatky);
        return $query;
    }

    function chotsodu($ngay, $taikhoan)
    {
        $dieukien = " WHERE tinh_trang=1 AND tai_khoan=$taikhoan AND ngay_gio>='$ngay' ";
        $query = $this->db->query("SELECT id,loai,so_tien,so_du FROM socai $dieukien ORDER BY id ");
        if ($query) {
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $i = 0;
            foreach ($temp AS $item) {
                if ($i == 0) {
                    $sodudau = $item['so_du'];
                } else {
                    if ($item['loai'] == 0)
                        $sodu = $sodudau + $item['so_tien'];
                    else
                        $sodu = $sodudau - $item['so_tien'];
                    $id = $item['id'];
                    $query = $this->db->query("UPDATE socai SET so_du=$sodu WHERE id=$id ");
                    if (!$query)
                        break;
                    $sodudau = $sodu;
                }
                $i++;
            }
        }
        return $query;
    }

    function invoice($khachhang, $tinhtrang)
    {
        $temp = array();
        if ($tinhtrang > 0)
            $dieukien = " WHERE tinh_trang=$tinhtrang AND khach_hang=$khachhang  ";
        else
            $dieukien = " WHERE tinh_trang>0 AND khach_hang=$khachhang  ";
        $query = $this->db->query("SELECT id,noi_dung,
        (SELECT sum(don_gia*so_luong) FROM invoicesub WHERE invoice=a.id) as sotien,
        CONCAT(id,'-',noi_dung) as invname
        FROM invoice a $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function getInvoicesub($invoice)
    {
        $temp = array();
        $query = $this->db->query("SELECT *,
        (SELECT gia_han FROM dichvu WHERE id=a.dich_vu) as giahan,
        (SELECT phan_loai FROM dichvu WHERE id=a.dich_vu) as loaidichvu
        FROM invoicesub a WHERE invoice=$invoice AND tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function updateInvoice($id, $tinhtrang)
    {
        $query = $this->db->query("UPDATE invoice SET tinh_trang = $tinhtrang WHERE id=$id ");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function checkdichvu($invoicesub, $invoice, $dichvu)
    {
        if ($invoicesub != '' & $invoice != '' & $dichvu != '') {
            $query = $this->db->query("SELECT COUNT(1) as total FROM donhang WHERE invoice_sub = $invoicesub AND invoice=$invoice AND product=$dichvu ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            return $temp[0]['total'];
        } else return 1;
    }

    function themdichvu($data)
    {
        $query = $this->insert("donhang", $data);
        return $query;
    }
}

?>

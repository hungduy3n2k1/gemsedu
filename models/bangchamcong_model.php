<?php
class bangchamcong_model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($nam,$thang,$sort, $order, $offset, $rows)
    {
        $result   = array();
        // cập nhật bảng tổng hợp chấm công
        $query = $this->db->query("SELECT id,nhan_vien FROM bangchamcong WHERE thang = '$thang' AND nam = '$nam' ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $thangnam = $nam.'-'.$thang;
        foreach ($temp AS $item) {
            $id = $item['id'];
            $nhanvien = $item['nhan_vien'];
            $query = $this->db->query("SELECT * FROM chamcong WHERE tinh_trang=1 AND ngay LIKE '$thangnam%' AND nhan_vien=$nhanvien ");
            $chamcong = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($chamcong AS $cong) {
                $chamcong = 0;
                $dd = date("d",strtotime($cong['ngay']));
                if (in_array($cong['sang'],[1,2,3,4,5,6,7,11,12]))
                    $chamcong=$chamcong+0.5;
                if (in_array($cong['chieu'],[1,2,3,4,5,6,7,11,12]))
                    $chamcong=$chamcong+0.5;
                $data = array('ngay_'.$dd=>$chamcong);
                $ok = $this->update("bangchamcong", $data, " id=$id ");
            }

        }

        $query = $this->db->query("SELECT *,
            (ngay_01+ngay_02+ngay_03+ngay_04+ngay_05+ngay_06+ngay_07+ngay_08+
            ngay_09+ngay_10+ngay_11+ngay_12+ngay_13+ngay_14+ngay_15+ngay_16+
            ngay_17+ngay_18+ngay_19+ngay_20+ngay_21+ngay_22+ngay_23+ngay_24+
            ngay_25+ngay_26+ngay_27+ngay_28+ngay_29+ngay_30+ngay_31) AS tong,
            (SELECT name FROM nhanvien WHERE id=nhan_vien) AS nhanvien
            FROM bangchamcong WHERE thang = '$thang' AND nam = '$nam' ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result AS $key=>$item) {
            $nhanvien = $item['nhan_vien'];
            $congchuan = $this->workingday($thang,$nam,$nhanvien);
            $result[$key]['ngaycong'] = $congchuan;
        }
        return $result;
    }

    function addObj($thang,$nam) // tạo bảng chấm công hoặc thêm nhân viên mới
    {
        $ok   = false;
        $dieukien = " WHERE tinh_trang IN (1,2,3) AND van_phong>0 AND ca>0 ";
        $query = $this->db->query("SELECT id FROM nhanvien $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp AS $nhanvien) {
            $id = $nhanvien['id'];
            $dieukien = " WHERE thang = '$thang' AND nam = '$nam' AND nhan_vien=$id ";
            $query = $this->db->query("SELECT COUNT(1) AS total FROM bangchamcong $dieukien ");
            $row = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($row[0]['total']==0) {
                $data = array('nhan_vien'=>$id,'thang'=>$thang,'nam'=>$nam);
                $ok = $this->insert("bangchamcong", $data);
            }
        }
        return $ok;
    }

    // function chamcong($thang,$nam)
    // {


    //
              // }
              // $congchuan = $this->workingday($thang,$nam,$nhanvien);
               // // Cập nhật bảng công đoàn (tính đi muộn về sớm)
              // if (isset($chamcong) && (($chamcong==17) || ($chamcong==19))) { // quen cong
              //     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
              //     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
              //     if (isset($temp[0]['id'])) {
              //           $xid = $temp[0]['id'];
              //           $ok = $this->db->query("UPDATE congdoan SET so_lan_qc=so_lan_qc+1 WHERE id=$xid");
              //     } else {
              //           $data = array('nhan_vien'=>$nhanvien,'so_lan_qc'=>1,'thang'=>$thang,'nam'=>$nam);
              //           $ok = $this->insert("congdoan", $data);
              //     }
              // } elseif (isset($chamcong) && ($chamcong==15)) { // di muon
              //     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
              //     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
              //     $sophut = ROUND((strtotime($giovao)-strtotime($cavao))/60);
              //     if (isset($temp[0]['id'])) {
              //           $xid = $temp[0]['id'];
              //           $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
              //     } else {
              //           $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
              //           $ok = $this->insert("congdoan", $data);
              //     }
              // } elseif (isset($chamcong) && ($chamcong==16)) { //ve som
              //     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
              //     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
              //     $sophut = ROUND((strtotime($cara)-strtotime($giora))/60);
              //     if (isset($temp[0]['id'])) {
              //           $xid = $temp[0]['id'];
              //           $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
              //     } else {
              //           $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
              //           $ok = $this->insert("congdoan", $data);
              //     }
              // } elseif (isset($chamcong) && ($chamcong==18)) { //di muon va ve som
              //     $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
              //     $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
              //     $sophut = ROUND((strtotime($cara)-strtotime($giora))/60)+ROUND((strtotime($giovao)-strtotime($cavao))/60);
              //     if (isset($temp[0]['id'])) {
              //           $xid = $temp[0]['id'];
              //           $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
              //     } else {
              //           $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
              //           $ok = $this->insert("congdoan", $data);
              //     }
              // }



    // function chamcong()
    // {
    //               $data = array('tinh_trang'=>1,'cham_cong'=>$chamcong);

    //           // Cập nhật bảng công đoàn (tính đi muộn về sớm)
    //           if (isset($chamcong) && (($chamcong==17) || ($chamcong==19))) { // quen cong
    //               $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
    //               $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
    //               if (isset($temp[0]['id'])) {
    //                     $xid = $temp[0]['id'];
    //                     $ok = $this->db->query("UPDATE congdoan SET so_lan_qc=so_lan_qc+1 WHERE id=$xid");
    //               } else {
    //                     $data = array('nhan_vien'=>$nhanvien,'so_lan_qc'=>1,'thang'=>$thang,'nam'=>$nam);
    //                     $ok = $this->insert("congdoan", $data);
    //               }
    //           } elseif (isset($chamcong) && ($chamcong==15)) { // di muon
    //               $query    = $this->db->query("SELECT id FROM congdoan WHERE nhan_vien=$nhanvien AND thang='$thang' AND nam='$nam' ");
    //               $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
    //               $sophut = ROUND((strtotime($giovao)-strtotime($cavao))/60);
    //               if (isset($temp[0]['id'])) {
    //                     $xid = $temp[0]['id'];
    //                     $ok = $this->db->query("UPDATE congdoan SET so_lan_ms=so_lan_ms+1,so_phut_ms=so_phut_ms+$sophut WHERE id=$xid");
    //               } else {
    //                     $data = array('nhan_vien'=>$nhanvien,'so_lan_ms'=>1,'so_phut_ms'=>$sophut, 'thang'=>$thang,'nam'=>$nam);
    //                     $ok = $this->insert("congdoan", $data);
    //               }
    //       return $ok;
    // }
}
?>

<?php
class phieuchi_Model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $khachhang, $taikhoan)
    {
        $result = [];
        $dieukien = " WHERE  tinh_trang=1 AND loai=1 AND ngay_gio>'$tungay' AND ngay_gio<'$denngay 23:59:59' ";
        if ($khachhang > 0) {
            $dieukien .= " AND khach_hang=$khachhang ";
        }
        if ($taikhoan != '') {
            $dieukien .= " AND tai_khoan=$taikhoan ";
        }
        $query = $this->db->query("SELECT COUNT(id) AS total FROM socai $dieukien");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay_gio,'%d/%m/%Y') AS ngaygio,
            CONCAT('PC-',id) AS sophieu,
            (SELECT name FROM nhanvien WHERE id=nhan_vien) AS nhanvien,
            (SELECT name FROM nhacungcap WHERE id=khach_hang) AS khachhang,
            (SELECT name FROM taikhoan WHERE id = tai_khoan) as taikhoan
            FROM socai $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $doanhthu = 0;
        foreach ($result['rows'] as $item) {
            $doanhthu = $doanhthu + $item['so_tien'];
        }
        $result['footer'] = [0 => ['ngaygio' => 'Tổng cộng', 'so_tien' => $doanhthu]];
        return $result;
    }

    function addObj($data)
    {
        $query = $this->insert("socai", $data);
        if ($query && ($data['name']=='')) {
            $id=$this->db->lastInsertId();
            $query = $this->db->query("UPDATE socai SET name=CONCAT('PC-',id) WHERE id=$id ");
        }
        return $query;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("socai", $data, " id= $id ");
        return $query;
    }

    function delObj($id)
    {
        $data = ['tinh_trang' => 0];
        $query = $this->update("socai", $data, "id = $id ");
        return $query;
    }

    //--------------------------------------


    function invoice($id)
    {
        $temp = [];
        $query = $this->db->query("SELECT * FROM invoice WHERE khach_hang=$id AND tinh_trang>0  ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function getsodu()
    {
        $temp = [];

        $query = $this->db->query("SELECT so_du AS total FROM thuchi WHERE id=(SELECT MAX(id) FROM `thuchi` WHERE tinh_trang=1) ");

        $temp = $query->fetchAll(PDO::FETCH_ASSOC);

        return $temp[0]['total'];
    }

    function getsodu1($id)
    {
        $temp = [];

        $query = $this->db->query("SELECT so_du AS total FROM thuchi WHERE id=$id ");

        $temp = $query->fetchAll(PDO::FETCH_ASSOC);

        return $temp[0]['total'];
    }

    function updateObj1($invoice, $thanhtoan)
    {
        $data = [
            'tinh_trang' => $thanhtoan,
        ];
        $query = $this->update("invoice", $data, " id= $invoice ");
        return $query;
    }

    function addObj1($khachhang, $sotien, $invoice, $noidung)
    {
        $query = $this->db->query("SELECT * FROM phaichi WHERE khach_hang=$khachhang ORDER BY ngay_gio DESC LIMIT 1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $duno = isset($temp[0]['du_no']) ? $sotien + $temp[0]['du_no'] : $sotien;
        $data = ['ngay_gio' => date("Y-m-d H:i:s"), 'khach_hang' => $khachhang, 'so_tien' => $sotien, 'du_no' => $duno, 'noi_dung' => $noidung, 'tinh_trang' => 1, 'loai_phieu' => 2, 'so_phieu' => $invoice];
        $query = $this->insert("phaichi", $data);
        return $query;
    }

    function getsotienold($id)
    {
        $temp = [];

        $query = $this->db->query("SELECT so_tien AS total FROM thuchi WHERE id=$id ");

        $temp = $query->fetchAll(PDO::FETCH_ASSOC);

        return $temp[0]['total'];
    }



    function danhsach($khachhang)
    {
        $query = $this->db->query("SELECT *,DATE_FORMAT(ngay_gio, '%d/%m/%Y %H:%i:%s') AS ngay_gio FROM donhang WHERE khach_hang = $khachhang");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
    function phieuchi($id)
    {
        $query = $this->db->query("SELECT *,DATE_FORMAT(ngay_gio, '%d/%m/%Y %H:%i:%s') AS ngay_gio,
    (SELECT name FROM khachhang WHERE khachhang.id = khach_hang) as khachhang,
    (SELECT dia_chi FROM khachhang WHERE khachhang.id = khach_hang) as diachi,
    (SELECT dien_thoai FROM khachhang WHERE khachhang.id = khach_hang) as dienthoai,
    (SELECT name FROM nhanvien WHERE nhanvien.id = nhan_vien) as nhanvien
     FROM thuchi WHERE id = $id");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp[0];
    }
    // function items($id){
    //   $query = $this->db->query("SELECT * FROM donhang WHERE id = (SELECT chung_tu FROM phieuchi WHERE id = $id)");
    //   $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //   return $temp[0];
    // }
}
?>

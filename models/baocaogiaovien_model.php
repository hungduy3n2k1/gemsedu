<?php

class baocaogiaovien_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $hocvien, $khachhang, $tungay, $denngay)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0  ";
        $listhv = '';
        $query = $this->db->query("SELECT id,`name`,phan_loai FROM giaovien $dieukien ");
        $giaoviens = $query->fetchAll(PDO::FETCH_ASSOC);
        $total = 0;
        $gvvn = 0;
        $gvnn = 0;
        $dangday = 0;
        $nghiday = 0;
        foreach ($giaoviens as $item) {
            $idgv = $item['id'];
            $dieukien = " WHERE tinh_trang > 0 AND giao_vien=$idgv ";
            if ($tungay != "")
                $dieukien .= " AND ngay>='$tungay' ";
            if ($denngay != "")
                $dieukien .= " AND ngay<='$denngay' ";
            $query = $this->db->query("SELECT id FROM lichhoc  $dieukien ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            if ($temp)
                $dangday++;
            else
                $nghiday++;
            if ($item['phan_loai'] == 2)
                $gvvn++;
            else
                $gvnn++;
            $total++;
        }
        $result = [
            ['noidung' => 'Tổng số lượng giáo viên Việt Nam', 'ketqua' => $gvvn],
            ['noidung' => 'Tổng số lượng giáo viên nước ngoài', 'ketqua' => $gvnn],
            ['noidung' => 'Tổng số lượng giáo viên đang dạy', 'ketqua' => $dangday],
            ['noidung' => 'Tổng số lượng giáo viên đã nghỉ', 'ketqua' => $nghiday]
        ];
        return $result;
    }

}

?>

<?php

class lophoc_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function donhang($hocvien)
    {
        $query = $this->db->query("SELECT id,hoc_vien,product,so_buoi,khuyen_mai,FORMAT(so_tien,0) as so_tien,
        (SELECT name FROM hocvien WHERE id=hoc_vien) as hocvien
        FROM donhang WHERE hoc_vien = $hocvien AND tinh_trang > 0");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function getFetObj($sort, $order, $offset, $rows, $tukhoa, $giaovien, $khoahoc, $phanloai, $tinhtrang)
    {
        $result = array();
        if ($tinhtrang != '')
            $dieukien = " WHERE tinh_trang=$tinhtrang ";
        else
            $dieukien = " WHERE tinh_trang>0 ";
        if ($giaovien != '')
            $dieukien .= " AND giao_vien=$giaovien ";
        if ($khoahoc != '')
            $dieukien .= " AND khoa_hoc=$khoahoc ";
        if ($phanloai != '')
            $dieukien .= " AND phan_loai=$phanloai ";
        if ($tukhoa != '')
            $dieukien .= " AND (name LIKE '%" . $tukhoa . "%')";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM lophoc $dieukien ");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $sql = "SELECT *,
            IFNULL((SELECT DATE_FORMAT(MIN(ngay),'%d/%m/%Y') FROM lichhoc WHERE tinh_trang>0 AND lop_hoc=a.id),'') as ngaybatdau,
            IFNULL((SELECT DATE_FORMAT(MAX(ngay),'%d/%m/%Y') FROM lichhoc WHERE tinh_trang>0 AND lop_hoc=a.id),'') as ngayketthuc,
            (SELECT name FROM khoahoc WHERE id=a.khoa_hoc) as khoahoc,
            (SELECT name FROM chinhanh WHERE id=a.chi_nhanh) as coso,
            (SELECT name FROM giaotrinh WHERE id=a.giao_trinh) as giaotrinh,
            (SELECT name FROM nhanvien WHERE id=a.tro_giang) as trogiang,
             (SELECT name FROM nhanvien WHERE id=a.phu_trach) as phutrach,
            (SELECT name FROM giaovien WHERE id=a.giao_vien) as giaovien,
            IF(tinh_trang=1,'Chờ',IF(tinh_trang=2,'Đang học',IF(tinh_trang=3,'Hoàn thành','Hủy'))) as tinhtrang
           FROM lophoc a $dieukien ORDER BY $sort $order LIMIT $offset, $rows";
        $query = $this->db->query($sql);
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result['rows'] as $key => $item) {
            $lophoc = $item['id'];
            $hocvienid = $item['hoc_vien'];
            if ($item['phan_loai'] == 2) {
                $qr = $this->db->query("SELECT hoc_vien FROM saplop WHERE tinh_trang>0 AND lop_hoc=$lophoc
            AND (SELECT tinh_trang FROM hocvien WHERE id = hoc_vien)>0 GROUP BY hoc_vien ");
                $temp = $qr->fetchAll(PDO::FETCH_ASSOC);
                if ($temp)
                    $result['rows'][$key]['hocvien'] = sizeof($temp);
                else
                    $result['rows'][$key]['hocvien'] = 0;
            } else {
                $qr = $this->db->query("SELECT name,id as hoc_vien FROM hocvien WHERE tinh_trang>0 AND id=$hocvienid");
                $temp = $qr->fetchAll(PDO::FETCH_ASSOC);
                if ($temp)
                    $result['rows'][$key]['hocvien'] = $temp[0]['name'];
                else
                    $result['rows'][$key]['hocvien'] = '';
            }
            $malop = 'IG.';
            if ($temp) {
                if (strlen($temp[0]['hoc_vien']) < 4)
                    for ($i = 0; $i < 4 - strlen($temp[0]['hoc_vien']); $i++)
                        $malop .= "0";

                $malop .= $temp[0]['hoc_vien'];
            } else {
                $malop .= $lophoc;
            }
            //Loại hình
            if ($item['phan_loai'] == 2) {
                if ($item['phan_loai_2'] == 1)
                    $malop = "SM";
                elseif ($item['phan_loai_2'] == 2)
                    $malop = "SG";
                elseif ($item['phan_loai_2'] == 3)
                    $malop = "SC";
                else
                    $malop = "SM";
            } elseif ($item['phan_loai'] == 3) {
                if ($item['phan_loai_2'] == 1)
                    $malop = "FM";
                elseif ($item['phan_loai_2'] == 2)
                    $malop = "FG";
                elseif ($item['phan_loai_2'] == 3)
                    $malop = "FC";
                else
                    $malop = "FM";
            } elseif ($item['phan_loai'] == 4) {
                $malop = 'EM';
            }

            //Chi nhánh
            if ($item['phan_loai'] == 2)
                $malop .= '.' . $item['chi_nhanh'] . "." . $item['id'];
            $result['rows'][$key]['malop'] = $malop;
        }
        return $result;
    }

    function getHocVien($sort, $order, $offset, $rows, $tukhoa, $phanloai, $lophoc)
    {
        $result = array();
        $query = $this->db->query("SELECT hoc_vien FROM saplop WHERE tinh_trang>0 AND lop_hoc=$lophoc GROUP BY hoc_vien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $listhv = '';
        if ($temp)
            foreach ($temp as $item)
                $listhv .= $item['hoc_vien'] . ',';
        $listhv = rtrim($listhv, ',');
        if ($listhv != '') {
            $dieukien = " WHERE tinh_trang>0 AND id IN ($listhv)";
            if ($tukhoa != '')
                $dieukien .= " AND (name LIKE '%" . $tukhoa . "%' OR truong_hoc LIKE '%$tukhoa%' OR dien_thoai LIKE '%$tukhoa%') ";
            if ($phanloai > 0)
                $dieukien .= " AND phan_loai=$phanloai ";
            $query = $this->db->query("SELECT COUNT(*) AS total FROM hocvien $dieukien ");
            $row = $query->fetchAll(PDO::FETCH_ASSOC);
            $result['total'] = $row[0]['total'];
            $sql = "SELECT *,
            DATE_FORMAT(ngay_sinh,'%d/%m/%Y') as ngaysinh,
            (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
            (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh
           FROM hocvien a $dieukien ORDER BY $sort $order";
            $query = $this->db->query($sql);
            $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        }
        return $result;
    }

    function checkLich($lophoc)
    {
        $dieukien = " WHERE tinh_trang IN (1,2,3,4,5,6)  AND lop_hoc=$lophoc";
        $query = $this->db->query("SELECT COUNT(id) as total 
            FROM lichhoc $dieukien");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($rows)
            return $rows[0]['total'];
        else
            return 0;
    }

    function get_detail($lophoc, $sort, $order, $offset, $rows)
    {
        $result = array();
        $dieukien = " WHERE khach_hang=$lophoc ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM donhang $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *
           FROM donhang $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("lophoc", $data, "id = $id");
        return $query;
    }

    function addObj($data)
    {
        $query = $this->insert("lophoc", $data);
        if ($query)
            return $this->db->lastInsertId();
        else return 0;
    }

    function delObj($id)
    {
        $data = array('tinh_trang' => 0);
        $query = $this->update("lophoc", $data, "id = $id");
        $this->update("saplop", $data, "lop_hoc = $id");
        $this->update("lichhoc", $data, "lop_hoc = $id");
        return $query;
    }

    function ThemLichHoc($data)
    {
        $query = $this->insert("lichhoc", $data);
        if ($query)
            return $this->db->lastInsertId();
        else return 0;
    }

    function themPH($data)
    {
        $query = $this->insert("khachhang", $data);
        if ($query)
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function themSaplop($data)
    {
        $query = $this->insert("saplop", $data);
        if ($query)
            return $this->db->lastInsertId();
        else return 0;
    }


    function getLichhoc($lophoc, $thang, $nam)
    {
        $data = array();
        $query = $this->db->query("SELECT lich_hoc FROM saplop WHERE lop_hoc=$lophoc AND tinh_trang>0");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $ids = '';
        if ($temp) {
            foreach ($temp as $item)
                $ids .= $item['lich_hoc'] . ',';
            $ids = rtrim($ids, ',');
            $thangnam = $nam . '-' . $thang;
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' AND id IN ($ids) ";
            $query = $this->db->query("SELECT *, (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            (SELECT name FROM giaovien WHERE id=giao_vien) AS giaovien,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc
            FROM lichhoc $dieukien  ORDER BY ngay, gio");
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            $data['rows'] = $rows;
        }
        // $data['footer'] = array(0 => array('1' => 'X: đủ công, P: phép', '2' => 'C: công tác, L: nghỉ lễ', '3' => 'T: tết, B: nghỉ bù', '4' => 'V: nghỉ có lương', '5' => 'K: nghỉ không lương', '6' => 'O: ốm, -: không báo', '7' => 'M: muộn sớm, Q: quên chấm'));
        return $data;
    }

    function updateLichHoc($id,$data){
        if(isset($data['hoc_vien'])) {
            $this->update("lichhoc", $data, "lop_hoc=$id AND tinh_trang>0");
        }else{
            $this->update("lichhoc", $data, "lop_hoc=$id AND tinh_trang=1");
        }
    }

    function updateSapLop($id,$data){
        $this->update("saplop",$data,"lop_hoc=$id AND tinh_trang>0");
    }

}

?>

<?php

class doanhthu_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $thang, $nam, $loaidh, $khachhang)
    {
        $result = array();
        if ($nam != "" && $thang != "") {
            $ngay = "$nam-$thang";
            $dieukien = " WHERE tinh_trang IN (1,2) ";
            if($loaidh > 0 )
                $dieukien .= " AND phan_loai_sale =$loaidh ";
            if($khachhang != '')
                $dieukien .= " AND (SELECT name FROM khachhang WHERE id=khach_hang) LIKE '%$khachhang%' ";
            $query = $this->db->query("SELECT id, IFNULL((SELECT SUM(so_tien) FROM socai WHERE invoice IN (SELECT id FROM invoice WHERE don_hang = donhang.id AND ngay_gio LIKE '$ngay%')),0) AS doanhthu FROM donhang $dieukien HAVING doanhthu > 0");
            $row = $query->fetchAll();
            $result['total'] = count($row);
            $query = $this->db->query("SELECT so_tien,phan_loai_sale,
            (SELECT name FROM khachhang WHERE id=a.khach_hang) AS khachhang,
            (SELECT name FROM hocvien WHERE id=a.hoc_vien) AS hocvien,
            (SELECT ngay_gio FROM socai WHERE invoice IN (SELECT id FROM invoice WHERE don_hang = a.id) ORDER BY ngay_gio DESC LIMIT 1) AS ngaythanhtoan,
            IFNULL((SELECT SUM(so_tien) FROM socai WHERE invoice IN (SELECT id FROM invoice WHERE don_hang = a.id)),0) AS dadong,
            IFNULL((SELECT SUM(so_tien) FROM socai WHERE invoice IN (SELECT id FROM invoice WHERE don_hang = a.id) AND ngay_gio LIKE '$ngay%'),0) AS doanhthu
            FROM donhang a $dieukien HAVING doanhthu > 0 ORDER BY ngaythanhtoan DESC LIMIT $offset, $rows");
            $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
            $tonghocphi = 0;
            $tongdadong = 0;
            $tongdoanhthu = 0;
            foreach ($result['rows'] as $key => $item) {
                $result['rows'][$key]['conlai'] = $item['so_tien'] - $item['dadong'];
                $tonghocphi += $item['so_tien'];
                $tongdadong += $item['dadong'];
                $tongdoanhthu += $item['doanhthu'];
            }
            $result['footer'] = array(0 => array('khachhang' => 'Tổng cộng', 'dadong' => $tongdadong, 'so_tien' => $tonghocphi, 'conlai' => $tonghocphi - $tongdadong, 'doanhthu' => $tongdoanhthu));
        }
        return $result;
    }

    function getjson($sort, $order, $offset, $rows, $team, $thang, $nam)
    {
        $result = array();
        $thangnam = $nam . '-' . $thang;
        $dieukien = " WHERE tinh_trang=1 AND loai=0 AND hach_toan=1 AND ngay_gio LIKE '$thangnam%' ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM socai $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay_gio, '%d/%m/%Y') AS ngay,
           IF((SELECT name FROM khachhang WHERE id=khach_hang),'Khác') AS khachhang
           FROM socai $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongthu = 0;
        foreach ($result['rows'] as $item)
            $tongthu = $tongthu + $item['so_tien'];
        $result['footer'] = array(0 => array('dien_giai' => 'Tổng cộng', 'so_tien' => $tongthu));
        return $result;
    }

    function getTong()
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 ";
        $query = $this->db->query("SELECT SUM(so_tien) as tonghocphi, SUM(du_no) AS conlai
           FROM invoice $dieukien ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}

?>

<?php
class linhvuc_model extends model
{
   function __construct()
   {
       parent::__construct();

   }

   function getFetObj($sort, $order, $offset, $rows)
   {
       $result   = array();
       $dieukien = " WHERE tinh_trang=1 ";
       $query           = $this->db->query("SELECT COUNT(*) AS total FROM linhvuc $dieukien ");
       $row             = $query->fetchAll(PDO::FETCH_ASSOC);
       $result['total'] = $row[0]['total'];
       $query           = $this->db->query("SELECT * FROM linhvuc $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
       $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
       return $result;
   }

   function addObj($data)
   {
       $ok=$this->insert("linhvuc", $data);
       return $ok;
   }

   function updateObj($id, $data)
   {
       $query=$this->update("linhvuc", $data, "id = $id");
       return $query;
   }

   function delObj($id)
   {
       $data  = array('tinh_trang' => 0);
       $ok=true;
       if($this->update("linhvuc", $data, "id = $id")) {
           $data=array(
               'ngay_gio'=>date("Y-m-d H:i:s"),
               'user' => $_SESSION['user']['id'],
               'doi_tuong' => 'Lĩnh vực',
               'action' => 'Xóa lĩnh vực có id = '.$id
           );
           $this->insert('nhatky', $data);
           $ok=true;
       }
       return $ok;
   }
}
?>

<?php

class congno_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $khachhang, $thang, $nam)
    {
        $result = array();
        $thangnam = $nam . "-" . $thang;
        $dieukien = " WHERE tinh_trang > 0 AND ngay LIKE '$thangnam%' AND du_no>0 ";
        if ($khachhang != '')
            $dieukien .= " AND (SELECT khach_hang FROM donhang WHERE tinh_trang IN (1,2) AND id=don_hang)='$khachhang' ";
        $query = $this->db->query("SELECT id FROM invoice $dieukien ");
        $temp = $query->fetchAll();
        $result['total'] = count($temp);
        $query = $this->db->query("SELECT id ,so_tien,du_no,
            DATE_FORMAT(ngay,'%d/%m/%Y') AS hanthanhtoan,
            (SELECT name FROM khachhang WHERE id=(SELECT khach_hang FROM donhang WHERE id=don_hang)) as khachhang,
            (SELECT name FROM hocvien WHERE id=(SELECT hoc_vien FROM donhang WHERE id=don_hang)) as hocvien,
             (SELECT dien_thoai FROM khachhang WHERE id=(SELECT khach_hang FROM donhang WHERE id=don_hang)) as dienthoaikh 
            FROM invoice $dieukien  ORDER BY $sort $order LIMIT $offset,$rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongno = 0;
        $duno = 0;
        $dathanhtoan = 0;
        foreach ($result['rows'] as $key => $item) {
            $result['rows'][$key]['ngaythanhtoan'] = '';
            $tongno += $item['so_tien'];
            $duno += $item['du_no'];
            $invoices = $item['id'];
            $dathanhtoan += $result['rows'][$key]['dathanhtoan'] = $item['so_tien'] - $item['du_no'];
            $qr = $this->db->query("SELECT DATE_FORMAT('%d/%m/%Y',ngay_gio) as ngay_gio FROM socai WHERE invoice IN ($invoices)");
            $rows = $qr->fetchAll(PDO::FETCH_ASSOC);
            if ($rows) {
                foreach ($rows as $thuchi) {
                    $result['rows'][$key]['ngaythanhtoan'] .= $thuchi['ngay_gio'] . ",";
                }
                $result['rows'][$key]['ngaythanhtoan'] = rtrim($result['rows'][$key]['ngaythanhtoan'], ",");
            }
        }
        $result['footer'] = array(0 => array('khachhang' => 'Tổng cộng', 'so_tien' => $tongno,'dathanhtoan'=>$dathanhtoan,'du_no'=>$duno));
        return $result;
    }

    function getjson($sort, $order, $offset, $rows, $team, $thang, $nam)
    {
        $result = array();
        $thangnam = $nam . '-' . $thang;
        $dieukien = " WHERE tinh_trang=1 AND loai=0 AND hach_toan=1 AND ngay_gio LIKE '$thangnam%' ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM socai $dieukien");
        $row = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay_gio, '%d/%m/%Y') AS ngay,
           (SELECT name FROM khachhang WHERE id=khach_hang) AS khachhang
           FROM socai $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongthu = 0;
        foreach ($result['rows'] as $item)
            $tongthu = $tongthu + $item['so_tien'];
        $result['footer'] = array(0 => array('dien_giai' => 'Tổng cộng', 'so_tien' => $tongthu));
        return $result;
    }

    function getTongCongNo($khachhang, $thang, $nam,$loaidh)
    {
        $thangnam = $nam . "-" . $thang;
        $dieukien = " WHERE tinh_trang > 0 AND ngay LIKE '$thangnam%' AND du_no>0 ";
        if ($khachhang != '')
            $dieukien .= " AND (SELECT khach_hang FROM donhang WHERE tinh_trang IN (1,2) AND id=don_hang)='$khachhang' ";
        $query = $this->db->query("SELECT id ,so_tien,du_no
            FROM invoice $dieukien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongno = 0;
        $duno = 0;
        foreach ($temp as $item) {
            $tongno += $item['so_tien'];
            $duno += $item['du_no'];
        }
        $result = array('tongcongno' => $tongno,'dathanhtoan'=>$tongno-$duno,'conlai'=>$duno);
        return $result;
    }

}

?>

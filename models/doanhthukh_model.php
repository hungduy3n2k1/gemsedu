<?php
class doanhthukh_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt)
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 AND loai=0 AND ngay_gio>'$ngaybd' AND ngay_gio<='$ngaykt 23:59:59' ";
        $query           = $this->db->query("SELECT id FROM socai $dieukien GROUP BY khach_hang");
        $row             = $query->fetchAll();
        $result['total'] = count($row);
        $query           = $this->db->query("SELECT 
            sum(so_tien) AS doanhthu,
           IFNULL((SELECT name FROM khachhang WHERE id=khach_hang),'Khác') AS khachhang
           FROM socai $dieukien GROUP BY khach_hang ORDER BY khachhang ASC LIMIT $offset, $rows");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongthu = 0;
        foreach($result['rows'] as $item)
            $tongthu = $tongthu + $item['doanhthu'];
        $result['footer'] = array(0=>array('khachhang'=>'Tổng cộng','doanhthu'=>$tongthu));
        return $result;
    }

    function getjson($sort, $order, $offset, $rows, $team, $thang, $nam)
    {
        $result   = array();
        $thangnam = $nam.'-'.$thang;
        $dieukien = " WHERE tinh_trang=1 AND loai=0 AND hach_toan=1 AND ngay_gio LIKE '$thangnam%' ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM socai $dieukien");
        $row   = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay_gio, '%d/%m/%Y') AS ngay,
           (SELECT name FROM khachhang WHERE id=khach_hang) AS khachhang
           FROM socai $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        $tongthu = 0;
        foreach($result['rows'] as $item)
            $tongthu = $tongthu + $item['so_tien'];
        $result['footer'] = array(0=>array('dien_giai'=>'Tổng cộng','so_tien'=>$tongthu));
        return $result;
    }

}
?>

<?php

class trainghiem_model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tukhoa)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 ";
        $dieukien .= " AND phan_loai = 20 ";
        if ($tukhoa != '')
            $dieukien .= " AND name LIKE '%$tukhoa%' ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM hocvien $dieukien ");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay_sinh,'%d/%m/%Y') as ngaysinh,
            (SELECT name FROM khachhang WHERE id = khach_hang) as khachhang,
            (SELECT dien_thoai FROM khachhang WHERE id = khach_hang) as dienthoai,
            (SELECT name FROM loaidichvu WHERE id=phan_loai) as phanloai,
            (SELECT phu_trach FROM khachhang WHERE id = khach_hang) as nhan_vien,
            (SELECT name FROM nhanvien WHERE id = (SELECT phu_trach FROM khachhang WHERE id = khach_hang)) as nhanvien
            FROM hocvien $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result['rows'] as $key => $item) {
            $idhocvien = $item['id'];
            $query1 = $this->db->query("SELECT 
            (SELECT name FROM lophoc WHERE id=a.lop_hoc) as lophoc
           FROM saplop a WHERE tinh_trang=1 AND hoc_vien=$idhocvien ");
            $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
            $result['rows'][$key]['lophoc'] = '';
            if ($temp) {
                foreach ($temp as $lophoc)
                    $result['rows'][$key]['lophoc'] .= $lophoc['lophoc'] . ',';
                $result['rows'][$key]['lophoc'] = rtrim($result['rows'][$key]['lophoc'], ',');
            }
        }
        return $result;
    }

    function lichhoc($id)
    {
        $result = [];
        $query = $this->db->query("SELECT GROUP_CONCAT(lop_hoc) AS lop FROM saplop WHERE hoc_vien=$id ");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $lophoc = $row[0]['lop'];
        $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(ngay,'%d/%m/%Y'),' ',DATE_FORMAT(gio,'%H:%i'),
                ' <span onclick=\"lichhoc(',id,')\" style=\"color:red; cursor:pointer\">',
                (SELECT name FROM lophoc WHERE id=lop_hoc),'</span>') AS noidung
            FROM lichhoc WHERE tinh_trang IN (1,2,3) AND lop_hoc IN ($lophoc) ");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function updateObj($id, $data)
    {
        $ok = $this->update('hocvien', $data, "id=$id");
        return $ok;
    }

    function updateKH($id,$data)
    {
        $ok = $this->update('khachhang', $data, "id=(SELECT khach_hang FROM hocvien WHERE id=$id)");
        return $ok;
    }

    function getrow($id)
    {
        $result = array();
        $query = $this->db->query("SELECT *, (SELECT phan_loai FROM lophoc WHERE id=lop_hoc) AS phanloai FROM lichhoc WHERE id=$id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $result = $temp[0];
        return $result;
    }

    function lophoc()
    {
        $temp = array();
        $query = $this->db->query("SELECT id,name FROM lophoc WHERE phan_loai = 2 AND tinh_trang = 2 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function sualich($id, $data)
    {
        $ok = $this->update('lichhoc', $data, "id=$id");
        return $ok;
    }

    function getkhachhang($phone)
    {
        $result = '';
        $query = $this->db->query("SELECT name FROM khachhang WHERE dien_thoai=$phone AND tinh_trang>0 LIMIT 1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($temp[0]['name']))
            $result = $temp[0]['name'];
        return $result;
    }

    function addObj($data, $khachhang, $sdt,$lophoc)
    {
        $khachhangid = 0;
        $query = $this->db->query("SELECT id FROM khachhang WHERE dien_thoai=$sdt AND tinh_trang>0 LIMIT 1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($temp[0]['id']))
            $khachhangid = $temp[0]['id'];
        else {
            $datakhachhang = ['name' => $khachhang, 'dien_thoai' => $sdt,'tinh_trang'=>1];
            if ($this->insert('khachhang', $datakhachhang))
                $khachhangid = $this->db->lastInsertId();
        }
        $data['khach_hang'] = $khachhangid;
        $ok = $this->insert('hocvien', $data);
        $idhv = $this->db->lastInsertId();
        $saplop = [
            'hoc_vien'=>$idhv,
            'lop_hoc'=>$lophoc,
            'ngay_bat_dau'=>date('Y-m-d'),
            'ngay_ket_thuc'=>'0000-00-00',
            'tinh_trang'=>1,
        ];
        $ok = $this->insert('saplop', $saplop);
        return $ok;
    }

    function themDonHang($data)
    {
        $ok = $this->insert('donhang', $data);
        if($ok)
            return $this->db->lastInsertId();
        else return 0;
    }

    function updateDonhang($id,$data)
    {
        $ok = $this->update('donhang', $data,"id=$id");
        return $ok;
    }

    function themInvoice($data){
        $ok = $this->insert('invoice', $data);
        if($ok)
            return $this->db->lastInsertId();
        else return 0;
    }

    function phanloai()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM loaidichvu WHERE tinh_trang=1 AND id NOT IN (15,16,18,19,20) ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function phanloaidemo()
    {
        $temp = array();
        $query = $this->db->query("SELECT id, name FROM loaidichvu WHERE tinh_trang=1 AND id IN (15,16,18,19) ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

}

?>

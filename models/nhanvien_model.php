<?php
class nhanvien_model extends model
{
   function __construct()
   {
       parent::__construct();
   }

   function getFetObj($sort, $order, $offset, $rows, $tukhoa,$thang,$nam,$tinhtrang)
   {
       $result   = array();
       $dieukien = " WHERE tinh_trang>0 ";
      // $dieukien .= " AND tinh_trang<5  ";
//       if($tungay!=''){
//           $tungay= functions::convertDate($tungay);
//       }
       if($nam!='' && $thang!=''){
           $thangnam = strtotime('last day of this month',strtotime($nam.'-'.$thang.'-01'));
           $thangnam =  date('Y-m-d',$thangnam);
//           $dieukien .= " AND ngay_di_lam <= '$thangnam' AND ngay_di_lam!='0000-00-00' ";
//           $dieukien .= " AND (ngay_ket_thuc>= '$thangnam' OR ngay_ket_thuc='0000-00-00') ";
       }
       if ($tukhoa != '')
           $dieukien.= " AND name LIKE '%" . $tukhoa . "%' ";
       if ($tinhtrang>0)
           $dieukien.= " AND tinh_trang=$tinhtrang ";
       if ($tinhtrang==0)
           $dieukien.= " AND tinh_trang<5 ";
       $query           = $this->db->query("SELECT COUNT(*) AS total FROM nhanvien $dieukien ");
       $row             = $query->fetchAll(PDO::FETCH_ASSOC);
       $result['total'] = $row[0]['total'];
       $sql = "SELECT *,
           IF(ngay_sinh='0000-00-00 00:00:00','',DATE_FORMAT(ngay_sinh, '%d/%m/%Y')) AS ngaysinh,
           IF(ngay_cap='0000-00-00 00:00:00','',DATE_FORMAT(ngay_cap, '%d/%m/%Y')) AS ngaycap,
           (SELECT name FROM phongban WHERE id=phong_ban LIMIT 1) AS phongban,
           (SELECT name FROM chinhanh WHERE id=van_phong LIMIT 1) AS vanphong,
           (SELECT ca FROM ca WHERE id=a.ca LIMIT 1) AS calamviec,
           IF(ngay_di_lam='0000-00-00 00:00:00','',DATE_FORMAT(ngay_di_lam, '%d/%m/%Y')) AS ngaydilam,
           IF(ngay_ket_thuc='0000-00-00 00:00:00','',DATE_FORMAT(ngay_ket_thuc, '%d/%m/%Y')) AS ngayketthuc
           FROM nhanvien a $dieukien ORDER BY $sort $order LIMIT $offset, $rows";
       $query           = $this->db->query($sql);
       $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
       return $result;
   }

   function get_detail($nhanvien, $sort, $order, $offset, $rows)
   {
       $result          = array();
       $dieukien        = " WHERE khach_hang=$nhanvien ";
       $query           = $this->db->query("SELECT COUNT(*) AS total FROM donhang $dieukien");
       $row             = $query->fetchAll();
       $result['total'] = $row[0]['total'];
       $query           = $this->db->query("SELECT *
           FROM donhang $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
       $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
       return $result;
   }

   function updateObj($id, $data)
   {
       $query = $this->update("nhanvien", $data, "id = $id");
       return $query;
   }

   function addObj($data)
   {
       $query = $this->insert("nhanvien", $data);
       return $query;
   }

   function delObj($id)
   {
       $data = array('tinh_trang'=>0);
       $query = $this->update("nhanvien", $data, "id = $id");
       return $query;
   }

}
?>

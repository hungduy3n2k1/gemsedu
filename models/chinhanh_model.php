<?php
// Module: quản lý chi nhánh
class chinhanh_model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows)
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 ";
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM chinhanh $dieukien ");
        $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT * FROM chinhanh $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function addObj($data)
    {
        $ok=$this->insert("chinhanh", $data);
        return $ok;
    }

    function updateObj($id, $data)
    {
        $query=$this->update("chinhanh", $data, "id = $id");
        return $query;
    }
}
?>

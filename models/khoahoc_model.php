<?php
class khoahoc_model extends model{
    function __construct(){
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows){
        $result = array();
        $query = $this->db->query("SELECT COUNT(*) AS total FROM khoahoc WHERE tinh_trang=1");
        $row = $query->fetchAll();
		    $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, IF(giao_vien=1,'Nước ngoài','Việt Nam') AS phanloai,
            FORMAT(don_gia,0) AS dongia 
            FROM khoahoc WHERE tinh_trang=1 ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

	function delObj($id){
		$data=array('tinh_trang'=>0);
        $query = $this->update("khoahoc", $data, "id = $id");
        return $query;
    }

    function updateObj($id, $data){
        $query = $this->update("khoahoc", $data, "id = $id");
        return $query;
    }

    function addObj($data){
        $query = $this->insert("khoahoc", $data);
        return $query;
    }

}
?>

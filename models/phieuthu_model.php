<?php

class phieuthu_Model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tungay, $denngay, $khachhang, $taikhoan)
    {
        $result = [];
        $dieukien = " WHERE  tinh_trang=1 AND loai=0 AND ngay_gio>'$tungay' AND ngay_gio<'$denngay 23:59:59' ";
        if ($khachhang > 0)
            $dieukien .= " AND khach_hang=$khachhang ";
        if ($taikhoan > 0)
            $dieukien .= " AND tai_khoan=$taikhoan ";
        $query = $this->db->query("SELECT COUNT(id) AS total FROM socai $dieukien");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *, DATE_FORMAT(ngay_gio,'%d/%m/%Y %H:%i:%s') AS ngaygio,
            CONCAT('PT-',id) AS sophieu,
            (SELECT name FROM nhanvien WHERE id=nhan_vien) AS nhanvien,
            (SELECT name FROM khachhang WHERE id=khach_hang) AS khachhang,
            (SELECT name FROM taikhoan WHERE id = tai_khoan) as taikhoan,
            (SELECT don_hang FROM invoice WHERE id = socai.invoice) as donhang,
            (SELECT du_no FROM invoice WHERE id = socai.invoice) as duno
            FROM socai $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        $doanhthu = 0;
        foreach ($result['rows'] as $item) {
            $doanhthu = $doanhthu + $item['so_tien'];
        }
        $result['footer'] = [0 => ['ngaygio' => 'Tổng cộng', 'so_tien' => $doanhthu]];
        return $result;
    }

    function addObj($data)
    {
        // $taikhoan = $data['tai_khoan'];
        // $query = $this->db->query("SELECT so_du FROM socai WHERE tinh_trang=1 AND tai_khoan=$taikhoan ORDER BY id DESC ");
        // $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        // if ($temp)
        //     $data['so_du'] = $temp[0]['so_du'] + $data['so_tien'];
        // else
        //     $data['so_du'] = $data['so_tien'];
        $query = $this->insert("socai", $data);
//        $id=$this->db->lastInsertId();
//        if ($query) {

//            if ($data['khach_hang']>1) {
//                $khachhang = $data['khach_hang'];
//                $sotien = $data['so_tien'];
//                $query = $this->db->query("SELECT du_no FROM phaithu WHERE tinh_trang=1 AND khach_hang=$khachhang ORDER BY ngay_gio DESC LIMIT 1 ");
//                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
//                $duno = isset($temp[0]['du_no']) ? $temp[0]['du_no'] - $sotien: -$sotien;
//                $congno = ['ngay_gio' => date("Y-m-d H:i:s"), 'khach_hang' => $khachhang , 'so_tien' => $sotien,
//                    'du_no' => $duno, 'noi_dung' => $data['dien_giai'], 'tinh_trang' => 1, 'loai_phieu' => 2, 'so_phieu' => $id];
//                $query = $this->insert("phaithu", $congno);
//            }
//        }
        return $query;
    }

    // function updateInvoice1($donhang, $sotien)
    // {
    //     $query = $this->db->query("SELECT id,so_tien,du_no FROM invoice WHERE don_hang=$donhang AND tinh_trang=1 AND du_no>0 ORDER BY ngay ASC ");
    //     $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //     if ($temp)
    //         foreach ($temp as $item) {
    //             $invid = $item['id'];
    //             $duno = $item['du_no'];
    //             if ($sotien > 0) {
    //                 if ($sotien >= $duno) {
    //                     $nomoi = 0;
    //                     $tinhtrang = 3;
    //                 } else {
    //                     $nomoi = $duno - $sotien;
    //                     $tinhtrang = 2;
    //                 }
    //                 $data = ['du_no' => $nomoi, 'tinh_trang' => $tinhtrang];
    //                 $this->update("invoice", $data, "id=$invid");
    //                 $sotien = $sotien - $item['du_no'];
    //             } else
    //                 break;
    //         }
    // }
    

    function updateInvoice($id,$data){
        return  $this->update("invoice", $data, "id = $id ");
    }

    function updateObj($id, $data)
    {
        $query = $this->db->query("SELECT * FROM socai WHERE id = $id ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $dulieu = json_encode($temp[0]);
        $query = $this->update("socai", $data, " id= $id ");
        $nhatky = array(
            'ngay_gio' => date("Y-m-d H:i:s"),
            'user' => $_SESSION['user']['nhan_vien'],
            'doi_tuong' => 'Phiếu thu',
            'action' => 'Sửa phiếu:' . $dulieu
        );
        $query = $this->insert("nhatky", $nhatky);
        return $query;
    }

    function delObj($id)
    {
        $data = ['tinh_trang' => 0];
        $query = $this->update("socai", $data, "id = $id ");
        if ($query) {
            $nhatky = array(
                'ngay_gio' => date("Y-m-d H:i:s"),
                'user' => $_SESSION['user']['nhan_vien'],
                'doi_tuong' => 'Phiếu thu',
                'action' => 'Xóa phiếu:' . $id
            );
            $query = $this->insert("nhatky", $nhatky);
            $query = $this->db->query("SELECT invoice, so_tien FROM socai WHERE id = $id ");
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $sotien = $temp[0]['so_tien'];
            $invoice = $temp[0]['invoice'];
            $dieukien = " WHERE tinh_trang > 0 AND id = $invoice ";
            $query = $this->db->query("UPDATE IGNORE invoice SET du_no = du_no + $sotien, tinh_trang = 1 $dieukien ");
        }
        return $query;
    }

    function donhang($khachhang)
    {
        $query = $this->db->query("SELECT id,hoc_vien,product,khach_hang,so_tien,so_buoi,
        (SELECT name FROM hocvien WHERE id=hoc_vien) as hocvien
        FROM donhang WHERE khach_hang = $khachhang AND tinh_trang>0");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function khachhang($khachhang)
    {
        $query = $this->db->query("SELECT name FROM khachhang WHERE id = $khachhang AND tinh_trang>0");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function invoice($donhang)
    {
        $query = $this->db->query("SELECT id,CONCAT('INV-',id,' (',
        DATE_FORMAT(ngay,'%d/%m/%Y'),')',': ',FORMAT(du_no,0)) as name, du_no
        FROM invoice WHERE don_hang = $donhang AND tinh_trang > 0");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    function checkInv($id)
    {
        $query = $this->db->query("SELECT COUNT(id) as total
            FROM invoice WHERE id = $id AND tinh_trang=1 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp)
            return $temp[0]['total'];
        else
            return 0;
    }

    function updateDonHang($id,$data){
        return  $this->update("donhang", $data, "id = $id ");
    }



    // if (count($invoice)==0)
    //     $query = $this->db->query("SELECT id,
    //         (SELECT ROUND(sum(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(1+thue_suat_vat/100))) FROM invoicesub WHERE invoice=a.id AND tinh_trang=1) AS sotien
    //         FROM invoice a WHERE khach_hang=$khachhang AND tinh_trang IN (1,2,3) ORDER BY ngay ");
    // else {
    //     $ids = implode(',',$invoice);
    //     $query = $this->db->query("SELECT id,
    //         (SELECT ROUND(sum(so_luong*(don_gia-chiet_khau_tm)*(1-chiet_khau_pt/100)*(1+thue_suat_vat/100))) FROM invoicesub WHERE invoice=a.id AND tinh_trang=1) AS sotien
    //         FROM invoice a WHERE khach_hang=$khachhang AND tinh_trang IN (1,2,3) AND id IN ($ids) ORDER BY ngay ");
    // }
    // $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    // foreach ($temp AS $item) {
    //     // if (isset($item['sotien']))
    //         if ($item['sotien']>$sotien) {
    //             $query = $this->db->query("UPDATE invoice SET tinh_trang=3 WHERE id=".$item['id']);
    //             break;
    //         } elseif ($item['sotien']==$sotien) {
    //             $query = $this->db->query("UPDATE invoice SET tinh_trang=4 WHERE id=".$item['id']);
    //             break;
    //         } else {
    //             $query = $this->db->query("UPDATE invoice SET tinh_trang=4 WHERE id=".$item['id']);
    //             $sotien = $sotien - $item['sotien'];
    //         }
    // }

    function phieuthu($id)
    {
        $query = $this->db->query("SELECT *,DATE_FORMAT(ngay_gio, '%d/%m/%Y %H:%i:%s') AS ngay_gio,
     (SELECT name FROM khachhang WHERE khachhang.id = khach_hang) as khachhang,
     (SELECT dia_chi FROM khachhang WHERE khachhang.id = khach_hang) as diachi,
     (SELECT dien_thoai FROM khachhang WHERE khachhang.id = khach_hang) as dienthoai,
     (SELECT name FROM nhanvien WHERE nhanvien.id = nhan_vien) as nhanvien,
     (SELECT don_hang FROM invoice WHERE id=socai.invoice) AS donhang
      FROM socai WHERE id = $id");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp[0];
    }
    // // function items($id){
    //   $query = $this->db->query("SELECT * FROM donhang WHERE id = (SELECT chung_tu FROM phieuthu WHERE id = $id)");
    //   $temp = $query->fetchAll(PDO::FETCH_ASSOC);
    //   return $temp[0];
    // }


    // function updateObj($id,$data,$taikhoan)
    // {
    //       $query = false;
    //       $query = $this->db->query("SELECT so_du FROM socai WHERE id<$id AND tai_khoan=$taikhoan ORDER BY id DESC LIMIT 1 ");
    //       $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
    //       if (isset($temp[0]['so_du']))
    //           $data['so_du'] = ($data['loai'] == 0) ? $temp[0]['so_du'] + $data['so_tien'] : $temp[0]['so_du'] - $data['so_tien'];
    //       else
    //           $data['so_du'] = ($data['loai'] == 0) ? $data['so_tien'] : -$data['so_tien'];
    //       $query = $this->update("socai", $data, "id = $id");
    //       $query = $this->db->query("SELECT so_tien, id, loai FROM socai WHERE id>$id AND tai_khoan=$taikhoan ");
    //       $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
    //       $sodu  = $data['so_du'];
    //       foreach ($temp as $row) {
    //           $sodu  = ($row['loai'] == 0) ? $sodu + $row['so_tien'] : $sodu - $row['so_tien'];
    //           $rowid = $row['id'];
    //           $query = $this->db->query("UPDATE socai SET so_du=$sodu WHERE id=$rowid ");
    //       }
    //       return $query;
    // }
    //
    // function addObj($data)
    // {
    //     $taikhoan=$data['tai_khoan'];
    //     $query = $this->db->query("SELECT so_du FROM socai WHERE tai_khoan=$taikhoan ORDER BY id DESC LIMIT 1 ");
    //     $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
    //     if (isset($temp[0]['so_du']))
    //         $data['so_du'] = ($data['loai'] == 0) ? $temp[0]['so_du'] + $data['so_tien'] : $temp[0]['so_du'] - $data['so_tien'];
    //     else
    //         $data['so_du'] = ($data['loai'] == 0) ? $data['so_tien'] : -$data['so_tien'];
    //     $query = $this->insert("socai", $data);
    //     return $query;
    // }


}

?>

<?php
class nguondata_model extends model
{
   function __construct()
   {
       parent::__construct();

   }

   function getFetObj($sort, $order, $offset, $rows)
   {
       $result   = array();
       $dieukien = " WHERE tinh_trang > 0 ";
       $query           = $this->db->query("SELECT COUNT(*) AS total FROM nguondata $dieukien ");
       $row             = $query->fetchAll(PDO::FETCH_ASSOC);
       $result['total'] = $row[0]['total'];
       $query           = $this->db->query("SELECT * FROM nguondata $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
       $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
       return $result;
   }

   function addObj($data)
   {
       $ok=$this->insert("nguondata", $data);
       return $ok;
   }

   function updateObj($id, $data)
   {
       $query=$this->update("nguondata", $data, "id = $id");
       return $query;
   }

   function delObj($id)
   {
       $data  = array('tinh_trang' => 0);
       $ok=true;
       if($this->update("nguondata", $data, "id = $id")) {
           $data=array(
               'ngay_gio'=>date("Y-m-d H:i:s"),
               'user' => $_SESSION['user']['id'],
               'doi_tuong' => 'Nguồn data',
               'action' => 'Xóa nguồn data có id = '.$id
           );
           $this->insert('nhatky', $data);
           $ok=true;
       }
       return $ok;
   }
}
?>

<?php

class baocaohv2_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $hocvien, $khachhang)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang > 0 ";
        if ($hocvien != '')
            $dieukien .= " AND name LIKE '%$hocvien%' ";
        if ($khachhang != '')
            $dieukien .= " AND khach_hang IN (SELECT id FROM khachhang WHERE name LIKE '%$khachhang%') ";
        $query = $this->db->query("SELECT count(id) as total
        FROM hocvien $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $temp[0]['total'];
        $sql = "SELECT *,
        IF(ngay_sinh!='0000-00-00',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
        (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
        CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id) as mahocvien,
        (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh,
        (SELECT email FROM khachhang WHERE id=a.khach_hang) as emailkh
        FROM hocvien a $dieukien ORDER BY $sort $order LIMIT $offset, $rows";
        $query = $this->db->query($sql);
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result['rows'] as $key => $item) {
            $tongbuoi = 0;
            $dahoc = 0;
            $conlai = 0;
            $listlh = '';
            $idhocvien = $item['id'];
            $query1 = $this->db->query("SELECT lop_hoc
            FROM saplop a WHERE tinh_trang>0 AND hoc_vien = $idhocvien
            AND (SELECT so_tien FROM donhang WHERE id = (SELECT don_hang FROM lophoc WHERE id = a.lop_hoc)) > 0
            GROUP BY lop_hoc");
            $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
            foreach ($temp as $saplop) {
                $lophocid = $saplop["lop_hoc"];
                $listlh .= $lophocid . ",";
            }
            $listlh = rtrim($listlh, ",");
            if($listlh != ''){
                $query1 = $this->db->query("SELECT SUM(so_buoi+buoi_khuyen_mai) as tongbuoi 
                FROM lophoc a WHERE tinh_trang > 0 AND id IN ($listlh)");
                $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                $tongbuoi = $temp[0]['tongbuoi'];
                $result['rows'][$key]['tongbuoi'] = $tongbuoi;
                $query1 = $this->db->query("SELECT MAX(ngay) as ngayketthuc, MIN(ngay) AS ngaybatdau
                FROM lichhoc a WHERE tinh_trang IN (2,3,4,5,6) AND lop_hoc IN ($listlh) ");
                $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                $result['rows'][$key]['ngayketthuc'] = $temp[0]['ngayketthuc'];
                $result['rows'][$key]['ngaybatdau'] = $temp[0]['ngaybatdau'];

                $query1 = $this->db->query("SELECT count(id) as dahoc
                FROM lichhoc a WHERE tinh_trang IN (2,3,4,5,6) AND lop_hoc IN ($listlh) ");
                $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
                if ($temp) {
                    $dahoc = $temp[0]['dahoc'];
                    $result['rows'][$key]['dahoc'] = $dahoc;
                    $result['rows'][$key]['conlai'] = $tongbuoi-$dahoc;
                } else {
                    $result['rows'][$key]['dahoc'] = $dahoc;
                    $result['rows'][$key]['conlai'] = $tongbuoi-$dahoc;
                }
            } else {
                $result['rows'][$key]['tongbuoi'] = 0;
                $result['rows'][$key]['dahoc'] = 0;
                $result['rows'][$key]['conlai'] = 0;
            }
            
        }
        // }
        return $result;
    }

}

?>

<?php
class nhacungcap_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tukhoa)
    {
        $result   = array();
        $dieukien = " WHERE tinh_trang=1 ";
        if ($tukhoa != '')
            $dieukien.= " AND name LIKE '%$tukhoa%' ";
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM nhacungcap $dieukien ");
        $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *, DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay
            FROM nhacungcap $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function get_detail($nhacungcap)
    {
        $result          = array();
        $dieukien        = " WHERE khach_hang=$nhacungcap AND tinh_trang>0 AND tinh_trang<5";
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM donhang $dieukien");
        $row             = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *,
            IF(ngay_bd='0000-00-00','',DATE_FORMAT(ngay_bd, '%d/%m/%Y')) AS ngaybd,
            IF(ngay_kt='0000-00-00','',DATE_FORMAT(ngay_kt, '%d/%m/%Y')) AS ngaykt,
            (SELECT name FROM product WHERE id=product) AS sanpham
            FROM donhang $dieukien ORDER BY ngay_kt");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("nhacungcap", $data, "id = $id");
        return $query;
    }

    function addObj($data,$copy)
    {
        $query = $this->insert("nhacungcap", $data);
        if ($copy) {
            $id=$this->db->lastInsertId();
            $lienhe      = array(
                'name' => $data['dai_dien'],
                'khach_hang' =>$id,
                'chuc_vu' => $data['chuc_vu'],
                'dien_thoai' => $data['dien_thoai'],
                'email' => $data['email'],
                'ghi_chu' => 'Người đại diện',
                'tinh_trang' => 1
            );
            $query = $this->insert("lienhe", $lienhe);
        }
        return $query;
    }

    function lienhe($id)
    {
        $result          = array();
        $dieukien        = " WHERE khach_hang=$id AND tinh_trang=1";
        $query           = $this->db->query("SELECT id, name AS hoten,
            dien_thoai AS dienthoai, email AS emailcts, chuc_vu AS chucvu, ghi_chu AS ghichu
            FROM lienhe $dieukien ");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function addCts($data)
    {
        $query = $this->insert("lienhe", $data);
        return $query;
    }

    function saveCts($id, $data)
    {
        $query = $this->update("lienhe", $data, "id = $id");
        return $query;
    }

    function delObj($id)
    {
        $data = array('tinh_trang'=>0);
        $query = $this->update("nhacungcap", $data, "id = $id");
        return $query;
    }

    function delCts($id)
    {
        $data = array('tinh_trang'=>0);
        $query = $this->update("lienhe", $data, "id = $id");
        return $query;
    }

}

?>

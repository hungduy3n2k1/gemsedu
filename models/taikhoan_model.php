<?php
class taikhoan_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows)
    {
        $result          = array();
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM taikhoan WHERE tinh_trang=1");
        $row             = $query->fetchAll();
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *,
           IFNULL((SELECT so_du FROM socai WHERE tinh_trang=1 AND tai_khoan=a.id
           AND id IN (SELECT MAX(id) FROM socai WHERE tinh_trang=1 GROUP BY tai_khoan)),0) AS sodu
           FROM taikhoan a WHERE tinh_trang=1 ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        $total = 0;
        foreach ($result['rows'] AS $item)
            $total = $total+$item['sodu'];
        $result['footer']  = array(0=>array("name"=>"Tổng cộng","sodu"=>$total));
        return $result;
    }

    function delObj($id)
    {
        $data  = array('tinh_trang' => 0);
        $query = $this->update("taikhoan", $data, "id = $id");
        return $query;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("taikhoan", $data, "id = $id");
        return $query;
    }

    function addObj($data, $sodu)
    {
        $query = $this->insert("taikhoan", $data);
        if($query) {
            $id=$this->db->lastInsertId();
            $data = ['name'=>'PT-1', 'ngay_gio'=>date('Y-m-d H:i:s'), 'dien_giai'=>'Số dư đầu', 'khach_hang'=>1,
            'nhan_vien'=>1, 'tai_khoan'=>$id,'loai'=>0, 'hach_toan'=>'3','so_tien'=>$sodu, 'tinh_trang'=>1];
            $query = $this->insert("socai", $data);
        }
        return $query;
    }

}
?>

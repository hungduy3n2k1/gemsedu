<?php
class resource_model extends model
{
   function __construct()
   {
       parent::__construct();
   }

   function getFetObj($sort, $order, $offset, $rows, $tukhoa,$phanloai,$sohuu,$nhacungcap)
   {
       $result   = array();
       $dieukien = " WHERE tinh_trang=1 ";
       $nv= $_SESSION['user']['nhan_vien'];
       if ($nv>1)
          $dieukien.= " AND ((nhan_vien = '$nv') OR (nhan_vien LIKE '$nv,%') OR (nhan_vien LIKE '%,$nv') OR (nhan_vien LIKE '%,$nv,%') OR (nguoi_tao=$nv)) ";
       if ($tukhoa != '')
           $dieukien .= " AND name LIKE '%" . $tukhoa . "%' ";
       if ($phanloai>0)
           $dieukien .= " AND phan_loai = $phanloai ";
       if ($sohuu>0)
           $dieukien .= " AND chu_so_huu = $sohuu ";
       if ($nhacungcap>0)
           $dieukien .= " AND nha_cung_cap = $nhacungcap ";
       $query           = $this->db->query("SELECT COUNT(*) AS total FROM resource $dieukien ");
       $row             = $query->fetchAll(PDO::FETCH_ASSOC);
       $result['total'] = $row[0]['total'];
       $query           = $this->db->query("SELECT id,name,chu_so_huu,nha_cung_cap,link,ghi_chu,phan_loai,nhan_vien, nguoi_tao,
           (SELECT name FROM nhacungcap WHERE id=nha_cung_cap) AS doitac,
           (SELECT name FROM khachhang WHERE id=chu_so_huu) AS sohuu,
           (SELECT name FROM nhanvien WHERE id=nguoi_tao) AS nguoitao,
           (SELECT name FROM phanloai WHERE id=phan_loai) AS phanloai
           FROM resource $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
       $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
       return $result;
   }

   function getpass($id)
   {
       $return = array();
       $query           = $this->db->query("SELECT ten_dang_nhap, mat_khau FROM resource WHERE id=$id ");
       $row             = $query->fetchAll(PDO::FETCH_ASSOC);
       if (isset($row[0]['mat_khau']))
          $return = $row[0];
       return $return;
   }

   function updateObj($id, $data)
   {
       $query = $this->update("resource", $data, "id = $id");
       return $query;
   }

   function addObj($data)
   {
       $query = $this->insert("resource", $data);
       return $query;
   }

   function delObj($id)
   {
       $data = array('tinh_trang'=>0);
       $query = $this->update("resource", $data, "id = $id");
       if ($query) {
          $data = array(
              'ngay_gio'=>date("Y-m-d H:i:s"),
              'user' => $_SESSION['user']['nhan_vien'],
              'doi_tuong' => 'Tài nguyên',
              'action' => 'Xóa tài nguyên có id='.$id
          );
          $this->insert("nhatky", $data);
       }
       return $query;
   }

}
?>

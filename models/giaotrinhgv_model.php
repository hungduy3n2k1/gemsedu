<?php
class giaotrinhgv_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tukhoa,$giaovien)
    {
        $result   = array();
        $dieukien = " WHERE id IN (SELECT giao_trinh FROM lophoc WHERE giao_vien = $giaovien) AND tinh_trang > 0 ";
        if ($tukhoa != '')
            $dieukien .= " AND (name LIKE '%$tukhoa%' OR level LIKE '%$tukhoa%') ";
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM giaotrinh $dieukien ");
        $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *
        FROM giaotrinh $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}

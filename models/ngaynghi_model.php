<?php
class ngaynghi_model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows,$tungay,$denngay,$nhanvien)
    {
        $result   = array();
        $dieukien = "WHERE  ngay>='$tungay' AND ngay<='$denngay' ";
        if ($nhanvien > 0)
          $dieukien.= " AND nhan_vien=$nhanvien ";
        $query           = $this->db->query("SELECT COUNT(*) AS total FROM ngaynghi $dieukien ");
        $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query           = $this->db->query("SELECT *, DATE_FORMAT(ngay_tao, '%d/%m/%Y') AS ngaytao,
        DATE_FORMAT(ngay, '%d/%m/%Y') AS ngaynghi,
        (SELECT name FROM cong WHERE id = loai_hinh ) as loaihinh,
        (SELECT name FROM nhanvien WHERE id = nhan_vien ) as nhanvien
        FROM ngaynghi $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function champhep($data)
    {
        $query=false;
        $ngay = $data['ngay'];
        $nhanvien = $data['nhan_vien'];
        $query    = $this->db->query("SELECT * FROM ngaynghi WHERE nhan_vien=$nhanvien AND ngay='$ngay' ");
        $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($temp[0]['tinh_trang'])) {
            if ($temp[0]['tinh_trang']==0) {
                $id = $temp[0]['id'];
                $query    = $this->update("ngaynghi", $data, "id = $id");
            }
        } else {
            $query = $this->insert("ngaynghi", $data);
        }
        return $query;
    }

    function duyet($id,$data,$loaihinh,$ngay,$nhanvien)
    {
        $query=$this->update("ngaynghi", $data, "id = $id");
        if ($query && ($data['tinh_trang']==1)) {
            //update table phep
            if (in_array($loaihinh, [6,7,8])) {
                $phep = ($loaihinh==6) ? 1:0.5;
                $query    = $this->db->query("SELECT id FROM phep WHERE nhan_vien=$nhanvien ");
                $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
                if (isset($temp[0]['id'])) {
                    $phepid = $temp[0]['id'];
                    $query    = $this->db->query("UPDATE phep SET phep_da_nghi=phep_da_nghi+$phep WHERE id=$phepid ");
                } else {
                    $phepdata = array('nhan_vien'=>$nhanvien, 'phep_da_nghi'=>$phep);
                    $query = $this->insert("phep", $phepdata);
                }
            }
            //update table chamcong
            $query    = $this->db->query("SELECT id FROM chamcong WHERE nhan_vien=$nhanvien AND ngay='$ngay' ");
            $temp     = $query->fetchAll(PDO::FETCH_ASSOC);
            if (isset($temp[0]['id'])) {
                $congid = $temp[0]['id'];
                $query    = $this->db->query("UPDATE chamcong SET cham_cong=$loaihinh  WHERE id=$congid ");
            } else {
                $congdata = array('nhan_vien'=>$nhanvien, 'ngay'=>$ngay, 'cham_cong'=>$loaihinh);
                $query = $this->insert("chamcong", $congdata);
            }
            // Thông báo
            $event = array('name'=>'Duyệt phép: '.$nhanvien,
                        'nguoi_gui'=>$_SESSION['user']['nhan_vien'], 'nguoi_nhan'=>2,
                        'ngay_gio'=>date("Y-m-d H:i:s"), 'tinh_trang'=>0);
            $query = $this->insert("events", $event);
        }
        return $query;
    }

    function loaiphep() {
        $query    = $this->db->query("SELECT id,name FROM cong WHERE tinh_trang=1 AND id>5 AND id<15  ");
        $rows     = $query->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    function letet() {
        $query    = $this->db->query("SELECT id,name FROM cong WHERE tinh_trang=1 AND id IN(4,5,20) ");
        $rows     = $query->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    function nghile($ngay, $loaihinh,$ghichu)
    {
        $dieukien = " WHERE tinh_trang>0 AND tinh_trang<5 ";
        $query           = $this->db->query("SELECT id, name FROM nhanvien $dieukien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp AS $item) {
            $nhanvien = $item['id'];
            $query = $this->db->query("SELECT id FROM chamcong WHERE nhan_vien=$nhanvien AND ngay = $ngay ");
            $x = $query->fetchAll(PDO::FETCH_ASSOC);
            if (isset($x['id'])) {
                $id = $x['id'];
                $data = array('cham_cong'=>$loaihinh);
                $query = $this->update("chamcong", $data, " id=$id ");
            } else {
                $ca = $this->ca($nhanvien,$ngay);
                $data = array('nhan_vien'=>$nhanvien, 'ngay'=>$ngay, 'ca_vao'=>$ca['vao'], 'ca_ra'=> $ca['ra'], 'ghi_chu'=>$ghichu,  'cham_cong'=>$loaihinh);
                $query = $this->insert("chamcong", $data);
            }
        }
        return $query;
    }
}
?>

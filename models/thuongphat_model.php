<?php
class thuongphat_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $ngaybd, $ngaykt, $tukhoa)
    {
        $result   = array();
        $dieukien = " WHERE ngay>='$ngaybd' AND ngay<='$ngaykt' AND cham_cong=3 AND nhan_vien>1 ";
        // $query           = $this->db->query("SELECT COUNT(*) AS total FROM thuongphat $dieukien");
        // $row             = $query->fetchAll();
        // $result['total'] = $row[0]['total'];
        // $query           = $this->db->query("SELECT *,
        //     (SELECT name FROM nhanvien WHERE id=nhan_vien) AS nhanvien,
        //     (SELECT name FROM noiquy WHERE id=event) AS noiquy
        //     FROM thuongphat $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
            // IF(ngay_gio='0000-00-00 00:00:00','',DATE_FORMAT(ngay_gio, '%d/%m/%Y')) AS ngay,
            // IF(loai=0,so_tien,'') AS thu, IF(loai=1,so_tien,'') AS chi,
        $query           = $this->db->query("SELECT *,
              DATE_FORMAT (ngay,'%d/%m/%Y') as ngay,
              DATE_FORMAT (gio_vao,'%H:%i:%s') as gio,
              '50,000' AS sotien,
              (SELECT name FROM nhanvien WHERE id=nhan_vien) AS nhanvien
              FROM chamcong $dieukien ORDER BY nhan_vien ");
        $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
        $n=sizeof($result['rows']);
        $result['footer'] = array(0=>array('nhanvien'=>'Tổng cộng:','sotien'=>number_format($n*50000)));
        return $result;
    }

    function delObj($id)
    {
        $data  = array('tinh_trang' => 0);
        $query = $this->update("thuongphat", $data, "id = $id");
        return $query;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("thuongphat", $data, "id=$id");
        return $query;
    }

    function addObj($data)
    {

        $query = $this->insert("thuongphat", $data);
        return $query;
    }

}
?>

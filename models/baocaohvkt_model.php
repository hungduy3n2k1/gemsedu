<?php

class baocaohvkt_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tenhocvien, $khachhang, $giaovien, $chuyenmon, $thang, $nam, $phanloaidh, $nhanviensale)
    {
        $result = array();
        $result['rows'] = array();
        if ($nam != "" && $thang != "") {
            $ngay = "$nam-$thang";
            $dieukien = " WHERE tinh_trang IN (1,2) AND (SELECT MAX(ngay) FROM lichhoc WHERE lop_hoc=a.id) LIKE '$ngay%' ";
            if ($phanloaidh == 2)
                $dieukien .= " AND so_buoi < 18 ";
            if ($phanloaidh == 3) 
                $dieukien .= " AND so_buoi >= 18 ";
            if ($giaovien != '') 
                $dieukien .= " AND giao_vien = $giaovien ";
            if ($chuyenmon != '')
                $dieukien .= " AND phu_trach = $chuyenmon ";
            if ($tenhocvien != '')
                $dieukien .= " AND (SELECT name FROM hocvien WHERE id=a.hoc_vien) LIKE '%$tenhocvien%' ";
            if ($khachhang != '')
                $dieukien .= " AND (SELECT name FROM khachhang WHERE id=(SELECT khach_hang FROM hocvien WHERE id=a.hoc_vien)) LIKE '%$khachhang%' ";
            if ($nhanviensale > 0)
                $dieukien .= " AND (SELECT nhan_vien_sale FROM donhang WHERE id=a.don_hang) = $nhanviensale ";

            $query = $this->db->query("SELECT COUNT(id) AS total
            FROM lophoc a $dieukien AND phan_loai_2!=3 "); 
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $result['total'] = $temp[0]['total']; 

            $query = $this->db->query("SELECT *,
            (SELECT name FROM giaovien WHERE id=giao_vien) as giaovien,
            (SELECT name FROM nhanvien WHERE id=phu_trach) as chuyenmon
            FROM lophoc a $dieukien AND phan_loai_2!=3 ORDER BY $sort $order LIMIT $offset, $rows "); 
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($temp as $key => $item) {
                $result['rows'][$key]['giaovien'] = $item['giaovien'];
                $result['rows'][$key]['chuyenmon'] = $item['chuyenmon'];
                if ($item['so_buoi'] < 18) {
                    $result['rows'][$key]['phanloaisale'] = "Tái tục trải nghiệm";
                } else {
                    $result['rows'][$key]['phanloaisale'] = "Tái tục new";
                }
                $hocvienid = $item['hoc_vien'];
                $sql = "SELECT id,name,
                IF(ngay_sinh!='0000-00-00',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
                (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
                CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id) as mahocvien,
                /*(SELECT DATE_FORMAT(ngay_bat_dau,'%d/%m/%Y') FROM saplop 
                WHERE hoc_vien=a.id AND tinh_trang=1 ORDER BY ngay_bat_dau ASC LIMIT 1) as ngaybatdau,*/
                (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh,
                (SELECT email FROM khachhang WHERE id=a.khach_hang) as emailkh
                FROM hocvien a WHERE id=$hocvienid";
                $query = $this->db->query($sql);
                $hocvien = $query->fetchAll(PDO::FETCH_ASSOC);
                $result['rows'][$key]['name'] = $hocvien[0]['name'];
                $result['rows'][$key]['ngaysinh'] = $hocvien[0]['ngaysinh'];
                $result['rows'][$key]['khachhang'] = $hocvien[0]['khachhang'];
                $result['rows'][$key]['mahocvien'] = $hocvien[0]['mahocvien'];
                $result['rows'][$key]['dienthoaikh'] = $hocvien[0]['dienthoaikh'];
                $result['rows'][$key]['emailkh'] = $hocvien[0]['emailkh'];
                $lophocid = $item['id'];
                $query1 = $this->db->query("SELECT MAX(ngay) AS ngayketthuc,MIN(ngay) AS ngaybatdau
                    FROM lichhoc WHERE tinh_trang IN (1,2,3,4,5,6) AND lop_hoc=$lophocid");
                $lichhoc = $query1->fetchAll(PDO::FETCH_ASSOC);
                $result['rows'][$key]['ngaybatdau'] = date("d/m/Y", strtotime($lichhoc[0]['ngaybatdau']));
                $result['rows'][$key]['ngayketthuc'] = date("d/m/Y", strtotime($lichhoc[0]['ngayketthuc']));
                
            }
        }
        return $result;
    }

    function getFetObjAll($tenhocvien, $khachhang, $giaovien, $chuyenmon, $thang, $nam, $phanloaidh, $nhanviensale)
    {
        $result = array();
        $result['rows'] = array();
        if ($nam != "" && $thang != "") {
            $ngay = "$nam-$thang";
            $dieukien = " WHERE tinh_trang IN (1,2) AND (SELECT MAX(ngay) FROM lichhoc WHERE lop_hoc=a.id) LIKE '$ngay%' ";
            if ($phanloaidh == 2)
                $dieukien .= " AND so_buoi < 18 ";
            if ($phanloaidh == 3) 
                $dieukien .= " AND so_buoi >= 18 ";
            if ($giaovien != '') 
                $dieukien .= " AND giao_vien = $giaovien ";
            if ($chuyenmon != '')
                $dieukien .= " AND phu_trach = $chuyenmon ";
            if ($tenhocvien != '')
                $dieukien .= " AND (SELECT name FROM hocvien WHERE id=a.hoc_vien) LIKE '%$tenhocvien%' ";
            if ($khachhang != '')
                $dieukien .= " AND (SELECT name FROM khachhang WHERE id=(SELECT khach_hang FROM hocvien WHERE id=a.hoc_vien)) LIKE '%$khachhang%' ";
            if ($nhanviensale > 0)
                $dieukien .= " AND (SELECT nhan_vien_sale FROM donhang WHERE id=a.don_hang) = $nhanviensale ";

            $query = $this->db->query("SELECT COUNT(id) AS total
            FROM lophoc a $dieukien AND phan_loai_2!=3 "); 
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $result['total'] = $temp[0]['total']; 

            $query = $this->db->query("SELECT *,
            (SELECT name FROM giaovien WHERE id=giao_vien) as giaovien,
            (SELECT name FROM nhanvien WHERE id=phu_trach) as chuyenmon
            FROM lophoc a $dieukien AND phan_loai_2!=3 ORDER BY id DESC"); 
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($temp as $key => $item) {
                $result['rows'][$key]['giaovien'] = $item['giaovien'];
                $result['rows'][$key]['chuyenmon'] = $item['chuyenmon'];
                if ($item['so_buoi'] < 18) {
                    $result['rows'][$key]['phanloaisale'] = "Tái tục trải nghiệm";
                } else {
                    $result['rows'][$key]['phanloaisale'] = "Tái tục new";
                }
                $hocvienid = $item['hoc_vien'];
                $sql = "SELECT id,name,
                IF(ngay_sinh!='0000-00-00',DATE_FORMAT(ngay_sinh,'%d/%m/%Y'),'') as ngaysinh,
                (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
                CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id) as mahocvien,
                /*(SELECT DATE_FORMAT(ngay_bat_dau,'%d/%m/%Y') FROM saplop 
                WHERE hoc_vien=a.id AND tinh_trang=1 ORDER BY ngay_bat_dau ASC LIMIT 1) as ngaybatdau,*/
                (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh,
                (SELECT email FROM khachhang WHERE id=a.khach_hang) as emailkh
                FROM hocvien a WHERE id=$hocvienid";
                $query = $this->db->query($sql);
                $hocvien = $query->fetchAll(PDO::FETCH_ASSOC);
                $result['rows'][$key]['name'] = $hocvien[0]['name'];
                $result['rows'][$key]['ngaysinh'] = $hocvien[0]['ngaysinh'];
                $result['rows'][$key]['khachhang'] = $hocvien[0]['khachhang'];
                $result['rows'][$key]['mahocvien'] = $hocvien[0]['mahocvien'];
                $result['rows'][$key]['dienthoaikh'] = $hocvien[0]['dienthoaikh'];
                $result['rows'][$key]['emailkh'] = $hocvien[0]['emailkh'];
                $lophocid = $item['id'];
                $query1 = $this->db->query("SELECT MAX(ngay) AS ngayketthuc,MIN(ngay) AS ngaybatdau
                    FROM lichhoc WHERE tinh_trang IN (1,2,3,4,5,6) AND lop_hoc=$lophocid");
                $lichhoc = $query1->fetchAll(PDO::FETCH_ASSOC);
                $result['rows'][$key]['ngaybatdau'] = date("d/m/Y", strtotime($lichhoc[0]['ngaybatdau']));
                $result['rows'][$key]['ngayketthuc'] = date("d/m/Y", strtotime($lichhoc[0]['ngayketthuc']));
                
            }
        }
        return $result;
    }

}

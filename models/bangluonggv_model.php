<?php

class bangluonggv_model extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($nam, $thang, $sort, $order, $offset, $rows, $giaovien)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang=1 AND thang='$thang' AND nam='$nam' ";
        if ($giaovien != '')
            $dieukien .= " AND (SELECT name FROM giaovien WHERE id=a.giao_vien) LIKE '%$giaovien%' ";
        $query = $this->db->query("SELECT *,
            (SELECT name FROM giaovien WHERE id=a.giao_vien) as giaovien,
            (SELECT bang_tieng_anh FROM giaovien WHERE id=a.giao_vien) as bangtienganh,
            (SELECT bang_dai_hoc FROM giaovien WHERE id=a.giao_vien) as bangdaihoc,
            (SELECT loai_hinh FROM giaovien WHERE id=a.giao_vien) as loaihinh
            FROM giodayonline a $dieukien");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $key => $item) {
            $mucluongkid = $item['luong_kid'];
            $mucluonggt = $item['luong_giao_tiep'];
            if ($item['bangtienganh'] == 2) {
                $mucluongkid = $mucluongkid + 7000;
                $mucluonggt = $mucluonggt + 7000;
            } elseif ($item['bangtienganh'] == 1) {
                $mucluongkid = $mucluongkid + 5000;
                $mucluonggt = $mucluonggt + 5000;
            }
            if ($item['bangdaihoc'] == 1) {
                $mucluongkid = $mucluongkid + 7000;
                $mucluonggt = $mucluonggt + 7000;
            }
            $result[$key]['luongkid'] = $item['kid'] * $mucluongkid;
            $result[$key]['luonggt'] = $item['giao_tiep'] * $mucluonggt;
            if ($item['loaihinh'] == 1) {
//                if ($item['kid'] > 25) {
//                    $result[$key]['luongkid'] = $item['kid'] * ($mucluongkid + 5000);
//                } else {
//                    $result[$key]['luongkid'] = $item['kid'] * $mucluongkid;
//                }
//                if ($item['giao_tiep'] > 25) {
//                    $result[$key]['luonggt'] = $item['giao_tiep'] * ($mucluonggt + 5000);
//                } else {
//                    $result[$key]['luonggt'] = $item['giao_tiep'] * $mucluonggt;
//                }
                $result[$key]['luongdemo'] = ($item['demo_huy'] * 25000) + ($item['demo_hoan_thanh'] * 50000) + ($item['demo_chot'] * 75000) + ($item['demo_coc'] * 100000);
            } else {
//                if ($item['kid'] > 25) {
////                    $result[$key]['luongkid'] = (25 * $mucluongkid) + (($item['kid'] - 25) * ($mucluongkid + 5000));
////                } else {
////                    $result[$key]['luongkid'] = $item['kid'] * $mucluongkid;
////                }
////                if ($item['giao_tiep'] > 25) {
////                    $result[$key]['luonggt'] = (25 * $mucluonggt) + (($item['giao_tiep'] - 25) * ($mucluonggt + 5000));
////                } else {
////                    $result[$key]['luonggt'] = $item['giao_tiep'] * $mucluonggt;
////                }
                $result[$key]['luongdemo'] = ($item['demo_huy'] * 20000) + ($item['demo_hoan_thanh'] * 40000) + ($item['demo_chot'] * 60000);
            }
            $result[$key]['luongtong'] = $result[$key]['luongkid'] + $result[$key]['luonggt'] + $result[$key]['luongdemo'];
        }
        return $result;
    }


    function lapbangGioday($thang, $nam)
    {
        $namthang = $nam . "-" . $thang;
        $dieukien = " WHERE tinh_trang IN (2,4,5,6) AND ngay LIKE '$namthang%' 
        AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc)=1 ";
        $query = $this->db->query("SELECT id,giao_vien,
            (SELECT name FROM giaovien WHERE id=a.giao_vien) as giaovien,
            (SELECT luong_kid FROM giaovien WHERE id=a.giao_vien) as luongkid,
            (SELECT luong_giao_tiep FROM giaovien WHERE id=a.giao_vien) as luonggiaotiep
            FROM lichhoc a $dieukien GROUP BY giao_vien");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp as $item) {
            $idgv = $item['giao_vien'];
            if ($idgv != 0) {
                $data = [];
                $data["giao_vien"] = $item['giao_vien'];
                $data["luong_kid"] = $item['luongkid'];
                $data["luong_giao_tiep"] = $item['luonggiaotiep'];
                $data["nam"] = $nam;
                $data["thang"] = $thang;
                $thoiluongkid = 0;
                $thoiluonggt = 0;
                $buoidemohuy = 0;
                $buoidemoht = 0;
                $buoidemochot = 0;
                $buoidemococ = 0;
                $qr = $this->db->query("SELECT id,thoi_luong,tinh_trang,
                        (SELECT phan_loai_2 FROM lophoc WHERE id=a.lop_hoc) phanloai2,
                        IFNULL((SELECT min(gio_vao) FROM diemdanh WHERE lich_hoc=a.id),gio) as gio,
                        IFNULL((SELECT max(gio_ra) FROM diemdanh WHERE lich_hoc=a.id),gio_ra) as gio_ra
                        FROM lichhoc a WHERE tinh_trang IN (2,4,5,6) AND ngay LIKE '$namthang%' AND giao_vien=$idgv 
                        AND (SELECT phan_loai FROM lophoc WHERE id=a.lop_hoc)=1");
                $t = $qr->fetchAll(PDO::FETCH_ASSOC);
                if ($t) {
                    foreach ($t as $lh) {
                        $time = $lh['thoi_luong'];
                        $thoiluong = ROUND((strtotime($lh['gio_ra']) - strtotime($lh['gio'])) / 60, 2);
                        if ($thoiluong < 0 || $thoiluong > $time)
                            $thoiluong = ROUND($time, 2);
                        if ($lh['phanloai2'] == 1 && $lh['tinh_trang'] > 2)
                            $thoiluongkid += $thoiluong;
                        elseif ($lh['phanloai2'] == 2 && $lh['tinh_trang'] > 2)
                            $thoiluonggt += $thoiluong;
                        elseif ($lh['phanloai2'] == 3) {
                            if ($lh['tinh_trang'] == 2)
                                $buoidemohuy++;
                            if ($lh['tinh_trang'] == 4)
                                $buoidemoht++;
                            if ($lh['tinh_trang'] == 5)
                                $buoidemochot++;
                            if ($lh['tinh_trang'] == 6)
                                $buoidemococ++;
                        } elseif($lh['tinh_trang']>2)
                            $thoiluongkid += $thoiluong;
                    }
                    $data["kid"] = ROUND($thoiluongkid / 60, 2);
                    $data["giao_tiep"] = ROUND($thoiluonggt / 60, 2);
                    $data["demo_huy"] = $buoidemohuy;
                    $data["demo_hoan_thanh"] = $buoidemoht;
                    $data["demo_chot"] = $buoidemochot;
                    $data["demo_coc"] = $buoidemococ;
                    $qr = $this->db->query("SELECT id FROM giodayonline WHERE tinh_trang=1 AND thang='$thang' AND nam='$nam' AND giao_vien=$idgv");
                    $tp = $qr->fetchAll(PDO::FETCH_ASSOC);
                    //  echo "SELECT id FROM giodayonline WHERE tinh_trang=1 AND thang='$thang' AND nam='$nam' AND giao_vien=$idgv <br>";
                    if ($tp) {
                        $id = $tp[0]['id'];
                        $ok = $this->update("giodayonline", $data, "id=$id");
                    } else {
                        $data["tinh_trang"] = 1;
                        $ok = $this->insert("giodayonline", $data);
                    }
                }
            }
        }
        return $ok;
    }

}

?>

<?php

class vanban_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $folder, $tungay, $denngay, $tukhoa)
    {
        $result = array();
        $userid = $_SESSION['user']['id'];
        $qr = $this->db->query("SELECT thu_muc FROM users WHERE id=$userid");
        $t = $qr->fetchAll(PDO::FETCH_ASSOC);
        $f = rtrim($t[0]['thu_muc'],',');
        if ($_SESSION['user']['id'] == 1)
            $dieukien = " WHERE tinh_trang = 1 ";
        else
            $dieukien = " WHERE tinh_trang = 1 AND folder IN ($f) ";
        if ($folder > 0)
            $dieukien .= " AND folder=$folder ";
        if ($tungay != '')
            $dieukien .= " AND ngay>='$tungay' ";
        if ($denngay != '')
            $dieukien .= " AND ngay<='$denngay' ";
        if ($tukhoa != '')
            $dieukien .= " AND name LIKE '%$tukhoa%' ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM vanban $dieukien ");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $query = $this->db->query("SELECT *,
          DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
          (SELECT name FROM nhanvien WHERE id = nhan_vien) as nhanvien
          FROM vanban a $dieukien ORDER BY $sort $order LIMIT $offset, $rows");
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function themphanloai($data)
    {
        $query = $this->insert("folders", $data);
        $fid = $this->db->lastInsertId();
        $uid = $_SESSION['user']['id'];
        $qr = $this->db->query("SELECT thu_muc FROM users WHERE id=$uid ");
        $row = $qr->fetchAll(PDO::FETCH_ASSOC);
        $thumuc = $row[0]['thu_muc'];
        if($thumuc=='')
            $thumuc=$fid;
        else
            $thumuc.=','.$fid;
        $data = array('thu_muc'=>$thumuc);
        $this->update("user", $data, "id = $uid");
        return $fid;
    }

    function addObj($data)
    {
        $ok = $this->insert("vanban", $data);
        return $ok;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("vanban", $data, "id = $id");
        return $query;
    }

    function updateFolder($id, $data)
    {
        $query = $this->update("folders", $data, "id = $id");
        return $query;
    }

    function delObj($id)
    {
        $data = array('tinh_trang' => 0);
        $query = $this->update("vanban", $data, "id = $id ");
        if ($query) {
            $nhatky = array(
                'ngay_gio' => date("Y-m-d H:i:s"),
                'user' => $_SESSION['user']['id'],
                'doi_tuong' => 'Văn bảnote',
                'action' => 'Xóa văn bản số = ' . $id
            );
            $this->insert('nhatky', $nhatky);
        }
        return $query;
    }

    function bieumau()
    {
        $result = array();
        $query = $this->db->query("SELECT * FROM bieumau WHERE tinh_trang=1 AND phan_loai=2 ");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getFolder($id){
        $result = array();
        $query = $this->db->query("SELECT folder,parentid FROM folders WHERE tinh_trang=1 AND id=$id ");
        if ($query)
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function checkfolder($userid,$folderid){
        $result = array();
        $query = $this->db->query("SELECT COUNT(1) as total FROM users 
        WHERE tinh_trang=1 AND id= $userid 
        AND (thu_muc LIKE '%$folderid%' OR thu_muc LIKE '$folderid%' 
        OR thu_muc LIKE '%$folderid'  OR thu_muc LIKE '$folderid' )");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['total'];
    }

}

?>

<?php
class phaithu_model extends model
{
   function __construct()
   {
       parent::__construct();
   }

   function getFetObj($sort, $order, $offset, $rows)
   {
       $result   = array();
       $query           = $this->db->query("SELECT COUNT(1) AS total FROM phaithu WHERE tinh_trang=1 GROUP BY khach_hang ");
       $row             = $query->fetchAll(PDO::FETCH_ASSOC);
       $result['total'] = $row[0]['total'];
       $query           = $this->db->query("SELECT id, du_no,
            (SELECT name FROM khachhang WHERE id=khach_hang) AS khachhang,
            CONCAT('<a href=\"javascript:void(9)\" onclick=\"chitiet(',khach_hang,')\" style=\"text-decoration:underline\">Xem chi tiết</a>') AS chitiet
            FROM phaithu WHERE id IN (SELECT MAX(id) FROM phaithu WHERE tinh_trang=1 GROUP BY khach_hang) ");
       $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
       return $result;
   }

   function getdetail($khachhang)
   {
       $result          = array();
       $dieukien        = " WHERE tinh_trang=1 AND khach_hang=$khachhang";
       $query           = $this->db->query("SELECT *,
          DATE_FORMAT(ngay_gio,'%d/%m/%Y') AS ngay,
          (SELECT name FROM khachhang WHERE id=khach_hang) AS khachhang,
          IF (loai_phieu=1, so_tien,'') AS no,
          IF (loai_phieu=2, so_tien,'') AS co,
          CONCAT(IF(loai_phieu=1,'INV-','PT-'),so_phieu) AS sophieu
          FROM phaithu $dieukien");
       $result['rows']  = $query->fetchAll(PDO::FETCH_ASSOC);
       $ghino = 0;
       $ghico = 0;
       foreach($result['rows'] AS $item){
            $ghino = $ghino + $item['no'];
            $ghico = $ghico + $item['co'];
       }
       $result['footer']  = array(0=>array('khachhang'=>'Tổng: ','no'=>$ghino,'co'=>$ghico,'du_no'=>0));
       return $result;
   }
}
?>

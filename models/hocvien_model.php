<?php

class hocvien_model extends model
{
    function __construct()
    {
        parent::__construct();
    }

    function getFetObj($sort, $order, $offset, $rows, $tukhoa, $phanloai, $khachhang, $tinhtrang)
    {
        $result = array();
        $dieukien = " WHERE tinh_trang>0 ";
        if ($tukhoa != '')
            $dieukien .= " AND (name LIKE '%$tukhoa%' OR dien_thoai LIKE '%$tukhoa%' OR e_name LIKE '%$tukhoa%') ";
        if ($phanloai > 0)
            $dieukien .= " AND phan_loai=$phanloai ";
        if ($khachhang > 0)
            $dieukien .= " AND khach_hang=$khachhang ";
        if ($tinhtrang > 0)
            $dieukien .= " AND tinh_trang=$tinhtrang ";
        $query = $this->db->query("SELECT COUNT(*) AS total FROM hocvien $dieukien ");
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        $result['total'] = $row[0]['total'];
        $sql = "SELECT *,
            DATE_FORMAT(ngay_sinh,'%d/%m/%Y') as ngaysinh,
            (SELECT name FROM khachhang WHERE id=a.khach_hang) as khachhang,
            CONCAT((SELECT name FROM loaidichvu WHERE id=a.phan_loai),id) as mahocvien,
            /*(SELECT DATE_FORMAT(ngay_bat_dau,'%d/%m/%Y') FROM saplop 
            WHERE hoc_vien=a.id AND tinh_trang=1 ORDER BY ngay_bat_dau ASC LIMIT 1) as ngaybatdau,*/
            (SELECT id FROM lophoc WHERE hoc_vien=a.id ORDER BY id DESC LIMIT 1) AS lop,
            (SELECT dien_thoai FROM khachhang WHERE id=a.khach_hang) as dienthoaikh
           FROM hocvien a $dieukien ORDER BY $sort $order LIMIT $offset, $rows";
        $query = $this->db->query($sql);
        $result['rows'] = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result['rows'] as $key => $item) {
            $idhocvien = $item['id'];
            $query1 = $this->db->query("SELECT COUNT(id) as sobuoi,
            (SELECT name FROM lophoc WHERE id=a.lop_hoc) as lophoc
           FROM saplop a WHERE tinh_trang>0 AND hoc_vien=$idhocvien GROUP BY lop_hoc");
            $temp = $query1->fetchAll(PDO::FETCH_ASSOC);
            $result['rows'][$key]['lophoc'] = '';
            if ($temp) {
                foreach ($temp as $lophoc)
                    $result['rows'][$key]['lophoc'] .= '+ ' . $lophoc['lophoc'] . ' (' . $lophoc['sobuoi'] . ' buổi) <br>';
                $result['rows'][$key]['lophoc'] = rtrim($result['rows'][$key]['lophoc'], '<br>');
            }
        }
        return $result;
    }


    function getLichhoc($hocvien, $thang, $nam)
    {
        $data = array();
        $query = $this->db->query("SELECT lich_hoc FROM saplop WHERE hoc_vien=$hocvien AND tinh_trang>0");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $ids = '';
        if ($temp) {
            foreach ($temp as $item)
                $ids .= $item['lich_hoc'] . ',';
            $ids = rtrim($ids, ',');
            $thangnam = $nam . '-' . $thang;
            $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' AND id IN ($ids) ";
            $query = $this->db->query("SELECT *, (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            DATE_FORMAT(ngay,'%d/%m/%Y') AS ngay,
            (SELECT name FROM giaovien WHERE id=giao_vien) AS giaovien,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc
            FROM lichhoc $dieukien  ORDER BY ngay, gio");
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            $data['rows'] = $rows;
        }
        // $data['footer'] = array(0 => array('1' => 'X: đủ công, P: phép', '2' => 'C: công tác, L: nghỉ lễ', '3' => 'T: tết, B: nghỉ bù', '4' => 'V: nghỉ có lương', '5' => 'K: nghỉ không lương', '6' => 'O: ốm, -: không báo', '7' => 'M: muộn sớm, Q: quên chấm'));
        return $data;
    }

    function getLichhoc1($sort, $order, $offset, $rows, $hocvien, $thang, $nam)
    {
        $qr = $this->db->query("SELECT lop_hoc FROM saplop WHERE hoc_vien=$hocvien AND tinh_trang>0 GROUP BY lop_hoc");
        $temp = $qr->fetchAll(PDO::FETCH_ASSOC);
        $idlop = '';
        foreach ($temp as $item) {
            $idlop .= $item['lop_hoc'] . ',';
        }
        $idlop = rtrim($idlop, ',');
        $data = array();
        $thangnam = $nam . '-' . $thang;
        //     $today = date('Y-m-d');
        $dieukien = " WHERE tinh_trang>0 AND ngay LIKE '$thangnam%' ";
        $query = $this->db->query("SELECT count(id) as total
            FROM lichhoc $dieukien AND lop_hoc IN ($idlop)");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $data['total'] = $temp[0]['total'];
        $query = $this->db->query("SELECT id,gio,lop_hoc,phong_hoc,tinh_trang,
            DATE_FORMAT(ngay,'%d/%m/%Y') as ngayhoc,
            (SELECT name FROM lophoc WHERE id=lop_hoc) AS lophoc,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc
            FROM lichhoc $dieukien AND lop_hoc IN ($idlop) ORDER BY $sort $order LIMIT $offset, $rows");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        $data['rows'] = $rows;
        // $data['footer'] = array(0 => array('1' => 'X: đủ công, P: phép', '2' => 'C: công tác, L: nghỉ lễ', '3' => 'T: tết, B: nghỉ bù', '4' => 'V: nghỉ có lương', '5' => 'K: nghỉ không lương', '6' => 'O: ốm, -: không báo', '7' => 'M: muộn sớm, Q: quên chấm'));
        return $data;
    }

    function getLichhoc2($lophoc, $hocvien)
    {
        $dieukien = " WHERE tinh_trang>0 AND lop_hoc=$lophoc";
        $query = $this->db->query("SELECT id,tinh_trang,gio,
            DATE_FORMAT(ngay,'%d/%m/%Y') as ngay,
            (SELECT name FROM giaovien WHERE id=giao_vien) AS giaovien,
            (SELECT name FROM phonghoc WHERE id=phong_hoc) AS phonghoc,
            (SELECT COUNT(id) FROM saplop WHERE lich_hoc=a.id AND hoc_vien=$hocvien AND tinh_trang>0) AS checkhv,
            IF(tinh_trang=1,'Chờ',IF(tinh_trang=2,'Đang học',IF(tinh_trang=3,'Đã kết thúc',IF(tinh_trang=4,'Hủy','')))) as tinhtrang
            FROM lichhoc a $dieukien ORDER BY ngay ASC");
        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        // $data['footer'] = array(0 => array('1' => 'X: đủ công, P: phép', '2' => 'C: công tác, L: nghỉ lễ', '3' => 'T: tết, B: nghỉ bù', '4' => 'V: nghỉ có lương', '5' => 'K: nghỉ không lương', '6' => 'O: ốm, -: không báo', '7' => 'M: muộn sớm, Q: quên chấm'));
        return $rows;
    }

    function updateObj($id, $data)
    {
        $query = $this->update("hocvien", $data, "id = $id");
        return $query;
    }

    function addObj($data)
    {
        $query = $this->insert("hocvien", $data);
        if ($query)
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function delObj($id)
    {
        $data = array('tinh_trang' => 0);
        $query = $this->update("hocvien", $data, "id = $id");
        return $query;
    }

    function themPH($data)
    {
        $query = $this->insert("khachhang", $data);
        if ($query)
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function themSapLop($data)
    {
        return $query = $this->insert("saplop", $data);
//        if ($query)
//            return $this->db->lastInsertId();
//        else
//            return 0;
    }

    function themLop($data)
    {
        $query = $this->insert("lophoc", $data);
        if ($query)
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function xoasaplop($hocvien, $lophoc, $listlh)
    {
        if ($listlh != '')
            return $query = $this->delete("saplop", "hoc_vien=$hocvien AND lop_hoc=$lophoc AND lich_hoc NOT IN ($listlh)");
        else
            return $query = $this->delete("saplop", "hoc_vien=$hocvien AND lop_hoc=$lophoc");
    }

    function themLichhoc($data)
    {
        $query = $this->insert("lichhoc", $data);
        if ($query)
            return $this->db->lastInsertId();
        else
            return 0;
    }

    function getLopHocByHV($hocvien)
    {
        $temp = array();
        $query = $this->db->query("SELECT lop_hoc 
        FROM saplop a WHERE tinh_trang>0 AND hoc_vien=$hocvien GROUP BY lop_hoc ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        $listlh = '';
        foreach ($temp as $item)
            $listlh .= $item['lop_hoc'] . ',';
        $listlh = rtrim($listlh, ',');
        $query = $this->db->query("SELECT id,name,
        IFNULL((SELECT DATE_FORMAT(MIN(ngay),'%d/%m/%Y') FROM lichhoc WHERE tinh_trang>0 AND lop_hoc=a.id),'') as ngaybatdau,
        IFNULL((SELECT DATE_FORMAT(MAX(ngay),'%d/%m/%Y') FROM lichhoc WHERE tinh_trang>0 AND lop_hoc=a.id),'') as ngayketthuc,
        (SELECT name FROM giaovien WHERE id=a.giao_vien) as giaovien,
        IF(tinh_trang=1,'Chờ',IF(tinh_trang=2,'Đang học',IF(tinh_trang=3,'Đã kết thúc','Hủy'))) as tinhtrang
        FROM lophoc a WHERE tinh_trang>0 AND id IN ($listlh)");
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
//        foreach($result as $key=>$item){
//            $lophoc = $item['id'];
//            $qr = $this->db->query("SELECT id
//        FROM saplop a WHERE tinh_trang>0 AND lop_hoc=$lophoc GROUP BY hoc_vien ");
//            $t = $qr->fetchAll(PDO::FETCH_ASSOC);
//            $result[$key]['hocvien'] = sizeof($t);
//        }
        return $result;
    }

    function lophoc()
    {
        $temp = array();
        $query = $this->db->query("SELECT id,name,
        IFNULL((SELECT DATE_FORMAT(MIN(ngay),'%d/%m/%Y') FROM lichhoc WHERE tinh_trang>0 AND lop_hoc=a.id),'') as ngaybatdau,
        IFNULL((SELECT DATE_FORMAT(MAX(ngay),'%d/%m/%Y') FROM lichhoc WHERE tinh_trang>0 AND lop_hoc=a.id),'') as ngayketthuc
        FROM lophoc a WHERE tinh_trang>0 ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp;
    }

    function checkTenlop($tenlop)
    {
        $temp = array();
        $query = $this->db->query("SELECT id FROM lophoc WHERE tinh_trang>0 AND name='$tenlop' ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp)
            return $temp[0]['id'];
        else
            return 0;
    }

    function checkSaplop($hocvien, $lichhoc)
    {
        $temp = array();
        $query = $this->db->query("SELECT COUNT(1) as total FROM saplop WHERE tinh_trang>0 AND lich_hoc='$lichhoc' AND hoc_vien=$hocvien ");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($temp)
            return $temp[0]['total'];
        else
            return 0;
    }


}

?>

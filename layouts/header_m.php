<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <base href="<?php echo URL; ?>/">
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>GemsEdu - Văn phòng điện tử</title>
    <link rel="stylesheet" type="text/css" href="libs/jquery/themes/metro/easyui.css">
    <link rel="stylesheet" type="text/css" href="libs/jquery/themes/mobile.css">
    <link rel="stylesheet" type="text/css" href="libs/jquery/themes/icon.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <script type="text/javascript" src="libs/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="libs/jquery/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="libs/jquery/jquery.easyui.mobile.js"></script>
    <script type="text/javascript" src="public/js/scripts.js"></script>
    <style>
        * {font-family:Roboto, Arial, sans-serif;}
    </style>
    <script>
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var homnay = dd + '/' + mm + '/' + yyyy;
        var nhanvien = '<?php echo $_SESSION['user']['nhan_vien'] ?>';
        var tennhanvien = '<?php echo $_SESSION['user']['nhanvien'] ?>';
        var baseUrl = "<?php echo URL; ?>";
        // window.onkeydown = function(e) {
        //     if (e.keyCode == 8 && e.target == document.body)
        //         e.preventDefault();
        // }
    		// history.pushState(null, null, location.href);
    		// 	window.onpopstate = function () {
    		// 		history.go(1);
    		// };
    </script>
</head>
<body>
    <div class="easyui-navpanel">
        <header style="background-color:#2471a3">
            <div class="m-toolbar">
                <div class="m-title" style="color:#FFFF">I-Offices - GemsEdu</div>
            </div>
        </header>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <base href="<?php echo URL; ?>/">
    <meta http-equiv="pragma" content="no-cache"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <title>GEMS EDU - Văn phòng điện tử</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL ?>/public/css/roboto.css"/>
    <link rel="stylesheet" type="text/css" href="public/easyui/themes/<?= $_SESSION['themes'] ?>/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="public/easyui/icon.css"/>
    <link rel="stylesheet" type="text/css" href="public/easyui/demo.css"/>
    <link rel="stylesheet" type="text/css" href="public/easyui/color.css"/>
    <link rel="stylesheet" type="text/css" href="public/css/baocao.css"/>
    <script type="text/javascript" src="public/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="public/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="public/js/scripts.js"></script>
    <script type="text/javascript" src="public/easyui/datagrid-detailview.js"></script>
    <script src="js/notification.js"></script>

    <script>
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var homnay = dd + '/' + mm + '/' + yyyy;
        var manhanvien = '<?php echo $_SESSION['user']['nhan_vien'] ?>';
        var baseUrl = "<?php echo URL; ?>";
        window.onkeydown = function (e) {
            if (e.keyCode == 8 && e.target == document.body)
                e.preventDefault();
        }
        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        };
    </script>
</head>
<body class="easyui-layout">
<div data-options="region:'north'" style="height:40px;background:#D2E0F2;overfllow:hidden;" border="false">
    <div class="right-top">
        <?php
        $model = new model();
        $phanloai=1;
        if($_SESSION['user']['nhom']==16)
            $phanloai=2;
        $menu = $model->mainmenu($phanloai);
        foreach ($menu as $item) {
            echo '<a href="javascripts:void(0)" class="easyui-menubutton" style="height:38px; margin-top:1px" id="'.$item['id'].'"
                          data-options="menu:\'#sub-menu-' . $item['id'] . '\', iconCls:\'' . $item['icon'] . '\',plain:false">' . $item['name'] . '</a>
                          <div id="sub-menu-' . $item['id'] . '" style="width:200px;">
                          ';
            $submenu = $model->submenu($item['id']);
            foreach ($submenu as $sub){
                echo '<div data-options="iconCls:\'' . $sub['icon'] . '\', itemHeight:40" ';
                if (isset($_GET['url']) && $_GET['url']!='menu') {
                    if ($sub['link']==$_GET['url']) {
                       echo 'style="background: rgb(102, 204, 255);"';
                    }
                  }
                  echo ' onClick="window.location.href=\'' . $sub['link'] . '\'"> ' . $sub['name'] . '</div>
                  <div class="menu-sep"></div>';

            }
            
            if (isset($_GET['url']) && $_GET['url']!='menu') {
                $check=$model->check($_GET['url']); 
                if($check){
                    if ($check['parentid']==$item['id']) {
                        echo '<script type="text/javascript">document.getElementById("'.$check['parentid'].'").style.background = "rgb(102, 204, 255)";</script>';
                      }
                }
              
           }
            echo '</div>';
        }
        if (!isset($module)) $module = 'CÔNG TY VDATA';
        $inbox = '';
        $events = $model->events();
        if ($events > 0)
            $inbox = "background: #FF6C2C;color: white;";
        ?>
        <a href="javascripts:void(0)" class="easyui-menubutton" style="height:38px; margin-top:1px;<?=$inbox?>"
           data-options="menu:'#sub-account', iconCls:'icon-man',plain:false">
            <?php
            if($_SESSION['user']['nhom']==16)
                echo $_SESSION['user']['giaovien'];
            else
                echo $_SESSION['user']['nhanvien'];
            ?>
        </a>
        <div id="sub-account" style="width:200px;">
            <div data-options="iconCls:'icon-account', itemHeight:40"
                 onClick="window.location.href='index'"> Dashboard
            </div>
            <div class="menu-sep"></div>
            <div data-options="iconCls:'icon-phieuthu', itemHeight:40"
                 onClick="window.location.href='inbox'">Inbox</div>
<!--            <div class="menu-sep"></div>-->
            <div class="menu-sep"></div>
            <div data-options="iconCls:'icon-lock', itemHeight:40"
                 onClick="window.location.href='index/matkhau'"> Change password
            </div>
            <div class="menu-sep"></div>
            <div data-options="iconCls:'icon-logout', itemHeight:40"
                 onClick="window.location.href='index/logout'"> Logout
            </div>
<!--            <div class="menu-sep"></div>-->
        </div>
        <div style="float:right; height:40px; line-height:40px; font-weight:bold; padding-right:20px">
            <span style="color:#FF6C2C">Edu Offices</span> - <span style="color:blue"><?= $module ?></span>
        </div>
    </div>
</div>
<div data-options="region:'center'" style="background:#9CF">

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <base href="<?php echo URL; ?>/">
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <title>VDATA - Văn phòng điện tử</title>
    <link rel="shortcut icon" href="https://velo.vn/images/logo-velo-200x200.png" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL ?>/public/css/roboto.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL ?>/public/css/admin.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL ?>/public/css/baocao.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>/public/easyui/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>/public/easyui/icon.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>/public/easyui/demo.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>/public/easyui/color.css"/>
    <script type="text/javascript" src="<?php echo URL; ?>/public/easyui/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/easyui/jquery.edatagrid.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/easyui/datagrid-detailview.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/easyui/datagrid-scrollview.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>/public/js/scripts.js"></script>
    <script>
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var homnay = dd + '/' + mm + '/' + yyyy;
        var manhanvien = '<?php echo $_SESSION['user']['nhan_vien'] ?>';
        var baseUrl = "<?php echo URL; ?>";
        window.onkeydown = function(e) {
            if (e.keyCode == 8 && e.target == document.body)
                e.preventDefault();
        }
		history.pushState(null, null, location.href);
			window.onpopstate = function () {
				history.go(1);
			};
    </script>
</head>
<body class="easyui-layout">
<div data-options="region:'north'" style="height:40px;background:#D2E0F2;overfllow:hidden;" border="false">
   	<div class="right-top">
      <?php
            $model = new model();
            $menu = $model->mainmenu();
            $role = $model->getmenu($_SESSION['user']['id']);
            foreach ($menu as $item) {
                //if (in_array($item['id'],$role)) {
                    echo '<a href="javascripts:void(0)" class="easyui-menubutton" style="height:38px; margin-top:1px"
                          data-options="menu:\'#sub-menu-'.$item['id'].'\', iconCls:\''.$item['icon'].'\',plain:false">'.$item['name'].'</a>
                          <div id="sub-menu-'.$item['id'].'" style="width:200px;">
                          ';
                    $submenu=$model->submenu($item['id']);
                    foreach ($submenu as $sub)
                      //if (in_array($sub['id'],$role))
                          echo '<div data-options="iconCls:\''.$sub['icon'].'\', itemHeight:40"
                                onClick="window.location.href=\''.$sub['link'].'\'"> '.$sub['name'].'</div>
                                <div class="menu-sep"></div>';
                    echo '</div>';
                //}
            }
             // $model = new model();
             //      $menu=$model->mainmenu();
             //      //if  ($_SESSION['user']['id']==1)
             //          foreach ($menu as $item) {
             //              echo '<a href="javascripts:void(0)" class="easyui-menubutton" style="height:38px; margin-top:1px; margin-right:2px;" data-options="menu:\'#sub-menu-'.$item['id'].'\', iconCls:\''.$item['icon'].'\',plain:false">'.$item['name'].'</a>';
             //              echo '<div id="sub-menu-'.$item['id'].'" style="width:200px;">';
             //              $menucon=$model->submenu($item['id']);
             //              foreach ($menucon as $subitem) {
             //                  echo '<div data-options="iconCls:\''.$subitem['icon'].'\'" onClick="window.location.href=\''.$subitem['link'].'\'"> '.$subitem['name'].'</div><div class="menu-sep"></div>';
             //              }
             //              echo '</div>';
             //          }
        ?>
        <a href="javascripts:void(0)" class="easyui-menubutton" style="height:38px; margin-top:1px"
              data-options="menu:'#sub-account', iconCls:'icon-man',plain:false"><?php echo $_SESSION['user']['nhanvien'] ?></a>
        <div id="sub-account" style="width:200px;">
              <div data-options="iconCls:'icon-account', itemHeight:40"> Dashboard</div>
              <div class="menu-sep"></div>
              <div data-options="iconCls:'icon-lock', itemHeight:40"
                    onClick="window.location.href='index/matkhau'"> Đổi mật khẩu</div>
              <div class="menu-sep"></div>
              <div data-options="iconCls:'icon-logout', itemHeight:40"
                    onClick="window.location.href='index/logout'"> Đăng xuất</div>
              <div class="menu-sep"></div>
        </div>
        <div style="float:right; height:40px; line-height:40px; font-weight:bold; color:#DC4C3F; padding-right:20px">
            	VĂN PHÒNG ĐIỆN TỬ - CÔNG TY VDATA
        </div>
    </div>
</div>
<div data-options="region:'center'" style="background:#9CF">

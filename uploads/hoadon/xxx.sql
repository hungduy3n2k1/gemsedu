-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th7 16, 2020 lúc 03:49 PM
-- Phiên bản máy phục vụ: 10.1.40-MariaDB-cll-lve
-- Phiên bản PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `xvdata43nv_vpdt`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL,
  `icon` text COLLATE utf8_unicode_ci NOT NULL,
  `parentid` int(11) NOT NULL,
  `mobile` tinyint(1) NOT NULL,
  `thu_tu` int(2) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu`
--

INSERT INTO `menu` (`id`, `name`, `link`, `icon`, `parentid`, `mobile`, `thu_tu`, `active`) VALUES
(1, 'Cài đặt', 'caidat', 'icon-setting', 0, 0, 1, 1),
(2, 'Hành chính', 'congcu', 'icon-cut', 0, 0, 2, 1),
(3, 'Menu', 'menu', 'icon-menu', 1, 0, 10, 0),
(4, 'Người dùng', 'user', 'icon-man', 1, 0, 3, 1),
(5, 'Thêm', 'add()', 'icon-add', 93, 1, 1, 1),
(6, 'Báo cáo KPI', 'kpi', 'icon-logout', 10, 1, 3, 1),
(7, 'Phân loại SP dịch vụ', 'dichvu', 'icon-danhmuc', 1, 0, 5, 1),
(8, 'Danh bạ', 'lienhe', 'icon-customer', 2, 0, 3, 1),
(9, 'Loại tài nguyên', 'phanloai', 'icon-nhaphang', 2, 0, 1, 1),
(10, 'Công việc', 'taskman', 'icon-phieuthu', 0, 1, 6, 1),
(11, 'Dự án - Nhóm công việc', 'duan', 'icon-baocao', 10, 1, 1, 1),
(12, 'Danh sách công việc', 'congviec', 'icon-menu', 10, 1, 2, 1),
(13, 'Sale & Marketing', 'sale', 'icon-doanhthu', 0, 1, 4, 1),
(14, 'Khách hàng', 'khachhang', 'icon-customer', 13, 1, 1, 1),
(15, 'Nhà cung cấp', 'nhacungcap', 'icon-donvitinh', 13, 0, 3, 1),
(16, 'Sản phẩm & gói dịch vụ', 'product', 'icon-hanghoa', 1, 0, 6, 1),
(17, 'Đơn hàng', 'donhang', 'icon-thuonghieu', 13, 1, 2, 1),
(18, 'Thuê ngoài', 'thuengoai', 'icon-phieuchi', 13, 0, 4, 1),
(19, 'Nhân sự', 'nhansu', 'icon-khachhang', 0, 1, 3, 1),
(20, 'Hồ sơ nhân sự', 'nhanvien', 'icon-man', 19, 0, 2, 1),
(21, 'Chấm công', 'chamcong', 'icon-cate', 19, 1, 3, 1),
(22, 'Theo dõi phép', 'phep', 'icon-hello', 19, 1, 5, 1),
(23, 'Bảng lương', 'bangluong', 'icon-excel', 19, 0, 7, 1),
(24, 'Tổng hợp chấm công', 'bangchamcong', 'icon-mausac', 19, 0, 4, 1),
(25, 'Tài khoản', 'taikhoan', 'icon-size', 26, 1, 3, 1),
(26, 'Kế toán', 'ketoan', 'icon-sum', 0, 1, 5, 1),
(27, 'Thu chi', 'thuchi', 'icon-phieuchi', 26, 1, 1, 1),
(28, 'Báo cáo', 'baocao', 'icon-baocao', 0, 1, 6, 1),
(29, 'Doanh thu chi phí', 'doanhthu', 'icon-sale', 28, 1, 1, 1),
(30, 'Tài nguyên', 'resource', 'icon-taisan', 2, 0, 2, 1),
(31, 'Biểu mẫu văn bản', 'bieumau', 'icon-cate', 2, 0, 5, 1),
(32, 'Phòng ban', 'phongban', 'icon-phieuchi', 1, 0, 2, 1),
(33, 'Trình độ', 'trinhdo', 'icon-thuonghieu', 19, 0, 7, 1),
(34, 'Sửa', 'edit()', 'icon-edit', 93, 0, 2, 1),
(35, 'Hợp đồng', 'hopdong()', 'icon-phieuchi', 20, 0, 4, 1),
(36, 'Ứng viên', 'ungvien()', 'icon-khachhang', 20, 0, 5, 1),
(37, 'Thêm', 'add()', 'icon-add', 7, 0, 1, 1),
(38, 'Sửa', 'edit()', 'icon-edit', 7, 0, 2, 1),
(39, 'Xóa', 'del()', 'icon-no', 7, 0, 3, 1),
(40, 'Thêm ', 'add()', 'icon-add', 8, 0, 1, 1),
(41, 'Sửa', 'edit()', 'icon-edit', 8, 0, 2, 1),
(42, 'Xóa', 'del()', 'icon-no', 8, 0, 3, 1),
(43, 'Xóa', 'del()', 'icon-no', 93, 0, 3, 1),
(45, 'Sửa', 'edit()', 'icon-edit', 9, 0, 3, 1),
(46, 'Xóa', 'del()', 'icon-no', 9, 0, 4, 1),
(47, 'Xóa', 'del()', 'icon-no', 16, 0, 3, 1),
(48, 'Thêm', 'add()', 'icon-add', 9, 0, 1, 1),
(49, 'Thêm ', 'add()', 'icon-add', 16, 0, 1, 1),
(50, 'Sửa', 'edit()', 'icon-edit', 16, 0, 2, 1),
(51, 'Thêm ', 'add()', 'icon-add', 25, 0, 1, 1),
(52, 'Sửa ', 'edit()', 'icon-edit', 25, 0, 2, 1),
(53, 'Xóa', 'del()', 'icon-no', 25, 0, 3, 1),
(54, 'Thêm', 'add()', 'icon-add', 31, 0, 1, 1),
(55, 'Sửa', 'edit()', 'icon-edit', 31, 0, 2, 1),
(56, 'Xóa', 'duyetmau()', 'icon-no', 31, 0, 3, 1),
(57, 'Thêm', 'add()', 'icon-add', 32, 0, 1, 1),
(58, 'Sửa', 'edit()', 'icon-edit', 32, 0, 2, 1),
(59, 'Xóa', 'del()', 'icon-no', 32, 0, 3, 1),
(60, 'Thêm', 'add()', 'icon-add', 33, 0, 1, 1),
(61, 'Sửa', 'edit90', 'icon-edit', 33, 0, 2, 1),
(62, 'Xóa', 'del()', 'icon-no', 33, 0, 3, 1),
(63, 'Xuất Excel', 'xuatfile()', 'icon-excel', 8, 0, 4, 1),
(64, 'Thêm ', 'add()', 'icon-add', 4, 0, 1, 1),
(65, 'Sửa', 'edit()', 'icon-edit', 4, 0, 2, 1),
(66, 'Thêm ', 'add()', 'icon-add', 11, 0, 1, 1),
(67, 'Sửa', 'edit()', 'icon-edit', 11, 0, 2, 1),
(68, 'Xóa', 'del()', 'icon-no', 11, 0, 3, 1),
(69, 'Thêm ', 'add()', 'icon-add', 12, 0, 1, 1),
(70, 'Sửa', 'edit()', 'icon-edit', 12, 0, 2, 1),
(71, 'Xóa', 'del()', 'icon-no', 12, 0, 3, 1),
(72, 'Thêm', 'add()', 'icon-add', 14, 0, 1, 1),
(73, 'Sửa', 'edit()', 'icon-edit', 14, 0, 2, 1),
(74, 'Xóa', 'del()', 'icon-no', 14, 0, 3, 1),
(75, 'Thêm ', 'add()', 'icon-add', 17, 0, 1, 1),
(76, 'Sửa', 'edit()', 'icon-edit', 17, 0, 2, 1),
(77, 'Xóa', 'del()', 'icon-no', 17, 0, 3, 1),
(78, 'Xuất Excel', 'xuatfile()', 'icon-excel', 15, 0, 1, 1),
(79, 'Thêm', 'add()', 'icon-add', 15, 0, 2, 1),
(80, 'Sửa', 'edit()', 'icon-edit', 15, 0, 3, 1),
(81, 'Xuất Excel', 'xuatfile()', 'icon-excel', 18, 0, 1, 1),
(83, 'Thêm ', 'add()', 'icon-add', 18, 0, 2, 1),
(84, 'Sửa', 'edit()', 'icon-edit', 18, 0, 2, 1),
(85, 'Thêm ', 'add()', 'icon-add', 20, 0, 1, 1),
(86, 'Sửa', 'edit()', 'icon-edit', 20, 0, 2, 1),
(87, 'Xóa', 'del()', 'icon-no', 20, 0, 3, 1),
(88, 'Chấm công', 'chamcong()', 'icon-hanghoa', 21, 0, 1, 1),
(89, 'Phép/công tác', 'champhep()', 'icon-hello', 119, 0, 1, 1),
(90, 'Điều chỉnh phép', 'edit()', 'icon-edit', 22, 0, 1, 1),
(92, 'Thêm', 'add()', 'icon-add', 27, 0, 1, 1),
(93, 'Thông tin công ty', 'thongtin', 'icon-mausac', 1, 0, 1, 1),
(94, 'Xóa', 'del()', 'icon-no', 27, 0, 3, 1),
(95, 'Thêm ', 'add()', 'icon-add', 30, 0, 1, 1),
(96, 'Sửa', 'edit()', 'icon-edit', 30, 0, 2, 1),
(97, 'Xóa', 'del()', 'icon-no', 30, 0, 3, 1),
(98, 'Đã xong', 'ketthuc()', 'icon-ok', 12, 0, 5, 1),
(99, 'Điều chỉnh', 'dieuchinh()', 'icon-account', 12, 0, 4, 1),
(100, 'Lập bảng lương', 'add()', 'icon-add', 23, 0, 1, 1),
(101, 'Xóa bảng lương', 'del()', 'icon-no', 23, 0, 2, 1),
(102, 'Điều chỉnh bảng lương', 'edit()', 'icon-edit', 23, 0, 3, 1),
(103, 'Gửi phiếu lương', 'guiphieu()', 'icon-email', 23, 0, 4, 1),
(104, 'Xuất Excel', 'xuatfile()', 'icon-excel', 23, 0, 5, 1),
(105, 'Điều chỉnh', 'edit()', 'icon-edit', 27, 0, 4, 1),
(106, 'Mật khẩu', 'matkhau()', 'icon-lock', 30, 0, 4, 1),
(107, 'Chia sẻ', 'chiase()', 'icon-customer', 30, 0, 5, 1),
(108, 'Quyền truy cập', 'phanquyen()', 'icon-man', 4, 0, 4, 1),
(110, 'Phân ca', 'ca', 'icon-hanghoa', 19, 0, 8, 1),
(111, 'Thêm', 'add()', 'icon-add', 110, 0, 1, 1),
(112, 'Sửa', 'edit()', 'icon-edit', 110, 0, 2, 1),
(113, 'Xóa', 'del()', 'icon-no', 110, 0, 3, 1),
(114, 'Xóa', 'del()', 'icon-no', 4, 0, 3, 1),
(115, 'Hồ sơ ứng viên', 'ungvien', 'icon-donvitinh', 19, 0, 1, 1),
(116, 'Thêm', 'add()', 'icon-add', 115, 0, 1, 1),
(117, 'Sửa', 'edit()', 'icon-edit', 115, 0, 2, 1),
(118, 'Xóa', 'del()', 'icon-no', 115, 0, 3, 1),
(119, 'Quản lý ngày nghỉ', 'ngaynghi', 'icon-man', 19, 0, 6, 1),
(120, 'Chi nhánh', 'chinhanh', 'icon-thuonghieu', 1, 0, 2, 1),
(121, 'Thêm', 'add()', 'icon-add', 120, 0, 1, 1),
(122, 'Sửa', 'edit()', 'icon-edit', 120, 0, 2, 1),
(123, 'Xóa', 'del()', 'icon-no', 120, 0, 3, 1),
(124, 'Công', 'cong', 'icon-edit', 1, 0, 6, 1),
(125, 'Thêm', 'add()', 'icon-add', 124, 0, 1, 1),
(126, 'Sửa', 'edit()', 'icon-edit', 124, 0, 2, 1),
(127, 'Xóa', 'del()', 'icon-no', 124, 0, 3, 1),
(128, 'Phép - công tác - nghỉ bù', 'nghiphep()', 'icon-add', 119, 0, 1, 0),
(129, 'Duyệt phép', 'duyet()', 'icon-edit', 119, 0, 2, 1),
(130, 'Nghỉ lễ tết', 'nghile()', 'icon-mausac', 119, 0, 3, 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

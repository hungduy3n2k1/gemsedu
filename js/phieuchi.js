$(function () {
    $("#khach_hang").combobox({
        onSelect: function (row) {
            //$("#invoice").combobox("reload", "phieuchi/invoice?khachhang=" + row.id);
        },
    });
});

function timkiem() {
    var tungay = $("#tungay").datebox("getValue");
    var denngay = $("#denngay").datebox("getValue");
    var khachhang = $("#khachhang").combobox("getValue");
    var taikhoan = $("#taikhoan").textbox("getValue");
    if (tungay.length > 0 || denngay.length > 0 || khachhang.length > 0 || taikhoan.length > 0)
        $("#dg").datagrid("options").url = baseUrl + "/phieuchi/json?tungay=" + tungay + "&denngay=" + denngay + "&taikhoan=" + taikhoan + "&khachhang=" + khachhang;
    else
        $("#dg").datagrid("options").url = baseUrl + "/phieuchi/json";
    $("#dg").datagrid("reload");
}

function add() {
    $("#dlg").dialog({
        title: "&nbsp;Thêm mới phiếu chi",
        iconCls: "icon-add",
        closed: false,
    });
    $("#fm").form("clear");
    $("#ngaygio").textbox("setValue", homnay);
    url = baseUrl + "/phieuchi/add";
}

function edit() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        $("#dlg").dialog("open").dialog("setTitle", "Sửa phiếu chi");
        $("#fm").form("clear");
        $("#fm").form("load", row);
        url = baseUrl + "/phieuchi/update?id=" + row.id;
    } else {
        show_messager("Không có bản ghi nào được chọn");
    }
}

function save() {
    $("#fm").form("submit", {
        url: url,
        onSubmit: function () {
            return $(this).form("validate");
        },
        success: function (result) {
            var result = eval("(" + result + ")");
            if (result.success) {
                $("#dlg").dialog("close");
                show_messager(result.msg);
                $("#dg").datagrid("reload");
            } else {
                show_messager(result.msg);
            }
        },
    });
}

function del() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        $.messager.confirm("Thông báo", "Bạn có chắc chắn muốn xóa bản ghi này?", function (r) {
            if (r) {
                $.post(
                    baseUrl + "/phieuchi/del",
                    {
                        id: row.id,
                    },
                    function (result) {
                        if (result.success) {
                            show_messager(result.msg);
                            $("#dg").datagrid("reload");
                        } else {
                            show_messager(result.msg);
                        }
                    },
                    "json"
                );
            }
        });
    } else {
        show_messager("Không có bản ghi nào được chọn");
    }
}

function xuatfile() {
    var tungay = $("#tungay").datebox("getValue");
    var denngay = $("#denngay").datebox("getValue");
    var khachhang = $("#khachhang").combobox("getValue");
    var taikhoan = $("#taikhoan").textbox("getValue");
    window.location.href = baseUrl + "/phieuchi/xuatfile?tungay=" + tungay + "&denngay=" + denngay + "&taikhoan=" + taikhoan + "&khachhang=" + khachhang;
}
function inphieu() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        window.location.href = baseUrl + "/phieuchi/inphieu?id=" + row.id;
        window.location.href = baseUrl + "/phieuchi/inphieu?id=" + row.id;
    } else {
        show_messager("Bạn chưa chọn phiếu để in");
    }
}

$(function () {
    $('#lop_hoc').combobox({
        onSelect: function (row) {
            document.getElementById('lichhoc').value = row.id;
            document.getElementById('giovao').value = row.gio;
            document.getElementById('phanloai').value = row.phanloai;
            document.getElementById('tinhtranghuy').value = row.tinhtranghuy;
        }
    });
});

function diemdanh() {
    var rows = $('#dg').datagrid('getSelections');
    var list = '';
    rows.forEach(
        function myFunction(item, index) {
            list += item.id + ',';
        });
    document.getElementById('data-hocvien').value = list;
    // document.getElementById('fm-diemdanh').submit();
    $('#fm-diemdanh').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $.messager.alert('Notice', result.msg, 'info');
                document.getElementById('huylich').style.display = "inline-block";
            } else {
                $.messager.alert('Notice', result.msg, 'info');
            }
        }
    });
}


// for mobile view
function chonlop() {
    $('#dlg').dialog({closed: false});
    $("#lop_hoc").combobox({url: 'diemdanh/lichhoc'});
}

function tieptuc() {
    var lichhoc = $('#lop_hoc').combobox('getValue');
    $('#dg').datagrid('options').url = 'diemdanh/json?lichhoc=' + lichhoc;
    // show_messager('diemdanh/json?lichhoc=' + lichhoc);
    $.post('diemdanh/checkin?lichhoc=' + lichhoc, function (result) {
    }, 'json');
    var tinhtranghuy = document.getElementById('tinhtranghuy').value;
    if (tinhtranghuy == 1) {
        document.getElementById('huylich').style.display = "inline-block";
    } else {
        document.getElementById('huylich').style.display = "none";
    }
    $('#dg').datagrid({
        onLoadSuccess: function (data) {
            for (i = 0; i < data.rows.length; ++i) {
               // $(this).datagrid('uncheckRow', i);
              //  if (data.rows[i]['checkhv'] > 0)
                 $(this).datagrid('checkRow', i);
            }
            diemdanh();
        }
    });
    $('#dlg').dialog('close');
}

function checkout() {
    var lichhoc = document.getElementById('lichhoc').value;
    var phanloai = document.getElementById('phanloai').value;
    // document.getElementById('fm-diemdanh').submit();
    if (lichhoc.length > 0) {
        if (phanloai > 1) {
            $.post('diemdanh/checkout?lichhoc=' + lichhoc, function (result) {
                if (result.success) {
                    // $('#dg').datagrid('loadData', []);
                    // document.getElementById('lichhoc').value = '';
                    $.messager.confirm("Notice", result.msg, function (r) {
                        if (r)
                            window.location.href = baseUrl;
                    });
                } else {
                    $.messager.alert('Notice', result.msg, 'warning');
                }
            }, 'json');
        } else {
            $('#dlg-link').dialog({closed: false});
            $('#fm-link').form('clear');
            url = baseUrl + '/diemdanh/checkout?lichhoc=' + lichhoc;
        }
    } else {
        $.messager.alert('Info', 'Pls choose a class', 'warning');
    }
}

function checkout1() {
    // document.getElementById('fm-link').action = url;
    // document.getElementById('fm-link').submit();
    $('#fm-link').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $.messager.confirm("Notice", result.msg, function (r) {
                    if (r)
                        window.location.href = baseUrl;
                });
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function huylich() {
    var lichhoc = $("#lichhoc").val();
    $.messager.confirm("Notice", "Are you sure to cancel this schedule?", function (r) {
        if (r) {
            $.post('diemdanh/huylich?lichhoc=' + lichhoc, function (result) {
                result = (JSON).parse(result);
                if (result.success) {
                    $.messager.alert({
                        title: '',
                        msg: result.msg,
                        fn: function () {
                            window.location.href = baseUrl+'/lichhocgv';
                        }
                    });
                } else {
                    show_messager(result.msg);
                }
            });
        }
    });
}

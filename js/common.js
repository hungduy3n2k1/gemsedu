$(function(){	
    reset_format_date('#ngay');	
	$('#sophieu').combobox({
        formatter:function(row){
			var s = '<span style="font-weight:bold">' + row.so_phieu + '</span><br/>' +
					'<span style="color:#888">'+row.ngay+'<br>'+row.khachhang+'('+row.so_tien+')</span>';
			return s;
        }
    });
	$('#loai').combobox({
		onSelect:function(rec){ 
			define_combobox('#sophieu', baseUrl + '/common/getphieu?loai='+rec.value, 
				true, false, true, false, 'id', 'so_phieu');
			$('#ngay').textbox('setValue', '');
			$('#sotien').textbox('setValue', '');
			$('#khachhang').textbox('setValue', '');
			$('#gio').textbox('setValue', '');
			$('#ghichu').textbox('setValue', '');	
		}

 	});			
	$('#sophieu').combobox({
		onSelect:function(rec){ 
			//define_combobox('#sophieu', baseUrl + '/common/getphieu?loai='+rec.value, 
			//	false, false, false, false, 'id', 'so_phieu');
			$('#ngay').textbox('setValue', rec.ngay);
			$('#sotien').textbox('setValue', rec.so_tien);
			$('#khachhang').textbox('setValue', rec.khachhang);
			$('#gio').textbox('setValue', rec.gio);
			$('#ghichu').textbox('setValue', rec.Ghichu);	
		}

 	});			

});
// ghi giờ mới vào database
function suaphieu(){  
    $('#fm').form('submit',{
        url: baseUrl + '/common/saveitem',
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                show_messager(result.msg);	
				//$('#dlg').dialog('close');	
            } else {
                show_messager(result.msg);
				//$('#dlg').dialog('close');					
            }
        }
    });
}	



//xóa sản phẩm trong phiếu đang nhập

function xoaitem(){

		var row = $('#dg_sanpham').datagrid('getSelected');

		var rowIndex = $("#dg_sanpham").datagrid("getRowIndex", row);

		if (row){  

			$.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa sản phẩm '+row.ten_hang,function(r){

				if (r){

					jQuery.get(baseUrl + '/phieuxuat/xoaitem?id='+row.id, function(data) {

						$('#dg_sanpham').datagrid('load', 

								baseUrl + '/phieuxuat/loaditems?sophieu='+document.getElementById('sophieu').value);

					});

				}

			});

		}else{

			show_messager('Không có bản ghi nào được chọn');	

		}

	}



// load item từ datagrid vào form để sửa

function suaitem(){ 

		var row = $('#dg_sanpham').datagrid('getSelected');

		if (row){

			$('#dlg-1').dialog('open').dialog('setTitle','Sửa sản phẩm trên phiếu nhập');

			define_combobox('#don_vi_id', baseUrl + '/common/getdonvi?hh='+row.hang_hoa_id, false, false, false, true, 'Id', 'Tendonvi');

			$('#fm').form('load',row);	

		}else{

			show_messager('Không có bản ghi nào được chọn');	

		}

}



// cập nhật sản phẩm mới sửa vào database và reload datagrid 

function update(){  

		$('#fm').form('submit',{

			url: baseUrl + '/phieuxuat/update',

			onSubmit: function(){

				return $(this).form('validate');

			},

			success: function(result){

					var result = eval('('+result+')');

					if (result.success){

						$('#dlg-1').dialog('close');

						$('#dg_sanpham').datagrid('load', 

							baseUrl + '/phieuxuat/loaditems?sophieu='+document.getElementById('sophieu').value);

					} else {

						show_messager(result.msg);

					}

			}

		});	

}	



// Hủy phiếu nhập (không ghi lại)

	function cancel(){ 
		$.messager.confirm('Thông báo','Bạn có chắc chắn hủy bỏ các số liệu đang nhập',function(r){
			if (r){
				$.post(
					baseUrl+'/phieuxuat/cancel',
					{sophieutam:document.getElementById('sophieu').value},
					function(result){
						var result = eval('('+result+')');
						if (result.success){
							if(document.getElementById('hinhthuc').value==1)
								window.location.href = baseUrl + '/banbuon';	
							else
								window.location.href = baseUrl + '/banle';

						} else {
								show_messager(result.msg);	
						}
				});
			}
		});
	}



// Save thông tin phiếu nhập và cập nhật kho, phiếu thu

function save(){  

		var items = $('#dg_sanpham').datagrid('getRows');

		if(items.length) {  

			$('#phieu').form('submit',{

				url: baseUrl + '/phieuxuat/save',

				onSubmit: function(){

					return $(this).form('validate');

				},

				success: function(result){

					var result = eval('('+result+')');

					if (result.success){

						if(document.getElementById('hinhthuc').value==1)

							window.location.href = baseUrl + '/banbuon';	

						else

							window.location.href = baseUrl + '/banle';

					} else {

						show_messager(result.msg);

					}

				}

			});

		}

		else {	

				show_messager('Chưa có sản phẩm nào được thêm vào phiếu!');

				return;

		}	

	}


function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm mục tiêu',
        iconCls:'icon-add',
        closed:false
    });
    $('#fm').form('clear');
    url = baseUrl + '/muctieu/add';
}

function edit(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $('#dlg').dialog({
            title: '&nbsp;Sửa thông tin',
            closed:false
        });
        $('#fm').form('load',row);
        url = baseUrl+'/muctieu/update?id='+ row.id;
    }
    else
        show_messager('Không có bản ghi nào được chọn');
}

function save(){
    $('#fm').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function kpi(val, row) {
    if (row.mt_doanh_thu != 0){
        // var kpi =1;
        //var kpi = ((row.thucte*100/row.yeucau)*15/100).toFixed(2);
        var kpi = (row.thucte * 100 / row.mt_doanh_thu).toFixed(2);
    } else
        var kpi = '';
    return kpi;
}


function del(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
            if (r){
                $.post(baseUrl+'/muctieu/del',{id:row.id},function(result){
                    if (result.success){
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                        $('#dg').datagrid('clearSelections');
                    } else {
                        show_messager(result.msg);
                    }
                },'json');
            }
        });
    }else{
        show_messager('Không có bản ghi nào được chọn');
    }
}

$(function () {
    $('#sdt').textbox({
        onChange: function (value) {
            $.post(baseUrl + '/trainghiem/checkphone',{phone: value},
            function (result) {
                if (result.success) {
                    $('#khachhang').textbox('setValue',result.data);
                    show_messager(result.msg);
                } else {
                    show_messager(result.msg);
                }
            }, 'json');
        }
    });

    $('#dot_thanh_toan').combobox({
        onSelect: function (rec) {
            for (var k = 1; k < 7; k++) {
                $('#sotiendot' + k).numberbox({value: ''});
                $('#ngaydot' + k).datebox({value: ''});
            }
            var sotien = $('#so_tien').numberbox('getValue');
            var ngaydangky = $('#ngay_dang_ky').datebox('getValue');
            if (rec.id == 1) {
                $('#sotiendot1').numberbox({value: sotien});
                $('#ngaydot1').datebox({value: ngaydangky});
            } else {
                $.post(baseUrl + '/demo/chiadot', {
                    sotien: sotien,
                    ngaydangky: ngaydangky,
                    sodot: rec.id
                }, function (result) {
                    if (result.success) {
                        for (var i = 1; i <= rec.id; i++) {
                            $('#sotiendot' + i).numberbox({value: result.data[i].sotiendot});
                            $('#ngaydot' + i).datebox({value: result.data[i].ngaydot});
                        }
                    } else {
                        show_messager(result.success)
                    }
                }, 'json');
            }
            var dot = rec.id;
            $('#sotiendot1').numberbox({
                onChange: function (val) {
                    if (dot > 1) {
                        var conlai = (sotien.replaceAll(',', '') - val) / (dot - 1);
                        for (var m = 2; m <= dot; m++) {
                            $('#sotiendot' + m).numberbox({value: conlai});
                        }
                    }
                }
            });
        }
    });
});

function timkiem() {
    // var tinhtrang = $('#tinhtrang').combobox('getValue');
    var tukhoa = $('#tukhoa').textbox('getValue');
    if (phanloai.length > 0 || tukhoa.length>0) {
        $('#dg').datagrid('options').url = baseUrl + '/trainghiem/json?tukhoa='+tukhoa;
    } else {
        $('#dg').datagrid('options').url = baseUrl + '/trainghiem/json';
    }
    $('#dg').datagrid('reload');
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Cập nhật ',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('load', row);
        $('#lichhoc').datalist({
            title: 'Lịch học',
            url: 'trainghiem/lichhoc?id='+row.id,
            textField:'noidung',
            lines: true
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function capnhat() {
    $('#fm').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function lichhoc(id) {
    $.post(baseUrl+'/trainghiem/getrow',{id:id},function(result){
        if (result.success){
            var row = result.data;
            if (row.phanloai == 3) {
                $('#dlg-lich').dialog({
                    title: '&nbsp;Cập nhật lịch học',
                    iconCls: 'icon-edit',
                    closed: false
                });
                $('#fm-lich').form('load',row);
                $('#idlich').val(row.id);
            } else {
                show_messager('Lớp offline ko được thay đổi giờ!');
            }
        } else {
            show_messager(result.msg);
        }
    },'json');
}

function save() {
    $('#fm-lich').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-lich').dialog('close');
                $('#lichhoc').datalist('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function add() {
    $('#dlg-them').dialog({
        title: '&nbsp;Thêm lớp trải nghiệm',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-them').form('clear');
}

function addsave() {
    $('#fm-them').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-them').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function del() {
    var rows = $('#dg').datagrid('getSelections');
    if (rows.length > 0) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa các bản ghi đã chọn?', function (r) {
            if (r) {
                var json = JSON.stringify(rows);
                $.post(baseUrl + '/trainghiem/del', {
                    data: json
                }, function (result) {
                    if (result.success) {
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}




function baocao() {
    $('#dlg-baocao').dialog({
        title: '&nbsp;Báo cáo telesale',
        iconCls: 'icon-baocao',
        closed: false
    });
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    $('#dg-baocao').datagrid('load', 'trainghiem/baocao?tungay=' + tungay + '&denngay=' + denngay);
}

function getback() {
    $('#dlg-ungvien').dialog('close');
    $('#dlg').dialog('open');
    $('#tt').tree('reload');
}

function saveuv() {
    var nodes = $('#tt').tree('getChecked');
    var str = '';
    for (var i = 0; i < nodes.length; i++) {
        if (str != '') str += ',';
        str += nodes[i].id;
    }
    document.getElementById("danhmuc").value = str;
    // var fmuv = document.getElementById('fm-ungvien');
    // fmuv.action = url;
    // fmuv.submit();
    $('#fm-ungvien').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-ungvien').dialog('close');
                show_messager(result.msg);
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function dangky() {
    $('#dlg-dangky').dialog({
        title: '&nbsp;Đăng ký khóa học',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-dangky').form('clear');
    var row = $('#dg').datagrid('getSelected');
    $('#ngay_dang_ky').datebox({value: homnay});
    $('#product').combobox({
        onSelect: function (rec) {
            $('#buoi_hoc').textbox({value: rec.so_buoi});
            $('#so_tien').textbox({value: Comma(rec.don_gia)});
        }
    });
    url = baseUrl + '/trainghiem/dangky?id=' + row.id+'&kh='+row.khach_hang;
}

function savedk() {
    // document.getElementById('fm-dangky').action=url;
    // document.getElementById('fm-dangky').submit();
    $('#fm-dangky').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-dangky').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

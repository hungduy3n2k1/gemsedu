window.onload = function () {
    askNotificationPermission();
    function checkDeadlines() {
        $.post("index/events",function (result) {
            var text = 'Bạn có tin nhắn mới';
            // if (result[0].chuaxuly > 0)
            //     text = 'Bạn có 1 tin nhắn mới và ' + result[0].chuaxuly + ' tin nhắn chưa xử lý';
            // else
            //     text = 'Bạn có 1 tin nhắn mới';
            if ((typeof result === 'object') && (manhanvien==result[0].nguoi_nhan))
                createNotification('VPDT-EDU',text,result[0].id);
        },"json");
    }

    function notifyMe() {
        if (Notification.permission !== 'granted') {
            Notification.requestPermission();
        }
        else {
            var notification = new Notification('Notification title', {
                icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
                body: 'Hey there! You\'ve been notified!',
            });
            notification.onclick = function() {
                window.open('http://stackoverflow.com/a/13328397/1269037');
            };
        }
    }

    function checkEndClass()
    {
        let img = "public/images/notification.png";
        $.post("index/ketthuc",function (result) {
            if(result.length>0){
                var text = 'Có '+ result.length +' học viên kết thúc trong vòng 10 ngày tới!';
                let notification = new Notification('VPDT-EDU', { body: text, icon: img, requireInteraction: true});
                notification.onclick = function() {
                    //window.open(baseUrl+"/inbox?id="+id);
                    window.location.href=baseUrl+"/baocaohvkt";
                };
            }
        },"json");
       
            // if (result[0].chuaxuly > 0)
            //     text = 'Bạn có 1 tin nhắn mới và ' + result[0].chuaxuly + ' tin nhắn chưa xử lý';
            // else
            //     text = 'Bạn có 1 tin nhắn mới';
            // if ((typeof result === 'object') && (manhanvien==result[0].nguoi_nhan))
            //     createNotification('VPDT-EDU',text,result[0].id);
        
        // createNotification('VPDT-EDU',text,result[0].id);
    }

    function askNotificationPermission() {
        function handlePermission(permission) {
            if (!("permission" in Notification)) {
                Notification.permission = permission;
            }
        }
        if (!"Notification" in window) {
            console.log("This browser does not support notifications.");
        } else {
            if (checkNotificationPromise()) {
                Notification.requestPermission().then((permission) => {handlePermission(permission);});
            } else {
                Notification.requestPermission(function (permission) {handlePermission(permission);});
            }
        }
    }

    function checkNotificationPromise() {
        try {
            Notification.requestPermission().then();
        } catch (e) {
            return false;
        }
        return true;
    }

    function createNotification(nguoigui,text,id) {
        let img = "public/images/notification.png";
        let notification = new Notification(nguoigui, { body: text, icon: img, requireInteraction: true});
        notification.onclick = function() {
            //window.open(baseUrl+"/inbox?id="+id);
            window.location.href=baseUrl+"/inbox?id="+id;
        };
    }
    
    setInterval(checkDeadlines, 180000);
    // setInterval(checkEndClass, 7200000);
};

// const now = new Date();
// const minuteCheck = now.getMinutes();
// if (minuteCheck == "48") {
//     createNotification("---");
// }
// Notification.onclick = function(event) {
//   //event.preventDefault(); // prevent the browser from focusing the Notification's tab
//   //window.open('http://www.mozilla.org', '_blank');
//   show_messager('ABC');
// }

$(function () {
    $("#tukhoa").textbox({
        onChange: function (rec) {
            timkiem();
        },
    });
});

function timkiem() {
    var tukhoa = $("#tukhoa").textbox("getValue");
    //if(tukhoa.length > 0 ) {
    $("#dg").datagrid("options").url = baseUrl + "/contact/json?tukhoa=" + tukhoa;
    $("#dg").datagrid("reload");
    //}
}

function add() {
    $("#dlg").dialog({
        title: "&nbsp;Thêm mới danh sách liên hệ",
        iconCls: "icon-add",
        closed: false,
    });
    $("#fm").form("clear");
    url = baseUrl + "/contact/add";
}

function del() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        $.messager.confirm("Thông báo", "Bạn có chắc chắn muốn xóa bản ghi này?", function (r) {
            if (r) {
                $.post(
                    baseUrl + "/contact/del",
                    {
                        id: row.id,
                    },
                    function (result) {
                        if (result.success) {
                            show_messager(result.msg);
                            $("#dg").datagrid("reload");
                        } else {
                            show_messager(result.msg);
                        }
                    },
                    "json"
                );
            }
        });
    } else {
        show_messager("Không có bản ghi nào được chọn");
    }
}

function edit() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        $("#dlg").dialog({
            title: "&nbsp;Sửa thông tin",
            iconCls: "icon-edit",
            closed: false,
        });
        $("#fm").form("load", row);
        url = baseUrl + "/contact/update?id=" + row.id;
    } else show_messager("Không có bản ghi nào được chọn");
}

function save() {
    $("#fm").form("submit", {
        url: url,
        onSubmit: function () {
            return $(this).form("validate");
        },
        success: function (result) {
            var result = eval("(" + result + ")");
            if (result.success) {
                show_messager(result.msg);
                $("#dlg").dialog("close");
                $("#dg").datagrid("reload");
                $("#dg").datagrid("clearSelections");
            } else {
                show_messager(result.msg);
            }
        },
    });
}

function xuatfile() {
    window.location.href = "contact/xuatfile";
}

function nhap() {
    $('#dlg-nhap').dialog({
        title: '&nbsp;Nhập data từ file exccel',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-nhap').form('clear');
}

function nhapexel() {
    $('#fm-nhap').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-nhap').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}


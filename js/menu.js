$(function(){
    $('#dg').treegrid({
        url: baseUrl + '/menu/json',
        onDblClickRow:function(){
            edit();
		}
    });
});

function timkiem() {
    var loai = $('#chonloai').combobox('getValue');
    if (loai.length > 0)
        $('#dg').treegrid('options').url = 'menu/json?loai=' + loai;
    else
        $('#dg').treegrid('options').url = 'menu/json';
    if ($('#dlg-search').dialog)
        $('#dlg-search').dialog('close');
    $('#dg').treegrid('reload');
    // var p = $('#dg').datagrid('getPanel');  // get the panel object
    // p.panel('setTitle',dgtitle);
}

function mobile(val,row) {
    if (val==1)
        return 'Yes';
    else
        return ''
}

function active(val,row) {
    if (val==1)
        return 'Bật';
    else
        return ''
}


function add(){
    $('#dlg').dialog('open').dialog('setTitle','Thêm mới menu');
    $('#fm').form('clear');
    $('#loai').combobox({value:1});
    url = baseUrl + '/menu/add';
}

function edit(){
    var row = $('#dg').treegrid('getSelected');
    if (row){
        $('#dlg').dialog('open').dialog('setTitle','Cập nhật menu');
        $('#fm').form('clear');
        $('#fm').form('load',row);
        // if(row.functions != ''){
        //     $('#functions').combobox('setValues', row.functions);
        // }
        // if(row.active != ''){
        //     $('#active').combobox('setValues', row.active);
        // }
        url = baseUrl+'/menu/update?id='+ row.id;
    }else{
        show_messager('Không có bản ghi nào được chọn');
    }
}

// xoa du lieu
function del(){
    var row = $('#dg').treegrid('getSelected');
    if (row){
        $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
            if (r){
                $.post(baseUrl+'/menu/del',{id:row.id},function(result){
        				    if (result.success){
        				        show_messager(result.msg);
        				        $('#dg').treegrid('reload');
                        // $('#dg').treegrid('clearSelections');
        				    } else
        						    show_messager(result.msg);
				        },'json');
				        // $('#dg').treegrid('reload');
            }
        });
    } else
		  show_messager('Không có bản ghi nào được chọn');
}

function save(){
    $('#fm').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');
                show_messager(result.msg);
				        $('#dg').treegrid('reload');
                // $('#dg').treegrid('clearSelections');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function timkiem() {
    var ngaybd = $('#ngaybd').datebox('getValue');
    var ngaykt = $('#ngaykt').datebox('getValue');
    if (ngaybd.length>0 || ngaykt.length>0) {
        $('#dg').datagrid('options').url = 'doanhthukh/json?ngaybd=' + ngaybd + '&ngaykt=' + ngaykt;
    }
    else
        $('#dg').datagrid('options').url = 'doanhthukh/json';
    $('#dg').datagrid('reload');
}

function search() { //mobile
    var team = $('#team').combobox('getValue');
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    if (team.length>0 || thang.length>0 || nam.length>0) {
        $('#dg').datagrid('options').url = 'doanhthukh/jsonmobile?team='+team+'&thang='+thang+'&nam='+nam;
        var title = 'Báo cáo doanh thu tháng '+thang+' năm '+nam;
        var dgPanel = $('#dg').datagrid('getPanel');
        dgPanel.panel('setTitle', title);
    } else
        $('#dg').datagrid('options').url = 'doanhthukh/jsonmobile';
    $('#dlg-search').dialog('close');
    $('#dg').datagrid('reload');
}

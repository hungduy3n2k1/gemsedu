$(function() {
    // $('#dg').datagrid({
    //     view: detailview,
    //     detailFormatter: function(index, row) {
    //         return '<div id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
    //     },
    //     onExpandRow: function(index, row) {
    //         $('#ddv-' + index).panel({
    //             border: false,
    //             cache: false,
    //             href: baseUrl + '/phaithu/detail?id=' + row.so_phieu,
    //             onLoad: function() {
    //                 $('#dg').datagrid('fixDetailRowHeight', index);
    //             }
    //         });
    //     }
    // });
});
// function phanloai(val,row) {
//     if (val==1)
//     		return 'Hợp đồng';
// 		else if (val==2)
// 				return 'Báo giá';
// 		else if (val==3)
// 				return 'Đề nghị tạm ứng';
// 		else if (val==4)
// 				return 'Đề nghị thanh toán';
// 		else
// 				return 'Yêu cầu tuyển dụng';
// }
//
// function download(val,row) {
//     if (val=='')
//     		return '';
// 		else
// 				return '<a style="text-decoration:underline; color:blue" href="'+val+'" target="_blank">Tải về</a>';
// }
//
// function xemnoidung(val,row) {
//     if (val=='')
//     		return '';
// 		else
// 				return '<a style="text-decoration:underline; color:blue" href="phaithu/view?id='+row.id+'" target="_blank">Xem nội dung</a>';
// }

// function timkiem() {
//     var tungay = $('#tungay').datebox('getValue');
//     var denngay = $('#denngay').datebox('getValue');
//     var khachhang = $('#khachhang').combobox('getValue');
// 	  if(tungay.length > 0 || denngay.length > 0 || khachhang.length > 0 )
//         $('#dg').datagrid('options').url = baseUrl + '/phaithu/json?tungay='+tungay+'&denngay='+denngay+'&khachhang='+khachhang;
//     else
//         $('#dg').datagrid('options').url = baseUrl + '/phaithu/json';
//     $('#dg').datagrid('reload');
// }
//
//
// function add() {
//     $('#dlg').dialog({
//         title: '&nbsp;Thêm ',
//         iconCls: 'icon-add',
//         closed: false
//     });
//     $('#fm').form('clear');
//     url = baseUrl + '/phaithu/add';
// }

function chitiet(id) {
    if (id>0) {
        $('#dlg').dialog({
            title: '&nbsp;Chi tiết công nợ: ',
            iconCls: 'icon-edit',
            closed: false
        });
        // $('#dg-chitiet').datagrid({url: 'phaithu/chitiet?id='+id});
        $('#dg-chitiet').datagrid('load', 'phaithu/chitiet?id='+id);
        // $('#dg-chitiet').datagrid('options').url = 'phaithu/chitiet?id='+id;
        // $('#dg-chitiet').datagrid('reload');
    } else {
        show_messager('Không có dữ liệu');
    }
}

function save() {
    $('#fm').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function noidung() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg-noidung').dialog({
            title: row.name,
            iconCls: 'icon-edit',
            closed: false
        });
        //$('#fm').form('clear');
        //document.getElementById('noi_dung').value=row.noi_dung;
        tinyMCE.activeEditor.setContent(row.noi_dung);
        url = baseUrl + '/phaithu/noidung?id=' + row.id;
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function savenoidung(){
    $('#fm-noidung').form('submit',{
        url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg-noidung').dialog('close');
								$('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}



// function duyet(val,row) {
//     if (val==1)
//     		return 'Đã duyệt';
// 		else
// 				return 'Chưa duyệt';
// }
//
//
// function duyetmau() {
//     var row = $('#dg').datagrid('getSelected');
//     if (row) {
// 				// if (row.tinh_trang==1) {
// 				// 		show_messager('Biểu mẫu này đã được duyệt, vui lòng không duyệt lại');
// 				// } else {
// 						$.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn duyệt biểu mẫu này?', function(r) {
// 		            if (r) {
// 		                $.post(baseUrl + '/phaithu/duyet', {id: row.id}, function(result) {
// 		                    if (result.success) {
// 		                        $('#dg').datagrid('reload');
// 		                    } else {
// 		                        show_messager(result.msg);
// 		                    }
// 		                }, 'json');
// 		            }
// 		        });
// 				//}
//     } else {
//         show_messager('Không có bản ghi nào được chọn');
//     }
// }

// function del() {
//     var row = $('#dg').datagrid('getSelected');
//     if (row) {
//         $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa bản ghi này?', function(r) {
//             if (r) {
//                 $.post(baseUrl + '/phaithu/del', {
//                     id: row.id
//                 }, function(result) {
//                     if (result.success) {
//                         $('#dg').datagrid('reload');
//                     } else {
//                         show_messager(result.msg);
//                     }
//                 }, 'json');
//             }
//         });
//     } else {
//         show_messager('Không có bản ghi nào được chọn');
//     }
// }
//
//
//
//
//
//
// function xoa() { //xoa san phẩm
//     var row = $('#dg-don').datagrid('getSelected');
//     if (row) {
//         $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa?', function(r) {
//             if (r) {
//                 $.post(baseUrl + '/xuatkho/xoa', {id:row.id, phieuid:row.phieu_id}, function(result) {
//                     if (result.success) {
//                         show_messager(result.msg);
//                         $('#dg-don').datagrid('reload');
//                     } else {
//                         show_messager(result.msg);
//                     }
//                 }, 'json');
//             }
//         });
//     } else {
//         show_messager('Không có bản ghi nào được chọn');
//     }
// }
//
// // ---------------- in đơn -----------------
// function indonhang(){
// 		var row = $('#dg').datagrid('getSelected');
// 		if (row) {
//     		window.location.href = baseUrl + '/xuatkho/indon?id='+row.id;
// 		} else {
// 				show_messager('Không có bản ghi nào được chọn');
// 		}
// }
//
// // ------------ Kiểm hàng và xuất kho -----------------------------
// function xuatkho() {
// 		var row = $('#dg').datagrid('getSelected');
// 		if (row) {
//       	window.location.href = baseUrl + '/xuatkho/xuatkho?id='+row.id;
// 		} else {
// 					show_messager('Không có bản ghi nào được chọn');
// 		}
// }
//
// function xuatdon(){  //Xuất hàng và ghi nhận biến động vào sổ kho
// 	  var data = $('#dg-don').datagrid('getRows');
// 		var thongbao = '';
// 		var i;
// 		for (i = 0; i < data.length; i++) { // tìm xem có mã hàng nào xuất thiếu hay không?
// 				if (data[i]['soluong']<data[i]['so_luong'] ) {
// 						thongbao =  data[i]['tenhang']+' Thiếu lượng!';
// 						break;
// 				}
// 		}
// 		if (thongbao == '') {
// 				document.getElementById('data').value=JSON.stringify(data);
// 				$('#fm-xuat').form('submit',{
// 						onSubmit: function(){
// 								return $(this).form('validate');
// 						},
// 						success: function(result){
// 								var result = eval('('+result+')');
// 								if (result.success){
// 										window.location.href = baseUrl + '/phieuxuat';
// 								} else {
// 										show_messager(result.msg);
// 								}
// 						}
// 				});
// 		} else
// 				show_messager(thongbao);
// }

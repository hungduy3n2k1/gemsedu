$(function() {
    $('#tukhoa').textbox({
        onChange: function(rec) {
            timkiem();
        }
    });
});

function gioitinh(val,row) {
    if(val==1)
        return 'Nam';
    else
        return 'Nữ';
}

function taixuong(val,row) {
    if(val!='')
        return '<a href="'+val+'" target="_blank" style="text-decoration:underline; color:blue;">Tải CV về</a>';
    else
        return '';
}

function timkiem() {
    var tukhoa = $('#tukhoa').textbox('getValue');
    if (tukhoa.length>0)
        $('#dg').datagrid('options').url = baseUrl + '/ungvien/json?tukhoa=' + tukhoa;
    else
        $('#dg').datagrid('options').url = baseUrl + '/ungvien/json';
    $('#dg').datagrid('reload');
}

function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm mới ứng viên',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm').form('clear');
    document.getElementById("hinhanh").src='https://cdn5.vectorstock.com/i/1000x1000/82/59/anonymous-user-flat-icon-vector-18958259.jpg';
    url = baseUrl + '/ungvien/add';
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Xem và cập nhật thông tin nhân sự',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('clear');
        $('#fm').form('load', row);
        if (row.hinh_anh=='')
            document.getElementById("hinhanh").src='https://cdn5.vectorstock.com/i/1000x1000/82/59/anonymous-user-flat-icon-vector-18958259.jpg';
        else
            document.getElementById("hinhanh").src=row.hinh_anh;
        url = baseUrl + '/ungvien/update?id=' + row.id;
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function save() {
    $('#fm').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function del() {
  var row = $('#dg').treegrid('getSelected');
  if (row){
      $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
          if (r){
              $.post(baseUrl+'/ungvien/del',{id:row.id},function(result){
                  if (result.success){
                      $('#dg').datagrid('reload');
                  } else {
                      show_messager(result.msg);
                  }
              },'json');
          }
      });
  }else{
      show_messager('Không có bản ghi nào được chọn');
  }
}

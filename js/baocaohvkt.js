

function gioitinh(val, row) {
    if (val == 1)
        return 'Nam';
    else
        return 'Nữ';
}

function format_phanloai(val, row) {
    if (val == 1)
        return 'Online';
    else if (val == 2)
        return 'Offline';
    else if (val == 3)
        return 'Test Online';
    else if (val == 4)
        return 'Test Offline';
    else
        return '';
}

function format_tinhtrang(val, row) {
    if (val == 1)
        return 'Đang chờ';
    else if (val == 2)
        return 'Đang học';
    else if (val == 3)
        return 'Đã tốt nghiệp';
    else
        return '';
}

function phanloaisale(val, row) {
    let strloai = '';
    if(val != ''){
        const loai = val.split(",");
        for(var i = 0; i < loai.length; i++){
            if (loai[i] == 1)
                strloai += ' New sale,';
            else if (loai[i] == 2)
                strloai += ' Tái tục trải nghiệm,';
            else if (loai[i] == 3)
                strloai += ' Tái tục new,';
        }
        
    }
    strloai = strloai.slice(0, strloai.length - 1);
    return strloai;
}

function timkiem() {
    var khachhang = $('#khachhang').textbox('getValue');
    var nhanviensale = $('#nhanviensale').combobox('getValue');
    var hocvien = $('#hocvien').textbox('getValue');
    var giaovien = $('#giaovien').combobox('getValue');
    var chuyenmon = $('#chuyenmon').combobox('getValue');
    var thang = $('#thang').datebox('getValue');
    var nam = $('#nam').datebox('getValue');
    var phanloaidh = $('#phanloaidh').combobox('getValue');
    if (khachhang.length > 0 || hocvien.length > 0 || thang.length > 0 || nam.length > 0 || phanloaidh.length>0 || nhanviensale.length>0 || giaovien.length>0 || chuyenmon.length>0)
        $('#dg').datagrid('options').url = baseUrl + '/baocaohvkt/json?hocvien=' + hocvien + '&khachhang=' + khachhang +
            '&thang=' + thang + '&nam=' + nam + '&phanloaidh=' + phanloaidh + '&giaovien=' + giaovien + '&chuyenmon=' + chuyenmon + '&nhanviensale=' + nhanviensale;
    else
        $('#dg').datagrid('options').url = baseUrl + '/baocaohvkt/json';
    $('#dg').datagrid('reload');
}

function xuatfile() {
    var khachhang = $('#khachhang').textbox('getValue');
    var nhanviensale = $('#nhanviensale').combobox('getValue');
    var hocvien = $('#hocvien').textbox('getValue');
    var giaovien = $('#giaovien').combobox('getValue');
    var chuyenmon = $('#chuyenmon').combobox('getValue');
    var thang = $('#thang').datebox('getValue');
    var nam = $('#nam').datebox('getValue');
    var phanloaidh = $('#phanloaidh').combobox('getValue');
    window.location.href = baseUrl + '/baocaohvkt/xuatfile?hocvien=' + hocvien + '&hocvien=' + hocvien + '&khachhang=' + khachhang +
    '&thang=' + thang + '&nam=' + nam + '&phanloaidh=' + phanloaidh + '&giaovien=' + giaovien + '&chuyenmon=' + chuyenmon + '&nhanviensale=' + nhanviensale;
}
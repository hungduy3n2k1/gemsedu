function tinhtrang(val, row) {
    if (val == 1)
        return 'Chờ';
    else if (val == 2)
        return 'Đã setup';
    else if (val == 3)
        return 'Đang học';
    else if (val == 4)
        return 'Hoàn thành';
    else if (val == 5)
        return 'Đã chốt';
    else if (val == 6)
        return 'Cọc ngay';
    else if (val == 7)
        return 'Hủy';
    else
        return '';
}

function format_hocvien(val, row) {
    if (row.loailop == 1)
        return val;
    else
        return '';
}

function timkiem() {
    var gio = $('#giohoc').timespinner('getValue');
    var ngay = $('#ngay1').textbox('getValue');
    var thang = $('#thang').textbox('getValue');
    var nam = $('#nam').textbox('getValue');
    var loai = $('#loai').textbox('getValue');
    var giaovien = $('#giaovien').textbox('getValue');
    var phonghoc = $('#phonghoc').textbox('getValue');
    var tenlop = $('#tenlop').textbox('getValue');
    if (thang.length > 0 || nam.length > 0 || gio.length > 0 || giaovien.length > 0 ||
        loai.length > 0 || phonghoc.length > 0 || tenlop.length > 0 || ngay.length > 0) {
        var curl = window.location.href;
        var res = curl.split("/");
        if (res[res.length - 1] == 'lichhoc')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/json?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonview?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonday?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsontime?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
    } else {
        var curl = window.location.href;
        var res = curl.split("/");
        if (res[res.length - 1] == 'lichhoc')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/json";
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonview";
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonday";
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsontime";
    }
    $("#dg").datagrid("reload");
}

function edit() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        if (row.loailop == 1) {
            if (row.tinh_trang == 1) {
                $('#dlg-lichhoc').dialog({
                    title: '&nbsp;Edit Schedule',
                    iconCls: 'icon-edit',
                    closed: false
                });
                $('#fm-lichhoc').form('load', row);
                url = baseUrl + '/lichhocgv/update?id=' + row.id;
            } else {
                $.messager.alert('Notice', "Lịch học đang diễn ra hoặc đã kết thúc", 'warning');
            }
        }
    } else {
        $.messager.alert('Notice', 'Choose a schedule', 'warning');
    }
}

function daythay() {
    alert();
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        if (row.loailop == 1) {
            if (row.tinh_trang == 1) {
                $('#dlg-daythay').dialog({
                    title: '&nbsp;Dạy thay',
                    iconCls: 'icon-edit',
                    closed: false
                });
                $('#fm-daythay').form('load', row);
                // url = baseUrl + '/lichhocgv/update?id=' + row.id;
            } else {
                $.messager.alert('Notice', "Lịch học đang diễn ra hoặc đã kết thúc", 'warning');
            }
        }
    } else {
        $.messager.alert('Notice', 'Choose a schedule', 'warning');
    }
}

function edittime() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        if (row.loailop == 1) {
            if (row.tinh_trang == 1) {
                $('#dlg-edittime').dialog({
                    title: '&nbsp;Edit Time',
                    iconCls: 'icon-edit',
                    closed: false
                });
                $('#fm-edittime').form('load', row);
                url = baseUrl + '/lichhocgv/suathoiluong?id=' + row.id;
            } else {
                $.messager.alert('Notice', "Lịch học đang diễn ra hoặc đã kết thúc", 'warning');
            }
        }
    } else {
        $.messager.alert('Notice', 'Choose a schedule', 'warning');
    }
}

function save(i) {
    // document.getElementById('fm-lichhoc').action=url;
    // document.getElementById('fm-lichhoc').submit();
    if (i == 1) {
        $('#fm-lichhoc').form('submit', {
            url: url,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    $('#dlg-lichhoc').dialog('close');
                    $('#dg').datagrid('reload');
                    $('#dg').datagrid('clearSelections');
                } else {
                    $.messager.alert('Notice', result.msg, 'warning');
                }
            }
        });
    } else if (i == 2) {
        $('#fm-edittime').form('submit', {
            url: url,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    $('#dlg-edittime').dialog('close');
                    $('#dg').datagrid('reload');
                    $('#dg').datagrid('clearSelections');
                } else {
                    $.messager.alert('Notice', result.msg, 'warning');
                }
            }
        });
    }
}

function add() {
    $('#dlg-add').dialog({
        title: '&nbsp;Add schedules',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-add').form('clear');
    $('#lop_hoc2').combobox({
        onSelect: function (rec) {
            $('#giao_vien').combobox({value: rec.giao_vien});
            document.getElementById('sobuoi').value = rec.so_buoi;
            document.getElementById('buoikm').value = rec.buoi_khuyen_mai;
            document.getElementById('hocvien').value = rec.hoc_vien;
            document.getElementById('phanloai').value = rec.phan_loai;
        }
    });
    url = baseUrl + '/lichhocgv/add';
}


function saveadd() {
    // document.getElementById('fm-add').action=url;
    // document.getElementById('fm-add').submit();
    $('#fm-add').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-add').dialog('close');
                $('#dg').datagrid('reload');
                $('#dg').datagrid('clearSelections');
            } else {
                show_messager(result.msg);
            }
        }
    });
}


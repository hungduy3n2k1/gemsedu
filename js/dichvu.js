function timkiem() {
    var loai = $('#loai').combobox('getValue');
    var tukhoa = $('#tukhoa').textbox('getValue');
    if (loai.length>0 || tukhoa.length>0)
       	$('#dg').datagrid('options').url = baseUrl + '/dichvu/json?loai='+loai+'&tukhoa='+tukhoa;
    else
        $('#dg').datagrid('options').url = baseUrl + '/dichvu/json';
    if($('#dlg-search').dialog)
        $('#dlg-search').dialog('close');
    $('#dg').datagrid('reload');
}

function add() {
	$('#dlg').dialog({
        title: '&nbsp;Thêm sản phẩm',
		iconCls:'icon-add',
		closed:false
    });
    $('#fm').form('clear');
    $('#gia_han').combobox('setValue',0);
    $('#don_vi_tinh').combobox('setValue',3);
    url = baseUrl + '/dichvu/add';
}

function format_giahan(val,row){
    if (val == 1){
        return 'Có';
    } else {
        return 'Không';
    }
}


function edit(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
		$('#dlg').dialog({
			title: '&nbsp;Sửa thông tin',
			iconCls:'icon-edit',
			closed:false
		});
        $('#fm').form('load',row);
        url = baseUrl+'/dichvu/update?id='+ row.id;
    }
	else
        show_messager('Không có bản ghi nào được chọn');
}

function save(){
    $('#fm').form('submit',{
       	url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');
				$('#dg').datagrid('reload');
                $('#dg').datagrid('clearSelections');
            } else {
                show_messager(result.msg);
            }
        }
    });
}


function del(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
            if (r){
                $.post(baseUrl+'/dichvu/del',{id:row.id},function(result){
				    if (result.success){
				        show_messager(result.msg);
				        $('#dg').datagrid('reload');
                        $('#dg').datagrid('clearSelections');
				    } else {
						show_messager(result.msg);
				    }
				},'json');
            }
        });
    }else{
		show_messager('Không có bản ghi nào được chọn');
    }
}

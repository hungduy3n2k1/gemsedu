

function gioitinh(val, row) {
    if (val == 1)
        return 'Nam';
    else
        return 'Nữ';
}

function format_phanloai(val, row) {
    if (val == 1)
        return 'Online';
    else if (val == 2)
        return 'Offline';
    else if (val == 3)
        return 'Test Online';
    else if (val == 4)
        return 'Test Offline';
    else
        return '';
}

function format_tinhtrang(val, row) {
    if (val == 1)
        return 'Đang chờ';
    else if (val == 2)
        return 'Đang học';
    else if (val == 3)
        return 'Đã tốt nghiệp';
    else
        return '';
}

function timkiem() {
    var khachhang = $('#khachhang').textbox('getValue');
    var hocvien = $('#hocvien').textbox('getValue');
    var giaovien = $('#giaovien').combobox('getValue');
    var chuyenmon = $('#chuyenmon').combobox('getValue');
    // var tungay = $('#tungay').datebox('getValue');
    // var denngay = $('#denngay').datebox('getValue');
    if (khachhang.length > 0 || hocvien.length > 0 || giaovien.length > 0 || chuyenmon.length > 0)
        $('#dg').datagrid('options').url = baseUrl + '/baocaohv/json?hocvien=' + hocvien + '&khachhang=' + khachhang + '&giaovien=' + giaovien + '&chuyenmon=' + chuyenmon;
    else
        $('#dg').datagrid('options').url = baseUrl + '/baocaohv/json';
    $('#dg').datagrid('reload');
}
$(function() {
    $('#congviec').datalist({
        onSelect:function(index,row){
            $.post(baseUrl+'/congviec/chitiet',{id:row.id},function(data){
                $('#fm').form('load',data);
                document.getElementById('hancuoicu').value=data.hancuoi;
                $("tt").val(row.tinh_trang);
            },'json');
            $('#comments').datalist({url: 'congviec/comment?id='+row.id, method: 'get' });
        }
    });

    $('#nhanvien').combobox({
        onSelect:function(rec){
            $('#congviec').datalist({url: 'congviec/json?nhanvien='+rec.id});
        }
    });

    $('#deadline').datebox().datebox('calendar').calendar({
        validator: function(date){
            var now = new Date();
            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            var d2 = new Date(now.getFullYear(), now.getMonth(), now.getDate()+45);
            return d1<=date && date<=d2;
        }
    });
});

function giaoviec() {
    $('#dlg').dialog({closed: false});
    $('#fm-giaoviec').form('clear');
    var nguoinhan = $('#nhanvien').combobox('getValue');
    $('#giaocho').combobox('setValue',nguoinhan);
}

function add() {
    $('#fm-giaoviec').form('submit',{
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#congviec').datalist('reload');
                $('#dlg').dialog('close');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function save() { //Cập nhật công việc
    var id = document.getElementById('id').value;
    if (id>0) {
        $('#fm').form('submit',{
            onSubmit: function(){
                return $(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
                    show_messager(result.msg);
                    $('#congviec').datalist('reload');
                } else {
                    show_messager(result.msg);
                }
            }
        });
    } else
        show_messager('Bạn chưa chọn công việc');
}

function comment(){
    var id = document.getElementById('id').value;
    if (id>0) {
		    $('#dlg-comment').dialog({closed:false});
        $('#comment').textbox('setValue','');
        $('#hinhanh').textbox('setValue','');
    } else {
        show_messager('Bạn chưa chọn công việc');
    }
}

function commentsave() {
    var id = document.getElementById('id').value;
    var noidung = $('#comment').textbox('getValue');
    var hinhanh = $('#hinhanh').textbox('getValue');
    $.post(baseUrl+'/congviec/addcomment',{id:id,noidung:noidung,hinhanh:hinhanh},function(result){
        if (result.success){
            show_messager(result.msg);
            $('#comments').datalist({url: 'congviec/comment?id='+id, method: 'get' });
            $('#dlg-comment').dialog('close');
        } else {
            show_messager(result.msg);
        }
    },'json');
}

// function duyet() {
//     var id = document.getElementById('id').value;
//     // var tinhtrang = $('#tinh_trang').combobox('getValue');
//     if (id>0) {
//         $('#dlg-danhgia').dialog({closed: false});
//     } else {
//         show_messager('Bạn chưa chọn công việc');
//     }
// }
//
// function danhgiasave() {
//     var id = document.getElementById('id').value;
//     var danhgia = $('#ketqua').combobox('getValue');
//     $.post(baseUrl + '/congviec/danhgia',{ id: id,danhgia: danhgia
//             }, function(result) {
//             if (result.success) {
//                   var row = $('#nhanvien').datalist('getSelected');
//                   if (row)
//                       $('#congviec').datalist({url: 'congviec/json?id='+row.id, method: 'get', groupField: 'group' });
//                   else
//                       $('#congviec').datalist({url: 'congviec/json?id=0', method: 'get', groupField: 'group' });
//                   show_messager(result.msg);
//                   $('#dlg-danhgia').dialog({closed: true});
//             } else {
//                   show_messager(result.msg);
//             }
//     }, 'json');
// }

// function giahan() {
//     var id = document.getElementById('id').value;
//     var ngay = document.getElementById('hancuoi').value;
//     if (id>0) {
//         $('#dlg-giahan').dialog({closed: false});
//         $('#ngaygiahan').datebox('setValue',ngay);
//     } else {
//         show_messager('Bạn chưa chọn công việc');
//     }
// }
//
// function giahansave() {
//     var id = document.getElementById('id').value;
//     var ngay = $('#ngaygiahan').datebox('getValue');
//     var tinhtrang = $('#tinh_trang').combobox('getValue');
//     $.post(baseUrl + '/congviec/giahan',
//             { id: id,
//               ngay: ngay,
//               tinhtrang: tinhtrang,
//             }, function(result) {
//             if (result.success) {
//                   var row = $('#nhanvien').datalist('getSelected');
//                   if (row)
//                       $('#congviec').datalist({url: 'congviec/json?id='+row.id, method: 'get', groupField: 'group' });
//                   else
//                       $('#congviec').datalist({url: 'congviec/json?id=0', method: 'get', groupField: 'group' });
//                   show_messager(result.msg);
//                   $('#dlg-giahan').dialog({closed: true});
//             } else {
//                   show_messager(result.msg);
//             }
//     }, 'json');
// }





// function del() {
//     var id = document.getElementById('id').value;
//     if (id>0) {
//         $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn hủy công việc này', function(r) {
//             if (r) {
//                 $.post(baseUrl + '/congviec/del', {id: id}, function(result) {
//                       if (result.success) {
//                             show_messager(result.msg);
//                             $('#fm').form('clear');
//                             var data = $('#congviec').datalist('getRows');
//                             var temp = data.filter(function (el) {
//                                 return el.id != id;
//                             });
//                             $('#congviec').datalist('loadData',temp);
//                             //delete data[0];
//                             // var x=  JSON.stringify(temp);
//                       } else {
//                             show_messager(result.msg);
//                       }
//                 }, 'json');
//             }
//         });
//     } else {
//         show_messager('Bạn chưa chọn công việc');
//     }
// }



//--------------- MOBILE

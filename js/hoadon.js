$(function () {
    $('#dg').datagrid({
        // view: detailview,
        // detailFormatter: function(index, row) {
        //     return '<div id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
        // },
        // onExpandRow: function(index, row) {
        //     $('#ddv-' + index).panel({
        //         border: false,
        //         cache: false,
        //         href: baseUrl + '/hoadon/detail?id=' + row.id,
        //         onLoad: function() {
        //             $('#dg').datagrid('fixDetailRowHeight', index);
        //         }
        //     });
        // }
    });
    $('#phanloai').combobox({
        onSelect: function (row) {
            if (row.id == 0) {
                $('#doitac').combobox('reload', 'hoadon/doitac');
                $('#doitac').combobox({prompt: "Nhà cung cấp"});
            } else {
                $('#doitac').combobox('reload', 'hoadon/khachhang');
                $('#doitac').combobox({prompt: "Khách hàng"});
            }
            $('#doitac').combobox({value: ''});
        }
    });
    $('#phan_loai').combobox({
        onSelect: function (row) {
            if (row.id == 0) {
                document.getElementById('txtDoitac').innerText = 'Nhà cung cấp';
                $('#doi_tac').combobox('reload', 'hoadon/doitac');
            } else {
                document.getElementById('txtDoitac').innerText = 'Khách hàng';
                $('#doi_tac').combobox('reload', 'hoadon/khachhang');
            }
            $('#doi_tac').combobox({value: ''});
        }
    });
    $('#doi_tac').combobox({
        onSelect: function (row) {
            $('#dia_chi').textbox({value: row.dia_chi});
            $('#ma_so_thue').textbox({value: row.ma_so});
        }
    });
});

function taixuong(val, row) {
    if (val != '')
        return '<a href="' + val + '" target="_blank" style="text-decoration:underline; color:blue;">Xem hóa đơn</a>';
    else
        return '';
}

function timkiem() {
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    var phanloai = $('#phanloai').combobox('getValue');
    var doitac = $('#doitac').combobox('getValue');
    var tukhoa = $('#tukhoa').textbox('getValue');
    if (tungay.length > 0 || denngay.length > 0 || phanloai.length > 0 || doitac.length > 0 || tukhoa.length > 0) {
        $('#dg').datagrid('options').url = baseUrl + '/hoadon/json?tungay=' + tungay + '&denngay=' + denngay +
            '&phanloai=' + phanloai + '&doitac=' + doitac + '&tukhoa=' + tukhoa;
    } else {
        $('#dg').datagrid('options').url = baseUrl + '/hoadon/json'
    }
    $('#dg').datagrid('reload');
}

function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm hóa đơn',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm').form('clear');
    $('#phan_loai').combobox('setValue', 0);
    $('#ngaygio').textbox('setValue', get_today());
    url = baseUrl + '/hoadon/add';
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Sửa thông tin',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('load', row);
        $('#name').textbox({
            readonly: true
        });
        url = baseUrl + '/hoadon/update?id=' + row.id;
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function save() {
    $('#fm').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function del() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa?', function (r) {
            if (r) {
                $.post(baseUrl + '/hoadon/del', {
                    id: row.id
                }, function (result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

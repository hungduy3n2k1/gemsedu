function celltop(value, row) {
    if (value)
        return '<div style="min-height:70px;line-height:12px">' + value + '</div>';
}

function format_hocvien(val, row) {
    if (row.loailop == 1)
        return val;
    else
        return '';
}

function timkiem() {
    var gio = $('#giohoc').timespinner('getValue');
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    var loai = $('#loai').textbox('getValue');
    var giaovien = document.getElementById('giaovien').value
    var phonghoc = $('#phonghoc').textbox('getValue');
    var tenlop = $('#tenlop').textbox('getValue');
    var curl = window.location.href;
    var res = curl.split("/");
    $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/json?tungay=" + tungay + "&denngay=" + denngay + '&gio=' + gio +
        "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop;
    $("#dg").datagrid("reload");
}

function pre() {
    var thang = $('#thang').textbox('getValue');
    var nam = $('#nam').textbox('getValue');
    var x = parseInt(thang) - 1;
    if (x > 0) {
        if (x < 10)
            thang = '0' + x;
        else
            thang = x;
        $('#thang').textbox('setValue', thang);
    } else {
        thang = '12';
        var y = parseInt(nam) - 1;
        $('#thang').textbox('setValue', thang);
        $('#nam').textbox('setValue', y);
    }
    var gio = $('#giohoc').timespinner('getValue');
    var ngay = $('#ngay1').textbox('getValue');
    var loai = $('#loai').textbox('getValue');
    var giaovien = document.getElementById('giaovien').value
    var phonghoc = $('#phonghoc').textbox('getValue');
    var tenlop = $('#tenlop').textbox('getValue');
    var curl = window.location.href;
    var res = curl.split("/");
    if (thang.length > 0 || nam.length > 0 || gio.length > 0 || giaovien.length > 0 ||
        loai.length > 0 || phonghoc.length > 0 || tenlop.length > 0 || ngay.length > 0) {
        if (res[res.length - 1] == 'lichhocgv')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/json?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonview?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonday?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsontime?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'duyetview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonduyet?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
    } else {
        if (res[res.length - 1] == 'lichhocgv')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/json";
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonview";
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonday";
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsontime";
        else if (res[res.length - 1] == 'duyetview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonduyet";
    }
    $("#dg").datagrid("reload");
}

function next() {
    var thang = $('#thang').textbox('getValue');
    var nam = $('#nam').textbox('getValue');
    var x = parseInt(thang) + 1;
    if (x < 13) {
        if (x < 10)
            thang = '0' + x;
        else
            thang = x;
        $('#thang').textbox('setValue', thang);
    } else {
        thang = '01';
        var y = parseInt(nam) + 1;
        $('#thang').textbox('setValue', thang);
        $('#nam').textbox('setValue', y);
    }
    var curl = window.location.href;
    var res = curl.split("/");
    var gio = $('#giohoc').timespinner('getValue');
    var ngay = $('#ngay1').textbox('getValue');
    var loai = $('#loai').textbox('getValue');
    var giaovien = document.getElementById('giaovien').value
    var phonghoc = $('#phonghoc').textbox('getValue');
    var tenlop = $('#tenlop').textbox('getValue');
    var curl = window.location.href;
    var res = curl.split("/");
    if (thang.length > 0 || nam.length > 0 || gio.length > 0 || giaovien.length > 0 ||
        loai.length > 0 || phonghoc.length > 0 || tenlop.length > 0 || ngay.length > 0) {
        if (res[res.length - 1] == 'lichhocgv')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/json?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonview?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonday?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsontime?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'duyetview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonduyet?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
    } else {
        if (res[res.length - 1] == 'lichhocgv')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/json";
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonview";
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonday";
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsontime";
        else if (res[res.length - 1] == 'duyetview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhocgv/jsonduyet";
    }
    $("#dg").datagrid("reload");
}


function add() {
    $('#dlg-add').dialog({
        title: '&nbsp;Add schedule',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-add').form('clear');
    $('#lop_hoc1').combobox({
        onSelect: function (rec) {
            document.getElementById('sobuoi').value = rec.so_buoi;
            document.getElementById('buoikm').value = rec.buoi_khuyen_mai;
            document.getElementById('hocvien').value = rec.hoc_vien;
            document.getElementById('phanloai').value = rec.phan_loai;
        }
    });
    url = baseUrl + '/lichhocgv/add';
}

function change1() {
    $('#dlg-add').dialog({
        title: '&nbsp;Change schedule',
        iconCls: 'icon-reload',
        closed: false
    });
    $('#fm-add').form('clear');
    $('#lop_hoc1').combobox({
        onSelect: function (rec) {
            document.getElementById('sobuoi').value = rec.so_buoi;
            document.getElementById('buoikm').value = rec.buoi_khuyen_mai;
            document.getElementById('hocvien').value = rec.hoc_vien;
            document.getElementById('phanloai').value = rec.phan_loai;
        }
    });
    url = baseUrl + '/lichhocgv/change';
}

function format_link(val, row) {
    if (val != '')
        return '<a href="' + val + '" target="_blank" style="color: blue;font-weight: bold;text-decoration: none;">View</a>';
    else
        return '';
}

function xemthem(ngay) {
    alert(ngay);
}

function edit() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        if(row.tinh_trang==1) {
            $('#dlg').dialog({
                title: '&nbsp;Edit info schedule',
                iconCls: 'icon-edit',
                closed: false
            });
            $('#fm').form('load', row);
            document.getElementById('applyallweekday').checked=false;
            document.getElementById('applyalltime').checked=false;
            document.getElementById('applyall').checked=false;
            document.getElementById('ngaycu').value = row.ngay;
            url = baseUrl + '/lichhocgv/update?id=' + row.id;
        }else{
            show_messager('Schedule is done');
        }
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function daythay() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        if (row.loailop == 1) {
            if (row.tinh_trang == 1) {
                $.post(baseUrl + "/lichhocgv/checkdaythay", { lichhoc: row.id, },
                function (result) {
                    if (result != '') {
                        $.messager.alert('Notice', "Yêu cầu đã được đề xuất! Vui lòng chờ kiểm duyệt!", 'warning');
                    } else {
                        $('#dlg-daythay').dialog({
                            title: '&nbsp;Dạy thay',
                            iconCls: 'icon-edit',
                            closed: false
                        });
                        $("#gv_daythay").combobox({ url: 'lichhocgv/gvdaythay?giaovien=' + row.giao_vien });
                        url = baseUrl + '/lichhocgv/adddaythay?id=' + row.id +'&giaovien=' +row.giao_vien;
                    } 
                }, "json");
               
            } else {
                $.messager.alert('Notice', "Lịch học đang diễn ra hoặc đã kết thúc", 'warning');
            }
        }
    } else {
        $.messager.alert('Notice', 'Choose a schedule', 'warning');
    }
}

function cancel() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        if(row.tinh_trang==1) {
            $.messager.confirm("Notice", "Are you sure to cancel this schedule?", function (r) {
                if (r) {
                    $.post('lichhocgv/huylich?lichhoc=' + row.id, function (result) {
                        result = (JSON).parse(result);
                        if (result.success) {
                            $('#dg').datagrid('reload');
                        } else {
                            show_messager(result.msg);
                        }
                    });
                }
            });
        }
    }else
        show_messager('Pls select schedule');
}

function save() {
    // document.getElementById('fm').action=url;
    // document.getElementById('fm').submit();
    $('#fm').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
                $('#dg').datagrid('clearSelections');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function edittime() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        if (row.loailop == 1) {
            if (row.tinh_trang == 1) {
                $('#dlg-edittime').dialog({
                    title: '&nbsp;Edit Time',
                    iconCls: 'icon-edit',
                    closed: false
                });
                $('#fm-edittime').form('load', row);
                url = baseUrl + '/lichhocgv/suathoiluong?id=' + row.id;
            } else {
                $.messager.alert('Notice', "Lịch học đang diễn ra hoặc đã kết thúc", 'warning');
            }
        }
    } else {
        $.messager.alert('Notice', 'Choose a schedule', 'warning');
    }
}

function savetime() {
    $('#fm-edittime').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-edittime').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}


function saveadd() {
    // document.getElementById('fm-add').action=url;
    // document.getElementById('fm-add').submit();
    $('#fm-add').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-add').dialog('close');
                $('#dg').datagrid('reload');
                $('#dg').datagrid('clearSelections');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function savedaythay() {
    $('#fm-daythay').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-daythay').dialog('close');
                // $('#dg').datagrid('reload');
                // $('#dg').datagrid('clearSelections');
                show_messager(result.msg);
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function tableview() {
    window.location.assign('lichhocgv/view');
}

function dayview() {
    window.location.assign('lichhocgv/dayview');
}

function indexview() {
    window.location.assign('lichhocgv');
}

function timeview() {
    window.location.assign('lichhocgv/timeview');
}

function duyetview() {
    window.location.assign('lichhocgv/duyetview');
}

function tinhtrang(val, row) {
    if (val == 1)
        return 'New';
    else if (val == 2)
        return 'Setup';
    else if (val == 3)
        return 'Studying';
    else if (val == 4)
        return 'Done';
    else if (val == 5)
        return 'Đã chốt';
    else if (val == 6)
        return 'Cọc ngay';
    else if (val == 7)
        return 'Cancel';
    else
        return '';
}

function del() {
    $('#dlg-del').dialog({
        title: '&nbsp;Del schedule',
        iconCls: 'icon-no',
        closed: false
    });
}


function savedel() {
    $.messager.confirm('Notice', 'Are you sure you want to delete all schedules?', function (r) {
        $('#fm-del').form('submit', {
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    $('#dlg-del').dialog('close');
                    $('#dg').datagrid('reload');
                } else {
                    show_messager(result.msg);
                }
            }
        });
    });
}

$(function () {
    $('#tt').tree({
        onSelect: function (node) {
            $('#dg').datagrid('options').url = 'vanban/json?folder=' + node.id;
            $('#dg').datagrid('reload');
        }
    });
    $('#folderm').combotree({
        onSelect: function (row) {
            document.getElementById('idfolder').value = row.id;
            document.getElementById('upload').style = 'display:block;';
        }
    });
});

function formatlink(val, row) {
    return '<a style="text-decoration:underline; color:blue" href="' + baseUrl + '/uploads/vanban/' + row.link + row.filename + '" target="_blank">Xem/Tải về</a>';
}

function themphanloai() {
    var row = $('#tt').tree('getSelected');
    if (row) {
        $.post(baseUrl + '/vanban/checkfolder', {
            id: row.id
        }, function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-phanloai').dialog({
                    title: '&nbsp;Tạo folder ',
                    iconCls: 'icon-add',
                    closed: false
                });
                $('#fm-phanloai').form('clear');
                $('#parentid').combotree('setValue', row.id);
            } else
                show_messager(result.msg);
        });
    } else {
        $('#dlg-phanloai').dialog({
            title: '&nbsp;Tạo folder ',
            iconCls: 'icon-add',
            closed: false
        });
        // alert();
    }
    url = baseUrl + '/vanban/themphanloai';
    $('#fm-phanloai').form('clear');
}

function savephanloai() {
    // var row = $('#tt').tree('getSelected');
    // alert(baseUrl);
    // document.getElementById('fm-phanloai').action=url;
    // document.getElementById('fm-phanloai').submit();
    $('#fm-phanloai').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-phanloai').dialog('close');
                $('#tt').tree('reload');
                $('#parentid').combotree('reload');
                $('#tt').tree({
                    onLoadSuccess:function(){
                        var node = $(this).tree('find',result.fid);
                        if (node){
                            $(this).tree('select', node.target);
                        }
                    }
                });
                show_messager(result.msg);
            } else {
                show_messager(result.msg);
            }
        }
    });
}


function doiten() {
    var row = $('#tt').tree('getSelected');
    if (row) {
        $.post(baseUrl + '/vanban/checkfolder', {
            id: row.id
        }, function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-editfolder').dialog({
                    title: '&nbsp;Sửa tên folder ',
                    iconCls: 'icon-edit',
                    closed: false
                });
                $('#fm-editfolder').form('clear');
                $('#foldername').textbox('setValue', row.name);
                $('#thu_tu1').textbox('setValue', row.thu_tu);
            } else
                show_messager(result.msg);
        });
    }
    else
        show_messager('Chưa chọn folder')
    url = baseUrl + '/vanban/suafolder?id='+row.id;
}

function editfolder() {
    $('#fm-editfolder').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-editfolder').dialog('close');
                $('#parentid').combotree('reload');
                $('#tt').tree('reload');
                $('#tt').tree({
                    onLoadSuccess:function(){
                        var node = $(this).tree('find',result.fid);
                        if (node){
                            $(this).tree('select', node.target);
                        }
                    }
                });
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function timkiem() {
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    var tukhoa = $('#tukhoa').textbox('getValue');
    if (tungay.length > 0 || denngay.length > 0 || tukhoa.length > 0) {
        $('#dg').datagrid('options').url = baseUrl + '/vanban/json?tungay=' + tungay + '&denngay=' + denngay + '&tukhoa=' + tukhoa;
    } else
        $('#dg').datagrid('options').url = baseUrl + '/vanban/json';
    $('#dg').datagrid('reload');
}

function add() {
    var row = $('#tt').tree('getSelected');
    if (row) {
        $.post(baseUrl + '/vanban/checkfolder', {
            id: row.id
        }, function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog({
                    title: '&nbsp;Tải văn bản lên ',
                    iconCls: 'icon-add',
                    closed: false
                });
                $('#fm').form('clear');
                $('#folder').combotree({required: true});
                $('#file').filebox({required: true});
                $('#folder').combotree('setValue', row.id);
                url = baseUrl + '/vanban/add';
            } else
                show_messager(result.msg);
        });
    }
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Cập nhật ',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('load', row);
        $('#folder').combotree({readonly: true, required: false});
        $('#folder').combotree('setValue', row.folder);
        $('#file').filebox({required: false});
        url = baseUrl + '/vanban/update?id=' + row.id;
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function save() {
    // document.getElementById('fm').action=url;
    // document.getElementById('fm').submit();
    $('#fm').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
                $('#tt').tree({
                    onLoadSuccess:function(){
                        var node = $(this).tree('find',result.fid);
                        if (node){
                            $(this).tree('select', node.target);
                        }
                    }
                });
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function xoafolder() {
    var row = $('#tt').tree('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa folder này?', function (r) {
            if (r) {
                $.post(baseUrl + '/vanban/xoafolder', {
                    id: row.id
                }, function (result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#tt').tree('reload');
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function del() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa văn bản này?', function (r) {
            if (r) {
                $.post(baseUrl + '/vanban/del', {
                    id: row.id
                }, function (result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                        $('#tt').tree({
                            onLoadSuccess:function(){
                                var node = $(this).tree('find',result.fid);
                                if (node){
                                    $(this).tree('select', node.target);
                                }
                            }
                        });
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}


function uploadmulti() {
    var row = $('#tt').tree('getSelected');
    if (row) {
        $.post(baseUrl + '/vanban/checkfolder', {
            id: row.id
        }, function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-multiupload').dialog({
                    title: '&nbsp;Tải nhiều file ',
                    iconCls: 'icon-add',
                    closed: false
                });
                $('#folderm').combotree('setValue', row.id);
                document.getElementById('idfolder').value = row.id;
                document.getElementById('upload').style = 'display:block;';
            } else
                show_messager(result.msg);
        });
    }
}

function bieumau() {
    $('#dlg-bieumau').dialog({
        title: '&nbsp;Download biểu mẫu ',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-bieumau').form('clear');
}

function download() {
    var bieumau = $('#bieumau').combobox('getValue');
    if (bieumau) {
        $('#dlg-bieumau').dialog('close');
        window.location.href =  bieumau;
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

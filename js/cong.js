function add() {
	  $('#dlg').dialog({
        title: '&nbsp;Thêm chấm công',
				iconCls:'icon-add',
				closed:false
    });
    $('#fm').form('clear');
    url = baseUrl + '/cong/add';
}

function nhom(val,row) {
		if (val==1) return 'Ngày công';
	  else if (val==2) return 'Lễ tết';
		else if (val==3) return 'Nghỉ phép';
		else if (val==4) return 'Công tác';
		else if (val==5) return 'Nghỉ bù';
		else if (val==6) return 'Đi muộn về sớm';
		else return 'Khác';
}

function edit(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
		$('#dlg').dialog({
			title: '&nbsp;Sửa thông tin',
			iconCls:'icon-edit',
			closed:false
		});
        $('#fm').form('load',row);
        url = baseUrl+'/cong/update?id='+ row.id;
    }
 	 else
        show_messager('Không có bản ghi nào được chọn');
}

function save(){
    $('#fm').form('submit',{
       	url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');
								$('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}


function del(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
            if (r){
                $.post(baseUrl+'/cong/del',{id:row.id},function(result){
				    if (result.success){
				        show_messager(result.msg);
				        $('#dg').datagrid('reload');
                        $('#dg').datagrid('clearSelections');
				    } else {
						show_messager(result.msg);
				    }
				},'json');
            }
        });
    }else{
		show_messager('Không có bản ghi nào được chọn');
    }
}

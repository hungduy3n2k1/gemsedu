function timkiem() {
    var tinhtrang = $('#tinhtrang').combobox('getValue');
    if (tinhtrang.length>0)
       	$('#dg').datagrid('options').url = baseUrl + '/thongbao/json?tinhtrang='+tinhtrang;
    else
        $('#dg').datagrid('options').url = baseUrl + '/thongbao/json';
    if($('#dlg-search').dialog)
        $('#dlg-search').dialog('close');
    $('#dg').datagrid('reload');
}

function add() {
	$('#dlg').dialog({
        title: '&nbsp;Thêm thông báo mới',
		iconCls:'icon-add',
		closed:false
    });
    $('#fm').form('clear');
    tinymce.get("noi_dung").setContent('');
    url = baseUrl + '/thongbao/add';
}

function format_giahan(val,row){
    if (val == 1){
        return 'Có';
    } else {
        return 'Không';
    }
}


function edit(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
		$('#dlg').dialog({
			title: '&nbsp;Sửa thông báo',
			iconCls:'icon-edit',
			closed:false
		});
        $('#fm').form('load',row);
        $('#ngay_gio').datetimebox({
            value: (row.ngaygio) 
        });
        $('#ket_thuc').datetimebox({
            value: (row.ketthuc) 
        });
        tinymce.get("noi_dung").setContent(row.noi_dung);
        url = baseUrl+'/thongbao/update?id='+ row.id;
    }
	else
        show_messager('Không có bản ghi nào được chọn');
}

function save(){
    $('#fm').form('submit',{
       	url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');
				$('#dg').datagrid('reload');
                $('#dg').datagrid('clearSelections');
            } else {
                show_messager(result.msg);
            }
        }
    });
}


function del(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
            if (r){
                $.post(baseUrl+'/thongbao/del',{id:row.id},function(result){
				    if (result.success){
				        show_messager(result.msg);
				        $('#dg').datagrid('reload');
                        $('#dg').datagrid('clearSelections');
				    } else {
						show_messager(result.msg);
				    }
				},'json');
            }
        });
    }else{
		show_messager('Không có bản ghi nào được chọn');
    }
}

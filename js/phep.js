function search() {
		var thang = $('#thang').combobox('getValue');
		var nam = $('#nam').combobox('getValue');
		if (thang.length>0 || nam.length>0 ) {
	        $('#dg').datagrid('options').url = 'phep/json?thang='+thang+'&nam='+nam;
		} else{
		     	$('#dg').datagrid('options').url = 'phep/json';
		}
		$('#dg').datagrid('reload');
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Cập nhật ',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('load', row);
        url = baseUrl + '/phep/update?id=' + row.id;
        console.log(url);
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function save() {
    $('#fm').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

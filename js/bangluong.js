function daduyet(val,row) {
    if (row.tinh_trang==0)
        return '<span style="color:red">'+val+'</span>';
    else
        return val;
}

function timkiem() {
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    // var full = document.getElementById('full').value;
   // show_messager('bangluong/json?full='+full+'&thang='+thang+'&nam='+nam);
    if (thang.length > 0 || nam.length > 0) {
        $('#dg').datagrid('options').url = 'bangluong/json?&thang='+thang+'&nam='+nam;
    } else {
        $('#dg').datagrid('options').url = 'bangluong/json';
    }
    $('#dg').datagrid('reload');
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Điều chỉnh bảng lương',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('load', row);
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function save() {
    $('#fm').form('submit', {
        url: baseUrl + '/bangluong/update',
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function lapbang() {
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    $.post('bangluong/lapbang', {thang: thang,nam:nam}, function(result) {
        if (result.success) {
            show_messager(result.msg);
            $('#dg').datagrid('options').url = 'bangluong/json?thang='+thang+'&nam='+nam;
            $('#dg').datagrid('reload');
        } else {
            show_messager(result.msg);
        }
    }, 'json');
    // $.get('bangluong/checkduyet?thang='+thang+'&nam='+nam, function(result,status) {
    //     if (result==0)
    //         show_messager('Chưa có bảng lương hoặc bảng lương đã duyệt!');
    //     else {
    //         var full = document.getElementById('full').value;
    //         $.messager.confirm('Thông báo', 'Bạn muốn lập lại bảng lương tháng '+thang+'/'+nam, function(r) {
    //             if (r) {
    //                 $.post(baseUrl + '/bangluong/lapbang', {
    //                     thang: thang,nam:nam
    //                 }, function(result) {
    //                     if (result.success) {
    //                         show_messager(result.msg);
    //                         $('#dg').datagrid('options').url = 'bangluong/json?full='+full+'&thang='+thang+'&nam='+nam;
    //                         $('#dg').datagrid('reload');
    //                     } else {
    //                         show_messager(result.msg);
    //                     }
    //                 }, 'json');
    //             }
    //         });
    //     }
    // });
}

function duyet() {
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    var full = document.getElementById('full').value;
    $.messager.confirm('Thông báo', 'Bạn có chắc chắn duyệt bảng lương tháng '+thang+'/'+nam, function(r) {
        if (r) {
            $.post(baseUrl + '/bangluong/duyet', {
                thang: thang,nam:nam
            }, function(result) {
                if (result.success) {
                    show_messager(result.msg);
                    $('#dg').datagrid('options').url = 'bangluong/json?full='+full+'&thang='+thang+'&nam='+nam;
                    $('#dg').datagrid('reload');
                } else {
                    show_messager(result.msg);
                }
            }, 'json');
        }
    });
}

function xuatfile() {
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');

    // var data = $('#dg').datagrid('getRows');
    // if (Object.keys(data).length) {
    //       document.getElementById('rows').value = JSON.stringify(data)
    //       $('#fm-xuatfile').form('submit');
                // $.post(baseUrl + '/bangluong/xuatfile');

    // } else {
    //     show_messager('Chưa có dữ liệu');
    // }

  // document.getElementById('getrow').value=;
//  alert(rows);
  //if (thang.length > 0 && thang.length > 0  ) {
        window.location.href = baseUrl + '/bangluong/xuatfile?thang='+thang+'&nam='+nam;
  //} else
    //    show_messager('Chưa chọn tháng');
}

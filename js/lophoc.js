$(function () {
    $('#tukhoa').textbox({
        onChange: function (rec) {
            timkiem();
        }
    });
    $('#hoc_vien').combobox({
        onSelect: function (row) {
            $("#don_hang").combobox({ url: 'lophoc/donhang?hoc_vien=' + row.id });
        }
    });
    $('#don_hang').combobox({
        formatter: function (row) {
            return "DH-" + row.id + '&nbsp;(<span style="font-weight:bold;">' + row.hocvien + ')</span>&nbsp;(<span>' + row.so_tien + ')</span>';
            //return '<span id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
        },
        onSelect: function (row) {
            // $("#khoa_hoc").combobox({ value: row.product });
            // $("#so_buoi").textbox({ value: row.so_buoi });
            // $("#buoi_khuyen_mai").textbox({ value: row.khuyen_mai });
            var sotien = row.so_tien.replace(/,/g, "");
            $("#so_tien").textbox({ value: sotien });
        }
    });
    $('#khoa_hoc').combobox({
        onSelect: function (row) {
            $('#so_buoi').textbox({value: row.so_buoi});
            // $('#sotien').textbox({value: row.don_gia});
        }
    });
});

function gioitinh(val, row) {
    if (val == 1)
        return 'Nam';
    else
        return 'Nữ';
}

function format_phanloai(val, row) {
    if (val == 1)
        return 'Online';
    else if (val == 2)
        return 'Mầm non (S)';
    else if (val == 3)
        return 'Tiểu học (F)';
    else if (val == 4)
        return 'Trung học (E)';
    else if (val == 5)
        return 'Demo 30';
    else if (val == 6)
        return 'Demo 45';
    else if (val == 7)
        return 'Demo 60';
    else
        return '';
}

function format_phanloai2(val, row) {
    if (row.phan_loai == 1) {
        if (val == 1)
            return 'Kid';
        else if (val == 2)
            return 'Giao tiếp';
        else if (val == 3)
            return 'Demo';
        else
            return '';
    } else {
        if (val == 1)
            return 'Main';
        else if (val == 2)
            return 'Grammar';
        else if (val == 3)
            return 'Club';
        else
            return '';
    }
}

function timkiem() {
    var tukhoa = $('#tukhoa').textbox('getValue');
    var giaovien = $('#giaovien').combobox('getValue');
    var khoahoc = $('#khoahoc').combobox('getValue');
    var phanloai = $('#phanloai').combobox('getValue');
    var tinhtrang = $('#tinhtrang').combobox('getValue');
    if (tukhoa.length > 0 || giaovien.length > 0 || khoahoc.length > 0 || phanloai.length > 0 || tinhtrang.length > 0)
        $('#dg').datagrid('options').url = baseUrl + '/lophoc/json?tukhoa=' + tukhoa + '&giaovien=' + giaovien +
            '&khoahoc=' + khoahoc + '&phanloai=' + phanloai + '&tinhtrang=' + tinhtrang;
    else
        $('#dg').datagrid('options').url = baseUrl + '/lophoc/json';
    $('#dg').datagrid('reload');
}

function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm lớp học mới ',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm').form('clear');
    $('#tinh_trang').combobox({value: 1});
    $('#phan_loai').combobox({
        onSelect: function (row) {
            $("#phan_loai_2").combobox({url: 'lophoc/phanloai2?phanloai=' + row.id});
            if (row.id == 1) {
                $('#thoi_luong').numberbox({value: 45});
                $('#hocvien').combobox({readonly: false});
            } else {
                $('#hocvien').combobox({readonly: true});
            }
            if (row.id == 2 || row.id == 3 || row.id == 4)
                $('#thoi_luong').numberbox({value: 90});
            if (row.id == 5)
                $('#thoi_luong').numberbox({value: 30});
            if (row.id == 6)
                $('#thoi_luong').numberbox({value: 45});
            if (row.id == 7)
                $('#thoi_luong').numberbox({value: 60});
        }
    });
    
    document.getElementById('trtinhtrang1').style.display = "none";
    document.getElementById('trtinhtrang2').style.display = "none";
    url = baseUrl + '/lophoc/add';
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Xem và cập nhật thông tin lớp học',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#phan_loai').combobox({
            onSelect: function (row) {
                $("#phan_loai_2").combobox({url: 'lophoc/phanloai2?phanloai=' + row.id});
            }
        });
        $("#phan_loai_2").combobox({value:row.phan_loai_2})
        $('#fm').form('clear');
        $('#fm').form('load', row);
        $('#thoi_luong').numberbox({value: row.thoi_luong});
        document.getElementById('trtinhtrang1').style.display = "table-cell";
        document.getElementById('trtinhtrang2').style.display = "table-cell";
        url = baseUrl + '/lophoc/update?id=' + row.id+'&giaoviencu='+row.giao_vien+'&hocviencu='+row.hoc_vien;
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function save() {
    // document.getElementById('fm').action=url;
    // document.getElementById('fm')
    var name = $('#name').val();
    var ngaybatdau = $('#ngaybatdau').val();
    var phan_loai = $('#phan_loai').val();
    var phan_loai_2 = $('#phan_loai_2').val();
    var thoi_luong = $('#thoi_luong').val();
    if(name != '' && ngaybatdau != '' && phan_loai != '' && phan_loai_2 != '' && thoi_luong != ''){
        $('#dlg-buttons').children('button')[0].disabled = true;
    }
    $('#fm').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
           // alert(result);
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
            $('#dlg-buttons').children('button')[0].disabled = false;
        }
    });
}

function del() {
    var row = $('#dg').treegrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa?', function (r) {
            if (r) {
                $.post(baseUrl + '/lophoc/del', {id: row.id}, function (result) {
                    if (result.success) {
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function hocvien() {
    var row = $('#dg').treegrid('getSelected');
    if (row) {
        $('#dlg-hocvien').dialog({
            title: '&nbsp;Danh sách học viên',
            iconCls: 'icon-customer',
            closed: false
        });
        $('#dg-hocvien').datagrid('options').url = baseUrl + '/lophoc/jsonhocvien?lophoc=' + row.id;
        $('#dg-hocvien').datagrid('reload');
    } else {
        show_messager('Không có bản ghi nào được chọn!');
    }
}

function nhap() {
    $('#dlg-nhap').dialog({
        title: '&nbsp;Nhập học viên từ file exccel',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-nhap').form('clear');
}

function nhapexel() {
    // document.getElementById('fm-nhap').submit();
    $('#fm-nhap').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-nhap').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function lichhoc() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        window.location.href = baseUrl + '/lophoc/lichhoc?lophoc=' + row.id + '&tenlophoc=' + row.name;
    } else {
        show_messager('Bạn chưa chọn học viên!');
    }
}



$(function () {
    $('#khach_hang').combobox({
        onSelect: function (row) {
            $("#don_hang").combobox({url: 'invoice/donhang?khachhang='+row.id});
        }
    });
    $('#don_hang').combobox({
        formatter: function (row) {
            return "DH-" + row.id + '&nbsp;(<span style="font-weight:bold;">'+row.hocvien+')</span>';
            //return '<span id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
        },
        onSelect: function (row) {
            $("#hocvien").textbox({value:row.hocvien});
            $("#khoahoc").combobox({value:row.product});
            $("#sobuoi").textbox({value:row.so_buoi});
            $("#tongtien").textbox({value:Comma(row.so_tien)});
        }
    });
    $('#invoice').combobox({
        onSelect: function (row) {
            $("#so_tien").textbox({value:Comma(row.so_tien)});
        }
    });
});

function loai(val, row) {
    if (val == 1)
        return 'Mới';
    else
        return 'Gia hạn';
}

function tinhtrang(val, row) {
    if (val == 1)
        return 'Chờ thanh toán';
    else if (val == 2)
        return '<span style="font-weight: bold; color:#681818;">Thanh toán 1 phần</span>';
    else if(val == 3)
        return '<span style="font-weight: bold; color:blue;">Đã thanh toán</span>';
    else
        return '';
}

function gui(val, row) {
    if (val > '0000-00-00 00:00:00') {
        var date = new Date(val);
        var year = date.getFullYear();
        var month = date.getMonth() + 1; // months are zero indexed
        var day = date.getDate();
        var hour = date.getHours();
        var minute = date.getMinutes();
        var second = date.getSeconds();
        return day + "/" + month + "/" + year + " " + hour + ":" + minute;
    }
}

function gui1(val, row) {
    if (val == '0000-00-00 00:00:00' || val == '') {
        return '<a href="' + val + '" target="_blank" style="text-decoration:underline; color:blue;">Gửi ngay</a>';
    } else {
        var date = new Date(val);
        var year = date.getFullYear();
        var month = date.getMonth() + 1; // months are zero indexed
        var day = date.getDate();
        var hour = date.getHours();
        var minute = date.getMinutes();
        var second = date.getSeconds();
        return day + "/" + month + "/" + year + " " + hour + ":" + minute;
    }
}

function gui2(val, row) {
    if (val == '0000-00-00 00:00:00' || val == '') {
        if (row.lan_1 > '0000-00-00 00:00:00')
            return '<a href="' + val + '" target="_blank" style="text-decoration:underline; color:blue;">Gửi ngay</a>';
    } else {
        var date = new Date(val);
        var year = date.getFullYear();
        var month = date.getMonth() + 1; // months are zero indexed
        var day = date.getDate();
        var hour = date.getHours();
        var minute = date.getMinutes();
        var second = date.getSeconds();
        return day + "/" + month + "/" + year + " " + hour + ":" + minute;
    }
}

function gui3(val, row) {
    if (val == '0000-00-00 00:00:00' || val == '') {
        if (row.lan_2 > '0000-00-00 00:00:00')
            return '<a href="' + val + '" target="_blank" style="text-decoration:underline; color:blue;">Gửi ngay</a>';
    } else {
        var date = new Date(val);
        var year = date.getFullYear();
        var month = date.getMonth() + 1; // months are zero indexed
        var day = date.getDate();
        var hour = date.getHours();
        var minute = date.getMinutes();
        var second = date.getSeconds();
        return day + "/" + month + "/" + year + " " + hour + ":" + minute;
    }
}

function timkiem() {
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    var khachhang = $('#khachhang').combobox('getValue');
    var nhanvien = '';
    // show_messager('invoice/json?tungay=' + tungay + '&denngay=' + denngay + '&khachhang=' + khachhang+'&nhanvien='+nhanvien);
    if (tungay.length > 0 || denngay.length > 0 || khachhang.length > 0 || nhanvien.length>0) {
        $('#dg').datagrid('options').url = 'invoice/json?tungay=' + tungay + '&denngay=' + denngay + '&khachhang=' + khachhang+'&nhanvien='+nhanvien;
    } else {
        $('#dg').datagrid('options').url = 'invoice/json'
    }
    $('#dg').datagrid('reload');
}

// function gop() {
//     $('#dlg-gop').dialog({
//         title: '&nbsp; Gộp invoice',
//         iconCls: 'icon-sum',
//         closed: false,
//     });
//     var tungay = $('#tungay').datebox('getValue');
//     var denngay = $('#denngay').datebox('getValue');
//     $('#invoice').combobox({url: 'invoice/invoice?tungay=' + tungay + '&denngay=' + denngay});
// }
//
// function gopinvoice() {
//     $('#fm-gop').form('submit', {
//         onSubmit: function () {
//             return $(this).form('validate');
//         },
//         success: function (result) {
//             var result = eval('(' + result + ')');
//             if (result.success) {
//                 show_messager(result.msg);
//                 // $('#dlg-noidung').dialog('close');
//                 // $('#dg').datagrid('reload');
//             } else {
//                 show_messager(result.msg);
//             }
//         }
//     });
// }

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        if (row.tinh_trang == 1) {
            $('#dlg').dialog({
                title: '&nbsp;Cập nhật',
                iconCls: 'icon-edit',
                closed: false,
            });
            $('#fm').form('clear');
            $('#fm').form('load', row);
            $('#khach_hang').combobox({value: row.idkh});
            $('#don_hang').combobox({value: row.don_hang});
            $('#khach_hang').combobox({readonly: true});
            $('#don_hang').combobox({readonly: true});
            url = baseUrl + '/invoice/update?id=' + row.id + '&tiencu=' + row.so_tien;
        }else{
            show_messager('Invoice đã thanh toán không thể sửa!');
        }
    }else
            show_messager('Không có bản ghi nào được chọn');
}

function save() {
    $('#fm').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
             //   show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function del() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        if (row.tinh_trang == 1) {
            $.messager.confirm("Thông báo", "Bạn có chắc chắn muốn xóa bản ghi này?", function (r) {
                if (r) {
                    $.post("invoice/del", {id: row.id, khachhang: row.khach_hang},
                        function (result) {
                            if (result.success) {
                                show_messager(result.msg);
                                $("#dg").datagrid("reload");
                            } else {
                                show_messager(result.msg);
                            }
                        }, "json");
                }
            });
        }else{
            show_messager('Invoice đã thanh toán không thể xóa!');
        }
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function sendmail() {
    $('#dlg-sendmail').dialog({
        title: '&nbsp;Send mail',
        iconCls: 'icon-edit',
        closed: false
    });
    $('#fm-send').form('clear');
    var row = $('#dg').datagrid('getSelected');
    $('#emailtosend').combobox({
        url: 'invoice/email?id=' + row.khach_hang,
        valueField: 'email',
        textField: 'email',
        required: true
    });
    if (row.lan_1 == '0000-00-00 00:00:00' || row.lan_1 == '')
        url = baseUrl + '/invoice/sendmail?id=' + row.id + '&lan=lan_1';
    else if (row.lan_2 == '0000-00-00 00:00:00' || row.lan_2 == '')
        url = baseUrl + '/invoice/sendmail?id=' + row.id + '&lan=lan_2';
    else
        url = baseUrl + '/invoice/sendmail?id=' + row.id + '&lan=lan_3';
}

function sendmailsubmit() {
    // document.getElementById('fm-send').action=url;
    // document.getElementById('fm-send').submit();
    $('#fm-send').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#emailtosend').combobox({required: false});
                $('#dlg-sendmail').dialog('close');
                $('#dlg-noidung').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}


function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm invoice mới',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm').form('clear');
    $('#ngay').datebox('setValue', homnay);
    $('#khach_hang').combobox({readonly:false});
    $('#don_hang').combobox({readonly:false});
}

function addrow1() { // Mở form thêm dịch vụ
    $('#dich_vu1').combobox({
        onSelect: function (row) {
            $('#don_gia1').numberbox('setValue', row.don_gia);
            $('#don_vi1').textbox('setValue', row.donvitinh);
            $('#thue_suat_vat1').numberbox('setValue', row.thue_suat_vat);
        }
    });
    $('#dlg-row1').dialog({
        title: '&nbsp;Thêm sản phẩm/dịch vụ',
        iconCls: 'icon-add',
        closed: false,
        buttons: [{
            text: 'OK',
            iconCls: 'icon-ok',
            handler: function () {
                saverow1();
            }
        }]
    });
    $('#chiet_khau_tm1').textbox({
        onChange: function (val) {
            if(val.length>0)
                $('#chiet_khau_pt1').textbox({readonly:true});
            else
                $('#chiet_khau_pt1').textbox({readonly:false});
        }
    });
    $('#chiet_khau_pt1').textbox({
        onChange: function (val) {
            if(val.length>0)
                $('#chiet_khau_tm1').textbox({readonly:true});
            else
                $('#chiet_khau_tm1').textbox({readonly:false});
        }
    });
    $('#fm-row1').form('clear');
    $('#tungay1').datebox('setValue', homnay);
    $('#denngay1').datebox('setValue', homnay);
}

function saverow1() { // thêm dịch vụ vào invoice
    $('#fm-row1').form('submit', {
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
           var hanghoaid = $('#dich_vu1').combobox('getValue');
            var hanghoa = $('#dich_vu1').combobox('getText');
            var dongia = $('#don_gia1').numberbox('getValue');
            var chietkhau = $('#chiet_khau_tm1').numberbox('getValue');
            var chietkhaupt = $('#chiet_khau_pt1').numberbox('getValue');
            var thuevat = $('#thue_suat_vat1').numberbox('getValue');
            var tangthang = $('#tang_thang1').numberbox('getValue');
            var soluong = $('#so_luong1').numberbox('getValue');
            var donviid = $('#don_vi1').combobox('getValue');
            var donvi = $('#don_vi1').combobox('getText');
            var tungay = $('#tungay1').datebox('getValue');
            var denngay = $('#denngay1').datebox('getValue');
            var tenmien = $('#ten_mien').textbox('getValue');
            var ghichu = $('#ghi_chu1').textbox('getValue');
            var thanhtien = Math.round((dongia - chietkhau) * (1 - chietkhaupt / 100) * (1 + thuevat / 100) * soluong);
            var lastid = $('#dg-add').datagrid('getRows').length;
            $('#dg-add').datagrid('appendRow', {
                hanghoaid: hanghoaid,
                hanghoa: hanghoa,
                dongia: dongia,
                donviid: donviid,
                donvi: donvi,
                soluong: soluong,
                chietkhau: chietkhau,
                chietkhaupt: chietkhaupt,
                thanhtien: thanhtien,
                thuevat: thuevat,
                tangthang: tangthang,
                tungay: tungay,
                denngay: denngay,
                tenmien: tenmien,
                ghichu: ghichu,
                xoa: '<a href="javascript:void(0)" onclick="delrow1(' + lastid + ')"><img src="public/easyui/icons/clear.png"></a>'
            });
            var rows = $('#dg-add').datagrid('getRows');
            var sum = 0;
            for (i = 0; i < rows.length; i++)
                sum += parseInt(rows[i].thanhtien);
            $('#dg-add').datagrid('reloadFooter', [{hanghoa: 'Tổng cộng:', dongia: 0, chietkhau: 0, thanhtien: sum}]);
            $('#dlg-row1').dialog('close');
        }
    });
    
}

function delrow1(index) { // xóa một dịch vụ trong báo giá
    $('#dg-add').datagrid('deleteRow', index);
    var data = $('#dg-add').datagrid('getRows');
    var sum = 0;
    data.forEach(function (row, index) {
        row.xoa = '<a href="javascript:void(0)" onclick="delrow(' + index + ')"><img src="public/easyui/icons/clear.png"></a>';
        sum += parseInt(row.thanhtien);
    });
    $('#dg-add').datagrid('loadData', data);
    $('#dg-add').datagrid('reloadFooter', [{hanghoa: 'Tổng cộng:', dongia: 0, chietkhau: 0, thanhtien: sum}]);
}

function save1(send) { // save báo giá mới
    var data = $('#dg-add').datagrid('getRows');
    data.forEach(function (row, index) {
        row.xoa = ''
    });
    document.getElementById('invoice1').value = JSON.stringify(data);
    var url1 = 'invoice/add?send=' + send;
    $('#fm-add').form('submit', {
        url: url1,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-add').dialog('close')
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

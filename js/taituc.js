function timkiem() {
    var ngaybd = $('#ngaybd').datebox('getValue');
    var ngaykt = $('#ngaykt').datebox('getValue');
    var phanloai = $('#phanloai').combobox('getValue');
    if (ngaybd.length>0 || ngaykt.length>0 || phanloai.length>0)
        $('#dg').datagrid('options').url = 'taituc/json?ngaybd='+ngaybd+'&ngaykt='+ngaykt+'&phanloai='+phanloai;
    else
        $('#dg').datagrid('options').url = 'taituc/json';
    $('#dg').datagrid('reload');
}

function phanloai(val, row){
    if(val==1){
        return 'New sale';
    } else if(val==2){
        return 'Tái tục trải nghiệm';
    } else if(val==3){
        return 'Tái tục new';
    } else {
        return '';
    }
}

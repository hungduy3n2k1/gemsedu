function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm ',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm').form('clear');
    url = baseUrl + '/chinhanh/add';
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Cập nhật ',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('load', row);
        url = baseUrl + '/chinhanh/update?id=' + row.id;
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function del() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa bản ghi này?', function(r) {
            if (r) {
                $.post(baseUrl + '/chinhanh/del', {
                    id: row.id
                }, function(result) {
                    if (result.success) {
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function save() {
    $('#fm').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

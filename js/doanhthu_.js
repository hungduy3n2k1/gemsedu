$(function() {
    // $('#dg').datagrid({
    //     view: detailview,
    //     detailFormatter: function(index, row) {
    //         return '<div id="ddv-' + index + '" style="padding:5px 0;height:120px;width:100%; line-height:20px"></div>';
    //     },
    //     onExpandRow: function(index, row) {
    //         $('#ddv-' + index).panel({
    //             border: false,
    //             cache: false,
    //             href: baseUrl + '/doanhthu/detail?id=' + row.khach_hang,
    //             onLoad: function() {
    //                 $('#dg').datagrid('fixDetailRowHeight', index);
    //             }
    //         });
    //     }
    // });
    $('#dg').datagrid({
        view: detailview,
        detailFormatter: function(index, row) {
            return '<div style="padding:2px;position:relative;"><table class="ddv"></table></div>';
        },
        onExpandRow: function(index, row) {
            var ddv = $(this).datagrid('getRowDetail', index).find('table.ddv');
            ddv.datagrid({
                url: baseUrl + '/doanhthu/jsondetail?id=' + row.id,
                fitColumns: true,
                singleSelect: true,
                rownumbers: true,
                height: 'auto',
                columns: [
                    [{
                        field: 'dichvu',
                        title: 'Dịch vụ',
                        width: 200
                    }, {
                        field: 'so_luong',
                        title: 'Số lượng',
                        width: 100
                    }, {
                        field: 'don_vi',
                        title: 'Đơn vị tính',
                        width: 100
                    }, {
                        field: 'don_gia',
                        title: 'Đơn giá',
                        width: 100,
                        align: 'right',
                        formatter: CurrencyFormatted
                    }, {
                        field: 'chiet_khau_tm',
                        title: 'Chiết khấu',
                        width: 100,
                        align: 'right',
                        formatter: CurrencyFormatted
                    },{
                        field: 'chiet_khau_pt',
                        title: 'Chiết khấu %',
                        width: 100,
                        align: 'center',
                    },{
                        field: 'thanhtien',
                        title: 'Thành tiền',
                        width: 100,
                        align: 'right',
                        formatter: CurrencyFormatted
                    }
                  ]
                ],
                onResize: function() {
                    $('#dg').datagrid('fixDetailRowHeight', index);
                },
                onLoadSuccess: function() {
                    // setTimeout(function() {
                            $('#dg').datagrid('fixDetailRowHeight', index);
                        // },
                        // 0);
                }
            });
            $('#dg').datagrid('fixDetailRowHeight', index);
        }
    });
});

function search() {
    $('#dlg-timkiem').dialog({
        title: '&nbsp;Lọc báo cáo',
        closed: false
    });
    $('#fm-timkiem').form('clear');
}

function tieptuc() {
		document.getElementById('fm-timkiem').submit();
}

function timkiem() {
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    if (tungay.length > 0 || denngay.length > 0) {
        $('#dg').datagrid('options').url = baseUrl + '/doanhthu/json?tungay=' + tungay + '&denngay=' + denngay;
        $('#dg').datagrid('reload');
    }
}

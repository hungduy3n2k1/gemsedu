$(function () {
    $('#sdt').textbox({
        onChange: function (value) {
            $.post(baseUrl + '/demo/checkphone', {phone: value},
                function (result) {
                    if (result.success) {
                        $('#khachhang').textbox('setValue', result.data);
                        show_messager(result.msg);
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
        }
    });
    $('#lop_hoctn').combobox({
        onSelect: function (rows) {
            $('#khoa_hoctn').combobox({value: rows.khoa_hoc});
            $('#giao_trinhtn').combobox({value: rows.giao_trinh});
        }
    });

    $('#dot_thanh_toan').combobox({
        onSelect: function (rec) {
            for (var k = 1; k < 7; k++) {
                $('#sotiendot' + k).numberbox({value: ''});
                $('#ngaydot' + k).datebox({value: ''});
            }
            var sotien = $('#so_tien').numberbox('getValue');
            var ngaydangky = $('#ngay_dang_ky').datebox('getValue');
            if (rec.id == 1) {
                $('#sotiendot1').numberbox({value: sotien});
                $('#ngaydot1').datebox({value: ngaydangky});
            } else {
                $.post(baseUrl + '/demo/chiadot', {
                    sotien: sotien,
                    ngaydangky: ngaydangky,
                    sodot: rec.id
                }, function (result) {
                    if (result.success) {
                        for (var i = 1; i <= rec.id; i++) {
                            $('#sotiendot' + i).numberbox({value: result.data[i].sotiendot});
                            $('#ngaydot' + i).datebox({value: result.data[i].ngaydot});
                        }
                        show_messager(result.msg);
                    } else {
                        show_messager(result.success)
                    }
                }, 'json');
            }
            var dot = rec.id;
            $('#sotiendot1').numberbox({
                onChange: function (val) {
                    if (dot > 1) {
                        var conlai = (sotien.replaceAll(',', '') - val) / (dot - 1);
                        for (var m = 2; m <= dot; m++) {
                            $('#sotiendot' + m).numberbox({value: conlai});
                        }
                    }
                }
            });
        }
    });
});

function phanloai(val, row) {
    if (val == 3)
        return 'Học thử Online';
    else if (val == 4)
        return 'Học thử Offline';
    else
        return '';
}

function timkiem() {
    // var tinhtrang = $('#tinhtrang').combobox('getValue');
    var phanloai = $('#phanloai').combobox('getValue');
    var tukhoa = $('#tukhoa').textbox('getValue');
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    if (phanloai.length > 0 || tukhoa.length > 0 || tungay.length>0 ||  denngay.length>0) {
        $('#dg').datagrid('options').url = baseUrl + '/demo/json?phanloai=' + phanloai + '&tukhoa=' + tukhoa+'&tungay='+tungay+'&denngay='+denngay;
    } else {
        $('#dg').datagrid('options').url = baseUrl + '/demo/json';
    }
    $('#dg').datagrid('reload');
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Cập nhật ',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('load', row);
        $('#lichhoc').datalist({
            title: 'Lịch học',
            url: 'demo/lichhoc?id=' + row.id,
            textField: 'noidung',
            lines: true
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function capnhat() {
    $('#fm').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function lichhoc(id) {
    $.post(baseUrl + '/demo/getrow', {id: id}, function (result) {
        if (result.success) {
            var row = result.data;
            if (row.phanloai == 3) {
                $('#dlg-lich').dialog({
                    title: '&nbsp;Cập nhật lịch học',
                    iconCls: 'icon-edit',
                    closed: false
                });
                $('#fm-lich').form('load', row);
                $('#idlich').val(row.id);
            } else {
                show_messager('Lớp demo offline ko được thay đổi giờ!');
            }
            show_messager(result.msg);
        } else {
            show_messager(result.msg);
        }
    }, 'json');
}

function save() {
    $('#fm-lich').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-lich').dialog('close');
                $('#lichhoc').datalist('reload');
                show_messager(result.msg);
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function add() {
    $('#dlg-them').dialog({
        title: '&nbsp;Thêm mới demo',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-them').form('clear');
}

function addsave() {
    $('#fm-them').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-them').dialog('close');
                $('#dg').datagrid('reload');
                show_messager(result.msg);
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function trainghiem() {
    $('#dlg-trainghiem').dialog({
        title: '&nbsp;Đăng ký học trải nghiệm',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-trainghiem').form('clear');
    var row = $('#dg').datagrid('getSelected');
    url = baseUrl + '/demo/trainghiem?id=' + row.id+'&kh='+row.khach_hang;
}

function savetn() {
    // document.getElementById('fm-trainghiem').action=url;
    // document.getElementById('fm-trainghiem').submit();
    $('#fm-trainghiem').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                $('#dlg-goi').dialog('close');
                $('#dlg-trainghiem').dialog('close');
                $('#dg').datagrid('reload');
                show_messager(result.msg);
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function dangky() {
    $('#dlg-dangky').dialog({
        title: '&nbsp;Đăng ký khóa học',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-dangky').form('clear');
    var row = $('#dg').datagrid('getSelected');
    $('#ngay_dang_ky').datebox({value: homnay});
    $('#product').combobox({
        onSelect: function (rec) {
            $('#buoi_hoc').textbox({value: rec.so_buoi});
            $('#so_tien').textbox({value: Comma(rec.don_gia)});
        }
    });
    $("#buoi_chot1").combobox({
        url: 'demo/buoichot?hocvien='+row.id,
        onSelect:function (rec) {
            $('#tinh_trang_chot1').combobox({value:rec.tinh_trang});
        }
    });
    url = baseUrl + '/demo/dangky?id=' + row.id+'&kh='+row.khach_hang;
}

function savedk() {
    // document.getElementById('fm-dangky').action=url;
    // document.getElementById('fm-dangky').submit();
    $('#fm-dangky').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                $('#dlg-dangky').dialog('close');
                $('#dg').datagrid('reload');
                show_messager(result.msg);
            } else {
                show_messager(result.msg);
            }
        }
    });
}

// function nhatky() {
//     $('#dlg-baocao').dialog({
//         title: '&nbsp;Ghi nhật ký',
//         iconCls: 'icon-add',
//         closed: false
//     });
//     $('#nhatky').val('');
// }


//     edit(row);
//     $('#hocvien').val(row.id);
//     $('#idhocvien').val(row.id);
//
//
// }

// $('#hotenphuhuynh').val('');
// $('#dienthoaiphuhuynh').val('');


// var tinhtrang = $('#tinhtrang').combobox('getValue');
// var nhanvien = $('#nhanvien').combobox('getValue');
// var tungay = $('#tungay').datebox('getValue');
// var denngay = $('#denngay').datebox('getValue');
// if (tinhtrang.length > 0 || nhanvien.length > 0 || tungay.length > 0 || denngay.length > 0) {
//     var url = baseUrl + '/demo/json?loaidata=' + loaidata + '&chuyenkhoa=' + chuyenkhoa + '&tinhtrang=' + tinhtrang + '&nhanvien=' + nhanvien + '&tungay=' + tungay + '&denngay=' + denngay;
// } else {
//     var url = baseUrl + '/baocaodangtin/json?loaidata=' + loaidata + '&chuyenkhoa=' + chuyenkhoa;
// }
// //show_messager(url);
// $('#dg').datagrid('options').url = url;
// $('#dg').datagrid('reload');
// $('#dlg-chondata').dialog('close');

// $('#tinh_trang').combobox({
//     onSelect: function (val) {
//         if (row.tinh_trang != 7 && val.id == 7) {
//             $('#dlg').dialog({
//                 closed: true
//             });
//             $('#fm').form('clear');
//             var name = $('#ho_ten').textbox('getValue');
//             var ngaysinh = $('#ngaysinh').textbox('getValue');
//             var dt = $('#dien_thoai').textbox('getValue');
//             var dc = $('#dia_chi').textbox('getValue');
//             var email = $('#email').textbox('getValue');
//             $('#ten_day_du').textbox({value: name});
//             $('#name').textbox({value: name});
//             $('#ngaysinh1').datebox({value: ngaysinh});
//             $('#dien_thoai1').textbox({value: dt});
//             $('#dia_chi1').textbox({value: dc});
//             $('#email1').textbox({value: email});
//             $('#loaiungvien').combobox({value: 1});
//             $('#dlg').dialog({
//                 title: '&nbsp;Chuyển sang khách hàng',
//                 iconCls: 'icon-edit',
//                 closed: false
//             });
//             $('#thoigian').datebox('setValue', 'today');
//             $('#nhan_vien').combobox('setValue', manhanvien);
//             // $('#dien_thoai').numberbox({required:true});
//             document.getElementById("hinhanh").src = 'https://cdn5.vectorstock.com/i/1000x1000/82/59/anonymous-user-flat-icon-vector-18958259.jpg';
//             document.getElementById("linkcv").href = 'javascript:void(0);';
//             document.getElementById("xx").innerHTML = 'Chưa có CV';
//             //var loai = location.search.split('loai=')[1];
//             $('#tt').tree('reload');
//             tinyMCE.activeEditor.setContent('');
//             url = baseUrl + '/baocaocall/themungvien?id=' + row.id;
//         }
//     }
// });


// function zero(val, row) {
//     if (val > 0)
//         return val;
//     else
//         return '';
// }


function del() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa các bản ghi đã chọn?', function (r) {
            if (r) {
                $.post(baseUrl + '/demo/del', {
                    id: row.id
                }, function (result) {
                    if (result.success) {
                        $('#dg').datagrid('reload');
                        show_messager(result.msg);
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}


function baocao() {
    $('#dlg-baocao').dialog({
        title: '&nbsp;Báo cáo telesale',
        iconCls: 'icon-baocao',
        closed: false
    });
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    $('#dg-baocao').datagrid('load', 'demo/baocao?tungay=' + tungay + '&denngay=' + denngay);
}

function getback() {
    $('#dlg-ungvien').dialog('close');
    $('#dlg').dialog('open');
    $('#tt').tree('reload');
}

function saveuv() {
    var nodes = $('#tt').tree('getChecked');
    var str = '';
    for (var i = 0; i < nodes.length; i++) {
        if (str != '') str += ',';
        str += nodes[i].id;
    }
    document.getElementById("danhmuc").value = str;
    // var fmuv = document.getElementById('fm-ungvien');
    // fmuv.action = url;
    // fmuv.submit();
    $('#fm-ungvien').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-ungvien').dialog('close');
                show_messager(result.msg);
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function chot() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg-chot').dialog({
            title: '&nbsp;Chốt lịch demo',
            iconCls: 'icon-baocao',
            closed: false
        });
        $('#fm-chot').form('clear');
        $("#buoi_chot").combobox({
            url: 'demo/buoichot?hocvien='+row.id,
            onSelect:function (rec) {
               $('#tinh_trang_chot').combobox({value:rec.tinh_trang});
            }
        });
        url='demo/chot?hocvien='+row.id;
    }else{
        show_messager('Không có bản ghi nào được chọn');
    }
}
function savechot() {
    $('#fm-chot').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-chot').dialog('close');
                $('#dg').datagrid('reload');
                show_messager(result.msg);
            } else {
                show_messager(result.msg);
            }
        }
    });
}

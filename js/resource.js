function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm mới',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm').form('clear');
    $("#abc").show()
    url = baseUrl + '/resource/add';
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Sửa thông tin',
            iconCls: 'icon-edit',
            closed: false
        });
				$('#fm').form('clear');
        $('#fm').form('load', row);
        $("#abc").hide()
        // if(row.nhan_vien!='')  {
        //     var temp = row.nhan_vien.split(",");
        //     $('#nhanvien').combobox('setValue',temp);
        // }
        url = baseUrl + '/resource/update?id=' + row.id;
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function save() {
    $('#fm').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function del() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        if(row.nguoi_tao==manhanvien || manhanvien==1) {
            $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa bản ghi này?', function(r) {
                if (r) {
                    $.post(baseUrl + '/resource/del', {
                        id: row.id
                    }, function(result) {
                        if (result.success) {
                            show_messager(result.msg);
                            $('#dg').datagrid('reload');
                        } else {
                            show_messager(result.msg);
                        }
                    }, 'json');
                }
            });
        } else {
            show_messager('Bạn không phải chủ sở hữu tài nguyên này nên không được phép xóa');
        }
    } else {
        show_messager('Bạn cần chọn tài nguyên để xóa');
    }
}

function chiase(val, row) {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        if(row.nguoi_tao==manhanvien || manhanvien==1) {
            $('#dlg-chiase').dialog({
                title: '&nbsp;Chia sẻ tài nguyên',
                iconCls: 'icon-lock',
                closed: false
            });
            $('#fm-chiase').form('clear');
            $('#nhanvien').combobox({value:row.nhan_vien});
            $('#nguoi_tao').combobox({value:row.nguoi_tao});
            url = baseUrl + '/resource/chiase?id=' + row.id;
        } else {
            show_messager('Bạn không có quyền chia sẻ tài nguyên này!');
        }
    } else
        show_messager('Bạn chưa chọn tài nguyên');
}

function capnhat() {
    $('#fm-chiase').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-chiase').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}


function matkhau(id) {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg-mk').dialog({
            title: '&nbsp;Xem cập nhập mật khẩu',
            iconCls: 'icon-lock',
            closed: false
        });
        $('#fm-mk').form('clear');
        $.post(baseUrl + '/resource/copypass', {id: row.id}, function(result) {
            var data = JSON.parse(result);
            $('#ten_dang_nhap').textbox('setValue',data.ten_dang_nhap);
            $('#mat_khau').passwordbox('setValue',data.mat_khau);
            $('#mat_khau').passwordbox('hidePassword');
        });
        url = baseUrl + '/resource/updatepass?id=' + row.id;
    } else
        show_messager('Bạn chưa chọn tài nguyên');
}

function updatepass() {
    $('#fm-mk').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-mk').dialog('close');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function timkiem() {
    var tukhoa = $('#tukhoa').textbox('getValue');
    var phanloai = $('#phanloai').combobox('getValue');
    var sohuu = $('#sohuu').combobox('getValue');
    var nhacungcap = $('#nhacungcap').combobox('getValue');
    if (tukhoa.length > 0 || phanloai.length > 0 || sohuu.length > 0 || nhacungcap.length > 0) {
        $('#dg').datagrid('options').url = baseUrl + '/resource/json?tukhoa=' + tukhoa + '&sohuu=' + sohuu + '&phanloai=' + phanloai+ '&nhacungcap=' + nhacungcap;
    } else {
        $('#dg').datagrid('options').url = baseUrl + '/resource/json';
    }
    $('#dg').datagrid('reload');
}

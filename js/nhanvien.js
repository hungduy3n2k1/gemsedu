$(function() {
    $('#tukhoa').textbox({
        onChange: function(rec) {
            timkiem();
        }
    });
});

function gioitinh(val,row) {
    if(val==1)
        return 'Nam';
    else
        return 'Nữ';
}

function timkiem() {
    var tukhoa = $('#tukhoa').textbox('getValue');
    var nam = $('#chonnam').combobox('getValue');
    var thang = $('#chonthang').combobox('getValue');
    var tinhtrang = $('#tinhtrang').combobox('getValue');
    if (tukhoa.length>0 || thang.length>0 || nam.length>0 || tinhtrang.length>0)
        $('#dg').datagrid('options').url = baseUrl + '/nhanvien/json?tukhoa=' + tukhoa+'&thang='+thang+'&nam='+nam+'&tinhtrang='+tinhtrang;
    else
        $('#dg').datagrid('options').url = baseUrl + '/nhanvien/json';
    $('#dg').datagrid('reload');
}

function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm nhân sự mới',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm').form('clear');
    // $('#phong_ban').combobox('setValue',1);
    // $('#gioi_tinh').combobox('setValue',1);
    // $('#hon_nhan').combobox('setValue',1);
    // $('#trinh_do').combobox('setValue',1);
    // $('#tinh_trang').combobox('setValue',1);
    // tinyMCE.activeEditor.setContent('');
    url = baseUrl + '/nhanvien/add';
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Xem và cập nhật thông tin nhân sự',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('load', row);
        url = baseUrl + '/nhanvien/update?id=' + row.id;
        // tinyMCE.activeEditor.setContent(row.ghi_chu);
        // tinyMCE.get('ghi_chu').setContent(row.kinh_nghiem);
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function save() {
    $('#fm').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function del() {
  var row = $('#dg').treegrid('getSelected');
  if (row){
      $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
          if (r){
              $.post(baseUrl+'/nhanvien/del',{id:row.id},function(result){
                  if (result.success){
                      $('#dg').datagrid('reload');
                  } else {
                      show_messager(result.msg);
                  }
              },'json');
          }
      });
  }else{
      show_messager('Không có bản ghi nào được chọn');
  }
}

function hopdong() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg-hopdong').dialog({
            title: '&nbsp;Hợp đồng lao động',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm-hopdong').form('load', row);
        url = baseUrl + '/nhanvien/hopdong?id=' + row.id;
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function savehopdong() {
    $('#fm-hopdong').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-hopdong').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

$(function () {
    $('#loai').combobox({
        onSelect: function (row) {
            if (row.id == 0)
                $("#khach_hang").combobox({url: 'common/khachhang'});
            else
                $("#khach_hang").combobox({url: 'common/nhacungcap'});
        }
    });
    $('#taikhoan').combobox({
        onLoadSuccess: function (items) {
            if (items.length) {
                var opts = $(this).combobox('options');
                $(this).combobox('select', items[0][opts.valueField]);
            }
        }
    });
    $('#invoice').combobox({
        formatter: function (row) {
            return "IV-" + row.id + '&nbsp;(<span style="font-weight:bold;">'+Comma(row.sotien)+')</span><br><span style="color:red">' + row.noi_dung + '</span>';
            //return '<span id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
        }
    });
});

function timkiem() {
    var ngaybd = $('#ngaybd').datebox('getValue');
    var ngaykt = $('#ngaykt').datebox('getValue');
    var tukhoa = $('#tukhoa').textbox('getValue');
    var taikhoan = $('#taikhoan').combobox('getValue');
    // var dgtitle = 'Thu chi '+$('#taikhoan').combobox('getText');
    // show_messager('thuchi/json?ngaybd=' + ngaybd + '&ngaykt=' + ngaykt + '&tukhoa=' + tukhoa + '&taikhoan=' + taikhoan);
    if (ngaybd.length > 0 || ngaykt.length > 0 || tukhoa.length > 0 || taikhoan.length > 0)
        $('#dg').datagrid('options').url = 'thuchi/json?ngaybd=' + ngaybd + '&ngaykt=' + ngaykt + '&tukhoa=' + tukhoa + '&taikhoan=' + taikhoan;
    else
        $('#dg').datagrid('options').url = 'thuchi/json';
    if ($('#dlg-search').dialog)
        $('#dlg-search').dialog('close');
    $('#dg').datagrid('reload');
    // var p = $('#dg').datagrid('getPanel');  // get the panel object
    // p.panel('setTitle',dgtitle);
}

function add() {
    $("#dlg").dialog({
        title: "&nbsp;Lập phiếu",
        iconCls: "icon-add",
        closed: false,
    });
    $('#khach_hang').combobox({
        onSelect: function (row) {
            $("#invoice").combobox({url: 'thuchi/invoice?khachhang='+row.id});
            $("#invoice").combobox('reload');
        }
    });
    $("#invoice").combobox({ value:''});
    $("#fm").form("clear");
    $("#loai").combobox({readonly: false, required: true});
    $("#khach_hang").combobox({readonly: false, required: true});
    $("#tai_khoan").combobox({readonly: false, required: true});
    $("#invoice").combobox({readonly: false, required: false});
    $("#ngaygio").datebox("setValue", homnay);
    $('#tinhtranginv2').radiobutton({
        checked: true
    });
    $('#nhan_vien_sale').combobox({value:5});
    url = "thuchi/add";
}

function edit() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        $("#dlg").dialog("open").dialog("setTitle", "Sửa phiếu");
        $("#fm").form("clear");
        $("#loai").combobox({readonly: true, required: false});
        $("#invoice").combobox({readonly: false, required: false});
        if (row.loai == 0)
            $("#khach_hang").combobox({url: 'common/khachhang', readonly: true, required: false});
        else
            $("#khach_hang").combobox({url: 'common/nhacungcap', readonly: true, required: false});
        $("#tai_khoan").combobox({readonly: true, required: false});
        $("#fm").form("load", row);
        $('#khach_hang').combobox({
            onSelect: function (rec) {
                $("#invoice").combobox({url: 'thuchi/invoice?khachhang='+rec.id});
                $("#invoice").combobox('reload');
            }
        });
        $("#khach_hang").combobox('setValue', row.khach_hang);
        $("#invoice").combobox({ value:row.invoice});
        $("#nhan_vien_sale").combobox({ value:row.nhan_vien_sale});
        if(row.tinhtranginv==3) {
            $('#tinhtranginv1').radiobutton({
                checked: true
            });
        }else{
            $('#tinhtranginv2').radiobutton({
                checked: true
            });
        }
        url = "thuchi/update?id=" + row.id;
    } else {
        show_messager("Không có bản ghi nào được chọn");
    }
}

function save() {
    $("#fm").form("submit", {
        url: url,
        onSubmit: function () {
            return $(this).form("validate");
        },
        success: function (result) {
            var result = eval("(" + result + ")");
            if (result.success) {
                $("#dlg").dialog("close");
                show_messager(result.msg);
                $("#dg").datagrid("reload");
            } else {
                show_messager(result.msg);
            }
        },
    });
    // document.getElementById('fm').action=url;
    // document.getElementById('fm').submit();
}

function del() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        $.messager.confirm("Thông báo", "Bạn có chắc chắn muốn xóa bản ghi này?", function (r) {
            if (r) {
                $.post("thuchi/del", {id: row.id,},
                    function (result) {
                        if (result.success) {
                            show_messager(result.msg);
                            $("#dg").datagrid("reload");
                        } else {
                            show_messager(result.msg);
                        }
                    }, "json");
            }
        });
    } else {
        show_messager("Không có bản ghi nào được chọn");
    }
}


function sodu() {
    $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn cập nhật lại số dư?', function (r) {
        if (r) {
            var ngay = $('#ngaybd').datebox('getValue');
            var taikhoan = $('#taikhoan').combobox('getValue');
            $.post('thuchi/sodu', {ngay: ngay, taikhoan: taikhoan},
                function (result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
        }
    });
}

// set default select the first item of all combobox
// $.fn.combobox.defaults.onLoadSuccess = function(items){
// 	if (items.length){
// 		var opts = $(this).combobox('options');
// 		$(this).combobox('select', items[0][opts.valueField]);
// 	}
// }

function tinhtrang(val,row) {
    if (val==0)
        return ('Chờ duyệt');
    else if (val==1)
        return ('Đã duyệt');
    else
        return ('Không duyệt');
    return '0';
}

function champhep(){
		$('#dlg-phep').dialog({
			title: '&nbsp;Nghỉ phép, công tác, nghỉ bù',
			iconCls:'icon-phieuthu',
			closed:false
		});
		$('#fm-phep').form('clear');
    $('#nhanvien').combobox({value:manhanvien});
}

function savephep(){
    var ngay = convert_date($('#ngay').datebox('getValue'));
    var date = [today.getFullYear(),('0' + (today.getMonth() + 1)).slice(-2),('0' + today.getDate()).slice(-2)].join('-');
    if (ngay>=date) {
        $('#fm-phep').form('submit',{
            onSubmit: function(){
                return $(this).form('validate');
            },
            success: function(result){
                var result = eval('('+result+')');
                if (result.success){
        						show_messager(result.msg);
                    $('#dlg-phep').dialog('close');
        						$('#dg').datagrid('reload');
                } else {
                    show_messager(result.msg);
                }
            }
        });
    } else
        show_messager('Ngày xin nghỉ phải sau ngày hôm nay');
}

function duyet() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Duyệt ngày nghỉ ',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('clear');
        $('#fm').form('load', row);
        document.getElementById("id").value=row.id;
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function save() {
    $('#fm').form('submit', {
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function nghile() {
    $('#dlg-le').dialog({
        title: '&nbsp; Khai báo nghỉ lễ',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-le').form('clear');
}

function savele() {
    $('#fm-le').form('submit', {
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-le').dialog('close');
                show_messager(result.msg);
            } else {
                show_messager(result.msg);
            }
        }
    });
}

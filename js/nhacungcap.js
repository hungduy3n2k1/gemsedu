$(function() {
    // $('#dg').datagrid({
    //     view: detailview,
    //     detailFormatter: function(index, row) {
    //         return '<div id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
    //     },
    //     onExpandRow: function(index, row) {
    //         $('#ddv-' + index).panel({
    //             border: false,
    //             cache: false,
    //             href: baseUrl + '/nhacungcap/detail?id=' + row.id,
    //             onLoad: function() {
    //                 $('#dg').datagrid('fixDetailRowHeight', index);
    //             }
    //         });
    //     }
    // });
});

function timkiem() {
    // var tinhtrang = $('#tinhtrang').combobox('getValue');
    var tukhoa = $('#tukhoa').textbox('getValue');
    // var nhanvien = $('#nhanvien').combobox('getValue');
    if (tukhoa.length>0)
        $('#dg').datagrid('options').url = baseUrl + '/nhacungcap/json?tukhoa='+tukhoa;
    else
        $('#dg').datagrid('options').url = baseUrl + '/nhacungcap/json';
    $('#dg').datagrid('reload');
}

function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm khách hàng mới',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm').form('reset');
    $('#loai').combobox({value:0});
    $('#dg-lienhe').datagrid({url:'nhacungcap/lienhe?id=null',toolbar: [{text:'Danh sách contact '}]});
    $('#dg-lienhe').datagrid('reload');
    url = baseUrl + '/nhacungcap/add';
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Thông tin nhà cung cấp',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('clear');
        $('#fm').form('load', row);
        url = baseUrl + '/nhacungcap/update?id=' + row.id;
        $('#dg-lienhe').datagrid({url:'nhacungcap/lienhe?id='+row.id,
            toolbar: [{
                text:'Danh sách contact '
            },'-',{
                text:'Thêm',
                iconCls:'icon-add',
                handler:function(){contactadd()}
            },{
                text:'Sửa',
                iconCls:'icon-edit',
                handler:function(){contactedit()}
            },{
                text:'Xóa',
                iconCls:'icon-no',
                handler:function(){contactdel()}
            }]
        });
        $('#dg-lienhe').datagrid('reload');
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function save() {
    // var data = $('#dg-lienhe').datagrid('getRows');
    // document.getElementById('contact').value=JSON.stringify(data);
    $('#fm').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function del(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
            if (r){
                $.post(baseUrl+'/nhacungcap/del',{id:row.id},function(result){
				    if (result.success){
				        $('#dg').datagrid('reload');
				    } else {
						    show_messager(result.msg);
				    }
				},'json');
            }
        });
    }else{
		show_messager('Không có bản ghi nào được chọn');
    }
}

//--------------- xử lý contacts
function contactadd() {
      $('#dlg-lienhe').dialog({
          title: '&nbsp;Thêm contact',
          iconCls: 'icon-edit',
          closed: false
      });
      $('#fm-lienhe').form('clear');
      var row = $('#dg').datagrid('getSelected');
      url2 = baseUrl + '/nhacungcap/addcts?nhacungcap='+row.id;
}

function contactedit() {
    var row = $('#dg-lienhe').datagrid('getSelected');
    if (row) {
        $('#dlg-lienhe').dialog({
            title: '&nbsp;Cập nhật contact',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm-lienhe').form('clear');
        $('#fm-lienhe').form('load', row);
        url2 = baseUrl + '/nhacungcap/savects?id='+row.id;
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function savects() {
    $('#fm-lienhe').form('submit', {
        url: url2,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-lienhe').dialog('close');
                $('#dg-lienhe').datagrid('clearSelections');
                $('#dg-lienhe').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function contactdel(){
    var row = $('#dg-lienhe').datagrid('getSelected');
    if (row){
        $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
            if (r){
                $.post(baseUrl+'/nhacungcap/delcts',{id:row.id},function(result){
				    if (result.success){
				        $('#dg-lienhe').datagrid('reload');
				    } else {
						    show_messager(result.msg);
				    }
				},'json');
            }
        });
    }else{
		show_messager('Không có bản ghi nào được chọn');
    }
}

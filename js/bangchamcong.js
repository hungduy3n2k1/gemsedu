$(function(){
     var thang = $('#thang').combobox('getValue');
     var nam = $('#nam').combobox('getValue');
     var column =  [{field:'giaovien',title:'Giáo viên',width:200
                       // formatter:function(value,row){
                       //    return value+' ('+row.tong+'/'+row.ngaycong+')';
                       // }
                   }];
     for (var i = 1; i <= 31; i++) {
        if (i<10) {i='0'+i;}
        var dt = new Date(nam+'-'+thang+'-'+i);
        if (dt.getDay()==0) {
            column.push({field:'ngay_'+i,title:'CN',width:60,align:'center'});
        }else if (dt.getDay()==1) {
             column.push({field:'ngay_'+i,title:'Thứ 2<br>'+i,width:60,align:'center'});
        }else if (dt.getDay()==2) {
            column.push({field:'ngay_'+i,title:'Thứ 3<br>'+i,width:60,align:'center'});
        }else if (dt.getDay()==3) {
            column.push({field:'ngay_'+i,title:'Thứ 4<br>'+i,width:60,align:'center'});
        }else if (dt.getDay()==4) {
             column.push({field:'ngay_'+i,title:'Thứ 5<br>'+i,width:60,align:'center'});
        }else if (dt.getDay()==5) {
            column.push({field:'ngay_'+i,title:'Thứ 6<br>'+i,width:60,align:'center'});
        }else {
             column.push({field:'ngay_'+i,title:'Thứ 7<br>'+i,width:60,align:'center'});
        }
    }
    $('#dg').datagrid({
        columns:[column]
    });
});

function search() {
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    var column =  [{field:'giaovien',title:'Giáo viên',width:200}
       // {field:'ngay_cong',title:'Ngày công',width:80,align:'center'},
       // {field:'cong_chuan',title:'Công chuẩn',width:80,align:'center'}
     ];
    for (var i = 1; i <= 31; i++) {
       if (i<10) {i='0'+i;}
       var dt = new Date(nam+'-'+thang+'-'+i);
       if (dt.getDay()==0) {
           column.push({field:'ngay_'+i,title:'CN',width:60,align:'center'});
       }else if (dt.getDay()==1) {
            column.push({field:'ngay_'+i,title:'Thứ 2<br>'+i,width:60,align:'center'});
       }else if (dt.getDay()==2) {
           column.push({field:'ngay_'+i,title:'Thứ 3<br>'+i,width:60,align:'center'});
       }else if (dt.getDay()==3) {
           column.push({field:'ngay_'+i,title:'Thứ 4<br>'+i,width:60,align:'center'});
       }else if (dt.getDay()==4) {
            column.push({field:'ngay_'+i,title:'Thứ 5<br>'+i,width:60,align:'center'});
       }else if (dt.getDay()==5) {
           column.push({field:'ngay_'+i,title:'Thứ 6<br>'+i,width:60,align:'center'});
       }else {
            column.push({field:'ngay_'+i,title:'Thứ 7<br>'+i,width:60,align:'center'});
       }
   }
   $('#dg').datagrid({
       columns:[column]
   });
   $('#dg').datagrid('options').url = 'bangchamconggv/json?thang='+thang+'&nam='+nam;
   $('#dg').datagrid('reload');
}

function add() {
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    $.post('bangchamcong/add', {thang: thang,nam:nam}, function(result) {
        if (result.success) {
            // show_messager(result.msg);
            $('#dg').datagrid('options').url = 'bangchamcong/json?thang='+thang+'&nam='+nam;
            $('#dg').datagrid('reload');
        } else {
            show_messager(result.msg);
        }
    }, 'json');
}

// function chamcong() {
//     var thang = $('#thang').combobox('getValue');
//     var nam = $('#nam').combobox('getValue');
//     $.post('bangchamcong/chamcong', {thang: thang,nam:nam}, function(result) {
//         if (result.success) {
//             // show_messager(result.msg);
//             $('#dg').datagrid('options').url = 'bangchamcong/json?thang='+thang+'&nam='+nam;
//             $('#dg').datagrid('reload');
//         } else {
//             show_messager(result.msg);
//         }
//     }, 'json');
// }

$(function () {
    
        $('#dg').datagrid({
            onDblClickRow: function (index, row) {
                editST(row);
            }
        }); 

        $('#close-edit').on('click', function (){
            $('#dlg-edit').dialog('close')
        })
   

    $('#khach_hang').combobox({
        onSelect: function (rec) {
            $('#hoc_vien').combobox('reload', 'donhang/hocvien?khach_hang=' + rec.id);
            $('#sdt').textbox({value: rec.dien_thoai});
        }
    });
    
    $('#so_tien').numberbox({
        onChange: function (val) {
            for (var k = 1; k < 7; k++) {
                $('#sotiendot' + k).numberbox({value: ''});
                $('#ngaydot' + k).datebox({value: ''});
            }
            var dotthanhtoan = $('#dot_thanh_toan').combobox('getValue');
            var ngaydangky = $('#ngay_dang_ky').datebox('getValue');
            if (dotthanhtoan == 1) {
                $('#sotiendot1').numberbox({value: val});
                $('#ngaydot1').datebox({value: ngaydangky});
            } else {
                $.post(baseUrl + '/data/chiadot', {
                    sotien: val,
                    ngaydangky: ngaydangky,
                    sodot: dotthanhtoan
                }, function (result) {
                    if (result.success) {
                        for (var i = 1; i <= dotthanhtoan; i++) {
                            $('#sotiendot' + i).numberbox({value: result.data[i].sotiendot});
                            $('#ngaydot' + i).datebox({value: result.data[i].ngaydot});
                        }
                    } 
                }, 'json');
            }
        }
    });
    $('#dot_thanh_toan').combobox({
        onSelect: function (rec) {
            for (var k = 1; k < 7; k++) {
                $('#sotiendot' + k).numberbox({value: ''});
                $('#ngaydot' + k).datebox({value: ''});
            }
            var sotien = $('#so_tien').numberbox('getValue');
            var ngaydangky = $('#ngay_dang_ky').datebox('getValue');
            if (rec.id == 1) {
                $('#sotiendot1').numberbox({value: sotien});
                $('#ngaydot1').datebox({value: ngaydangky});
            } else {
                $.post(baseUrl + '/data/chiadot', {
                    sotien: sotien,
                    ngaydangky: ngaydangky,
                    sodot: rec.id
                }, function (result) {
                    if (result.success) {
                        for (var i = 1; i <= rec.id; i++) {
                            $('#sotiendot' + i).numberbox({value: result.data[i].sotiendot});
                            $('#ngaydot' + i).datebox({value: result.data[i].ngaydot});
                        }
                    }
                }, 'json');
            }    
        }
    });
    
    $('#sotiendot1').numberbox({
        onChange: function (val) {
            var dot = $('#dot_thanh_toan').combobox('getValue');
            var sotien = $('#so_tien').numberbox('getValue');
            if (dot > 1) {
                var conlai = (sotien.replaceAll(',', '') - val) / (dot - 1);
                for (var m = 2; m <= dot; m++) {
                    $('#sotiendot' + m).numberbox({value: conlai});
                }
            }
        }
    });
    
    // $('#dg').datagrid({
    //     view: detailview,
    //     detailFormatter: function(index, row) {
    //         return '<div id="ddv-' + index + '" style="padding:5px 0;height:120px;width:100%; line-height:20px"></div>';
    //     },
    //     onExpandRow: function(index, row) {
    //         $('#ddv-' + index).panel({
    //             border: false,
    //             cache: false,
    //             href: baseUrl + '/donhang/detail?id=' + row.khach_hang,
    //             onLoad: function() {
    //                 $('#dg').datagrid('fixDetailRowHeight', index);
    //             }
    //         });
    //     }
    // });
});

function tinhtrang(val, row) {
    if (val == 1)
        return 'Mới';
    else if (val == 2)
        return 'Đang thực hiện';
    else if (val == 3)
        return 'Đã hoàn thành';
    else if (val == 4)
        return 'Tạm dừng';
    else if (val == 5)
        return 'Hủy';
    else
        return '';
}

function timkiem() {
    var tinhtrang = $('#tinhtrang').combobox('getValue');
    var khachhang = $('#khachhang').combobox('getValue');
    var hocvien = $('#hocvien').combobox('getValue');
    var loaidh = $('#loaidh').combobox('getValue');
    if (tinhtrang.length > 0 || khachhang.length > 0 || hocvien.length>0 || loaidh.length>0) {
        $('#dg').datagrid('options').url = baseUrl + '/donhang/json?tinhtrang=' + tinhtrang + '&khachhang=' + khachhang+'&hocvien='+hocvien+'&loaidh='+loaidh;
    } else {
        $('#dg').datagrid('options').url = baseUrl + '/donhang/json';
    }
    // if($('#dlg-search').dialog)
    //     $('#dlg-search').dialog('close');
    $('#dg').datagrid('reload');
}

function add() {
    $('#btnSave').linkbutton('enable');
    $('#dlg').dialog({
        title: '&nbsp;Thêm đơn hàng',
        iconCls: 'icon-add',
        closed: false
    });
    $('#product').combobox({
        onSelect: function (row) {
            $.post(baseUrl + '/donhang/sobuoi', {product: row.id}, function (result) {
                if (result.success) {
                    $('#so_buoi').numberbox('setValue', result.sobuoi);
                    // $('#so_tien').numberbox('setValue', result.sotien);
                } 
            }, 'json');
        }
    });
    $('#khach_hang').combobox({readonly: false});
    $('#hoc_vien').combobox({readonly: false});
    $('#fm').form('clear');
    $('#ngay_dang_ky').datebox({value: homnay});
    
    for (var k = 1; k < 7; k++) {
        $('#sotiendot' + k).numberbox({readonly: false});
        $('#ngaydot' + k).datebox({readonly: false});
    }
    $('#so_tien').numberbox({readonly: false});
   
    url = baseUrl + '/donhang/add';
}

function edit() {
    $('#btnSave').linkbutton('enable');
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Sửa thông tin',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('load', row);
        if (row.tinh_trang != 1) {
            for (var k = 1; k < 7; k++) {
                $('#sotiendot' + k).numberbox({readonly: true});
                $('#ngaydot' + k).datebox({readonly: true});
            }
            $('#so_tien').numberbox({readonly: true});
            $('#dot_thanh_toan').combobox({readonly: true});
            $('#product').combobox({readonly: true});
            
        } else {
            for (var k = 1; k < 7; k++) {
                $('#sotiendot' + k).numberbox({readonly: false});
                $('#ngaydot' + k).datebox({readonly: false});
            }
            $('#so_tien').numberbox({readonly: false});
            $('#dot_thanh_toan').combobox({readonly: false});
            $('#product').combobox({readonly: false});
            $('#product').combobox({
                onChange: function (val) {
                    $.post(baseUrl + '/donhang/sobuoi', {product: val}, function (result) {
                        if (result.success) {
                            $('#so_buoi').numberbox('setValue', result.sobuoi);
                            // $('#so_tien').numberbox('setValue', result.sotien);
                        } 
                    }, 'json');
                }
            });
        }
        // $('#loaikh1').combobox({value: row.loaikh});
        $('#product').combobox({value: row.product});
        $('#khach_hang').combobox({readonly: true, value: row.khach_hang});
        $('#hoc_vien').combobox({readonly: true, value: row.hoc_vien});
        $('#ngay_dang_ky').datebox({value: row.ngay});
        $('#dot_thanh_toan').combobox({value: row.dot_thanh_toan});
        for (var k = 1; k < 7; k++) {
            $('#sotiendot' + k).numberbox({value: ''});
            $('#ngaydot' + k).datebox({value: ''});
        } 
        $.post(baseUrl + '/donhang/dotthanhtoan', {
            donhang: row.id
        }, function (result) {
            // var result = JSON.parse(result);
            for (var k = 1; k < 7; k++) {
                $('#sotiendot' + k).numberbox({value: result[k-1].so_tien});
                $('#ngaydot' + k).datebox({value: result[k-1].ngaythanhtoan});
            }
        }, 'json');
        
        url = baseUrl + '/donhang/update?id=' + row.id;
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function editST(row) {
    $('#dlg-edit').dialog({
        title: '&nbsp;Cập nhật',
        iconCls: 'icon-edit',
        closed: false
    });
    $('#fm-edit').form('clear');
    $('#fm-edit').form('load', row);
    $('#sotien').textbox({value: row.so_tien});
    // url = baseUrl + '/donhang/updateST?id=' + row.id;
}

function saveEditST() {
    $('#btnSave').linkbutton('enable');
    $('#fm-edit').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            
            $('#btnSave').linkbutton('disable');
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-edit').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function save() {
    $('#btnSave').linkbutton('enable');
    $('#fm').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            
            $('#btnSave').linkbutton('disable');
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function xuatfile() {

    var tinhtrang = $('#tinhtrang').combobox('getValue');
    var tukhoa = $('#tukhoa').textbox('getValue');
    var sohuu = $('#sohuu').combobox('getValue');
    var dichvu = $('#dichvu').combobox('getValue');
    var tenmien = $('#tukhoa').textbox('getValue');
    window.location.href = baseUrl + '/donhang/xuatfile?tinhtrang=' + tinhtrang + '&tukhoa=' + tukhoa +
        '&sohuu=' + sohuu + '&dichvu=' + dichvu + '&tenmien=' + tenmien;
}

function del() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa?', function (r) {
            if (r) {
                $.post(baseUrl + '/donhang/del', {
                    id: row.id
                }, function (result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}


function nhap() {
    $('#dlg-nhap').dialog({
        title: '&nbsp;Nhập đơn hàng từ file exccel',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-nhap').form('clear');
}

function nhapexel() {
    // document.getElementById('fm-nhap').submit();
    $('#fm-nhap').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-nhap').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

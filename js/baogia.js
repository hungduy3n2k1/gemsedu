$(function() {
    $('#dg-add').datagrid('enableCellEditing');
    $('#dg-add').datagrid({
        onEndEdit: function(index,row){
            row.thanhtien = Math.round((row.dongia-row.chietkhau)*(1-row.chietkhaupt/100)*(1+row.thuevat/100)*row.soluong);
            var rows = $('#dg-add').datagrid('getRows');
            var sum = 0;
            for (i = 0; i < rows.length; i++)
                sum += parseInt(rows[i].thanhtien);
            $('#dg-add').datagrid('reloadFooter', [{hanghoa: 'Tổng cộng:',dongia:0,chietkhau:0,thanhtien: sum}]);
        }
    });
    $('#dich_vu').combobox({
        onSelect: function(row) {
            $('#don_gia').numberbox('setValue',row.don_gia);
            $('#don_vi').combobox('setValue',row.don_vi_tinh);
            $('#thue_suat_vat').numberbox('setValue',row.thue_suat_vat);
        }
    });
    $('#dg').datagrid({
        view: detailview,
        detailFormatter: function(index, row) {
            return '<div id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
        },
        onExpandRow: function(index, row) {
            $('#ddv-' + index).panel({
                border: false,
                cache: false,
                href: 'baogia/detail?id=' + row.id,
                onLoad: function() {
                    $('#dg').datagrid('fixDetailRowHeight', index);
                }
            });
        }
    });
});

function tinhtrang(val,row) {
    if(val==2)
        return '<span style="text-decoration:underline; color:orange;">Đã gửi đi</span>';
    else if(val==3)
        return '<span style="text-decoration:underline; color:blue;">Đã chốt</span>';
    else if(val==4)
        return '<span style="text-decoration:underline; color:red;">Hủy</span>';
}
// lập báo giá
function add() {
    $('#dlg-add').dialog({
              title: '&nbsp;Lập báo giá mới',
              iconCls: 'icon-add',
              closed: false
    });
    $('#fm-add').form('clear');
    $('#ngaybao').datebox('setValue',homnay);
    $('#tinhtrang').combobox('setValue',1);
    $('#dg-add').datagrid('loadData', []);
}

function addrow() { // Mở form thêm dịch vụ
      $('#dlg-row').dialog({
          title: '&nbsp;Thêm sản phẩm/dịch vụ',
          iconCls: 'icon-add',
          closed: false,
          buttons: [{
              text:'OK',
              iconCls:'icon-ok',
              handler:function(){saverow();}
          }]
      });
      $('#fm-row').form('clear');
      $('#tu_ngay').datebox('setValue',homnay);
      $('#den_ngay').datebox('setValue',homnay);
}

function saverow() { // thêm dịch vụ vào báo giá
    $('#fm-row').form('submit', {
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var hanghoaid = $('#dich_vu').combobox('getValue');
            var hanghoa = $('#dich_vu').combobox('getText');
            var dongia = $('#don_gia').numberbox('getValue');
            var chietkhau = $('#chiet_khau_tm').numberbox('getValue');
            var chietkhaupt = $('#chiet_khau_pt').numberbox('getValue');
            var thuevat = $('#thue_suat_vat').numberbox('getValue');
            var tangthang = $('#tang_thang').numberbox('getValue');
            var soluong = $('#so_luong').numberbox('getValue');
            var donviid = $('#don_vi').combobox('getValue');
            var donvi = $('#don_vi').combobox('getText');
            var tungay = $('#tungay1').datebox('getValue');
            var denngay = $('#denngay1').datebox('getValue');
            var ghichu = $('#ghi_chu').textbox('getValue');
            var thanhtien = Math.round((dongia-chietkhau)*(1-chietkhaupt/100)*(1+thuevat/100)*soluong);
            var lastid = $('#dg-add').datagrid('getRows').length;
            $('#dg-add').datagrid('appendRow', {
                hanghoaid: hanghoaid,
                hanghoa: hanghoa,
                dongia: dongia,
                donviid: donviid,
                donvi: donvi,
                soluong: soluong,
                chietkhau:chietkhau,
                chietkhaupt:chietkhaupt,
                thanhtien:thanhtien,
                thuevat:thuevat,
                tangthang:tangthang,
                tungay:tungay,
                denngay:denngay,
                ghichu:ghichu,
                xoa:'<a href="javascript:void(0)" onclick="delrow('+lastid+')"><img src="public/easyui/icons/clear.png"></a>'
            });
            var rows = $('#dg-add').datagrid('getRows');
            var sum = 0;
            for (i = 0; i < rows.length; i++)
                sum += parseInt(rows[i].thanhtien);
            $('#dg-add').datagrid('reloadFooter', [{hanghoa: 'Tổng cộng:',dongia:0,chietkhau:0,thanhtien: sum}]);
            $('#dlg-row').dialog('close');
        }
    });
    
}

function delrow(index) { // xóa một dịch vụ trong báo giá
    $('#dg-add').datagrid('deleteRow', index);
    var data = $('#dg-add').datagrid('getRows');
    var sum = 0;
    data.forEach(function(row,index){
        row.xoa='<a href="javascript:void(0)" onclick="delrow('+index+')"><img src="public/easyui/icons/clear.png"></a>';
        sum+=parseInt(row.thanhtien);
    });
    $('#dg-add').datagrid('loadData', data);
    $('#dg-add').datagrid('reloadFooter', [{hanghoa: 'Tổng cộng:',dongia:0,chietkhau:0,thanhtien: sum}]);
}

function save() { // save báo giá mới
    var data = $('#dg-add').datagrid('getRows');
    data.forEach(function(row,index){row.xoa=''});
    document.getElementById('baogia').value=JSON.stringify(data);
    $('#fm-add').form('submit', {
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-add').dialog('close')
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}
// Xóa báo giá
function del() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        if(row.tinh_trang==2 || row.tinh_trang==3) {
            show_messager('Bạn không được phép xóa báo giá đã gửi hoặc đã chốt!');
        } else {
            $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa?', function(r) {
                if (r) {
                    $.post(baseUrl + '/baogia/del', {
                        id: row.id
                    }, function(result) {
                        if (result.success) {
                            show_messager(result.msg);
                            $('#dg').datagrid('reload');
                        } else {
                            show_messager(result.msg);
                        }
                    }, 'json');
                }
            });
        }
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

// Sửa - xem - in - gửi email báo giá
function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        if(row.tinh_trang==3) {
            show_messager('Bạn không được phép cập nhật báo giá đã chốt!');
        } else {
            $('#dlg-edit').dialog({
                title: '&nbsp;Sửa - xem - in báo giá',
                iconCls: 'icon-add',
                closed: false
            });
            $('#fm-edit').form('load',row);
            data = $('#dg-edit').datagrid('load','baogia/noidung?id='+row.id);
        }
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function delitem() {
    var row = $('#dg-edit').datagrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa bản ghi này?', function(r) {
            if (r) {
                $.post(baseUrl + '/baogia/delrow', {id: row.id},
                function(result) {
                    if (result.success) {
                        $('#dg-edit').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function edititem() {
    var row = $('#dg-edit').datagrid('getSelected');
    if (row) {
        $('#dlg-row').dialog({
            title: '&nbsp;Sửa sản phẩm trong phiếu',
            iconCls: 'icon-edit',
            closed: false,
            buttons: [{
                text:'Save',
                iconCls:'icon-save',
                handler:function(){saveitem();}
            }]
        });
        $('#fm-row').form('clear');
        $('#fm-row').form('load', row);
        url = 'baogia/saveitem?id='+row.id;
    } else
        show_messager('Bạn chưa chọn sản phẩm');
}

function additem() {
    $('#dlg-row').dialog({
        title: '&nbsp;Thêm sản phẩm vào phiếu',
        iconCls: 'icon-edit',
        closed: false,
        buttons: [{
            text:'Save',
            iconCls:'icon-save',
            handler:function(){saveitem();}
        }]
    });
    $('#fm-row').form('clear');
    var sophieu = document.getElementById('id').value;
    url = 'baogia/saveitem?sophieu='+sophieu;
}

function saveitem() {
    $('#fm-row').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dg-edit').datagrid('reload');
                $('#dlg-row').dialog('close');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function update() { // save báo giá sửa
    $('#fm-edit').form('submit', {
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-edit').dialog('close')
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function sendmail() {
    $('#dlg-sendmail').dialog({
        title: '&nbsp;Send mail',
        iconCls: 'icon-edit',
        closed: false
    });
    $('#fm-send').form('clear');
    var row = $('#dg').datagrid('getSelected');
    $('#emailtosend').combobox({url:'baogia/email?id='+row.khach_hang, valueField:'email', textField:'email'});
    urlx= baseUrl + '/baogia/sendmail?id='+row.id;
}

function sendmailsubmit() {
    $('#fm-send').form('submit', {
        url: urlx,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-sendmail').dialog('close');
                $('#dlg-edit').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function chot() {
    var row = $('#dg').datagrid('getSelected');
    $.messager.confirm('Thông báo', 'Lưu ý báo giá sau khi chốt sẽ không được phép xóa hoặc sửa!', function(r) {
        if (r) {
                $.post(baseUrl + '/baogia/chot', {
                    id: row.id
                }, function(result) {
                    if (result.success) {
                        $('#dlg-edit').dialog('close');
                        $('#dg').datagrid('reload');
                        show_messager(result.msg);
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
        }
    });
}

function timkiem() {
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    var phanloai = $('#phanloai').combobox('getValue');
    var doitac = $('#doitac').combobox('getValue');
    if (tungay.length > 0 || denngay.length > 0 || phanloai.length > 0 || doitac.length > 0 ) {
        $('#dg').datagrid('options').url = baseUrl + '/baogia/json?tungay=' + tungay + '&denngay=' + denngay+ '&phanloai=' + phanloai+ '&doitac=' + doitac;
    } else {
        $('#dg').datagrid('options').url = baseUrl + '/baogia/json'
    }
    $('#dg').datagrid('reload');
}

function format_canhbao(val, row) {
		var date = new Date(new Date().setDate(new Date().getDate() + 30));
		var exp = new Date(row.ngay_kt);
		if (date.getTime()>exp.getTime())
				return '<span style="color:red">' + val + "</span>";
		else
				return val;
}

function timkiem() {
    var tinhtrang = $("#tinhtrang").combobox("getValue");
    var dichvu = $("#dichvu").combobox("getValue");
    var ncc = $("#ncc").combobox("getValue");
    var tukhoa = $("#tukhoa").textbox("getValue");
    if (tinhtrang.length > 0 || tukhoa.length > 0) {
        $("#dg").datagrid("options").url = baseUrl + "/thuengoai/json?tinhtrang=" + tinhtrang + "&dichvu=" + dichvu + "&ncc=" + ncc + "&tukhoa=" + tukhoa;
        $("#dg").datagrid("reload");
    }
}

function xuatfile() {
    
    var tinhtrang = $("#tinhtrang").combobox("getValue");
    var dichvu = $("#dichvu").combobox("getValue");
    var ncc = $("#ncc").combobox("getValue");
    var tukhoa = $("#tukhoa").textbox("getValue");
        window.location.href = baseUrl + "/thuengoai/xuatfile?tinhtrang=" + tinhtrang + "&dichvu=" + dichvu + "&ncc=" + ncc + "&tukhoa=" + tukhoa;
    
}

function add() {
    $("#dlg").dialog({
        title: "&nbsp;Thêm dịch vụ",
        iconCls: "icon-add",
        closed: false,
    });
    $("#fm").form("clear");
		// $("#khach_hang").combobox({readonly: false, required:true});
		// $("#product").combobox({readonly: false, required:true});
		$("#tinh_trang").combobox('setValue',1);
    url = baseUrl + "/thuengoai/add";
}

function edit() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        $("#dlg").dialog({
            title: "&nbsp;Sửa thông tin",
            iconCls: "icon-edit",
            closed: false,
        });
				// $("#khach_hang").combobox({readonly: true, required:false});
				// $("#product").combobox({readonly: true, required:false});
				$("#fm").form("load", row);
        url = baseUrl + "/thuengoai/update?id=" + row.id;
    } else show_messager("Không có bản ghi nào được chọn");
}

function save() {
    $("#fm").form("submit", {
        url: url,
        onSubmit: function () {
            return $(this).form("validate");
        },
        success: function (result) {
            var result = eval("(" + result + ")");
            if (result.success) {
                show_messager(result.msg);
                $("#dlg").dialog("close");
                $("#dg").datagrid("reload");
                $("#dg").datagrid("clearSelections");
            } else {
                show_messager(result.msg);
            }
        },
    });
}

function giahan() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
				$("#dlg-giahan").dialog({
						title: "&nbsp;Gia hạn dịch vụ",
						iconCls: "icon-edit",
						closed: false,
				});
				$("#fm-giahan").form("clear");
				document.getElementById("id").value=row.id;
				document.getElementById("khachhang").value=row.khach_hang;
    } else {
        show_messager("Không có bản ghi nào được chọn");
    }
}

function tieptuc() {
    $("#fm-giahan").form("submit", {
        onSubmit: function () {
            return $(this).form("validate");
        },
        success: function (result) {
            var result = eval("(" + result + ")");
            if (result.success) {
                show_messager(result.msg);
                $("#dlg-giahan").dialog("close");
                $("#dg").datagrid("reload");
            } else {
                show_messager(result.msg);
            }
        },
    });
}

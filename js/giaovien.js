function add() {
	$('#dlg').dialog({
        title: '&nbsp;Thêm giáo viên',
		iconCls:'icon-add',
		closed:false
    });
    $('#fm').form('clear');
    url = baseUrl + '/giaovien/add';
}

function edit(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
		$('#dlg').dialog({
			title: '&nbsp;Sửa thông tin',
			iconCls:'icon-edit',
			closed:false
		});
        $('#fm').form('load',row);
        url = baseUrl+'/giaovien/update?id='+ row.id;
    }
	else
        show_messager('Không có bản ghi nào được chọn');
}

function save(){
    var name = $('#name').val();
    var loai_hinh = $('#loai_hinh').val();
    var bang_dai_hoc = $('#bang_dai_hoc').val();
    var phan_loai = $('#phan_loai').val();
    if(name == '' && loai_hinh == '' && bang_dai_hoc == '' && phan_loai == ''){
        $('#dlg-buttons').children('button')[0].disabled = true;
    }
    $('#fm').form('submit',{
       	url: url,
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');
				$('#dg').datagrid('reload');
                $('#dg').datagrid('clearSelections');
            } else {
                show_messager(result.msg);
            }
            $('#dlg-buttons').children('button')[0].disabled = false;
        }
    });
}


function del(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
            if (r){
                $.post(baseUrl+'/giaovien/del',{id:row.id},function(result){
				    if (result.success){
				        show_messager(result.msg);
				        $('#dg').datagrid('reload');
                        $('#dg').datagrid('clearSelections');
				    } else {
						show_messager(result.msg);
				    }
				},'json');
            }
        });
    }else{
		show_messager('Không có bản ghi nào được chọn');
    }
}

function nhap() {
    $('#dlg-nhap').dialog({
        title: '&nbsp;Nhập giáo viên từ file exccel',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-nhap').form('clear');
}

function nhapexel() {
    // document.getElementById('fm-nhap').submit();
    $('#fm-nhap').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-nhap').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function timkiem() {
    var tukhoa = $('#tukhoa').textbox('getValue');
    if (tukhoa.length>0) {
        $('#dg').datagrid('options').url = baseUrl + '/giaovien/json?tukhoa='+tukhoa;
    } else {
        $('#dg').datagrid('options').url = baseUrl + '/giaovien/json';
    }
    $('#dg').datagrid('reload');
}



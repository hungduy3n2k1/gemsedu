function celltop(value, row) {
    if (value)
        return '<div style="min-height:70px;line-height:12px">' + value + '</div>';
}

function format_hocvien(val, row) {
    if (row.loailop == 1)
        return val;
    else
        return '';
}

function timkiem() {
    var gio = $('#giohoc').timespinner('getValue');
    var ngay = $('#ngay1').textbox('getValue');
    var thang = $('#thang').textbox('getValue');
    var nam = $('#nam').textbox('getValue');
    var loai = $('#loai').textbox('getValue');
    var giaovien = $('#giaovien').textbox('getValue');
    var phonghoc = $('#phonghoc').textbox('getValue');
    var tenlop = $('#tenlop').textbox('getValue');
    if (thang.length > 0 || nam.length > 0 || gio.length > 0 || giaovien.length > 0 ||
        loai.length > 0 || phonghoc.length > 0 || tenlop.length > 0 || ngay.length > 0) {
        var curl = window.location.href;
        var res = curl.split("/");
        if (res[res.length - 1] == 'lichhoc')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/json?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonview?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonday?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsontime?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'duyetview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonduyet?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
    } else {
        var curl = window.location.href;
        var res = curl.split("/");
        if (res[res.length - 1] == 'lichhoc')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/json";
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonview";
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonday";
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsontime";
        else if (res[res.length - 1] == 'duyetview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonduyet";
    }
    $("#dg").datagrid("reload");
}

function pre() {
    var thang = $('#thang').textbox('getValue');
    var nam = $('#nam').textbox('getValue');
    var x = parseInt(thang) - 1;
    if (x > 0) {
        if (x < 10)
            thang = '0' + x;
        else
            thang = x;
        $('#thang').textbox('setValue', thang);
    } else {
        thang = '12';
        var y = parseInt(nam) - 1;
        $('#thang').textbox('setValue', thang);
        $('#nam').textbox('setValue', y);
    }
    var gio = $('#giohoc').timespinner('getValue');
    var ngay = $('#ngay1').textbox('getValue');
    var loai = $('#loai').textbox('getValue');
    var giaovien = $('#giaovien').textbox('getValue');
    var phonghoc = $('#phonghoc').textbox('getValue');
    var tenlop = $('#tenlop').textbox('getValue');
    var curl = window.location.href;
    var res = curl.split("/");
    if (thang.length > 0 || nam.length > 0 || gio.length > 0 || giaovien.length > 0 ||
        loai.length > 0 || phonghoc.length > 0 || tenlop.length > 0 || ngay.length > 0) {
        if (res[res.length - 1] == 'lichhoc')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/json?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonview?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonday?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsontime?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'duyetview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonduyet?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
    } else {
        if (res[res.length - 1] == 'lichhoc')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/json";
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonview";
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonday";
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsontime";
        else if (res[res.length - 1] == 'duyetview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonduyet";
    }
    $("#dg").datagrid("reload");
}

function next() {
    var thang = $('#thang').textbox('getValue');
    var nam = $('#nam').textbox('getValue');
    var x = parseInt(thang) + 1;
    if (x < 13) {
        if (x < 10)
            thang = '0' + x;
        else
            thang = x;
        $('#thang').textbox('setValue', thang);
    } else {
        thang = '01';
        var y = parseInt(nam) + 1;
        $('#thang').textbox('setValue', thang);
        $('#nam').textbox('setValue', y);
    }
    var curl = window.location.href;
    var res = curl.split("/");
    var gio = $('#giohoc').timespinner('getValue');
    var ngay = $('#ngay1').textbox('getValue');
    var loai = $('#loai').textbox('getValue');
    var giaovien = $('#giaovien').textbox('getValue');
    var phonghoc = $('#phonghoc').textbox('getValue');
    var tenlop = $('#tenlop').textbox('getValue');
    var curl = window.location.href;
    var res = curl.split("/");
    if (thang.length > 0 || nam.length > 0 || gio.length > 0 || giaovien.length > 0 ||
        loai.length > 0 || phonghoc.length > 0 || tenlop.length > 0 || ngay.length > 0) {
        if (res[res.length - 1] == 'lichhoc')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/json?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonview?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonday?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsontime?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
        else if (res[res.length - 1] == 'duyetview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonduyet?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
                "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop + '&ngay=' + ngay;
    } else {
        if (res[res.length - 1] == 'lichhoc')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/json";
        else if (res[res.length - 1] == 'view')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonview";
        else if (res[res.length - 1] == 'dayview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonday";
        else if (res[res.length - 1] == 'timeview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsontime";
        else if (res[res.length - 1] == 'duyetview')
            $("#dg").datagrid("options").url = baseUrl + "/lichhoc/jsonduyet";
    }
    $("#dg").datagrid("reload");
}


function add() {
    $('#dlg-add').dialog({
        title: '&nbsp;Thêm lịch học',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-add').form('clear');
    $('#lop_hoc1').combobox({
        onSelect: function (rec) {
            $('#giao_vien').combobox({value: rec.giao_vien});
            document.getElementById('sobuoi').value = rec.so_buoi;
            document.getElementById('buoikm').value = rec.buoi_khuyen_mai;
            document.getElementById('hocvien').value = rec.hoc_vien;
            document.getElementById('phanloai').value = rec.phan_loai;
        }
    });
    url = baseUrl + '/lichhoc/add';
}

function format_link(val, row) {
    if (val != '')
        return '<a href="' + val + '" target="_blank" style="color: blue;font-weight: bold;text-decoration: none;">View</a>';
    else
        return '';
}

function xemthem(ngay) {
    alert(ngay);
}

function edit(id) {
    document.getElementById('editmulti').checked = false;
    document.getElementById('hienthithu').style = 'display:table-row';
    document.getElementById('suatuongtu').style = 'display:table-row';
    if (id > 0) {
        $('#dlg').dialog({
            title: '&nbsp;Sửa thông tin',
            iconCls: 'icon-edit',
            closed: false
        });
        $.post(baseUrl + '/lichhoc/getrow', {id: id}, function (result) {
            if (result.success) {
                var row = result.data;
                $('#fm').form('load', row);
                url = baseUrl + '/lichhoc/update?id=' + row.id;
            } else {
                show_messager(result.msg);
            }
        }, 'json');
    } else {
        var row = $("#dg").datagrid("getSelected");
        if (row) {
            $('#dlg').dialog({
                title: '&nbsp;Sửa thông tin',
                iconCls: 'icon-edit',
                closed: false
            });
            $('#fm').form('load', row);
            url = baseUrl + '/lichhoc/update?id=' + row.id;
        } else {
            show_messager('Không có bản ghi nào được chọn');
        }
    }
}

function save() {
    // document.getElementById('fm').action=url;
    // document.getElementById('fm').submit();
    var thu = $('#thu').val();
    var ngay = $('#ngay').val();
    var thoi_luong = $('#thoi_luong').val();
    if (thu != '' && ngay != '' && thoi_luong != '') {
        $('#dlg-buttons').children('button')[0].disabled = true;
    }
    $('#fm').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
                $('#dg').datagrid('clearSelections');
            } else {
                show_messager(result.msg);
            }
            $('#dlg-buttons').children('button')[0].disabled = false;
        }
    });
}


function saveadd() {
    // document.getElementById('fm-add').action=url;
    // document.getElementById('fm-add').submit();
    var gio1 = $('#gio1').val();
    var thoi_luong1 = $('#thoi_luong1').val();
    var ngay1 = $('#ngay1').val();
    if (gio1 != '' && thoi_luong1 != '' && ngay1 != '') {
        $('#dlg-buttons').children('button')[0].disabled = true;
    }
    $('#fm-add').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-add').dialog('close');
                $('#dg').datagrid('reload');
                $('#dg').datagrid('clearSelections');
            } else {
                show_messager(result.msg);
            }
            $('#dlg-buttons-add').children('button')[0].disabled = false;
        }
    });
}


function del() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa?', function (r) {
            if (r) {
                $.post(baseUrl + '/lichhoc/del', {id: row.id}, function (result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                        $('#dg').datagrid('clearSelections');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function delall() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa toàn bộ lịch lớp này?', function (r) {
            if (r) {
                $.post(baseUrl + '/lichhoc/delall', {lophoc: row.lop_hoc}, function (result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                        $('#dg').datagrid('clearSelections');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function tableview() {
    window.location.assign('lichhoc/view');
}

function dayview() {
    window.location.assign('lichhoc/dayview');
}

function indexview() {
    window.location.assign('lichhoc');
}

function timeview() {
    window.location.assign('lichhoc/timeview');
}

function duyetview() {
    window.location.assign('lichhoc/duyetview');
}

function tinhtrang(val, row) {
    if (val == 1)
        return 'Chờ';
    else if (val == 2)
        return 'Đã setup';
    else if (val == 3)
        return 'Đang học';
    else if (val == 4)
        return 'Hoàn thành';
    else if (val == 5)
        return 'Đã chốt';
    else if (val == 6)
        return 'Cọc ngay';
    else if (val == 7)
        return 'Hủy';
    else
        return '';
}

function nhap() {
    $('#dlg-nhap').dialog({
        title: '&nbsp;Nhập lịch học từ file exccel',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-nhap').form('clear');
}

function nhapexel() {
    // document.getElementById('fm-nhap').submit();
    $('#fm-nhap').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-nhap').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function huylich() {
    // $('#dlg-huy').dialog({
    //     title: '&nbsp;Hủy lịch học',
    //     iconCls: 'icon-no',
    //     closed: false
    // });
    $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn hủy lịch chưa diễn ra ở quá khứ?', function (result) {
        if (result) {
            $('#dlg-huy').html('Quá trình hủy lịch đang diễn ra');
            $('#huybutton').html('');
            $('#dlg-huy').dialog({
                title: '&nbsp;Hủy lịch học',
                iconCls: 'icon-no',
                closed: false
            });
            $.post(baseUrl + '/lichhoc/huylich', {lophoc: 0}, function (result) {
                if (result.success) {
                    $('#dlg-huy').html(result.msg);
                    $('#huybutton').html('');
                    $('#dlg-huy').dialog({
                        title: '&nbsp;Hủy lịch học',
                        iconCls: 'icon-no',
                        closed: false
                    });
                    $('#dg').datagrid('reload');
                } else {
                    show_messager(result.msg);
                }
            }, 'json');
        }
    });
}

function submithuy() {
    $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn hủy lịch chưa diễn ra ở quá khứ?', function (result) {
        if (result) {
            // $('#huybutton').linkbutton('disable');
            // $('#fm-huy').form('submit', {
            //     onSubmit: function () {
            //         return $(this).form('validate');
            //     },
            //     success: function (result) {
            //         var result = eval('(' + result + ')');
            //         if (result.success) {
            //             $('#dlg-huy').dialog('close');
            //             $('#dg').datagrid('reload');
            //         } else {
            //             show_messager(result.msg);
            //         }
            //     }
            // });
            $.post(baseUrl + '/lichhoc/huylich', {}, function (result) {
                if (result.success) {
                    $('#dg').datagrid('reload');
                    show_messager(result.msg);
                } else {
                    show_messager(result.msg);
                }
            }, 'json');
        }
    });
}

$(function(){
		$('#ngaynghi').datebox().datebox('calendar').calendar({
				validator: function(date){
						var now = new Date();
						var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
						var d2 = new Date(now.getFullYear(), now.getMonth(), now.getDate()+45);
						return d1<=date && date<=d2;
				}
		});
});

function search() {
		var nhanvien = $('#nhanvien').combobox('getValue');
		var thang = $('#thang').combobox('getValue');
		var nam = $('#nam').combobox('getValue');
		if (nhanvien.length>0 || thang.length>0 || nam.length>0 ) {
	        $('#dg').datagrid('options').url = 'chamcong/json?nhanvien='+nhanvien+'&thang='+thang+'&nam='+nam;
					$.post('chamcong/phep',{nhanvien:nhanvien, thang:thang, nam:nam},function(result){
							if (result.msg=='OK'){
									$('#phep').textbox('setValue',result.phep);
									$('#khongluong').textbox('setValue',result.khongluong);
									$('#khongbao').textbox('setValue',result.phat);
									$('#tongphep').textbox('setValue',result.tongphep);
									$('#danghi').textbox('setValue',result.danghi);
									$('#conlai').textbox('setValue',result.conlai);
							} else {
									show_messager(result.msg);
							}
					},'json');
		} else{
		     	$('#dg').datagrid('options').url = 'chamcong/json';
		}
		$('#dg').datagrid('reload');
}

function chamcong() {
		var nhanvien = $('#nhanvien').combobox('getText');
		$('#dlg').dialog({
		    title: '&nbsp;Chấm công <span style="color:red; font-weight:bold;">'+nhanvien+'</span> ',
				iconCls:'icon-add',
				closed:false
	  });
    $('#fm').form('clear');
		document.getElementById('idnhanvien').value=$('#nhanvien').combobox('getValue');
		$('#apdung').combobox('setValue','1');
		//$('#giovao').timespinner('setValue','08:00:00');
		//$('#giora').timespinner('setValue','17:00:00');
}

function save(){
    $('#fm').form('submit',{
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');
								$('#dg').datagrid('reload');
								var nhanvien = $('#nhanvien').combobox('getValue');
								var thang = $('#thang').combobox('getValue');
								var nam = $('#nam').combobox('getValue');
								$.post('chamcong/phep',{nhanvien:nhanvien, thang:thang, nam:nam},function(result){
										if (result.msg=='OK'){
												$('#phep').textbox('setValue',result.phep);
												$('#khongluong').textbox('setValue',result.khongluong);
												$('#khongbao').textbox('setValue',result.phat);
												$('#tongphep').textbox('setValue',result.tongphep);
												$('#danghi').textbox('setValue',result.danghi);
												$('#conlai').textbox('setValue',result.conlai);
										} else {
												show_messager(result.msg);
										}
								},'json');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function baonghi() {
		$('#dlg-baonghi').dialog({
	    title: '&nbsp;Xin phép nghỉ, công tác...',
			iconCls:'icon-add',
			closed:false
	  });
    $('#fm-baonghi').form('clear');
}

function savenghi(){
    $('#fm-baonghi').form('submit',{
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg-baonghi').dialog('close');
								$('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function checkout(){
		$.post('chamcong/checkout',function(result){
				if (result.msg){
						show_messager(result.msg);
						$('#dg').datagrid('load','chamcong/json');
				} else {
						show_messager(result.msg);
				}
		},'json');
}


// for mobile view
function tieptuc() {
	 	var nhanvien = $('#nhanvien').combobox('getValue');
		var name = $('#nhanvien').combobox('getText');
		var thang = $('#thang').combobox('getValue');
		var nam = $('#nam').combobox('getValue');
		if (nhanvien.length>0 || thang.length>0 || nam.length>0 ) {
				$('#dlg').dialog('close');
				document.getElementById("title").innerHTML = "Chấm công: "+name+" ("+thang+"/"+nam+")";
				$('#dg').datagrid('options').url = 'chamcong/json?nhanvien='+nhanvien+'&thang='+thang+'&nam='+nam;
		}
		else
				$('#dg').datagrid('options').url = 'chamcong/json'
		$('#dg').datagrid('reload');
}

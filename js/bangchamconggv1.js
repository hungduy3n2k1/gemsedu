$(function () {
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    var column = [
        {field: 'tenlop', title: 'Class', width: 100},
        {field: 'tonggio', title: 'Hour', width: 60}
    ];
    for (var i = 1; i <= 31; i++) {
        if (i < 10) {
            i = '0' + i;
        }
        var dt = new Date(nam + '-' + thang + '-' + i);
        if (dt.getDay() == 0) {
            column.push({field: 'ngay_' + i, title: 'Sunday', width: 60, align: 'center'});
        } else if (dt.getDay() == 1) {
            column.push({field: 'ngay_' + i, title: 'Monday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 2) {
            column.push({field: 'ngay_' + i, title: 'Tuesday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 3) {
            column.push({field: 'ngay_' + i, title: 'Wednesday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 4) {
            column.push({field: 'ngay_' + i, title: 'Thursday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 5) {
            column.push({field: 'ngay_' + i, title: 'Friday<br>' + i, width: 60, align: 'center'});
        } else {
            column.push({field: 'ngay_' + i, title: 'Sarturday<br>' + i, width: 60, align: 'center'});
        }
    }
    $('#dg').datagrid({
        columns: [column]
    });
    $('#dg').datagrid('options').url = 'bangchamconggv1/json';
    $('#dg').datagrid('reload');
});

function search() {
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    var column = [
        {field: 'tenlop', title: 'Class', width: 100},
        {field: 'tonggio', title: 'Hour', width: 60}
    ];
    for (var i = 1; i <= 31; i++) {
        if (i < 10) {
            i = '0' + i;
        }
        var dt = new Date(nam + '-' + thang + '-' + i);
        if (dt.getDay() == 0) {
            column.push({field: 'ngay_' + i, title: 'Sunday', width: 60, align: 'center'});
        } else if (dt.getDay() == 1) {
            column.push({field: 'ngay_' + i, title: 'Monday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 2) {
            column.push({field: 'ngay_' + i, title: 'Tuesday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 3) {
            column.push({field: 'ngay_' + i, title: 'Wednesday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 4) {
            column.push({field: 'ngay_' + i, title: 'Thursday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 5) {
            column.push({field: 'ngay_' + i, title: 'Friday<br>' + i, width: 60, align: 'center'});
        } else {
            column.push({field: 'ngay_' + i, title: 'Sarturday<br>' + i, width: 60, align: 'center'});
        }
    }
    $('#dg').datagrid({
        columns: [column]
    });
    $('#dg').datagrid('options').url = 'bangchamconggv1/json?thang=' + thang + '&nam=' + nam;
    $('#dg').datagrid('reload');
}

function searchdemo() {
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    var column = [
        {field: 'tenlop', title: 'Class', width: 100},
        {field: 'tonggio', title: 'Times', width: 60}
    ];
    for (var i = 1; i <= 31; i++) {
        if (i < 10) {
            i = '0' + i;
        }
        var dt = new Date(nam + '-' + thang + '-' + i);
        if (dt.getDay() == 0) {
            column.push({field: 'ngay_' + i, title: 'Sunday', width: 60, align: 'center'});
        } else if (dt.getDay() == 1) {
            column.push({field: 'ngay_' + i, title: 'Monday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 2) {
            column.push({field: 'ngay_' + i, title: 'Tuesday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 3) {
            column.push({field: 'ngay_' + i, title: 'Wednesday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 4) {
            column.push({field: 'ngay_' + i, title: 'Thursday<br>' + i, width: 60, align: 'center'});
        } else if (dt.getDay() == 5) {
            column.push({field: 'ngay_' + i, title: 'Friday<br>' + i, width: 60, align: 'center'});
        } else {
            column.push({field: 'ngay_' + i, title: 'Sarturday<br>' + i, width: 60, align: 'center'});
        }
    }
    $('#dg').datagrid({
        columns: [column]
    });
    $('#dg').datagrid('options').url = 'bangchamconggv1/jsondemo?thang=' + thang + '&nam=' + nam;
    $('#dg').datagrid('reload');
}

function add() {
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    $.post('bangchamconggv1/add', {thang: thang, nam: nam}, function (result) {
        if (result.success) {
            // show_messager(result.msg);
            $('#dg').datagrid('options').url = 'bangchamcong/jsondemo?thang=' + thang + '&nam=' + nam;
            $('#dg').datagrid('reload');
        } else {
            show_messager(result.msg);
        }
    }, 'json');
}

// function chamcong() {
//     var thang = $('#thang').combobox('getValue');
//     var nam = $('#nam').combobox('getValue');
//     $.post('bangchamcong/chamcong', {thang: thang,nam:nam}, function(result) {
//         if (result.success) {
//             // show_messager(result.msg);
//             $('#dg').datagrid('options').url = 'bangchamcong/json?thang='+thang+'&nam='+nam;
//             $('#dg').datagrid('reload');
//         } else {
//             show_messager(result.msg);
//         }
//     }, 'json');
// }

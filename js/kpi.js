$(function() {
    $('#dg').datagrid({
        view: detailview,
        detailFormatter: function(index, row) {
            return '<div id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
        },
        onExpandRow: function(index, row) {
            var ngaybd = $('#ngaybd').datebox('getValue');
            var ngaykt = $('#ngaykt').datebox('getValue');
            $('#ddv-' + index).panel({
                border: false,
                cache: false,
                href: baseUrl + '/kpi/detail?id='+row.id+'&ngaybd='+ngaybd+'&ngaykt='+ngaykt,
                onLoad: function() {
                    $('#dg').datagrid('fixDetailRowHeight', index);
                }
            });
        }
    });
});

function timkiem() {
    var ngaybd = $('#ngaybd').datebox('getValue');
    var ngaykt = $('#ngaykt').datebox('getValue');
    // var nhanvien = $('#nhanvien').combobox('getValue');
    if (ngaybd.length > 0 || ngaykt.length > 0) {
        $('#dg').datagrid('options').url = baseUrl + '/kpi/json?ngaybd='+ngaybd+'&ngaykt='+ngaykt;
        $('#dg').datagrid('reload');
    }
}


function deadline(val,row) { //formatter
		var dd = String(today.getDate()).padStart(2, '0');
		var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();
		var x = format_date_2(val);
		var y = new Date(x);
		var z = new Date(yyyy+'-'+mm+'-'+dd);
		var k = format_date_2(row.ngaykt);
		var n = new Date(k);
		if (z.getTime() > y.getTime())
				if (row.ngaykt=='00/00/0000')
						return '<span style="color:red">'+val+'</span>'; // trễ deadline do chưa có ngày kết thúc
				else if (n.getTime() > y.getTime())
						return '<span style="color:red">'+val+'</span>'; // trễ deadline do ngày kết thúc sau ngày deadline
				else
						return val;
		else if (z.getTime() == y.getTime())
				return '<span style="color:orange">'+val+'</span>';
		else
				return val;
}

function ngay(val,row) { //formatter
		if (val=='00/00/0000')
				return '';
		else
				return val;
}

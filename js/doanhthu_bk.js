function timkiem() {
    var ngaybd = $('#ngaybd').datebox('getValue');
    var ngaykt = $('#ngaykt').datebox('getValue');
    var loaidh = $('#loaidh').combobox('getValue');
    var khachhang = $('#khachhang').textbox('getValue');
    if (ngaybd.length>0 || ngaykt.length>0 || loaidh.length>0 || khachhang.length>0)
        $('#dg').datagrid('options').url = 'doanhthu/json?ngaybd='+ngaybd+'&ngaykt='+ngaykt+'&loaidh='+loaidh+'&khachhang='+khachhang;
    else
        $('#dg').datagrid('options').url = 'doanhthu/json';
    $('#dg').datagrid('reload');
}

function search() { //mobile
    var team = $('#team').combobox('getValue');
    var thang = $('#thang').combobox('getValue');
    var nam = $('#nam').combobox('getValue');
    if (team.length>0 || thang.length>0 || nam.length>0) {
				$('#dg').datagrid('options').url = 'doanhthu/jsonmobile?team='+team+'&thang='+thang+'&nam='+nam;
				var title = 'Báo cáo doanh thu tháng '+thang+' năm '+nam;
				var dgPanel = $('#dg').datagrid('getPanel');
 				dgPanel.panel('setTitle', title);
		} else
        $('#dg').datagrid('options').url = 'doanhthu/jsonmobile';
    $('#dlg-search').dialog('close');
    $('#dg').datagrid('reload');
}

function phanloai(val, row){
    if(val==1){
        return 'New sale';
    } else if(val==2){
        return 'Tái tục trải nghiệm';
    } else if(val==3){
        return 'Tái tục new';
    } else {
        return '';
    }
}

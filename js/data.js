$(function () {
    $('#dg').datagrid({
        onDblClickRow: function (index, row) {
            // $('#phuhuynh').datalist({
            //     title: 'Phụ huynh',
            //     url: 'data/phuhuynh?id='+row.id,
            //     textField:'hoten',
            //     lines: true
            // });
            $('#lichsu').datalist({
                title: 'Lịch sử chăm sóc',
                url: 'data/lichsu?id=' + row.id,
                textField: 'noidung',
                lines: true
            });
            edit(row);
            $('#hocvien').val(row.id);
            $('#idhocvien').val(row.id);
        }
    });
    $('#dot_thanh_toan').combobox({
        onSelect: function (rec) {
            for (var k = 1; k < 7; k++) {
                $('#sotiendot' + k).numberbox({value: ''});
                $('#ngaydot' + k).datebox({value: ''});
            }
            var sotien = $('#so_tien').numberbox('getValue');
            var ngaydangky = $('#ngay_dang_ky').datebox('getValue');
            if (rec.id == 1) {
                $('#sotiendot1').numberbox({value: sotien});
                $('#ngaydot1').datebox({value: ngaydangky});
            } else {
                $.post(baseUrl + '/data/chiadot', {
                    sotien: sotien,
                    ngaydangky: ngaydangky,
                    sodot: rec.id
                }, function (result) {
                    if (result.success) {
                        for (var i = 1; i <= rec.id; i++) {
                            $('#sotiendot' + i).numberbox({value: result.data[i].sotiendot});
                            $('#ngaydot' + i).datebox({value: result.data[i].ngaydot});
                        }
                    } else {
                        show_messager(result.success)
                    }
                }, 'json');
            }
            var dot = rec.id;
            $('#sotiendot1').numberbox({
                onChange: function (val) {
                    if (dot > 1) {
                        var conlai = (sotien.replaceAll(',', '') - val) / (dot - 1);
                        for (var m = 2; m <= dot; m++) {
                            $('#sotiendot' + m).numberbox({value: conlai});
                        }
                    }
                }
            });
        }
    });
});

// function phanloai(val, row) {
//     var phanloai = '';
//     if (val == 1)
//         var phanloai = 'Học sinh';
//     else if (val == 2) {
//         var phanloai = 'Phụ huynh';
//     } else if (val == 3)
//         var phanloai = 'Đăng ký dùng thử';
//     else if (val == 4)
//         var phanloai = 'Facebook';
//     else
//         var phanloai = '';
//     return phanloai;
// }

function timkiem() {
    var phanloai = $('#chonphanloai').combobox('getValue');
    var nguoinhap = $('#nguoinhap').combobox('getValue');
    var tinhtrang = $('#tinhtrang').combobox('getValue');
    var nhanvien = $('#nhanvien').combobox('getValue');
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    var kieungay = $('#kieungay').combobox('getValue');
    var tukhoa = $('#tukhoa').textbox('getValue');
    if (tinhtrang.length > 0 || nhanvien.length > 0 || tungay.length > 0 || denngay.length > 0 || nguoinhap.length > 0 || kieungay.length > 0 || tukhoa.length > 0) {
        $('#dg').datagrid('options').url = baseUrl + '/data/json?tinhtrang=' + tinhtrang + '&nhanvien=' + nhanvien +
            '&tungay=' + tungay + '&denngay=' + denngay + '&nguoinhap=' + nguoinhap +
            '&kieungay=' + kieungay + '&tukhoa=' + tukhoa + '&phanloai=' + phanloai;
    } else {
        $('#dg').datagrid('options').url = baseUrl + '/data/json';
    }
    $('#dg').datagrid('reload');
}

function loctrung() {
    $('#dg').datagrid('options').url = baseUrl + '/data/json?loctrung=1';
    $('#dg').datagrid('reload');
}

function chia() {
    var rows = $('#dg').datagrid('getSelections');
    if (rows.length > 0) {
        var json = JSON.stringify(rows);
        document.getElementById("data").value = json;
        $('#dlg-chia').dialog({
            title: '&nbsp;Chia data',
            iconCls: 'icon-edit',
            closed: false
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function savechia() {
    $('#fm-chia').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-chia').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function nhap() {
    $('#dlg-nhap').dialog({
        title: '&nbsp;Nhập data từ file exccel',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-nhap').form('clear');
}

function nhapexel() {
    // document.getElementById('fm-nhap').submit();
    $('#fm-nhap').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-nhap').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function edit(row) {
    $('#dlg-goi').dialog({
        title: '&nbsp;Cập nhật',
        iconCls: 'icon-edit',
        closed: false
    });
    $('#fm-goi').form('clear');
    $('#fm-goi').form('load', row);
    $('#phanloai1').combobox({value: row.phan_loai});
    // url = baseUrl + '/data/updateData?id=' + row.id;
}

function capnhat() {
    $('#fm-goi').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-goi').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function add() {
    $('#dlg-add').dialog({
        title: '&nbsp;Thêm data mới',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-add').form('clear');
}

function saveadd() {
    $('#fm-add').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-add').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function themph() {
    $('#dlg-ph').dialog({
        title: '&nbsp;Thêm phụ huynh',
        iconCls: 'icon-add',
        closed: false
    });
    $('#hotenphuhuynh').val('');
    $('#dienthoaiphuhuynh').val('');
}

function save() {
    $('#fm').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-ph').dialog('close');
                // $('#phuhuynh').datalist('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function nhatky() {
    $('#dlg-baocao').dialog({
        title: '&nbsp;Ghi nhật ký',
        iconCls: 'icon-add',
        closed: false
    });
    $('#nhatky').val('');
}

function ghinhatky() {
    $('#fm-nhatky').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-baocao').dialog('close');
                $('#lichsu').datalist('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function demo() {
    var tinhtrang = $('#tinh_trang').combobox('getValue');
    if (tinhtrang != 7) {
        var hocvien = document.getElementById('hocvien').value;
        $('#dlg-demo').dialog({
            title: '&nbsp;Đăng ký học demo',
            iconCls: 'icon-add',
            closed: false
        });
        $('#fm-demo').form('clear');
        var tenhocvien = $('#hoc_vien').textbox('getValue');
        if (tenhocvien != '')
            $('#ten_hoc_vien').textbox({value: tenhocvien});
        // $('#loailop').combobox({
        //     onSelect: function(row) {
        //         var tr_ngaygio1 = document.getElementById('tr_ngaygio1');
        //         var tr_ngaygio2 = document.getElementById('tr_ngaygio2');
        //         var tr_lophoc = document.getElementById('tr_lophoc');
        //         if(row.id==15 || row.id==16){
        //             tr_lophoc.style.display='none';
        //             tr_ngaygio1.removeAttribute('style');
        //             tr_ngaygio2.removeAttribute('style');
        //         }else{
        //             tr_ngaygio1.style.display='none';
        //             tr_ngaygio2.style.display='none';
        //             tr_lophoc.removeAttribute('style');
        //         }
        //     }
        // });
        
        url = baseUrl + '/data/hocdemo?idkh=' + hocvien;
    } else {
        show_messager("Khách hàng đã đăng ký học demo");
    }
}

function savedemo() {
    // document.getElementById('fm-demo').action=url;
    // document.getElementById('fm-demo').submit();
    $('#fm-demo').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-goi').dialog('close');
                $('#dlg-demo').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function trainghiem() {
    var tinhtrang = $('#tinh_trang').combobox('getValue');
    if (tinhtrang != 7) {
        var hocvien = document.getElementById('hocvien').value;
        $('#dlg-trainghiem').dialog({
            title: '&nbsp;Đăng ký học trải nghiệm',
            iconCls: 'icon-add',
            closed: false
        });
        $('#fm-trainghiem').form('clear');
        var tenhocvien = $('#hoc_vien').textbox('getValue');
        if (tenhocvien != '')
            $('#ten_hoc_vientn').textbox({value: tenhocvien});
        url = baseUrl + '/data/trainghiem?idkh=' + hocvien;
    } else {
        show_messager("Khách hàng đã đăng ký trải nghiệm");
    }
}

function savetn() {
    // document.getElementById('fm-trainghiem').action=url;
    // document.getElementById('fm-trainghiem').submit();
    $('#fm-trainghiem').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-goi').dialog('close');
                $('#dlg-trainghiem').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function dangky() {
    var tinhtrang = $('#tinh_trang').combobox('getValue');
    if (tinhtrang != 9) {
        var tenhocvien = $('#hoc_vien').textbox('getValue');
        var hocvien = document.getElementById('hocvien').value;
        $('#dlg-dangky').dialog({
            title: '&nbsp;Đăng ký khóa học',
            iconCls: 'icon-add',
            closed: false
        });
        $('#fm-dangky').form('clear');
        if (tenhocvien != '')
            $('#ten_hoc_viendk').textbox({value: tenhocvien});
        $('#ngay_dang_ky').datebox({value: homnay});
        $('#product').combobox({
            onSelect: function (rec) {
                $('#buoi_hoc').textbox({value: rec.so_buoi});
                $('#so_tien').textbox({value: Comma(rec.don_gia)});
                document.getElementById('thangtt').value = rec.thoi_gian;
            }
        });
        url = baseUrl + '/data/dangky?idkh=' + hocvien;
    } else {
        show_messager("Khách hàng đã đăng ký trải nghiệm");
    }
}

function savedk() {
    // document.getElementById('fm-dangky').action=url;
    // document.getElementById('fm-dangky').submit();
    $('#fm-dangky').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg-dangky').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function kpi(val, row) {
    if (row.tongdata != 0)
        var kpi = (row.calls * 100 / row.tongdata).toFixed(2);
    else
        var kpi = '';
    return kpi;
}

function del() {
    var rows = $('#dg').datagrid('getSelections');
    if (rows.length > 0) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa các bản ghi đã chọn?', function (r) {
            if (r) {
                var json = JSON.stringify(rows);
                $.post(baseUrl + '/data/del', {
                    data: json
                }, function (result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}


function baocao() {
    $('#dlg-baocao').dialog({
        title: '&nbsp;Báo cáo telesale',
        iconCls: 'icon-baocao',
        closed: false
    });
    var tungay = $('#tungay').datebox('getValue');
    var denngay = $('#denngay').datebox('getValue');
    $('#dg-baocao').datagrid('load', 'data/baocao?tungay=' + tungay + '&denngay=' + denngay);
}

function getback() {
    $('#dlg-ungvien').dialog('close');
    $('#dlg-goi').dialog('open');
    $('#tt').tree('reload');
}

function saveuv() {
    var nodes = $('#tt').tree('getChecked');
    var str = '';
    for (var i = 0; i < nodes.length; i++) {
        if (str != '') str += ',';
        str += nodes[i].id;
    }
    document.getElementById("danhmuc").value = str;
    // var fmuv = document.getElementById('fm-ungvien');
    // fmuv.action = url;
    // fmuv.submit();
    $('#fm-ungvien').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-ungvien').dialog('close');
                show_messager(result.msg);
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

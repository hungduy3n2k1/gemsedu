$(function() {
    $('#loai').combobox({
        onSelect:function(row) {
            if (row.id==0)
                $("#khach_hang").combobox({url:'common/khachhang'});
            else
                $("#khach_hang").combobox({url:'common/nhacungcap'});
        }
    });
    $('#taikhoan').combobox({
        onLoadSuccess:function(items) {
            if (items.length){
                var opts = $(this).combobox('options');
                $(this).combobox('select', items[0][opts.valueField]);
            }
        }
    });
});

function timkiem() {
    var nam = $('#chonnam').combobox('getValue');
    var thang = $('#chonthang').combobox('getValue');
    // var tukhoa = $('#tukhoa').textbox('getValue');
    var nhanvien = $('#nhanvien').combobox('getValue');
    // var loaicongdoan = $('#loaicongdoan').combobox('getValue');
    // var dgtitle = 'Thu chi '+$('#taikhoan').combobox('getText');
    if (thang.length>0 || nam.length>0 || tukhoa.length>0 || taikhoan.length>0 && nhanvien.length>0)
        $('#dg').datagrid('options').url = 'congdoan/json?thang='+thang+'&nam='+nam+'&nhanvien='+nhanvien;
    else
        $('#dg').datagrid('options').url = 'congdoan/json';
    if($('#dlg-search').dialog)
        $('#dlg-search').dialog('close');
    $('#dg').datagrid('reload');
    // var p = $('#dg').datagrid('getPanel');  // get the panel object
    // p.panel('setTitle',dgtitle);
}

function add() {
    $("#dlg").dialog({
        title: "&nbsp;Lập phiếu công đoàn",
        iconCls: "icon-add",
        closed: false,
    });
    $("#fm").form("clear");
    $("#loai").combobox({readonly:false,required:true});
    // $("#khach_hang").combobox({readonly:false,required:true});
    // $("#tai_khoan").combobox({readonly:false,required:true});
    // $("#ngaygio").textbox("setValue", homnay);
    url = "congdoan/add";
}

function edit() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        $("#dlg").dialog("open").dialog("setTitle", "Sửa phiếu");
        $("#fm").form("clear");
        $("#loai").combobox({readonly:true,required:false});
        // if (row.loai==0)
        //     $("#khach_hang").combobox({url:'common/khachhang',readonly:true,required:false});
        // else
        //     $("#khach_hang").combobox({url:'common/nhacungcap',readonly:true,required:false});
        // $("#tai_khoan").combobox({readonly:true,required:false});
        $("#fm").form("load", row);
        // $("#khach_hang").combobox('setValue',row.khach_hang);
        url = "congdoan/update?id=" + row.id;
    } else {
        show_messager("Không có bản ghi nào được chọn");
    }
}

function save() {
    $("#fm").form("submit", {
        url: url,
        onSubmit: function () {
            return $(this).form("validate");
        },
        success: function (result) {
            var result = eval("(" + result + ")");
            if (result.success) {
                $("#dlg").dialog("close");
                show_messager(result.msg);
                $("#dg").datagrid("reload");
            } else {
                show_messager(result.msg);
            }
        },
    });
}

function del() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        $.messager.confirm("Thông báo", "Bạn có chắc chắn muốn xóa bản ghi này?", function (r) {
            if (r) {
                $.post("congdoan/del",{id: row.id,},
                    function (result) {
                    alert(result);
                        if (result.success) {
                            show_messager(result.msg);
                            $("#dg").datagrid("reload");
                        } else {
                            show_messager(result.msg);
                        }
                    },"json");
            }
        });
    } else {
        show_messager("Không có bản ghi nào được chọn");
    }
}


function sodu() {
    $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn cập nhật lại số dư?', function(r) {
        if (r) {
            var ngay = $('#ngaybd').datebox('getValue');
           // var taikhoan = $('#taikhoan').combobox('getValue');
            $.post('congdoan/sodu', {ngay:ngay},
                function(result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
        }
    });
}

function capnhat() {
    var nam = $('#chonnam').combobox('getValue');
    var thang = $('#chonthang').combobox('getValue');
    $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn cập nhật lại quỹ công đoàn?', function(r) {
        if (r) {
            // var taikhoan = $('#taikhoan').combobox('getValue');
            $.post('congdoan/capnhat', {thang:thang,nam:nam},
                function(result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
        }
    });
}

// set default select the first item of all combobox
// $.fn.combobox.defaults.onLoadSuccess = function(items){
// 	if (items.length){
// 		var opts = $(this).combobox('options');
// 		$(this).combobox('select', items[0][opts.valueField]);
// 	}
// }

$(function () {
    $('#loai').combobox({
        onSelect: function (row) {
            if (row.id == 0)
                $("#khach_hang").combobox({url: 'common/khachhang'});
            else
                $("#khach_hang").combobox({url: 'common/nhacungcap'});
        }
    });
    $('#taikhoan').combobox({
        onLoadSuccess: function (items) {
            if (items.length) {
                var opts = $(this).combobox('options');
                $(this).combobox('select', items[0][opts.valueField]);
            }
        }
    });
    $('#invoice').combobox({
        formatter: function (row) {
            return "IV-" + row.id + '&nbsp;(<span style="font-weight:bold;">'+Comma(row.sotien)+')</span><br><span style="color:red">' + row.noi_dung + '</span>';
            //return '<span id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
        }
    });
});

function timkiem() {
    var ngaybd = $('#ngaybd').datebox('getValue');
    var ngaykt = $('#ngaykt').datebox('getValue');
    var nhanvien = $('#nhanvien').combobox('getValue');
    var tinhtrang = $('#tinhtrang').combobox('getValue');
    // var dgtitle = 'Thu chi '+$('#taikhoan').combobox('getText');
    // show_messager('denghi/json?ngaybd=' + ngaybd + '&ngaykt=' + ngaykt + '&nhanvien=' + nhanvien + '&tinhtrang=' + tinhtrang);
    if (ngaybd.length > 0 || ngaykt.length > 0 || tinhtrang.length > 0 || nhanvien.length > 0)
        $('#dg').datagrid('options').url = 'denghi/json?ngaybd=' + ngaybd + '&ngaykt=' + ngaykt + '&nhanvien=' + nhanvien + '&tinhtrang=' + tinhtrang;
    else
        $('#dg').datagrid('options').url = 'denghi/json';
    if ($('#dlg-search').dialog)
        $('#dlg-search').dialog('close');
    $('#dg').datagrid('reload');
    // var p = $('#dg').datagrid('getPanel');  // get the panel object
    // p.panel('setTitle',dgtitle);
}

function add() {
    $("#dlg").dialog({
        title: "&nbsp;Lập phiếu",
        iconCls: "icon-add",
        closed: false,
    });
    $("#fm").form("clear");
    $("#nhan_vien").combobox("setValue", manhanvien);
    $("#ngaygio").textbox("setValue", homnay);
    url = "denghi/add";
}

function edit() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        if (row.tinh_trang == 1) {
            $("#dlg").dialog({
                title: "&nbsp;Sửa phiếu",
                iconCls: "icon-edit",
                closed: false,
            });
            $("#fm").form("load", row);
            url = "denghi/update?id=" + row.id;
        } else {
            show_messager("Phiếu đã duyệt không thể sửa!");
        }
    }else
        show_messager("Không có bản ghi nào được chọn!");
}

function save() {
    $("#fm").form("submit", {
        url: url,
        onSubmit: function () {
            return $(this).form("validate");
        },
        success: function (result) {
            var result = eval("(" + result + ")");
            if (result.success) {
                $("#dlg").dialog("close");
                show_messager(result.msg);
                $("#dg").datagrid("reload");
            } else {
                show_messager(result.msg);
            }
        },
    });
    // document.getElementById('fm').action=url;
    // document.getElementById('fm').submit();
}

function del() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        if (row.tinh_trang == 1) {
            $.messager.confirm("Thông báo", "Bạn có chắc chắn muốn xóa bản ghi này?", function (r) {
                if (r) {
                    $.post("denghi/del", {id: row.id,},
                        function (result) {
                            if (result.success) {
                                show_messager(result.msg);
                                $("#dg").datagrid("reload");
                            } else {
                                show_messager(result.msg);
                            }
                        }, "json");
                }
            });
        }else{
            show_messager("Phiếu đã duyệt không thể xóa!");
        }
    }else {
        show_messager("Không có bản ghi nào được chọn");
    }
}


function duyet() {
    var row = $("#dg").datagrid("getSelected");
    if(row.tinh_trang==1) {
        if (row) {
            $("#dlg-duyet").dialog({
                title: "&nbsp;Duyệt đề nghị",
                iconCls: "icon-edit",
                closed: false,
            });
            $('#ngaygio1').datebox({value:row.ngaygio});
            $('#nhan_vien1').combobox({value:row.nhan_vien});
            $('#so_tien1').numberbox({value:row.so_tien});
            $('#noi_dung1').textbox({value:row.noi_dung});
            $('#tinh_trang').combobox({value:row.tinh_trang});
            url = "denghi/duyet?id=" + row.id;
        } else {
            show_messager("Không có bản ghi nào được chọn");
        }
    }else{
        show_messager("Phiếu đã duyệt không thể duyệt lại");
    }
}

function saveduyet() {
    $("#fm-duyet").form("submit", {
        url: url,
        onSubmit: function () {
            return $(this).form("validate");
        },
        success: function (result) {
            var result = eval("(" + result + ")");
            if (result.success) {
                $("#dlg-duyet").dialog("close");
                show_messager(result.msg);
                $("#dg").datagrid("reload");
            } else {
                show_messager(result.msg);
            }
        },
    });
    // document.getElementById('fm').action=url;
    // document.getElementById('fm').submit();
}

$(function () {
    $('#khach_hang').combobox({
        onSelect: function (row) {
            $("#donhang").combobox({ url: 'phieuthu/donhang?khachhang=' + row.id });
        }
    });
    $('#donhang').combobox({
        formatter: function (row) {
            return "DH-" + row.id + '&nbsp;(<span style="font-weight:bold;">' + row.hocvien + ')</span>';
            //return '<span id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
        },
        onSelect: function (row) {
            $("#hocvien").textbox({ value: row.hocvien });
            $("#khoahoc").combobox({ value: row.product });
            $("#sobuoi").textbox({ value: row.so_buoi });
            $("#tongtien").textbox({ value: Comma(row.so_tien) });
            $("#invoice").combobox({ url: 'phieuthu/invoice?donhang=' + row.id });
        }
    });
    // $('#invoice').combobox({
    //     onSelect: function (row) {
    //         $("#so_tien").textbox({value:Comma(row.so_tien)});
    //     }
    // });
    $('#taikhoan').combobox({
        onLoadSuccess: function (items) {
            if (items.length) {
                var opts = $(this).combobox('options');
                $(this).combobox('select', items[0][opts.valueField]);
            }
        }
    });
});
function add() {
    let duno = 0;
    $("#dlg").dialog({
        title: "&nbsp;Thêm mới phiếu thu",
        iconCls: "icon-add",
        closed: false,
    });
    $("#fm").form("clear");
    $("#khach_hang").combobox({ readonly: false, required: true , value: ''});
    $("#tai_khoan").combobox({ readonly: false, required: true ,value: ''});
    $("#ngaygio").textbox("setValue", homnay);
    $("#donhang").combobox({required:true,readonly: false, value: ''});
    $("#invoice").combobox({required:true,readonly: false, value: ''});
    url = baseUrl + "/phieuthu/add?duno=" + duno;
    $('#invoice').combobox({
        onSelect: function (row) {
            duno = row.du_no;
            $("#duno").textbox({value: duno});
            url = baseUrl + "/phieuthu/add";
        }
    });

}

function edit() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Sửa phiếu thu',
            iconCls: 'icon-phieuthu',
            closed: false
        });

        $("#donhang").combobox({required:false});
        $("#fm").form("clear");
        $("#fm").form("load", row);
        // "DH-" + row.id + '&nbsp;(<span style="font-weight:bold;">' + row.hocvien + ')
        $("#donhang").combobox({readonly:true,value: row.donhang});
        $("#donhang").combobox({text: "DH-" + row.id + '&nbsp;(<span style="font-weight:bold;">' + row.hocvien + ')</span>'});
        $("#khach_hang").combobox({ readonly: true, value: row.khach_hang });
        $("#invoice").combobox({readonly:true,value: row.invoice});
        $("#so_tien_cu").textbox({value: row.so_tien});
        $("#duno").textbox({value: row.du_no});

        url = baseUrl + "/phieuthu/update?id=" + row.id;
    } else {
        show_messager("Không có bản ghi nào được chọn");
    }
}

function save() {
    // document.getElementById('fm').action = url;
    // document.getElementById('fm').submit();
    $('#btnSave').linkbutton('enable');
    $("#fm").form("submit", {
        url: url,
        onSubmit: function () {
            return $(this).form("validate");
        },
        success: function (result) {
            var result = eval("(" + result + ")");
            if (result.success) {
                $("#dlg").dialog("close");
                show_messager(result.msg);
                $("#dg").datagrid("reload");
            } else {
                show_messager(result.msg);
            }
        },
    });
}

function del() {
    var row = $("#dg").datagrid("getSelected");
    $("#so_tien_cu").textbox({value: row.so_tien});
    $("#duno").textbox({value: row.du_no});
    if (row) {
        $.messager.confirm("Thông báo", "Bạn có chắc chắn muốn xóa bản ghi này?", function (r) {
            if (r) {
                $.post(baseUrl + "/phieuthu/del", { id: row.id, },
                    function (result) {
                        if (result.success) {
                            show_messager(result.msg);
                            $("#dg").datagrid("reload");
                        } else {
                            show_messager(result.msg);
                        }
                    }, "json");
            }
        });
    } else {
        show_messager("Không có bản ghi nào được chọn");
    }
}

function timkiem() {
    var tungay = $("#tungay").datebox("getValue");
    var denngay = $("#denngay").datebox("getValue");
    var khachhang = $("#khachhang").combobox("getValue");
    var taikhoan = $("#taikhoan").combobox("getValue");
    if (tungay.length > 0 || denngay.length > 0 || khachhang.length > 0 || taikhoan.length > 0 || tukhoa.length > 0) {
        $("#dg").datagrid("options").url = "phieuthu/json?tungay=" + tungay + "&denngay=" + denngay + "&taikhoan=" + taikhoan + "&khachhang=" + khachhang;
    } else $("#dg").datagrid("options").url = "phieuthu/json";
    $("#dg").datagrid("reload");
}
function xuatfile() {
    var tungay = $("#tungay").datebox("getValue");
    var denngay = $("#denngay").datebox("getValue");
    var khachhang = $("#khachhang").combobox("getValue");
    var tukhoa = $("#tukhoa").textbox("getValue");
    window.location.href = baseUrl + "/phieuthu/xuatfile?tungay=" + tungay + "&denngay=" + denngay + "&tukhoa=" + tukhoa + "&khachhang=" + khachhang;
}
function inphieu() {
    var row = $("#dg").datagrid("getSelected");
    if (row) {
        window.location.href = baseUrl + "/phieuthu/inphieu?id=" + row.id;
        window.location.href = baseUrl + "/phieuthu/inphieu?id=" + row.id;
    } else {
        show_messager("Bạn chưa chọn phiếu để in");
    }
}

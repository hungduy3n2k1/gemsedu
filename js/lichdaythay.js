function tinhtrang(val, row) {
    if (val == 1)
        return 'Chờ duyệt';
    else (val == 2)
        return 'Đã duyệt';
}

function duyet()
{
    var rows = $('#dg').datagrid('getSelections');
    if (rows.length > 0) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn duyệt các bản ghi đã chọn?', function (r) {
            if (r) {
                var json = JSON.stringify(rows);
                $.post(baseUrl + '/lichdaythay/duyet', {
                    data: json
                }, function (result) {
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function timkiem() {
    // var gio = $('#giohoc').timespinner('getValue');
    // var ngay = $('#ngay1').textbox('getValue');
    // var thang = $('#thang').textbox('getValue');
    // var nam = $('#nam').textbox('getValue');
    // var loai = $('#loai').textbox('getValue');
    var giaovien = $('#giaovien').combobox('getValue');
    // var phonghoc = $('#phonghoc').textbox('getValue');
    var tenlop = $('#tenlop').textbox('getValue');
    if (giaovien.length > 0 || tenlop.length > 0 ) {
        $("#dg").datagrid("options").url = baseUrl + "/lichdaythay/json?giaovien=" + giaovien + '&tenlop=' + tenlop;
    } else {
        $("#dg").datagrid("options").url = baseUrl + "/lichdaythay/json";
    }
    $("#dg").datagrid("reload");
}

// function pre() {
//     var thang = $('#thang').textbox('getValue');
//     var nam = $('#nam').textbox('getValue');
//     var x = parseInt(thang) - 1;
//     if (x > 0) {
//         if (x < 10)
//             thang = '0' + x;
//         else
//             thang = x;
//         $('#thang').textbox('setValue', thang);
//     } else {
//         thang = '12';
//         var y = parseInt(nam) - 1;
//         $('#thang').textbox('setValue', thang);
//         $('#nam').textbox('setValue', y);
//     }
//     var gio = $('#giohoc').timespinner('getValue');
//     var ngay = $('#ngay1').textbox('getValue');
//     var loai = $('#loai').textbox('getValue');
//     var giaovien = $('#giaovien').textbox('getValue');
//     var phonghoc = $('#phonghoc').textbox('getValue');
//     var tenlop = $('#tenlop').textbox('getValue');
//     if (thang.length > 0 || nam.length > 0 || gio.length > 0 || giaovien.length > 0 ||
//         loai.length > 0 || phonghoc.length > 0 || tenlop.length > 0 || ngay.length>0) {
//         $("#dg").datagrid("options").url = baseUrl + "/lichdaythay/json?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
//                 "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop+'&ngay='+ngay;
        
//     } else {
//         $("#dg").datagrid("options").url = baseUrl + "/lichdaythay/json";
//     }
//     $("#dg").datagrid("reload");
// }

// function next() {
//     var thang = $('#thang').textbox('getValue');
//     var nam = $('#nam').textbox('getValue');
//     var x = parseInt(thang) + 1;
//     if (x < 13) {
//         if (x < 10)
//             thang = '0' + x;
//         else
//             thang = x;
//         $('#thang').textbox('setValue', thang);
//     } else {
//         thang = '01';
//         var y = parseInt(nam) + 1;
//         $('#thang').textbox('setValue', thang);
//         $('#nam').textbox('setValue', y);
//     }
//     var gio = $('#giohoc').timespinner('getValue');
//     var ngay = $('#ngay1').textbox('getValue');
//     var loai = $('#loai').textbox('getValue');
//     var giaovien = $('#giaovien').textbox('getValue');
//     var phonghoc = $('#phonghoc').textbox('getValue');
//     var tenlop = $('#tenlop').textbox('getValue');
//     if (thang.length > 0 || nam.length > 0 || gio.length > 0 || giaovien.length > 0 ||
//         loai.length > 0 || phonghoc.length > 0 || tenlop.length > 0 || ngay.length>0) {
//         $("#dg").datagrid("options").url = baseUrl + "/lichdaythay/json?thang=" + thang + "&nam=" + nam + '&gio=' + gio +
//                 "&loai=" + loai + "&giaovien=" + giaovien + "&phonghoc=" + phonghoc + '&tenlop=' + tenlop+'&ngay='+ngay;
//     } else {
//         $("#dg").datagrid("options").url = baseUrl + "/lichdaythay/json";
//     }
//     $("#dg").datagrid("reload");
// }
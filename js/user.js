function timkiem() {
    var email = $('#email').textbox('getValue');
    var nhanvien = $('#nhanvien').combobox('getValue');
    var giaovien = $('#giaovien').combobox('getValue');
    if (email.length > 0 || nhanvien.length > 0 || giaovien.length>0) {
        $('#dg').datagrid('options').url = baseUrl + '/user/json?email=' + email + '&nhanvien=' + nhanvien+'&giaovien='+giaovien;
    } else {
        $('#dg').datagrid('options').url = baseUrl + '/user/json';
    }
    $('#dg').datagrid('reload');
}

function format_role(val, row) {
    return '<a href="javascript:void(0)" style="font-weight:bold;color:green" onclick="window.location.href=\'' + baseUrl + '/user/phanquyen?id=' + row.id + '\'">Phân quyền</a>';
}

function add() {
    $('#dlg').dialog('open').dialog('setTitle', 'Thêm mới người dùng');
    $('#fm').form('clear');
    $('#username').textbox({required: true});
    url = baseUrl + '/user/add';
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog('open').dialog('setTitle', 'Cập nhật thông tin nhân viên');
        $('#fm').form('clear');
        $('#fm').form('load', row);
        // $('#username').textbox({readonly: true});
        url = baseUrl + '/user/update?id=' + row.id;
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}
function format_nhom(val, row){
    if(val==1)
        return 'CEO';
    else if (val==2)
        return 'Hành chính nhân sự'
    else if (val==3)
        return 'Kinh doanh'
    else if (val==4)
        return 'Markering'
    else if (val==5)
        return 'Kỹ thuật'
    else if(val==6)
        return 'Kế toán'
}

function format_ca(val, row){
    if(val==1)
        return 'Hành Chính';
    else if (val==2)
        return 'Parttime Sáng'
    else if (val==3)
        return 'Parttime Chiều'
}

function del() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa bản ghi này?', function(r) {
            if (r) {
                $.post(baseUrl + '/user/del', {
                    id: row.id
                }, function(result) {
                    if (result.success) {
                        //show_messager(result.msg);
                        $('#dg').datagrid('reload');
                        //$('#dg').datagrid('clearSelections');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
                //$('#dg').datagrid('reload');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function save() {
    $('#fm').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                show_messager(result.msg);
                $('#dg').datagrid('reload');
                //$('#dg').datagrid('clearSelections');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

// function uncheckall(id) {
//       $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa hết toàn bộ quyền của nhân viên này?', function(r) {
//             if (r) {
//                 $.post(baseUrl + '/user/uncheckall', {
//                     id: id
//                 }, function(result) {
//                     if (result.success) {
//                         window.location.href = baseUrl + '/user/phanquyen?id='+id;
//                     } else {
//                         show_messager(result.msg);
//                     }
//                 }, 'json');
//             }
//       });
// }

// function checkall(id) {
//       $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn cấp toàn bộ quyền cho nhân viên này?', function(r) {
//             if (r) {
//                 $.post(baseUrl + '/user/checkall', {
//                     id: id
//                 }, function(result) {
//                     if (result.success) {
//                         window.location.href = baseUrl + '/user/phanquyen?id='+id;
//                     } else {
//                         show_messager(result.msg);
//                     }
//                 }, 'json');
//             }
//       });
// }

// function saverole(id) {
//     $('#fm').form('submit', {
//         onSubmit: function() {
//             return $(this).form('validate');
//         },
//         success: function(result) {
//             var result = eval('(' + result + ')');
//             if (result.success) {
//                 show_messager(result.msg);
//                 //window.location.href = baseUrl + '/user/phanquyen?id='+id;
//             } else {
//                 show_messager(result.msg);
//             }
//         }
//     });
// }

function phanquyen() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg-role').dialog('open').dialog('setTitle', 'Phân quyền chức năng');
        var t = $('#menu');
        var mynode = null;
        var roots = t.tree('getRoots');
        var str = row.menu;
        var arr = str.split(',');
        $.easyui.forEach(roots, true, function(node){
            mynode = node;
            var el = $('#'+mynode.domId);
            t.tree('uncheck', el[0]);
            if (arr.includes(node.id))
                t.tree('check', el[0]);
        });
        url = baseUrl + '/user/saverole?id=' + row.id;
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function saverole() {
    var nodes = $('#menu').tree('getChecked');
    var s = '';
    for(var i=0; i<nodes.length; i++){
        if (s != '') s += ',';
        s += nodes[i].id;
    }
    document.getElementById('role').value = s;
    $('#fm-role').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-role').dialog('close');
                show_messager(result.msg);
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function phanquyenf() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        url = baseUrl + '/user/saverolef?id=' + row.id;
        $('#dlg-rolef').dialog('open').dialog('setTitle', 'Phân quyền thư mục');
        var t = $('#thumuc');
        var mynode = null;
        var roots = t.tree('getRoots');
        var str = row.thu_muc;
        var arr = str.split(',');
        $.easyui.forEach(roots, true, function (node) {
            mynode = node;
            var el = $('#' + mynode.domId);
            t.tree('uncheck', el[0]);
            if (arr.includes(node.id))
                t.tree('check', el[0]);
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function saverolef() {
    var nodes = $('#thumuc').tree('getChecked');
    var s = '';
    for (var i = 0; i < nodes.length; i++) {
        if (s != '') s += ',';
        s += nodes[i].id;
    }
    document.getElementById('rolef').value = s;
    $('#fm-rolef').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-rolef').dialog('close');
                show_messager(result.msg);
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

$(function () {
    $('#tukhoa').textbox({
        onChange: function (rec) {
            timkiem();
        }
    });
    $('#khach_hang').combobox({
        // formatter: function (row) {
        //     if (row.dien_thoai.length > 0)
        //         return '<span style="font-weight:bold;">' + row.name + '</span>-<span style="color:red">' + row.dien_thoai + '</span>';
        //     else
        //         return '<span style="font-weight:bold;">' + row.name + '</span>';
        //     //return '<span id="ddv-' + index + '" style="padding:5px 0;height:200px;width:100%;"></div>';
        // },
        onSelect: function (row) {
            $('#dienthoai').textbox({value: row.dien_thoai});
        }
    });
});

function format_xoasaplop(val, row) {
    return '<a href="javascript:void(0)" onClick="xoaSapLop(' + val + ')" style="font-weight: bold;color: blue;">Xóa</a>'
}

function gioitinh(val, row) {
    if (val == 1)
        return 'Nam';
    else
        return 'Nữ';
}

function format_phanloai(val, row) {
    if (val == 1)
        return 'Online';
    else if (val == 2)
        return 'Offline';
    else if (val == 3)
        return 'Test Online';
    else if (val == 4)
        return 'Test Offline';
    else
        return '';
}

function format_tinhtrang(val, row) {
    if (val == 1)
        return 'Đang chờ';
    else if (val == 2)
        return 'Đang học';
    else if (val == 3)
        return 'Đã tốt nghiệp';
    else
        return '';
}

function timkiem() {
    var tukhoa = $('#tukhoa').textbox('getValue');
    var phanloai = $('#phanloai').combobox('getValue');
    var khachhang = $('#khachhang').combobox('getValue');
    var tinhtrang = $('#tinhtrang').combobox('getValue');
    if (tukhoa.length > 0 || phanloai.length > 0 || tinhtrang.length > 0 || khachhang.length > 0)
        $('#dg').datagrid('options').url = baseUrl + '/hocvien/json?tukhoa=' + tukhoa + '&phanloai=' + phanloai +
            '&tinhtrang=' + tinhtrang + '&khachhang=' + khachhang;
    else
        $('#dg').datagrid('options').url = baseUrl + '/hocvien/json';
    $('#dg').datagrid('reload');
}

function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm học viên mới ',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm').form('clear');
    url = baseUrl + '/hocvien/add';
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Xem và cập nhật thông tin học viên',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('clear');
        $('#fm').form('load', row);
        url = baseUrl + '/hocvien/update?id=' + row.id;
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function save() {
    // $(this).attr('disabled', 'disabled');
    var name = $('#name').val();
    var e_name = $('#e_name').val();
    if(name != '' && e_name != ''){
        $('#dlg-buttons').children('button')[0].disabled = true;
    }
    $('#fm').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
            $('#dlg-buttons').children('button')[0].disabled = false;
        }
    });
}

function del() {
    var row = $('#dg').treegrid('getSelected');
    if (row) {
        $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa?', function (r) {
            if (r) {
                $.post(baseUrl + '/hocvien/del', {id: row.id}, function (result) {
                    if (result.success) {
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }
        });
    } else {
        show_messager('Không có bản ghi nào được chọn');
    }
}

function nhap() {
    $('#dlg-nhap').dialog({
        title: '&nbsp;Nhập học viên từ file exccel',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-nhap').form('clear');
}

function nhapexel() {
    // document.getElementById('fm-nhap').submit();
    $('#fm-nhap').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-nhap').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function nhaplich() {
    var row = $('#dg').treegrid('getSelected');
    if (row) {
        $('#dlg-nhaplich').dialog({
            title: '&nbsp;Nhập học viên từ file exccel',
            iconCls: 'icon-add',
            closed: false
        });
        $('#fm-nhaplich').form('clear');
        $('#chonlop').combobox({value:row.lop});
        url = baseUrl + '/hocvien/importlich?hocvien=' + row.id;
    } else {
        show_messager('Không có bản ghi nào được chọn!');
    }
}

function nhapexcellich() {
    // document.getElementById('fm-nhaplich').action=url;
    // document.getElementById('fm-nhaplich').submit();
    $('#fm-nhaplich').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-nhaplich').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function lichhoc() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        document.getElementById('hocvien').value = row.id;
        document.getElementById('tenhocvien').value = row.name;
        document.getElementById('fm-lichhoc').submit();
    } else {
        show_messager('Bạn chưa chọn học viên!');
    }
}

function xoaSapLop(id) {
    var row = $('#dg').datagrid('getSelected');
    $.messager.confirm('Thông báo', 'Bạn có chắc chắn muốn xóa?', function (r) {
        if (r) {
            $.post(baseUrl + '/hocvien/xoasaplop', {hocvien: row.id, lophoc: id}, function (result) {
                if (result.success) {
                    $('#dg-saplop').datagrid('reload');
                    $('#dg').datagrid('reload');
                    show_messager(result.msg);
                } else {
                    show_messager(result.msg);
                }
            }, 'json');
        }
    });
}
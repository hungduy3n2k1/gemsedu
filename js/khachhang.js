$(function() {
    $('#contact').combobox({
        onSelect: function(row) {
            $('#chucvu').textbox('setValue',row.chuc_vu);
            $('#dienthoai').textbox('setValue',row.dien_thoai);
            $('#emailcts').textbox('setValue',row.email);
            $('#ghichu').textbox('setValue',row.ghi_chu);
        }
    });
});

function timkiem() {
    var tukhoa = $('#tukhoa').textbox('getValue');
    var nhanvien = $('#nhanvien').combobox('getValue');
    var phutrach = $('#phutrach').combobox('getValue');
    var loai = $('#chonloai').combobox('getValue');
    // show_messager(baseUrl + '/khachhang/json?tukhoa='+tukhoa+'&tinhtrang='+tinhtrang+
    //     '&nhanvien='+nhanvien+'&phutrach='+phutrach+'&loai='+loai);
    if (tukhoa.length>0 || phutrach.length>0 || nhanvien.length>0 || loai.length>0)
        $('#dg').datagrid('options').url = baseUrl + '/khachhang/json?tukhoa='+tukhoa+
            '&nhanvien='+nhanvien+'&phutrach='+phutrach+'&loai='+loai;
    else
        $('#dg').datagrid('options').url = baseUrl + '/khachhang/json';
    $('#dg').datagrid('reload');
}

function xuatfile1() {
    
    var tinhtrang = $('#tinhtrang').combobox('getValue');
    var tukhoa = $('#tukhoa').textbox('getValue');
    var nhanvien = $('#nhanvien').combobox('getValue');
    var phutrach = $('#phutrach').combobox('getValue');
        window.location.href = baseUrl + '/khachhang/xuatfile?tukhoa='+tukhoa+'&tinhtrang='+tinhtrang+'&nhanvien='+nhanvien+'&phutrach='+phutrach;
    
}

function add() {
    $('#dlg').dialog({
        title: '&nbsp;Thêm khách hàng mới',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm').form('reset');
    $('#tinh_trang').combobox({readonly:true, value:1});
    $('#nhan_vien').combobox({readonly:true, value:manhanvien});
    $('#phu_trach').combobox({value:manhanvien});
    // $('#dg-lienhe').datagrid('loadData', []);
    $('#dg-lienhe').datagrid({url:'khachhang/contact?id=null',toolbar: [{text:'Danh sách contact '}]});
    $('#dg-lienhe').datagrid('reload');
    url = baseUrl + '/khachhang/add';
}

function edit() {
    var row = $('#dg').datagrid('getSelected');
    if (row) {
        $('#dlg').dialog({
            title: '&nbsp;Thông tin khách hàng',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm').form('clear');
        $('#fm').form('load', row);
        $('#tinh_trang').combobox({readonly:false, value:row.tinh_trang});
        $('#nhan_vien').combobox({readonly:false, value:row.nhan_vien});
        url = baseUrl + '/khachhang/update?id=' + row.id;
        $('#dg-lienhe').datagrid({url:'khachhang/contact?id='+row.id,
            toolbar: [{
                text:'Danh sách contact '
            },'-',{
                text:'Thêm',
                iconCls:'icon-add',
                handler:function(){contactadd()}
            },{
                text:'Sửa',
                iconCls:'icon-edit',
                handler:function(){contactedit()}
            },{
                text:'Xóa',
                iconCls:'icon-no',
                handler:function(){contactdel()}
            }]
        });
        $('#dg-lienhe').datagrid('reload');
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function save() {
    // var data = $('#dg-lienhe').datagrid('getRows');
    // document.getElementById('contact').value=JSON.stringify(data);
    var name = $('#name').val();
    var dien_thoai1 = $('#dien_thoai1').val();
    if(name != '' && dien_thoai1 != ''){
        $('#dlg-buttons').children('button')[0].disabled = true;
    }
    $('#fm').form('submit', {
        url: url,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                show_messager(result.msg);
                $('#dlg').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
            $('#dlg-buttons').children('button')[0].disabled = false;
        }
    });
    
}

function del(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
            if (r){
                $.post(baseUrl+'/khachhang/del',{id:row.id},function(result){
				    if (result.success){
				        $('#dg').datagrid('reload');
				    } else {
						    show_messager(result.msg);
				    }
				},'json');
            }
        });
    }else{
		show_messager('Không có bản ghi nào được chọn');
    }
}

function nhap() {
    $('#dlg-nhap').dialog({
        title: '&nbsp;Nhập data từ file exccel',
        iconCls: 'icon-add',
        closed: false
    });
    $('#fm-nhap').form('clear');
}

function nhapexel() {
    $('#fm-nhap').form('submit', {
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-nhap').dialog('close');
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

//--------------- xử lý contacts
function contactadd() {
      $('#dlg-lienhe').dialog({
          title: '&nbsp;Thêm contact',
          iconCls: 'icon-edit',
          closed: false
      });
      $('#fm-lienhe').form('clear');
      var row = $('#dg').datagrid('getSelected');
      url2 = baseUrl + '/khachhang/addcts?khachhang='+row.id;
}

function contactedit() {
    var row = $('#dg-lienhe').datagrid('getSelected');
    if (row) {
        $('#dlg-lienhe').dialog({
            title: '&nbsp;Cập nhật contact',
            iconCls: 'icon-edit',
            closed: false
        });
        $('#fm-lienhe').form('clear');
        $('#contact').combobox('setValue',row.id);
        // $('#fm-lienhe').form('load', row);
        url2 = baseUrl + '/khachhang/savects?id='+row.id;
    } else
        show_messager('Không có bản ghi nào được chọn');
}

function savects() {
    $('#fm-lienhe').form('submit', {
        url: url2,
        onSubmit: function() {
            return $(this).form('validate');
        },
        success: function(result) {
            var result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg-lienhe').dialog('close');
                $('#dg-lienhe').datagrid('clearSelections');
                $('#dg-lienhe').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function contactdel(){
    var row = $('#dg-lienhe').datagrid('getSelected');
    if (row){
        $.messager.confirm('Thông báo','Bạn có chắc chắn muốn xóa?',function(r){
            if (r){
                $.post(baseUrl+'/khachhang/delcts',{id:row.id},function(result){
				    if (result.success){
				        $('#dg-lienhe').datagrid('reload');
				    } else {
						    show_messager(result.msg);
				    }
				},'json');
            }
        });
    }else{
		show_messager('Không có bản ghi nào được chọn');
    }
}

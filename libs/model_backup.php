<?php
class model
{
   function __construct()
   {
       $this->db = new database();
   }
   // các hàm thao tác trên database
   function insert($table, $array)
   {
       $cols = array();
       $bind = array();
       foreach ($array as $key => $value) {
           $cols[] = $key;
           $bind[] = "'" . $value . "'";
       }
       $query = $this->db->query("INSERT INTO " . $table . " (" . implode(",", $cols) . ") VALUES (" . implode(",", $bind) . ")");
       return $query;
   }

   function update($table, $array, $where)
   {
       $set = array();
       foreach ($array as $key => $value) {
           $set[] = $key . " = '" . $value . "'";
       }
       $query = $this->db->query("UPDATE " . $table . " SET " . implode(",", $set) . " WHERE " . $where);
       return $query;
   }

   function delete($table, $where = '')
   {
       if ($where == '') {
           $query = $this->db->query("DELETE FROM " . $table);
       } else {
           $query = $this->db->query("DELETE FROM " . $table . " WHERE " . $where);
       }
       return $query;
   }

   // Các hàm mở rộng
   // function updateip($userip)
   // {
   //     $query = $this->db->query("SELECT COUNT(*) AS total FROM users WHERE id=1 AND ip='$userip' ");
   //     $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
   //     if ($temp[0]['total'] == 0)
   //         $this->db->query("UPDATE users SET ip='$userip' ");
   // }

   function chamcong($nhanvien, $userip, $time)
   {
       $query = $this->db->query("SELECT COUNT(*) AS total FROM system WHERE id=1 AND value='$userip' ");
       $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
       if ($temp[0]['total'] == 1) {
           $ngay  = substr($time, 0, 10);
           $gio   = substr($time, -8);
           $query = $this->db->query("SELECT COUNT(*) AS total FROM chamcong WHERE nhan_vien=$nhanvien
               AND ngay='$ngay' ");
           $temp  = $query->fetchAll(PDO::FETCH_ASSOC);
           if ($temp[0]['total'] == 0) {
               if ($gio < "08:00:01")
                   $chamcong = 1;
               elseif ($gio < "09:00:01")
                   $chamcong = 3;
               elseif ($gio < "13:30:01")
                   $chamcong = 5;
               else
                   $chamcong = 0;
               if ($chamcong==1 || $chamcong==3)
                  $cong=1;
              elseif ($chamcong==5)
                  $cong = 0.5;
              else
                  $cong=0;
               $data = array(
                   'nhan_vien' => $nhanvien,
                   'ngay' => $ngay,
                   'gio_vao' => $gio,
                   'ghi_chu' => 'chấm công',
                   'cham_cong' => $chamcong,
                   'cong' => $cong
               );
               $this->insert("chamcong", $data);
               if ($chamcong==3) {
                 $data = array(
                     'nhan_vien' => $nhanvien,
                     'ngay_gio' => $time,
                     'event' => 2,
                     'so_tien' =>-50000,
                     'ghi_chu' => '',
                     'tinh_trang' => 1
                 );
                 $this->insert("thuongphat", $data);
               }
           }
       }
   }
    function mainmenu(){
            $query = $this->db->query("SELECT id, name, link, icon,mobile FROM menu WHERE parentid = 0 AND active=1 ORDER BY thu_tu");
            return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    // function submenu($cha){
    //            $query = $this->db->query("SELECT id, name, link, icon,mobile FROM menu
    //              WHERE active=1 AND id!=3 AND parentid = $cha ORDER BY thu_tu");
    //            return $query->fetchAll(PDO::FETCH_ASSOC);
    // }

    function submenu($id)
    {

        $submenu = array();
        $query = $this->db->query("SELECT id, name, link, icon FROM menu WHERE active=1 AND parentid = $id ORDER BY thu_tu");
        $temp = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($temp AS $item) {
            $found = false;
            if ($_SESSION['user']['id']==1) 
                $found = true;
            else {
                $conid = $item['id'];
                $query = $this->db->query("SELECT menu FROM users WHERE id=".$_SESSION['user']['id']);
                $a = $query->fetchAll(PDO::FETCH_ASSOC);
                $menu = explode(',',$a[0]['menu']);
                if (in_array($conid, $menu))
                    $found=true;
                else {
                    $query = $this->db->query("SELECT id FROM menu WHERE active=1 AND parentid = $conid ");
                    $rows = $query->fetchAll(PDO::FETCH_ASSOC);
                    if (sizeof($rows)>0)
                        foreach ($rows AS $chau)
                            if (in_array($chau['id'], $menu)) {$found=true;break;}
                }
            }
            if ($found)
                $submenu[$item['id']] = $item;
        }
        return $submenu;
    }

    function checkrole($name) // lấy menu id để kiểm tra xem có được phân quyền truy xuất module này ko
    {
        if ($_SESSION['user']['id']==1)
            return false;
        else {
            $query  = $this->db->query("SELECT id FROM menu WHERE link='$name' ");
            $temp   = $query->fetchAll(PDO::FETCH_ASSOC);
            $id = $temp[0]['id'];
            $query = $this->db->query("SELECT menu FROM users WHERE id=".$_SESSION['user']['id']);
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            $role = explode(',',$temp[0]['menu']);
            if (in_array($id,$role))
                return false;
            else
                return true;
        }
    }


    function checkright($text)  // kiem tra quyen truy xuat menu
    {
        $yes = false;
        if ($_SESSION['user']['id']==1)
            $yes = true;
        else {
          $query = $this->db->query("SELECT id FROM menu WHERE active=1 AND link LIKE '$text%' ");
          $temp = $query->fetchAll(PDO::FETCH_ASSOC);
          $conid = $temp[0]['id'];
          $query = $this->db->query("SELECT menu FROM users WHERE id=".$_SESSION['user']['id']);
          $a = $query->fetchAll(PDO::FETCH_ASSOC);
          $menu = explode(',',$a[0]['menu']);
          if (in_array($conid, $menu))
              $yes = true;
          else {
              $query = $this->db->query("SELECT id FROM menu WHERE active=1 AND parentid = $conid ");
              $rows = $query->fetchAll(PDO::FETCH_ASSOC);
              if (sizeof($rows)>0)
                  foreach ($rows AS $chau)
                      if (in_array($chau['id'], $menu)) {$yes = true; break;}
          }
        }
        return $yes;
    }

    function getfun($module)
    {
        $return = array();
        $query           = $this->db->query("SELECT id FROM menu WHERE link='$module' ");
        $row             = $query->fetchAll(PDO::FETCH_ASSOC);
        if (isset($row[0]['id'])) {
            $parid = $row[0]['id'];
            $query = $this->db->query(" SELECT * FROM menu WHERE parentid=$parid AND active=1 ORDER BY thu_tu");
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
            if (sizeof($rows)>0) {
                $uid = $_SESSION['user']['id'];
                if ($uid==1) {
                   $return = $rows;
                } else {
                    $query = $this->db->query(" SELECT menu FROM users WHERE id=$uid ");
                    $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                    if(isset($temp[0]['menu'])) {
                         $menu = explode(',',$temp[0]['menu']);
                         foreach ($rows AS $item)
                             if (in_array($item['id'], $menu))
                                 $return[$item['id']]=$item;
                    }
                }
            }
        }
        return $return;
    }

    function getmenu($uid) // lấy menu id cho user admin
    {
        $menu = '';
        if ($uid==1) {
            $query  = $this->db->query("SELECT id FROM menu WHERE active=1 ");
            $temp   = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($temp as $item) {
                $menu.= $item['id'].',';
            }
            $menu = rtrim($menu,',');
        } else {
            $query  = $this->db->query("SELECT menu FROM users WHERE id=$uid ");
            $temp   = $query->fetchAll(PDO::FETCH_ASSOC);
            $menu = $temp[0]['menu'];
        }
        return explode(',',$menu);
    }
    // function menucon($link){
    //            $query = $this->db->query("SELECT id, name, link, icon, functions FROM menu WHERE active=1 AND
    //              parentid = (SELECT id FROM menu WHERE link='$link')
    //            ORDER BY thu_tu");
    //            return $query->fetchAll(PDO::FETCH_ASSOC);
    // }



    // hien thi menu con
    function get_menucon($userid, $id)
    {
        if ($userid == 7) {
            $query = $this->db->query("SELECT id, name, link, icon, functions FROM tblmenu WHERE parentid = $id
            AND active=1 ORDER BY thu_tu");
            return $query->fetchAll();
        } else {
            $query   = $this->db->query("SELECT menu FROM tblnhanvien WHERE id = $userid ");
            $menuids = $query->fetchAll();
            $dkien   = $menuids[0]['menu'];
            if ($dkien == '')
                return false;
            else {
                $query = $this->db->query("SELECT id, name, link, icon FROM tblmenu WHERE parentid = $id
                AND id IN ($dkien) AND active=1 ORDER BY thu_tu");
                return $query->fetchAll();
            }
        }
    }

    function getmodule($name) // lấy menu id để kiểm tra xem có được phân quyền truy xuất module này ko
    {
        $query  = $this->db->query("SELECT id FROM menu WHERE link='$name' ");
        $temp   = $query->fetchAll(PDO::FETCH_ASSOC);
        return $temp[0]['id'];
    }
    function checkfun($name) // Kiem tra quyen truy xuat chuc nang them xoa sua
    {
        if ($_SESSION['user']['id']==1)
            return array("1"=>true,"2"=>true,"3"=>true);
        else {
            $fun=array("1"=>false,"2"=>false,"3"=>false);
            $query = $this->db->query("SELECT menu FROM users WHERE id=".$_SESSION['user']['id']);
            $temp = $query->fetchAll(PDO::FETCH_ASSOC);
            if (isset($temp[0]['menu']) && ($temp[0]['menu']!='')) {
                $role = explode(',',$temp[0]['menu']);
                $query = $this->db->query("SELECT id FROM menu WHERE active=1 AND link = '$name' ");
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                if (in_array($temp[0]['id'].'.1',$role))
                    $fun[1]=true;
                if (in_array($temp[0]['id'].'.2',$role))
                    $fun[2]=true;
                if (in_array($temp[0]['id'].'.3',$role))
                    $fun[3]=true;
            }
            return $fun;
        }
    }
}
?>

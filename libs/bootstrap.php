<?php

class bootstrap
{
    function __construct()
    {
        $checktoken = false;
        $this->db = new database();
        if (isset($_REQUEST['token']) && $_REQUEST['token'] != '') {
            $token = $_REQUEST['token'];
            $query = $this->db->query("SELECT COUNT(id) as total
                    FROM users WHERE tinh_trang=1 AND token='$token'");
            if ($query) {
                $temp = $query->fetchAll(PDO::FETCH_ASSOC);
                if ($temp[0]['total'] > 0)
                    $checktoken = true;
            }
        }
        $this->view = new view();
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        $module = (empty($url[0])) ? 'index' : $url[0];
        $method = (isset($url[1])) ? $url[1] : 'index';
        $arrMethod = array("login", "checkToken");
        if ($module == 'index' && in_array($method, $arrMethod)) {
            require 'controllers/' . $module . '.php';
            $controller = new $module;
            $controller->loadModel($module); // load model tương ứng
            $controller->{$method}();
        } elseif (isset($_SESSION[SID]) || $checktoken == true) {
            if (file_exists('controllers/' . $module . '.php')) {
                require 'controllers/' . $module . '.php';
                $controller = new $module;
                $controller->loadModel($module); // load model tương ứng
                if (method_exists($controller, $method)) {
                    if (isset($url[2]))
                        $controller->{$method}($url[2]);
                    else
                        $controller->{$method}();
                } else {
                    require(HEADER);
                    $this->view->thongbao = 'Không tìm thấy method!';
                    $this->view->render('common/thongbao');
                    require(FOOTER);
                }
            } else {
                require(HEADER);
                $this->view->thongbao = 'Không tìm thấy controller!';
                $this->view->render('common/thongbao');
                require(FOOTER);
            }
        } else {
            $this->view = new view();
            if (isset($_REQUEST['username'])) {
                $username = str_replace("'", "", $_REQUEST['username']);
                $password = md5(md5($_REQUEST['password']));
                $query = $this->db->query("SELECT id,menu,nhan_vien,giao_vien,nhom,
                    (SELECT ip FROM chinhanh WHERE chinhanh.id=(SELECT van_phong FROM nhanvien WHERE nhanvien.id=users.nhan_vien)) AS ip,
                    (SELECT name FROM nhanvien WHERE id=nhan_vien) as nhanvien,
                    (SELECT name FROM giaovien WHERE id=giao_vien) as giaovien
                    FROM users WHERE tinh_trang=1 AND email = '$username' AND mat_khau = '$password'");
                if ($query)
                    $userdata = $query->fetchAll(PDO::FETCH_ASSOC);
                else
                    $userdata = array();
                if (empty($userdata)) {
                    $this->view->loginfail = true;
                    $this->view->render('index/login');
                } else {
                    $userid = $userdata[0]['id'];
//                    $token = md5(time() . $userid);
//                    $this->db->query("UPDATE IGNORE users SET token='$token' WHERE id=$userid");
                    $_SESSION[SID] = true;
                    $_SESSION['user'] = $userdata[0];
//                    $_SESSION['user']['token'] = $token;
                    $_SESSION['themes'] = 'default';
                    // $model = new model();
                    // $model->chamcong();
                    header('Location: ' . URL);
                }
            } else {
                $this->view->loginfail = false;
                $this->view->render('index/login');
            }
        }
    }
}

?>

<script type="text/javascript" src="js/congdoan.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="congdoan/json" toolbar="#toolbar" pagination="true" idField="id"
    rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu"
    data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
        <tr>
            <th field="nhanvien" width="250">Nhân viên</th>
            <th field="so_phut_ms" width="100" align="center">Số phút MS</th>
            <th field="sotien_ms" width="100" align="right" formatter="CurrencyFormatted">Muộn sớm</th>
            <th field="sotien_qc" width="100" align="right" formatter="CurrencyFormatted">Quên checkout</th>
            <th field="tredeadline" width="100" align="right" formatter="CurrencyFormatted">Trễ Deadline</th>
            <th field="khongdat" width="100" align="right" formatter="CurrencyFormatted">Không đạt</th>
            <th field="x" width="100" align="center">No plan</th>
            <th field="y" width="100" align="right"  >No reply</th>
            <th field="tong" width="180" align="right" formatter="CurrencyFormatted">Tổng cộng</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <span style="color:blue; font-weight:bold; margin-right:20px">QUỸ CÔNG ĐOÀN</span>
    <input id="thang" name="thang" class="easyui-combobox" url="common/thang" valueField="id" textField="name"
      value="<?php echo date("m") ?>" />
    <input id="nam" name="nam" class="easyui-combobox" url="common/nam" valueField="id" textField="name"
      value="<?php echo date("Y") ?>" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <button class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="update()">Cập nhật</button>
</div>

<script type="text/javascript" src="js/congdoan.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="congdoan/json" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu"
       data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
    <tr>
        <th field="ngay" width="150">Ngày giờ</th>
        <th field="nhanvien" width="180">Nhân viên</th>
        <th field="dien_giai" width="120">Nội dung</th>
        <th field="thu" width="100" align="right" formatter="CurrencyFormatted">Thu</th>
        <th field="chi" width="100" align="right" formatter="CurrencyFormatted">Chi</th>
        <th field="so_du" width="100" align="right" formatter="CurrencyFormatted">Số dư</th>
        <th field="ghichu" width="280">Ghi chú</th>
<!--        <th field="hachtoan" width="80">Hạch toán</th>-->
<!--        <th field="ghi_chu" width="200">Ghi chú</th>-->
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input class="easyui-combobox" name="nhanvien" id="nhanvien" valueField="id" textField="name"
           url="common/nhanvien" prompt="Nhân viên" style="width:160px" "
    >
<!--    <input id="loaicongdoan" name="loaicongdoan"  value="0" prompt="Loại công đoàn" class="easyui-combobox" data-options="data:[{'id':'0','text':'Thu'},{'id':'1','text':'Chi'}]" valueField="id"  textField="text" style="width:150px" />-->
    <input class="easyui-combobox" name="chonthang" id="chonthang" url="common/thang"
           style="width: 100px;margin-left: 50px;"
           valueField="id" textField="name"
           value="<?=date('m');?>">
    <input class="easyui-combobox" name="chonnam" id="chonnam" url="common/nam"
           style="width: 100px;margin-left: 50px;"
           valueField="id" textField="name"
           value="<?=date('Y'); ?>">
<!--    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Từ khóa" style="width:150px" />-->
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>&nbsp;&nbsp;
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 600px; height: auto; padding: 10px;" closed="true" buttons="#dlg-buttons" modal="true" iconCls="icon-phieuthu">
    <form id="fm" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Loại :</div>
                </td>
                <td>
                    <input id="loai" name="loai" class="easyui-combobox" style="width: 200px;height:28px;"
                           data-options="valueField:'id',textField:'text',data:[{'id':'0','text':'Thu'},{'id':'1','text':'Chi'}] " />
                </td>
                <td>
                    <div>Số tiền:</div>
                </td>
                <td>
                    <input id="so_tien" name="so_tien" style="width:200px; height:28px;" class="easyui-numberbox" groupSeparator="," required />
                </td>
            </tr>
            <tr>
                <td>
                    <div>Nội dung:</div>
                </td>
                <td colspan="3">
                    <input id="dien_giai" name="dien_giai" style="width:478px; height:28px;" class="easyui-textbox" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dlg').dialog('close')">Quay lại
    </button>
</div>

<script type="text/javascript" src="js/lichhocgv1.js"></script>
<script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-groupview.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="lichhocgv/json"
       toolbar="#toolbar" fitColumns="false" singleSelect="true" nowrap="true" showFooter="false"
       data-options="border:false,fit:true,iconCls:'icon-nhaphang', view:groupview,
      groupField:'lophoc',
      groupFormatter:function(value,rows){
            var dahoc = 0;
            var conlai = 0;
            rows.forEach(function (item, index) {
                  if(item.tinh_trang==1)
                  conlai++;
                  if(item.tinh_trang>1 && item.tinh_trang<7)
                  dahoc++;
                });
          return value + ' - ' + rows.length + ' Schedule. Done:' + dahoc + '; New: '+conlai;
    }
    "  >
    <thead>
    <tr>
        <th field="id" width="120">Class</th>
        <th field="ngaygio" width="120" align="center">Date</th>
        <th field="gio" width="100" align="center">Start</th>
        <th field="thoi_luong" width="80" align="center">Interval</th>
        <th field="giora" width="100" align="center">Finish</th>
<!--        <th field="giaovien" width="160" >Teacher</th>-->
        <th field="hocvien" width="160" formatter="format_hocvien" >Student</th>
        <th field="phonghoc" width="160" >Class room</th>
        <th field="tinh_trang" width="160" align="center" formatter="tinhtrang">Status</th>
        <th field="link" formatter="format_link" align="center" width="100" >Link</th>
    </tr>
    </thead>
</table>

<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input class="easyui-datebox" id="tungay" name="tungay" style="width: 120px;height: 28px" prompt="Start date">
    <input class="easyui-datebox" id="denngay" name="denngay" style="width: 120px;height: 28px" prompt="End date">
    <input class="easyui-timespinner" name="giohoc" id="giohoc" style="width: 80px;height:28px;" prompt="Hour">
    <input type="hidden" name="giaovien" id="giaovien" value="<?=$_SESSION['user']['giao_vien'] ?>">
    <input class="easyui-combobox" name="loai" id="loai" style="width: 150px;height:28px;" prompt="Type"
           panelHeight="auto"
           data-options="data:[{'id':'1','name':'Online'},{'id':'2','name':'Offline'},{'id':'3','name':'Demo'}]" valueField="id"
           textField="name">
    <input class="easyui-combobox" name="phonghoc" id="phonghoc" style="width: 150px;height:28px;"
           url="common/phonghoc" valueField="id" textField="name" prompt="Room">
    <input class="easyui-textbox" name="tenlop" id="tenlop" style="width: 150px;height:28px;" prompt="Class name">
    <button class="easyui-linkbutton" iconCls="icon-ok" plain="false" onclick="timkiem()">Search</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
    <button class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="add()">Add</button>
    <button class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="edit()">Edit</button>
    <button class="easyui-linkbutton" iconCls="icon-no" plain="false" onclick="del()">Delete</button>
    <button class="easyui-linkbutton" iconCls="icon-reload" plain="false" onclick="change1()">Change</button>
    <button class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="edittime()">Edit time</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="cancel()">Cancel</button>

    <!--    <button class="easyui-linkbutton" iconCls="icon-phieuthu" plain="false" onclick="dayview()">Day view</button>-->
<!--    <button class="easyui-linkbutton" iconCls="icon-tip" plain="false" onclick="timeview()">Time view</button>-->
</div>

<!--<div id="dlg" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">-->
<!--    <form id="fm" method="post" enctype="multipart/form-data">-->
<!--        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">-->
<!--            <tr id="hienthithu">-->
<!--                <td>Day:</td>-->
<!--                <td>-->
<!--                    <input id="thu" name="thu" class="easyui-textbox" required="required"-->
<!--                           style="width: 200px;height:28px;" readonly/>-->
<!--                </td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Date:</td>-->
<!--                <td>-->
<!--                    <input id="ngaygio" name="ngaygio" class="easyui-datebox" required="required" style="width: 200px;height:28px;"/>-->
<!--                </td>-->
<!--            </tr><tr>-->
<!--                <td>Hour:</td>-->
<!--                <td>-->
<!--                    <input id="gio" name="gio" class="easyui-timespinner" style="width: 200px;height:28px;" required/>-->
<!--                </td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Time:</td>-->
<!--                <td>-->
<!--                    <input id="thoi_luong" name="thoi_luong" class="easyui-numberbox" style="width: 200px;height:28px;" readonly-->
<!--                           required/>-->
<!--                </td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Class:</td>-->
<!--                <td>-->
<!--                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc" style="width: 200px;height:28px;" readonly-->
<!--                           url="lichhocgv/lophoc" valueField="id" textField="name" >-->
<!--                </td>-->
<!--            </tr>-->
<!--            <tr id="suatuongtu">-->
<!--                <td>Edit multi:</td>-->
<!--                <td>-->
<!--                    <input type="checkbox" id="editmulti" name="editmulti">-->
<!--                </td>-->
<!--            </tr>-->
<!--        </table>-->
<!--    </form>-->
<!--</div>-->
<!--<div id="dlg-buttons">-->
<!--    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Save</button>-->
<!--    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Close</button>-->
<!--</div>-->

<div id="dlg" class="easyui-dialog" style="width: 600px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr id="hienthithu">
                <td>Weekday:</td>
                <td>
                    <input id="thu" name="thu" class="easyui-textbox" required="required"
                           style="width: 200px;height:28px;" readonly/>
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td>
                    <input id="ngaygio" name="ngaygio" class="easyui-datebox" required="required" style="width: 200px;height:28px;"/>
                </td>
                <td>
                    <input class="easyui-checkbox" type="checkbox" id="applyall" name="applyall"> Apply all
                </td>
            </tr><tr>
                <td>Start time:</td>
                <td>
                    <input id="gio" name="gio" class="easyui-timespinner" style="width: 200px;height:28px;" required/>
                </td>
                <td>
                    <input type="checkbox" id="applyallweekday" name="applyallweekday"> Apply all weekdays
                </td>
                <td><input type="checkbox" id="applyalltime" name="applyalltime"> Apply all times</td>
            </tr>
            <tr>
                <td>Interval:</td>
                <td>
                    <input id="thoi_luong" name="thoi_luong" class="easyui-numberbox" style="width: 200px;height:28px;" readonly
                           required/>
                </td>
            </tr>
            <input type="hidden" name="lop_hoc" id="lop_hoc" >
            <input type="hidden" name="ngaycu" id="ngaycu" >
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Save</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Close</button>
</div>


<div id="dlg-edittime" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons-edittime" modal="true">
    <form id="fm-edittime" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <input type="hidden" name="lop_hoc" id="lop_hoc2">
            <tr>
                <td>Weekday:</td>
                <td>
                    <input id="thu1" name="thu" class="easyui-textbox"
                           style="width: 200px;height:28px;" readonly/>
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td>
                    <input id="ngaygio1" name="ngaygio" class="easyui-datebox" required="required" style="width: 200px;height:28px;" readonly />
                </td>
            </tr><tr>
                <td>Start time:</td>
                <td>
                    <input id="gio1" name="gio" class="easyui-timespinner" style="width: 200px;height:28px;" readonly/>
                </td>
            </tr>
            <tr>
                <td>Interval:</td>
                <td>
                    <input id="thoi_luong1" name="thoi_luong" class="easyui-numberbox" style="width: 200px;height:28px;"
                           />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-edittime">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save(2)">Save</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-edittime').dialog('close')">Close</button>
</div>


<div id="dlg-add" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false"
     closed="true"
     buttons="#dlg-buttons-add" modal="true">
    <form id="fm-add" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Class:</td>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc1" style="width: 200px;height:28px;"
                           url="lichhocgv/lophoc" valueField="id" textField="name">
                </td>
                <input type="hidden" id="sobuoi" name="sobuoi"/>
                <input type="hidden" id="buoikm" name="buoikm"/>
                <input type="hidden" id="hocvien" name="hocvien"/>
                <input type="hidden" id="phanloai" name="phanloai"/>
            </tr>
            <tr>
                <td>Start time:</td>
                <td>
                    <input id="gio1" name="gio" class="easyui-timespinner" style="width: 200px;height:28px;" required/>
                </td>
            </tr>
            <tr>
                <td>Interval:</td>
                <td>
                    <input id="thoi_luong1" name="thoi_luong" class="easyui-numberbox" style="width: 200px;height:28px;"
                           required/>
                </td>
            </tr>
            <tr>
                <td>Start date:</td>
                <td>
                    <input id="ngaygio1" name="ngaygio" class="easyui-datebox" required="required"
                           style="width: 200px;height:28px;"/>
                </td>
            </tr>
            <tr>
                <td>Weekday:</td>
                <td>
                    <input class="easyui-combobox" name="buoihoc[]" id="buoihoc" style="width: 200px;height:28px;" required
                           multiple="true"
                           url="common/thu" valueField="id" textField="name">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-add">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="saveadd()">Save</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-add').dialog('close')">Close
    </button>
</div>

<div id="dlg-del" class="easyui-dialog" style="width: 290px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons-del" modal="true">
    <form id="fm-del" method="post" enctype="multipart/form-data" action="lichhocgv/dellich">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Class:</td>
                <td>
                    <input class="easyui-combobox" name="lophoc" id="lophoc" style="width: 200px;height:28px;" required
                           url="lichhocgv/lophoc1" valueField="id" textField="name" >
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-del">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savedel()">Ok</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-del').dialog('close')">Close</button>
</div>



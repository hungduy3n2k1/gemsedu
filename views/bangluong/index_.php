<script type="text/javascript" src="js/bangluong.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="bangluong/json" toolbar="#toolbar" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu" data-options="border:false,fit:true"
    pageSize="30">
    <thead>
        <tr>
            <th field="nhanvien" width="150">Nhân viên</th>
            <th field="luong" width="100" align="right" formatter="CurrencyFormatted">Lương HĐ</th>
            <th field="ngay_cong" width="100" align="center">Ngày công</th>
            <th field="luong_tg" width="100" align="right" formatter="CurrencyFormatted">Lương t/g</th>
            <th field="thuong" width="100" align="right" formatter="CurrencyFormatted">Thưởng</th>
            <th field="phat" width="100" align="right" formatter="CurrencyFormatted">Phạt</th>
            <th field="an_ca" width="100" align="right" formatter="CurrencyFormatted">Ăn ca</th>
            <th field="gui_xe" width="100" align="right" formatter="CurrencyFormatted">Gửi xe</th>
            <th field="thuong_ds" width="100" align="right" formatter="CurrencyFormatted">Thưởng DS</th>
            <th field="thuong_lt" width="100" align="right" formatter="CurrencyFormatted">Thưởng lễ tết</th>
            <th field="tong" width="100" align="right" formatter="CurrencyFormatted">Tổng cộng</th>
            <th field="bao_hiem" width="100" align="right" formatter="CurrencyFormatted">Bảo hiểm</th>
            <th field="tam_ung" width="100" align="right" formatter="CurrencyFormatted">Tạm ứng</th>
            <th field="thuclinh" width="100" align="right" formatter="CurrencyFormatted">Thực lĩnh</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <span style="color:blue; font-weight:bold; margin-right:20px">BẢNG LƯƠNG </span>
    <input id="thang" name="thang" class="easyui-combobox" url="common/thang?thang=<?php echo date("m",strtotime('last month')) ?>" valueField="id" , textField="name" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
        $role = explode(',',$_SESSION['user']['menu']);
        if (in_array($this->module.'.1',$role))
          echo '<button class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="add()">Lập bảng lương</button>';
        if (in_array($this->module.'.2',$role))
          echo '<button class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="edit()">Điều chỉnh</button>';
        if (in_array($this->module.'.3',$role))
          echo '<button class="easyui-linkbutton" iconCls="icon-no" plain="false" onclick="xoa()">Xóa</button>';
    ?>
    <button class="easyui-linkbutton" iconCls="icon-print" plain="false" onclick="xuatfile()">Xuất excel</button>

</div>

<div id="dlg" class="easyui-dialog" style="width: 350px; height: auto; padding:10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Chọn tháng:</td>
                <td>
                    <input id="chonthang" name="chonthang" class="easyui-combobox" url="common/thang?thang=<?php echo date("m",strtotime('last month')) ?>" valueField="id" , textField="name"
                      style="width:200px; height:25px;"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="tieptuc()">Tiếp tục</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Quay lại</button>
</div>

<div id="dlg-1" class="easyui-dialog" style="width: 370px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons-1" modal="true">
    <form id="fm-1" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Nhân viên:</td>
                <td>
                    <input id="id" name="id" type="hidden">
                    <input id="nhan_vien" name="nhan_vien" class="easyui-combobox" style="width:200px; height:25px" url="common/nhanvien" valueField="id" textField="name" readonly />
                </td>
            </tr><tr>
                <td>Thưởng doanh số:</td>
                <td>
                    <input id="thuong_ds" name="thuong_ds" class="easyui-validatebox" style="width: 197px;height:22px;border: 1px solid #6B9CDE; border-radius: 5px;" onkeyup="javascript:this.value=Comma(this.value);" />
                </td>
            </tr><tr>
                <td>Thưởng lễ tết:</td>
                <td>
                    <input id="thuong_lt" name="thuong_lt" class="easyui-validatebox" style="width: 197px;height:22px;border: 1px solid #6B9CDE; border-radius: 5px;" onkeyup="javascript:this.value=Comma(this.value);" />
                </td>
            </tr><tr>
                <td>Tạm ứng:</td>
                <td>
                    <input id="tam_ung" name="tam_ung" class="easyui-validatebox" style="width: 197px;height:22px;border: 1px solid #6B9CDE; border-radius: 5px;" onkeyup="javascript:this.value=Comma(this.value);" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-1">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-1').dialog('close')">Bỏ qua</button>
</div>

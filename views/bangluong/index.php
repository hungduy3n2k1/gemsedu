<script type="text/javascript" src="js/bangluong.js"></script>
<table id="dg" class="easyui-datagrid" style="width:100%" toolbar="#toolbar"
        url="bangluong/json" singleSelect="true" nowrap="true" idField="id" rownumbers="true" iconCls="icon-dichvu"
        fitColumns="true" rownumbers="true" showFooter="true">
    <thead>
        <tr>
            <th field="nhanvien" width="180" formatter="daduyet">Nhân viên</th>
            <th field="ngay_cong_chuan" width="100" align="center">Công chuẩn</th>
            <th field="luong" width="100" align="right" formatter="CurrencyFormatted">Lương HĐ</th>
            <th field="cham_cong" width="100" align="center">Ngày công</th>
            <th field="luongtg" width="100" align="right" formatter="CurrencyFormatted">Lương t/g</th>
            <th field="ngay_an_ca" width="100" align="center">Ngày ăn ca</th>
            <th field="anca" width="100" align="right" formatter="CurrencyFormatted">Ăn ca</th>
            <th field="gui_xe" width="100" align="right" formatter="CurrencyFormatted">Gửi xe</th>
            <th field="thuong_ds" width="100" align="right" formatter="CurrencyFormatted">Thưởng DS</th>
            <th field="thuong_lt" width="100" align="right" formatter="CurrencyFormatted">Thưởng lễ tết</th>
            <th field="thuong_khac" width="100" align="right" formatter="CurrencyFormatted">Thưởng khác</th>
            <th field="tong" width="100" align="right" formatter="CurrencyFormatted">Tổng cộng</th>
            <th field="bao_hiem" width="100" align="right" formatter="CurrencyFormatted">Bảo hiểm</th>
            <th field="tam_ung" width="100" align="right" formatter="CurrencyFormatted">Tạm ứng</th>
            <th field="thuclinh" width="120" align="right" formatter="CurrencyFormatted">Thực lĩnh</th>
        </tr>
    </thead>
</table>

<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <span style="color:blue; font-weight:bold; margin-right:20px">BẢNG LƯƠNG </span>
    <input id="thang" name="thang" class="easyui-combobox" url="common/thang" valueField="id" textField="name"
      value="<?php echo date("m") ?>" />
    <input id="nam" name="nam" class="easyui-combobox" url="common/nam" valueField="id" textField="name"
      value="<?php echo date("Y") ?>" />
    <input id="full" name="full" type="hidden" value="<?php echo $fullview ?>" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
       foreach ($this->funs as $item)
          echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 370px; height: auto; padding:10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Họ tên:</td>
                <td>
                  <input id="id" name="id" type="hidden">
                  <input id="nhan_vien" name="nhan_vien" class="easyui-combobox" style="width:200px; height:25px" url="common/nhanvien" valueField="id" textField="name" readonly />
                </td>
            </tr><tr>
                <td>Thưởng doanh số:</td>
                <td>
                    <input id="thuong_ds" name="thuong_ds" class="easyui-numberbox" style="width:200px; height:25px" groupSeparator="," />
                </td>
            </tr><tr>
                <td>Thưởng lễ tết:</td>
                <td>
                    <input id="thuong_lt" name="thuong_lt" class="easyui-numberbox" style="width:200px; height:25px" groupSeparator="," />
                </td>
            </tr><tr>
                <td>Thưởng khác:</td>
                <td>
                    <input id="thuong" name="thuong" class="easyui-numberbox" style="width:200px; height:25px" groupSeparator="," />
                </td>
            </tr><tr>
                <td>Tạm ứng:</td>
                <td>
                    <input id="tam_ung" name="tam_ung" class="easyui-numberbox" style="width:200px; height:25px" groupSeparator="," />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Cập nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Quay lại</button>
</div>

<?php
	$data = $this->data['rows'];
	require_once ROOT_DIR.'/libs/phpexcel/PHPExcel.php';
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("VDATA")
							 ->setLastModifiedBy("VDATA")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
	$sheet = $objPHPExcel->getActiveSheet ();
	//formatting
	$center=array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$sheet->getColumnDimension('A')->setWidth(6);
	$sheet->getColumnDimension('B')->setWidth(30);
	$sheet->getColumnDimension('C')->setWidth(15);
	$sheet->getColumnDimension('D')->setWidth(10);
	$sheet->getColumnDimension('E')->setWidth(15);
	$sheet->getColumnDimension('F')->setWidth(15);
	$sheet->getColumnDimension('G')->setWidth(15);
	$sheet->getColumnDimension('H')->setWidth(15);
	$sheet->getColumnDimension('I')->setWidth(15);
	$sheet->getColumnDimension('J')->setWidth(15);
	$sheet->getColumnDimension('K')->setWidth(15);
	$sheet->getColumnDimension('L')->setWidth(15);
	$sheet->getColumnDimension('M')->setWidth(15);
	$sheet->getColumnDimension('N')->setWidth(15);
	$sheet->getColumnDimension('O')->setWidth(15);
	//$sheet->getStyle("E")->getAlignment()->applyFromArray($center);
	$sheet->setCellValue('A1', 'BẢNG LƯƠNG THÁNG '.$data[0]['thang'].'/'.$data[0]['nam']);
	$sheet->mergeCells('A1:P1');
	$sheet->getStyle('A1')->getAlignment()->applyFromArray($center);
    $sheet->setCellValue('A2', 'Công ty cổ phần truyền thông số VDATA');
	$sheet->mergeCells('A2:P2');
	$sheet->getStyle('A2')->getAlignment()->applyFromArray($center);
	$sheet->setCellValue('A3', ''  );
	$sheet->mergeCells('A3:P3');
	$sheet->getStyle('A3')->getAlignment()->applyFromArray($center);
  $sheet->setCellValue('A4', '');
	$sheet->mergeCells('A4:P4');
	$sheet->setCellValue('A5','STT');
	$sheet->setCellValue('B5','Nhân viên');
  $sheet->setCellValue('C5','Công chuẩn');
	$sheet->setCellValue('D5','Lương HĐ');
	$sheet->setCellValue('E5','Ngày công');
	$sheet->setCellValue('F5','Lương t/g');
	$sheet->setCellValue('G5','Ăn ca');
	$sheet->setCellValue('H5','Gửi xe');
	$sheet->setCellValue('I5','Thưởng DS');
	$sheet->setCellValue('J5','Thưởng LT');
	$sheet->setCellValue('K5','Thưởng khác');
	$sheet->setCellValue('L5','Tổng cộng');
	$sheet->setCellValue('M5','Bảo hiểm');
	$sheet->setCellValue('N5','Tạm ứng');
	$sheet->setCellValue('O5','Thực lĩnh');
	$sheet->setCellValue('P5','Ký nhận');
	$i=6;
 	foreach($data as $row){
			$sheet->setCellValue('A' . $i, $i-5);
			$sheet->setCellValue('B' . $i, $row['nhanvien']);
			$sheet->setCellValue('C' . $i, $row['congchuan']);
			$sheet->getStyle('D' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('D' . $i, $row['luong']);
			$sheet->setCellValue('E' . $i, $row['ngaycong']);
			$sheet->getStyle('F' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('F' . $i, $row['luong_tg']);
			$sheet->getStyle('G' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('G' . $i, $row['an_ca']);
			$sheet->getStyle('H' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('H' . $i, $row['gui_xe']);
			$sheet->getStyle('I' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('I' . $i, $row['thuong_ds']);
			$sheet->getStyle('J' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('J' . $i, $row['thuong_lt']);
			$sheet->getStyle('K' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('K' . $i, $row['thuong']);
			$sheet->getStyle('L' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('L' . $i, $row['tong']);
			$sheet->getStyle('M' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('M' . $i, $row['bao_hiem']);
			$sheet->getStyle('N' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('N' . $i, $row['tam_ung']);
			$sheet->getStyle('O' . $i)->getNumberFormat()->setFormatCode('#,###');
			$sheet->setCellValue('O' . $i, $row['thuclinh']);
			$i++;
	}
	$i=$i-1;
	$sheet->getStyle("A5:P".$i)->applyFromArray(
		array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		)
	);


// Rename worksheet
$sheet->setTitle('Bảng lương');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Bảng lương tháng '.$data[0]['thang'].'-'.$data[0]['nam'].'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
//header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_end_clean();  //xóa các định dạng html trước khi ghi file excel để loại trừ lỗi format or file extension not valid  khi mở file
$objWriter->save('php://output');
exit;
?>

<?php
		require "libs/class.phpmailer.php";
		require "libs/class.smtp.php";
		$mail = new PHPMailer();
		$mail->SMTPDebug = false;
		$mail->IsSMTP();
		// $mail->Host = "smtp.gmail.com";
		$mail->Host = "smtp-relay.sendinblue.com";
		// $mail->Port = 465;
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';
		// $mail->Username = "mailsender.vietnam@gmail.com";
		$mail->Username = "info@vdata.com.vn";
		// $mail->Password = "vdata@123";
		$mail->Password = "UIhEdDsN8qnWvCzA";
		$mail->From = "mailsender.vietnam@gmail.com";
		$mail->FromName = 'VDATA - Văn phòng điện tử';
		$mail->AddAddress($this->data['email'],"VDATA");
		// $mail->AddReplyTo($this->data['email'],"Khach hang");
		$mail->WordWrap = 50;
		$mail->IsHTML(true);
		$thang = date("m",strtotime($this->data['ngay']));
		$mail->Subject = 'Phieu luong thang '.$thang;
		$noidung='Họ và tên: '.$this->data['nhanvien'];
		$noidung.='<br> Phiếu lương tháng '.$thang;
		$noidung.='<br> Tổng ngày công tháng này: '.$this->data['so_ngay'];
		$noidung.='<br><table style="border-collapse: collapse;"><tr>
			<td style="border:1px solid; padding:3px">Lương HD</td>
			<td style="border:1px solid; padding:3px">Ngày công</td>
			<td style="border:1px solid; padding:3px">Lương TG</td>
			<td style="border:1px solid; padding:3px">Thưởng</td>
			<td style="border:1px solid; padding:3px">Phạt</td>
			<td style="border:1px solid; padding:3px">Ăn ca</td>
			<td style="border:1px solid; padding:3px">Gửi xe</td>
			<td style="border:1px solid; padding:3px">Thưởng DS</td>
			<td style="border:1px solid; padding:3px">Thưởng lễ tết</td>
			<td style="border:1px solid; padding:3px">Tổng lương</td>
			<td style="border:1px solid; padding:3px">Bảo hiểm</td>
			<td style="border:1px solid; padding:3px">Tạm ứng</td>
			<td style="border:1px solid; padding:3px">Thực lĩnh</td>
		  </tr><tr>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['luong']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['ngay_cong']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['luong_tg']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['thuong']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['phat']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['an_ca']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['gui_xe']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['thuong_ds']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['thuong_lt']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['tong']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['bao_hiem']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['tam_ung']).'</td>
			<td style="border:1px solid; padding:3px">'.number_format($this->data['thuclinh']).'</td>
			</tr>
			</table>';
		$mail->Body = $noidung;
		$mail->AltBody = "Phieu luong thang ".$thang;
		$ok=$mail->Send()?true:false;
		if ($ok)
				echo '<h3><span>Đã gửi phiếu lương</span></h3>
				<p><a href="bangluong">Nhấn vào đây để quay về bảng lương</a></p>';
		else
				echo '<h3><span>Có lỗi khi gửi email phiếu lương</span></h3>
				<p><a href="bangluong">Nhấn vào đây để quay về bảng lương</a></p>';
?>

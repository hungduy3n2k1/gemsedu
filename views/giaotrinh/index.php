<script type="text/javascript" src="js/giaotrinhgv.js"></script>
<script type="text/javascript" src="public/tinymce/tinymce.min.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="giaotrinh/json" toolbar="#toolbar"
       pagination="true" idField="id" rownumbers="true" fitColumns="true"
       singleSelect="true" nowrap="true" iconCls="icon-giaotrinh" data-options="border:false,fit:true"
    pageSize="30">
    <thead>
        <tr>
<!--            <th field="ngaygio" width="120">Ngày giờ</th>-->
            <th field="level" width="250" >Chương trình</th>
            <th field="name" width="250" >Tên giáo trình</th>
<!--            <th field="phan_loai" width="240" >Phân loại</th>-->
            <th field="unit" width="100" formatter="format_zero">Unit</th>
            <th field="lesson" width="100" formatter="format_zero">Lesson</th>
            <th field="review" width="100" formatter="format_zero">Review</th>
            <th field="bonus" width="100" formatter="format_zero">Bonus</th>
            <th field="test" width="100" formatter="format_zero">Test</th>
<!--            <th field="nguoinhap" width="140"  align="center" >Người nhập</th>-->
            <th field="link" width="120"  align="center" formatter="download">Tải về</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; padding-top:4px; text-align:left">
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width: 200px;height:25px;" prompt="Từ khóa" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
        foreach ($this->funs as $item)
          echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>
<div id="dlg" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true" buttons="#dlg-buttons" modal="true" iconCls="icon-thanhpho">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Tên giáo trình:</div>
                </td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" style="width: 200px;height:28px;"  required/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Link file:</div>
                </td>
                <td>
                    <input id="link"  name="link" class="easyui-textbox" style="width: 200px;height:28px;" prompt="Link file" required/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Chương trình:</div>
                </td>
                <td>
                    <input id="level"  name="level" class="easyui-textbox" style="width: 200px;height:28px;"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Unit:</div>
                </td>
                <td>
                    <input id="unit"  name="unit" class="easyui-textbox" style="width: 200px;height:28px;"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Lesson:</div>
                </td>
                <td>
                    <input id="lesson"  name="lesson" class="easyui-textbox" style="width: 200px;height:28px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <div>Review:</div>
                </td>
                <td>
                    <input id="review"  name="review" class="easyui-textbox" style="width: 200px;height:28px;"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Bonus:</div>
                </td>
                <td>
                    <input id="bonus"  name="bonus" class="easyui-textbox" style="width: 200px;height:28px;"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Test:</div>
                </td>
                <td>
                    <input id="test"  name="test" class="easyui-textbox" style="width: 200px;height:28px;"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-nhap" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true" buttons="#dlg-buttons-nhap" modal="true" iconCls="icon-thanhpho">
    <form id="fm-nhap" method="post" enctype="multipart/form-data" action="giaotrinh/import">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Tải file:</div>
                </td>
                <td>
                    <input id="file1"  name="file1" class="easyui-filebox" style="width: 200px;height:28px;" prompt="Tải file excel..."/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-nhap">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savenhap()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-nhap').dialog('close')">Bỏ qua</button>
</div>
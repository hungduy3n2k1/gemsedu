<script type="text/javascript" src="js/bangchamcong.js"></script>
<table
    id="dg"
    style="width: 100%;"
    class="easyui-datagrid"
    url="bangchamcong/json"
    toolbar="#toolbar"
    pagination="true"
    idField="id"
    rownumbers="true"
    fitColumns="false"
    singleSelect="true"
    nowrap="true"
    iconCls="icon-thanhpho"
    data-options="border:false,fit:true"
    pageSize="50"
></table>
<div id="toolbar" style="height: 40px; padding-top: 4px;">
    <span style="font-weight: bold; color: blue;">Bảng chấm công</span>
    <input class="easyui-combobox" style="width:120px"
    url="common/thang" valueField="id" textField="name" editable="false" name="thang" id="thang" value="<?php echo date("m") ?>">
    <input class="easyui-combobox" style="width:120px"
    url="common/nam" valueField="id" textField="name" editable="false" name="nam" id="nam" value="<?php echo date("Y") ?>">
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="search()">Tìm kiếm</button>
    &nbsp;&nbsp;&nbsp;
    <?php
       foreach ($this->funs as $item)
          echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

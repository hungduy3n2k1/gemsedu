<script type="text/javascript" src="js/chamcong.js"></script>

<table id="dg" class="easyui-datagrid" url="chamcong/json" pagination="false" data-options="header:'#hh',singleSelect:true,border:false,fit:true,fitColumns:true,scrollbarSize:0">
    <thead>
        <tr>
            <th field="1" width="70">Thứ 2</th>
            <th field="2" width="70">Thứ 3</th>
            <th field="3" width="70">Thứ 4</th>
            <th field="4" width="70">Thứ 5</th>
            <th field="5" width="70">Thứ 6</th>
            <th field="6" width="70">Thứ 7</th>
            <th field="7" width="70">CN</th>
        </tr>
    </thead>
</table>
<div id="hh">
    <div class="m-toolbar">
        <div class="m-title" id="title">Chấm công <?php echo $_SESSION['user']['nhanvien']." (".date("m/Y").")" ?> </div>
    </div>
</div>

<div id="dlg" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px"
    data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm" method="post" enctype="multipart/form-data">
        <div style="margin-bottom:10px" >
            <input id="nhanvien" name="nhanvien" class="easyui-combobox" style="width:100%;" label="Nhân viên:" prompt="Nhân viên"
            url="common/nhanvien" valueField="id" textField="name" required
            value="<?php echo $_SESSION['user']['nhan_vien'] ?>" />
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-combobox" label="Tháng:" prompt="Chọn tháng" style="width:100%" name="thang" id="thang"
            url="common/thang" valueField="id" textField="name" editable="false" value="<?php echo date("m") ?>"
            required>
        </div>
        <div style="margin-bottom:10px">
            <input id="nam" name="nam" class="easyui-combobox" label="Năm:" prompt="Chọn năm" style="width:100%" editable="false"
               url="common/nam" valueField="id" textField="name" value="<?php echo date("Y") ?>"
               required  >
        </div>
    </form>
    <div class="dialog-button" style="text-align:center">
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="tieptuc()">OK</a>
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg').dialog('close')">Bỏ qua</a>
    </div>
</div>

<footer>
<div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="javascript:$('#dlg').dialog({closed:false})" style="font-size:14px">
            <img src='libs/jquery/images/search.png' width="32" height="32" />
            <br>Tìm kiếm
        </div>
    </div>



<!--
<div id="dlg-le" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px"
    data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm-le" method="post" enctype="multipart/form-data">
        <div style="margin-bottom:10px">
            <input class="easyui-datebox" label="Ngày:" prompt="Chọn ngày" style="width:100%" name="ngay_le" id="ngay_le">
        </div>
        <div style="margin-bottom:10px">
            <input id="note" name="note" class="easyui-textbox" label="Ghi chú:" prompt="Ghi chú" style="width:100%" >
        </div>
    </form>
    <div class="dialog-button" style="text-align:center">
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="nghile()">OK</a>
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg-le').dialog('close')">Bỏ qua</a>
    </div>
</div>


<div id="dlg-search" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px; top:100px" closable="false" closed="true"
    buttons="#dlg-buttons-2" modal="true">
    <form id="fm-search" method="post" enctype="multipart/form-data">
        <div style="margin-bottom:10px">
          <input class="easyui-combobox" label="Nhân viên:" prompt="Nhân viên" style="width:100%"
          url="common/nhanvien" valueField="id" textField="name" editable="false" name="nhanvien" id="nhanvien">
        </div>
        <div style="margin-bottom:10px">
          <input class="easyui-datebox" label="Tháng:" prompt="Chọn tháng" style="width:100%" name="thang" id="thang"
          editable="false" value="<?php echo date('d/m/Y') ?>">
        </div>
    </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="timkiem()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-search').dialog('close')">Bỏ qua</button>
</div> -->

<!-- <footer>
    <div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true"> -->
    <?php
        // if ($this->fun[1])
        //       echo '
        //       <div style="padding:10px">
        //           <div class="panel-header tt-inner" onClick="add()" style="font-size:14px">
        //               <img src="libs/jquery/images/add.png" width="32" height="32" />
        //               <br>Thêm
        //           </div>
        //       </div>
        //       ';
        //   if ($this->fun[2])
        //         echo '
        //         <div style="padding:10px">
        //             <div class="panel-header tt-inner" onClick="edit()" style="font-size:14px">
        //                 <img src="libs/jquery/images/edit.png" width="32" height="32" />
        //                 <br>Sửa
        //             </div>
        //         </div>
        //         ';
          // if ($this->fun[3])
          //       echo '
          //       <div style="padding:10px">
          //           <div class="panel-header tt-inner" onClick="del()" style="font-size:14px">
          //               <img src="libs/jquery/images/delete.png" width="32" height="32" />
          //               <br>Xóa
          //           </div>
          //       </div>
          //       ';
    ?>
    <!-- <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="javascript:$('#dlg-search').dialog({closed:false})" style="font-size:14px">
            <img src="libs/jquery/images/search.png" width="32" height="32" />
            <br>Tìm kiếm
        </div>
    </div> -->

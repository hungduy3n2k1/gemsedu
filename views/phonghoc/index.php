<script type="text/javascript" src="js/phonghoc.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="phonghoc/json" toolbar="#toolbar" pagination="true"
  idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-phanloai"
  data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="id" width="50" sortable="true">ID</th>
            <th field="name" width="150" sortable="true">Tên gọi</th>
            <th field="dia_chi" width="280">Địa chỉ</th>
<!--            <th field="access_id" width="180">Access ID</th>-->
<!--            <th field="access_pass" width="180">Access Pass</th>-->
            <th field="phanloai" width="180">Phân loại</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <?php
        foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên phòng học:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px;height:28px;" />
                </td>
            </tr><tr>
                <td>Địa chỉ:</td>
                <td>
                    <input id="dia_chi" name="dia_chi" class="easyui-textbox" style="width: 200px;height:28px;" />
                </td>
            </tr><tr>
                <td>Access ID:</td>
                <td>
                    <input id="access_id" name="access_id" class="easyui-textbox"  style="width: 200px;height:28px;" />
                </td>
            </tr><tr>
                <td>Access Pass</td>
                <td>
                    <input id="access_pass" name="access_pass" class="easyui-textbox"  style="width: 200px;height:28px;" />
                </td>
            </tr><tr>
                <td>Phân loại:</td>
                <td>
                    <select class="easyui-combobox" name="phan_loai" id="phan_loai" style="width: 200px;height:25px;" required>
                        <option value="0">Online</option>
                        <option value="1">Offline</option>
                    </select>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<table id="dg" style="width: 100%;" url="baocaogiaovien/json" class="easyui-datagrid" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="false"
       data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
    <thead>
    <tr>
        <!--        <th field="ma_hoc_vien" width="120">Mã học viên (Cũ)</th>-->
        <!--        <th field="id" width="70">ID</th>-->
        <th field="noidung" width="240">Nội dung báo cáo</th>
        <th field="ketqua" width="100">Kết quả</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    Từ ngày: <input id="tungay" name="tungay" class="easyui-datebox" style="width: 150px;height:28px;" prompt="Từ ngày" value=""/>&nbsp;
    Đến ngày: <input id="denngay" name="denngay" class="easyui-datebox" style="width: 150px;height:28px;" prompt="Từ ngày" value=""/>
    <!--    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width: 150px;height:28px;" prompt="Từ Khóa"/> &nbsp;&nbsp;&nbsp;-->
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
</div>
<script>
    function timkiem() {
        var tungay = $('#tungay').datebox('getValue');
        var denngay = $('#denngay').datebox('getValue');
        if ( tungay.length > 0 || denngay.length > 0)
            $('#dg').datagrid('options').url = baseUrl + '/baocaogiaovien/json?tungay=' + tungay + '&denngay=' + denngay;
        else
            $('#dg').datagrid('options').url = baseUrl + '/baocaogiaovien/json';
        $('#dg').datagrid('reload');
    }
</script>

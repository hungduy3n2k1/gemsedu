<script>
    $(function(){
        $('#dg-detail-<?php echo $this->id; ?>').datagrid({
            url: baseUrl + '/baogia/jsondetail?id=<?php echo $this->id; ?>',
        });
    });
</script>
<table id="dg-detail-<?php echo $this->id; ?>" class="easyui-datagrid"
     style="width: 100%;" pagination="false" idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true"
	   data-options="border:true,fit:true" pageSize="50" >
	<thead>
		<tr>
        <th field="hanghoa" width="300">Hàng hóa/dịch vụ</th>
        <th field="so_luong" width="120" align="center" editor="numberbox">Số lượng</th>
        <th field="donvi" width="120" align="center">Đơn vị tính</th>
        <th field="don_gia" width="120" align="right" formatter="CurrencyFormatted">Đơn giá</th>
        <th field="chiet_khau_tm" width="120" editor="numberbox" align="right" formatter="CurrencyFormatted">CKTM</th>
        <th field="chiet_khau_pt" width="120" editor="numberbox" align="center">CK %</th>
        <th field="thue_suat_vat" width="120" editor="numberbox" align="center">Thuế VAT %</th>
        <th field="thanhtien" width="120" align="right" formatter="CurrencyFormatted" >Thành tiền</th>
        <th field="tang_thang" width="120" editor="numberbox" align="center" >Tặng TGSD</th>
        <th field="tu_ngay" width="120" align="center" editor="textbox">Từ ngày</th>
        <th field="den_ngay" width="120" align="center" editor="textbox">Đến ngày</th>
        <th field="ghi_chu" width="150" editor="textbox">Ghi chú</th>
		</tr>
	</thead>
</table>

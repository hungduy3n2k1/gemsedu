<script type="text/javascript" src="public/easyui/datagrid-cellediting.js"></script>
<script type="text/javascript" src="js/baogia.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="baogia/json" toolbar="#toolbar" pagination="true"
    idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true" iconCls="icon-dichvu"
    data-options="border:false,fit:true" showFooter="true" pageSize="30">
    <thead>
        <tr>
            <th field="ngay" width="100">Ngày</th>
            <th field="sobaogia" width="100">Số báo giá</th>
            <th field="khachhang" width="250">Khách hàng</th>
            <th field="noi_dung" width="300">Nội dung</th>
            <th field="sotien" width="120" align="right" formatter="CurrencyFormatted">Số tiền</th>
            <th field="tinh_trang" width="100" formatter="tinhtrang">Tình trạng</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <span style="color:blue; font-weight:bold; margin-right:20px">Báo giá và chốt đơn hàng</span>
    Từ ngày: <input id="tungay" name="tungay" class="easyui-datebox" style="width:120px" value="<?php echo date(" d/m/Y ",strtotime('first day of this month')) ?>"/> &nbsp;&nbsp;&nbsp;
    Đến ngày: <input id="denngay" name="denngay" class="easyui-datebox" style="width:120px" value="<?php echo date(" d/m/Y ") ?>"/>
    <input id="doitac" name="doitac" class="easyui-combobox" style="width:150px;" prompt="Khách hàng"
      url="common/khachhang" valueField="id" textField="name" />
    <input id="phanloai" name="phanloai" class="easyui-combobox" style="width:150px;" prompt="Tình trạng"
      url="baogia/tinhtrang" valueField="id" textField="name" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>
<!-- Lập báo giá mới -->
<div id="dlg-add" style="width:1200px;height:450px;padding:10px" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm-add" method="post" enctype="multipart/form-data" action="baogia/add" >
        <input id="baogia" name="baogia" type="hidden">
          <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
              <tr>
                  <td>Ngày báo giá:</td>
                  <td>
                      <input id="ngaybao" name="ngaybao" class="easyui-datebox" style="width: 250px; height:28px;" readonly />
                  </td>
                  <td>Khách hàng:</td>
                  <td>
                      <input id="khachhang" name="khachhang" class="easyui-combobox" style="width:250px; height:28px;"
                       url="common/khachhang" valueField="id" textField="name" required/>
                  </td>
              </tr><tr>
                  <td>Nội dung:</td>
                  <td>
                      <input id="noidung" name="noidung" class="easyui-textbox" style="width:250px; height:28px;" >
                  </td>
                  <td>Tình trạng:</td>
                  <td>
                      <input id="tinhtrang" name="tinhtrang" class="easyui-combobox" style="width:250px; height:28px;"
                      url="baogia/tinhtrang" valueField="id" textField="name" required />
                  </td>
              </tr><tr>
                  <td>File đính kèm:</td>
                  <td>
                      <input id="dinhkem" name="dinhkem" class="easyui-textbox" style="width:250px; height:28px;" />
                  </td>
                  <td>Upload:</td>
                  <td>
                      <input id="file" name="file" class="easyui-filebox" style="width:250px; height:28px;" />
                  </td>
              </tr>
          </table>
    </form>
    <table class="easyui-datagrid" id="dg-add" style="width: 100%; height: 230px;" data-options="toolbar:toolbar"
          rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true" showFooter="true">
          <thead>
              <tr>
                  <th field="hanghoaid" width="300" hidden>ID</th>
                  <th field="hanghoa" width="300">Hàng hóa/dịch vụ</th>
                  <th field="soluong" width="120" align="center" editor="numberbox">Số lượng</th>
                  <th field="donviid" width="120" hidden>Đơn vị</th>
                  <th field="donvi" width="120" align="center">Đơn vị tính</th>
                  <th field="dongia" width="120" align="right" formatter="CurrencyFormatted">Đơn giá</th>
                  <th field="chietkhau" width="120" editor="numberbox" align="right" formatter="CurrencyFormatted">CKTM</th>
                  <th field="chietkhaupt" width="120" editor="numberbox" align="center">CK %</th>
                  <th field="thuevat" width="120" editor="numberbox" align="center">Thuế VAT %</th>
                  <th field="thanhtien" width="120" align="right" formatter="CurrencyFormatted" >Thành tiền</th>
                  <th field="tangthang" width="120" editor="numberbox" align="center" >Tặng TGSD</th>
                  <th field="tungay" width="120" align="center" editor="textbox">Từ ngày</th>
                  <th field="denngay" width="120" align="center" editor="textbox">Đến ngày</th>
                  <th field="ghichu" width="150" editor="textbox">Ghi chú</th>
                  <th field="xoa" width="50" >...</th>
              </tr>
          </thead>
    </table>
    <script>
    var toolbar = [
        {text: "Nội dung"},"-",
        {text: "Thêm",iconCls: "icon-add",handler: function () {addrow();}},
    ];
    </script>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-add').dialog('close')">Bỏ qua</button>
</div>

<!-- Form thêm sản phẩm, sửa sản phẩm trong báo giá  -->
<div id="dlg-row" class="easyui-dialog" style="width:700px;padding:10px"
        data-options="closed: true,modal:true">
        <form id="fm-row" method="post" >
          <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
              <tr>
                  <td>Dịch vụ:</td>
                  <td>
                      <input id="dich_vu" name="dich_vu" class="easyui-combobox" style="width:200px; height:28px;"
                      url="common/dichvu" valueField="id" textField="name" required />
                  </td>
                  <td>Đơn giá:</td>
                  <td>
                      <input id="don_gia" name="don_gia" class="easyui-numberbox" style="width:200px; height:28px;" groupSeparator=","  />
                  </td>
              </tr><tr>
                  <td>Chiết khấu TM:</td>
                  <td>
                      <input id="chiet_khau_tm" name="chiet_khau_tm" class="easyui-numberbox" style="width:200px; height:28px;" groupSeparator="," />
                  </td>
                  <td>Chiết khấu %:</td>
                  <td>
                      <input id="chiet_khau_pt" name="chiet_khau_pt" class="easyui-numberbox" style="width:200px; height:28px;" groupSeparator="," />
                  </td>
              </tr><tr>
                  <td>Số lượng:</td>
                  <td>
                      <input id="so_luong" name="so_luong" class="easyui-numberbox" style="width:200px; height:28px;" required />
                  </td>
                  <td>Đơn vị tính:</td>
                  <td>
                      <input class="easyui-combobox" style="width:200px; height:28px;" id="don_vi" name="don_vi"
                        required url="common/donvitinh" valueField="id" textField="name">
                  </td>
              </tr><tr>
                  <td>Thuế suất vat %:</td>
                  <td>
                      <input id="thue_suat_vat" name="thue_suat_vat" class="easyui-numberbox" style="width:200px; height:28px;" groupSeparator="," />
                  </td>
                  <td>Tặng tháng SD:</td>
                  <td>
                      <input id="tang_thang" name="tang_thang" class="easyui-numberbox" style="width:200px; height:28px;" />
                  </td>
              </tr><tr>
                  <td>Từ ngày:</td>
                  <td>
                      <input id="tungay1" name="tungay1" class="easyui-datebox" style="width:200px; height:28px;" required />
                  </td>
                  <td>Đến ngày</td>
                  <td>
                      <input id="denngay1" name="denngay1" class="easyui-datebox" style="width:200px; height:28px;" required />
                  </td>
              </tr><tr>
                  <td>Ghi chú:</td>
                  <td colspan="3">
                      <input id="ghi_chu" name="ghi_chu" class="easyui-textbox" style="width:534px; height:28px;" />
                  </td>
              </tr>
          </table>
        </form>
</div>

<!-- Sửa xem in và gửi email báo giá -->
<div id="dlg-edit" style="width:1200px;height:450px;padding:10px" closable="false" closed="true" buttons="#dlg-buttons-2" modal="true">
    <form id="fm-edit" method="post" enctype="multipart/form-data" action="baogia/update" >
        <input id="id" name="id" type="hidden">
          <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
              <tr>
                  <td>Ngày báo giá:</td>
                  <td>
                      <input id="ngay" name="ngay" class="easyui-datebox" style="width: 250px; height:28px;" readonly />
                  </td>
                  <td>Khách hàng:</td>
                  <td>
                      <input id="khach_hang" name="khach_hang" class="easyui-combobox" style="width:250px; height:28px;"
                       url="common/khachhang" valueField="id" textField="name" readonly/>
                  </td>
              </tr><tr>
                  <td>Nội dung:</td>
                  <td>
                      <input id="noi_dung" name="noi_dung" class="easyui-textbox" style="width:250px; height:28px;" >
                  </td>
                  <td>Tình trạng:</td>
                  <td>
                      <input id="tinh_trang" name="tinh_trang" class="easyui-combobox" style="width:250px; height:28px;"
                      url="baogia/tinhtrang" valueField="id" textField="name" required />
                  </td>
              </tr><tr>
                  <td>File đính kèm:</td>
                  <td>
                      <input id="dinh_kem" name="dinh_kem" class="easyui-textbox" style="width:250px; height:28px;" />
                  </td>
                  <td>Upload:</td>
                  <td>
                      <input id="file" name="file" class="easyui-filebox" style="width:250px; height:28px;" />
                  </td>
              </tr>
          </table>
    </form>
    <table class="easyui-datagrid" id="dg-edit" style="width: 100%; height: 230px;" data-options="toolbar:toolbar2"
          rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true" showFooter="true">
          <thead>
              <tr>
                  <th field="hanghoa" width="300">Hàng hóa/dịch vụ</th>
                  <th field="so_luong" width="120" align="center">Số lượng</th>
                  <th field="donvi" width="120" align="center">Đơn vị tính</th>
                  <th field="don_gia" width="120" align="right" formatter="CurrencyFormatted">Đơn giá</th>
                  <th field="chiet_khau_tm" width="120" align="right" formatter="CurrencyFormatted">CKTM</th>
                  <th field="chiet_khau_pt" width="120"  align="center">CK %</th>
                  <th field="thue_suat_vat" width="120" align="center">Thuế VAT %</th>
                  <th field="thanhtien" width="120" align="right" formatter="CurrencyFormatted" >Thành tiền</th>
                  <th field="tang_thang" width="120" align="center" >Tặng TGSD</th>
                  <th field="tungay" width="120" align="center" editor="textbox">Từ ngày</th>
                  <th field="denngay" width="120" align="center" editor="textbox">Đến ngày</th>
                  <th field="ghi_chu" width="150" >Ghi chú</th>
              </tr>
          </thead>
    </table>
    <script>
    var toolbar2 = [
        {text: "Nội dung"},
        "-",
        {text: "Thêm",iconCls: "icon-add",handler: function () {additem();}},
        {text: "Sửa",iconCls: "icon-edit",handler: function () {edititem();}},
        {text: "Xóa",iconCls: "icon-no",handler: function () {delitem();}}
    ];
    </script>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-save" onclick="update()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-excel" onclick="xuatfile();">Xuất excel</button>
    <button class="easyui-linkbutton" iconCls="icon-email" onclick="sendmail()">Gửi email</button>
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="chot()">Chốt</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-edit').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-sendmail" class="easyui-dialog" style="width:350px;padding:10px"
        data-options="
            closed: true,
            modal:true,
            iconCls: 'icon-phieuthu',
            buttons: [{
                text:'Send',
                iconCls:'icon-ok',
                handler:function(){sendmailsubmit();}
            }]
        ">
        <form id="fm-send" method="post">
          <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
              <tr>
                  <td>Email:</td>
                  <td>
                      <input id="emailtosend" name="emailtosend" style="width:250px; height:28px;" required/>
                  </td>
              </tr><tr>
                  <td>Ghi chú:</td>
                  <td>
                      <input id="note" name="note" class="easyui-textbox" style="width:250px; height:100px;" multiline="true" />
                  </td>
              </tr>
          </table>
        </form>
</div>

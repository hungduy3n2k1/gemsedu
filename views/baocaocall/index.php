<script>
    function timkiem() {
        var ngaybd = $('#ngaybd').datebox('getValue');
        var ngaykt = $('#ngaykt').datebox('getValue');
        var nhanvien = $('#nhanvien').combobox('getValue');
        if (ngaybd.length > 0 || ngaykt.length > 0 || nhanvien.length > 0)
            $('#dg').datagrid('options').url = 'baocaocall/json?tungay=' + ngaybd + '&denngay=' + ngaykt + '&nhanvien=' + nhanvien;
        else
            $('#dg').datagrid('options').url = 'baocaocall/json';
        if ($('#dlg-search').dialog)
            $('#dlg-search').dialog('close');
        $('#dg').datagrid('reload');
        // var p = $('#dg').datagrid('getPanel');  // get the panel object
        // p.panel('setTitle',dgtitle);
    }
    function format_kpi(val,row) {
           return (row.dachot/row.tongdata).toFixed(2)*100;
       }
</script>

<table id="dg" style="width: 100%;" class="easyui-datagrid" url="baocaocall/json"
       toolbar="#toolbar" pagination="true" idField="id" rownumbers="true"
       fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-baocao"
       data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
    <tr>
        <th field="nguoigoi" width="240" sortable="true">Nhân viên</th>
        <th field="tong" width="80" align="center">Tổng data</th>
        <th field="dagoi" width="80" align="center">Đã gọi</th>
        <th field="thuebao" width="80" align="center">Thuê bao</th>
        <th field="khongnghemay" width="120" align="center">Không nghe máy</th>
        <th field="khongquantam" width="120" align="center">Không quan tâm</th>
        <th field="chamsoctiep" width="120" align="center">Chăm sóc tiếp</th>
        <th field="dachot" width="80" align="center">Đã chốt</th>
<!--        <th field="x" width="80" align="center" formatter="format_kpi">KPI (%)</th>-->
    </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; padding-top:4px; text-align:left">
    <input id="nhanvien" name="nhanvien" class="easyui-combobox" url="common/nhanvien"
           style="width: 200px;height:25px;" valueField="id" textField="name" prompt="Nhân viên"/>
    Từ ngày: <input id="ngaybd" name="ngaybd" class="easyui-datebox" style="width:120px" value=""/>
    Đến ngày: <input id="ngaykt" name="ngaykt" class="easyui-datebox" style="width:120px" value=""/>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
</div>

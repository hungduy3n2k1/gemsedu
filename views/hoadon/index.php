<script type="text/javascript" src="js/hoadon.js"></script>
<table id="dg" style="width: 100%;" url="hoadon/json" class="easyui-datagrid" toolbar="#toolbar" pagination="true" idField="id" showFooter="true"
    rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true" iconCls="icon-nhaphang"
    data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="ngaygio" width="120">Ngày</th>
            <th field="name" width="120">Số hóa đơn</th>
            <th field="khachhang" width="200">Đối tác</th>
            <th field="noi_dung" width="300">Nội dung</th>
<!--            <th field="ma_so_thue" width="120">Mã số thuế</th>-->
            <th field="so_tien" width="120" align="right" formatter="CurrencyFormatted">Số tiền</th>
            <th field="thue_vat" width="120" align="right" formatter="CurrencyFormatted">Thuế VAT</th>
            <th field="tong" width="120" align="right" formatter="CurrencyFormatted">Tổng tiền</th>
            <th field="ghi_chu" width="250">Ghi chú</th>
            <th field="dinh_kem" width="150" formatter="taixuong">Xem/tải về</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    Từ ngày: <input id="tungay" name="tungay" class="easyui-datebox" style="width:120px" value="<?php echo date(" d/m/Y",strtotime('first day of this month')) ?>"/>
    Đến ngày: <input id="denngay" name="denngay" class="easyui-datebox" style="width:120px" value="<?php echo date(" d/m/Y ") ?>"/>
    <input class="easyui-textbox" name="tukhoa" id="tukhoa" prompt="Số hóa đơn" value="">
    <input class="easyui-combobox" name="phanloai" id="phanloai" valueField="id" textField="name" url="hoadon/loaihoadon"
      prompt="Phân loại" value="0">
    <input class="easyui-combobox" name="doitac" id="doitac" valueField="id" textField="name" url="hoadon/doitac"
      prompt="Nhà cung cấp/Khách hàng" >
    <!-- <input class="easyui-combobox" name="sohuu" id="sohuu" valueField="id" textField="name" url="common/khachhang?loai=2"
      prompt="doitac" > -->
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    &nbsp;&nbsp;&nbsp;
    <?php
           foreach ($this->funs as $item)
               echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
     ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 700px; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Số hóa đơn:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px;height:25px;" />
                </td>
                <td>Loại hóa đơn:</td>
                <td>
                      <input id="phan_loai" name="phan_loai" class="easyui-combobox" style="width:200px; height:25px"
                      url="hoadon/loaihoadon" valueField="id" textField="name" />
                </td>
            </tr><tr>
                <td>Ngày hóa đơn:</td>
                <td>
                    <input id="ngaygio" name="ngaygio" class="easyui-datebox" style="width: 200px;height:25px;" prompt="Ngày hóa đơn" />
                </td>
                <td><span id="txtDoitac">Khách hàng:</span></td>
                <td>
                    <input id="doi_tac" name="doi_tac" class="easyui-combobox" style="width:200px; height:25px"
                    valueField="id" textField="name" />
                </td>
            </tr><tr>
                <td>Địa chỉ:</td>
                <td>
                    <input id="dia_chi" name="dia_chi" class="easyui-textbox" style="width:200px; height:25px" />
                </td>
                <td>Mã số thuế:</td>
                <td>
                    <input id="ma_so_thue" name="ma_so_thue" class="easyui-textbox" style="width: 200px;height:25px;" />
                </td>
            </tr><tr>
                <td>Số tiền:</td>
                <td>
                    <input id="so_tien" name="so_tien" class="easyui-numberbox" groupSeparator="," style="width:200px; height:25px" />
                </td>
                <td>Thuế VAT:</td>
                <td>
                    <input id="thue_vat" name="thue_vat" class="easyui-numberbox" groupSeparator="," style="width: 200px;height:25px;" />
                </td>
            </tr><tr>
                <td>Ghi chú:</td>
                <td>
                    <input id="ghi_chu" name="ghi_chu" class="easyui-textbox" style="width: 200px;height:25px;" />
                </td>
                <td>Upload đính kèm:</td>
                <td>
                    <input class="easyui-filebox" data-options=" prompt:'Tải hóa đơn lên...'" style="width: 200px;height:25px;" name="file" id="file" >
                </td>
            </tr>
            <tr>
                <td>Link đính kèm:</td>
                <td colspan="3">
                    <input type="text" name="dinh_kem" id="dinh_kem" class="easyui-textbox" style="width: 550px;height:25px;" prompt="Link đính kèm" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">Nội dung:</td>
                <td colspan="3">
                    <input type="text" name="noi_dung" id="noi_dung" class="easyui-textbox" style="width: 550px;height:75px;" prompt="" multiple="true" multiline="true"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-chiase" class="easyui-dialog" style="width: 350px; height: auto; padding: 20px;" closable="false" closed="true"
buttons="#dlg-buttons-1" modal="true">
    <form id="fm-chiase" method="post" >
      <div style="margin-bottom:20px">
          Chia sẻ cho:
          <input class="easyui-combobox" name="nhanvien[]" style="width:100%; height:60px" url="common/nhanvien"
          valueField="id" textField="name"
          multiple="true" multiline="true" editable="false" id="nhanvien" >
      </div>
      <div style="margin-bottom:20px">
          Chuyển quyền sở hữu:
          <input class="easyui-combobox" name="nguoi_tao" style="width:100%; " url="common/nhanvien"
          valueField="id" textField="name" editable="false" id="nguoi_tao" >
      </div>
    </form>
</div>
<div id="dlg-buttons-1">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="capnhat()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-chiase').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-mk" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closable="false"
closed="true" buttons="#dlg-buttons-2" modal="true">
    <form id="fm-mk" method="post" >
      <div style="margin-bottom:20px">
          <input class="easyui-textbox" prompt="Tên đăng nhập" iconWidth="28" style="width:100%;height:34px;padding:10px;"
          id="ten_dang_nhap" name="ten_dang_nhap">
      </div>
      <div style="margin-bottom:20px">
          <input class="easyui-passwordbox" prompt="Mật khẩu hiện tại" iconWidth="28" style="width:100%;height:34px;padding:10px"
          id="mat_khau" name="mat_khau">
      </div>
      <div style="margin-bottom:20px">
          <input class="easyui-passwordbox" prompt="Mật khẩu mới" iconWidth="28" style="width:100%;height:34px;padding:10px"
          id="mat_khau_moi" name="mat_khau_moi">
      </div>
    </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="updatepass()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-mk').dialog('close')">Bỏ qua</button>
</div>

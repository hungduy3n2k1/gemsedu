<script>
    $(function(){
        $('#dg-detail-<?php echo $this->id; ?>').datagrid({
            url: baseUrl + '/hoadon/jsondetail?id=<?php echo $this->id; ?>',
        });
    });
</script>
<table id="dg-detail-<?php echo $this->id; ?>"
    style="width: 100%;" pagination="false" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true"
	data-options="border:true,fit:true" pageSize="50">
	<thead>
		<tr>
      <th field="ten_hang" width="100" >Tên hàng</th>
      <th field="don_vi_tinh" width="150">ĐVT</th>
      <th field="so_luong" width="150" sortable="true">Số lượng</th>
      <th field="don_gia" width="100" align="right" formatter="CurrencyFormatted">Đơn giá</th>
      <th field="thue_suat" width="100">Thuế suất</th>
      <th field="thanhtien" width="100" align="right" formatter="CurrencyFormatted">Thành tiền</th>
      <th field="ghi_chu" width="300">Ghi chú</th>
		</tr>
	</thead>
</table>

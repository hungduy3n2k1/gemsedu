<script type="text/javascript" src="js/doanhthu.js"></script>
<div class="easyui-accordion" fit="true" border="false">
    <div title="Doanh thu: <?php echo number_format($this->doanhthu) ?>">
                <table id="dg-doanhthu" class="easyui-datagrid" data-options="singleSelect:true,border:false,fit:true,fitColumns:false,scrollbarSize:0"
                  url="doanhthu/chitietdoanhthu?tungay=<?php echo $this->tungay ?>&denngay=<?php echo $this->denngay ?>" pagination="false" pageSize="50">
                    <thead>
                        <tr>
                        <th field="ngay" width="80">Ngày</th>
                        <th field="dien_giai" width="180">Diễn giải</th>
                        <th field="so_tien" width="80" align="right" formatter="CurrencyFormatted">Số tiền</th>
                        <th field="khachhang" width="150">Khách hàng</th>
                        </tr>
                    </thead>
                </table>
    </div>
    <div title="Chi phí: <?php echo number_format($this->chiphi) ?>">
              <table id="dg-chiphi" class="easyui-datagrid" data-options="singleSelect:true,border:false,fit:true,fitColumns:false,scrollbarSize:0"
                url="doanhthu/chitietchiphi?tungay=<?php echo $this->tungay?>&denngay=<?php echo $this->denngay?>" pagination="false" pageSize="50">
                  <thead>
                      <tr>
                      <th field="ngay" width="80">Ngày</th>
                      <th field="dien_giai" width="180">Diễn giải</th>
                      <th field="so_tien" width="80" align="right" formatter="CurrencyFormatted">Số tiền</th>
                      <th field="khachhang" width="150">Đối tác</th>
                      </tr>
                  </thead>
              </table>
    </div>
    <div title="Lợi nhuận: <?php echo number_format($this->doanhthu-$this->chiphi) ?>">
    </div>
</div>

        <!-- <div id="dlg-timkiem" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px"
            data-options="inline:true,modal:true,closed:true,closable:false">
            <form id="fm-timkiem" method="post" action="doanhthu">
                <div style="margin-bottom:10px">
                    <input class="easyui-datebox" label="Từ ngày:" prompt="Chọn ngày" style="width:100%" name="tungay" id="tungay">
                </div>
                <div style="margin-bottom:10px">
                    <input class="easyui-datebox" label="Đến ngày:" prompt="Chọn ngày" style="width:100%" name="denngay" id="denngay">
                </div>
            </form>
            <div class="dialog-button" style="text-align:center">
               <button class="easyui-linkbutton" style="width:45%;height:35px" onclick="tieptuc();$(\'#dlg-timkiem\').dialog(\'close\')">OK</button>
               <button class="easyui-linkbutton" style="width:45%;height:35px" onclick="$(\'#dlg-timkiem\').dialog(\'close\')">Bỏ qua</button>
            </div>
        </div> -->

<div id="dlg-search" class="easyui-dialog" style="width:90%; height: auto; padding: 10px; top:30px" closable="false" closed="true"
    buttons="#dlg-buttons-2" modal="true">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Nhân viên:</td>
                <td>
                    <input class="easyui-combobox" style="width:200px; height:25px" id="nhanvien" name="nhanvien"
                      valueField="id" textField="name" url="common/nhanvien" value="<?php echo $_SESSION['user']['nhan_vien'] ?>">
                </td>
            </tr>
        </table>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="search()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-search').dialog('close')">Bỏ qua</button>
</div>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <?php
        // if ($this->fun[1])
        //       echo '
        //       <div style="padding:10px">
        //           <div class="panel-header tt-inner" onClick="add()" style="font-size:14px">
        //               <img src="libs/jquery/images/add.png" width="32" height="32" />
        //               <br>Thêm
        //           </div>
        //       </div>
        //       ';
        // if ($this->fun[2])
        //       echo '
        //       <div style="padding:10px">
        //           <div class="panel-header tt-inner" onClick="edit()" style="font-size:14px">
        //               <img src="libs/jquery/images/edit.png" width="32" height="32" />
        //               <br>Sửa
        //           </div>
        //       </div>
        //       ';
        if ($this->fun[3])
              echo '
              <div style="padding:10px">
                  <div class="panel-header tt-inner" onClick="javascript:$(\'#dlg-search\').dialog({closed:false})" style="font-size:14px">
                      <img src="libs/jquery/images/search.png" width="32" height="32" />
                      <br>Tìm kiếm
                  </div>
              </div>
              ';
    ?>

<script type="text/javascript" src="js/khoahoc.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="khoahoc/json" toolbar="#toolbar" pagination="true"
  idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-phanloai"
  data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="id" width="50" >ID</th>
            <th field="name" width="250" >Tên khóa học</th>
            <th field="dongia" width="120" align="right" >Học phí</th>
            <th field="so_buoi" width="120" align="center" >Số buổi</th>
            <th field="thoi_gian" width="120" align="center" >Thời gian (tháng)</th>
            <th field="phanloai" width="180">Giáo viên</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <?php
        foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên khóa học:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px;height:28px;" />
                </td>
            </tr><tr>
                <td>Đơn giá:</td>
                <td>
                    <input id="don_gia" name="don_gia" class="easyui-textbox"  style="width: 200px;height:28px;" />
                </td>
            </tr><tr>
              <td>Số buổi:</td>
              <td>
                  <input id="so_buoi" name="so_buoi" class="easyui-textbox" style="width: 200px;height:28px;" />
              </td>
            </tr><tr>
              <td>Thời gian:</td>
              <td>
                  <input id="thoi_gian" name="thoi_gian" class="easyui-textbox" style="width: 150px;height:28px;" /> tháng
              </td>
            </tr><tr>
                <td>Giáo viên:</td>
                <td>
                    <select class="easyui-combobox" name="giao_vien" id="giao_vien" style="width: 200px;height:25px;" required>
                        <option value="1">Giáo viên nước ngoài</option>
                        <option value="2">Giáo viên Việt Nam</option>
                    </select>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

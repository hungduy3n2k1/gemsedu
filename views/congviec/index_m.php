<?php
    if(isset($this->funs[128])) 
      echo '
        <script>
        $(function() {
            $("#tasklist").datalist({
                onSelect:function(index,rec){
                    $("#dlg-check").dialog({closed:false});
                    $("#tencongviec").textbox("setValue",rec.name);
                    $("#viecid").val(rec.id);
                    $("#nguoigiao").val(rec.nguoi_giao);
                    $("#check").combobox("setValue",rec.tinh_trang);
                    $("#thoihan").datebox("setValue",rec.thoihan);
                }
            });
        });
        </script>
      ';
?>
<script>
function search() {
    var nv = $('#nv').combobox('getValue');
    var name = $('#nv').combobox('getText');
    $('#tasklist').datalist({url: 'congviec/json?nhanvien='+nv, title:name, method: 'get', groupField: 'group'});
    $('#dlg-search').dialog('close');
}

function themviec() {
    var nv = $('#nguoinhan').combobox('getValue');
    var name = $('#nguoinhan').combobox('getText');
    $('#dlg-search').dialog('close');
    $('#fm-add').form('submit',{
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#tasklist').datalist({url: 'congviec/json?nhanvien='+nv, title:name, method: 'get', groupField: 'group'});
                $('#dlg-giaoviec').dialog('close');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

function check() {
    $('#fm-check').form('submit',{
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#tasklist').datalist('reload');
                // $('#tasklist').datalist({url: 'congviec/json?nhanvien='+nv, title:name, method: 'get', groupField: 'group'});
                $('#dlg-check').dialog('close');
            } else {
                show_messager(result.msg);
            }
        }
    });
}

</script>
<div class="easyui-datalist" id="tasklist" style="width: 100%; height: 100%;"
    url="congviec/json" title="<?=$_SESSION['user']['nhanvien']?>" method="get" groupField="group"></div>

<div id="dlg-search" class="easyui-dialog" style="width:90%; height: auto; padding: 10px; top:30px" closable="false" closed="true"
    buttons="#dlg-buttons-2" modal="true" title="Tìm kiếm">
    <form id="fm-search" method="post" >
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Nhân viên:</td>
                <td>
                    <input class="easyui-combobox" id="nv" name="nv" value="<?=$_SESSION['user']['nhan_vien']?>"
                    valueField="id" textField="name" url="common/nhanvien">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="search()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-search').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-giaoviec" class="easyui-dialog" style="width:90%; height: auto; padding: 10px; top:30px" closable="false" closed="true"
    buttons="#dlg-buttons-1" modal="true" title="Giao việc">
    <form id="fm-add" method="post" action="congviec/giaoviec">
        <div style="margin-bottom:10px">
            <input class="easyui-combobox" id="nguoinhan" name="nguoinhan" label="Nhân viên:" prompt="Nhân viên" style="width:100%"
            required valueField="id" textField="name" url="common/nhanvien">
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-textbox" id="mota" name="mota" label="Công việc:" prompt="Mô tả công việc" style="width:100%;height:60px"
            multiline="true" required>
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-datebox" label="Deadline:" prompt="Hạn hoàn thành" data-options="editable:false,panelWidth:220,panelHeight:240,iconWidth:30"
            id="deadli" name="deadli"  required style="width:100%">
        </div>
    </form>
</div>
<div id="dlg-buttons-1">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="themviec()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-giaoviec').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-check" class="easyui-dialog" style="width:90%; height: auto; padding: 10px; top:30px" closable="false" closed="true"
    buttons="#dlg-buttons" modal="true" title="Giao việc">
    <form id="fm-check" method="post" action="congviec/capnhat">
        <div style="margin-bottom:10px">
            <input class="easyui-textbox" id="tencongviec" name="tencongviec" label="Công việc:" style="width:100%; height:60px"
              multiline="true" readonly>
            <input type="hidden" id="viecid" name="viecid">
            <input type="hidden" id="nguoigiao" name="nguoigiao">
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-combobox" id="check" name="check" label="Tình trạng:" editable="false" style="width:100%" panelHeight="auto"
            required valueField="id" textField="name" url="congviec/<?=($_SESSION['user']['nhan_vien']==2)?'tinhtrang':'tinhtrang2'?>">
        </div>
        <div style="margin-bottom:10px">
            <input class="easyui-datebox" label="Deadline:" data-options="editable:false,panelWidth:220,panelHeight:240,iconWidth:30"
            id="thoihan" name="thoihan"  <?=($_SESSION['user']['nhan_vien']==2)?'':'readonly'?> style="width:100%">
        </div>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="check()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-check').dialog('close')">Bỏ qua</button>
</div>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="javascript:$('#dlg-search').dialog({closed:false})" style="font-size:14px">
            <img src="libs/jquery/images/search.png" width="32" height="32" />
            <br>Tìm kiếm
        </div>
    </div>
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="javascript:$('#dlg-giaoviec').dialog({closed:false})" style="font-size:14px">
            <img src="libs/jquery/images/edit.png" width="32" height="32" />
            <br>Giao việc
        </div>
    </div>

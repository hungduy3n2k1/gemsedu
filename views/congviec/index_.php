<script type="text/javascript" src="js/congviec.js"></script>
<div class="easyui-layout" style="width: 100%; height: 100%;">
    <div data-options="region:'west',split:true,hideCollapsedContent:false,collapsed:false" title="Nhân viên" style="width: 20%;">
        <div class="easyui-datalist" id="nhanvien" style="width: 100%; height: 400px;"></div>
        <br />
        <input class="easyui-combobox" style="width: 120px; height: 25px;" id="thang" name="thang"
            valueField="id" textField="name" url="common/thang" value="<?php echo date("m") ?>" />
        <input class="easyui-combobox" style="width: 120px; height: 25px;" id="thang" name="thang"
            valueField="id" textField="name" url="common/nam" value="<?php echo date("Y") ?>" />
        <br><br>
        <button class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="giaoviec()">Giao việc</button>
    </div>
    <div data-options="region:'center',title:'Danh sách công việc'">
        <div class="easyui-datalist" id="congviec" style="width: 100%; height: 100%;"></div>
    </div>
    <div data-options="region:'east',split:true,hideCollapsedContent:false" title="Chi tiết công việc" style="width: 50%; padding: 6px;">
        <form id="fm" method="post" enctype="multipart/form-data">
            <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                <tr>
                    <td>Công việc:</td>
                    <td>
                        <input id="id" name="id" type="hidden" />
                        <input id="hancuoi" name="hancuoi" type="hidden" />
                        <input id="name" name="name" class="easyui-textbox" style="width: 200px; height: 60px;" multiline="true" />
                    </td>
                    <td>Yêu cầu:</td>
                    <td>
                        <input class="easyui-textbox" style="width: 200px; height: 60px;" id="mo_ta" name="mo_ta" multiline="true" />
                    </td>
                </tr>
                <tr>
                    <td>Thuộc nhóm:</td>
                    <td>
                        <input id="du_an" name="du_an" class="easyui-combobox" style="width: 200px; height: 25px;" url="common/duan" valueField="id" textField="name" />
                    </td>
                    <td>Tình trạng:</td>
                    <td>
                        <input class="easyui-combobox" style="width: 200px; height: 25px;" id="tinh_trang" name="tinh_trang" valueField="id" textField="name" url="congviec/tinhtrang" readonly />
                    </td>
                </tr>
                <tr>
                    <td>Ngày giao:</td>
                    <td>
                        <input id="ngaygiao" name="ngaygiao" class="easyui-datebox" style="width: 200px; height: 25px;" readonly />
                    </td>
                    <td>Ngày cập nhật:</td>
                    <td>
                        <input id="ngaycapnhat" name="ngaycapnhat" class="easyui-datebox" style="width: 200px; height: 25px;" readonly />
                    </td>
                </tr>
                <tr>
                    <td>Người giao:</td>
                    <td>
                        <input id="nguoi_giao" name="nguoi_giao" class="easyui-combobox" style="width: 200px; height: 25px;" url="common/nhanvien" valueField="id" textField="name" readonly />
                    </td>
                    <td>Người thực hiện:</td>
                    <td>
                        <input class="easyui-combobox" style="width: 200px; height: 25px;" id="nhan_vien" name="nhan_vien" valueField="id" textField="name" url="common/nhanvien" />
                    </td>
                </tr>
                <tr>
                    <td>Ngày bắt đầu:</td>
                    <td>
                        <input id="ngaybd" name="ngaybd" class="easyui-datebox" style="width: 200px; height: 25px;" />
                    </td>
                    <td>Ngày hoàn thành:</td>
                    <td>
                        <input id="ngaykt" name="ngaykt" class="easyui-datebox" style="width: 200px; height: 25px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 10px;">
                        <input
                            id="tien_do"
                            name="tien_do"
                            class="easyui-slider"
                            style="width: 100%;"
                            data-options="
                                showTip:true,
                                rule: [0,'|',25,'|',50,'|',75,'|',100]
                            "
                        />
                    </td>
                    <td colspan="2">
                        <a id="save" href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="save()">Cập nhật</a>
                        <?php
                           foreach ($this->funs as $item)
                              if ($item['name']!='Fullview')
                                  echo '<a href="javascript:void(0);" id="'.rtrim($item['link'],'()').'" class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</a> ';
                        ?>
                    </td>
                </tr>
            </table>
        </form>
        <div style="margin-bottom: 30px; width: 100%;"></div>
        <div class="easyui-datalist" id="comments" style="width: 100%; height: 230px;" data-options="toolbar:toolbar,nowrap:false"></div>
    </div>
</div>

<script type="text/javascript">
    var toolbar = [
        {
            text: "Comments",
        },
        "-",
        {
            text: "Add",
            iconCls: "icon-add",
            handler: function () {
                comment();
            },
        },
    ];
</script>

<div id="dlg-comment" class="easyui-dialog" style="width: 350px; padding: 10px; height: auto;" closable="false" closed="true" buttons="#comment-buttons" modal="true" title=" Comment" iconCls="icon-edit">
    <input class="easyui-textbox" style="width: 100%; height: 100px;" id="comment" name="comment" multiline="true" prompt="Comment here..." />
    <br><br>
    <input class="easyui-textbox" style="width: 100%; height: 50px;" id="hinhanh" name="hinhanh" multiline="true"prompt="Link ảnh hoặc file đính kèm" />
</div>
<div id="comment-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="commentsave()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-comment').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg" class="easyui-dialog" style="width: 350px; padding: 10px; height: auto;" closable="false" closed="true" buttons="#dlg-buttons" modal="true" title=" Giao việc" iconCls="icon-add">
    <form id="fm-giaoviec" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Công việc:</td>
                <td>
                    <input id="viec" name="viec" class="easyui-textbox" style="width: 200px; height: 60px;" multiline="true" required />
                </td>
            </tr>
            <tr>
                <td>Yêu cầu:</td>
                <td>
                    <input class="easyui-textbox" style="width: 200px; height: 60px;" id="yeucau" name="yeucau" multiline="true" />
                </td>
            </tr>
            <tr>
                <td>Thuộc nhóm:</td>
                <td>
                    <input id="nhom" name="nhom" class="easyui-combobox" style="width: 200px; height: 25px;" url="common/duan" valueField="id" textField="name" />
                </td>
            </tr>
            <tr>
                <td>Dealine:</td>
                <td>
                    <input id="deadline" name="deadline" class="easyui-datebox" style="width: 200px; height: 25px;" required />
                </td>
            </tr>
            <tr>
                <td>Người thực hiện:</td>
                <td>
                    <input class="easyui-combobox" style="width: 200px; height: 25px;" id="giaocho" name="giaocho" valueField="id" textField="name" url="common/nhanvien" required />
                </td>
            </tr>
            <tr>
                <td>Ngày bắt đầu:</td>
                <td>
                    <input id="start" name="start" class="easyui-datebox" style="width: 200px; height: 25px;" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="add()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-giahan" class="easyui-dialog" style="width: 300px; padding: 10px; height: auto;" closable="false" closed="true" buttons="#giahan-buttons" modal="true" title="Gia hạn deadline" iconCls="icon-hanghoa">
    Gia hạn: <input class="easyui-datebox" style="width: 200px; height: 28px;" id="ngaygiahan" name="ngaygiahan"  />
</div>
<div id="giahan-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="giahansave()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-giahan').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-danhgia" class="easyui-dialog" style="width: 300px; padding: 10px; height: auto;" closable="false" closed="true" buttons="#danhgia-buttons" modal="true" title="Gia hạn deadline" iconCls="icon-hanghoa">
    Đánh giá: <input class="easyui-combobox" style="width: 200px; height: 28px;" id="ketqua" name="ketqua"
      valueField="id" textField="name" url="congviec/hoanthanh" required />
</div>
<div id="danhgia-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="danhgiasave()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-danhgia').dialog('close')">Bỏ qua</button>
</div>

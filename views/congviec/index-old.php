<script type="text/javascript" src="js/congviec.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="congviec/json" toolbar="#toolbar" pagination="true"
    idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu"
    data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="ngaygiao" width="100">Ngày</th>
            <th field="name" width="300">Công việc</th>
            <th field="duan" width="150">Dự án</th>
            <th field="hancuoi" width="100" formatter="deadline">Deadline</th>
            <th field="mo_ta" width="300">Yêu cầu</th>
            <th field="ngay_bd" width="100" formatter="ngay" >Ngày bắt đầu</th>
            <th field="ngaykt" width="100" formatter="ngay" >Ngày hoàn thành</th>
            <th field="ketqua" width="200">Kết quả</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <span style="color:blue; font-weight:bold; margin-right:20px">CÔNG VIỆC</span>
    <input class="easyui-combobox" name="nhanvien" id="nhanvien" valueField="id" textField="name"
        url="common/nhanvien" value="<?php echo $_SESSION['user']['nhan_vien'] ?>" >
    <input class="easyui-combobox" name="tinhtrang" id="tinhtrang" valueField="id" textField="name"
        url="congviec/tinhtrang" value="1" >
    <input class="easyui-combobox" name="duan" id="duan" valueField="id" textField="name" prompt="Dự án"
        url="common/duan"  >
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
          foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 700px; height: auto; " closable="false" closed="true" buttons="#dlg-buttons" modal="true">
  <form id="fm" method="post" enctype="multipart/form-data">
    <div class="easyui-tabs" style="width:100%;">
      <div title="Công việc" style="padding:10px">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
          <tr>
              <td>Ngày giao việc:</td>
              <td>
                  <input id="ngaygiao" name="ngaygiao" class="easyui-datebox" style="width: 200px; height:25px"
                    value="<?php echo date("Y-m-d") ?>" readonly/>
              </td>
              <td>Công việc:</td>
              <td>
                  <input id="name" name="name" class="easyui-textbox" style="width: 200px; height:25px" required />
              </td>
          </tr><tr>
              <td>Dự án:</td>
              <td>
                  <input id="du_an" name="du_an" class="easyui-combobox" style="width: 200px; height:25px" url="common/duan"
                    valueField="id" textField="name" required />
              </td>
              <td>Yêu cầu:</td>
              <td>
                  <input class="easyui-textbox" style="width:200px; height:25px" id="mo_ta" name="mo_ta">
              </td>
          </tr><tr>
              <td>Giao cho:</td>
              <td>
                  <input id="nhan_vien" name="nhan_vien" class="easyui-combobox" style="width: 200px; height:25px" url="common/nhanvien"
                    valueField="id" textField="name" required/>
              </td>
              <td>Dealine:</td>
              <td>
                  <input id="hancuoi" name="hancuoi" class="easyui-datebox" style="width: 200px; height:25px" required/>
              </td>
          </tr><tr>
              <td>Ngày bắt đầu:</td>
              <td>
                  <input id="ngay_bd" name="ngay_bd" class="easyui-datebox" style="width: 200px; height:25px"  />
              </td>
              <td>Ngày kết thúc:</td>
              <td>
                  <input id="ngaykt" name="ngaykt" class="easyui-datebox" style="width: 200px; height:25px" />
              </td>
          </tr><tr>
              <td>Tình trạng:</td>
              <td>
                  <input class="easyui-combobox" style="width:200px; height:25px" id="tinh_trang" name="tinh_trang"
                    valueField="id" textField="name" url="congviec/tinhtrang" >
              </td>
              <td>Kết quả:</td>
              <td>
                    <input class="easyui-combobox" style="width:200px; height:25px" id="ket_qua" name="ket_qua"
                      valueField="id" textField="name" url="congviec/ketqua" >
              </td>
          </tr>
        </table>
      </div>
      <div title="Báo cáo tiến độ" style="height:300px" >
          <table id="dg-tiendo" class="easyui-datagrid" style="width: 100%; " toolbar="#toolbar-1" pagination="true"
              idField="id" rownumbers="true" fitColumns="false" nowrap="true" singleSelect="false"
              data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="10">
              <thead>
                  <tr>
                      <th field="ngaygio" width="150">Ngày giờ</th>
                      <th field="nhanvien" width="150">Nhân viên</th>
                      <th field="noi_dung" width="350">Comments</th>
                  </tr>
              </thead>
          </table>
          <div id="toolbar-1" style="padding-top:5px; padding-bottom:5px;">
                <input class="easyui-textbox" name="noidung" id="noidung" style="width: 620px;height:30px;" >
                <input type="hidden" name="id" id="id">
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="nhatky()"
                  style="height:30px;">OK</a>
          </div>
      </div>
    </div>
  </form>
</div>
<div id="dlg-buttons">
  <button class="easyui-linkbutton" iconCls="icon-edit" onclick="save()">Câp nhật</button>
  <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

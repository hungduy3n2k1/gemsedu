<script type="text/javascript" src="js/trainghiem.js"></script>
<script type="text/javascript">
    function tinhtrang(val, row) {
        var tinhtrang = JSON.parse('<?php echo $this->tinhtrang ?>');
        return '<div style="background-color:' + tinhtrang[val]["bg_color"] + '; color:' + tinhtrang[val]["text_color"] + '; padding:5px">' + tinhtrang[val]["name"] + '</div>';
    }
</script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="trainghiem/json" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="true" nowrap="false" iconCls="icon-baocao" singleSelect="true"
       data-options="border:false,fit:true" pageSize="50">
    <thead>
    <tr>
        <th field="id" width="50">ID</th>
        <th field="khachhang" width="250">Khách hàng</th>
        <th field="dienthoai" width="120" align="center">Số điện thoại</th>
        <th field="name" width="250">Học viên</th>
        <th field="ngaysinh" width="120">Ngày sinh</th>
        <th field="dien_thoai" width="120" align="center">Số điện thoại</th>
        <th field="nhanvien" width="200">Người chăm sóc</th>
        <th field="phanloai" width="150" align="center">Phân loại</th>
        <th field="lophoc" width="150" align="center">Lớp học</th>
        <th field="ghi_chu" width="200">Ghi chú</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:6px; text-align:left ">
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width: 150px;height:28px;" prompt="Từ khóa"/>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
</div>

<!-- Cập nhật -->
<div id="dlg" class="easyui-dialog" style="width: 800px; height: auto; padding: 10px;" closable="false"
     closed="true" buttons="#dlg-buttons-goi" modal="true" iconCls="icon-thanhpho">
    <form id="fm" method="post" action="trainghiem/update">
        <input type="hidden" id="id" name="id">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Khách hàng:</td>
                <td>
                    <input name="khach_hang" id="khach_hang" class="easyui-combobox" style="width:250px;height: 28px;"
                           valueField="id" textField="name" url="common/khachhang" readonly>
                </td>
                <td>Điện thoại:</td>
                <td>
                    <input name="dienthoai" id="dienthoai" class="easyui-textbox" style="width:250px;height: 28px;"
                           readonly>
                </td>
            </tr>
            <tr>
                <td>Học viên:</td>
                <td>
                    <input name="name" id="name" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
                <td>Ngày sinh:</td>
                <td>
                    <input name="ngaysinh" id="ngaysinh" class="easyui-datebox" style="width:250px;height: 28px;">
                </td>
            </tr>
            <tr>
                <td>Giới tính:</td>
                <td>
                    <input name="gioi_tinh" id="gioi_tinh" class="easyui-combobox" style="width:250px;height: 28px;"
                           valueField="id" textField="name" panelHeight="auto"
                           url="common/gioitinh">
                </td>
                <td>Điện thoại:</td>
                <td>
                    <input name="dien_thoai" id="dien_thoai" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
            </tr>
            <tr>
                <td>Phân loại</td>
                <td>
                    <input name="phan_loai" id="phan_loai" class="easyui-combobox" style="width:250px;height: 28px;"
                           valueField="id" textField="name" url="trainghiem/phanloaidemo" panelHeight="auto">
                </td>
                <td>Người chăm sóc:</td>
                <td>
                    <input name="nhan_vien" id="nhan_vien" class="easyui-combobox" style="width:250px;height: 28px;"
                           valueField="id" textField="name" url="common/nhanvien">
                </td>

            </tr>
            <tr>
                <td>Ghi chú:</td>
                <td>
                    <input name="ghi_chu" id="ghi_chu" class="easyui-textbox" style="width:250px;height:100px;"
                           prompt="Nhận xét về học viên" multiline="true">
                </td>
                <td colspan="2">
                    <div id="lichhoc" style="width:90%; height:100px"></div>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-goi">
    <button class="easyui-linkbutton" iconCls="icon-phieuthu" onclick="dangky()">Đăng ký</button>
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="capnhat()">Cập nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua
    </button>
</div>

<div id="dlg-them" class="easyui-dialog" style="width: 600px; padding: 10px;" closable="false"
     closed="true" buttons="#dlg-buttons-baocao" modal="true" iconCls="icon-thanhpho">
    <form id="fm-them" method="post" action="trainghiem/add">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <input class="easyui-textbox" label="Khách hàng:" labelPosition="top" style="width:250px;"
                           required id="khachhang" name="khachhang">
                </td>
                <td>
                    <input class="easyui-textbox" label="Số điện thoại:" labelPosition="top" style="width:250px;"
                           required id="sdt" name="sdt">
                </td>
            </tr>
            <tr>
                <td>
                    <input class="easyui-textbox" label="Học viên:" labelPosition="top" style="width:250px;"
                           required id="hocvien" name="hocvien">
                </td>
                <td>
                    <input class="easyui-datebox" label="Ngày sinh:" labelPosition="top" style="width:250px;"
                           id="bd" name="bd">
                </td>
            </tr>
            <tr>
                <td>
                    <input name="gioitinh" id="gioitinh" class="easyui-combobox" style="width:250px;" editable="false"
                           valueField="id" textField="name" panelHeight="auto" label="Giới tính:" labelPosition="top"
                           url="common/gioitinh">
                </td>
                <td>
                    <input class="easyui-textbox" label="Số điện thoại:" labelPosition="top" style="width:250px;"
                           id="sdthv" name="sdthv">
                </td>
            </tr>
            <tr>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc" label="Lớp học:" labelPosition="top"
                           style="width:250px;"
                           url="trainghiem/lophoc" valueField="id" textField="name">
                </td>
                <td>
                    <input class="easyui-textbox" label="Ghi chú:" labelPosition="top" style="width:250px;"
                           id="note" name="note">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-baocao">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="addsave()">Ghi lại</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-them').dialog('close')">Đóng
    </button>
</div>

<div id="dlg-dangky" class="easyui-dialog" style="width: 750px; height: 430px; padding:0px;overflow: hidden;"
     closable="false"
     closed="true" buttons="#dlg-buttons-dangky" modal="true" iconCls="icon-thanhpho" title="Đăng ký khóa học">
    <form id="fm-dangky" method="post">
        <div class="easyui-layout" style="width: 99%; height: 430px;overflow: hidden;">
            <div data-options="region:'west',split:false,title:'Thông tin đăng ký'"
                 style="width: 45%;overflow: hidden;padding: 10px;">
                <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                    <tr>
                        <td>Phân loại:</td>
                        <td>
                            <input name="loailop" id="loailopdk" class="easyui-combobox"
                                   style="width:200px;height: 28px;"
                                   valueField="id" textField="name" panelHeight="auto" panelHeight="auto"
                                   url="demo/phanloai" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>Khóa học:</td>
                        <td>
                            <input id="product" name="product" class="easyui-combobox" style="width: 200px;height:28px;"
                                   url="common/khoahoc" valueField="id" textField="name" required editable="false"
                                   required/>
                        </td>
                    </tr>
                    <tr>
                        <td>Số tiền:</td>
                        <td>
                            <input id="so_tien" name="so_tien" class="easyui-numberbox"
                                   style="width:200px;height: 28px;" groupSeparator="," required/>
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt thanh toán:</td>
                        <td>
                            <input id="dot_thanh_toan" name="dot_thanh_toan" class="easyui-combobox"
                                   style="width:200px;height: 28px;"
                                   data-options="valueField:'id',textField:'text',
                           data:[{'id':'1','text':'1 đợt'},
                                {'id':'2','text':'2 đợt'},
                                {'id':'3','text':'3 đợt'},
                                {'id':'4','text':'4 đợt'},
                                {'id':'5','text':'5 đợt'},
                                {'id':'6','text':'6 đợt'}] " required/>
                        </td>
                    </tr>
                    <tr>
                        <td>Số buổi học:</td>
                        <td>
                            <input name="buoi_hoc" id="buoi_hoc" class="easyui-numberbox"
                                   style="width:200px;height: 28px;" required>
                        </td>
                    </tr>
                    <tr>
                        <td>Số buổi KM:</td>
                        <td>
                            <input name="buoi_hoc_km" id="buoi_hoc_km" class="easyui-numberbox"
                                   style="width:200px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Ngày đăng ký:</td>
                        <td>
                            <input name="ngay_dang_ky" id="ngay_dang_ky" class="easyui-datebox"
                                   style="width:200px;height: 28px;" required>
                        </td>
                    </tr>
                </table>
            </div>
            <div data-options="region:'center',split:false,title:'Đợt thanh toán'"
                 style="width: 60%;overflow: hidden;padding: 10px;">
                <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                    <tr>
                        <td>Đợt 1:</td>
                        <td>
                            <input id="sotiendot1" name="sotiendot1" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot1" id="ngaydot1" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 2:</td>
                        <td>
                            <input id="sotiendot2" name="sotiendot2" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot2" id="ngaydot2" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 3:</td>
                        <td>
                            <input id="sotiendot3" name="sotiendot3" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot3" id="ngaydot3" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 4:</td>
                        <td>
                            <input id="sotiendot4" name="sotiendot4" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot4" id="ngaydot4" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 5:</td>
                        <td>
                            <input id="sotiendot5" name="sotiendot5" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot5" id="ngaydot5" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 6:</td>
                        <td>
                            <input id="sotiendot6" name="sotiendot6" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot6" id="ngaydot6" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>
<div id="dlg-buttons-dangky">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savedk()">Cập nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-dangky').dialog('close')">Bỏ qua
    </button>
</div>

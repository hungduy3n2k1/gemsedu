<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>VELO - Phần mềm quản lý kho và bán hàng</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		body {font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px;}
        .letter-head {width:100%; float:left; margin-bottom:20px; text-align:center}
        .indam { font-weight:bold}
		.tieude { text-align:center; margin-bottom:20px; width:100%;}
		.thongtin {margin-bottom:20px; float:left; width:100%; text-align:center  }
        .tin-left { width:50%; float:left}
        .tin-right {width:50%; float:left}
		.noidung {float:left; margin-bottom:20px; width:100%; }
		table.main {
			border-collapse: collapse; border:#666 1px solid;
			width: 100%;
		}
		table.main th, table.main td {
			text-align: left;
			padding: 8px; border: 1px solid #666;
		}
		table.main tr.title {background-color:#CCC}
		/*table.main tr:nth-child(even){background-color: #f2f2f2}*/
		.footer {float:left; margin-bottom:20px; width:100%}
		.chuky {float:left; margin-bottom:20px; width:100%}
		.noprint {float:left; text-align:center; width:100%}
		@media print{
		   .noprint{
			   display:none;
		   }
		}
    </style>
</head>
<body>

<div class="letter-head">
        <span class="indam"><?php echo $_SESSION['cuahang']['ten_cua_hang'] ?></span> <br />
        Email: <span class="indam"><?php echo $_SESSION['cuahang']['email'] ?></span> <br />
        Điện thoại: <span class="indam"><?php echo $_SESSION['cuahang']['dien_thoai'] ?></span>
</div>
<div class="tieude">
	<h2>INVOICE</h2>
</div>
<div class="thongtin">
	Số phiếu: <span class="indam"><?php echo $this->sophieu; ?></span><br />
    Ngày: <span class="indam"><?php echo date("d/m/Y"); ?></span><br />
    Nhân viên:<span class="indam"><?php echo $_SESSION['user']['name']; ?></span>  
</div> 
<div class="noidung">
	<table class="main" >
		<tr class="title">
            <th>Item</th>
            <th>Qty</th>
            <th>Rate</th>
            <th>Total</th>
      	</tr>
		<?php
        	$i = 1; 
			$tong=0;
			$data=$this->data;
            foreach ($data['rows'] as $row){
				//$thanhtien=$row['soluong']*$row['dongia'];
				$tong=$tong+$row['thanhtien'];
			?>
            <tr>
                <td style="text-align: left;"><?php echo $row['tenhang']; ?></td>
                <td style="text-align: center;"><?php echo number_format($row['soluong'],0,'.',',') ?></td>
                <td style="text-align: right;"><?php echo number_format($row['dongia'],0,'.',',') ?></td>
                <td style="text-align: right;"><?php echo number_format($row['thanhtien'],0,'.',',') ?></td>    
			</tr>
            <?php $i++; 
                        }
            ?>
           	<tr>
            	<td colspan="3" style="font-weight: bold;text-align: right;">Tổng hóa đơn</td>
              	<td style="text-align: right;font-weight: bold;"><?php echo number_format($tong,0,'.',',') ?></td>
			</tr>
		</table>
</div><!--end info-nnhapkho-->
<div class="footer">
	Hàng đã xuất xin miễn đổi trả <br>
    Cảm ơn quý khách!
</div> 
<div class="noprint">
    <button style="height:50px; padding:10px; font-size:18px" onClick="window.print();">In ra</button>
    <button style="height:50px; padding:10px; font-size:18px" 
    	onClick="window.location.href = '<?php echo URL; ?>'">Quay lại</button>
</div>
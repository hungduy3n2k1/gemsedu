<script type="text/javascript" src="js/chinhanh.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="chinhanh/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-thanhpho" data-options="border:false,fit:true"
    pageSize="50">
    <thead>
        <tr>
            <th field="name" width="250">Văn phòng</th>
            <th field="dia_chi" width="250">Địa chỉ</th>
            <th field="ip" width="150" align="center">IP</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; line-height: 30px; text-align:left">
    <span style="font-weight:bold; color:blue">Địa chỉ chấm công</span>
    <?php
          foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true" iconCls="icon-thanhpho">
    <form id="fm" method="post" >
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Văn phòng:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" style="width: 200px;height:25px;" required />
                </td>
            </tr><tr>
                <td>Địa chỉ:</td>
                <td>
                    <input id="dia_chi" name="dia_chi" class="easyui-textbox" style="width: 200px;height:25px;"/>
                </td>
            </tr><tr>
                <td>IP:</td>
                <td>
                    <input id="ip" name="ip" class="easyui-textbox" style="width: 200px;height:25px;" required />
                </td>
            </tr>
        </table>
        <p id="progress" style="color:red"><?php echo $_SERVER["REMOTE_ADDR"] ?></p>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>


<link href="libs/dropzone/dropzone.css" type="text/css" rel="stylesheet"/>
<script src="libs/dropzone/dropzone.js"></script>
<div class="easyui-layout" style="width:100%;height:100%;">
    <div data-options="region:'west',split:true" title="Chức năng" style="width:18%; padding:0px">
        <!--        <div data-options="region:'west',split:true" title="Chức năng" style="width:100%; padding:0px">-->
        <div class="easyui-panel" style="width:100%; height:100%">
            <div class="easyui-panel" style="height:45%" width="100%" title="Folder">
                <ul id="tt" class="easyui-tree" url="common/folder">

                </ul>
            </div>
            <div class="easyui-panel" style="height:55%; width:100%; padding:10px" title="Chức năng">
                <?php
                foreach ($this->funs as $item)
                    echo '<button style="width:90%" class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button><br><br>';
                ?>
            </div>
        </div>

        <!--        </div>-->
    </div>
    <div data-options="region:'center',title:'Tài liệu',iconCls:'icon-ok'">
        <table id="dg" style="width: 80%;" class="easyui-datagrid" url="vanban/json" pagination="true" idField="id"
               rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-vanban"
               data-options="border:false,fit:true" toolbar="#toolbar"
               pageSize="30">
            <thead>
            <tr>
                <th field="ngay" width="100" sortable="true">Ngày tạo</th>
                <th field="name" width="550" sortable="true">Tên tài liệu</th>
                <th field="nhanvien" width="150" sortable="true">Người nhập</th>
                <th field="filename" width="120" align="center" formatter="formatlink">...</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
<div id="toolbar" style="height:40px; padding-top:4px; text-align:left">
    <input id="tungay" name="tungay" class="easyui-datebox" style="width: 150px;"
           value="<?php echo date('d/m/Y', strtotime('first day of this month')) ?>"/>
    <input id="denngay" name="denngay" class="easyui-datebox" style="width: 150px;"
           value="<?php echo date('d/m/Y') ?>"/>
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Tên tài liệu" style="width:250px"/>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
</div>
<div id="dlg" class="easyui-dialog" style="width: 400px; height: auto; padding: 10px;" closable="false" closed="true"
     buttons="#dlg-buttons" modal="true" iconCls="icon-thanhpho">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên tài liệu:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" style="width:250px" required/>
                </td>
            </tr>
            <tr>
                <td>Tên folder:</td>
                <td>
                    <input id="folder" name="folder" class="easyui-combotree" url="common/folder" readonly
                           style="width: 250px;" valueField="id" textField="name" prompt="Chọn folder"/>
                </td>
            </tr>
            <tr>
                <td>Upload file mới:</td>
                <td>
                    <input class="easyui-filebox" data-options="prompt:'Tải file mới lên...'"
                           style="width:250px" id="file" name="file" required>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua
    </button>
</div>

<div id="dlg-phanloai" class="easyui-dialog" style="width: 370px; height: auto; padding: 10px;" closed="true"
     buttons="#dlg-buttons-phanloai" modal="true">
    <form id="fm-phanloai" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Folder cha:</div>
                </td>
                <td>
                    <input id="thumuccha" name="thumuccha" type="hidden">
                    <input id="parentid" name="parentid" class="easyui-combotree"
                           data-options="url:'common/thumuc',method:'get'" style="width:200px;height:25px;" readonly
                           prompt="Menu cha">
                </td>
            </tr>
            <tr>
                <td>
                    <div>Tên folder:</div>
                </td>
                <td>
                    <input id="name" name="name" data-options="required:true" class="easyui-textbox"
                           style="width: 200px;height:25px;"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Thứ tự:</div>
                </td>
                <td>
                    <input id="thu_tu" name="thu_tu" class="easyui-textbox" style="width: 200px;height:25px;"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-phanloai">
    <button class="easyui-linkbutton c6" iconCls="icon-ok" onclick="savephanloai()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-phanloai').dialog('close')">
        Hủy
    </button>
</div>

<div id="dlg-editfolder" class="easyui-dialog" style="width: 370px; height: auto; padding: 10px;" closed="true"
     buttons="#dlg-buttons-editfolder" modal="true">
    <form id="fm-editfolder" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Tên folder:</div>
                </td>
                <td>
                    <input id="foldername" name="foldername" data-options="required:true" class="easyui-textbox"
                           style="width: 200px;height:25px;"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Thứ tự:</div>
                </td>
                <td>
                    <input id="thu_tu1" name="thu_tu1" class="easyui-textbox" style="width: 200px;height:25px;"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-editfolder">
    <button class="easyui-linkbutton c6" iconCls="icon-ok" onclick="editfolder()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-editfolder').dialog('close')">
        Hủy
    </button>
</div>


<div id="dlg-multiupload" class="easyui-dialog" style="width: 500px; height: 300px; padding: 10px;"
     closable="true" closed="true" modal="true" iconCls="icon-thanhpho">
    <div style="height:50px">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên folder:</td>
                <td>
                    <input id="folderm" name="folderm" class="easyui-combotree" url="common/folder" readonly
                           style="width: 250px;" valueField="id" textField="name" prompt="Chọn folder"/>
                </td>
            </tr>
        </table>
    </div>
    <form id="upload" action="vanban/uploadm" class="dropzone" style="height:100px;display: none;border:1px dashed">
        <input id="idfolder" name="idfolder" type="hidden">
        <input id="thumucm" name="thumucm" type="hidden">
        <input id="thumuccham1" name="thumuccham1" type="hidden">
    </form>
    <div style="height:50px;padding-left: 200px;">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="javacript:window.location.reload()">
                        Ok
                    </button>
                </td>
            </tr>
        </table>
    </div>
</div>

<div id="dlg-bieumau" class="easyui-dialog" style="width: 400px; height: auto; padding: 10px;"
     closable="false" closed="true" buttons="#dlg-buttons-1" modal="true" iconCls="icon-thanhpho">
    <form id="fm-bieumau" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Chọn biểu mẫu:</td>
                <td>
                    <input id="bieumau" name="bieumau" class="easyui-combobox" style="width:250px"
                           url="common/bieumau" valueField="filename" textField="name" required/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-1">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="download()">Tải về</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-bieumau').dialog('close')">Bỏ
        qua
    </button>
</div>
<script type="text/javascript" src="js/vanban.js" xmlns="http://www.w3.org/1999/html"></script>

<?php
//$sql = new Model();
?>
<!-- <script type="text/javascript" src="<?php echo URL; ?>/public/scripts/xuatnhapton.js"></script> -->
<div class="easyui-panel" style="height:100%;" data-options="footer:'#ft'">
    <div class="nhapkho">
        <div class="top-nhapkho">
            <div class="top-left">
                <p><span>CÔNG TY CỔ PHẦN TRUYỀN THÔNG SỐ VDATA</span></p>
                <p><span>Địa chỉ: </span>P.2909, N02, New Horizon City, 87 Lĩnh Nam, Hoàng Mai, Hà Nội</p>
                <p><span>Điện thoại: </span>(024) 3990 9643 * <span>Email: </span>info@vdata.com.vn</p>
            </div><!--end top-left-->
        </div><!--end top-nhapkho-->
        <div class="middle-nhapkho">
            <h2>BÁO CÁO KPI</h2>
            <p>Tháng: <?php echo $this->data[0]['thang']; ?></p>
            <p>Nhân viên: <?php echo $this->data[0]['nhanvien'] ?></p>
        </div><!--end middle-nhapkho-->
        <div class="content-nhapkho">
            <div class="info-nhapkho">
                <table>
                    <thead>
                        <tr class="title">
                            <th>STT</th>
                            <th>Chỉ tiêu KPI</th>
                            <th>Giá trị</th>
                            <th>Điểm số</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr>
                          <td style="text-align: center;"><?php echo 1 ?></td>
                          <td style="text-align: left;"><?php echo 'Khối lượng công việc' ?></td>
                          <td style="text-align: center;"><?php echo $this->data[0]['total_job'] ?></td>
                          <td style="text-align: center;">
                              <?php
                                  if ($this->data[0]['total_job']>60)
                                    $tong=1;
                                  elseif ($this->data[0]['total_job']>40)
                                    $tong=0.5;
                                  else
                                    $tong=0;
                                  echo $tong; ?>
                          </td>
                      </tr>
                      <tr>
                          <td style="text-align: center;"><?php echo 2 ?></td>
                          <td style="text-align: left;"><?php echo 'Chủ động lập kế hoạch' ?></td>
                          <td style="text-align: center;"><?php $temp=ROUND($this->data[0]['post_job']/$this->data[0]['total_job'],2)*100;
                            echo $temp.'%'; ?></td>
                          <td style="text-align: center;">
                            <?php
                                if ($temp>80)
                                  $plan=1;
                                elseif ($temp>60)
                                  $plan=0.5;
                                else
                                  $plan=0;
                                echo $plan; ?>
                          </td>
                      </tr>
                      <tr>
                          <td style="text-align: center;"><?php echo 3 ?></td>
                          <td style="text-align: left;"><?php echo 'Tỷ lệ hoàn thành' ?></td>
                          <td style="text-align: center;"><?php $temp=ROUND($this->data[0]['completed_job']/$this->data[0]['total_job'],2)*100;
                            echo $temp.'%'; ?></td>
                          <td style="text-align: center;">
                            <?php
                                if ($temp>90)
                                  $comp=2;
                                elseif ($temp>80)
                                  $comp=1.5;
                                elseif ($temp>70)
                                  $comp=1;
                                elseif ($temp>60)
                                    $comp=0.5;
                                else
                                  $comp=0;
                                echo $comp; ?>
                          </td>
                      </tr>
                      <tr>
                          <td style="text-align: center;"><?php echo 4 ?></td>
                          <td style="text-align: left;"><?php echo 'Tỷ lệ trễ deadline' ?></td>
                          <td style="text-align: center;"><?php $temp=ROUND($this->data[0]['deadline_job']/$this->data[0]['total_job'],2)*100;
                            echo $temp.'%'; ?></td>
                          <td style="text-align: center;">
                            <?php
                                if ($temp<5)
                                  $dead=2;
                                elseif ($temp<10)
                                  $dead=1.5;
                                elseif ($temp<15)
                                  $dead=1;
                                elseif ($temp<20)
                                  $dead=0.5;
                                else
                                  $dead=0;
                                echo $dead; ?>
                          </td>
                      </tr><tr>
                          <td style="text-align: center;"><?php echo 5 ?></td>
                          <td style="text-align: left;"><?php echo 'Tỷ lệ không đạt' ?></td>
                          <td style="text-align: center;"><?php $temp=ROUND($this->data[0]['fail_job']/$this->data[0]['total_job'],2)*100;
                            echo $temp.'%'; ?></td>
                          <td style="text-align: center;">
                            <?php
                                if ($temp<5)
                                  $fail=2;
                                elseif ($temp<10)
                                  $fail=1.5;
                                elseif ($temp<15)
                                    $fail=1;
                                elseif ($temp<20)
                                  $fail=0.5;
                                else
                                  $fail=0;
                                echo $fail; ?>
                          </td>
                     </tr><tr>
                            <td style="text-align: center;"><?php echo 6 ?></td>
                            <td style="text-align: left;"><?php echo 'Đạt mục tiêu' ?></td>
                            <td style="text-align: center;"></td>
                            <td style="text-align: center;"><?php echo $this->data[0]['muc_tieu'] ?></td>
                      </tr><tr>
                          <td style="text-align: center;"><?php echo 7 ?></td>
                          <td style="text-align: left;"><?php echo 'Điểm tích cực' ?></td>
                          <td style="text-align: center;"></td>
                          <td style="text-align: center;"><?php
                              if ($this->data[0]['tich_cuc']==1)
                                $tichcuc=0.5;
                              else
                                $tichcuc=0;
                              echo $tichcuc; ?></td>
                      </tr><tr>
                          <td style="text-align: center;"><?php echo 8 ?></td>
                          <td style="text-align: left;"><?php echo 'Điểm trách nhiệm' ?></td>
                          <td style="text-align: center;"></td>
                          <td style="text-align: center;"><?php
                              if ($this->data[0]['trach_nhiem']==1)
                                $trachnhiem=0.5;
                              else
                                $trachnhiem=0;
                              echo $trachnhiem; ?></td>
                      </tr>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" style="font-weight: bold;text-align: right;">Tổng điểm:</td>
                            <td style="text-align: center;font-weight: bold;"><?php echo $tong+$plan+$comp+$dead+$fail+$this->data[0]['muc_tieu']+$tichcuc+$trachnhiem ?></td>
                        </tr>
                    </tfoot>
                </table>

                <textarea cols="95" style="margin-top:20px; padding:10px;">Ghi chú: <?php echo $this->data[0]['ghi_chu'] ?></textarea>
            </div><!--end info-nnhapkho-->
        </div><!--end content-nhapkho-->
    </div><!--end nhapkho-->
</div>
<div id="ft" style="padding:5px;text-align: center;">
    <button class="easyui-linkbutton" iconCls="icon-print" onclick="xuatfile('<?php echo $this->tungay ?>', '<?php echo $this->denngay ?>')">In báo cáo</button>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" onclick="window.location.href = 'kpi'">Quay lại</a>
</div>

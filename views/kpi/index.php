<script type="text/javascript" src="js/kpi.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="kpi/json" toolbar="#toolbar" idField="id"
    rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true" iconCls="icon-dichvu"
    data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="name" width="300">Nhân viên</th>
            <th field="tong" width="100" align="center">Tổng số việc</th>
            <th field="hoanthanh" width="100" align="center">Hoàn thành</th>
            <th field="khongdat" width="100" align="center">Không đạt</th>
            <th field="tre" width="100" align="center">Trễ deadline</th>
            <th field="tondong" width="100" align="center">Tồn đọng</th>
            <th field="kpi" width="100" align="center">KPI</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <!-- <input class="easyui-combobox" name="nhanvien" id="nhanvien" valueField="id" textField="name"
        url="common/nhanvien" value="<?php echo $_SESSION['user']['nhan_vien'] ?>" > -->
    Từ ngày: <input id="ngaybd" name="ngaybd" class="easyui-datebox" style="width:120px" value="<?php echo date(" d/m/Y ",strtotime('first day of this month')) ?>"/>
    Đến ngày: <input id="ngaykt" name="ngaykt" class="easyui-datebox" style="width:120px" value="<?php echo date(" d/m/Y ") ?>"/>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <!-- <button class="easyui-linkbutton" iconCls="icon-size" plain="false" onclick="baocao()">Báo cáo</button> -->
</div>

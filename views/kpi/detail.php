<script>
    $(function(){
        $('#dg-detail-<?php echo $this->index; ?>').datagrid({
            url: 'kpi/jsondetail?nhanvien=<?php echo $this->index; ?>&ngaybd=<?php echo $this->ngaybd; ?>&ngaykt=<?php echo $this->ngaykt; ?>'
        });
    });
</script>
<table id="dg-detail-<?php echo $this->index; ?>"
    style="width: 100%;" pagination="false" idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true"
	  data-options="border:true,fit:true" pageSize="50">
	  <thead>
		<tr>
        <th field="ngaygiao" width="100">Ngày giao</th>
        <th field="nguoigiao" width="100">Người giao</th>
        <th field="name" width="300">Công việc</th>
        <th field="ngaycapnhat" width="300">Ngày cập nhật</th>
        <th field="tinhtrang" width="200">Kết quả</th>
		</tr>
	  </thead>
</table>

<style type="text/css">
    .datagrid-header .datagrid-cell {
        line-height: normal;
        height: auto;
        white-space: inherit;
        text-align: center !important;
    }
</style>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="bangluonggv/json" toolbar="#toolbar" pagination="false"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-phanloai"
       data-options="border:false,fit:true" pageSize="30">
    <thead>
    <tr>
        <th field="giaovien" width="220">Giáo viên</th>
        <th field="kid" width="80" formatter="format_zero">Kid</th>
        <th field="giao_tiep" width="80" formatter="format_zero">Giao tiếp</th>
        <th field="luong_kid" width="80" formatter="format_zero">Lương Kid<br>Cơ bản</th>
        <th field="luong_giao_tiep" width="80" formatter="format_zero">Lương giao tiếp<br>Cơ bản</th>
        <th field="demo_huy" width="80" formatter="format_zero">Demo hủy</th>
        <th field="demo_hoan_thanh" width="100" formatter="format_zero">Demo HT</th>
        <th field="demo_chot" width="100" formatter="format_zero">Demo chốt</th>
        <th field="demo_coc" width="100" formatter="format_zero">Demo cọc</th>
        <th field="luongkid" width="100" align="right" formatter="format_zero">Lương kid</th>
        <th field="luonggt" width="100" align="right" formatter="format_zero">Lương giao tiếp</th>
        <th field="luongdemo" width="100" align="right" formatter="format_zero">Lương demo</th>
        <th field="luongtong" width="100" align="right" formatter="format_zero">Tổng lương</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input id="thang" name="thang" class="easyui-combobox" url="common/thang" valueField="id" textField="name"
           value="<?php echo date("m") ?>" />
    <input id="nam" name="nam" class="easyui-combobox" url="common/nam" valueField="id" textField="name"
           value="<?php echo date("Y") ?>" />
    <input class="easyui-textbox" name="giaovien" id="giaovien" style="width: 220px;"
           prompt="Tên giáo viên" >
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <button class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="lapbang()">Lập bảng</button>
</div>
<script>
    function timkiem() {
        var thang = $('#thang').datebox('getValue');
        var nam = $('#nam').datebox('getValue');
        var giaovien = $('#giaovien').textbox('getValue');
        if (thang.length > 0 || nam.length > 0 || giaovien.length > 0)
            $('#dg').datagrid('options').url = 'bangluonggv/json?thang=' + thang + '&nam=' + nam + '&giaovien=' + giaovien;
        else
            $('#dg').datagrid('options').url = 'bangluonggv/json';
        if ($('#dlg-search').dialog)
            $('#dlg-search').dialog('close');
        $('#dg').datagrid('reload');
    }
    function lapbang() {
        var thang = $('#thang').datebox('getValue');
        var nam = $('#nam').datebox('getValue');
        $.post('bangluonggv/lapbang', {thang: thang,nam:nam}, function(result) {
            if (result.success) {
                show_messager(result.msg);
                $('#dg').datagrid('options').url = 'bangluonggv/json?thang='+thang+'&nam='+nam;
                $('#dg').datagrid('reload');
            } else {
                show_messager(result.msg);
            }
        }, 'json');
    }
    function format_zero(val,row) {
        if(val!='0')
            return Comma(val);
        else return '';
    }
</script>
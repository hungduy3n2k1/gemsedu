<script type="text/javascript" src="js/trinhdo.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="trinhdo/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-phanloai" data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="name" width="200">Tình trạng</th>
            <th field="ghi_chu" width="480">Mô tả</th>
            <th field="bg_color" width="200">Mầu nền</th>
            <th field="text_color" width="200">Mầu chữ</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <?php
       foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 370px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tình trạng:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 250px;height:28px;" />
                </td>
            </tr><tr>
                <td>Mô tả:</td>
                <td>
                    <input class="easyui-textbox" multiline="true" style="width:250px; height:60px" id="ghi_chu" name="ghi_chu">
                </td>
            </tr><tr>
                <td>Mầu nền:</td>
                <td>
                    <input class="easyui-textbox" style="width:250px; height:28px" id="bg_color" name="bg_color">
                </td>
            </tr><tr>
                <td>Mầu chữ:</td>
                <td>
                    <input class="easyui-textbox" style="width:250px; height:28px" id="text_color" name="text_color">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<script>
    $(function () {
        $('#themes').combobox({
            onChange:function (val) {
                $.post("themes/changethemes", {themes: val},
                    function (result) {
                        location.reload();
                    }, "json");
            }
        });
    })
</script>
<div id="dlg" class="easyui-dialog" title="Đổi giao diện phần mềm" style="width: 340px; height: auto; padding: 10px;" closable="true" closed="false" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Chọn giao diện:</td>
                <td>
                    <input id="themes" name="themes" class="easyui-combobox" url="common/themes" editable="false" value="<?= $_SESSION['themes'] ?>"
                           style="width: 200px;height: 28px;" valueField="id" textField="name" prompt="Chọn giao diện"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

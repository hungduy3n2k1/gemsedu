<script type="text/javascript" src="js/nhacungcap.js"></script>
<table id="dg" style="width: 100%;" url="nhacungcap/json" toolbar="#toolbar" class="easyui-datagrid" pagination="true"
    idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="false" showFooter="true"
    data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="50">
    <thead>
        <tr>
            <th field="name" width="150">Tên nhà cung cấp</th>
            <th field="ma_so" width="100" align="center">Mã số thuế</th>
            <th field="van_phong" width="360">Địa chỉ văn phòng</th>
            <th field="dien_thoai" width="100" align="center">Điện thoại</th>
            <th field="email" width="240">Email</th>
            <th field="website" width="180">Website</th>
            <th field="ghi_chu" width="180">Ghi chú</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <span style="color:blue; font-weight:bold; margin-right:20px">DANH SÁCH NHÀ CUNG CẤP</span>
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Từ khóa" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button> &nbsp;&nbsp;&nbsp;&nbsp;
    <?php
          foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>
<div id="dlg" class="easyui-dialog" style="width: 1000px; padding:10px; top:60px" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" >
          <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên viết tắt:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px;height:30px;" />
                </td>
                <td>Tên đầy đủ:</td>
                <td>
                    <input id="ten_day_du" name="ten_day_du" class="easyui-textbox" style="width: 200px;height:30px;" />
                </td>
                <td>Phân loại:</td>
                <td>
                    <input id="loai" name="loai" class="easyui-combobox" style="width: 200px;height:30px;" prompt="Doanh nghiệp, cá nhân"
                      valueField="id" textField="name" url="khachhang/loai" panelHeight="auto" />
                </td>
            </tr><tr>
                <td>Địa chỉ văn phòng:</td>
                <td>
                    <input id="van_phong" name="van_phong" class="easyui-textbox" style="width: 200px;height:30px;" />
                </td>
                <td>Số điện thoại:</td>
                <td>
                    <input id="dien_thoai" name="dien_thoai" class="easyui-textbox" style="width: 200px;height:30px;" />
                </td>
                <td>Email:</td>
                <td>
                    <input id="email" name="email" class="easyui-textbox" style="width: 200px;height:30px;" />
                </td>
            </tr><tr>
                <td>Website:</td>
                <td>
                    <input id="website" name="website" class="easyui-textbox" style="width: 200px;height:30px;" />
                </td>
                <td>Người đại diện:</td>
                <td>
                    <input id="dai_dien" name="dai_dien" class="easyui-textbox" style="width: 200px;height:30px;" />
                </td>
                <td>Chức vụ:</td>
                <td>
                    <input id="chuc_vu" name="chuc_vu" class="easyui-textbox" style="width: 200px;height:30px;" />
                </td>
           </tr><tr>
                <td>Địa chỉ ĐKKD:</td>
                <td>
                    <input id="dia_chi" name="dia_chi" class="easyui-textbox" style="width: 200px;height:30px;" />
                </td>
                <td>Mã số thuế:</td>
                <td>
                    <input id="ma_so" name="ma_so" class="easyui-textbox" prompt="Mã số thuế hoặc số CMND" style="width: 200px;height:30px;" />
                </td>
                <td>Người phụ trách:</td>
                <td>
                    <input class="easyui-combobox" name="phu_trach" id="phu_trach" url="common/nhanvien" valueField="id" textField="name"
                    style="width: 200px;height:30px;" >
                </td>
            </tr><tr>
                <td>Ghi chú:</td>
                <td colspan="3">
                    <input class="easyui-textbox" style="width:500px; height:30px" id="ghi_chu" name="ghi_chu">
                </td>
            </tr>
          </table>
          <br>
          <input type="hidden" id="contact" name="contact">
          <table id="dg-lienhe" style="width: 100%; max-height:180px;" class="easyui-datagrid"
            idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true"
            data-options="border:true,fit:true,iconCls:'icon-nhaphang'" >
              <thead>
                  <tr>
                      <th field="hoten" width="150">Họ tên</th>
                      <th field="chucvu" width="100">Chức vụ</th>
                      <th field="dienthoai" width="100">Điện thoại</th>
                      <th field="emailcts" width="240">Email</th>
                      <th field="ghichu" width="150">Ghi chú</th>
                  </tr>
              </thead>
          </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>
<!-- Form liên hệ  -->
<div id="dlg-lienhe" class="easyui-dialog" style="width: 370px; padding:10px;" closed="true" modal="true" buttons="#dlg-buttons-2">
  <form id="fm-lienhe" method="post" >
      <div style="margin-bottom:20px">
           <input class="easyui-combobox" name="hoten" style="width:100%" data-options="label:'Họ tên:',required:true"
            valueField="id" textField="name" url="common/contact">
      </div>
      <div style="margin-bottom:20px">
          <input class="easyui-textbox" name="chucvu" style="width:100%" data-options="label:'Chức vụ:'">
      </div>
      <div style="margin-bottom:20px">
          <input class="easyui-textbox" name="dienthoai" style="width:100%;" label="Điện thoại:" required>
      </div>
      <div style="margin-bottom:20px">
           <input class="easyui-textbox" name="emailcts" style="width:100%" data-options="label:'Email:',validType:'email'">
      </div>
      <div style="margin-bottom:20px">
          <input class="easyui-textbox" name="ghichu" style="width:100%;height:50px" data-options="label:'Ghi chú:',multiline:true">
      </div>
  </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savects()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-lienhe').dialog('close')">Bỏ qua</button>
</div>

<script type="text/javascript" src="js/ngaynghi.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="ngaynghi/json" toolbar="#toolbar" pagination="true"
    idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-thanhpho"
    data-options="border:false,fit:true" pageSize="50">
    <thead>
        <tr>
            <th field="nhanvien" width="200">Nhân viên</th>
            <th field="ngaytao" width="100" align="center">Ngày</th>
            <th field="loaihinh" width="150" >Loại hình nghỉ</th>
            <th field="ngaynghi" width="100" align="center">Ngày nghỉ</th>
            <th field="ghi_chu" width="250" >Lý do</th>
            <th field="tinh_trang" width="100" align="center" formatter="tinhtrang" >Tình trạng</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input class="easyui-combobox" name="nhanvien" id="nhanvien" valueField="id" textField="name"
    url="common/nhanvien" prompt="Nhân viên" style="width:160px" >
    <input class="easyui-combobox" name="thang" id="thang" valueField="id" textField="name"
    url="common/thang" style="width:120px" value="<?=date('m')?>" >
    <input class="easyui-combobox" name="nam" id="nam" valueField="id" textField="name"
    url="common/nam" style="width:120px" value="<?=date('Y')?>" >
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <!-- &nbsp&nbsp&nbsp&nbsp&nbsp
    Nghỉ phép: <input id="phep" class="easyui-textbox" style="width:40px; text-align:center" value="2">
    Nghỉ không phép: <input id="phep" class="easyui-textbox" style="width:40px; text-align:center" value="2"> -->

    <!-- 2Tổng phép năm: 10 Phép đã nghỉ:5  Còn lại:5   Nghỉ không phép:1 -->
    <?php
        foreach ($this->funs as $item)
          echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg-phep" class="easyui-dialog" style="padding:10px; width:350px;" data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm-phep" method="post" action="ngaynghi/phep" >
        <table>
            <tr>
                <td>Nhân viên: </td>
                <td>
                    <input id="nv" name="nv" class="easyui-combobox" style="width:220px"
                           url="common/nhanvien" valueField="id" textField="name" readonly  />
                </td>
            </tr><tr>
                <td>Ngày nghỉ: </td>
                <td>
                    <input class="easyui-datebox" prompt="Ngày" style="width:220px" name="ngay" id="ngay" required>
                </td>
            </tr>
            <tr>
                <td>Loại hình: </td>
                <td>
                    <input id="loaihinh" name="loaihinh" class="easyui-combobox" style="width:220px" prompt="Loại hình"
                           url="ngaynghi/loaiphep" valueField="id" textField="name" required PanelHeight="auto"/>
                </td>
            </tr><tr>
                <td>Ghi chú: </td>
                <td>
                    <input id="ghichu" name="ghichu" class="easyui-textbox" prompt="Lý do" style="width:220px; height:60px;"
                           multiline="true" >
                </td>
            </tr>
        </table>
    </form>
    <div class="dialog-button" style="text-align:center">
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="savephep()">OK</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg-phep').dialog('close')">Bỏ qua</a>
    </div>
</div>

<div id="dlg" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closable="false" closed="true"
    buttons="#dlg-buttons" modal="true" iconCls="icon-thanhpho">
    <form id="fm" method="post" action="ngaynghi/duyet" >
      <input type="hidden" name="id" id="id">
      <table>
          <tr>
              <td>Nhân viên: </td>
              <td>
                  <input id="nhan_vien" name="nhan_vien" class="easyui-combobox" style="width:220px" url="common/nhanvien"
                    url="common/nhanvien" valueField="id" textField="name" readonly  />
              </td>
          </tr><tr>
              <td>Ngày: </td>
              <td>
                  <input class="easyui-datebox" prompt="Ngày" style="width:220px" name="ngaynghi" id="ngaynghi" readonly>
              </td>
          </tr>
          <tr>
              <td>Loại hình: </td>
              <td>
                  <input id="loai_hinh" name="loai_hinh" class="easyui-combobox" style="width:220px" prompt="Loại hình"
                         url="ngaynghi/loaiphep" valueField="id" textField="name" readonly />
              </td>
          </tr><tr>
              <td>Ghi chú: </td>
              <td>
                  <input id="ghi_chu" name="ghi_chu" class="easyui-textbox" prompt="Lý do" style="width:220px; height:60px;"
                         multiline="true" >
              </td>
          </tr><tr>
              <td>Tình trạng: </td>
              <td>
                  <input id="tinh_trang" name="tinh_trang" class="easyui-combobox" style="width:220px" prompt="Loại hình"
                         url="ngaynghi/tinhtrang" valueField="id" textField="name" required PanelHeight="auto" value="0"/>
              </td>
          </tr>
      </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-le" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closable="false" closed="true"
    modal="true" iconCls="icon-thanhpho">
    <form id="fm-le" method="post" action="ngaynghi/nghile" >
        <table>
            <tr>
                <td>Ngày: </td>
                <td>
                    <input class="easyui-datebox" prompt="Ngày" style="width:220px" name="ngayle" id="ngayle" required>
                </td>
            </tr>
            <tr>
                <td>Loại hình: </td>
                <td>
                    <input id="letet" name="letet" class="easyui-combobox" style="width:220px" prompt="Loại hình"
                           url="ngaynghi/letet" valueField="id" textField="name" required PanelHeight="auto"/>
                </td>
            </tr><tr>
                <td>Ghi chú: </td>
                <td>
                    <input id="lydo" name="lydo" class="easyui-textbox" prompt="Lý do" style="width:220px; height:60px;"
                           multiline="true" >
                </td>
            </tr>
        </table>
    </form>
    <div class="dialog-button" style="text-align:center">
        <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savele()">Câp nhật</button>
        <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-le').dialog('close')">Bỏ qua</button>
    </div>

</div>

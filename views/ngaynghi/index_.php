<script type="text/javascript" src="js/phep.js"></script>
<div class="m-left" style="color:#FFFF">
    <a href="javascript:void(0)" class="easyui-menubutton" style="color:#FFFF" outline="true" data-options="hasDownArrow:false,menu:'#mm',menuAlign:'right'">Menu</a>
</div>
<div class="m-right" style="color:#FFFF">
    <?php
        if ($this->fun[3])
          echo '<a href="javascript:void(0)" onclick="duyet()" class="easyui-linkbutton" style="color:#FFFF" iconCls="icon-search" plain="true" outline="true">Search</a>';
    ?>
</div>
</div>
</header>
<div id="mm" class="easyui-menu" style="width:150px;" data-options="itemHeight:30,noline:true">
    <?php
        if ($this->fun[1])
          echo '<div data-options="iconCls:\'icon-hanghoa\'" onclick="xinphep()">Đăng ký nghỉ phép</div>';
        if ($this->fun[2])
          echo '<div class="menu-sep"></div><div data-options="iconCls:\'icon-add\'" onclick="congphep()">Cộng phép</div>';
    ?>
</div>
<!-- <div style="text-align:center;padding:10px">
    <span class="m-buttongroup">
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="toggle:true,group:'g1',selected:true" onclick="xinphep()" style="width:100px;height:30px">ĐK nghỉ phép</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="toggle:true,group:'g1'" onclick="congphep()" style="width:100px;height:30px">Cộng phép</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="toggle:true,group:'g1'" onclick="duyet()" style="width:100px;height:30px">Duyệt phép</a>
    </span>
</div> -->
<table id="dg" class="easyui-datagrid" url="phep/json?nhanvien=<?php echo $this->nhanvien ?>" data-options="header:'#hh',singleSelect:true,border:false,fit:true,fitColumns:true,scrollbarSize:0">
    <thead>
        <tr>
            <th data-options="field:'ngaythang',width:80">Ngày</th>
            <th data-options="field:'congphep',width:100,align:'center'">Cộng phép</th>
            <th data-options="field:'truphep',width:80,align:'center'">Nghỉ phép</th>
            <th data-options="field:'con_lai',width:80,align:'center'">Phép còn</th>
        </tr>
    </thead>
</table>
<div id="hh">
    <div class="m-toolbar">
        <div class="m-title">Theo dõi phép: <?php echo $this->name ?></div>
    </div>
</div>

<div id="dlg" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px"
    data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm" method="post" enctype="multipart/form-data">
        <div style="margin-bottom:10px">
          <input class="easyui-datebox" label="Ngày:" prompt="Ngày cộng phép" style="width:100%" name="ngay" id="ngay">
        </div>
        <div style="margin-bottom:10px">
          <input class="easyui-combobox" label="Nhân viên:" prompt="Nhân viên" style="width:100%"
          url="common/nhanvien" valueField="id" textField="name" editable="false" name="nhanvien">
        </div>
        <div style="margin-bottom:10px">
          <input class="easyui-textbox" label="Số phép:" prompt="Số ngày phép" style="width:100%" name="phep" required>
        </div>
    </form>
    <div class="dialog-button" style="text-align:center">
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="save()">OK</a>
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg').dialog('close')">Bỏ qua</a>
    </div>
</div>

<div id="dlg-phep" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px"
    data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm-phep" method="post" enctype="multipart/form-data">
        <div style="margin-bottom:10px">
          <input class="easyui-datebox" label="Ngày:" prompt="Ngày nghỉ phép" style="width:100%" name="ngayphep" id="ngayphep">
        </div>
    </form>
    <div class="dialog-button" style="text-align:center">
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="savephep()">OK</a>
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg-phep').dialog('close')">Bỏ qua</a>
    </div>
</div>

<div id="dlg-duyet" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px"
    data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm-duyet" method="get" >
        <div style="margin-bottom:10px">
          <input class="easyui-combobox" label="Nhân viên:" prompt="Nhân viên" style="width:100%"
          url="common/nhanvien" valueField="id" textField="name" editable="false" name="nhan_vien" id="nhan_vien">
          <input type="hidden" name="name" id="name">
        </div>
    </form>
    <div class="dialog-button" style="text-align:center">
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="tieptuc()">OK</a>
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg-duyet').dialog('close')">Bỏ qua</a>
    </div>
</div>

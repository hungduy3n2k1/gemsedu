<script type="text/javascript" src="js/phep.js"></script>
<table id="dg" class="easyui-datagrid" url="phep/json" data-options="singleSelect:true,border:false,fit:true,fitColumns:true,scrollbarSize:0"
  title="Theo dõi phép: <?php echo $_SESSION['user']['nhanvien'] ?>">
    <thead>
        <tr>
            <th data-options="field:'ngaythang',width:80">Ngày</th>
            <th data-options="field:'congphep',width:100,align:'center'">Cộng phép</th>
            <th data-options="field:'truphep',width:80,align:'center'">Nghỉ phép</th>
            <th data-options="field:'con_lai',width:80,align:'center'">Phép còn</th>
        </tr>
    </thead>
</table>
<div id="dlg-search" class="easyui-dialog" style="width:90%; height: auto; padding: 10px; top:30px" closable="false" closed="true"
    buttons="#dlg-buttons-2" modal="true">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Nhân viên:</td>
                <td>
                    <input class="easyui-combobox" style="width:200px; height:25px" id="nhanvien" name="nhanvien"
                      valueField="id" textField="name" url="common/nhanvien" value="<?php echo $_SESSION['user']['nhan_vien'] ?>">
                </td>
            </tr>
        </table>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="search()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-search').dialog('close')">Bỏ qua</button>
</div>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <?php
        if ($this->fun[3])
              echo '
              <div style="padding:10px">
                  <div class="panel-header tt-inner" onClick="javascript:$(\'#dlg-search\').dialog({closed:false})" style="font-size:14px">
                      <img src="libs/jquery/images/search.png" width="32" height="32" />
                      <br>Tìm kiếm
                  </div>
              </div>
              ';
    ?>

<script type="text/javascript" src="js/baocaohvkt.js"></script>
<table id="dg" style="width: 100%;" url="baocaohvkt/json" class="easyui-datagrid" toolbar="#toolbar" pagination="true" 
       idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="false" 
       data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
       <thead>
              <tr>
                     <!--        <th field="ma_hoc_vien" width="120">Mã học viên (Cũ)</th>-->
                     <!--        <th field="id" width="70">ID</th>-->
                     <th field="mahocvien" width="100">Mã học viên</th>
                     <th field="name" width="160">Họ tên</th>
                     <th field="ngaysinh" width="120">Ngày sinh</th>
                     <th field="khachhang" width="150">Phụ huynh</th>
                     <th field="dienthoaikh" width="130">Điện thoại</th>
                     <th field="emailkh" width="120">Email</th>
                     <th field="ngaybatdau" width="120">Ngày bắt đầu</th>
                     <th field="ngayketthuc" width="120">Ngày kết thúc</th>
                     <th field="giaovien" width="200">Giáo viên</th>
                     <th field="chuyenmon" width="130">Chuyên môn</th>
                     <th field="phanloaisale" width="130">Phân loại sale</th>
              </tr>
       </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
       <input id="thang" name="thang" class="easyui-combobox" url="common/thang" style="width: 100px;height:28px;" valueField="id" textField="name" value="<?php echo date("m") ?>" />
       <input id="nam" name="nam" class="easyui-combobox" url="common/nam" style="width: 100px;height:28px;" valueField="id" textField="name" value="<?php echo date("Y") ?>" />
       <input class="easyui-textbox" name="khachhang" id="khachhang" style="width: 150px;height:28px;" prompt="Khách hàng">&nbsp;
       <input class="easyui-textbox" name="hocvien" id="hocvien" style="width: 150px;height:28px;" prompt="Học viên">&nbsp;
       <input class="easyui-combobox" name="phanloaidh" id="phanloaidh" valueField="id" textField="name" prompt="Phân loại đơn hàng" url="baocaohvkt/phanloaisale" style="width: 150px;height:28px;" />
       <input class="easyui-combobox" name="nhanviensale" id="nhanviensale" valueField="id" textField="name" prompt="Nhân viên sale" url="common/nhanvien" style="width: 150px;height:28px;" />
       <input class="easyui-combobox" name="giaovien" id="giaovien" valueField="id" textField="name" prompt="Giáo viên" url="common/giaovien" style="width: 200px;height:28px;">
       <input class="easyui-combobox" name="chuyenmon" id="chuyenmon" valueField="id" textField="name" prompt="Chuyên môn" url="common/nhanvien" style="width: 200px;height:28px;">
       <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
       <?php
       foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
       ?>
       <button class="easyui-linkbutton l-btn l-btn-small" iconcls="icon-excel" plain="false" onclick="xuatfile()" group="" id="">Xuất excel</button>
</div>
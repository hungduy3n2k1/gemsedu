<?php
	require_once ROOT_DIR.'/libs/phpexcel/PHPExcel.php'; 
	$sql = new Model();
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("VDATA")
							 ->setLastModifiedBy("VDATA")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
	$sheet = $objPHPExcel->getActiveSheet ();
	//formatting
	$center = array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
	$left = array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	$right = array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$sheet->getColumnDimension('A')->setWidth(7);
	$sheet->getColumnDimension('B')->setWidth(15);
	$sheet->getColumnDimension('C')->setWidth(30);
	$sheet->getColumnDimension('D')->setWidth(15);
	$sheet->getColumnDimension('E')->setWidth(30);
	$sheet->getColumnDimension('F')->setWidth(15);
	$sheet->getColumnDimension('G')->setWidth(20);
	$sheet->getColumnDimension('H')->setWidth(15);
	$sheet->getColumnDimension('I')->setWidth(15);
	$sheet->getColumnDimension('J')->setWidth(30);
	$sheet->getColumnDimension('K')->setWidth(30);
	$sheet->getColumnDimension('L')->setWidth(20);
	$sheet->getStyle("A")->getAlignment()->applyFromArray($center);
	$sheet->getStyle("E")->getAlignment()->applyFromArray($left);
	$sheet->getStyle("H")->getAlignment()->applyFromArray($right);
	$sheet->getStyle("I")->getAlignment()->applyFromArray($right);
	$sheet->setCellValue('A1', 'HỌC VIÊN KẾT THÚC KHÓA HỌC');
	$sheet->mergeCells('A1:G1');
	$sheet->getStyle('A1')->getAlignment()->applyFromArray($center);	
    $sheet->setCellValue('A2', 'Edu-Offices');
	$sheet->mergeCells('A2:G2');
	$sheet->getStyle('A2')->getAlignment()->applyFromArray($center);	
	$sheet->setCellValue('A3', '' );
	$sheet->mergeCells('A3:G3');
	$sheet->getStyle('A3')->getAlignment()->applyFromArray($center);	
    $sheet->setCellValue('A4', '');
	$sheet->mergeCells('A4:G4');

	$sheet->setCellValue('A5','STT');
	$sheet->setCellValue('B5','Mã học viên');
	$sheet->setCellValue('C5','Họ tên');
	$sheet->setCellValue('D5','Ngày sinh');
	$sheet->setCellValue('E5','Phụ huynh');
	$sheet->setCellValue('F5','Điện thoại');
	$sheet->setCellValue('G5','Email');	
	$sheet->setCellValue('H5','Ngày bắt đầu');
	$sheet->setCellValue('I5','Ngày kết thúc');
	$sheet->setCellValue('J5','Giáo viên');
	$sheet->setCellValue('K5','Chuyên môn');
	$sheet->setCellValue('L5','Phân loại sale');
	
	// print_r($this->baocaohvkt['rows']);
	$i=6;
 	foreach($this->baocaohvkt['rows'] as $row){
		$sheet->setCellValue('A' . $i, strval($i-5));
		$sheet->setCellValue('B' . $i, $row['mahocvien']);
		$sheet->setCellValue('C' . $i, $row['name']);
		$sheet->setCellValue('D' . $i, $row['ngaysinh']);
		$sheet->setCellValue('E' . $i, $row['khachhang']);
		$sheet->setCellValue('F' . $i, $row['dienthoaikh']);
		$sheet->setCellValue('G' . $i, $row['emailkh']);
		$sheet->setCellValue('H' . $i, $row['ngaybatdau']);
		$sheet->setCellValue('I' . $i, $row['ngayketthuc']);
		$sheet->setCellValue('J' . $i, $row['giaovien'] ?? '');
		$sheet->setCellValue('K' . $i, $row['chuyenmon'] ?? '');
		$sheet->setCellValue('L' . $i, $row['phanloaisale']);
		$i++;
	}
	$sheet->getStyle("A5:L".($i-1))->applyFromArray(
		array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		)
	);


// Rename worksheet
$sheet->setTitle('Báo cáo HVKT');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="doanh_thu('.date("d_m_Y").').xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
//header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_end_clean();  //xóa các định dạng html trước khi ghi file excel để loại trừ lỗi format or file extension not valid  khi mở file
$objWriter->save('php://output');
exit;
?>
<script type="text/javascript" src="js/nhatky.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="nhatky/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true"
    fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-nhatky" data-options="border:false,fit:true"
    pageSize="50">
    <thead>
        <tr>
            <th field="ngaygio" width="150" align="center">Ngày giờ</th>
            <th field="nhanvien" width="150" sortable="true">Nhân viên</th>
            <th field="doi_tuong" width="150" sortable="true">Dữ liệu</th>
            <th field="action" width="300" >Thao tác</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:45px; padding-top:4px; text-align:left">
    <span style="font-weight:bold; color:blue">Nhật ký xóa, sửa</span>
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Từ khóa" style="width:150px" />
    <input class="easyui-combobox" name="nhanvien" id="nhanvien" url="common/nhanvien" valueField="id" textField="name" prompt="Nhân viên">
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>&nbsp;&nbsp;
</div>

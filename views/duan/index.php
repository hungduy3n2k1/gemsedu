<script type="text/javascript" src="js/duan.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="duan/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu" data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="name" width="200" sortable="true">Tên dự án</th>
            <th field="ghi_chu" width="480">Mô tả</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <span style="color:blue; font-weight:bold; margin-right:20px">CÁC DỰ ÁN</span>
    <input class="easyui-combobox" name="tinhtrang" id="tinhtrang" valueField="id" textField="name" prompt="Tình trạng"
        url="duan/tinhtrang" value="1" >
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
      
          foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
  
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên dự án:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px; height:25px" required />
                </td>
            </tr><tr>
                <td>Mô tả:</td>
                <td>
                    <input class="easyui-textbox" multiline="true" style="width:200px; height:100px" id="ghi_chu" name="ghi_chu">
                </td>
            </tr><tr>
                <td>Tình trạng:</td>
                <td>
                    <input class="easyui-combobox" style="width:200px; height:25px" id="tinh_trang" name="tinh_trang"
                      valueField="id" textField="name" url="duan/tinhtrang" required>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>
<!-- <div class="m-left" style="color:#FFFF">
    <a href="javascript:void(0)" class="easyui-menubutton" style="color:#FFFF" outline="true" data-options="hasDownArrow:false,menu:'#mm',menuAlign:'right'">Menu</a>
</div>
<div class="m-right" style="color:#FFFF">

</div>
</div>
</header>
<div id="mm" class="easyui-menu" style="width:150px;" data-options="itemHeight:30,noline:true">

</div> -->


<!-- <div id="dlg" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px"
    data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm" method="post" enctype="multipart/form-data">
        <div style="margin-bottom:10px">
          <input class="easyui-textbox" label="Tên dự án:" prompt="Tên dự án" style="width:100%" name="name" required>
        </div>
        <div style="margin-bottom:10px">
          <input class="easyui-combobox" label="Người tạo:" prompt="Người khởi tạo" style="width:100%" id="nhan_vien"
          url="common/nhanvien" valueField="id" textField="name" editable="false" name="nhan_vien" readonly>
        </div>
        <div style="margin-bottom:10px">
          <input class="easyui-textbox" label="Ghi chú:" prompt="Ghi chú" style="width:100%; height:60px"
          name="ghi_chu" multiline="true">
        </div>
        <div style="margin-bottom:10px">
          <label class="textbox-label">Bật tắt:</label>
                <input class="easyui-switchbutton" id="tinh_trang" name="tinh_trang" value="1" >
        </div>
    </form>
    <div class="dialog-button" style="text-align:center">
       <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="save()">OK</a>
       <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg').dialog('close')">Bỏ qua</a>
    </div>
</div>

<footer>
<div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">

              echo '
              <div style="padding:10px">
                  <div class="panel-header tt-inner" onClick="window.location.href='duan/add'" style="font-size:14px">
                      <img src='libs/jquery/images/add.png' width="32" height="32" />
                      <br>Thêm
                  </div>
              </div> -->

    <!-- <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="window.location.href='duan/edit'" style="font-size:14px">
            <img src='libs/jquery/images/edit.png' width="32" height="32" />
            <br>Sửa
        </div>
    </div>
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="window.location.href='duan/delete'" style="font-size:14px">
            <img src='libs/jquery/images/delete.png' width="32" height="32" />
            <br>Xóa
        </div>
    </div>
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="window.location.href='duan/timkiem'" style="font-size:14px">
            <img src='libs/jquery/images/search.png' width="32" height="32" />
            <br>Tìm kiếm
        </div>
    </div> -->

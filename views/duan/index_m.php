<script type="text/javascript" src="js/duan.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="duan/json" pagination="true" idField="id" rownumbers="true"
fitColumns="true" singleSelect="true" nowrap="true" iconCls="icon-dichvu" data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="name" width="200">Tên dự án</th>
        </tr>
    </thead>
</table>

<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px; top:100px" closable="false" closed="true"
    buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên dự án:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px; height:25px" required />
                </td>
            </tr><tr>
                <td>Mô tả:</td>
                <td>
                    <input class="easyui-textbox" multiline="true" style="width:200px; height:100px" id="ghi_chu" name="ghi_chu">
                </td>
            </tr><tr>
                <td>Tình trạng:</td>
                <td>
                    <input class="easyui-combobox" style="width:200px; height:25px" id="tinh_trang" name="tinh_trang"
                      valueField="id" textField="name" url="duan/tinhtrang" value="1">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-search" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px; top:100px" closable="false" closed="true"
    buttons="#dlg-buttons-2" modal="true">
    <form id="fm-search" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tình trạng:</td>
                <td>
                    <input class="easyui-combobox" style="width:200px; height:25px" id="tinhtrang" name="tinhtrang"
                      valueField="id" textField="name" url="duan/tinhtrang" value="1">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="timkiem()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-search').dialog('close')">Bỏ qua</button>
</div>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <?php
        if ($this->fun[1])
              echo '
              <div style="padding:10px">
                  <div class="panel-header tt-inner" onClick="add()" style="font-size:14px">
                      <img src="libs/jquery/images/add.png" width="32" height="32" />
                      <br>Thêm
                  </div>
              </div>
              ';
          if ($this->fun[2])
                echo '
                <div style="padding:10px">
                    <div class="panel-header tt-inner" onClick="edit()" style="font-size:14px">
                        <img src="libs/jquery/images/edit.png" width="32" height="32" />
                        <br>Sửa
                    </div>
                </div>
                ';
          if ($this->fun[3])
                echo '
                <div style="padding:10px">
                    <div class="panel-header tt-inner" onClick="del()" style="font-size:14px">
                        <img src="libs/jquery/images/delete.png" width="32" height="32" />
                        <br>Xóa
                    </div>
                </div>
                ';
    ?>
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="javascript:$('#dlg-search').dialog({closed:false})" style="font-size:14px">
            <img src="libs/jquery/images/search.png" width="32" height="32" />
            <br>Tìm kiếm
        </div>
    </div>

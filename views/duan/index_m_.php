<script type="text/javascript" src="js/duan.js"></script>
<div class="easyui-accordion" fit="true" border="false">
    <div title="Dự án đang làm">
        <ul class="m-list" >
            <?php
              foreach ($this->data AS $item)
                if ($item['tinh_trang']==1) {
                    echo '<li><a href="javascript:editm('.$item['id'].')" class="datalist-link">'.$item['name'].'</a></li>';
                }
            ?>
        </ul>
    </div>
    <div title="Dự án đã kết thúc">
        <ul class="m-list" >
            <?php
              foreach ($this->data AS $item)
                if ($item['tinh_trang']==0) {
                    echo '<li><a href="javascript:editm('.$item['id'].')" class="datalist-link">'.$item['name'].'</a></li>';
                }
            ?>
        </ul>
    </div>
</div>

<div id="dlg" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px"
    data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm" method="post" enctype="multipart/form-data">
        <div style="margin-bottom:10px">
          <input class="easyui-textbox" label="Tên dự án:" prompt="Tên dự án" style="width:100%" name="name" required>
        </div>
        <div style="margin-bottom:10px">
          <input class="easyui-textbox" label="Ghi chú:" prompt="Ghi chú" style="width:100%; height:60px"
          name="ghi_chu" multiline="true">
        </div>
        <div style="margin-bottom:10px">
          <label class="textbox-label">Bật tắt:</label>
                <input class="easyui-switchbutton" id="tinh_trang" name="tinh_trang" value="1" >
        </div>
    </form>
    <div class="dialog-button" style="text-align:center">
       <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="save()">OK</a>
       <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg').dialog('close')">Bỏ qua</a>
    </div>
</div>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <?php
        if ($this->fun[1])
              echo '
              <div style="padding:10px">
                  <div class="panel-header tt-inner" onClick="add()" style="font-size:14px">
                      <img src="libs/jquery/images/add.png" width="32" height="32" />
                      <br>Thêm
                  </div>
              </div>
              ';
        // if ($this->fun[2])
        //       echo '
        //       <div style="padding:10px">
        //           <div class="panel-header tt-inner" onClick="editm()" style="font-size:14px">
        //               <img src="libs/jquery/images/edit.png" width="32" height="32" />
        //               <br>Sửa
        //           </div>
        //       </div>
        //       ';
        // if ($this->fun[3])
        //       echo '
        //       <div style="padding:10px">
        //           <div class="panel-header tt-inner" onClick="del()" style="font-size:14px">
        //               <img src="libs/jquery/images/delete.png" width="32" height="32" />
        //               <br>Xóa
        //           </div>
        //       </div>
        //       ';
    ?>

<script>
    $(function(){
        $('#dg-detail-<?php echo $this->id; ?>').datagrid({
            url: baseUrl + '/phaithu/jsondetail?id=<?php echo $this->id; ?>',
        });
    });
</script>
<table id="dg-detail-<?php echo $this->id; ?>"
    style="width: 100%;" pagination="false" idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true"
	  data-options="border:true,fit:true" pageSize="30" showFooter="true">
	<thead>
		<tr>
      <th field="dichvu" width="250">Hàng hóa/dịch vụ</th>
      <th field="don_gia" width="120" align="right" formatter="CurrencyFormatted">Đơn giá</th>
      <th field="don_vi" width="120" align="center">Đơn vị tính</th>
      <th field="so_luong" width="120" align="center">Số lượng</th>
      <th field="chiet_khau_tm" width="120" align="right" formatter="CurrencyFormatted">CKTM</th>
      <th field="chiet_khau_pt" width="120" align="center">CK %</th>
      <th field="thanhtien" width="120" align="right" formatter="CurrencyFormatted" >Thành tiền</th>
      <th field="thuevat" width="120" align="right" formatter="CurrencyFormatted"  >Thuế VAT</th>
		</tr>
	</thead>
</table>

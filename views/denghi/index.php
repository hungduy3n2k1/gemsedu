<script type="text/javascript" src="js/denghi.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="denghi/json" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu"
       data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
    <tr>
        <th field="ngaygio" width="100">Ngày giờ</th>
        <th field="nhanvien" width="180">Nhân viên</th>
        <th field="so_tien" width="120" formatter="CurrencyFormatted">Số tiền</th>
        <th field="noi_dung" width="340">Nội dung</th>
        <th field="tinhtrang" width="120">Tình trạng</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <?php
    $duyet = false;
    foreach ($this->funs as $item) {
        if ($item['link'] == 'duyet()')
            $duyet = true;
    }
    ?>
    Từ ngày: <input id="ngaybd" name="ngaybd" class="easyui-datebox" style="width:120px"
                    value="<?php echo date(" d/m/Y", strtotime('first day of this month')) ?>"/>
    Đến ngày: <input id="ngaykt" name="ngaykt" class="easyui-datebox" style="width:120px"
                     value="<?php echo date(" d/m/Y ") ?>"/>
<!--    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Từ khóa" style="width:150px"/>-->
    <input id="nhanvien" name="nhanvien" style="width:180px;" editable="false"
           url="common/nhanvien" <?php if ($duyet == false) echo 'readonly' ?>
           valueField="id" textField="name" class="easyui-combobox" value="<?php if ($duyet == false) echo $_SESSION['user']['id'] ?>"/>
    <input id="tinhtrang" name="tinhtrang" class="easyui-combobox" style="width: 150px;" prompt="Tình trạng"
           data-options="valueField:'id',textField:'text',
           data:[{'id':'1','text':'Tạo mới'},{'id':'2','text':'Đã duyệt'},{'id':'3','text':'Từ chối'}] " />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>&nbsp;&nbsp;
    <?php
    foreach ($this->funs as $item) {
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    }
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true"
     buttons="#dlg-buttons" modal="true" iconCls="icon-phieuthu">
    <form id="fm" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Ngày :</div>
                </td>
                <td>
                    <input id="ngaygio" name="ngaygio" class="easyui-datebox"
                           style="width: 200px;height:28px;" <?php if ($duyet == false) echo 'readonly' ?> />
                </td>
            </tr>
            <tr>
                <td>
                    <div>Số tiền:</div>
                </td>
                <td>
                    <input id="so_tien" name="so_tien" style="width:200px; height:28px;" class="easyui-numberbox"
                           groupSeparator="," required/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Nhân viên:</div>
                </td>
                <td>
                    <input id="nhan_vien" name="nhan_vien" style="width:200px; height:28px;" editable="false"
                           url="common/nhanvien" <?php if ($duyet == false) echo 'readonly' ?>
                           valueField="id" textField="name" class="easyui-combobox"
                           value="<?= $_SESSION['user']['id'] ?>"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Nội dung:</div>
                </td>
                <td>
                    <input id="noi_dung" name="noi_dung" style="width:200px; height:60px;" class="easyui-textbox"
                           multiline="true"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dlg').dialog('close')">Quay lại
    </button>
</div>

<div id="dlg-duyet" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true"
     buttons="#dlg-buttons-duyet" modal="true" iconCls="icon-phieuthu">
    <form id="fm-duyet" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Ngày :</div>
                </td>
                <td>
                    <input id="ngaygio1" name="ngaygio1" class="easyui-datebox" readonly
                           style="width: 200px;height:28px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <div>Số tiền:</div>
                </td>
                <td>
                    <input id="so_tien1" name="so_tien1" style="width:200px; height:28px;" class="easyui-numberbox" readonly
                           groupSeparator="," required/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Nhân viên:</div>
                </td>
                <td>
                    <input id="nhan_vien1" name="nhan_vien1" style="width:200px; height:28px;" editable="false" readonly
                           url="common/nhanvien"
                           valueField="id" textField="name" class="easyui-combobox"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Nội dung:</div>
                </td>
                <td>
                    <input id="noi_dung1" name="noi_dung1" style="width:200px; height:60px;" class="easyui-textbox" readonly
                           multiline="true"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Tình trạng:</div>
                </td>
                <td>
                    <input id="tinh_trang" name="tinh_trang" class="easyui-combobox" style="width:200px; height:28px;" prompt="Tình trạng"
                           data-options="valueField:'id',textField:'text',
           data:[{'id':'1','text':'Tạo mới'},{'id':'2','text':'Đã duyệt'},{'id':'3','text':'Từ chối'}] " />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-duyet">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="saveduyet()">Duyệt</button>
    <button class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dlg-duyet').dialog('close')">Quay lại
    </button>
</div>


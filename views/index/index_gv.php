<script type="text/javascript" src="js/index_gv.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" pagination="false" toolbar="#toolbar"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="false" nowrap="false"
       data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
    <thead>
    <tr>
        <th field="ck" checkbox="true"></th>
        <th field="name" width="180">Full name</th>
        <th field="e_name" width="150">Nickname</th>
    </tr>
    </thead>
</table>

<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="chonlopin()">Checkin</button>
    <!--    <button class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="diemdanh()">Check attendance</button>-->
    <button class="easyui-linkbutton" iconCls="icon-redo" plain="false" onclick="chonlopout()">Checkout</button>
    <button class="easyui-linkbutton" iconCls="icon-no" id="huylich" style="display: none;" plain="false" onclick="huylich()">Cancel</button>
</div>

<div id="dlg" class="easyui-dialog" style="padding:20px 6px;width:280px;" title="Choose a class"
     data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Hour:</td>
                <td style="color: red">
                    <?=date('H:i:s')?>
                </td>
            </tr>
            <tr>
                <td>Class:</td>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc" style="width: 180px;height:28px;"
                           url="diemdanh/lichhoc" valueField="id" textField="name" >
                </td>
            </tr>
        </table>
    </form>
    <div class="dialog-button" >
        <button class="easyui-linkbutton" iconCls="icon-ok" onclick="tieptuc()">Save</button>
        <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="$('#dlg').dialog('close')">Close</button>
    </div>
</div>

<div id="dlg-link" class="easyui-dialog" style="padding:20px 6px;width:350px; top:100px" title="Post a link"
     data-options="inline:true,modal:true,closed:true,closable:true">
    <form id="fm-link" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    Link:
                </td>
                <td>
                    <input class="easyui-textbox" name="link" id="link" style="width: 290px;height:28px;" required>
                </td>
            </tr>
            <tr>
                <td style="line-height: 1">
                    Link đánh giá
                </td>
                <td>
                    <input class="easyui-textbox" name="link_danh_gia" id="link_danh_gia" style="width: 290px;height:28px;">
                </td>
            </tr>
        </table>
    </form>
    <div class="dialog-button" style="text-align:center">
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="checkout1()">OK</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg-link').dialog('close')">Close</a>
    </div>
</div>

<form id="fm-diemdanh" method="post" enctype="multipart/form-data" action="diemdanh/diemdanh">
    <input type="hidden" id="data-hocvien" name="data-hocvien">
    <input type="hidden" id="lichhoc" name="lichhoc">
    <input type="hidden" id="giovao" name="giovao">
    <input type="hidden" id="phanloai" name="phanloai">
    <input type="hidden" id="tinhtranghuy" name="tinhtranghuy">
</form>
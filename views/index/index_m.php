<?php
echo '<ul class="m-list">';
$model = new model();
$phanloai = 1;
if ($_SESSION['user']['nhom'] == 16)
    $phanloai = 2;
$menu = $model->mobilemenu($phanloai);
// $role = $model->getmenu($_SESSION['user']['id']);
// $submenu=$model->submenu($item['id']);
foreach ($menu as $item)
    //   if (in_array($item['id'],$role) && ($item['mobile']==1)) {
    //        echo '<li class="m-list-group" style="height:40px; background-color:#85c1e9; line-height:40px">'.$item['name'].'</li>';
    //        $submenu = $model->submenu($item['id']);
    // foreach ($submenu as $sub)
    //            if (in_array($sub['id'],$role) && ($sub['mobile']==1))
    echo '<li><a href="' . $item['link'] . '" onclick="openit(this)">' . $item['name'] . '</a></li>';
// }

// echo '<li class="m-list-group" style="height:40px; background-color:#85c1e9; line-height:40px">Tài khoản</li>
//
//       <li><a href="index/matkhau" onclick="openit(this)">Đổi mật khẩu</a></li>
//
//       <li><a href="index/logout" onclick="openit(this)">Đăng xuất</a></li>';


echo '</ul>';

?>


<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" title="Đổi mật khẩu"
     closed="true" buttons="#dlg-buttons" modal="true" closable="false">
    <form id="fm" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Old password:</td>
                <td>
                    <input id="matkhaucu" name="matkhaucu" data-options="required:true"
                           class="easyui-passwordbox" style="width: 180px;height:25px;"/>
                </td>
            </tr>
            <tr>
                <td>New password:</td>
                <td>
                    <input id="matkhaumoi" name="matkhaumoi" data-options="required:true"
                           class="easyui-passwordbox" style="width: 180px;height:25px;"/>
                </td>
            </tr>
            <tr>
                <td>Confirm password:</td>
                <td>
                    <input id="retype" name="retype" data-options="required:true"
                           class="easyui-passwordbox" style="width: 180px;height:25px;"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="changepass()">Save</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Close
    </button>
</div>
<footer>
    <div class="easyui-tabs"
         data-options="tabHeight:70,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true"
         style="padding-bottom: 5px;">
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="javascript:$('#dlg').dialog({closed:false})"
                 style="font-size:14px">
                <img src='libs/jquery/images/lock.png' width="32" height="32"/>
                <br>Change password
            </div>
        </div>
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="window.location.href='index/logout'" style="font-size:14px">
                <img src='libs/jquery/images/logout.png' width="32" height="32"/>
                <br>Logout
            </div>
        </div>

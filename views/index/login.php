<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GEMS TECH - Văn phòng điện tử</title>
    <link rel="shortcut icon" href="<?php echo URL; ?>/public/images/favicon.png" type="image/x-icon"/>
    <link rel="stylesheet" href="<?php echo URL ?>/public/css/login.css">
    <link rel="stylesheet" type="text/css" href="<?php echo URL ?>/public/css/roboto.css"/>
</head>
<body>
<div class="login-page">
    <div class="form">
        <form class="login-form" action="" method="post">
            <input type="text" name="username" placeholder="Tên đăng nhập"/>
            <input type="password" name="password" id="password" placeholder="Mật khẩu"/>
            <input type="checkbox" id="checkbox" onclick="showpass()" style="width: auto;"/><span style="width: auto;margin-left: 3px;">Hiển thị mật khẩu</span>
            <script>
                function showpass() {
                    var x = document.getElementById("password");
                    if (x.type === "password") {
                        x.type = "text";
                    } else {
                        x.type = "password";
                    }
                }
            </script>
            <button>Đăng nhập</button>
            <?php
            if (@$this->loginfail)
                echo '<p class="message">Sai tên đăng nhập hoặc mật khẩu!</p>';
            else
                echo '<p>Bạn chưa có tài khoản?<br><a href="' . URL . '/index/dangky">Hãy đăng ký với phòng nhân sự</a></p>'
            ?>
        </form>
    </div>
</div>
<div class="container">
    <div class="info">
        <h1>Văn phòng điện tử 4.0</h1>
        <p class="message">Hỗ trợ 0989.848.886 hoặc email <a href="">hotro@gemstech.com.vn</a></p>
    </div>
</div>
</body>
</html>

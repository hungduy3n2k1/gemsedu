
<script src="https://cdn.tiny.cloud/1/ams33212wu9upfjfsxz4ryd2og0yyrri55o4gmagrl9dp5xo/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
<script type="text/javascript" src="js/index.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<div class="easyui-window" title="Doanh thu" data-options="width:430,height:250,left:10,top:10,inline:true"
     style="padding:10px">
    <div id="container-2" style="height:100%;"></div>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var myChart = Highcharts.chart('container-2', {
                chart: {type: 'column'},
                title: {text: 'Tỷ lệ hoàn thành doanh thu tháng <?=$this->thang?>/<?=$this->nam?>'},
                // subtitle: {
                //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
                // },
                accessibility: {announceNewData: {enabled: true}},
                xAxis: {type: 'category'},
                yAxis: {title: {text: 'Tính theo %. <br>Đơn vị: Triệu'}},
                legend: {enabled: false},
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        }
                    }
                },
                tooltip: {
                    // headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    // pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                    // headerFormat: '',
                    // pointFormat: '<b>{point.y:.2f}%</b>/200M<br/>'
                    formatter: function () {
                        // console.log(this)
                        // if (this.series.options.type == 'pie')
                        //     return Math.round(this.percentage)+'%';
                        //return this.series.data[point].value;
                        return this.point.value + '/' + this.point.target;
                    }
                },
                series: [
                    {
                        name: "Thống kê",
                        colorByPoint: true,
                        data: [
                            <?=$this->doanhthu;?>
                        ]
                    }
                ]
            });

        });
    </script>
</div>
<div class="easyui-window" title="Công việc" data-options="width:430,height:250,left:455,top:10,inline:true,cls:'c1'">
    <div class="easyui-datalist" id="congviec" style="width:100%;height:100%"></div>
</div>
<div class="easyui-window" title="Đợt thanh toán đến hạn"
     data-options="width:430,height:250,left:900,top:10,inline:true,cls:'c2'">
    <table id="dg-invoice" style="width: 100%;" class="easyui-datagrid" url="index/invoice" toolbar="#toolbar"
           pagination="false"
           idField="id" rownumbers="false" fitColumns="true" singleSelect="true" nowrap="false" iconCls="icon-dichvu"
           data-options="border:false,fit:true" showFooter="false" pageSize="30">
        <thead>
        <tr>
<!--            <th field="soinv" width="5%">Số inv</th>-->
            <th field="ngay" width="20%" align="center">Ngày TT</th>
            <th field="khachhang" width="20%">KH</th>
            <th field="don_hang" width="10%">ĐH</th>
            <th field="dot_thanh_toan" width="10%" align="center">Đợt TT</th>
            <th field="so_tien" width="20%" align="right" formatter="CurrencyFormatted">Số tiền</th>
            <th field="du_no" width="20%" align="right" formatter="CurrencyFormatted">Còn lại</th>
        </tr>
        </thead>
    </table>
</div>
<div class="easyui-window" title="Khách hàng"
     data-options="width:430,height:250,left:10,top:300,inline:true,border:'thin',cls:'c5'">
    <p class="w-content">Dịch vụ đến hạn</p>
</div>
<div class="easyui-window" title="Thông báo dạy thay" data-options="width:430,height:250,left:455,top:300,inline:true,border:'thin',cls:'c6'">
        <table id="dg-invoice" style="width: 100%;" class="easyui-datagrid" url="index/lichdaythay" toolbar="#toolbar" pagination="false" idField="id" rownumbers="false" fitColumns="true" singleSelect="true" nowrap="false" iconCls="icon-dichvu" data-options="border:false,fit:true" showFooter="false" pageSize="30">
            <thead>
                <tr>
                    <th field="ngay" width="20%" align="center">Ngày</th>
                    <th field="lophoc" width="20%">Lớp</th>
                    <th field="giaovien" width="30%">Giáo viên</th>
                    <th field="gvdaythay" width="30%">Giáo viên dạy thay</th>
                    <!-- <th field="so_tien" width="20%" align="right" formatter="CurrencyFormatted">Số tiền</th>
                    <th field="du_no" width="20%" align="right" formatter="CurrencyFormatted">Còn lại</th> -->
                </tr>
            </thead>
        </table>
    </div> 
<div class="easyui-window" title="Thông báo"
     data-options="width:430,height:250,left:900,top:300,inline:true,border:'thin',cls:'c7'">
    <div class="easyui-datalist" id="thongbao" style="width:100%;height:100%"></div>
</div>


<!-- show thông báo -->
<div id="dlg-add" style="width:1200px;height:450px;padding:10px" closable="false" closed="true" buttons="#dlg-buttons"
     modal="true">
    <p id="noidungthongbao"></p>
</div>
<div id="dlg-buttons" hidden>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-add').dialog('close')">Bỏ qua
    </button>
</div>
    
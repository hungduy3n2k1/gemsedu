<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;"  title="Đổi mật khẩu"
    closed="false" buttons="#dlg-buttons" modal="true" closable="false">
    <form id="fm" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Old password:</td>
				<td>
				    <input id="matkhaucu" name="matkhaucu" data-options="required:true"
                    class="easyui-passwordbox" style="width: 180px;height:25px;"/>
				</td>
            </tr>
            <tr>
                <td>New password:</td>
				<td>
				    <input id="matkhaumoi" name="matkhaumoi" data-options="required:true"
                    class="easyui-passwordbox" style="width: 180px;height:25px;"/>
				</td>
            </tr>
            <tr>
                <td>Confirm password:</td>
				<td>
				    <input id="retype" name="retype" data-options="required:true"
                    class="easyui-passwordbox" style="width: 180px;height:25px;"/>
				</td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
	<button class="easyui-linkbutton" iconCls="icon-ok" onclick="changepass()">Câp nhật</button>
	<button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<script>
function changepass(){
    $('#fm').form('submit',{
        url: baseUrl + '/index/changepass',
        onSubmit: function(){
            return $(this).form('validate');
        },
        success: function(result){
            var result = eval('('+result+')');
            if (result.success){
                $('#dlg').dialog('close');
                show_messager(result.msg);
            } else {
                show_messager(result.msg);
            }
        }
    });
}
</script>

<div class="doc">
    <div class="top-doc">
        <div class="top-left">
            <p><span>Công ty cổ phần truyền thông số VDATA</span></p>
            <p><span>Địa chỉ: </span>P2909, N02, New Horizon City, 97 Lĩnh Nam, Hoàng Mai, Hà Nội</p>
            <p><span>Điện thoại: </span>(024) 7777 8666</p>
        </div>
    </div>
    <div class="middle-doc">
        <h2><?php echo $this->content['name'] ?></h2>
    </div>
    <div class="content-doc">
        <div class="info-doc"><?php echo $this->content['noi_dung'] ?></div>
    </div>
</div>

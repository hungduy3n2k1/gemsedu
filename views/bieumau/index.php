<script type="text/javascript" src="js/bieumau.js"></script>
<script type="text/javascript" src="public/tinymce/tinymce.min.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="bieumau/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-bieumau" data-options="border:false,fit:true"
    pageSize="30">
    <thead>
        <tr>
            <th field="ngaycapnhat" width="120">Ngày cập nhật</th>
            <th field="name" width="240" >Tên biểu mẫu</th>
            <th field="phan_loai" width="150" align="center" formatter="phanloai" sortable="true">Phân loại</th>
            <th field="nguoinhap" width="140"  align="center" >Người nhập</th>
            <th field="filename" width="120"  align="center" formatter="download">Tải về</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; padding-top:4px; text-align:left">
    <input id="phanloai" name="phanloai" class="easyui-combobox" url="bieumau/phanloai" style="width: 200px;height:25px;" valueField="id" textField="text" prompt="Phân loại" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
        foreach ($this->funs as $item)
          echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true" iconCls="icon-thanhpho">
    <form id="fm" method="post" enctype="multipart/form-data" >
            <div style="margin-bottom:20px">
                  <label for="name">Tên biểu mẫu:</label>
                  <input id="name" name="name" class="easyui-textbox" style="width:100%;" required />
            </div>
            <div style="margin-bottom:20px">
                  <label for="phan_loai">Phân loại:</label>
                  <input id="phan_loai" name="phan_loai" class="easyui-combobox" url="bieumau/phanloai" style="width:100%;" valueField="id" textField="text" prompt="Phân loại" panelHeight="auto"/>
            </div>
            <div style="margin-bottom:20px">
                  <label for="file">File upload:</label>
                  <input class="easyui-filebox" data-options="prompt:'Choose a file...'" style="width:100%" id="file" name="file" >
            </div>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-noidung" class="easyui-dialog" style="width:100%;height:100%;padding:0px"
        data-options="
            closed: true,
            modal:true,
            iconCls: 'icon-phieuthu',
            buttons: [{
                text:'Ghi lại',
                iconCls:'icon-save',
                handler:function(){savenoidung();}
            },{
                text:'Hủy',
                iconCls:'icon-no',
                handler:function(){$('#dlg-noidung').dialog('close');}
            }]
        ">
        <form id="fm-noidung" method="post" >
        <textarea name="noi_dung" id="noi_dung"></textarea>
        </form>
</div>


<script>
    tinymce.init({
                  mode: "textareas",
                  entity_encoding : "raw",
                  plugins: ["advlist autolink lists link image charmap print preview anchor",
                              "searchreplace visualblocks code fullscreen textcolor", "media",
                              "insertdatetime media table contextmenu paste jbimages","fullscreen","moxiemanager"],
                  image_advtab: true,
                  paste_data_images: true,
                  browser_spellcheck : true,
                  relative_urls:false,
                  remove_script_host : false,
                  height : "450",
                  //convert_urls : true,
                  image_dimensions: false,
                  forced_root_block : false,
                  force_br_newlines : true,
                  force_p_newlines : false,
                  toolbar: " undo redo | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media insertfile |  fontsizeselect | forecolor backcolor | fullscreen"
      });
      // tinyMCE.activeEditor.setContent('');
      // tinymce.activeEditor.setContent("content");
      // tinyMCE.get('mota').setContent('<span>some</span> html');
</script>

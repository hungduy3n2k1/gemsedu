<script type="text/javascript" src="js/phep.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="phep/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-thanhpho" data-options="border:false,fit:true"
    pageSize="50">
    <thead>
        <tr>
            <th field="name" width="250">Nhân viên</th>
            <th field="phep_nam" width="100" align="center">Nghỉ bù</th>
            <th field="bu" width="100" align="center">Đã nghỉ bù</th>
            <th field="conlaibu" width="100" align="center">Còn lại</th>
            <th field="phep_luy_ke" width="100" align="center">Phép lũy kế</th>
            <th field="phep" width="100" align="center">Phép đã nghỉ</th>
            <th field="conlai" width="100" align="center">Còn lại</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="text-align:left">
    <input id="thang" name="thang" class="easyui-combobox" url="common/thang" valueField="id" textField="name"
      value="<?php echo date("m") ?>" />
    <input id="nam" name="nam" class="easyui-combobox" url="common/nam" valueField="id" textField="name"
      value="<?php echo date("Y") ?>" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="search()">Tìm kiếm</button>      
    <?php
        foreach ($this->funs as $item)
          echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true" iconCls="icon-thanhpho">
    <form id="fm" method="post" >
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Nhân viên:</td>
                <td>
                    <input id="name" name="name" url="common/nhanvien" style="width: 200px;height:28px;"
                    class="easyui-combobox" valueField="id" textField="name" readonly>
                </td>
            </tr><tr>
                <td>Phép năm:</td>
                <td>
                    <input id="phep_nam" name="phep_nam" class="easyui-numberbox" style="width: 200px;height:28px;" data-options="min:0,precision:2,groupSeparator:','"/>
                </td>
            </tr><tr>
                <td>Phép lũy kế:</td>
                <td>
                    <input id="phep_luy_ke" name="phep_luy_ke" class="easyui-numberbox" style="width: 200px;height:28px;" data-options="min:0,precision:2,groupSeparator:','"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

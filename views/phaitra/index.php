<script type="text/javascript" src="js/phaitra.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="phaitra/json" toolbar="#toolbar"
    pagination="true" idField="id" rownumbers="true" fitColumns="false" showFooter="true" singleSelect="true"
    nowrap="true" iconCls="icon-phaitra" data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
              <th field="khachhang" width="250">Khách hàng</th>
              <th field="du_no" width="120"  align="right" formatter="CurrencyFormatted">Số dư</th>
              <th field="chitiet" width="150" align="center" > ... </th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:30px; line-height: 30px; text-align:left">
    <span style="font-weight:bold; color:blue">NỢ PHẢI THU</span>
</div>

<div id="dlg" class="easyui-dialog" style="width: 1000px; height: 400px; padding: 0px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true" iconCls="icon-thanhpho">
  <table id="dg-chitiet" style="width: 100%;" class="easyui-datagrid"
      pagination="false" idField="id" rownumbers="true" fitColumns="true" showFooter="true" singleSelect="true"
      nowrap="false" iconCls="icon-phaitra" data-options="border:false,fit:true" pageSize="30">
      <thead>
          <tr>
              <th field="ngay" width="100">Ngày</th>
              <th field="khachhang" width="150">Khách hàng</th>
              <th field="sophieu" width="80">Số phiếu</th>
              <th field="noi_dung" width="350">Nội dung</th>
              <th field="no" width="100" align="right" formatter="CurrencyFormatted">Ghi nợ</th>
              <th field="co" width="100" align="right" formatter="CurrencyFormatted">Ghi có</th>
              <th field="du_no" width="100"  align="right" formatter="CurrencyFormatted">Số dư</th>
          </tr>
      </thead>
  </table>

</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="javascript:$('#dlg').dialog('close')">OK</button>
</div>

<script type="text/javascript" src="js/nguondata.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="nguondata/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-thanhpho" data-options="border:false,fit:true"
    pageSize="50">
    <thead>
        <tr>
            <th field="id" width="100" align="center">ID</th>
            <th field="name" width="250" >Nguồn data</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; line-height: 30px; text-align:left">
    <button class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="add()">Thêm</button>
    <button class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="edit()">Sửa</button>
    <button class="easyui-linkbutton" iconCls="icon-no" plain="false" onclick="del()">Xóa</button>
</div>

<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true" iconCls="icon-thanhpho">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Nguồn data:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" style="width: 200px;height:25px;" required />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

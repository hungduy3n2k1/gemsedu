<?php if (isset($this->lophoc)) { ?>
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-groupview.js"></script>
    <table id="dg" style="width: 100%;" class="easyui-datagrid" url="lophoc/jsonlichhoc?lophoc=<?= $this->lophoc ?>"
           toolbar="#toolbar" fitColumns="false" singleSelect="true" nowrap="true" showFooter="false"
           data-options="border:false,fit:true,iconCls:'icon-nhaphang', view:groupview,
              groupField:'ngay',
              groupFormatter:function(value,rows){
                  return value + ' - ' + rows.length + ' Buổi học(s)';
            }">
        <thead>
        <tr>
            <th field="ngay" width="120" align="center">Ngày</th>
            <th field="gio" width="120" align="center">Giờ</th>
            <th field="giaovien" width="250">Giáo viên</th>
            <th field="phonghoc" width="160">Phòng học</th>
            <th field="tinh_trang" width="160" align="center" formatter="tinhtrang">Tình trạng</th>
        </tr>
        </thead>
    </table>

    <div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
        <span style="color: blue;font-weight: bold;">Lớp học:</span>
        <span style="color: red;font-weight: bold;"><?= $this->tenlophoc ?></span>
        <button class="easyui-linkbutton" iconCls="icon-undo" plain="false" onclick="pre()">Tháng trước</button>
        <input class="easyui-textbox" name="thang" id="thang" value="<?= date('m') ?>" style="width: 30px">
        <input class="easyui-textbox" name="nam" id="nam" value="<?= date('Y') ?>" style="width: 60px">
        <button class="easyui-linkbutton" iconCls="icon-ok" plain="false" onclick="timkiem()">OK</button>
        <button class="easyui-linkbutton" iconCls="icon-redo" plain="false" onclick="next()">Tháng sau</button>
        <button class="easyui-linkbutton" iconCls="icon-undo" plain="false"
                onclick="javascript:window.location.href=baseUrl+'/lophoc'">Quay lại
        </button>
    </div>
    <script type="text/javascript">
        function celltop(value, row) {
            if (value)
                return '<div style="min-height:70px;line-height:12px">' + value + '</div>';
        }

        function tinhtrang(val, row) {
            if (val == 1)
                return 'Chờ';
            else if (val == 2)
                return 'Đã setup';
            else if (val == 3)
                return 'Đang học';
            else if (val == 4)
                return 'Hoàn thành';
            else if (val == 5)
                return 'Đã chốt';
            else if (val == 6)
                return 'Cọc ngay';
            else if (val == 7)
                return 'Hủy';
            else
                return '';
        }

        function timkiem() {
            var thang = $('#thang').textbox('getValue');
            var nam = $('#nam').textbox('getValue');
            if (thang.length > 0 || nam.length > 0) {
                $("#dg").datagrid("options").url = baseUrl + "/lophoc/jsonlichhoc?thang=" + thang + "&nam=" + nam + '&lophoc=<?=$this->lophoc?>';
            } else {
                $("#dg").datagrid("options").url = baseUrl + "/lophoc/jsonlichhoc?lophoc=<?=$this->lophoc?>";
            }
            $("#dg").datagrid("reload");
        }

        function pre() {
            var thang = $('#thang').textbox('getValue');
            var nam = $('#nam').textbox('getValue');
            var x = parseInt(thang) - 1;
            if (x > 0) {
                if (x < 10)
                    thang = '0' + x;
                else
                    thang = x;
                $('#thang').textbox('setValue', thang);
            } else {
                thang = '12';
                var y = parseInt(nam) - 1;
                $('#thang').textbox('setValue', thang);
                $('#nam').textbox('setValue', y);
            }
            $("#dg").datagrid("options").url = baseUrl + "/lophoc/jsonlichhoc?thang=" + thang + "&nam=" + nam + '&lophoc=<?=$this->lophoc?>';
            $("#dg").datagrid("reload");
        }

        function next() {
            var thang = $('#thang').textbox('getValue');
            var nam = $('#nam').textbox('getValue');
            var x = parseInt(thang) + 1;
            if (x < 13) {
                if (x < 10)
                    thang = '0' + x;
                else
                    thang = x;
                $('#thang').textbox('setValue', thang);
            } else {
                thang = '01';
                var y = parseInt(nam) + 1;
                $('#thang').textbox('setValue', thang);
                $('#nam').textbox('setValue', y);
            }
            $("#dg").datagrid("options").url = baseUrl + "/lophoc/jsonlichhoc?thang=" + thang + "&nam=" + nam + '&lophoc=<?=$this->lophoc?>';
            $("#dg").datagrid("reload");
        }

    </script>
<?php } ?>
<script type="text/javascript" src="js/lophoc.js"></script>
<table id="dg" style="width: 100%;" url="lophoc/json" class="easyui-datagrid" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="false"
       data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
    <thead>
    <tr>
        <th field="id" width="100">ID</th>
        <th field="malop" width="100">Mã lớp</th>
        <th field="name" width="80">Tên lớp</th>
        <th field="coso" width="150">Cơ sở</th>
        <th field="giaovien" width="150">Giáo viên</th>
        <th field="phutrach" width="150">Phụ trách</th>
        <th field="trogiang" width="150">Trợ giảng</th>
        <th field="hocvien" width="200">Học viên</th>
        <th field="giaotrinh" width="200">Giáo trình</th>
        <th field="khoahoc" width="180">Khóa học</th>
<!--        <th field="ngaybatdau" width="140">Ngày bắt đầu</th>-->
<!--        <th field="ngayketthuc" width="140">Ngày kết thúc</th>-->
        <th field="ghi_chu" width="150">Ghi chú</th>
        <th field="phan_loai" width="160" formatter="format_phanloai">Phân loại</th>
        <th field="phan_loai_2" width="140" formatter="format_phanloai2">Phân loại 2</th>
        <th field="tinhtrang" width="120">Tình trạng</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input class="easyui-combobox" name="giaovien" id="giaovien" style="width: 150px;height:28px;"
           url="common/giaovien" valueField="id" textField="name" prompt="Giáo viên">
    <input class="easyui-combobox" name="khoahoc" id="khoahoc" style="width: 150px;height:28px;"
           url="common/khoahoc" valueField="id" textField="name"  prompt="Khóa học">
    <input class="easyui-combobox" name="phanloai" id="phanloai" style="width: 150px;height:28px;"
           url="lophoc/phanloai1" valueField="id" textField="name" prompt="Phân loại">
    <input class="easyui-combobox" name="tinhtrang" id="tinhtrang" style="width: 150px;height:28px;"
           url="lophoc/tinhtrang" valueField="id" textField="name" prompt="Tình trạng">
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width: 150px;height:28px;" prompt="Tìm kiếm" /> &nbsp;&nbsp;&nbsp;
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
    <button class="easyui-linkbutton" iconCls="icon-cate" plain="false" onclick="lichhoc()">Lịch học</button>
</div>

<div id="dlg" class="easyui-dialog" style="width:740px; padding:10px" closed="true" buttons="#dlg-buttons" modal="true" data-options="resizable:true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Học viên:</td>
                <td>
                    <input class="easyui-combobox" name="hoc_vien" id="hoc_vien" style="width: 220px;height:28px;"
                           url="common/hocvien" valueField="id" textField="name" required>
                </td>
                <td>Đơn hàng:</td>
                <td>
                    <input id="don_hang" name="don_hang" class="easyui-combobox" style="width: 220px;height:28px;" valueField="id" textField="id" required />
                </td>
            </tr>
            <tr>
                <td>Mã lớp:</td>
                <td>
                    <input id="malop" name="malop" class="easyui-textbox" style="width: 220px;height:28px;" readonly/>
                </td>
                <td>Tên lớp học:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required style="width: 220px;height:28px;" />
                </td>
            </tr>
            <tr>
                <td>Giáo viên:</td>
                <td>
                    <input class="easyui-combobox" name="giao_vien" id="giao_vien" style="width: 220px;height:28px;"
                           url="common/giaovien" valueField="id" textField="name" >
                </td>
                <td>Trợ giảng:</td>
                <td>
                    <input class="easyui-combobox" name="tro_giang" id="tro_giang" style="width: 220px;height:28px;"
                           url="common/trogiang" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Phụ trách:</td>
                <td>
                    <input class="easyui-combobox" name="phu_trach" id="phu_trach" style="width: 220px;height:28px;"
                           url="common/nhanvien" valueField="id" textField="name" >
                </td>
                <td>Chi nhánh:</td>
                <td>
                    <input class="easyui-combobox" name="chi_nhanh" id="chi_nhanh" style="width: 220px;height:28px;"
                           url="common/chinhanh" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Giáo trình:</td>
                <td>
                    <input class="easyui-combobox" name="giao_trinh" id="giao_trinh" style="width: 220px;height:28px;"
                           url="common/giaotrinh" valueField="id" textField="name" >
                </td>
                <td>Khóa học:</td>
                <td>
                    <input class="easyui-combobox" name="khoa_hoc" id="khoa_hoc" style="width: 220px;height:28px;" required
                           url="common/khoahoc" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Số buổi:</td>
                <td>
                    <input class="easyui-textbox" name="so_buoi" id="so_buoi" style="width: 220px;height:28px;">
                </td>
                <td>Số buổi km:</td>
                <td>
                    <input class="easyui-textbox" name="buoi_khuyen_mai" id="buoi_khuyen_mai" style="width: 220px;height:28px;">
                </td>
            </tr>
            <tr>
                <td>Ngày bắt đầu:</td>
                <td>
                    <input class="easyui-datebox" name="ngaybatdau" id="ngaybatdau" style="width: 220px;height:28px;" required >
                </td>
                <td>Buổi học:</td>
                <td>
                    <input class="easyui-combobox" name="buoihoc[]" id="buoihoc" style="width: 220px;height:28px;"
                           multiple="true"
                           url="common/thu" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Phân loại 1:</td>
                <td>
                    <input class="easyui-combobox" name="phan_loai" id="phan_loai" style="width: 220px;height:28px;" required
                           url="lophoc/phanloai1" valueField="id" textField="name" >
                </td>
                <td>Phân loại 2:</td>
                <td>
                    <input class="easyui-combobox" name="phan_loai_2" id="phan_loai_2" style="width: 220px;height:28px;" required
                            valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Giờ học:</td>
                <td>
                    <input id="giohoc" name="giohoc" class="easyui-timespinner" style="width: 220px;height:28px;" />
                </td>
                <td>Thời lượng:</td>
                <td>
                    <input id="thoi_luong" name="thoi_luong" class="easyui-numberbox" style="width: 220px;height:28px;" required/>
                </td>
            </tr>
            <tr>
                <td>Ghi chú:</td>
                <td>
                    <input id="ghi_chu" name="ghi_chu" class="easyui-textbox" style="width: 220px;height:28px;" />
                </td>
            </tr>
            <tr>
                <td id="trtinhtrang1" style="display: none;">Tình trạng:</td>
                <td id="trtinhtrang2" style="display: none;">
                    <input class="easyui-combobox" name="tinh_trang" id="tinh_trang" style="width: 220px;height:28px;"
                           url="lophoc/tinhtrang" valueField="id" textField="name" >
                </td>
                
                <input name="so_tien" id="so_tien" type="hidden">
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>


<div id="dlg-hocvien" class="easyui-dialog" style="width:800px; padding:10px;height: 400px;" closed="true"  buttons="#dlg-buttons-hocvien" modal="true" data-options="resizable:true">
    <table id="dg-hocvien" style="width: 100%;"  class="easyui-datagrid" toolbar="#toolbar1" pagination="false"
           idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="false"
           data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="10">
        <thead>
        <tr>
            <th field="ma_hoc_vien" width="120">Mã học viên</th>
            <th field="name" width="180">Họ tên</th>
            <th field="e_name" width="180">Tên tiếng Anh</th>
            <th field="dien_thoai" width="150">Điện thoại</th>
<!--            <th field="lophoc" width="180">Lớp học</th>-->
            <th field="khachhang" width="200">Khách hàng</th>
            <th field="dienthoaikh" width="150">Điện thoại</th>
        </tr>
        </thead>
    </table>
</div>
<div id="dlg-buttons-hocvien">
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-hocvien').dialog('close')">Bỏ qua</button>
</div>



<div id="dlg-nhap" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true" buttons="#dlg-buttons-nhap" modal="true" iconCls="icon-thanhpho">
    <form id="fm-nhap" method="post" enctype="multipart/form-data" action="lichhoc/import">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Tải file:</div>
                </td>
                <td>
                    <input id="file"  name="file" class="easyui-filebox" style="width: 250px;height:28px;" prompt="Tải file excel..."/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-nhap">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="nhapexel()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-nhap').dialog('close')">Bỏ qua</button>
</div>

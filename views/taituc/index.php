<script type="text/javascript" src="js/taituc.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="taituc/json" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-thuonghieu"
       data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
    <tr>
        <th field="khachhang" width="250">Khách hàng</th>
        <th field="hocvien" width="220">Học viên</th>
        <th field="khoahoc" width="150">Khóa học</th>
        <th field="tonghocphi" width="120" align="right" formatter="CurrencyFormatted">Tổng học phí</th>
        <th field="dathu" width="120" align="right" formatter="CurrencyFormatted">Đã thu</th>
        <th field="canthu" width="120" align="right" formatter="CurrencyFormatted">Cần thu</th>
        <th field="ngaydangky" width="120" align="right">Ngày đăng ký</th>
        <th field="phanloaisale" width="140" formatter="phanloai">Phân loại</th>
        <th field="nhanvien" width="200">Nhân viên phụ trách</th>
    </tr>
    </thead>
</table>

<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input id="ngaybd" name="ngaybd" class="easyui-datebox" style="width:120px" value="" prompt="Từ ngày"/>
    <input id="ngaykt" name="ngaykt" class="easyui-datebox" style="width:120px" value="" prompt="Đến ngày"/>
    <input name="phanloai" id="phanloai" class="easyui-combobox" style="width:150px" panelHeight="auto"
        data-options="valueField:'id',textField:'text',
            data:[{'id':'2','text':'Tái tục trải nghiệm'},
                        {'id':'3','text':'Tái tục new'}]" prompt="Phân loại">
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>&nbsp;&nbsp;
    <?php
    if (count($this->tong) > 0) {
        
        ?>
        <span style="font-weight: bold;color: blue">Tổng hv tái tục new: </span>
        <span style="font-weight: bold;color: darkorange"><?= number_format($this->tong['ttnew']) ?></span>
        <span style="font-weight: bold;color: blue">Tổng hv tái tục trải nghiệm : </span>
        <span style="font-weight: bold;color: darkorange"><?= number_format($this->tong['tttrainghiem']) ?></span>
        <span style="font-weight: bold;color: blue">Tổng học phí: </span>
        <span style="font-weight: bold;color: darkorange"><?= number_format($this->tong['tonghocphi']) ?></span>
    <?php } ?>
</div>

<script type="text/javascript" src="js/thuengoai.js"></script>
<table
    id="dg"
    style="width: 100%;"
    class="easyui-datagrid"
    url="thuengoai/json"
    toolbar="#toolbar"
    pagination="true"
    idField="id"
    rownumbers="true"
    fitColumns="false"
    singleSelect="true"
    nowrap="true"
    iconCls="icon-dichvu"
    data-options="border:false,fit:true"
    pageSize="30"
    showFooter="true"
>
    <thead>
        <tr>
            <th field="name" width="150" sortable="true">Tên dịch vụ</th>
            <th field="khachhang" width="150" sortable="true">Nhà cung cấp</th>
            <th field="ngaybd" width="120" sortable="true">Ngày bắt đầu</th>
            <th field="ngaykt" width="120" sortable="true" formatter="format_canhbao">Ngày kết thúc</th>
            <th field="dichvu" width="150" sortable="true">Dịch vụ</th>
            <th field="so_tien" width="100" align="right" formatter="CurrencyFormatted">Số tiền</th>
            <th field="link" width="250" sortable="true">Tên miền</th>
            <th field="ghi_chu" width="300">Ghi chú</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top: 5px; padding-bottom: 5px;">
    <!-- <span style="color: blue; font-weight: bold; margin-right: 20px;">DỊCH VỤ THUÊ NGOÀI</span> -->
    <select class="easyui-combobox" name="tinhtrang" id="tinhtrang" style="width: 176px;">
        <option value="1" selected="selected">Đang sử dụng</option>
        <option value="0">Đã hủy</option>
    </select>
    <input id="dichvu" name="dichvu" class="easyui-combobox" style="width: 176px; height: 28px;" selectOnNavigation="false" prompt="Dịch vụ" url="common/loaidichvu" valueField="id" textField="name" />
    <input id="ncc" name="ncc" class="easyui-combobox" style="width: 176px; height: 28px;" selectOnNavigation="false" prompt="Nhà cung cấp" url="common/nhacungcap" valueField="id" textField="name" />
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Tìm kiếm" /> &nbsp;&nbsp;&nbsp;
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
      foreach ($this->funs as $item) echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> '; ?>
    <button class="easyui-linkbutton" iconCls="icon-excel" plain="false" onclick="xuatfile()">Xuất excel</button>
</div>

<div id="dlg" class="easyui-dialog" style="width: 700px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên dịch vụ:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" style="width: 200px; height: 28px;" required  />
                </td>
                <td>Tình trạng:</td>
                <td>
                    <select class="easyui-combobox" name="tinh_trang" id="tinh_trang" style="width: 200px; height: 28px;">
                        <option value="1" selected="selected">Đang sử dụng</option>
                        <option value="0">Đã hủy</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Nhà cung cấp:</td>
                <td>
                    <input id="khach_hang" name="khach_hang" class="easyui-combobox" style="width: 200px; height: 28px;" selectOnNavigation="false" url="common/nhacungcap" valueField="id" textField="name" />
                </td>
                <td>Dịch vụ:</td>
                <td>
                    <input id="product" name="product" class="easyui-combobox" style="width: 200px; height: 28px;" selectOnNavigation="false" url="common/loaidichvu" valueField="id" textField="name" />
                </td>
            </tr>
            <tr>
                <td>Ngày bắt đầu:</td>
                <td>
                    <input id="ngaybd" name="ngaybd" class="easyui-datebox" style="width: 200px; height: 28px;" required />
                </td>
                <td>Ngày kết thúc:</td>
                <td>
                    <input id="ngaykt" name="ngaykt" class="easyui-datebox" style="width: 200px; height: 28px;" required />
                </td>
            </tr>
            <tr>
                <td>Tên miền/IP:</td>
                <td>
                    <input id="link" name="link" class="easyui-textbox" style="width: 200px; height: 28px;" />
                </td>
                <td>Số tiền</td>
                <td>
                    <input id="so_tien" name="so_tien" class="easyui-numberbox" style="width: 200px; height: 28px;" groupSeparator="," required />
                </td>
            </tr>
            <!-- <tr>
                <td>Tình trạng:</td>
                <td>
                    <select class="easyui-combobox" name="tinh_trang" id="tinh_trang" style="width: 200px; height: 28px;">
                        <option value="1" selected="selected">Đang sử dụng</option>
                        <option value="0">Đã hủy</option>
                    </select>
                </td>
                <td>Tài nguyên:</td>
                <td>
                    <select class="easyui-combobox" name="hoa_don" id="hoa_don" style="width: 200px; height: 28px;" required>
                        <option value="0">Không</option>
                        <option value="1">Có </option>
                    </select>
                </td>
            </tr> -->
            <tr>
                <td>Mô tả:</td>
                <td>
                    <input class="easyui-textbox" multiline="true" style="width: 200px; height: 60px;" id="mo_ta" name="mo_ta" />
                </td>
                <td>Ghi chú:</td>
                <td>
                    <input class="easyui-textbox" multiline="true" style="width: 200px; height: 60px;" id="ghi_chu" name="ghi_chu" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-giahan" class="easyui-dialog" style="width: 370px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons-2" modal="true">
    <form id="fm-giahan" method="post" action="thuengoai/giahan">
        <input type="hidden" name="id" id="id">
        <input type="hidden" name="khachhang" id="khachhang">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Gia hạn đến:</td>
                <td>
                    <input id="ngaygiahan" name="ngaygiahan" class="easyui-datebox" style="width: 200px; height: 28px;" required />
                </td>
            </tr><tr>
                <td>Số tiền:</td>
                <td>
                    <input id="sotien" name="sotien" class="easyui-numberbox" style="width: 200px; height: 28px;" groupSeparator="," />
                </td>
            </tr><tr>
                <td>Tài khoản:</td>
                <td>
                    <input id="taikhoan"  name="taikhoan" class="easyui-combobox" url="common/taikhoan" style="width: 200px;height:28px;" valueField="id" textField="name" />
                </td>
            </tr><tr>
                <td>Hóa đơn:</td>
                <td>
                  <select class="easyui-combobox" name="hoadon" id="hoadon" style="width: 200px; height: 28px;">
                      <option value="0" >Không</option>
                      <option value="1" selected>Có </option>
                  </select>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="tieptuc()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-giahan').dialog('close')">Bỏ qua</button>
</div>

<script type="text/javascript" src="js/doanhthu.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="doanhthu/jsonmobile" toolbar="#toolbar" pagination="true"
    idField="id" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu" title="<?=$this->thang?>"
    data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
        <tr>
          <th field="ngay" width="100">Ngày giờ</th>
          <th field="khachhang" width="150">Khách hàng</th>
          <th field="so_tien" width="120" align="right" formatter="CurrencyFormatted">Số tiền</th>
          <th field="dien_giai" width="200">Diễn giải</th>
        </tr>
    </thead>
</table>

<div id="dlg-search" class="easyui-dialog" style="width:90%; height: auto; padding: 10px; top:30px" closable="false" closed="true"
    buttons="#dlg-buttons-2" modal="true" title="Tìm kiếm">
    <form id="fm-search" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Team:</td>
                <td>
                    <input class="easyui-combobox" id="team" name="team" panelHeight="auto" value="1"
                      valueField="id" textField="name" data-options="data:[{'id':'1','name':'Tất cả'}]">
                </td>
            </tr><tr>
                <td>Tháng:</td>
                <td>
                    <input id="thang" name="thang" class="easyui-combobox"
                        url="common/thang" valueField="id" textField="name" value="<?php echo date("m") ?>" />
                </td>
            </tr><tr>
                <td>Năm:</td>
                <td>
                    <input id="nam" name="nam" class="easyui-combobox"
                      url="common/nam" valueField="id" textField="name" value="<?php echo date("Y") ?>" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="search()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-search').dialog('close')">Bỏ qua</button>
</div>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="javascript:$('#dlg-search').dialog({closed:false})" style="font-size:14px">
            <img src="libs/jquery/images/search.png" width="32" height="32" />
            <br>Tìm kiếm
        </div>
    </div>

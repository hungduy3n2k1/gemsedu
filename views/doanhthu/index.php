<script type="text/javascript" src="js/doanhthu.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="doanhthu/json" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu"
       data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
    <tr>
        <th field="khachhang" width="250">Khách hàng</th>
        <th field="hocvien" width="220">Học viên</th>
        <th field="phanloaisale" width="140" align="right" formatter="phanloai">Loại đơn hàng</th>
        <th field="tonghocphi" width="120" align="right" formatter="CurrencyFormatted">Tổng học phí</th>
        <th field="dadong" width="120" align="right" formatter="CurrencyFormatted">Đã đóng</th>
        <th field="conlai" width="120" align="right" formatter="CurrencyFormatted">Còn lại</th>
        <th field="doanhthu" width="120" align="right" formatter="CurrencyFormatted" >Doanh thu</th>
        <th field="ngaythanhtoan" width="200" align="right" >Ngày thanh toán</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input id="thang" name="thang" class="easyui-combobox" url="common/thang" valueField="id" textField="name"
           value="<?php echo date("m") ?>" />
    <input id="nam" name="nam" class="easyui-combobox" url="common/nam" valueField="id" textField="name"
           value="<?php echo date("Y") ?>" />
    <input name="loaidh" id="loaidh" class="easyui-combobox" style="width:150px" panelHeight="auto"
        data-options="valueField:'id',textField:'text',
            data:[{'id':'1','text':'New sale'},
                        {'id':'2','text':'Tái tục trải nghiệm'},
                        {'id':'3','text':'Tái tục new'}]" prompt="Loại đơn hàng">
    <input id="khachhang" name="khachhang" class="easyui-textbox" style="width: 200px;height:28px;" prompt="Khách hàng"/>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>&nbsp;&nbsp;
    <?php
    if (count($this->tongdoanhthu) > 0) {
        ?>
        <span style="font-weight: bold;color: blue">Tổng học phí: </span>
        <span style="font-weight: bold;color: darkorange"><?= number_format($this->tongdoanhthu[0]['tonghocphi']) ?></span>
        <span style="font-weight: bold;color: blue">Đã đóng: </span>
        <span style="font-weight: bold;color: darkorange"><?= number_format($this->tongdoanhthu[0]['tonghocphi']-$this->tongdoanhthu[0]['conlai']) ?></span>
        <span style="font-weight: bold;color: blue">Còn lại: </span>
        <span style="font-weight: bold;color: darkorange"><?= number_format($this->tongdoanhthu[0]['conlai']) ?></span>
    <?php } ?>
</div>


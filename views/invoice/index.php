<style type="text/css">
    .datagrid-footer .datagrid-row{
        font-weight: bold;
        color: blue;
    }
</style>
<script type="text/javascript" src="public/easyui/datagrid-cellediting.js"></script>
<script type="text/javascript" src="js/invoice.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="invoice/json" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="false" iconCls="icon-dichvu"
       data-options="border:false,fit:true" showFooter="true" pageSize="30">
    <thead>
    <tr>
        <th field="soinv" width="80">Số inv</th>
        <th field="khachhang" width="180">Khách hàng</th>
        <th field="don_hang" width="80">Đơn hàng</th>
        <th field="sobuoi" width="69" align="center">Số buổi</th>
        <th field="tongtien" width="120" align="right" formatter="CurrencyFormatted">Tổng tiền</th>
        <th field="dot_thanh_toan" width="60" align="center">Đợt TT</th>
        <th field="so_tien" width="120" align="right" formatter="CurrencyFormatted">Số tiền</th>
        <th field="dathanhtoan" width="120" align="right" formatter="CurrencyFormatted">Đã thanh toán</th>
        <th field="du_no" width="120" align="right" formatter="CurrencyFormatted">Còn lại</th>
        <th field="ngay" width="100" align="center">Ngày TT</th>
        <th field="tinh_trang" width="140" formatter="tinhtrang">Tình trạng</th>
        <th field="ghi_chu" width="200">Ghi chú</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    Từ ngày: <input id="tungay" name="tungay" class="easyui-datebox" style="width:120px"
                    value="<?php //echo date(" d/m/Y ", strtotime('first day of this month')) ?>"/>
    Đến ngày: <input id="denngay" name="denngay" class="easyui-datebox" style="width:120px"
                     value="<?php //echo date(" d/m/Y ") ?>"/>
    <input id="khachhang" name="khachhang" class="easyui-combobox" url="common/khachhang" style="width: 180px;"
           valueField="id" textField="name" prompt="Khách hàng"/>
    <!--    <input id="nhanvien" name="nhanvien" class="easyui-combobox" url="invoice/nhanvien" style="width: 180px;"-->
    <!--           valueField="id" textField="name" prompt="Nhân viên"/>-->
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
</div>
<div id="dlg" style="width:800px;height:auto;padding:10px" closable="false" closed="true" buttons="#dlg-buttons"
     modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Khách hàng:</div>
                </td>
                <td>
                    <input id="khach_hang" name="khach_hang" class="easyui-combobox" url="common/khachhang"
                           style="width: 200px;height:28px;" valueField="id" textField="name"/>
                </td>
                <td>
                    <div>Đơn hàng:</div>
                </td>
                <td>
                    <input id="don_hang" name="don_hang" class="easyui-combobox" style="width: 200px;height:28px;"
                           valueField="id" textField="id" required/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Học viên:</div>
                </td>
                <td>
                    <input id="hocvien" name="hocvien" class="easyui-textbox" style="width: 200px;height:28px;"
                           readonly/>
                </td>
                <td>
                    <div>Khóa học:</div>
                </td>
                <td>
                    <input id="khoahoc" name="khoahoc" class="easyui-combobox" url="common/khoahoc"
                           style="width: 200px;height:28px;" valueField="id" textField="name" readonly/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Số buổi:</div>
                </td>
                <td>
                    <input id="sobuoi" name="sobuoi" class="easyui-numberbox" style="width: 200px;height:28px;"
                           readonly/>
                </td>
                <td>
                    <div>Tổng tiền:</div>
                </td>
                <td>
                    <input id="tongtien" name="tongtien" style="width:200px; height:28px;" class="easyui-numberbox"
                           groupSeparator="," readonly/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Ngày TT:</div>
                </td>
                <td>
                    <input id="ngay" name="ngay" class="easyui-datebox" style="width: 200px;height:28px;" />
                </td>
                <td>
                    <div>Số tiền:</div>
                </td>
                <td>
                    <input id="so_tien" name="so_tien" style="width:200px; height:28px;" class="easyui-numberbox"
                           groupSeparator="," required/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua
    </button>
</div>

<script type="text/javascript" src="js/menu.js"></script>
<table id="dg" style="width: 100%;" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-hanghoa" data-options="border:false,fit:true"
    pageSize="50" treeField="name">
    <thead>
        <tr>
          <th field="name" width="200" sortable="true">Tên menu</th>
          <th field="link" width="120">Link</th>
          <th field="icon" width="120">Icon</th>
          <th field="thu_tu" width="120" align="center">Thứ tự</th>
          <th field="id" width="120" align="center">Menu ID</th>
          <th field="mobile" width="120" align="center" formatter="mobile">Mobile</th>
          <th field="active" align="center" formatter="active" >Tình trạng</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; padding-top:4px; ">
    <select id="chonloai" name="chonloai" class="easyui-combobox" style="width: 120px;height:28px;" editable="false">
        <option value="1">Admin</option>
        <option value="2">Giáo viên</option>
        <option value="3">Phụ huynh</option>
    </select>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>&nbsp;&nbsp;
    <span style="font-weight:bold; color:blue">Danh mục menu</span>
    <button class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="add()">Thêm</button>
    <button class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="edit()">Sửa</button>
    <button class="easyui-linkbutton" iconCls="icon-no" plain="false" onclick="del()">Xóa</button>
</div>

<div id="dlg" class="easyui-dialog" style="width: 370px; height: auto; padding: 10px;" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Menu cha:</div>
                </td>
                <td>
                    <input id="parentid" name="parentid" class="easyui-combotree" data-options="url:'common/menu',method:'get'" style="width:200px;height:25px;"
                    prompt="Menu cha">
                </td>
            </tr>
            <tr>
                <td>
                    <div>Tên menu:</div>
                </td>
                <td>
                    <input id="name" name="name" data-options="required:true" class="easyui-textbox" style="width: 200px;height:25px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <div>Link:</div>
                </td>
                <td>
                    <input id="link" name="link" class="easyui-textbox" style="width: 200px;height:25px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <div>Icon:</div>
                </td>
                <td>
                    <input id="icon" name="icon" class="easyui-textbox" style="width: 200px;height:25px;" />
                </td>
            </tr>
            <tr>
                <td>
                    <div>Thứ tự:</div>
                </td>
                <td>
                    <input id="thu_tu" name="thu_tu" class="easyui-textbox" style="width: 200px;height:25px;" />
                </td>
            </tr><tr>
                <td>
                    <div>Bật/tắt:</div>
                </td>
                <td>
                    <select id="active" name="active" class="easyui-combobox" style="width: 200px;height:25px;">
                        <option value="1">Bật</option>
                        <option value="0">Tắt</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Mobile:</div>
                </td>
                <td>
                    <select id="mobile" name="mobile" class="easyui-combobox" style="width: 200px;height:25px;">
                        <option value="1">Có</option>
                        <option value="0">Không</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Loại menu:</div>
                </td>
                <td>
                    <select id="loai" name="loai" class="easyui-combobox" style="width: 200px;height:25px;">
                        <option value="1">Admin</option>
                        <option value="2">Giáo viên</option>
                        <option value="3">Phụ huynh</option>
                    </select>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton c6" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Hủy</button>
</div>

<script type="text/javascript" src="js/inbox.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="inbox/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true"
       fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-inbox" data-options="border:false,fit:true"
       pageSize="50">
    <thead>
    <tr>
        <th field="ngaygio" width="150" align="center">Ngày giờ</th>
        <th field="nguoigui" width="150" >Người gửi</th>
        <th field="name" width="500" >Nội dung</th>
        <th field="tinh_trang" formatter="format_tinhtrang" width="150" >Tình trạng</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="height:45px; padding-top:4px; text-align:left">
    <!--    <span style="font-weight:bold; color:blue">Nhật ký xóa, sửa</span>-->
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Từ khóa" style="width:150px" />
    <input class="easyui-combobox" name="nhanvien" id="nhanvien" url="common/nhanvien" valueField="id" textField="name" prompt="Nhân viên">
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>&nbsp;&nbsp;
    <button class="easyui-linkbutton" iconCls="icon-more" plain="false" onclick="dadoc()">Đánh dấu đã đọc</button>&nbsp;&nbsp;
</div>

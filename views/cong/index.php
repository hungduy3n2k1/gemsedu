<script type="text/javascript" src="js/cong.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="cong/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-cong" data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="name" width="180" sortable="true">Tên gọi</th>
            <th field="ky_hieu" width="100">Ký hiệu</th>
            <th field="nhom" width="150" formatter="nhom">Nhóm</th>
            <th field="id" width="100" align="center">ID</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <span style="color:blue; font-weight:bold; margin-right:20px">Định nghĩa chấm công</span>
    <button class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="add()">Thêm mới</button>
    <button class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="edit()">Sửa thông tin</button>
    <button class="easyui-linkbutton" iconCls="icon-no" plain="false" onclick="del()">Xóa</button>
</div>

<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên gọi:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px;height:25px;" />
                </td>
            </tr><tr>
                <td>Ký hiệu:</td>
                <td>
                    <input class="easyui-textbox" style="width:200px; height:25px" id="ky_hieu" name="ky_hieu">
                </td>
            </tr><tr>
                <td>Nhóm:</td>
                <td>
                    <input class="easyui-combobox" style="width:200px; height:25px" id="nhom" name="nhom" url="cong/nhom"
                    valueField="id" textField="name" required>
                </td>
            </tr><!-- <tr>
                <td>Mô tả:</td>
                <td>
                    <input class="easyui-textbox" multiline="true" style="width:200px; height:100px" id="ghi_chu" name="ghi_chu">
                </td>
            </tr> -->
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

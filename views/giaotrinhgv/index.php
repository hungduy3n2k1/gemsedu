<script type="text/javascript" src="js/giaotrinhgv.js"></script>
<script type="text/javascript" src="public/tinymce/tinymce.min.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="giaotrinhgv/json" toolbar="#toolbar"
       pagination="true" idField="id" rownumbers="true" fitColumns="true"
       singleSelect="true" nowrap="true" iconCls="icon-giaotrinh" data-options="border:false,fit:true"
    pageSize="30">
    <thead>
        <tr>
<!--            <th field="ngaygio" width="120">Ngày giờ</th>-->
            <th field="level" width="250" >Chương trình</th>
            <th field="name" width="250" >Tên giáo trình</th>
<!--            <th field="phan_loai" width="240" >Phân loại</th>-->
            <th field="unit" width="100" formatter="format_zero">Unit</th>
            <th field="lesson" width="100" formatter="format_zero">Lesson</th>
            <th field="review" width="100" formatter="format_zero">Review</th>
            <th field="bonus" width="100" formatter="format_zero">Bonus</th>
            <th field="test" width="100" formatter="format_zero">Test</th>
<!--            <th field="nguoinhap" width="140"  align="center" >Người nhập</th>-->
            <th field="link" width="120"  align="center" formatter="download">Tải về</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; padding-top:4px; text-align:left">
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width: 200px;height:25px;" prompt="Từ khóa" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
</div>
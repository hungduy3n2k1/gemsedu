<table id="dg" style="width: 100%;" class="easyui-datagrid" url="tongbuoihoc/json" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="true"
       nowrap="true" iconCls="icon-phanloai" showFooter="true"
       data-options="border:false,fit:true" pageSize="30"> 
    <thead>
    <tr>
        <th field="hocvien" width="220">Học viên</th>
        <th field="tongbuoi" width="100" align="right" >Tổng buổi</th>
        <th field="dahoc" width="100" align="right">Đã học</th>
        <th field="conlai" width="100" align="right">Còn lại</th>
        <th field="doanhthu" width="120" align="right" formatter="CurrencyFormatted">Doanh thu</th>
        <th field="phanloai" width="120" align="center">Khóa học</th>
        <th field="tongbuoicuakhoa" width="180" align="right">Tổng số buổi học của khóa</th>
        <th field="tongtienkhoa" width="120" align="right" formatter="CurrencyFormatted">Tổng số tiền</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input id="thang" name="thang" class="easyui-combobox" url="common/thang" valueField="id" textField="name"
           value="<?php echo date("m") ?>" />
    <input id="nam" name="nam" class="easyui-combobox" url="common/nam" valueField="id" textField="name"
           value="<?php echo date("Y") ?>" />
    <input class="easyui-textbox" name="hocvien" id="hocvien" style="width: 220px;"
          prompt="Tên học viên" >
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
        <span style="font-weight: bold;color: blue">Tổng doanh thu: </span>
        <span style="font-weight: bold;color: darkorange" id="tongdoanhthu"><?= number_format($this->tong['tongdoanhthu']) ?></span>
        <span style="font-weight: bold;color: blue">Tổng buổi học: </span>
        <span style="font-weight: bold;color: darkorange" id="tongbuoihoc"><?= number_format($this->tong['tongbuoihoc']) ?></span>

</div>
<script>
    function timkiem() {
        var thang = $('#thang').datebox('getValue');
        var nam = $('#nam').datebox('getValue');
        var hocvien = $('#hocvien').textbox('getValue');
        if (thang.length > 0 || nam.length > 0 || hocvien.length > 0)
            $('#dg').datagrid('options').url = 'tongbuoihoc/json?thang=' + thang + '&nam=' + nam + '&hocvien=' + hocvien;
        else
            $('#dg').datagrid('options').url = 'tongbuoihoc/json?thang=' + thang + '&nam=' + nam;
        if ($('#dlg-search').dialog)
            $('#dlg-search').dialog('close');
        $('#dg').datagrid('reload');

        $.post('tongbuoihoc/getTong', {thang: thang,nam:nam,hocvien:hocvien}, function(result) {
            // alert(Comma(result.tongdoanhthu));
            $('#tongdoanhthu').html(Comma(result.tongdoanhthu));
            $('#tongbuoihoc').html(Comma(result.tongbuoihoc));
    }, 'json');
    }
</script>
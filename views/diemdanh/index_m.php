<script type="text/javascript" src="js/diemdanh.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" pagination="false" toolbar="toolbar1"
       idField="id" rownumbers="true" fitColumns="true" singleSelect="false" nowrap="false"
       data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
    <thead>
    <tr>
        <th field="ck" checkbox="true"></th>
        <th field="name" width="180">Full name</th>
        <th field="e_name" width="150">Nickname</th>
    </tr>
    </thead>
</table>

<div id="dlg" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px" title="Choose a class"
     data-options="inline:true,modal:true,closed:false,closable:false">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Class:</td>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc" style="width: 100%;height:28px;"
                           url="diemdanh/lichhoc" valueField="id" textField="name" >
                </td>
            </tr>
        </table>
    </form>
    <div class="dialog-button" style="text-align:center">
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="tieptuc()">OK</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg').dialog('close')">Close</a>
    </div>
</div>

<div id="dlg-link" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px" title="Post a link"
     data-options="inline:true,modal:true,closed:true,closable:true">
    <form id="fm-link" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    Link:
                </td>
                <td>
                    <input class="easyui-textbox" name="link" id="link" style="width: 100%;height:28px;">
                </td>
            </tr>
        </table>
    </form>
    <div class="dialog-button" style="text-align:center">
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="checkout1()">OK</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg-link').dialog('close')">Close</a>
    </div>
</div>

<form id="fm-diemdanh" method="post" enctype="multipart/form-data" action="diemdanh/diemdanh">
    <input type="hidden" id="data-hocvien" name="data-hocvien">
    <input type="hidden" id="lichhoc" name="lichhoc">
    <input type="hidden" id="giovao" name="giovao">
    <input type="hidden" id="phanloai" name="phanloai">
    <input type="hidden" id="tinhtranghuy" name="tinhtranghuy">
</form>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:70,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true" style="padding-bottom: 5px;">
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="chonlop()" style="font-size:14px">
                <img src='libs/jquery/images/search.png' width="32" height="32" />
                <br>Choose a class
            </div>
        </div>
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="diemdanh()" style="font-size:14px;font-weight: bold;">
                <img src='libs/jquery/images/add.png' width="32" height="32" />
                <br>Check<br>attendance
            </div>
        </div>
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="checkout()" style="font-size:14px">
                <img src='libs/jquery/images/lock.png' width="32" height="32" />
                <br> Checkout
            </div>
        </div>
        <div style="padding:10px" id="huylich">
            <div class="panel-header tt-inner" onClick="huylich()" style="font-size:14px">
                <img src='libs/jquery/images/delete.png' width="32" height="32" />
                <br> Cancel
            </div>
        </div>


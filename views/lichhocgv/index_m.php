<script type="text/javascript" src="js/lichhocgv.js"></script>
<script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-groupview.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="lichhocgv/json"
       fitColumns="false" singleSelect="true" nowrap="true" showFooter="false"
       data-options="border:false,fit:true,iconCls:'icon-nhaphang', view:groupview,
      groupField:'lophoc',
      groupFormatter:function(value,rows){
          return value + ' - ' + rows.length + ' Item(s)';
    }
    "  >
    <thead>
    <tr>
        <th field="id" width="40">Lớp</th>
        <th field="ngaygio" width="80" align="center">Ngày</th>
        <th field="giohoc" width="50" align="center">Giờ</th>
        <th field="thoi_luong" width="60" align="center">Thời lượng</th>
        <th field="thoi_luong_moi" width="60" >Thời lượng mới</th>
        <th field="hocvien" width="100" formatter="format_hocvien" >Học viên</th>
        <th field="tinh_trang" width="100" align="center" formatter="tinhtrang">Tình trạng</th>
    </tr>
    </thead>
</table>

<div id="dlg" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px" title="Choose a class"
     data-options="inline:true,modal:true,closed:true,closable:true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Class:</td>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc" style="width: 100%;height:28px;"
                           url="lichhocgv/lophoc" valueField="id" textField="name" >
                </td>
            </tr>
        </table>
    </form>
    <div class="dialog-button" style="text-align:center">
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="tieptuc()">OK</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg').dialog('close')">Close</a>
    </div>
</div>

<div id="dlg-lichhoc" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons-lichhoc" modal="true">
    <form id="fm-lichhoc" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Lớp học:</td>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc1" style="width: 200px;height:28px;" readonly
                           url="common/lophoc" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Thứ:</td>
                <td>
                    <input id="thu" name="thu" class="easyui-textbox" required="required"
                           style="width: 200px;height:28px;" readonly/>
                </td>
            </tr>
            <tr>
                <td>Ngày học:</td>
                <td>
                    <input id="ngaygio" name="ngaygio" class="easyui-datebox" required="required" style="width: 200px;height:28px;" />
                </td>
            </tr><tr>
                <td>Giờ học:</td>
                <td>
                    <input id="gio" name="gio" class="easyui-timespinner" style="width: 200px;height:28px;" required/>
                </td>
            </tr>
            <tr>
                <td>Thời lượng:</td>
                <td>
                    <input id="thoi_luong" name="thoi_luong" class="easyui-numberbox" style="width: 200px;height:28px;" readonly
                           required/>
                </td>
            </tr>
            <tr id="suatuongtu">
                <td>Edit multi:</td>
                <td>
                    <input type="checkbox" id="editmulti" name="editmulti">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-lichhoc">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save(1)">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-lichhoc').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-edittime" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons-edittime" modal="true">
    <form id="fm-edittime" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Class:</td>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc2" style="width: 200px;height:28px;" readonly
                           url="common/lophoc" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Day:</td>
                <td>
                    <input id="thu1" name="thu" class="easyui-textbox"
                           style="width: 200px;height:28px;" readonly/>
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td>
                    <input id="ngaygio1" name="ngaygio" class="easyui-datebox" required="required" style="width: 200px;height:28px;" readonly />
                </td>
            </tr><tr>
                <td>Hours:</td>
                <td>
                    <input id="gio1" name="gio" class="easyui-timespinner" style="width: 200px;height:28px;" readonly/>
                </td>
            </tr>
            <tr>
                <td>Times:</td>
                <td>
                    <input id="thoi_luong1" name="thoi_luong" class="easyui-numberbox" style="width: 200px;height:28px;"
                           readonly/>
                </td>
            </tr>
            <tr>
                <td>New Times:</td>
                <td>
                    <input id="thoi_luong_moi" name="thoi_luong_moi" class="easyui-numberbox" style="width: 200px;height:28px;"/>
                </td>
            </tr>
<!--            <tr id="suatuongtu">-->
<!--                <td>Sửa tương tự:</td>-->
<!--                <td>-->
<!--                    <input type="checkbox" id="editmulti" name="editmulti">-->
<!--                </td>-->
<!--            </tr>-->
        </table>
    </form>
</div>
<div id="dlg-buttons-edittime">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save(2)">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-edittime').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-add" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false"
     closed="true"
     buttons="#dlg-buttons-add" modal="true">
    <form id="fm-add" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Class:</td>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc2" style="width: 200px;height:28px;"
                           url="lichhocgv/lophoc?phanloai=1" valueField="id" textField="name">
                </td>
                <input type="hidden" id="sobuoi" name="sobuoi"/>
                <input type="hidden" id="buoikm" name="buoikm"/>
                <input type="hidden" id="hocvien" name="hocvien"/>
                <input type="hidden" id="phanloai" name="phanloai"/>
            </tr>
            <tr>
                <td>Hour:</td>
                <td>
                    <input id="gio2" name="gio" class="easyui-timespinner" style="width: 200px;height:28px;" required/>
                </td>
            </tr>
            <tr>
                <td>Time:</td>
                <td>
                    <input id="thoi_luong2" name="thoi_luong" class="easyui-numberbox" style="width: 200px;height:28px;"
                           required/>
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td>
                    <input id="ngaygio2" name="ngaygio" class="easyui-datebox" required="required"
                           style="width: 200px;height:28px;"/>
                </td>
            </tr>
            <tr>
                <td>Day:</td>
                <td>
                    <input class="easyui-combobox" name="buoihoc[]" id="buoihoc" style="width: 200px;height:28px;"
                           multiple="true"
                           url="common/thu" valueField="id" textField="name">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-add">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="saveadd()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-add').dialog('close')">Bỏ qua
    </button>
</div>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:70,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true" style="padding-bottom: 5px;">
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="chonlop()"  style="font-size:14px;font-weight: bold;">
                <img src='libs/jquery/images/search.png' width="32" height="32" />
                <br>Seach
            </div>
        </div>
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="add()"  style="font-size:14px;font-weight: bold;">
                <img src='libs/jquery/images/add.png' width="32" height="32" />
                <br>Add
            </div>
        </div>
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="edit()" style="font-size:14px;font-weight: bold;">
                <img src='libs/jquery/images/edit.png' width="32" height="32" />
                <br>Edit
            </div>
        </div>
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="edittime()"  style="font-size:14px;font-weight: bold;">
                <img src='libs/jquery/images/modem.png' width="32" height="32" />
                <br> Edit Time
            </div>
        </div>


<table id="dg" style="width: 100%;" class="easyui-datagrid" url="congno/json" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu"
       data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
    <tr>
        <!--            <th field="ngay" width="120">Ngày giờ</th>-->
        <th field="khachhang" width="220">Khách hàng</th>
        <th field="dienthoaikh" width="160">Điện thoại</th>
        <th field="hocvien" width="220">Học viên</th>
        <th field="hanthanhtoan" width="160">Hạn thanh toán</th>
        <th field="so_tien" width="120" align="right" formatter="CurrencyFormatted">Cần thu</th>
        <th field="dathanhtoan" width="120" align="right" formatter="CurrencyFormatted">Đã thu</th>
        <th field="du_no" width="120" align="right" formatter="CurrencyFormatted">Còn lại</th>
        <th field="ngaythanhtoan" width="160">Ngày thanh toán</th>
        <th field="ghi_chu" width="200">Ghi chú</th>
        <!--            <th field="ghi_chu" width="300">Ghi chú</th>-->
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input id="thang" name="thang" class="easyui-combobox" url="common/thang" valueField="id" textField="name"
           value="<?php echo date("m") ?>"/>
    <input id="nam" name="nam" class="easyui-combobox" url="common/nam" valueField="id" textField="name"
           value="<?php echo date("Y") ?>"/>
    <input id="khachhang" name="khachhang" class="easyui-combobox" url="common/khachhang" valueField="id" textField="name"
           style="width:200px" prompt="Khách hàng"/>
    <input id="loaidh" name="loaidh" class="easyui-combobox" style="width:200px" prompt="Loại đơn hàng" panelHeight="auto"
            data-options="valueField:'id',textField:'text',
                    data:[{'id':'1','text':'Online'},
                        {'id':'2','text':'Offline'}]" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>&nbsp;&nbsp;
    <span style="font-weight: bold;color: blue">Tổng công nợ: </span>
    <span style="font-weight: bold;color: darkorange" id="tongcongno"><?= number_format($this->tongcongno['tongcongno']) ?></span>
    <span style="font-weight: bold;color: blue">Đã thanh toán: </span>
    <span style="font-weight: bold;color: darkorange" id="dathanhtoan"><?= number_format($this->tongcongno['dathanhtoan']) ?></span>
    <span style="font-weight: bold;color: blue">Còn lại: </span>
    <span style="font-weight: bold;color: darkorange" id="conlai"><?= number_format($this->tongcongno['conlai']) ?></span>
</div>
<script>
    function timkiem() {
        var thang = $('#thang').combobox('getValue');
        var nam = $('#nam').combobox('getValue');
        var khachhang = $('#khachhang').combobox('getValue');
        var loaidh = $('#loaidh').combobox('getValue');
        if (thang.length > 0 || nam.length > 0 || khachhang.length > 0 || loaidh.length > 0)
            $('#dg').datagrid('options').url = 'congno/json?khachhang=' + khachhang + '&thang=' + thang + '&nam=' + nam + '&loaidh=' + loaidh;
        else
            $('#dg').datagrid('options').url = 'congno/json';
        $('#dg').datagrid('reload');
        $.post('congno/getTongCongNo', {thang: thang,nam:nam,khachhang:khachhang}, function(result) {
            var result = JSON.parse(result);
            $('#tongcongno').html(Comma(result.tongcongno));
            $('#dathanhtoan').html(Comma(result.dathanhtoan));
            $('#conlai').html(Comma(result.conlai));
        });
    }
</script>

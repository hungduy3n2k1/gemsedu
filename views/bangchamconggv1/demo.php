<table
    id="dg"
    style="width: 100%;"
    class="easyui-datagrid"
    url="bangchamconggv1/jsondemo"
    toolbar="#toolbar"
    pagination="true"
    idField="id"
    rownumbers="true"
    fitColumns="false"
    singleSelect="true"
    nowrap="true"
    iconCls="icon-thanhpho"
    data-options="border:false,fit:true"
    showFooter="true"
    pageSize="50"
></table>
<div id="toolbar" style="height: 40px; padding-top: 4px;">
    <input class="easyui-combobox" style="width:120px"
           url="common/thang" valueField="id" textField="name" editable="false" name="thang" id="thang" value="<?php echo date("m") ?>">
    <input class="easyui-combobox" style="width:120px"
           url="common/nam" valueField="id" textField="name" editable="false" name="nam" id="nam" value="<?php echo date("Y") ?>">
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="searchdemo()">Tìm kiếm</button>
    <button class="easyui-linkbutton" iconCls="icon-phieuthu" plain="false" onclick="window.location.href=baseUrl+'/bangchamconggv1/index'">Main</button>
    &nbsp;&nbsp;&nbsp;
</div>
<script>
    $(function () {
        var thang = $('#thang').combobox('getValue');
        var nam = $('#nam').combobox('getValue');
        var column = [
            {field: 'tenlop', title: 'Class', width: 100},
            {field: 'tongbuoi', title: 'Times', width: 60}
        ];
        for (var i = 1; i <= 31; i++) {
            if (i < 10) {
                i = '0' + i;
            }
            var dt = new Date(nam + '-' + thang + '-' + i);
            if (dt.getDay() == 0) {
                column.push({field: 'ngay_' + i, title: 'Sunday', width: 60, align: 'center'});
            } else if (dt.getDay() == 1) {
                column.push({field: 'ngay_' + i, title: 'Monday<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 2) {
                column.push({field: 'ngay_' + i, title: 'Tuesday<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 3) {
                column.push({field: 'ngay_' + i, title: 'Wednesday<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 4) {
                column.push({field: 'ngay_' + i, title: 'Thursday<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 5) {
                column.push({field: 'ngay_' + i, title: 'Friday<br>' + i, width: 60, align: 'center'});
            } else {
                column.push({field: 'ngay_' + i, title: 'Sarturday<br>' + i, width: 60, align: 'center'});
            }
        }
        $('#dg').datagrid({
            columns: [column]
        });
    });

    function searchdemo() {
        var thang = $('#thang').combobox('getValue');
        var nam = $('#nam').combobox('getValue');
        var column = [
            {field: 'tenlop', title: 'Class', width: 100},
            {field: 'tonggio', title: 'Times', width: 60}
        ];
        for (var i = 1; i <= 31; i++) {
            if (i < 10) {
                i = '0' + i;
            }
            var dt = new Date(nam + '-' + thang + '-' + i);
            if (dt.getDay() == 0) {
                column.push({field: 'ngay_' + i, title: 'Sunday', width: 60, align: 'center'});
            } else if (dt.getDay() == 1) {
                column.push({field: 'ngay_' + i, title: 'Monday<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 2) {
                column.push({field: 'ngay_' + i, title: 'Tuesday<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 3) {
                column.push({field: 'ngay_' + i, title: 'Wednesday<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 4) {
                column.push({field: 'ngay_' + i, title: 'Thursday<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 5) {
                column.push({field: 'ngay_' + i, title: 'Friday<br>' + i, width: 60, align: 'center'});
            } else {
                column.push({field: 'ngay_' + i, title: 'Sarturday<br>' + i, width: 60, align: 'center'});
            }
        }
        $('#dg').datagrid({
            columns: [column]
        });
        $('#dg').datagrid('options').url = 'bangchamconggv1/jsondemo?thang=' + thang + '&nam=' + nam;
        $('#dg').datagrid('reload');
    }
</script>

<script type="text/javascript" src="js/bangchamconggv1.js"></script>
<table
        id="dg"
        style="width: 100%;"
        class="easyui-datagrid"
        toolbar="#toolbar"
        pagination="false"
        idField="id"
        rownumbers="true"
        fitColumns="false"
        singleSelect="true"
        nowrap="true"
        iconCls="icon-thanhpho"
        data-options="border:false,fit:true"
        showFooter="true"
        pageSize="50"
>
<!--    <thead>-->
<!--    <tr>-->
<!--        <th field="tenlop" width="120">Class</th>-->
<!--        <th field="tonggio" width="60" align="center">Hour</th>-->
<!--        --><?php
//        for ($i = 1; $i <= 31; $i++) {
//            if ($i < 10) {
//                $ngay = "ngay_0" . $i;
//            } else {
//                $ngay = "ngay_" . $i;
//            }
//            ?>
<!--            <th field="--><?//= $ngay ?><!--" width="120" align="center">Ngày --><?//= $i ?><!--</th>-->
<!--            --><?php
//        }
//        ?>
<!--    </tr>-->
<!--    </thead>-->
</table>
<div id="toolbar" style="height: 40px; padding-top: 4px;">
    <input class="easyui-combobox" style="width:120px"
           url="common/thang" valueField="id" textField="name" editable="false" name="thang" id="thang"
           value="<?php echo date("m") ?>">
    <input class="easyui-combobox" style="width:120px"
           url="common/nam" valueField="id" textField="name" editable="false" name="nam" id="nam"
           value="<?php echo date("Y") ?>">
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="search()">Tìm kiếm</button>
    <button class="easyui-linkbutton" iconCls="icon-phieuthu" plain="false"
            onclick="window.location.href=baseUrl+'/bangchamconggv1/demo'">Demo
    </button>
    &nbsp;&nbsp;&nbsp;
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
</div>

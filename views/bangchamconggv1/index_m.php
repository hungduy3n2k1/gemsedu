<script type="text/javascript" src="js/bangchamconggv1.js"></script>
<table
    id="dg"
    style="width: 100%;"
    class="easyui-datagrid"
    url="bangchamconggv1/json?giaovien=<?=$_SESSION['user']['giao_vien']?>"
    toolbar="#toolbar"
    pagination="false"
    idField="id"
    rownumbers="true"
    fitColumns="false"
    singleSelect="true"
    nowrap="true"
    iconCls="icon-thanhpho"
    data-options="border:false,fit:true"
    pageSize="50"
>
</table>

<div id="dlg" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px" title="Choose a class"
     data-options="inline:true,modal:true,closed:true,closable:true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tháng:</td>
                <td>
                    <input class="easyui-combobox" style="width:100%;height: 28px;"
                           url="common/thang" valueField="id" textField="name" editable="false" name="thang" id="thang"
                           value="<?php echo date("m") ?>">
                </td>
                <td>Năm:</td>
                <td>
                    <input class="easyui-combobox" style="width:100%;height: 28px;"
                           url="common/nam" valueField="id" textField="name" editable="false" name="nam" id="nam"
                           value="<?php echo date("Y") ?>">
                </td>
            </tr>
        </table>
    </form>
    <div class="dialog-button" style="text-align:center">
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="tieptuc()">OK</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px"
           onclick="$('#dlg').dialog('close')">Close</a>
    </div>
</div>
<footer>
    <div class="easyui-tabs"
         data-options="tabHeight:70,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true"
         style="padding-bottom: 5px;">
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="timkiem()" style="font-size:14px">
                <img src='libs/jquery/images/search.png' width="32" height="32"/>
                <br>Search
            </div>
        </div>
        <!--        <div style="padding:10px">-->
        <!--            <div class="panel-header tt-inner" onClick="diemdanh()" style="font-size:14px;font-weight: bold;">-->
        <!--                <img src='libs/jquery/images/add.png' width="32" height="32" />-->
        <!--                <br>Check<br>attendance-->
        <!--            </div>-->
        <!--        </div>-->
        <!--        <div style="padding:10px">-->
        <!--            <div class="panel-header tt-inner" onClick="checkout()" style="font-size:14px">-->
        <!--                <img src='libs/jquery/images/lock.png' width="32" height="32" />-->
        <!--                <br> Checkout-->
        <!--            </div>-->
        <!--        </div>-->
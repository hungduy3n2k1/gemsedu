<script type="text/javascript" src="libs/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="js/thongbao.js"></script>

<table
    id="dg"
    style="width: 100%;"
    class="easyui-datagrid"
    url="thongbao/json"
    toolbar="#toolbar"
    pagination="true"
    idField="id"
    rownumbers="true"
    fitColumns="false"
    singleSelect="true"
    nowrap="false"
    iconCls="icon-sokho"
    data-options="border:false,fit:true"
    pageSize="50"
>
    <thead>
        <tr>
            <th field="name" width="200" sortable="true">Tên thông báo</th>
            <th field="ngaygio" width="200">Thời gian bắt đầu</th>
            <th field="ketthuc" width="200">Thời gian kết thúc</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top: 5px; padding-bottom: 5px;">
    <?php
        foreach ($this->funs as $item) echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> '; ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 700px; height: auto; padding: 10px;top: 40px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
      <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
          <tr>
              <td>Tiêu đề</td>
              <td colspan="3">
                  <input id="name" name="name" class="easyui-textbox" required="required" style="width: 560px; height: 28px; " />
              </td>
          </tr><tr>
              <td>Thời gian bắt đầu</td>
              <td>
                  <input id="ngay_gio" name="ngay_gio" class="easyui-datetimebox" style="width: 200px; height: 28px;"
                  required />
              </td>
              <td>Thời gian kết thúc</td>
              <td>
                 <input id="ket_thuc" name="ket_thuc" class="easyui-datetimebox" style="width: 200px; height: 28px;"
                  required />
              </td>
          </tr><tr>
              <td>Nội dung:</td>
              <td colspan="3">
                  <textarea id="noi_dung" name="noi_dung" placeholder="Nội dung chi tiết :" style="width: 540px;" rows="5"></textarea>
              </td>
          </tr>
      </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>
<script>
  tinymce.init({
                mode: "textareas",
                entity_encoding : "raw",
                plugins: ["advlist autolink lists link image charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen textcolor", "media",
                            "insertdatetime media table contextmenu paste jbimages","fullscreen","moxiemanager"],
                image_advtab: true,
                paste_data_images: true,
                browser_spellcheck : true,
                relative_urls:false,
                remove_script_host : false,
                //convert_urls : true,
                image_dimensions: false,
                forced_root_block : false,
                force_br_newlines : true,
                force_p_newlines : false,
                toolbar: " undo redo | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media insertfile |  fontsizeselect | forecolor backcolor | fullscreen"
    });
</script>

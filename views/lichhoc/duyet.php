<script type="text/javascript" src="js/lichhoc.js"></script>
<script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-groupview.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="lichhoc/jsonduyet"
       toolbar="#toolbar" fitColumns="false" singleSelect="true" nowrap="true" showFooter="false"
       data-options="border:false,fit:true,iconCls:'icon-nhaphang', view:groupview,
      groupField:'lophoc',
      groupFormatter:function(value,rows){
          return value + ' - ' + rows.length + ' Item(s)';
    }
    "  >
    <thead>
    <tr>
        <th field="id" width="120">Lớp</th>
        <th field="ngay" width="120" align="center">Ngày</th>
        <th field="gio" width="120" align="center">Giờ</th>
        <th field="giaovien" width="160" >Giáo viên</th>
        <th field="hocvien" width="160" formatter="format_hocvien" >Học viên</th>
        <th field="phonghoc" width="150" >Phòng học</th>
        <th field="thoi_luong" width="80" align="center">Thời lượng</th>
        <th field="thoi_luong_moi" width="80" align="center">Chờ duyệt</th>
        <th field="tinh_trang" width="140" align="center" formatter="tinhtrang">Tình trạng</th>
        <th field="1" width="100" align="center" formatter="format_duyet">...</th>
    </tr>
    </thead>
</table>

<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <button class="easyui-linkbutton" iconCls="icon-undo" plain="false" onclick="pre()">Tháng trước</button>
    <input class="easyui-timespinner" name="giohoc" id="giohoc" style="width: 80px;height:28px;" prompt="Giờ học">
    <input class="easyui-textbox" name="ngay1" id="ngay1" style="width: 30px">
    <input class="easyui-textbox" name="thang" id="thang" value="<?= date('m') ?>" style="width: 30px">
    <input class="easyui-textbox" name="nam" id="nam" value="<?= date('Y') ?>" style="width: 60px">
    <input class="easyui-combobox" name="loai" id="loai" style="width: 150px;height:28px;" prompt="Phân loại"
           panelHeight="auto"
           data-options="data:[{'id':'1','name':'Online'},{'id':'2','name':'Offline'},{'id':'3','name':'Demo'}]" valueField="id"
           textField="name">
    <input class="easyui-combobox" name="giaovien" id="giaovien" style="width: 150px;height:28px;"
           url="common/giaovien" valueField="id" textField="name" prompt="Giáo viên">
    <input class="easyui-combobox" name="phonghoc" id="phonghoc" style="width: 150px;height:28px;"
           url="common/phonghoc" valueField="id" textField="name" prompt="Phòng học">
    <input class="easyui-textbox" name="tenlop" id="tenlop" style="width: 150px;height:28px;" prompt="Tên lớp học">
    <button class="easyui-linkbutton" iconCls="icon-ok" plain="false" onclick="timkiem()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-redo" plain="false" onclick="next()">Tháng sau</button>
    <?php
//    foreach ($this->funs as $item)
//        if($item['link']=='add()')
//        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="duyet()">Duyệt</button> ';
    ?>
    <br>
    <button class="easyui-linkbutton" iconCls="icon-nhaphang" plain="false" onclick="indexview()">Calendar</button>
    <button class="easyui-linkbutton" iconCls="icon-nhaphang" plain="false" onclick="tableview()">Class view</button>
    <button class="easyui-linkbutton" iconCls="icon-phieuthu" plain="false" onclick="dayview()">Day view</button>
    <button class="easyui-linkbutton" iconCls="icon-tip" plain="false" onclick="timeview()">Time view</button>
</div>

<div id="dlg" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Lớp học:</td>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc" style="width: 200px;height:28px;" readonly
                           url="common/lophoc" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Giáo viên:</td>
                <td>
                    <input class="easyui-combobox" name="giao_vien" id="giao_vien" style="width: 200px;height:28px;" readonly
                           url="common/giaovien" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Thứ:</td>
                <td>
                    <input id="thu" name="thu" class="easyui-textbox" required="required"
                           style="width: 200px;height:28px;" readonly/>
                </td>
            </tr>
            <tr>
                <td>Ngày học:</td>
                <td>
                    <input id="ngay" name="ngay" class="easyui-datebox" required="required" style="width: 200px;height:28px;" readonly />
                </td>
            </tr><tr>
                <td>Giờ học:</td>
                <td>
                    <input id="gio" name="gio" class="easyui-timespinner" style="width: 200px;height:28px;" readonly/>
                </td>
            </tr>
            <tr>
                <td>Thời lượng:</td>
                <td>
                    <input id="thoi_luong" name="thoi_luong" class="easyui-numberbox" style="width: 200px;height:28px;"
                           readonly/>
                </td>
            </tr>
            <tr>
                <td>Thời lượng:</td>
                <td>
                    <input id="thoi_luong_moi" name="thoi_luong_moi" class="easyui-numberbox" style="width: 200px;height:28px;"
                           readonly/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-no" onclick="tuchoi()">Từ chối</button>
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="saveduyet()">Duyệt</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>
<script>
    function format_duyet(val,row) {
        return '<button style="color: blue;font-weight: bold;" onclick="duyet('+row.id+','+row.tinh_trang+')">Duyệt</button>';
    }
    $(function () {
        $('#dg').datagrid({
            onSelect: function (index,row) {
                $(this).datagrid('unselectRow', index);
            }
        });
    });
    function duyet(id,tinhtrang) {
        if (id>0) {
            if(tinhtrang==1) {
                $('#dlg').dialog({
                    title: '&nbsp;Duyệt lịch học',
                    iconCls: 'icon-edit',
                    closed: false
                });
                $.post(baseUrl + '/lichhoc/getrow', {id: id}, function (result) {
                    if (result.success) {
                        var row = result.data;
                        $('#fm').form('load', row);
                        url = baseUrl + '/lichhoc/duyetlich?id=' + id;
                    } else {
                        show_messager(result.msg);
                    }
                }, 'json');
            }else{
                $.messager.alert('Notice',"Lịch học đang diễn ra hoặc đã kết thúc", 'warning');
            }
        } else {
            $.messager.alert('Notice','Choose a schedule','warning');
        }
    }

    function saveduyet() {
        // document.getElementById('fm').action=url;
        // document.getElementById('fm').submit();
        url = url+'&duyet=1';
     //   alert(url);
        $('#fm').form('submit', {
            url: url,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    $('#dlg').dialog('close');
                    $('#dg').datagrid('reload');
                    $('#dg').datagrid('clearSelections');
                } else {
                    show_messager(result.msg);
                }
            }
        });
    }
    function tuchoi() {
        // document.getElementById('fm').action=url;
        // document.getElementById('fm').submit();
        url = url+'&duyet=0';
     //   alert(url);
        $('#fm').form('submit', {
            url: url,
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (result) {
                var result = eval('(' + result + ')');
                if (result.success) {
                    $('#dlg').dialog('close');
                    $('#dg').datagrid('reload');
                    $('#dg').datagrid('clearSelections');
                } else {
                    show_messager(result.msg);
                }
            }
        });
    }
</script>

<script type="text/javascript" src="js/lichhoc.js"></script>
<script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-groupview.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="lichhoc/jsonview" pageSize="50" pagination="true"
    toolbar="#toolbar" fitColumns="false" singleSelect="true" nowrap="true" showFooter="false"
    data-options="border:false,fit:true,iconCls:'icon-nhaphang'"  >
        <thead>
            <tr>
                <th field="lophoc" width="120">Lớp</th>
                <th field="ngay" width="120" align="center">Ngày</th>
                <th field="gio" width="120" align="center">Giờ</th>
                <th field="thoi_luong" width="120" align="center">Thời lượng</th>
                <th field="giaovien" width="160" >Giáo viên</th>
                <th field="hocvien" width="160" formatter="format_hocvien" >Học viên</th>
                <th field="phonghoc" width="160" >Phòng học</th>
                <th field="tinh_trang" width="160" align="center" formatter="tinhtrang">Tình trạng</th>
                <th field="link" formatter="format_link" align="center" width="100" >Link</th>
                <th field="link_danh_gia" formatter="format_link" align="center" width="100" >Link đánh giá</th>
            </tr>
        </thead>
    </table>

<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <button class="easyui-linkbutton" iconCls="icon-undo" plain="false" onclick="pre()">Tháng trước</button>
    <input class="easyui-timespinner" name="giohoc" id="giohoc" style="width: 80px;height:28px;" prompt="Giờ học">
    <input class="easyui-textbox" name="ngay1" id="ngay1" style="width: 30px">
    <input class="easyui-textbox" name="thang" id="thang" value="<?= date('m') ?>" style="width: 30px">
    <input class="easyui-textbox" name="nam" id="nam" value="<?= date('Y') ?>" style="width: 60px">
    <input class="easyui-combobox" name="loai" id="loai" style="width: 150px;height:28px;" prompt="Phân loại"
           panelHeight="auto"
           data-options="data:[{'id':'1','name':'Online'},{'id':'2','name':'Offline'},{'id':'3','name':'Demo'}]" valueField="id"
           textField="name">
    <input class="easyui-combobox" name="giaovien" id="giaovien" style="width: 150px;height:28px;"
           url="common/giaovien" valueField="id" textField="name" prompt="Giáo viên">
    <input class="easyui-combobox" name="phonghoc" id="phonghoc" style="width: 150px;height:28px;"
           url="common/phonghoc" valueField="id" textField="name" prompt="Phòng học">
    <input class="easyui-textbox" name="tenlop" id="tenlop" style="width: 150px;height:28px;" prompt="Tên lớp học">
    <button class="easyui-linkbutton" iconCls="icon-ok" plain="false" onclick="timkiem()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-redo" plain="false" onclick="next()">Tháng sau</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
    <br>
    <button class="easyui-linkbutton" iconCls="icon-nhaphang" plain="false" onclick="indexview()">Calendar</button>
    <button class="easyui-linkbutton" iconCls="icon-phieuthu" plain="false" onclick="dayview()">Day view</button>
    <button class="easyui-linkbutton" iconCls="icon-tip" plain="false" onclick="timeview()">Time view</button>
    <button class="easyui-linkbutton" iconCls="icon-ok" plain="false" onclick="duyetview()">Duyệt</button>
</div>

<div id="dlg" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr id="hienthithu">
                <td>Thứ:</td>
                <td>
                    <input id="thu" name="thu" class="easyui-textbox" required="required"
                           style="width: 200px;height:28px;" readonly/>
                </td>
            </tr>
            <tr>
                <td>Ngày học:</td>
                <td>
                    <input id="ngay" name="ngay" class="easyui-datebox" required="required" style="width: 200px;height:28px;" />
                </td>
            </tr><tr>
                <td>Giờ học:</td>
                <td>
                    <input id="gio" name="gio" class="easyui-timespinner" style="width: 200px;height:28px;" required/>
                </td>
            </tr>
            <tr>
                <td>Thời lượng:</td>
                <td>
                    <input id="thoi_luong" name="thoi_luong" class="easyui-numberbox" style="width: 200px;height:28px;"
                           required/>
                </td>
            </tr>
            <tr>
                <td>Lớp học:</td>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc" style="width: 200px;height:28px;"
                           url="common/lophoc" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Giáo viên:</td>
                <td>
                    <input class="easyui-combobox" name="giao_vien" id="giao_vien" style="width: 200px;height:28px;"
                           url="common/giaovien" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Phòng học:</td>
                <td>
                    <input class="easyui-combobox" name="phong_hoc" id="phong_hoc" style="width: 200px;height:28px;"
                           url="common/phonghoc" valueField="id" textField="name" >
                </td>
            </tr>
            <tr>
                <td>Link:</td>
                <td>
                    <input class="easyui-textbox" name="link" id="link" style="width: 200px;height:28px;"  >
                </td>
            </tr>
            <tr>
                <td>Tình trạng:</td>
                <td>
                    <input class="easyui-combobox" name="tinh_trang" id="tinh_trang" style="width: 200px;height:28px;"
                           url="lichhoc/tinhtrang" valueField="id" textField="name" >
                </td>
            </tr>
            <tr id="suatuongtu">
                <td>Sửa tương tự:</td>
                <td>
                    <input type="checkbox" id="editmulti" name="editmulti">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-add" class="easyui-dialog" style="width: 360px; height: auto; padding: 10px;" closable="false"
     closed="true"
     buttons="#dlg-buttons-add" modal="true">
    <form id="fm-add" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Lớp học:</td>
                <td>
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc1" style="width: 200px;height:28px;"
                           url="common/lophoc" valueField="id" textField="name">
                </td>
                <input type="hidden" id="sobuoi" name="sobuoi"/>
                <input type="hidden" id="buoikm" name="buoikm"/>
                <input type="hidden" id="hocvien" name="hocvien"/>
                <input type="hidden" id="phanloai" name="phanloai"/>
            </tr>
            <tr>
                <td>Giờ học:</td>
                <td>
                    <input id="gio1" name="gio" class="easyui-timespinner" style="width: 200px;height:28px;" required/>
                </td>
            </tr>
            <tr>
                <td>Thời lượng:</td>
                <td>
                    <input id="thoi_luong1" name="thoi_luong" class="easyui-numberbox" style="width: 200px;height:28px;"
                           required/>
                </td>
            </tr>
            <tr>
                <td>Ngày bắt đầu:</td>
                <td>
                    <input id="ngay1" name="ngay" class="easyui-datebox" required="required"
                           style="width: 200px;height:28px;"/>
                </td>
            </tr>
            <tr>
                <td>Buổi học:</td>
                <td>
                    <input class="easyui-combobox" name="buoihoc[]" id="buoihoc" style="width: 200px;height:28px;"
                           multiple="true"
                           url="common/thu" valueField="id" textField="name">
                </td>
            </tr>
            <tr>
                <td>Giáo viên:</td>
                <td>
                    <input class="easyui-combobox" name="giao_vien" id="giao_vien1" style="width: 200px;height:28px;"
                           url="common/giaovien" valueField="id" textField="name">
                </td>
            </tr>
            <tr>
                <td>Phòng học:</td>
                <td>
                    <input class="easyui-combobox" name="phong_hoc" id="phong_hoc1" style="width: 200px;height:28px;"
                           url="common/phonghoc" valueField="id" textField="name">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-add">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="saveadd()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-add').dialog('close')">Bỏ qua
    </button>
</div>

<div id="dlg-nhap" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true" buttons="#dlg-buttons-nhap" modal="true" iconCls="icon-thanhpho">
    <form id="fm-nhap" method="post" enctype="multipart/form-data" action="lichhoc/import">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Tải file:</div>
                </td>
                <td>
                    <input id="file"  name="file" class="easyui-filebox" style="width: 250px;height:28px;" prompt="Tải file excel..."/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-nhap">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="nhapexel()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-nhap').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-huy" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true" buttons="#dlg-buttons-huy" modal="true" iconCls="icon-thanhpho">
    <form id="fm-huy" method="post" enctype="multipart/form-data" action="lichhoc/huylich">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Chọn lớp:</div>
                </td>
                <td>
                    <input class="easyui-combobox" name="lophoc" id="lophoc" style="width: 200px;height:28px;" prompt="Chọn lớp cần hủy"
                           url="lichhoc/lophuy" valueField="id" textField="name">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-huy">
    <button class="easyui-linkbutton" iconCls="icon-ok" id="huybutton" onclick="submithuy()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-huy').dialog('close')">Bỏ qua</button>
</div>




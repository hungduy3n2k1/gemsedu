<script type="text/javascript" src="js/doanhthukh.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="doanhthukh/json" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu"
       data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
    <tr>
        <!--            <th field="ngay" width="120">Ngày giờ</th>-->
        <th field="khachhang" width="250">Khách hàng</th>
        <!--            <th field="dien_giai" width="350">Diễn giải</th>-->
        <th field="doanhthukh" width="120" align="right" formatter="CurrencyFormatted">Số tiền</th>
        <!--            <th field="ghi_chu" width="300">Ghi chú</th>-->
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    Từ ngày: <input id="ngaybd" name="ngaybd" class="easyui-datebox" style="width:120px" value="<?php echo date(" d/m/Y",strtotime('first day of this month')) ?>"/>
    Đến ngày: <input id="ngaykt" name="ngaykt" class="easyui-datebox" style="width:120px" value="<?php echo date(" d/m/Y ") ?>"/>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>&nbsp;&nbsp;
</div>

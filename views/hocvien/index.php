<script type="text/javascript" src="js/hocvien.js"></script>
<table id="dg" style="width: 100%;" url="hocvien/json" class="easyui-datagrid" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="false"
       data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
    <thead>
    <tr>
<!--        <th field="ma_hoc_vien" width="120">Mã học viên (Cũ)</th>-->
        <th field="id" width="70">ID</th>
        <th field="mahocvien" width="120">Mã học viên</th>
        <th field="name" width="180">Họ tên</th>
        <th field="e_name" width="180">Tên tiếng Anh</th>
        <th field="ngaybatdau" width="120">Ngày bắt đầu</th>
        <th field="truong_hoc" width="150">Trường</th>
        <th field="lop_hoc" width="150">Lớp</th>
        <th field="dien_thoai" width="150">Điện thoại</th>
        <th field="lophoc" width="180">Lớp học</th>
        <th field="khachhang" width="200">Khách hàng</th>
        <th field="dienthoaikh" width="150">Điện thoại</th>
<!--        <th field="phan_loai" width="120">Phân loại</th>-->
        <th field="tinh_trang" width="120" formatter="format_tinhtrang">Tình trạng</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input class="easyui-combobox" name="khachhang" id="khachhang" style="width: 150px;height:28px;"
           url="common/khachhang" valueField="id" textField="name" prompt="Khách hàng">&nbsp;
    <input class="easyui-combobox" name="tinhtrang" id="tinhtrang" style="width: 150px;height:28px;"
           url="hocvien/tinhtrang" valueField="id" textField="name" prompt="Tình trạng">&nbsp;
    <input class="easyui-combobox" name="phanloai" id="phanloai" style="width: 150px;height:28px;"
           url="common/loaidichvu" valueField="id" textField="name" prompt="Phân Loại">&nbsp;
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width: 150px;height:28px;" prompt="Từ Khóa"/> &nbsp;&nbsp;&nbsp;
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width:740px; padding:10px" closed="true" buttons="#dlg-buttons" modal="true"
     data-options="resizable:true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Họ tên:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required style="width: 200px;height:25px;"/>
                </td>
                <td>Tên tiếng Anh:</td>
                <td>
                    <input id="e_name" name="e_name" class="easyui-textbox" required style="width: 200px;height:25px;"/>
                </td>
            </tr>
            <tr>
                <td>Giới tính:</td>
                <td>
                    <input class="easyui-combobox" name="gioi_tinh" id="gioi_tinh" style="width: 200px;height:25px;"
                           url="common/gioitinh" valueField="id" textField="name">
                </td>
            </tr>
            <tr>
                <td>Trường:</td>
                <td>
                    <input id="truong_hoc" name="truong_hoc" class="easyui-textbox" style="width: 200px;height:25px;"/>
                </td>
                <td>Lớp:</td>
                <td>
                    <input id="lop_hoc1" name="lop_hoc" class="easyui-textbox" style="width: 200px;height:25px;"/>
                </td>
            </tr>
            <tr>
                <td>Ngày sinh:</td>
                <td>
                    <input id="ngaysinh" name="ngaysinh" class="easyui-datebox" style="width: 200px;height:25px;"/>
                </td>
                <td>Điện thoại:</td>
                <td>
                    <input id="dien_thoai" name="dien_thoai" class="easyui-textbox" style="width: 200px;height:25px;"/>
                </td>
            </tr>
            <tr>
                <td>Địa chỉ:</td>
                <td>
                    <input id="dia_chi" name="dia_chi" class="easyui-textbox" style="width: 200px;height:25px;"/>
                </td>
                <td>Email:</td>
                <td>
                    <input id="email" name="email" class="easyui-textbox" style="width: 200px;height:25px;"/>
                </td>
            </tr>
            <tr>
                <td>Facebook:</td>
                <td>
                    <input id="facebook" name="facebook" class="easyui-textbox" style="width: 200px;height:25px;"/>
                </td>
                <td>Zalo:</td>
                <td>
                    <input id="zalo" name="zalo" class="easyui-textbox" style="width: 200px;height:25px;"/>
                </td>
            </tr>
            <tr>
                <td>Khách hàng:</td>
                <td>
                    <input class="easyui-combobox" name="khach_hang" id="khach_hang" style="width: 200px;height:25px;"
                           url="common/khachhang" valueField="id" textField="name">
                </td>
                <td>Điện thoại:</td>
                <td>
                    <input class="easyui-textbox" id="dienthoai" style="width: 200px;height:25px;"
                         readonly>
                </td>
            </tr>
            <tr>
                <td>Phân loại:</td>
                <td>
                    <input class="easyui-combobox" name="phan_loai" id="phan_loai" style="width: 200px;height:28px;"
                           url="common/loaidichvu" valueField="id" textField="name">
                </td>
                <td>Tình trạng:</td>
                <td>
                    <input class="easyui-combobox" name="tinh_trang" id="tinh_trang" style="width: 200px;height:25px;"
                           url="hocvien/tinhtrang" valueField="id" textField="name">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua
    </button>
</div>


<div id="dlg-nhap" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true"
     buttons="#dlg-buttons-nhap" modal="true" iconCls="icon-thanhpho">
    <form id="fm-nhap" method="post" enctype="multipart/form-data" action="hocvien/import">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Tải file:</div>
                </td>
                <td>
                    <input id="file" name="file" class="easyui-filebox" style="width: 250px;height:28px;"
                           prompt="Tải file excel..."/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-nhap">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="nhapexel()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-nhap').dialog('close')">Bỏ qua
    </button>
</div>

<div id="dlg-nhaplich" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true"
     buttons="#dlg-buttons-nhaplich" modal="true" iconCls="icon-thanhpho">
    <form id="fm-nhaplich" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Chọn lớp:</div>
                </td>
                <td>
                    <input id="chonlop" name="chonlop" class="easyui-combobox" style="width: 250px;height:28px;"
                           valueField="id" textField="name" url="hocvien/lophoc"
                           prompt="Tên lớp"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Tải file:</div>
                </td>
                <td>
                    <input id="file1" name="file1" class="easyui-filebox" style="width: 250px;height:28px;" required
                           prompt="Tải file excel..."/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-nhaplich">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="nhapexcellich()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-nhaplich').dialog('close')">Bỏ qua
    </button>
</div>
<form id="fm-lichhoc" method="post" enctype="multipart/form-data" action="hocvien/lichhoc">
    <input type="hidden" id="hocvien" name="hocvien">
    <input type="hidden" id="tenhocvien" name="tenhocvien">
</form>

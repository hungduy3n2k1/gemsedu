<?php if (isset($this->hocvien)) { ?>
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-groupview.js"></script>
    <table id="dg" style="width: 100%;" class="easyui-datagrid" url="hocvien/jsonhocvien?hocvien=<?= $this->hocvien ?>"
           toolbar="#toolbar" fitColumns="false" singleSelect="true" nowrap="true" showFooter="false"
           data-options="border:false,fit:true,iconCls:'icon-nhaphang', view:groupview,
              groupField:'lophoc',
              groupFormatter:function(value,rows){
                  return value + ' - ' + rows.length + ' Buổi học(s)';
            }">
        <thead>
        <tr>
            <th field="id" width="120">Lớp</th>
            <th field="ngay" width="120" align="center">Ngày</th>
            <th field="gio" width="120" align="center">Giờ</th>
            <th field="giaovien" width="250">Giáo viên</th>
            <th field="phonghoc" width="160">Phòng học</th>
            <th field="tinh_trang" width="160" align="center" formatter="tinhtrang">Tình trạng</th>
        </tr>
        </thead>
    </table>

    <div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
        <span style="color: blue;font-weight: bold;">Học viên:</span>
        <span style="color: red;font-weight: bold;"><?= $this->tenhocvien ?></span>
        <button class="easyui-linkbutton" iconCls="icon-undo" plain="false" onclick="pre()">Tháng trước</button>
        <input class="easyui-textbox" name="thang" id="thang" value="<?= date('m') ?>" style="width: 30px">
        <input class="easyui-textbox" name="nam" id="nam" value="<?= date('Y') ?>" style="width: 60px">
        <button class="easyui-linkbutton" iconCls="icon-ok" plain="false" onclick="timkiem()">OK</button>
        <button class="easyui-linkbutton" iconCls="icon-redo" plain="false" onclick="next()">Tháng sau</button>
        <button class="easyui-linkbutton" iconCls="icon-undo" plain="false"
                onclick="javascript:window.location.href=baseUrl+'/hocvien'">Quay lại
        </button>
        <button class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="themlich()">Thêm lịch</button>
    </div>
    <div id="dlg-saplop" class="easyui-dialog" style="width:800px;height:500px" closed="true"
         buttons="#dlg-buttons-saplop" modal="true" data-options="resizable:true">
        <div class="easyui-layout" style="width:100%;height:100%;padding-left: 5px;padding-right: 5px;">
            <div data-options="region:'west',title:'Chọn lớp',iconCls:'icon-ok'" style="width:20%; padding:20px">
                <form id="fm-saplop" method="post" enctype="multipart/form-data">
                    <input class="easyui-combobox" name="lop_hoc" id="lop_hoc"
                           style="width: 100%;height:28px;"
                           required
                           url="hocvien/lophoc" valueField="id" textField="name">
                    <input type="hidden" id="data-saplop" name="data-saplop">
                </form>
            </div>
            <div data-options="region:'center',title:'Lịch học',iconCls:'icon-ok'">
                <table id="dg-saplop" style="width: 100%;" class="easyui-datagrid" pagination="false" toolbar="toolbar1"
                       idField="id" rownumbers="true" fitColumns="true" singleSelect="false" nowrap="false"
                       data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
                    <thead>
                    <tr>
                        <th field="ck" checkbox="true"></th>
                        <th field="ngay" width="140">Ngày</th>
                        <th field="gio" width="90">Giờ</th>
                        <th field="giaovien" width="150">Giáo viên</th>
                        <th field="phonghoc" width="150">Phòng học</th>
                        <th field="tinhtrang" width="120">Tình trạng</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="dlg-buttons-saplop">
        <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savesaplop()">Câp nhật</button>
        <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-saplop').dialog('close')">Bỏ
            qua
        </button>
    </div>
    <script type="text/javascript">
        function celltop(value, row) {
            if (value)
                return '<div style="min-height:70px;line-height:12px">' + value + '</div>';
        }

        function tinhtrang(val, row) {
            if (val == 1)
                return 'Chờ';
            else if (val == 2)
                return 'Đã setup';
            else if (val == 3)
                return 'Đang học';
            else if (val == 4)
                return 'Hoàn thành';
            else if (val == 5)
                return 'Đã chốt';
            else if (val == 6)
                return 'Cọc ngay';
            else if (val == 7)
                return 'Hủy';
            else
                return '';
        }

        function timkiem() {
            var thang = $('#thang').textbox('getValue');
            var nam = $('#nam').textbox('getValue');
            if (thang.length > 0 || nam.length > 0) {
                $("#dg").datagrid("options").url = baseUrl + "/hocvien/jsonhocvien?thang=" + thang + "&nam=" + nam + '&hocvien=<?=$this->hocvien?>';
            } else {
                $("#dg").datagrid("options").url = baseUrl + "/hocvien/jsonhocvien?hocvien=<?=$this->hocvien?>";
            }
            $("#dg").datagrid("reload");
        }

        function pre() {
            var thang = $('#thang').textbox('getValue');
            var nam = $('#nam').textbox('getValue');
            var x = parseInt(thang) - 1;
            if (x > 0) {
                if (x < 10)
                    thang = '0' + x;
                else
                    thang = x;
                $('#thang').textbox('setValue', thang);
            } else {
                thang = '12';
                var y = parseInt(nam) - 1;
                $('#thang').textbox('setValue', thang);
                $('#nam').textbox('setValue', y);
            }
            $("#dg").datagrid("options").url = baseUrl + "/hocvien/jsonhocvien?thang=" + thang + "&nam=" + nam + '&hocvien=<?=$this->hocvien?>';
            $("#dg").datagrid("reload");
        }

        function next() {
            var thang = $('#thang').textbox('getValue');
            var nam = $('#nam').textbox('getValue');
            var x = parseInt(thang) + 1;
            if (x < 13) {
                if (x < 10)
                    thang = '0' + x;
                else
                    thang = x;
                $('#thang').textbox('setValue', thang);
            } else {
                thang = '01';
                var y = parseInt(nam) + 1;
                $('#thang').textbox('setValue', thang);
                $('#nam').textbox('setValue', y);
            }
            $("#dg").datagrid("options").url = baseUrl + "/hocvien/jsonhocvien?thang=" + thang + "&nam=" + nam + '&hocvien=<?=$this->hocvien?>';
            $("#dg").datagrid("reload");
        }

        function themlich() {
            $('#dlg-saplop').dialog({
                title: '&nbsp;Thêm lịch học ',
                iconCls: 'icon-reload',
                closed: false
            });
            $('#fm-saplop').form('clear');
            $('#dg-saplop').datagrid('loadData', []);
            $('#lop_hoc').combobox({
                onSelect: function (rec) {
                    $('#dg-saplop').datagrid({
                        url: baseUrl + '/hocvien/getLichHoc?lophoc=' + rec.id + '&hocvien=<?= $this->hocvien ?>'
                      //
                    });
                    $('#dg-saplop').datagrid('reload');
                    $('#dg-saplop').datagrid({
                        onLoadSuccess: function (data) {
                            for (i = 0; i < data.rows.length; ++i) {
                                $(this).datagrid('uncheckRow', i);
                                if (data.rows[i]['checkhv'] > 0) $(this).datagrid('checkRow', i);
                            }
                        }
                    });
                }
            });

            url = baseUrl + '/hocvien/saplop?hocvien=<?= $this->hocvien ?>';
        }


        function savesaplop() {
            var rows = $('#dg-saplop').datagrid('getSelections');
            var list = '';
            rows.forEach(
                function myFunction(item, index) {
                    list += item.id + ',';
                });
            document.getElementById('data-saplop').value = list;
            // document.getElementById('fm-saplop').action = url;
            // document.getElementById('fm-saplop').submit();
            $('#fm-saplop').form('submit', {
                url: url,
                onSubmit: function () {
                    return $(this).form('validate');
                },
                success: function (result) {
                    var result = eval('(' + result + ')');
                    if (result.success) {
                        show_messager(result.msg);
                        $('#dlg-saplop').dialog('close');
                        $('#dg').datagrid('reload');
                    } else {
                        show_messager(result.msg);
                    }
                }
            });
        }

    </script>
<?php } ?>
<script type="text/javascript" src="js/ca.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="ca/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-thanhpho" data-options="border:false,fit:true"
    pageSize="50">
    <thead>
        <tr>
            <th field="ca" width="250">Ca</th>
            <th field="t2_in" width="100" align="center">Thứ 2 IN</th>
            <th field="t2_out" width="100" align="center">Thứ 2 Out</th>
             <th field="t3_in" width="100" align="center">Thứ 3 IN</th>
            <th field="t3_out" width="100" align="center">Thứ 3 Out</th>
             <th field="t4_in" width="100" align="center">Thứ 4 IN</th>
            <th field="t4_out" width="100" align="center">Thứ 4 Out</th>
             <th field="t5_in" width="100" align="center">Thứ 5 IN</th>
            <th field="t5_out" width="100" align="center">Thứ 5 Out</th>
             <th field="t6_in" width="100" align="center">Thứ 6 IN</th>
            <th field="t6_out" width="100" align="center">Thứ 6 Out</th>
             <th field="t7_in" width="100" align="center">Thứ 7 IN</th>
            <th field="t7_out" width="100" align="center">Thứ 7 Out</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; line-height: 30px; text-align:left">
    <span style="font-weight:bold; color:blue">DANH SÁCH CA LÀM VIỆC</span>
    <?php
         foreach ($this->funs as $item)
          echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true" iconCls="icon-thanhpho">
    <form id="fm" method="post" >
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Ca:</td>
                <td>
                    <input id="ca" name="ca" class="easyui-textbox" style="width: 200px;height:25px;" required/>
                </td>
            </tr><tr>
                <td>T2 IN:</td>
                <td>
                    <input id="t2_in" name="t2_in" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"  editable="false"/>
                </td>
            </tr><tr>
                <td>T2 OUT:</td>
                <td>
                    <input id="t2_out" name="t2_out" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr><tr>
                <td>T3 IN:</td>
                <td>
                    <input id="t3_in" name="t3_in" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr><tr>
                <td>T3 OUT:</td>
                <td>
                    <input id="t3_out" name="t3_out" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr><tr>
                <td>T4 IN:</td>
                <td>
                    <input id="t4_in" name="t4_in" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr><tr>
                <td>T4 OUT:</td>
                <td>
                    <input id="t4_out" name="t4_out" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr><tr>
                <td>T5 IN:</td>
                <td>
                    <input id="t5_in" name="t5_in" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr><tr>
                <td>T5 OUT:</td>
                <td>
                    <input id="t5_out" name="t5_out" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr><tr>
                <td>T6 IN:</td>
                <td>
                    <input id="t6_in" name="t6_in" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr><tr>
                <td>T6 OUT:</td>
                <td>
                    <input id="t6_out" name="t6_out" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr><tr>
                <td>T7 IN:</td>
                <td>
                    <input id="t7_in" name="t7_in" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr><tr>
                <td>T7 OUT:</td>
                <td>
                    <input id="t7_out" name="t7_out" class="easyui-timespinner" style="width: 200px;height:25px;" required data-options="showSeconds:true"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

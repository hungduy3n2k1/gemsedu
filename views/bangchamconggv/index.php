<script type="text/javascript" src="js/bangchamconggv.js"></script>
<script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-groupview.js"></script>
<table
    id="dg"
    style="width: 100%;"
    class="easyui-datagrid"
    url="bangchamconggv/json"
    toolbar="#toolbar"
    pagination="true"
    idField="id"
    rownumbers="true"
    fitColumns="false"
    singleSelect="true"
    nowrap="true"
    iconCls="icon-thanhpho"
    data-options="border:false,fit:true
    "
    pageSize="30"
></table>
<div id="toolbar" style="height: 40px; padding-top: 4px;">
    <input class="easyui-combobox" style="width:120px"
           url="common/thang" valueField="id" textField="name" editable="false" name="thang" id="thang" value="<?php echo date("m") ?>">
    <input class="easyui-combobox" style="width:120px"
           url="common/nam" valueField="id" textField="name" editable="false" name="nam" id="nam" value="<?php echo date("Y") ?>">
    <input class="easyui-combobox" name="giaovien" id="giaovien" style="width: 200px;"
           url="common/giaovien" valueField="id" textField="name" prompt="Giáo viên">
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="search()">Tìm kiếm</button>
    &nbsp;&nbsp;&nbsp;
    <button class="easyui-linkbutton" iconCls="icon-phieuthu" plain="false" onclick="window.location.href=baseUrl+'/bangchamconggv/demo'">Demo</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

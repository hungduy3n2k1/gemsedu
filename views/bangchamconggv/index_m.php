<table
        id="dg"
        style="width: 100%;"
        class="easyui-datagrid"
        url="bangchamconggv/json?giaovien=<?=$_SESSION['user']['giao_vien']?>"
        toolbar="#toolbar"
        pagination="true"
        idField="id"
        rownumbers="true"
        fitColumns="false"
        singleSelect="true"
        nowrap="true"
        iconCls="icon-thanhpho"
        data-options="border:false,fit:true"
        pageSize="50"
>
</table>

<div id="dlg" class="easyui-dialog" style="padding:20px 6px;width:80%; top:100px" title="Choose a class"
     data-options="inline:true,modal:true,closed:true,closable:true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tháng:</td>
                <td>
                    <input class="easyui-combobox" style="width:100%;height: 28px;"
                           url="common/thang" valueField="id" textField="name" editable="false" name="thang" id="thang"
                           value="<?php echo date("m") ?>">
                </td>
                <td>Năm:</td>
                <td>
                    <input class="easyui-combobox" style="width:100%;height: 28px;"
                           url="common/nam" valueField="id" textField="name" editable="false" name="nam" id="nam"
                           value="<?php echo date("Y") ?>">
                </td>
            </tr>
        </table>
    </form>
    <div class="dialog-button" style="text-align:center">
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="tieptuc()">OK</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px"
           onclick="$('#dlg').dialog('close')">Close</a>
    </div>
</div>
<footer>
    <div class="easyui-tabs"
         data-options="tabHeight:70,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true"
         style="padding-bottom: 5px;">
        <div style="padding:10px">
            <div class="panel-header tt-inner" onClick="timkiem()" style="font-size:14px">
                <img src='libs/jquery/images/search.png' width="32" height="32"/>
                <br>Search
            </div>
        </div>
        <!--        <div style="padding:10px">-->
        <!--            <div class="panel-header tt-inner" onClick="diemdanh()" style="font-size:14px;font-weight: bold;">-->
        <!--                <img src='libs/jquery/images/add.png' width="32" height="32" />-->
        <!--                <br>Check<br>attendance-->
        <!--            </div>-->
        <!--        </div>-->
        <!--        <div style="padding:10px">-->
        <!--            <div class="panel-header tt-inner" onClick="checkout()" style="font-size:14px">-->
        <!--                <img src='libs/jquery/images/lock.png' width="32" height="32" />-->
        <!--                <br> Checkout-->
        <!--            </div>-->
        <!--        </div>-->

        <script>
            $(function () {
                var thang = $('#thang').combobox('getValue');
                var nam = $('#nam').combobox('getValue');
                var column = [{
                    field: 'tong', title: 'Tổng số giờ', width: 80,
                    formatter: function (value, row) {
                        return (value / 60);
                    }
                }];
                for (var i = 1; i <= 31; i++) {
                    if (i < 10) {
                        i = '0' + i;
                    }
                    var dt = new Date(nam + '-' + thang + '-' + i);
                    if (dt.getDay() == 0) {
                        column.push({field: 'ngay_' + i, title: 'CN', width: 60, align: 'center'});
                    } else if (dt.getDay() == 1) {
                        column.push({field: 'ngay_' + i, title: 'Thứ 2<br>' + i, width: 60, align: 'center'});
                    } else if (dt.getDay() == 2) {
                        column.push({field: 'ngay_' + i, title: 'Thứ 3<br>' + i, width: 60, align: 'center'});
                    } else if (dt.getDay() == 3) {
                        column.push({field: 'ngay_' + i, title: 'Thứ 4<br>' + i, width: 60, align: 'center'});
                    } else if (dt.getDay() == 4) {
                        column.push({field: 'ngay_' + i, title: 'Thứ 5<br>' + i, width: 60, align: 'center'});
                    } else if (dt.getDay() == 5) {
                        column.push({field: 'ngay_' + i, title: 'Thứ 6<br>' + i, width: 60, align: 'center'});
                    } else {
                        column.push({field: 'ngay_' + i, title: 'Thứ 7<br>' + i, width: 60, align: 'center'});
                    }
                }
                $('#dg').datagrid({
                    columns: [column]
                });
            });

            function search() {
                var thang = $('#thang').combobox('getValue');
                var nam = $('#nam').combobox('getValue');
                var column = [{
                    field: 'tong', title: 'Tổng số giờ', width: 80,
                    formatter: function (value, row) {
                        return (value / 60);
                    }
                }];
                // var column = [{
                //     field: 'giaovien', title: 'Giáo viên', width: 200,
                //     formatter: function (value, row) {
                //         return value + ' (' + row.tong / 60 + ')';
                //     }
                // }
                // {field:'ngay_cong',title:'Ngày công',width:80,align:'center'},
                // {field:'cong_chuan',title:'Công chuẩn',width:80,align:'center'}
                // ];
                for (var i = 1; i <= 31; i++) {
                    if (i < 10) {
                        i = '0' + i;
                    }
                    var dt = new Date(nam + '-' + thang + '-' + i);
                    if (dt.getDay() == 0) {
                        column.push({field: 'ngay_' + i, title: 'CN', width: 60, align: 'center'});
                    } else if (dt.getDay() == 1) {
                        column.push({field: 'ngay_' + i, title: 'Thứ 2<br>' + i, width: 60, align: 'center'});
                    } else if (dt.getDay() == 2) {
                        column.push({field: 'ngay_' + i, title: 'Thứ 3<br>' + i, width: 60, align: 'center'});
                    } else if (dt.getDay() == 3) {
                        column.push({field: 'ngay_' + i, title: 'Thứ 4<br>' + i, width: 60, align: 'center'});
                    } else if (dt.getDay() == 4) {
                        column.push({field: 'ngay_' + i, title: 'Thứ 5<br>' + i, width: 60, align: 'center'});
                    } else if (dt.getDay() == 5) {
                        column.push({field: 'ngay_' + i, title: 'Thứ 6<br>' + i, width: 60, align: 'center'});
                    } else {
                        column.push({field: 'ngay_' + i, title: 'Thứ 7<br>' + i, width: 60, align: 'center'});
                    }
                }
                $('#dg').datagrid({
                    columns: [column]
                });
                $('#dg').datagrid('options').url = 'bangchamcong/json?thang=' + thang + '&nam=' + nam + '&giaovien=<?=$_SESSION['user']['giao_vien']?>';
                $('#dg').datagrid('reload');
            }

                function add() {
                    var thang = $('#thang').combobox('getValue');
                    var nam = $('#nam').combobox('getValue');
                    $.post('bangchamcong/add', {thang: thang, nam: nam}, function (result) {
                        if (result.success) {
                            // show_messager(result.msg);
                            $('#dg').datagrid('options').url = 'bangchamcong/json?thang=' + thang + '&nam=' + nam + '&giaovien=<?=$_SESSION['user']['giao_vien']?>';
                            $('#dg').datagrid('reload');
                        } else {
                            show_messager(result.msg);
                        }
                    }, 'json');
                }

        </script>
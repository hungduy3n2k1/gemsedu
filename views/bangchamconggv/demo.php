<script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-groupview.js"></script>
<table
    id="dg"
    style="width: 100%;"
    class="easyui-datagrid"
    url="bangchamconggv/jsondemo"
    toolbar="#toolbar"
    pagination="true"
    idField="id"
    rownumbers="true"
    fitColumns="false"
    singleSelect="true"
    nowrap="true"
    iconCls="icon-thanhpho"
    data-options="border:false,fit:true
    "
    pageSize="50"
></table>
<div id="toolbar" style="height: 40px; padding-top: 4px;">
    <input class="easyui-combobox" style="width:120px"
           url="common/thang" valueField="id" textField="name" editable="false" name="thang" id="thang" value="<?php echo date("m") ?>">
    <input class="easyui-combobox" style="width:120px"
           url="common/nam" valueField="id" textField="name" editable="false" name="nam" id="nam" value="<?php echo date("Y") ?>">
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="search()">Tìm kiếm</button>
    &nbsp;&nbsp;&nbsp;
    <button class="easyui-linkbutton" iconCls="icon-phieuthu" plain="false" onclick="window.location.href=baseUrl+'/bangchamconggv/index'">Main</button>
</div>
<script>
    $(function () {
        var thang = $('#thang').combobox('getValue');
        var nam = $('#nam').combobox('getValue');
        var column = [
            // {field: 'giaovien', title: 'Giáo viên', width: 220},
            {field: 'tenlop', title: 'Lớp học', width: 100},
            {field: 'tonggio', title: 'Số buổi', width: 60}
        ];
        for (var i = 1; i <= 31; i++) {
            if (i < 10) {
                i = '0' + i;
            }
            var dt = new Date(nam + '-' + thang + '-' + i);
            if (dt.getDay() == 0) {
                column.push({field: 'ngay_' + i, title: 'CN', width: 60, align: 'center'});
            } else if (dt.getDay() == 1) {
                column.push({field: 'ngay_' + i, title: 'Thứ 2<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 2) {
                column.push({field: 'ngay_' + i, title: 'Thứ 3<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 3) {
                column.push({field: 'ngay_' + i, title: 'Thứ 4<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 4) {
                column.push({field: 'ngay_' + i, title: 'Thứ 5<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 5) {
                column.push({field: 'ngay_' + i, title: 'Thứ 6<br>' + i, width: 60, align: 'center'});
            } else {
                column.push({field: 'ngay_' + i, title: 'Thứ 7<br>' + i, width: 60, align: 'center'});
            }
        }
        $('#dg').datagrid({
            columns: [column],
            view:groupview,
            groupField:'giaovien',
            groupFormatter:function(value,rows){
                var tonggio=0;
                rows.forEach(function (row,index) {
                    tonggio+=row.tonggio
                });
                return value + ' - ' + tonggio + ' buổi';
            }
        });
    });

    function search() {
        var thang = $('#thang').combobox('getValue');
        var nam = $('#nam').combobox('getValue');
        var column = [
            // {field: 'giaovien', title: 'Giáo viên', width: 220},
            {field: 'tenlop', title: 'Lớp học', width: 100},
            {field: 'tonggio', title: 'Số buổi', width: 60}
        ];
        for (var i = 1; i <= 31; i++) {
            if (i < 10) {
                i = '0' + i;
            }
            var dt = new Date(nam + '-' + thang + '-' + i);
            if (dt.getDay() == 0) {
                column.push({field: 'ngay_' + i, title: 'CN', width: 60, align: 'center'});
            } else if (dt.getDay() == 1) {
                column.push({field: 'ngay_' + i, title: 'Thứ 2<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 2) {
                column.push({field: 'ngay_' + i, title: 'Thứ 3<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 3) {
                column.push({field: 'ngay_' + i, title: 'Thứ 4<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 4) {
                column.push({field: 'ngay_' + i, title: 'Thứ 5<br>' + i, width: 60, align: 'center'});
            } else if (dt.getDay() == 5) {
                column.push({field: 'ngay_' + i, title: 'Thứ 6<br>' + i, width: 60, align: 'center'});
            } else {
                column.push({field: 'ngay_' + i, title: 'Thứ 7<br>' + i, width: 60, align: 'center'});
            }
        }
        $('#dg').datagrid({
            columns: [column],
            view:groupview,
            groupField:'giaovien',
            groupFormatter:function(value,rows){
                var tonggio=0;
                rows.forEach(function (row,index) {
                    tonggio+=row.tonggio
                });
                return value + ' - ' + tonggio + ' buổi';
            }
        });
        $('#dg').datagrid('options').url = 'bangchamconggv/jsondemo?thang=' + thang + '&nam=' + nam;
        $('#dg').datagrid('reload');
    }

</script>

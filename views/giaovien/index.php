<script type="text/javascript" src="js/giaovien.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="giaovien/json" toolbar="#toolbar" pagination="true"
  idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true" iconCls="icon-phanloai"
  data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="id" width="50" >ID</th>
            <th field="name" width="200" >Họ tên</th>
            <th field="ngaysinh" width="140">Ngày sinh</th>
            <th field="tai_khoan" width="280">Tài khoản</th>
            <th field="dien_thoai" width="159">Điện thoại</th>
            <th field="email" width="220">Email</th>
            <th field="chuyen_nganh" width="250">Chuyên ngành</th>
            <th field="loaihinh" width="90">Cũ/mới</th>
            <th field="bangtienganh" width="90">Bằng TA</th>
            <th field="bangdaihoc" width="90">Bằng ĐH</th>
            <th field="phanloai" width="90">Phân loại</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width: 200px;height:25px;" prompt="Từ khóa" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
        foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 720px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Họ tên:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px;height:28px;" />
                </td>
                <td>Ngày sinh:</td>
                <td>
                    <input id="ngaysinh" name="ngaysinh" class="easyui-datebox" style="width: 200px;height:28px;" />
                </td>
            </tr>
            <tr>
                <td>Tài khoản:</td>
                <td>
                    <input id="tai_khoan" name="tai_khoan" class="easyui-textbox"  style="width: 200px;height:28px;" />
                </td>
                <td>Điện thoại:</td>
                <td>
                    <input id="dien_thoai" name="dien_thoai" class="easyui-textbox" style="width: 200px;height:28px;" />
                </td>
            </tr>
            <tr>
                <td>Email:</td>
                <td>
                    <input id="email" name="email" class="easyui-textbox" style="width: 200px;height:28px;" />
                </td>
                <td>Chuyên ngành:</td>
                <td>
                    <input id="chuyen_nganh" name="chuyen_nganh" class="easyui-textbox" style="width: 200px;height:28px;" />
                </td>
            </tr>
            <tr>
                <td>Lương kid:</td>
                <td>
                    <input id="luong_kid" name="luong_kid" class="easyui-numberbox" style="width: 200px;height:28px;" groupSeparator="," />
                </td>
                <td>Lương GT:</td>
                <td>
                    <input id="luong_giao_tiep" name="luong_giao_tiep" class="easyui-numberbox" groupSeparator="," style="width: 200px;height:28px;" />
                </td>
            </tr>
            <tr>
                <td>Loại GV:</td>
                <td>
                    <select class="easyui-combobox" name="loai_hinh" id="loai_hinh" style="width: 200px;height:25px;" required>
                        <option value="1">Cũ</option>
                        <option value="2">Mới</option>
                    </select>
                </td>
                <td>Bằng tiếng anh:</td>
                <td>
                    <select class="easyui-combobox" name="bang_tieng_anh" id="bang_tieng_anh" style="width: 200px;height:25px;">
                        <option value="0">Không có</option>
                        <option value="1">B2</option>
                        <option value="2">C1</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Bằng đại học:</td>
                <td>
                    <select class="easyui-combobox" name="bang_dai_hoc" id="bang_dai_hoc" style="width: 200px;height:25px;" required>
                        <option value="1">Có</option>
                        <option value="0">Không</option>
                    </select>
                </td>
                <td>Phân loại:</td>
                <td>
                    <select class="easyui-combobox" name="phan_loai" id="phan_loai" style="width: 200px;height:25px;" required>
                        <option value="1">Giáo viên nước ngoài</option>
                        <option value="2">Giáo viên Việt Nam</option>
                        <option value="0">Trợ giảng</option>
                    </select>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-nhap" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true"
     buttons="#dlg-buttons-nhap" modal="true" iconCls="icon-thanhpho">
    <form id="fm-nhap" method="post" enctype="multipart/form-data" action="giaovien/import">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Phân loại:</td>
                <td>
                    <select class="easyui-combobox" name="phan_loai" id="phan_loai1" style="width: 250px;height:28px;" required>
                        <option value="1">Giáo viên nước ngoài</option>
                        <option value="2">Giáo viên Việt Nam</option>
                        <option value="0">Trợ giảng</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Tải file:</div>
                </td>
                <td>
                    <input id="file" name="file" class="easyui-filebox" style="width: 250px;height:28px;" required
                           prompt="Tải file excel..."/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-nhap">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="nhapexel()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-nhap').dialog('close')">Bỏ qua
    </button>
</div>
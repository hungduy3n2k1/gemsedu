<script type="text/javascript" src="js/khachhang.js"></script>
<table id="dg" style="width: 100%;" url="khachhang/json" toolbar="#toolbar" class="easyui-datagrid" pagination="true"
    idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="false" showFooter="true"
    data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="50">
    <thead>
        <tr>
            <th field="id" width="50">ID</th>
            <th field="name" width="150">Tên khách hàng</th>
            <th field="dien_thoai" width="150">Điện thoại</th>
            <th field="dia_chi" width="180">Địa chỉ</th>
            <th field="email" width="150">Email</th>
            <th field="phanloai" width="120">Phân loại</th>
            <th field="phutrach" width="150">Phụ trách</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
<!--    <input class="easyui-combobox" name="tinhtrang" id="tinhtrang" url="common/loaikhachhang" value="4" valueField="id" textField="text"-->
<!--    prompt="Tình trạng" editable="true" >-->
    <input id="chonloai" name="chonloai" class="easyui-combobox" prompt="Phân loại"
           valueField="id" textField="name" url="common/loaikh" panelHeight="auto" />
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Từ khóa" />
    <input class="easyui-combobox" name="nhanvien" id="nhanvien" url="common/nhanvien" valueField="id" textField="name" prompt="Người cập nhật" >
    <input class="easyui-combobox" name="phutrach" id="phutrach" url="common/nhanvien" valueField="id" textField="name" prompt="Người phụ trách" >
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button> &nbsp;&nbsp;&nbsp;&nbsp;
    <?php
          foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
<!--    <button class="easyui-linkbutton" iconCls="icon-excel" plain="false" onclick="xuatfile1()">Xuất excel</button>-->
</div>
<div id="dlg" class="easyui-dialog" style="width: 1000px; padding:10px; top:60px" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" >
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Khách hàng:</td>
                <td>
                    <input name="name" id="name" class="easyui-textbox" style="width:250px;height: 28px;" required>
                </td>
                <td>Điện thoại:</td>
                <td>
                    <input name="dien_thoai" id="dien_thoai1" class="easyui-textbox" style="width:250px;height: 28px;"
                           required>
                </td>
            </tr>
            <tr>
                <td>Địa chỉ:</td>
                <td>
                    <input name="dia_chi" id="dia_chi" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
                <td>Email</td>
                <td>
                    <input name="email" id="email" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
            </tr>
            <tr>
                <td>Phụ trách:</td>
                <td>
                    <input id="phu_trach" name="phu_trach" class="easyui-combobox" style="width:250px;height: 28px;"
                           required
                           url="common/nhanvien" valueField="id" textField="name" prompt="Phụ trách"/>
                </td>
                <td>Loại KH</td>
                <td>
                    <input name="loai   " id="loai" class="easyui-combobox" style="width:250px;height: 28px;"
                           editable="false"
                           valueField="id" textField="name" panelHeight="auto" panelHeight="auto"
                           url="common/loaikh">
                </td>
            </tr>
            <tr>
                <td>Ghi chú:</td>
                <td>
                    <input name="ghi_chu" id="ghi_chu1" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
<!--                <td>Tình trạng</td>-->
<!--                <td>-->
<!--                    <input name="tinh_trang" id="tinh_trang" class="easyui-combobox" style="width:250px;height: 28px;"-->
<!--                           editable="false"-->
<!--                           valueField="id" textField="name" panelHeight="auto" panelHeight="auto"-->
<!--                           url="khachhang/tinhtrang">-->
<!--                </td>-->
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>
<!-- Form liên hệ  -->
<div id="dlg-lienhe" class="easyui-dialog" style="width: 370px; padding:10px;" closed="true" modal="true" buttons="#dlg-buttons-2">
  <form id="fm-lienhe" method="post" >
      <div style="margin-bottom:20px">
           <input class="easyui-combobox" name="contact" id="contact" style="width:100%" data-options="label:'Họ tên:',required:true"
           url="common/contact" valueField="id" textField="name">
      </div>
      <div style="margin-bottom:20px">
          <input class="easyui-textbox" id="chucvu" name="chucvu" style="width:100%" data-options="label:'Chức vụ:'">
      </div>
      <div style="margin-bottom:20px">
          <input class="easyui-textbox" id="dienthoai" name="dienthoai" style="width:100%;" label="Điện thoại:" required>
      </div>
      <div style="margin-bottom:20px">
           <input class="easyui-textbox" id="emailcts" name="emailcts" style="width:100%" data-options="label:'Email:',validType:'email'">
      </div>
      <div style="margin-bottom:20px">
          <input class="easyui-textbox" id="ghichu" name="ghichu" style="width:100%;height:50px" data-options="label:'Ghi chú:',multiline:true">
      </div>
  </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savects()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-lienhe').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-nhap" class="easyui-dialog" style="width: 350px; height: auto; padding: 15px;" closable="false"
     closed="true" buttons="#dlg-buttons-nhap" modal="true" iconCls="icon-thanhpho" resizable="true">
    <form id="fm-nhap" method="post" action="khachhang/import" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên file:</td>
                <td>
                    <input id="file" name="file" class="easyui-filebox" style="width: 230px;"
                           prompt="Tải file data lên..." required/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-nhap">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="nhapexel()">Tải lên</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-nhap').dialog('close')">Bỏ qua
    </button>
</div>

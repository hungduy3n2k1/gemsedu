<script type="text/javascript" src="js/khachhang.js"></script>

<table id="dg" style="width: 100%;" url="khachhang/json"  class="easyui-datagrid" pagination="true"
    idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="false" showFooter="true"
    data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="50">
    <thead>
        <tr>
            <th field="name" width="150">Tên khách hàng</th>
            <th field="van_phong" width="360">Địa chỉ văn phòng</th>
            <th field="dien_thoai" width="150">Điện thoại</th>
            <th field="website" width="180">Website</th>
            <th field="nguoilienhe" width="150">Người liên hệ</th>
            <th field="dienthoai" width="150">Số điện thoại</th>
            <th field="email" width="240">Email</th>
        </tr>
    </thead>
</table>

<!--
<table id="dg" style="width: 100%;" url="khachhang/json" pagination="true" idField="id" class="easyui-datagrid"
    fitColumns="false" singleSelect="true" nowrap="true" data-options="border:false,fit:true,iconCls:'icon-nhaphang'"
    pageSize="50">
    <thead>
        <tr>
            <th field="name" width="150">Tên khách hàng</th>
            <th field="dia_chi" width="360">Địa chỉ</th>
            <th field="dien_thoai" width="100" align="center">Điện thoại</th>
            <th field="email" width="240">Email</th>
            <th field="website" width="150">Website</th>
        </tr>
    </thead>
</table> -->

<div id="dlg-search" class="easyui-dialog" style="width:90%; height: auto; padding: 10px; top:30px" closable="false" closed="true"
    buttons="#dlg-buttons-2" modal="true">
    <form id="fm-search" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Loại khách hàng:</td>
                <td>
                    <input class="easyui-combobox" style="width:200px; height:25px" id="tinhtrang" name="tỉnhtrang"
                      valueField="id" textField="text" url="common/loaikhachhang" value="3">
                </td>
            </tr><tr>
                <td>Từ khóa:</td>
                <td>
                    <input class="easyui-textbox" style="width:200px; height:25px" id="tukhoa" name="tukhoa" prompt="Từ khóa" >
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="timkiem()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-search').dialog('close')">Bỏ qua</button>
</div>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="javascript:$('#dlg-search').dialog({closed:false})" style="font-size:14px">
            <img src="libs/jquery/images/search.png" width="32" height="32" />
            <br>Tìm kiếm
        </div>
    </div>

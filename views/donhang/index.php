<script type="text/javascript" src="js/donhang.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="donhang/json" toolbar="#toolbar" pagination="true"
       idField="id" rownumbers="true"
       fitColumns="true" singleSelect="true" nowrap="true" iconCls="icon-phieuxuat" data-options="border:false,fit:true"
       showFooter="false"
       pageSize="30">
    <thead>
    <tr>
        <th field="ngay" width="120">Ngày tạo đơn</th>
        <th field="id" width="80" align="center">Số đơn</th>
        <th field="khachhang" width="200">Khách hàng</th>
        <th field="sanpham" width="150">Khóa học</th>
        <th field="so_tien" width="150" formatter="CurrencyFormatted">Số tiền</th>
        <th field="dot_thanh_toan" width="80">Đợt TT</th>
        <th field="dathanhtoan" width="80">Đã TT</th>
        <th field="hocvien" width="200">Học viên</th>
        <th field="so_buoi" width="100" formatter="formatzero">Số buổi học</th>
        <th field="da_hoc" width="100" formatter="formatzero">Đã học</th>
        <th field="khuyen_mai" width="100" formatter="formatzero">Khuyến mại</th>
        <th field="bao_luu" width="100" formatter="formatzero"> Bảo lưu</th>
        <th field="conlai" width="100" formatter="formatzero">Còn lại</th>
        <th field="tinh_trang" width="130" align="center" formatter="tinhtrang">Tình trạng</th>
        <th field="ghi_chu" width="200">Ghi chú</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input class="easyui-combobox" name="tinhtrang" id="tinhtrang" valueField="id" textField="name" prompt="Tình trạng"
           url="donhang/tinhtrang" editable="false">
    <input class="easyui-combobox" name="khachhang" id="khachhang" valueField="id" textField="name"
           url="common/khachhang"
           prompt="Khách hàng">
    <input class="easyui-combobox" name="hocvien" id="hocvien" valueField="id" textField="name"
           url="common/hocvien"
           prompt="Học viên">
    <input id="loaidh" name="loaidh" class="easyui-combobox" style="width:200px" prompt="Loại đơn hàng" panelHeight="auto"
            data-options="valueField:'id',textField:'text',
                    data:[{'id':'1','text':'Online'},
                        {'id':'2','text':'Offline'}]" />       
    <!-- <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Từ khóa" /> -->
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
    <button class="easyui-linkbutton" iconCls="icon-excel" plain="false" onclick="xuatfile()">Xuất excel</button>
</div>

<?php
    if($_SESSION['user']['id'] == 5){
        echo '<div id="dlg-edit" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons-edit" modal="true" iconCls="icon-thanhpho">
        <form id="fm-edit" method="post" enctype="multipart/form-data" action="donhang/updateST">
            <input type="hidden" id="id" name="id">
            <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                <tr>
                    <td>Số tiền:</td>
                    <td>
                        <input id="sotien" name="so_tien" class="easyui-textbox" style="width: 200px;height:25px;" required />
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div id="dlg-buttons-edit">
        <button class="easyui-linkbutton" iconCls="icon-ok" onclick="saveEditST()">Câp nhật</button>
        <button class="easyui-linkbutton" iconCls="icon-cancel" id="close-edit" >Bỏ qua</button>
</div>';
    }

?>

<div id="dlg" class="easyui-dialog" style="width: 1050px; height: auto; padding: 0px;" closable="false" closed="true"
     buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <div class="easyui-layout" style="width: 100%; height: 360px;overflow: hidden;">
            <div data-options="region:'west',split:false,title:'Thông tin đơn hàng'"
                 style="width: 65%;overflow: hidden;padding: 10px;">
                <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                    <tr>
                        <td>Khách hàng:</td>
                        <td>
                            <input id="khach_hang" name="khach_hang" class="easyui-combobox"
                                   style="width:200px; height:28px"
                                   url="common/khachhang" valueField="id" textField="name" required/>
                        </td>
                        <td>Số điện thoại:</td>
                        <td>
                            <input id="sdt" name="sdt" class="easyui-textbox" style="width:200px; height:28px"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Ngày tạo đơn:</td>
                        <td>
                            <input name="ngay_dang_ky" id="ngay_dang_ky" class="easyui-datebox"
                                   style="width:200px; height:28px" >
                        </td>
                        <!-- <td>
                            Loại KH:
                        </td>
                        <td>
                            <input id="loaikh1" name="loaikh" class="easyui-combobox" style="width:200px;height: 28px;"
                                   data-options="valueField:'id',textField:'text',
                            data:[{'id':'1','text':'Online'},
                                {'id':'2','text':'Offline'}] "/>
                        </td> -->
                    </tr>
                    <tr>
                        <td>Học viên:</td>
                        <td>
                            <input id="hoc_vien" name="hoc_vien" class="easyui-combobox"
                                   style="width:200px; height:28px"
                                   valueField="id" textField="name" required/>
                        </td>
                        <td>Khóa học:</td>
                        <td>
                            <input id="product" name="product" class="easyui-combobox" style="width: 200px;height:28px;"
                                   url="common/khoahoc" valueField="id" textField="name" required editable="false"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Số buổi:</td>
                        <td>
                            <input id="so_buoi" name="so_buoi" class="easyui-numberbox"
                                   style="width: 200px;height:28px;"/>
                        </td>
                        <td>Số tiền:</td>
                        <td>
                            <input id="so_tien" name="so_tien" class="easyui-numberbox"
                                   style="width: 200px;height:28px;"
                                   groupSeparator="," required/>
                        </td>
                    </tr>
                    <tr>
                        <td>Số buổi KM:</td>
                        <td>
                            <input id="khuyen_mai" name="khuyen_mai" class="easyui-numberbox"
                                   style="width: 200px;height:28px;"/>
                        </td>
                        <!--                <td>Số buổi bảo lưu:</td>-->
                        <!--                <td>-->
                        <!--                    <input id="bao_luu" name="bao_luu" class="easyui-numberbox" style="width: 200px;height:28px;"/>-->
                        <!--                </td>-->
                        <td>Đợt thanh toán:</td>
                        <td>
                            <input id="dot_thanh_toan" name="dot_thanh_toan" class="easyui-combobox"
                                   style="width:200px;height: 28px;"
                                   data-options="valueField:'id',textField:'text',
                           data:[{'id':'1','text':'1 đợt'},
                                {'id':'2','text':'2 đợt'},
                                {'id':'3','text':'3 đợt'},
                                {'id':'4','text':'4 đợt'},
                                {'id':'5','text':'5 đợt'},
                                {'id':'6','text':'6 đợt'}] " required/>
                        </td>
                    </tr>
                    <tr>
                        <td>Giáo viên:</td>
                        <td>
                            <input id="giao_vien" name="giao_vien" class="easyui-combobox"
                                   style="width:200px; height:28px"
                                   url="common/giaovien" valueField="id" textField="name"/>
                        </td>
                        <td>Tình trạng:</td>
                        <td>
                            <input class="easyui-combobox" name="tinh_trang" id="tinh_trang" valueField="id"
                                   textField="name"
                                   url="donhang/tinhtrang" style="width: 200px;height:28px;" required editable="false">
                        </td>
                    </tr>
                    <tr>
                        <td>Nhân viên sale:</td>
                        <td>
                            <input id="nhan_vien_sale" name="nhan_vien_sale" class="easyui-combobox"
                                   style="width:200px; height:28px"
                                   url="common/nhanvien" valueField="id" textField="name" />
                        </td>
                        <td>Phân loại sale:</td>
                        <td>
                            <input class="easyui-combobox" name="phan_loai_sale" id="phan_loai_sale" valueField="id"
                                   textField="name"
                                   url="donhang/phanloaisale" style="width: 200px;height:28px;" />
                        </td>
                    </tr>
                    <tr>
                        <td>Ghi chú:</td>
                        <td>
                            <input class="easyui-textbox" multiline="true" style="width:200px; height:40px" id="ghi_chu"
                                   name="ghi_chu">
                        </td>
                        <!-- <td>
                            Loại đơn hàng:
                        </td>
                        <td>
                            <input id="loaidh" name="loaidh" class="easyui-combobox" style="width:200px;height: 28px;"
                                   data-options="valueField:'id',textField:'text',
                           data:[{'id':'1','text':'Online'},
                                {'id':'2','text':'Offline'}] "/>
                        </td> -->
                    </tr>
                </table>
            </div>
            <div data-options="region:'center',split:false,title:'Đợt thanh toán'"
                 style="width: 35%;overflow: hidden;padding: 10px;">
                <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                    <tr>
                        <td>Đợt 1:</td>
                        <td>
                            <input id="sotiendot1" name="sotiendot1" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot1" id="ngaydot1" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 2:</td>
                        <td>
                            <input id="sotiendot2" name="sotiendot2" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot2" id="ngaydot2" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 3:</td>
                        <td>
                            <input id="sotiendot3" name="sotiendot3" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot3" id="ngaydot3" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 4:</td>
                        <td>
                            <input id="sotiendot4" name="sotiendot4" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot4" id="ngaydot4" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 5:</td>
                        <td>
                            <input id="sotiendot5" name="sotiendot5" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot5" id="ngaydot5" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 6:</td>
                        <td>
                            <input id="sotiendot6" name="sotiendot6" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot6" id="ngaydot6" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" id="btnSave" onclick="save()">Cập nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close');">Bỏ qua
    </button>
</div>

<div id="dlg-nhap" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true"
     buttons="#dlg-buttons-nhap" modal="true" iconCls="icon-thanhpho">
    <form id="fm-nhap" method="post" enctype="multipart/form-data" action="donhang/import">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Loại KH:</div>
                </td>
                <td>
                    <input id="loaikh" name="loaikh" class="easyui-combobox" style="width:200px;height: 28px;"
                           data-options="valueField:'id',textField:'text',
                           data:[{'id':'1','text':'Online'},
                                {'id':'2','text':'Offline'}] " required/>
                </td>
            </tr>
            <tr>
                <td>
                    <div>Tải file:</div>
                </td>
                <td>
                    <input id="file" name="file" class="easyui-filebox" style="width:200px;height: 28px;"
                           prompt="Tải file excel..."/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-nhap">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="nhapexel()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-nhap').dialog('close')">Bỏ qua
    </button>
</div>


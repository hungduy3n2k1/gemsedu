<script type="text/javascript" src="js/donhang.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="donhang/json" pagination="true" idField="id" rownumbers="true"
    fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-phieuxuat" data-options="border:false,fit:true" showFooter="false"
    pageSize="50">
    <thead>
        <tr>
          <th field="name" width="100" sortable="true">Mã đơn hàng</th>
          <th field="ngaykt" width="100" sortable="true" formatter="format_canhbao">Ngày kết thúc</th>
          <th field="khachhang" width="150" sortable="true">Khách hàng</th>
          <th field="sanpham" width="150" sortable="true">Gói dịch vụ</th>
          <th field="link" width="150" sortable="true">Tên miền</th>
          <th field="thoi_han" width="100" align="center" formatter="thoihan">Thời hạn</th>
          <th field="so_tien" width="100" align="right" formatter="CurrencyFormatted">Số tiền</th>
        </tr>
    </thead>
</table>

<div id="dlg-search" class="easyui-dialog" style="width:90%; height: auto; padding: 10px; top:30px" closable="false" closed="true"
    buttons="#dlg-buttons-2" modal="true">
    <form id="fm-search" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Loại đơn hàng:</td>
                <td>
                  <input class="easyui-combobox" name="tinhtrang" id="tinhtrang" valueField="id" textField="text" prompt="Tình trạng"
                      url="common/loaidonhang" >
                </td>
            </tr><tr>
                <td>Dịch vụ:</td>
                <td>
                  <input class="easyui-combobox" name="dichvu" id="dichvu" valueField="id" textField="name" prompt="Dịch vụ"
                      url="common/dichvu" >
                </td>
            </tr><tr>
                <td>Khách hàng:</td>
                <td>
                  <input class="easyui-combobox" name="sohuu" id="sohuu" valueField="id" textField="name" url="common/khachhang?loai=1"
                      prompt="Khách hàng" >
                </td>
            </tr><tr>
                <td>Từ khóa:</td>
                <td>
                    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width:200px; height:25px" prompt="Từ khóa" />
                </td>
                </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="timkiem()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-search').dialog('close')">Bỏ qua</button>
</div>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="javascript:$('#dlg-search').dialog({closed:false})" style="font-size:14px">
            <img src="libs/jquery/images/search.png" width="32" height="32" />
            <br>Tìm kiếm
        </div>
    </div>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>VELO - Phần mềm quản lý kho và bán hàng</title>
	<style>
		body {font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px;}
        .letter-head {width:100%; float:left; margin-bottom:20px;}
        .head-left {width:70%; float:left}
        .head-right {width:30%; text-align:right; float:left}
        .indam { font-weight:bold}
		.tieude { text-align:center; margin-bottom:20px; width:100%;}
		.thongtin {margin-bottom:20px; float:left; width:100%;  }
        .tin-left { width:50%; float:left}
        .tin-right {width:50%; float:left}
		.noidung {float:left; margin-bottom:20px; width:100%; }
		table.main {
			border-collapse: collapse; border:#666 1px solid;
			width: 100%;
		}
		table.main th, table.main td {
			text-align: left;
			padding: 8px; border: 1px solid #666;
		}
		table.main tr.title {background-color:#CCC}
		/*table.main tr:nth-child(even){background-color: #f2f2f2}*/
		.footer {float:left; margin-bottom:20px; width:100%}
		.chuky {float:left; margin-bottom:20px; width:100%}
		.noprint {float:left; text-align:center; width:100%}
		@media print{
		   .noprint{
			   display:none;
		   }
		}
    </style>
</head>
<body>

<div class="letter-head">
	<div class="head-left">
        <span class="indam"><?php echo $_SESSION['cuahang']['ten_cua_hang'] ?></span> <br />
        Email: <span class="indam"><?php echo $_SESSION['cuahang']['email'] ?></span> <br />
        Điện thoại: <span class="indam"><?php echo $_SESSION['cuahang']['dien_thoai'] ?></span>
    </div> 
    <div class="head-right">
    	Số phiếu: <span class="indam"><?php echo $this->phieu[0]['so_phieu']; ?></span><br />
    	Ngày giờ: <span class="indam"><?php echo date("d/m/Y h:i:s",strtotime($this->phieu[0]['ngay_gio'])); ?></span><br />
        Người lập phiếu:<span class="indam"><?php echo $this->phieu[0]['nhanvien']; ?></span> 
    </div>
</div>
<div class="tieude">
	<h1>BẢNG KÊ NHẬP HÀNG</h1>
</div>
<div class="thongtin">
	<div class="tin-left">
    	<span class="indam">Tên khách hàng:</span> <?php echo $this->phieu[0]['khachhang']; ?> <br />
        <span class="indam">Địa chỉ:</span> <?php echo $this->phieu[0]['diachi']; ?> <br />
        <span  class="indam">Số điện thoại:</span> <?php echo $this->phieu[0]['dien_thoai']; ?> <br />
   	</div>
	<div class="tin-right">
  	</div>
</div> 
<div class="noidung">
	<table class="main" >
		<tr class="title">
        	<th>STT</th>
            <th>Tên hàng</th>
            <th>Đơn vị tính</th>
            <th>Số lượng</th>
            <th>Đơn giá</th>
            <th>Thành tiền</th>
             
      	</tr>
		<?php
        	$i = 1; 
			$tong=0;
            foreach($this->hanghoa as $row){
				$thanhtien=$row['so_luong']*$row['don_gia'];
				$tong=$tong+$thanhtien;
			?>
            <tr>
            	<td style="text-align: center;"><?php echo $i ?></td>
                <td style="text-align: left;"><?php echo $row['tenhang']; ?></td>
                <td style="text-align: center;"><?php echo $row['donvitinh'] ?></td>
                <td style="text-align: center;"><?php echo number_format($row['so_luong'],0,'.',',') ?></td>
                <td style="text-align: right;"><?php echo number_format($row['don_gia'],0,'.',',') ?></td>
                <td style="text-align: right;"><?php echo number_format($thanhtien,0,'.',',') ?></td>    
                 
			</tr>
            <?php $i++; 
                        }
            ?>
           	<tr>
            	<td colspan="5" style="font-weight: bold;text-align: right;">Tổng hóa đơn</td>
              	<td style="text-align: right;font-weight: bold;"><?php echo number_format($tong,0,'.',',') ?></td>
			</tr>
		</table>
</div><!--end info-nnhapkho-->
<div class="footer">
</div> 
<div class="chuky">
	<table width="100%">
        <tr>
        	<td style="text-align: center; width:20%"><b>Khách hàng</b></td>
            <td style="text-align: center; width:20%"><b>NV Giao hàng</b></td>
            <td style="text-align: center; width:20%"><b>NV Kiểm hàng</b></td>
            <td style="text-align: center; width:20%"><b>NV Đóng hàng</b></td>
            <td style="text-align: center; width:20%"><b>Thủ kho</b></td>
        </tr>        
    </table>
</div> 

<div class="noprint">
    <button style="height:50px; padding:10px; font-size:18px" onClick="window.print();">In ra</button>
    <button style="height:50px; padding:10px; font-size:18px" onClick="window.location.href = '<?php echo URL; ?>'">Quay lại</button>
</div>
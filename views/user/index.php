<script type="text/javascript" src="js/user.js"></script>
<table id="dg" style="width: 100%;" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true"
       fitColumns="false" 
       singleSelect="true" iconCls="icon-user" data-options="border:false,fit:true" url="user/json"
       class="easyui-datagrid" pageSize="30">
    
    <thead>
    <tr>
        <th field="email" width="170">Tên đăng nhập</th>
        <th field="nhanvien" width="180" sortable="true">Tên nhân viên</th>
        <th field="giaovien" width="180" sortable="true">Tên giáo viên</th>
        <th field="phongban" width="150">Phòng ban</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:4px; text-align:left">
    <input id="email" name="email" class="easyui-textbox" style="width: 150px;height:28px;" prompt="Tên đăng nhập"/>
    <input class="easyui-combobox" name="khachhang" id="nhanvien" valueField="id" textField="name"
           url="common/nhanvien"
           prompt="Tên nhân viên">
    <input class="easyui-combobox" name="khachhang" id="giaovien" valueField="id" textField="name"
           url="common/giaovien"
           prompt="Tên giáo viên">
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <br>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closed="true"
     buttons="#dlg-buttons" modal="true" iconCls="icon-user">
    <form id="fm" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên đăng nhập:</td>
                <td><input id="email" name="email" class="easyui-textbox" style="width: 200px;height:25px;" required/>
                </td>
            </tr>
            <tr>
                <td>Nhân viên:</td>
                <td>
                    <input id="nhan_vien" name="nhan_vien" class="easyui-combobox" style="width:200px; height:25px"
                           url="common/nhanvien" valueField="id" textField="name"/>
                </td>
            </tr>
            <tr>
                <td>Giáo viên:</td>
                <td>
                    <input id="giao_vien" name="giao_vien" class="easyui-combobox" style="width:200px; height:25px"
                           url="common/giaovien" valueField="id" textField="name"/>
                </td>
            </tr>
            <tr>
                <td>Phòng ban:</td>
                <td>
                    <input id="nhom" name="nhom" class="easyui-combobox" style="width:200px; height:25px"
                           url="common/phongban" valueField="id" textField="name"/>
                </td>
            </tr>
            <tr>
                <td>Mật khẩu:</td>
                <td><input id="matkhau" name="matkhau" class="easyui-passwordbox" style="width: 200px;height:25px;"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dlg').dialog('close')">Quay lại
    </button>
</div>
<!-- End From -->
<div id="dlg-role" class="easyui-dialog" style="width: 670px; height:500px; padding: 10px; top:100p" closed="true"
     buttons="#dlg-buttons-1" modal="true" iconCls="icon-user">
    <ul id="menu" class="easyui-tree"
        data-options="url:'common/menu',method:'get',animate:true,checkbox:true,cascadeCheck:false"></ul>
    <form id="fm-role" method="post">
        <input id="role" name="role" type="hidden">
    </form>
</div>
<div id="dlg-buttons-1">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="saverole()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dlg-role').dialog('close')">Quay lại
    </button>
</div>


<div id="dlg-rolef" class="easyui-dialog" style="width: 670px; height:500px; padding: 10px; top:100p" closed="true"
     buttons="#dlg-buttons-f" modal="true" iconCls="icon-user">
    <ul id="thumuc" class="easyui-tree"
        data-options="url:'common/folderphanquyen',method:'get',animate:true,checkbox:true,cascadeCheck:false"></ul>
    <form id="fm-rolef" method="post">
        <input id="rolef" name="rolef" type="hidden">
    </form>
</div>
<div id="dlg-buttons-f">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="saverolef()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dlg-rolef').dialog('close')">Quay lại
    </button>
</div>

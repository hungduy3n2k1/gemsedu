<script type="text/javascript" src="js/contact.js"></script>
<table
    id="dg"
    style="width: 100%;"
    url="contact/json"
    class="easyui-datagrid"
    toolbar="#toolbar"
    pagination="true"
    idField="id"
    rownumbers="true"
    fitColumns="true"
    singleSelect="true"
    nowrap="true"
    data-options="border:false,fit:true,iconCls:'icon-nhaphang'"
    pageSize="30"
>
    <thead>
        <tr>
            <th field="name" width="150">Họ tên</th>
            <th field="email" width="300">Email</th>
            <th field="dien_thoai" width="120">Điện thoại</th>
            <th field="facebook" width="150">Facebook</th>
            <th field="zalo" width="120">Zalo</th>
            <th field="doitac" width="200">Khách hàng</th>
            <th field="chuc_vu" width="150">Chức vụ</th>
        </tr>
    </thead>
</table>

<div id="toolbar" style="padding-top: 5px; padding-bottom: 5px;">
<!--    <span style="color: blue; font-weight: bold; margin-right: 20px;">DANH SÁCH LIÊN HỆ</span>-->
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Tìm kiếm" />
    &nbsp;&nbsp;&nbsp;
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
          foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 700px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Họ tên:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px; height: 25px;" />
                </td>
                <td>Khách hàng:</td>
                <td>
                    <input id="khach_hang" name="khach_hang" class="easyui-combobox" style="width: 200px; height: 25px;" url="common/khachhang" valueField="id" textField="name" />
                </td>
            </tr>
            <tr>
                <td>Chức vụ:</td>
                <td>
                    <input id="chuc_vu" name="chuc_vu" class="easyui-textbox" style="width: 200px; height: 25px;" />
                </td>
                <td>Điện thoại:</td>
                <td>
                    <input id="dien_thoai" name="dien_thoai" class="easyui-textbox" style="width: 200px; height: 25px;" />
                </td>
            </tr>
            <tr>
                <td>Email:</td>
                <td>
                    <input id="email" name="email" class="easyui-textbox" style="width: 200px; height: 25px;" />
                </td>
                <td>Facebook:</td>
                <td>
                    <input id="facebook" name="facebook" class="easyui-textbox" style="width: 200px; height: 25px;" />
                </td>
            </tr><tr>
                <td>Zalo:</td>
                <td>
                    <input id="zalo" name="zalo" class="easyui-textbox" style="width: 200px; height: 25px;" />
                </td>
                <td>Ghi chú:</td>
                <td>
                    <input id="ghi_chu" name="ghi_chu" class="easyui-textbox" style="width: 200px; height: 25px;" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-nhap" class="easyui-dialog" style="width: 350px; height: auto; padding: 15px;" closable="false"
     closed="true" buttons="#dlg-buttons-nhap" modal="true" iconCls="icon-thanhpho" resizable="true">
    <form id="fm-nhap" method="post" action="contact/import" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên file:</td>
                <td>
                    <input id="file" name="file" class="easyui-filebox" style="width: 230px;"
                           prompt="Tải file data lên..." required/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-nhap">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="nhapexel()">Tải lên</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-nhap').dialog('close')">Bỏ qua
    </button>
</div>

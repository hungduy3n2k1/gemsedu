<script type="text/javascript" src="js/baocaohv.js"></script>
<table id="dg" style="width: 100%;" url="baocaohv/json" class="easyui-datagrid" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="false" data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
    <thead>
        <tr>
            <!--        <th field="ma_hoc_vien" width="120">Mã học viên (Cũ)</th>-->
            <!--        <th field="id" width="70">ID</th>-->
            <th field="mahocvien" width="100">Mã học viên</th>
            <th field="name" width="160">Họ tên</th>
            <th field="ngaysinh" width="120">Ngày sinh</th>
            <th field="khachhang" width="150">Phụ huynh</th>
            <th field="dienthoaikh" width="130">Điện thoại</th>
            <th field="emailkh" width="120">Email</th>
            <th field="tongbuoi" width="80">Tổng buổi</th>
            <th field="dahoc" width="80">Đã học</th>
            <th field="conlai" width="80">Còn lại</th>
            <th field="ngaybatdau" width="120">Ngày bắt đầu</th>
            <th field="ngayketthuc" width="120">Ngày kết thúc</th>
            <th field="giaovien" width="200">Giáo viên</th>
            <th field="chuyenmon" width="130">Chuyên môn</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <!-- Từ ngày: <input id="tungay" name="tungay" class="easyui-datebox" style="width: 150px;height:28px;" prompt="Từ ngày" value="<?= "01/" . date("m/Y") ?>"/>&nbsp;
    Đến ngày: <input id="denngay" name="denngay" class="easyui-datebox" style="width: 150px;height:28px;" prompt="Từ ngày" value="<?= date("d/m/Y") ?>"/>&nbsp; -->
    <input class="easyui-textbox" name="khachhang" id="khachhang" style="width: 150px;height:28px;" prompt="Khách hàng">&nbsp;
    <input class="easyui-textbox" name="hocvien" id="hocvien" style="width: 150px;height:28px;" prompt="Học viên">&nbsp;
    <input class="easyui-combobox" name="giaovien" id="giaovien" valueField="id" textField="name" prompt="Giáo viên" url="common/giaovien" style="width: 200px;height:28px;">&nbsp;
    <input class="easyui-combobox" name="chuyenmon" id="chuyenmon" valueField="id" textField="name" prompt="Chuyên môn" url="common/nhanvien" style="width: 200px;height:28px;">
    <!--    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width: 150px;height:28px;" prompt="Từ Khóa"/> &nbsp;&nbsp;&nbsp;-->
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
</div>
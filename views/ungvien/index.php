<script type="text/javascript" src="js/ungvien.js"></script>
<table id="dg" style="width: 100%;" url="ungvien/json" class="easyui-datagrid" toolbar="#toolbar" pagination="true"
    idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="false"
    data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
    <thead>
        <tr>
            <th field="name" width="150">Họ tên</th>
            <th field="gioi_tinh" width="100" formatter="gioitinh">Giới tính</th>
            <th field="ngaysinh" width="150">Ngày sinh</th>
            <th field="email_canhan" width="250"> Email</th>
            <th field="dien_thoai" width="120">Điện thoại</th>
            <!-- <th field="facebook" width="250">Facebook</th> -->
            <th field="zalo" width="120">Vị trí ứng tuyển</th>
            <th field="dia_chi" width="200">Địa chỉ</th>
            <th field="ghi_chu" width="250">Ghi chú</th>
            <th field="cv" width="120" formatter="taixuong">CV</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <!-- <span style="color:blue; font-weight:bold; margin-right:20px">THÊM MỚI ỨNG VIÊN</span> -->
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Tìm kiếm" /> &nbsp;&nbsp;&nbsp;
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
       foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width:1000px; padding:10px" closed="true" buttons="#dlg-buttons" modal="true" data-options="resizable:true">
    <form id="fm" method="post" enctype="multipart/form-data">
             <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                    <tr>
                        <td>Họ tên:</td>
                        <td>
                            <input id="name" name="name" class="easyui-textbox" required style="width: 200px;height:25px;" />
                        </td>
                        <td>Giới tính:</td>
                        <td>
                            <input class="easyui-combobox" name="gioi_tinh" id="gioi_tinh" required style="width: 200px;height:25px;"
                              url="common/gioitinh" valueField="id" textField="name" >
                        </td>
                        <td rowspan="7" style="text-align:center" width="30%">
                            <img id="hinhanh" height="100"><br>
                            <input class="easyui-filebox" data-options="prompt:'Tải ảnh lên...'"
                            style="width: 200px;height:25px;" name="file" ><br>
                            <input type="text" name="cv" id="cv" class="easyui-textbox" style="width: 200px;height:25px;" prompt="Đính kèm link CV" />
                            <input class="easyui-filebox" data-options=" prompt:'Tải CV lên...'" style="width: 200px;height:25px;" name="taicv" id="taicv" >
                        </td>
                    </tr><tr>
                        <td>Ngày sinh:</td>
                        <td>
                            <input id="ngaysinh" name="ngaysinh" class="easyui-datebox" required style="width: 200px;height:25px;" />
                        </td>
                        <td>Tình trạng hôn nhân:</td>
                        <td>
                          <input class="easyui-combobox" name="hon_nhan" id="hon_nhan" style="width: 200px;height:25px;"
                            url="common/honnhan" valueField="id" textField="name" >
                        </td>
                    </tr><tr>
                        <td>Địa chỉ:</td>
                        <td>
                            <input type="text" name="dia_chi" id="dia_chi" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Quê quán:</td>
                        <td>
                          <input class="easyui-combobox" name="que_quan" id="que_quan" style="width: 200px;height:25px;"
                            url="common/thanhpho" valueField="id" textField="name" >
                        </td>
                    </tr><tr>
                        <td>Điện thoại:</td>
                        <td>
                            <input id="dien_thoai" name="dien_thoai" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Email cá nhân:</td>
                        <td>
                            <input id="email_canhan" name="email_canhan" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                    </tr><tr>
                        <td>Vị trí ứng tuyển:</td>
                        <td>
                            <input id="zalo" name="zalo" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Trường đào tạo:</td>
                        <td>
                            <input id="truong_hoc" name="truong_hoc" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <!-- <td>Facebook:</td>
                        <td>
                            <input id="facebook" name="facebook" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td> -->
                    </tr><tr>
                        <td>Học vấn:</td>
                        <td>
                          <input class="easyui-combobox" name="trinh_do" id="trinh_do" style="width: 200px;height:25px;"
                            url="common/trinhdo" valueField="id" textField="name" >
                        </td>
                    </tr><tr>
                      <td>Ghi chú:</td>
                      <td colspan="4">
                          <input class="easyui-textbox" style="width:560px; height:60px" id="ghi_chu" name="ghi_chu" multiline="true">
                      </td>
                  </tr>
                </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

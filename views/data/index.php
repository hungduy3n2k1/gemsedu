<script type="text/javascript" src="js/data.js"></script>
<script type="text/javascript">
    function tinhtrang(val, row) {
        var tinhtrang = JSON.parse('<?php echo $this->tinhtrang ?>');
        return '<div style="background-color:' + tinhtrang[val]["bg_color"] + '; color:' + tinhtrang[val]["text_color"] + '; padding:5px">' + tinhtrang[val]["name"] + '</div>';
    }
</script>
<?php
//  echo($this->tinhtrang);
?>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="data/json" toolbar="#toolbar"
       pagination="true"
       idField="id" rownumbers="true" fitColumns="true" nowrap="false" iconCls="icon-baocao"
       data-options="border:false,fit:true" pageSize="50">
    <thead>
    <tr>
        <th field="ck" checkbox="true"></th>
        <!-- <th field="id" width="120">ID</th> -->
        <th field="ngaynhap" width="120">Ngày nhập</th>
        <th field="nguoinhap" width="200">Người nhập</th>
        <th field="ho_ten" width="230">Khách hàng</th>
        <th field="dien_thoai" width="150" align="center">Số điện thoại</th>
        <th field="hoc_vien" width="200">Học viên</th>
        <!--        <th field="ngaysinh" width="120">Ngày sinh</th>-->
        <th field="loaikh" width="150">Loại KH</th>
        <th field="ngaychia" width="120">Ngày chia</th>
        <th field="nhanvien" width="200">Người chăm sóc</th>
        <th field="tinh_trang" width="160" align="center" formatter="tinhtrang">Tình trạng</th>
        <th field="ghi_chu" width="300">Ghi chú</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:6px; text-align:left ">
    <input id="tungay" name="tungay" class="easyui-datebox" style="width: 150px;height:28px;" prompt="Từ ngày"/>
    <input id="denngay" name="denngay" class="easyui-datebox" style="width: 150px;height:28px;" prompt="Đến ngày"/>
    <input id="kieungay" name="kieungay" class="easyui-combobox" style="width:150px"
           data-options="valueField:'id',textField:'text',data:[{'id':'1','text':'Ngày chia','selected':'true'},{'id':'2','text':'Ngày nhập'}] "/>
    <input name="nguoinhap" id="nguoinhap" class="easyui-combobox" style="width:150px" editable="true"
           url="common/nhanvien" valueField="id" textField="name" prompt="Người nhập">
    <input name="chonphanloai" id="chonphanloai" class="easyui-combobox" style="width:150px" editable="true"
           url="common/loaikh" valueField="id" textField="name" prompt="Phân loại">
    <?php
    //  if ($this->chiadata == 1)
    echo '<input name="nhanvien" id="nhanvien" class="easyui-combobox" style="width:150px" editable="true"
                  url="common/nhanvien" valueField="id" textField="name" prompt="Nhân viên chăm sóc">';
    ?>
    <input name="tinhtrang" id="tinhtrang" class="easyui-combobox" style="width:150px" editable="true"
           url="common/tinhtrang" valueField="id" textField="name" prompt="Tình trạng" panelHeight="auto">
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width: 150px;height:28px;" prompt="Từ khóa"/>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <br>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="' . $item['icon'] . '" plain="false" onclick="' . $item['link'] . '">' . $item['name'] . '</button> ';
    ?>
    <span style="color:blue; font-weight:bold"> Kích đúp vào data để cập nhật thông tin</span>
</div>
<!-- Nhập từ excel -->
<div id="dlg-nhap" class="easyui-dialog" style="width: 370px; height: auto; padding: 15px;" closable="false"
     closed="true" buttons="#dlg-buttons-nhap" modal="true" iconCls="icon-thanhpho" resizable="true">
    <form id="fm-nhap" method="post" action="data/import" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tải file mẫu:</td>
                <td>
                    <a target="_blank" href="<?= URL ?>/uploads/data hoc vien.xlsx" style="color: blue;">Link</a>
                </td>
            </tr>
            <!--            <tr>-->
            <!--                <td>Loại data</td>-->
            <!--                <td>-->
            <!--                    <input name="phanloai" id="phanloai" class="easyui-combobox" style="width:230px;height: 28px;"-->
            <!--                           editable="false"-->
            <!--                           valueField="id" textField="name" panelHeight="auto" panelHeight="auto" required-->
            <!--                           url="data/phanloaikh">-->
            <!--                </td>-->
            <!--            </tr>-->
            <tr>
                <td>Tên file:</td>
                <td>
                    <input id="file" name="file" class="easyui-filebox" style="width: 230px;"
                           prompt="Tải file data lên..."/>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-nhap">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="nhapexel()">Tải lên</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-nhap').dialog('close')">Bỏ qua
    </button>
</div>
<!-- Chia data cho nhân vien chăm sóc -->
<div id="dlg-chia" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false"
     closed="true"
     buttons="#dlg-buttons-chia" modal="true" iconCls="icon-reload">
    <form id="fm-chia" method="post" action="data/chia">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Nhân viên:</td>
                <td>
                    <input id="nguoinhan" name="nguoinhan" class="easyui-combobox" style="width: 200px;height:25px;"
                           required
                           url="common/nhanvien" valueField="id" textField="name" prompt="Nhân viên"/>
                    <input id="data" name="data" type="hidden">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-chia">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savechia()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-chia').dialog('close')">Quay lại
    </button>
</div>
<!-- Cập nhật -->
<div id="dlg-goi" class="easyui-dialog" style="width: 800px; height: auto; padding: 10px;" closable="false"
     closed="true" buttons="#dlg-buttons-goi" modal="true" iconCls="icon-thanhpho">
    <form id="fm-goi" method="post" action="data/updateData">
        <input type="hidden" id="id" name="id">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Khách hàng:</td>
                <td>
                    <input name="ho_ten" id="ho_ten" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
                <td>Nguồn data:</td>
                <td>
                    <input name="nguon_data" id="nguon_data" class="easyui-combobox" style="width:250px;height: 28px;"
                           valueField="id" textField="name" panelHeight="auto" panelHeight="auto"
                           url="common/nguondata">
                </td>
            </tr>
            <tr>
                <td>Email</td>
                <td>
                    <input name="email" id="email" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
                <td>Điện thoại:</td>
                <td>
                    <input name="dien_thoai" id="dien_thoai" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
            </tr>
            <tr>
                <td>Tên học viên:</td>
                <td>
                    <input name="hoc_vien" id="hoc_vien" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
                <td>Người phụ trách:</td>
                <td>
                    <input name="nhan_vien" id="nhan_vien" class="easyui-combobox" style="width:250px;height: 28px;"
                           valueField="id" textField="name" panelHeight="auto" panelHeight="auto"
                           url="common/nhanvien">
                </td>
            </tr>
            <!--            <tr>-->
            <!--                <td>Ngày học thử</td>-->
            <!--                <td>-->
            <!--                    <input name="ngayhoc" id="ngayhoc" class="easyui-datebox" style="width:250px;height: 28px;">-->
            <!--                </td>-->
            <!--                <td>Giờ học thử</td>-->
            <!--                <td>-->
            <!--                    <input name="gio_hoc" id="gio_hoc" class="easyui-timespinner" style="width:250px;height: 28px;">-->
            <!--                </td>-->
            <!--            </tr>-->
            <tr>
                <td>Loại KH</td>
                <td>
                    <input name="phan_loai" id="phan_loai" class="easyui-combobox" style="width:250px;height: 28px;"
                           editable="false"
                           valueField="id" textField="name" panelHeight="auto" panelHeight="auto"
                           url="common/loaikh">
                </td>
                <td>Tình trạng</td>
                <td>
                    <input name="tinh_trang" id="tinh_trang" class="easyui-combobox" style="width:250px;height: 28px;"
                           url="data/tinhtrang" valueField="id" textField="name" panelHeight="auto">
                </td>
            </tr>
            <tr>
                <td>Ghi chú:</td>
                <td>
                    <input name="ghi_chu" id="ghi_chu" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
            </tr>
            <tr>
                <!--                <td colspan="2">-->
                <!--                    <div id="phuhuynh"style="width:95%; height:200px"></div>-->
                <!--                </td>-->
                <td colspan="4">
                    <div id="lichsu" style="width:100%; height:200px"></div>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-goi">
    <button class="easyui-linkbutton" iconCls="icon-add" onclick="dangky()">Đăng ký</button>
    <button class="easyui-linkbutton" iconCls="icon-reload" onclick="demo()">Demo</button>
    <!-- <button class="easyui-linkbutton" iconCls="icon-hanghoa" onclick="trainghiem()">Trải nghiệm</button> -->
    <button class="easyui-linkbutton" iconCls="icon-phieuthu" onclick="nhatky()">Nhật ký</button>
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="capnhat()">Cập nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-goi').dialog('close')">Bỏ qua
    </button>
</div>

<div id="dlg-ph" class="easyui-dialog" style="width: 350px; padding:10px; top:60px" closable="false" closed="true"
     buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" action="data/addph">
        <input id="hocvien" name="hocvien" type="hidden">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên phụ huynh:</td>
                <td>
                    <input id="hotenphuhuynh" name="hotenphuhuynh" class="easyui-textbox" required="required"
                           style="width: 200px;height:28px;"/>
                </td>

            </tr>
            <tr>
                <td>Số điện thoại:</td>
                <td>
                    <input id="dienthoaiphuhuynh" name="dienthoaiphuhuynh" class="easyui-textbox"
                           style="width: 200px;height:28px;" required/>
                </td>
            </tr>
        </table>
        <br>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-ph').dialog('close')">Bỏ qua
    </button>
</div>

<div id="dlg-baocao" class="easyui-dialog" style="width: 350px; padding: 10px;" closable="false"
     closed="true" buttons="#dlg-buttons-baocao" modal="true" iconCls="icon-thanhpho">
    <form id="fm-nhatky" method="post" action="data/addnhatky">
        <input id="idhocvien" name="idhocvien" type="hidden">
        <input class="easyui-textbox" label="Nội dung:" labelPosition="top" multiline="true"
               style="width:100%;height:120px"
               required id="nhatky" name="nhatky">
    </form>
</div>
<div id="dlg-buttons-baocao">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="ghinhatky()">Ghi lại</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-baocao').dialog('close')">Đóng
    </button>
</div>

<!--Thêm data mới-->
<div id="dlg-add" class="easyui-dialog" style="width: 800px; height: auto; padding: 10px;" closable="false"
     closed="true" buttons="#dlg-buttons-add" modal="true" iconCls="icon-thanhpho">
    <form id="fm-add" method="post" action="data/add">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Khách hàng:</td>
                <td>
                    <input name="ho_ten" id="ho_ten1" class="easyui-textbox" style="width:250px;height: 28px;" required>
                </td>
                <td>Nguồn data:</td>
                <td>
                    <input name="nguon_data" id="nguon_data" class="easyui-combobox" style="width:250px;height: 28px;"
                           valueField="id" textField="name" panelHeight="auto" panelHeight="auto"
                           url="common/nguondata">
                </td>
            </tr>
            <tr>
                <td>Email</td>
                <td>
                    <input name="email" id="email1" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
                <td>Điện thoại:</td>
                <td>
                    <input name="dien_thoai" id="dien_thoai1" class="easyui-textbox" style="width:250px;height: 28px;"
                           required>
                </td>
            </tr>
            <tr>
                <td>Tên học viên:</td>
                <td>
                    <input name="hoc_vien" id="hoc_vien1" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
                <td>Loại KH</td>
                <td>
                    <input name="phan_loai" id="phan_loai1" class="easyui-combobox" style="width:250px;height: 28px;"
                           editable="false"
                           valueField="id" textField="name" panelHeight="auto" panelHeight="auto"
                           url="common/loaikh">
                </td>
            </tr>
            <tr>
                <td>Ghi chú:</td>
                <td>
                    <input name="ghi_chu" id="ghi_chu1" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-add">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="saveadd()">Cập nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-add').dialog('close')">Bỏ qua
    </button>
</div>

<!--Đăng ký học thử-->
<div id="dlg-demo" class="easyui-dialog" style="width: 1040px; height: auto; padding: 0px;display:table-cell"
     closable="false"
     closed="true" buttons="#dlg-buttons-demo" modal="true" iconCls="icon-thanhpho">
    <form id="fm-demo" method="post">
        <div class="easyui-layout" style="width: 99%; height: 260px;overflow: hidden;">
            <div data-options="region:'west',split:false,title:'Thông tin đăng ký'"
                 style="width: 35%;overflow: hidden;padding: 10px;">
                <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                    <tr>
                        <td>Khóa học:</td>
                        <td>
                            <input name="khoa_hoc" id="khoa_hocdm" class="easyui-combobox"
                                   style="width:200px;height: 28px;"
                                   editable="false"
                                   valueField="id" textField="name" 
                                   url="common/khoahoctn" required>
                        </td>
                    </tr>
                    <tr>
                        <td>Phân loại:</td>
                        <td>
                            <input id="loailop" name="loailop" class="easyui-combobox" style="width:200px;height: 28px;"
                                   valueField="id" textField="name" 
                                   url="data/phanloaidemo" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>Tên học viên:</td>
                        <td>
                            <input name="ten_hoc_vien" id="ten_hoc_vien" class="easyui-textbox"
                                   style="width:200px;height: 28px;" required>
                        </td>
                    </tr>
                    <tr>
                        <td>Ngày sinh:</td>
                        <td>
                            <input name="ngay_sinh" id="ngay_sinh" class="easyui-datebox"
                                   style="width:200px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Số điện thoại:</td>
                        <td>
                            <input name="dienthoai" id="dienthoai" class="easyui-textbox"
                                   style="width:200px;height: 28px;">
                        </td>
                    </tr>
                </table>
            </div>
            <div data-options="region:'center',split:false,title:'Thông tin lớp học'"
                 style="width: 65%;overflow: hidden;padding: 10px;">
                <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                    <tr>
                        <td>Tên lớp học:</td>
                        <td>
                            <input id="tenlopdm" name="tenlop" class="easyui-textbox" required
                                   style="width: 220px;height:28px;"/>
                        </td>
                        <td>Giáo viên:</td>
                        <td>
                            <input class="easyui-combobox" name="giao_vien" id="giao_viendm"
                                   style="width: 220px;height:28px;"
                                   url="common/giaovien" valueField="id" textField="name">
                        </td>
                    </tr>
                    <tr>
                        <td>Giáo trình:</td>
                        <td>
                            <input class="easyui-combobox" name="giao_trinh" id="giao_trinhdm"
                                   style="width: 220px;height:28px;"
                                   url="common/giaotrinh" valueField="id" textField="name">
                        </td>
                        <td>Số buổi:</td>
                        <td>
                            <input class="easyui-textbox" name="so_buoi" id="so_buoidm" style="width: 220px;height:28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Ngày bắt đầu:</td>
                        <td>
                            <input class="easyui-datebox" name="ngaybatdau" id="ngaybatdaudm"
                                   style="width: 220px;height:28px;" required>
                        </td>
                        <td>Buổi học:</td>
                        <td>
                            <input class="easyui-combobox" name="buoihoc[]" id="buoihocdm"
                                   style="width: 220px;height:28px;"
                                   multiple="true"
                                   url="common/thu" valueField="id" textField="name">
                        </td>
                    </tr>
                    <tr>
                        <td>Giờ học:</td>
                        <td>
                            <input id="giohoc" name="giohoc" class="easyui-timespinner"
                                   style="width: 220px;height:28px;"/>
                        </td>
                        <td>Thời lượng:</td>
                        <td>
                            <input id="thoi_luong" name="thoi_luong" class="easyui-numberbox"
                                   style="width: 220px;height:28px;" required/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>
<div id="dlg-buttons-demo">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savedemo()">Cập nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-demo').dialog('close')">Bỏ qua
    </button>
</div>

<!--Đăng ký học TN-->
<div id="dlg-trainghiem" class="easyui-dialog" style="width: 740px; height: auto; padding: 10px;display:table-cell"
     closable="false"
     closed="true" buttons="#dlg-buttons-trainghiem" modal="true" iconCls="icon-thanhpho">
    <form id="fm-trainghiem" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên học viên:</td>
                <td>
                    <input name="ten_hoc_vien" id="ten_hoc_vientn" class="easyui-textbox"
                           style="width:250px;height: 28px;" required>
                </td>
            </tr>
            <tr>
                <td>Khóa học:</td>
                <td>
                    <input name="khoa_hoc" id="khoa_hoctn" class="easyui-combobox" style="width:250px;height: 28px;"
                           editable="false"
                           valueField="id" textField="name" panelHeight="auto" panelHeight="auto"
                           url="common/khoahoctn" required>
                </td>
                <td>Số buổi:</td>
                <td>
                    <input name="so_buoi" id="so_buoitn" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
            </tr>
            <tr>
                <td>Ngày sinh:</td>
                <td>
                    <input name="ngay_sinh" id="ngay_sinhtn" class="easyui-datebox" style="width:250px;height: 28px;">
                </td>
                <td>Số điện thoại:</td>
                <td>
                    <input name="dienthoai" id="dienthoaitn" class="easyui-textbox" style="width:250px;height: 28px;">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-trainghiem">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savetn()">Cập nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-trainghiem').dialog('close')">Bỏ
        qua
    </button>
</div>

<div id="dlg-dangky" class="easyui-dialog" style="width: 750px; height: 430px; padding:0px;overflow: hidden;"
     closable="false"
     closed="true" buttons="#dlg-buttons-dangky" modal="true" iconCls="icon-thanhpho" title="Đăng ký khóa học">
    <form id="fm-dangky" method="post">
        <div class="easyui-layout" style="width: 99%; height: 430px;overflow: hidden;">
            <div data-options="region:'west',split:false,title:'Thông tin đăng ký'"
                 style="width: 45%;overflow: hidden;padding: 10px;">
                <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                    <tr>
                        <td>Tên học viên:</td>
                        <td>
                            <input name="ten_hoc_vien" id="ten_hoc_viendk" class="easyui-textbox"
                                   style="width:200px;height: 28px;" required>
                        </td>
                    </tr>
                    <tr>
                        <td>Phân loại:</td>
                        <td>
                            <input name="loailop" id="loailopdk" class="easyui-combobox"
                                   style="width:200px;height: 28px;"
                                   valueField="id" textField="name" panelHeight="auto" panelHeight="auto"
                                   url="data/phanloai" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>Khóa học:</td>
                        <td>
                            <input id="product" name="product" class="easyui-combobox" style="width: 200px;height:28px;"
                                   url="common/khoahoc" valueField="id" textField="name" required editable="false"
                                   required/>
                        </td>
                        <input type="hidden" id="thangtt" name="thangtt">
                    </tr>
                    <tr>
                        <td>Số tiền:</td>
                        <td>
                            <input id="so_tien" name="so_tien" class="easyui-numberbox"
                                   style="width:200px;height: 28px;"
                                   groupSeparator="," required/>
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt thanh toán:</td>
                        <td>
                            <input id="dot_thanh_toan" name="dot_thanh_toan" class="easyui-combobox"
                                   style="width:200px;height: 28px;"
                                   data-options="valueField:'id',textField:'text',
                           data:[{'id':'1','text':'1 đợt'},
                                {'id':'2','text':'2 đợt'},
                                {'id':'3','text':'3 đợt'},
                                {'id':'4','text':'4 đợt'},
                                {'id':'5','text':'5 đợt'},
                                {'id':'6','text':'6 đợt'}] "/>
                        </td>
                    </tr>
                    <tr>
                        <td>Số buổi học:</td>
                        <td>
                            <input name="buoi_hoc" id="buoi_hoc" class="easyui-numberbox"
                                   style="width:200px;height: 28px;"
                                   readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>Số buổi KM:</td>
                        <td>
                            <input name="buoi_hoc_km" id="buoi_hoc_km" class="easyui-numberbox"
                                   style="width:200px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Ngày đăng ký:</td>
                        <td>
                            <input name="ngay_dang_ky" id="ngay_dang_ky" class="easyui-datebox"
                                   style="width:200px;height: 28px;" readonly>
                        </td>
                    </tr>
                </table>
            </div>
            <div data-options="region:'center',split:false,title:'Đợt thanh toán'"
                 style="width: 60%;overflow: hidden;padding: 10px;">
                <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                    <tr>
                        <td>Đợt 1:</td>
                        <td>
                            <input id="sotiendot1" name="sotiendot1" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot1" id="ngaydot1" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 2:</td>
                        <td>
                            <input id="sotiendot2" name="sotiendot2" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot2" id="ngaydot2" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 3:</td>
                        <td>
                            <input id="sotiendot3" name="sotiendot3" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot3" id="ngaydot3" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 4:</td>
                        <td>
                            <input id="sotiendot4" name="sotiendot4" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot4" id="ngaydot4" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 5:</td>
                        <td>
                            <input id="sotiendot5" name="sotiendot5" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot5" id="ngaydot5" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Đợt 6:</td>
                        <td>
                            <input id="sotiendot6" name="sotiendot6" prompt="Số tiền" class="easyui-numberbox"
                                   style="width:100px;height: 28px;"
                                   groupSeparator=","/>
                        </td>
                        <td>Ngày TT:</td>
                        <td>
                            <input name="ngaydot6" id="ngaydot6" class="easyui-datebox"
                                   style="width:100px;height: 28px;">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>
<div id="dlg-buttons-dangky">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savedk()">Cập nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-dangky').dialog('close')">Bỏ qua
    </button>
</div>
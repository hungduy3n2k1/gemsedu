<script type="text/javascript" src="js/phieuchi.js"></script>
<table id="dg" style="width: 100%;" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="true"
    pageSize="50" showFooter="true" class="easyui-datagrid"
    singleSelect="true" nowrap="false" iconCls="icon-phieuchi" data-options="border:false,fit:true" url="phieuchi/json">
    </thead>
    <thead>
        <tr>
            <th field="ngaygio" width="150" sortable="true">Ngày giờ</th>
            <th field="name" width="100" sortable="true">Chứng từ</th>
            <th field="khachhang" width="180" >Khách hàng</th>
            <th field="dien_giai" width="250" >Nội dung</th>
            <th field="so_tien" width="100" align="right" formatter="FormatToCurrency">Số tiền</th>
            <th field="taikhoan" width="180" >Tài khoản</th>
            <th field="ghi_chu" width="230" >Ghi chú</th>
            <th field="nhanvien" width="150" align="center">Người lập phiếu</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; padding-top:4px; text-align:left">
    <span style="font-weight:bold; color:blue">Phiếu chi</span>
    Từ ngày: <input id="tungay" name="tungay" class="easyui-datebox" style="width: 110px;" value="<?php echo date('d/m/Y', strtotime('first day of this month')) ?>" />
    Đến ngày: <input id="denngay" name="denngay" class="easyui-datebox" style="width: 110px;" value="<?php echo date('d/m/Y') ?>"/>
    <input id="khachhang" name="khachhang" class="easyui-combobox" url="common/nhacungcap" style="width: 180px;" valueField="id" textField="name" prompt="Khách hàng" />
    <input id="taikhoan" name="taikhoan" class="easyui-combobox" url="common/taikhoan" style="width: 180px;" valueField="id" textField="name" prompt="Tài khoản" panelHeight="auto"/>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
          foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
    <button class="easyui-linkbutton" iconCls="icon-print" onclick="inphieu()">In Phiếu</button>
    <button class="easyui-linkbutton" iconCls="icon-excel" onclick="xuatfile()">Xuất Excel</button>
</div>

<div id="dlg" class="easyui-dialog" style="width: 670px; height: auto; padding: 10px;" closed="true" buttons="#dlg-buttons" modal="true" iconCls="icon-phieuchi">
    <form id="fm" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Ngày:</div>
                </td>
                <td>
                  <input id="ngaygio" name="ngaygio" class="easyui-datebox" style="width: 200px;height:28px;" required  />
                </td>
                <td>
                    <div>Nhà cung cấp:</div>
                </td>
                <td>
                    <input id="khach_hang"  name="khach_hang" class="easyui-combobox" url="common/nhacungcap" style="width: 200px;height:28px;" valueField="id" textField="name" required/>
                </td>
            </tr><tr>
                <td>
                    <div>Tài khoản:</div>
                </td>
                <td>
                    <input id="tai_khoan"  name="tai_khoan" class="easyui-combobox" url="common/taikhoan" style="width: 200px;height:28px;" valueField="id" textField="name"/>
                </td>
                <td>
                    <div>Số tiền:</div>
                </td>
                <td>
                    <input id="so_tien" name="so_tien" style="width:200px;height:28px; " class="easyui-numberbox" groupSeparator="," />
                </td>
           </tr><tr>
               <td>
                   <div>Số phiếu:</div>
               </td>
               <td>
                   <input id="name" name="name" style="width:200px; height:28px;" class="easyui-textbox" />
               </td>
               <td>
                   <div>Số hóa đơn:</div>
               </td>
               <td>
                   <input id="invoice"  name="invoice[]" class="easyui-combobox" style="width: 200px;height:28px;height:28px;" valueField="so_phieu" textField="soinv" multiple="true"/>
               </td>
           </tr><tr>
               <td>
                   <div>Nội dung:</div>
               </td>
               <td>
                   <input id="dien_giai" name="dien_giai" style="width:200px; height:60px;" class="easyui-textbox" multiline="true" />
               </td>
               <td>
                   <div>Ghi chú:</div>
               </td>
               <td>
                   <input id="ghi_chu" name="ghi_chu" style="width:200px; height:60px;"  class="easyui-textbox" multiline="true"  />
               </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dlg').dialog('close')">Quay lại
    </button>
</div>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>VDATA:: Phiếu Thu</title>
	<style>
		body {font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px;}
        .letter-head {width:100%; float:left; margin-bottom:15px;}
        .head-left {width:70%; float:left}
        .head-right {width:30%; text-align:right; float:left}
        .indam { font-weight:bold}
		.tieude { text-align:center; margin-bottom:20px; width:100%;}
		.thongtin {margin-bottom:15px; float:left; width:100%;  }
        .tin-left { width:50%; float:left}
        .tin-right {width:50%; float:left}
		.noidung {float:left; margin-bottom:15px; width:100%; }
		table.main {
			border-collapse: collapse; border:#666 1px solid;
			width: 100%;
		}
		table.main th, table.main td {
			text-align: left;
			padding: 3px; border: 1px solid #666;
		}
		table.main tr.title {background-color:#CCC}

		/*table.main tr:nth-child(even){background-color: #f2f2f2}*/

		.footer {float:left; margin-bottom:15px; width:100%}
		.chuky {float:left; margin-bottom:15px; width:100%}
		.noprint {float:left; text-align:center; width:100%}
		.linkbutton { height:30px;}
		@media print{
		   .noprint{display:none;}
		  @page { margin: 0; }
		  body { margin: 1.6cm; }
		}
    </style>

</head>
<body>

<div class="letter-head">
	<div class="head-left">
        <span class="indam">Công ty cổ phẩn truyền thông số VDATA</span> <br />
        Địa chỉ: <span class="indam">87 Lĩnh Nam - Hoàng Mai - Hà Nội</span> <br />
        Điện thoại: <span class="indam">(024) 7777 8666</span>
    </div>
    <div class="head-right">
    		Số phiếu: <span class="indam">PT-<?php echo $this->phieu['id'] ?></span><br />
    		Ngày: <span class="indam"><?php echo $this->phieu['ngay_gio'] ?></span>
      	<br>
        Người lập phiếu: <span class="indam"><?php echo $this->phieu['nhanvien']; ?></span>
    </div>

</div>

<div class="tieude">
	<h2>PHIẾU THU</h2>
</div>

<div class="thongtin">
    	<span class="indam">Tên khách hàng:</span> <?php echo $this->phieu['khachhang']; ?> <br />
      <span class="indam">Địa chỉ:</span> <?php echo $this->phieu['diachi']; ?> <br />
    	<span  class="indam">Số điện thoại:</span> <?php echo $this->phieu['dienthoai']; ?> <br />
</div>
<div class="noidung">
	<table class="main" >
		<tr class="title">
            <th>Mã đơn hàng</th>
            <th>Nội dung</th>
            <th>Số tiền</th>
            <th>Ghi chú</th>
      	</tr>
        <tr>
            	<td style="text-align: center;">PT-<?php echo $this->phieu['id'] ?></td>
                <td style="text-align: left;"><?php echo $this->phieu['dien_giai'] ?></td>
                <td style="text-align: right;"><?php echo number_format($this->phieu['so_tien']) ?></td>
                <td style="text-align: left;"><?php echo $this->phieu['ghi_chu'] ?></td>
		</tr>
		</table>

</div>
<div class="footer">
	<table class="main">
    	<tr>
        	<td rowspan="2" width="50%">
            	- Mọi khiếu nại về nhầm lẫn về phiếu thu phải được thông báo cho chúng tôi trong vòng 24h, liên hệ trực tiếp với nhân viên kế toán <br />
            </td>
        </tr>
        <tr>
        	<td style="text-align: right;">Tổng thanh toán</td>
            <td style="text-align: right;"><b><?php echo number_format($this->phieu['so_tien']) ?>
            </b></td>
        </tr>
    </table>
</div>
<div class="chuky" style="padding-bottom: 206px; border-bottom: 1px dotted black;">
	<table width="100%">
        <tr>
        	 <td style="text-align: center; width:50%"><b>Người lập phiếu</b></td>
             <td style="text-align: center; width:50%"><b>Khách hàng</b></td>
        </tr>
    </table>
</div>



<div class="noprint" style="padding-bottom: 20px;">
    <button class="linkbutton" onClick="window.print();">In ra</button>
    <button class="linkbutton" onClick="window.location.href = '<?php echo URL; ?>/phieuthu'">Quay lại</button>
</div>

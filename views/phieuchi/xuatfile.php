<?php
// print_r($this->jsonObj);
// exit();
	require_once ROOT_DIR.'/libs/phpexcel/PHPExcel.php'; 
	$sql = new Model();
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("VDATA")
							 ->setLastModifiedBy("VDATA")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
	$sheet = $objPHPExcel->getActiveSheet ();
	//formatting
	$center=array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
	$sheet->getColumnDimension('A')->setWidth(5);
	$sheet->getColumnDimension('B')->setWidth(25);
	$sheet->getColumnDimension('C')->setWidth(25);
	$sheet->getColumnDimension('D')->setWidth(40);
	$sheet->getColumnDimension('E')->setWidth(50);
	$sheet->getColumnDimension('F')->setWidth(15);
	$sheet->getColumnDimension('G')->setWidth(30);
	$sheet->getColumnDimension('H')->setWidth(30);
	$sheet->getStyle("E")->getAlignment()->applyFromArray($center);
	$sheet->setCellValue('A1', 'Danh sách phiếu thu');
	$sheet->mergeCells('A1:H1');
	$sheet->getStyle('A1')->getAlignment()->applyFromArray($center);	
    $sheet->setCellValue('A2', 'Công ty VDATA');
	$sheet->mergeCells('A2:H2');
	$sheet->getStyle('A2')->getAlignment()->applyFromArray($center);	
	$sheet->setCellValue('A3', 'Từ ngày: '.$this->tungay.' Đến ngày: '.$this->denngay );
	$sheet->mergeCells('A3:H3');
	$sheet->getStyle('A3')->getAlignment()->applyFromArray($center);	
    $sheet->setCellValue('A4', '');
	$sheet->mergeCells('A4:H4');

	$sheet->setCellValue('A5','STT');
	$sheet->setCellValue('B5','Ngày giờ');
	$sheet->setCellValue('C5','Tài khoản');
	$sheet->setCellValue('D5','Khách hàng');
	$sheet->setCellValue('E5','Nội dung');
	$sheet->setCellValue('F5','Số tiền');
	$sheet->setCellValue('G5','Ghi chú');	
	$sheet->setCellValue('H5','Người lập phiếu');
	
	$i=6;
 	foreach($this->jsonObj['rows'] as $row){
			$sheet->setCellValue('A' . $i, $i-5);
			$sheet->setCellValue('B' . $i, $row['ngaygio']);
			$sheet->setCellValue('C' . $i, $row['taikhoan']);
			$sheet->setCellValue('D' . $i, $row['khachhang']);
			$sheet->setCellValue('E' . $i, $row['dien_giai']);
			$sheet->setCellValue('F' . $i, $row['so_tien']);
			$sheet->setCellValue('G' . $i, $row['ghi_chu']);
			$sheet->setCellValue('H' . $i, $row['nhanvien']);
			$i++;
	}
	$sheet->getStyle("A5:H".$i)->applyFromArray(
		array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		)
	);


// Rename worksheet
$sheet->setTitle('Danh sách phiếu thu');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="danh_sach_phieu_thu('.date("d_m_Y").').xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
//header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_end_clean();  //xóa các định dạng html trước khi ghi file excel để loại trừ lỗi format or file extension not valid  khi mở file
$objWriter->save('php://output');
exit;
?>
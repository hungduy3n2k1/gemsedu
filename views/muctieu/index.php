<script type="text/javascript" src="js/muctieu.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="muctieu/json" toolbar="#toolbar" showFooter="true"
       pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true"
       nowrap="true" iconCls="icon-phanloai" data-options="border:false,fit:true" pageSize="30">
    <thead>
    <tr>
        <th field="name" width="200">Phân loại</th>
        <th field="ghi_chu" width="300" >Ghi chú</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 330px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
      <div style="margin-bottom:20px">
          <input id="name" name="name" label="Phân loại:" labelPosition="top" class="easyui-textbox" required style="width:100%;">
      </div>
      <div style="margin-bottom:20px">
          <input id="ghi_chu" name="ghi_chu" label="Ghi chú:" labelPosition="top" class="easyui-textbox" style="width:100%;">
      </div>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

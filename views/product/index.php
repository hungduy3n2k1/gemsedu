<script type="text/javascript" src="js/product.js"></script>
<table
    id="dg"
    style="width: 100%;"
    class="easyui-datagrid"
    url="product/json"
    toolbar="#toolbar"
    pagination="true"
    idField="id"
    rownumbers="true"
    fitColumns="false"
    singleSelect="true"
    nowrap="true"
    iconCls="icon-sokho"
    data-options="border:false,fit:true"
    pageSize="30"
>
    <thead>
        <tr>
            <th field="name" width="150" sortable="true">Gói dịch vụ</th>
            <th field="loai" width="100" sortable="true">Loại dịch vụ</th>
            <th field="ghi_chu" width="400">Mô tả</th>
            <th field="don_vi_tinh" width="100" align="center">Đơn vị tính</th>
            <th field="don_gia" width="100" align="right" formatter="CurrencyFormatted">Đơn giá</th>
            <th field="gia_han" width="100" align="right" formatter="format_giahan">Gia hạn</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top: 5px; padding-bottom: 5px;">
    <span style="color: blue; font-weight: bold; margin-right: 20px;">SẢN PHẨM VÀ CÁC GÓI DỊCH VỤ</span>
    <!-- <input class="easyui-combobox" name="tinhtrang" id="tinhtrang" url="product/tinhtrang" valueField="id" textField="text"
        prompt="Tình trạng" value="1"> -->
    <!-- <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button> &nbsp;&nbsp;&nbsp;&nbsp; -->
    <?php
        foreach ($this->funs as $item) echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> '; ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Gói dịch vụ</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px; height: 28px; " />
                </td>
            </tr>
            <tr>
                <td>Mô tả:</td>
                <td>
                    <input class="easyui-textbox" multiline="true" style="width: 200px; height: 100px;" id="ghi_chu" name="ghi_chu" />
                </td>
            </tr>
            <tr>
                <td>Loại dịch vụ:</td>
                <td>
                    <input id="phan_loai" name="phan_loai" class="easyui-combobox" style="width: 200px; height: 25px;"
                    required url="common/loaisdichvu" valueField="id" textField="name" />
                </td>
            </tr>
            <tr>
                <td>Đơn giá:</td>
                <td>
                    <input id="don_gia" name="don_gia" class="easyui-numberbox" style="width: 200px; height: 28px;" groupSeparator="," />
                </td>
            </tr>
            <tr>
                <td>Đơn vị tính:</td>
                <td>
                    <input id="don_vi_tinh" name="don_vi_tinh" style="width: 200px; height: 25px;" class="easyui-textbox" />
                </td>
            </tr>
            <tr>
                <td>Giá vốn:</td>
                <td>
                    <input id="gia_von" name="gia_von"class="easyui-numberbox" style="width: 200px; height: 28px;" groupSeparator="," />
                </td>
            </tr>
            <tr>
                <td>Tình trạng:</td>
                <td>
                    <select class="easyui-combobox" name="tinh_trang" id="tinh_trang" style="width: 200px; height: 25px;">
                        <option value="1">Bật</option>
                        <option value="2">Tắt</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Gia hạn:</td>
                <td>
                    <select class="easyui-combobox" name="gia_han" id="gia_han" style="width: 200px; height: 25px;">
                        <option value="1">Có</option>
                        <option value="0">Không</option>
                    </select>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<?php
	require_once ROOT_DIR.'/libs/phpexcel/PHPExcel.php'; 
	$sql = new Model();
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("VDATA")
							 ->setLastModifiedBy("VDATA")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
	$sheet = $objPHPExcel->getActiveSheet ();
	//formatting
	$center=array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
	$sheet->getColumnDimension('A')->setWidth(7);
	$sheet->getColumnDimension('B')->setWidth(15);
	$sheet->getColumnDimension('C')->setWidth(50);
	$sheet->getColumnDimension('D')->setWidth(25);
	$sheet->getColumnDimension('E')->setWidth(10);
	$sheet->getColumnDimension('F')->setWidth(15);
	$sheet->getColumnDimension('G')->setWidth(15);
	$sheet->getColumnDimension('H')->setWidth(15);
	$sheet->getStyle("E")->getAlignment()->applyFromArray($center);
	$sheet->setCellValue('A1', 'DANH MỤC HÀNG HÓA');
	$sheet->mergeCells('A1:H1');
	$sheet->getStyle('A1')->getAlignment()->applyFromArray($center);	
    $sheet->setCellValue('A2', 'Tên kho: OTC Dược Thái Bình');
	$sheet->mergeCells('A2:H2');
	$sheet->getStyle('A2')->getAlignment()->applyFromArray($center);	
	$sheet->setCellValue('A3', 'Kiểm kê ngày:...............................................' );
	$sheet->mergeCells('A3:H3');
	$sheet->getStyle('A3')->getAlignment()->applyFromArray($center);	
    $sheet->setCellValue('A4', '');
	$sheet->mergeCells('A4:H4');

	$sheet->setCellValue('A5','STT');
	$sheet->setCellValue('B5','Mã SP');
	$sheet->setCellValue('C5','Tên sản phẩm');
	$sheet->setCellValue('D5','Quy cách');
	$sheet->setCellValue('E5','Mã ĐV');
	$sheet->setCellValue('F5','Đơn vị tính');
	$sheet->setCellValue('G5','Đơn giá');	
	$sheet->setCellValue('H5','Tồn thực tế');
	
	$donvi=$this->donvi;
	$i=6;
 	foreach($this->hanghoa as $row){
		$quycach=$row['donvitinh'];
		$tyledongia=1;
		$tyle=1;
		for ($n=1; $n<4; $n++) {
			if (($row['don_vi_'.$n]>0) && ($row['ty_le_'.$n]>0)) {
				if ($row['don_vi_nhap']==$row['don_vi_'.$n])
					$tyledongia=$row['ty_le_'.$n];
				$quycach=$donvi[$row['don_vi_'.$n]].' = '.$row['ty_le_'.$n]/$tyle.' '.$quycach;
				$tyle=$row['ty_le_'.$n];
			}
			else
				break;
		}
		$dongia= $row['gia_ban_buon']/$tyledongia;
			$sheet->setCellValue('A' . $i, $i-5);
			$sheet->setCellValue('B' . $i, $row['id']);
			$sheet->setCellValue('C' . $i, $row['ten_hang']);
			$sheet->setCellValue('D' . $i, $quycach);
			$sheet->setCellValue('E' . $i, $row['don_vi_tinh']);
			$sheet->setCellValue('F' . $i, $row['donvitinh']);
			$sheet->setCellValue('G' . $i, $dongia);
			$sheet->setCellValue('H' . $i, '');
			$i++;
	}
	$sheet->getStyle("A5:H".$i)->applyFromArray(
		array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		)
	);


// Rename worksheet
$sheet->setTitle('Danh muc hang hoa');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="danh_muc_hang_hoa('.date("d_m_Y").').xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
//header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_end_clean();  //xóa các định dạng html trước khi ghi file excel để loại trừ lỗi format or file extension not valid  khi mở file
$objWriter->save('php://output');
exit;
?>
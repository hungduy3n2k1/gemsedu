<script>
    function timkiem() {
        var tungay = $("#tungay").datebox("getValue");
        var denngay = $("#denngay").datebox("getValue");
        var tukhoa = $("#tukhoa").textbox("getValue");
        if (tungay.length > 0 || denngay.length > 0 || tukhoa.length > 0) {
            $("#dg").datagrid("options").url = baseUrl + "/thanhtoanonline/json?tungay=" + tungay + "&denngay=" + denngay +
                "&tukhoa=" + tukhoa ;
        } else $("#dg").datagrid("options").url = baseUrl + "/thanhtoanonline/json";
        $("#dg").datagrid("reload");
    }
</script>
<table id="dg" style="width: 100%;" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="true" pageSize="30" showFooter="true"
       singleSelect="true" nowrap="true" iconCls="icon-phieuthu" data-options="border:false,fit:true" url="thanhtoanonline/json" class="easyui-datagrid" >
    </thead>
    <thead>
    <tr>
        <th field="ngaygio" width="150" sortable="true">Ngày</th>
        <th field="don_hang" width="100" sortable="true">Đơn hàng</th>
        <th field="noi_dung" width="300" >Nội dung</th>
        <th field="so_tien" width="100" align="right" formatter="FormatToCurrency">Số tiền</th>
    </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; padding-top:4px; text-align:left">
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" style="width: 180px;height:28px;" prompt="Từ khóa" panelHeight="auto"/>
    Từ ngày: <input id="tungay" name="tungay" class="easyui-datebox" style="width: 110px;height:28px;" value="<?php echo date('d/m/Y', strtotime('first day of this month')) ?>" />
    Đến ngày: <input id="denngay" name="denngay" class="easyui-datebox" style="width: 110px;height:28px;" value="<?php echo date('d/m/Y') ?>"/>
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    <?php
    foreach ($this->funs as $item)
        echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

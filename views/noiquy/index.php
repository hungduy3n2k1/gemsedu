<script type="text/javascript" src="js/noiquy.js"></script>

<table id="dg" style="width: 100%;" class="easyui-datagrid" url="noiquy/json" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-noiquy" data-options="border:false,fit:true"
    pageSize="30">
    <thead>
        <tr>
            <th field="name" width="auto" sortable="true">Quy định</th>
            <th field="thuong" width="100" align="right" formatter="CurrencyFormatted">Thưởng</th>
            <th field="phat" width="100" align="right" formatter="CurrencyFormatted">Phạt</th>
              <th field="tinh_trang" width="100" align="right" formatter="apdung">Áp dụng</th>
            <th field="ghi_chu" width="auto">Ghi chú</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <span style="color:blue; font-weight:bold; margin-right:20px">NỘI QUY CÔNG TY</span>
    <button class="easyui-linkbutton" iconCls="icon-excel" plain="false" onclick="xuatfile()">Xuất excel</button>
    <button class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="add()">Thêm mới</button>
    <button class="easyui-linkbutton" iconCls="icon-edit" plain="false" onclick="edit()">Sửa thông tin</button>
    <button class="easyui-linkbutton" iconCls="icon-no" plain="false" onclick="del()">Xóa</button>
</div>

<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Nội quy:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px;height:25px;" />
                </td>
            </tr>
            <tr>
                <td>Thưởng/phạt:</td>
                <td>
                    <input id="loai" name="loai" class="easyui-combobox" style="width:200px; height:25px" required url="common/thuongphat" valueField="id" textField="name" />
                </td>
            </tr>            <tr>
                <td>Số tiền:</td>
                <td>
                    <input id="so_tien" name="so_tien" class="easyui-validatebox" style="width: 197px;height:22px;border: 1px solid #6B9CDE; border-radius: 5px;" onkeyup="javascript:this.value=Comma(this.value);" />
                </td>
            </tr>
            <tr>
                <td>Tối đa:</td>
                <td>
                    <input id="toi_da" name="toi_da" class="easyui-validatebox" style="width: 197px;height:22px;border: 1px solid #6B9CDE; border-radius: 5px;" />
                </td>
            </tr>
            <tr>
                <td>Ghi chú:</td>
                <td>
                    <input class="easyui-textbox" multiline="true" style="width:200px; height:100px" id="ghi_chu" name="ghi_chu">
                </td>
            </tr>
            <tr>
                <td>Tình trạng:</td>
                <td>
                    <select class="easyui-combobox" name="tinh_trang" id="tinh_trang" style="width: 200px;height:25px;">
                    <option value="1">Bật</option>
                    <option value="2">Tắt</option>
    			          </select>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

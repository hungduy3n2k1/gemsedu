<script type="text/javascript" src="js/dichvu.js"></script>

<table
    id="dg"
    style="width: 100%;"
    class="easyui-datagrid"
    url="dichvu/json"
    toolbar="#toolbar"
    pagination="true"
    idField="id"
    rownumbers="true"
    fitColumns="false"
    singleSelect="true"
    nowrap="false"
    iconCls="icon-sokho"
    data-options="border:false,fit:true"
    pageSize="50"
>
    <thead>
        <tr>
            <th field="loai" width="150" sortable="true">Loại dịch vụ</th>
            <th field="name" width="200" sortable="true">Tên sản phẩm</th>
            <th field="ghi_chu" width="400">Mô tả</th>
            <th field="donvitinh" width="100" align="center">Đơn vị tính</th>
            <th field="don_gia" width="100" align="right" formatter="CurrencyFormatted">Đơn giá</th>
            <th field="thue_suat_vat" width="100" align="right">Thuế suất VAT</th>
            <th field="khoahoc" width="100" align="right" >Khóa học</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top: 5px; padding-bottom: 5px;">
    <input class="easyui-combobox" name="loai" id="loai" url="common/loaidichvu" valueField="id" textField="name"
        prompt="Loại dịch vụ">
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Từ khóa" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button> &nbsp;&nbsp;&nbsp;&nbsp;
    <?php
        foreach ($this->funs as $item) echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> '; ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 700px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
      <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
          <tr>
              <td>Dịch vụ:</td>
              <td>
                  <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px; height: 28px; " />
              </td>
              <td>Loại dịch vụ:</td>
              <td>
                  <input id="phan_loai" name="phan_loai" class="easyui-combobox" style="width: 200px; height: 28px;"
                  required url="common/loaidichvu" valueField="id" textField="name" />
              </td>
          </tr><tr>
              <td>Đơn giá:</td>
              <td>
                  <input id="don_gia" name="don_gia" class="easyui-numberbox" style="width: 200px; height: 28px;" groupSeparator="," />
              </td>
              <td>Đơn vị tính:</td>
              <td>
                  <input id="don_vi_tinh" name="don_vi_tinh" style="width: 200px; height: 28px;" class="easyui-combobox" readonly
                  required url="common/donvitinh" valueField="id" textField="name" />
              </td>
          </tr><tr>
              <td>Thuế suất VAT %:</td>
              <td>
                  <input id="thue_suat_vat" name="thue_suat_vat"class="easyui-numberbox" style="width: 200px; height: 28px;" groupSeparator="," />
              </td>
              <td>Giá vốn:</td>
              <td>
                  <input id="gia_von" name="gia_von"class="easyui-numberbox" style="width: 200px; height: 28px;" groupSeparator="," />
              </td>
          </tr><tr>
              <td>Gia hạn:</td>
              <td>
                  <select class="easyui-combobox" name="gia_han" id="gia_han" style="width: 200px; height: 28px;" readonly>
                      <option value="1">Có</option>
                      <option value="0">Không</option>
                  </select>
              </td>
              <td>Tình trạng:</td>
              <td>
                  <select class="easyui-combobox" name="tinh_trang" id="tinh_trang" style="width: 200px; height: 28px;">
                      <option value="1">Bật</option>
                      <option value="2">Tắt</option>
                  </select>
              </td>
          </tr>
          <tr>
              <td style="vertical-align: top">Khóa học:</td>
              <td style="vertical-align: top">
                  <input id="khoa_hoc" name="khoa_hoc" style="width: 200px; height: 28px;" class="easyui-combobox"
                         required url="common/khoahoc" valueField="id" textField="name" />
              </td>
              <td style="vertical-align: top">Mô tả:</td>
              <td>
                  <input class="easyui-textbox" multiline="true" style="width: 200px; height: 80px;" id="ghi_chu" name="ghi_chu" />
              </td>
          </tr>
      </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<script type="text/javascript" src="js/loaidichvu.js"></script>
<table
    id="dg"
    style="width: 100%;"
    class="easyui-datagrid"
    url="loaidichvu/json"
    toolbar="#toolbar"
    pagination="true"
    idField="id"
    rownumbers="true"
    fitColumns="false"
    singleSelect="true"
    nowrap="true"
    iconCls="icon-loaidichvu"
    data-options="border:false,fit:true"
    pageSize="30"
>
    <thead>
        <tr>
            <th field="id" width="60">ID</th>
            <th field="name" width="120">Mã học viên</th>
            <th field="ghi_chu" width="480">Ghi chú</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; padding-top:4px;">
    <?php
      foreach ($this->funs as $item)
          echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên dịch vụ:</td>
                <td>
                    <input id="name" name="name" class="easyui-validatebox" required="required" style="width: 197px; height: 22px; border: 1px solid #6b9cde; border-radius: 5px;" />
                </td>
            </tr>
            <tr>
                <td>Mô tả:</td>
                <td>
                    <input class="easyui-textbox" multiline="true" style="width: 200px; height: 100px;" id="ghi_chu" name="ghi_chu" />
                </td>
            </tr>
            <tr>
                <td>Tình trạng:</td>
                <td>
                    <select class="easyui-combobox" name="tinh_trang" id="tinh_trang" style="width: 200px; height: 25px;">
                        <option value="1">Bật</option>
                        <option value="2">Tắt</option>
                    </select>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

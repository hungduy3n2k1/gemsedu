<script type="text/javascript" src="js/resource.js"></script>
<table id="dg" style="width: 100%;" url="resource/json" class="easyui-datagrid" toolbar="#toolbar" pagination="true" idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true" data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
    <thead>
        <tr>
            <th field="name" width="200">Tài nguyên</th>
            <th field="sohuu" width="120">Chủ sở hữu</th>
            <th field="phanloai" width="100">Phân loại</th>
            <th field="doitac" width="100">Nhà cung cấp</th>
            <th field="nguoitao" width="180">Người tạo</th>
            <th field="link" width="400">Link đăng nhập</th>
            <th field="ghi_chu" width="250">Ghi chú</th>

        </tr>
    </thead>
</table>
<div id="toolbar" style="height:40px; padding-top:4px;">
    <!-- <span style="color:blue; font-weight:bold; margin-right:20px">DANH SÁCH TÀI NGUYÊN</span> -->
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Từ khóa" />
    <input class="easyui-combobox" name="phanloai" id="phanloai" valueField="id" textField="name" url="common/phanloai"
      prompt="Phân loại">
    <input class="easyui-combobox" name="nhacungcap" id="nhacungcap" valueField="id" textField="name" url="common/nhacungcap"
      prompt="Nhà cung cấp" >
    <input class="easyui-combobox" name="sohuu" id="sohuu" valueField="id" textField="name" url="common/khachhang?loai=1"
      prompt="Chủ sở hữu" >
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>
    &nbsp;&nbsp;&nbsp;
    <?php
        foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
   <!--  <button class="easyui-linkbutton" iconCls="icon-customer" plain="false" onclick="chiase()">Chia sẻ</button> -->
</div>

<div id="dlg" class="easyui-dialog" style="width: 700px; height: 250px; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên tài nguyên:</td>
                <td>
                    <input id="name" name="name" class="easyui-textbox" required="required" style="width: 200px;height:25px;" />
                </td>
                <td>Chủ sở hữu:</td>
                <td>
                    <input id="chu_so_huu" name="chu_so_huu" class="easyui-combobox" style="width:200px; height:25px" url="common/khachhang?loai=1" valueField="id" textField="name" />
                </td>
            </tr><tr>
                <td>Nhà cung cấp:</td>
                <td>
                    <input id="nha_cung_cap" name="nha_cung_cap" class="easyui-combobox" style="width:200px; height:25px" url="common/nhacungcap" valueField="id" textField="name" />
                </td>
                <td>Link quản trị:</td>
                <td>
                    <input id="link" name="link" class="easyui-textbox" style="width: 200px;height:25px;" />
                </td>
            </tr><tr id="abc">
                <td>Tên đăng nhập:</td>
                <td>
                    <input id="username" name="username" class="easyui-textbox" style="width:200px; height:25px" />
                </td>
                <td>Mật khẩu:</td>
                <td>
                    <input id="passwd" name="passwd" class="easyui-passwordbox" style="width: 200px;height:25px;" />
                </td>
            </tr><tr>
                <td>Ghi chú:</td>
                <td>
                    <input id="ghi_chu" name="ghi_chu" class="easyui-textbox" style="width: 200px;height:25px;" />
                </td>
                <td>Phân loại:</td>
                <td>
                    <input id="phan_loai" name="phan_loai" class="easyui-combobox" style="width:200px; height:25px" url="common/phanloai" valueField="id" textField="name" />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-chiase" class="easyui-dialog" style="width: 350px; height: auto; padding: 20px;" closable="false" closed="true"
buttons="#dlg-buttons-1" modal="true">
    <form id="fm-chiase" method="post" >
      <div style="margin-bottom:20px">
          Chia sẻ cho:
          <input class="easyui-combobox" name="nhanvien[]" style="width:100%; height:60px" url="common/nhanvien"
          valueField="id" textField="name"
          multiple="true" multiline="true" editable="false" id="nhanvien" >
      </div>
      <div style="margin-bottom:20px">
          Người tạo:
          <input class="easyui-combobox" name="nguoi_tao" style="width:100%; " url="common/nhanvien"
          valueField="id" textField="name" editable="false" id="nguoi_tao">
      </div>
    </form>
</div>
<div id="dlg-buttons-1">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="capnhat()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-chiase').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-mk" class="easyui-dialog" style="width: 350px; height: auto; padding: 10px;" closable="false"
closed="true" buttons="#dlg-buttons-2" modal="true">
    <form id="fm-mk" method="post" >
      <div style="margin-bottom:20px">
          <input class="easyui-textbox" prompt="Tên đăng nhập" iconWidth="28" style="width:100%;height:34px;padding:10px;"
          id="ten_dang_nhap" name="ten_dang_nhap">
      </div>
      <div style="margin-bottom:20px">
          <input class="easyui-passwordbox" prompt="Mật khẩu hiện tại" iconWidth="28" style="width:100%;height:34px;padding:10px"
          id="mat_khau" name="mat_khau">
      </div>
      <div style="margin-bottom:20px">
          <input class="easyui-passwordbox" prompt="Mật khẩu mới" iconWidth="28" style="width:100%;height:34px;padding:10px"
          id="mat_khau_moi" name="mat_khau_moi">
      </div>
    </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="updatepass()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-mk').dialog('close')">Bỏ qua</button>
</div>

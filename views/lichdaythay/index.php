<script type="text/javascript" src="js/lichdaythay.js"></script>

<table id="dg" style="width: 100%;" class="easyui-datagrid" url="lichdaythay/json" toolbar="#toolbar"

       pagination="true"

       idField="id" rownumbers="true" fitColumns="true" nowrap="false" iconCls="icon-phieuchi"

       data-options="border:false,fit:true" pageSize="50" showFooter="false">

    <thead>

    <tr>

        <th field="ck" checkbox="true"></th>

        <th field="ngay_gio" width="100">Ngày đề nghị</th>

        <th field="lophoc" width="120">Lớp</th>

        <th field="ngay" width="100">Ngày</th>

        <th field="gio" width="100">Giờ</th>

        <th field="hocvien" width="160">Học viên</th>

        <th field="giaovien" width="160">Giáo viên</th>

        <th field="gvdaythay" width="160">Giáo viên dạy thay</th>

        <th field="tinh_trang" width="120" formatter="tinhtrang">Tình trạng</th>

    </tr>

    </thead>

</table>

<div id="toolbar" style="padding-top:6px; text-align:left ">

    <!-- <button class="easyui-linkbutton" iconCls="icon-undo" plain="false" onclick="pre()">Tháng trước</button>

    <input class="easyui-timespinner" name="giohoc" id="giohoc" style="width: 80px;height:28px;" prompt="Giờ học">

    <input class="easyui-textbox" name="ngay1" id="ngay1" style="width: 30px">

    <input class="easyui-textbox" name="thang" id="thang" value="<?= date('m') ?>" style="width: 30px">

    <input class="easyui-textbox" name="nam" id="nam" value="<?= date('Y') ?>" style="width: 60px">

    <input class="easyui-combobox" name="loai" id="loai" style="width: 150px;height:28px;" prompt="Phân loại"

           panelHeight="auto"

           data-options="data:[{'id':'1','name':'Online'},{'id':'2','name':'Offline'},{'id':'3','name':'Demo'}]" valueField="id"

           textField="name"> -->

    <input class="easyui-combobox" name="giaovien" id="giaovien" style="width: 200px;height:28px;"

           url="common/giaovien" valueField="id" textField="name" prompt="Giáo viên">

    <!-- <input class="easyui-combobox" name="phonghoc" id="phonghoc" style="width: 150px;height:28px;"

           url="common/phonghoc" valueField="id" textField="name" prompt="Phòng học"> -->

    <input class="easyui-textbox" name="tenlop" id="tenlop" style="width: 150px;height:28px;" prompt="Tên lớp học">

    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>

    <!-- <button class="easyui-linkbutton" iconCls="icon-redo" plain="false" onclick="next()">Tháng sau</button> -->

       <button class="easyui-linkbutton" iconCls="icon-ok" onclick="duyet()">Duyệt</button>

</div>


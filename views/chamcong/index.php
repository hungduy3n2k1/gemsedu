<script type="text/javascript" src="js/chamcong.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="chamcong/json"
    toolbar="#toolbar" fitColumns="true" singleSelect="true" nowrap="true" showFooter="true"
    data-options="border:false,fit:true,iconCls:'icon-nhaphang'"  >
    <thead >
        <tr>
            <th field="1" width="70">Thứ 2</th>
            <th field="2" width="70">Thứ 3</th>
            <th field="3" width="70">Thứ 4</th>
            <th field="4" width="70">Thứ 5</th>
            <th field="5" width="70">Thứ 6</th>
            <th field="6" width="70">Thứ 7</th>
            <th field="7" width="70">CN</th>
        </tr>
    </thead>
</table>

<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input class="easyui-combobox" name="nhanvien" id="nhanvien" valueField="id" textField="name"
    url="common/nhanvien" prompt="Nhân viên" style="width:160px" value="<?=$_SESSION['user']['nhan_vien'] ?>"
    >
    <input class="easyui-combobox" name="thang" id="thang" valueField="id" textField="name"
    url="common/thang" style="width:120px" value="<?=date('m')?>" >
    <input class="easyui-combobox" name="nam" id="nam" valueField="id" textField="name"
    url="common/nam" style="width:120px" value="<?=date('Y')?>" >
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="search()">Tìm kiếm</button>
    <?php
           foreach ($this->funs as $item)
               echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
    <p></p>
    Nghỉ phép: <input id="phep" class="easyui-textbox" style="width:60px; text-align:center" value="<?=(($this->phep['phepsang']+$this->phep['phepchieu'])/2)?>">
    Nghỉ không lương: <input id="khongluong" class="easyui-textbox" style="width:60px; text-align:center" value="<?=(($this->phep['khongsang']+$this->phep['khongchieu'])/2)?>">
    Nghỉ không đăng ký: <input id="khongbao" class="easyui-textbox" style="width:60px; text-align:center" value="<?=(($this->phep['phatsang']+$this->phep['phatchieu'])/2)?>">
    &nbsp&nbsp&nbsp&nbsp&nbsp
    Phép lũy kế: <input id="tongphep" class="easyui-textbox" style="width:60px; text-align:center" value="<?=$this->phep['phep_luy_ke']?>">
    Phép đã nghỉ: <input id="danghi" class="easyui-textbox" style="width:60px; text-align:center" value="<?=(($this->phep['phepnamsang']+$this->phep['phepnamchieu'])/2)?>">
    Phép còn: <input id="conlai" class="easyui-textbox" style="width:60px; text-align:center" value="<?=($this->phep['phep_luy_ke']-($this->phep['phepnamsang']+$this->phep['phepnamchieu'])/2)?>">
    &nbsp&nbsp&nbsp&nbsp&nbsp
    Quỹ nghỉ bù: <input id="nghibu" class="easyui-textbox" style="width:60px; text-align:center" value="<?=$this->phep['phep_nam']?>">
    Đã nghỉ: <input id="danghi1" class="easyui-textbox" style="width:60px; text-align:center" value="<?=(($this->phep['busang']+$this->phep['buchieu'])/2)?>">
    Còn lại: <input id="conlai1" class="easyui-textbox" style="width:60px; text-align:center" value="<?=($this->phep['phep_nam']-($this->phep['busang']+$this->phep['buchieu'])/2)?>">
</div>

<div id="dlg" class="easyui-dialog" style="padding:10px; width:340px;" data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm" method="post" action="chamcong/chamcong">
    <table>
        <tr>
            <td>Ngày: </td>
            <td>
                <input class="easyui-datebox" prompt="Ngày" style="width:200px" name="ngay" id="ngay" required>
                <input type="hidden" name="idnhanvien" id="idnhanvien">
            </td>
        </tr><tr>
            <td>Giờ vào: </td>
            <td>
              <input id="giovao" name="giovao" class="easyui-timespinner" style="width:200px">
            </td>
        </tr><tr>
            <td>Giờ ra: </td>
            <td>
              <input id="giora" name="giora" class="easyui-timespinner" style="width:200px">
            </td>
        </tr><tr>
            <td>Công sáng: </td>
            <td>
                <input id="sang" name="sang" class="easyui-combobox" style="width:200px" prompt="Chấm công sáng"
                       url="chamcong/loaiphep" valueField="id" textField="name" />
            </td>
        </tr><tr>
            <td>Công chiều: </td>
            <td>
                <input id="chieu" name="chieu" class="easyui-combobox" style="width:200px" prompt="Chấm công chiều"
                       url="chamcong/loaiphep" valueField="id" textField="name" />
            </td>
        </tr><tr>
            <td>Ghi chú: </td>
            <td>
              <input id="lydo" name="lydo" class="easyui-textbox" prompt="Lý do" style="width:200px; height:60px;" multiline="true" >
            </td>
        </tr><tr>
            <td>Áp dụng: </td>
            <td>
                <input id="apdung" name="apdung" class="easyui-combobox" style="width:200px"
                valueField="id" textField="name" data-options="data:[{'id':'1','name':'cá nhân'},{'id':'2','name':'tất cả'}]" />
            </td>
        </tr>
    </table>
    </form>
    <div class="dialog-button" style="text-align:center">
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="save()">OK</a>
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg').dialog('close')">Bỏ qua</a>
    </div>
</div>

<div id="dlg-baonghi" class="easyui-dialog" style="padding:10px; width:340px;" data-options="inline:true,modal:true,closed:true,closable:false">
    <form id="fm-baonghi" method="post" action="chamcong/baonghi">
    <table>
        <tr>
            <td>Ngày: </td>
            <td>
                <input class="easyui-datebox" prompt="Ngày báo nghỉ" style="width:200px" name="ngaynghi" id="ngaynghi" required>
            </td>
        </tr><tr>
            <td>Sáng: </td>
            <td>
                <input id="nghisang" name="nghisang" class="easyui-combobox" style="width:200px" prompt="Hình thức nghỉ"
                       url="chamcong/loainghi" valueField="id" textField="name" />
            </td>
        </tr><tr>
            <td>Chiều: </td>
            <td>
                <input id="nghichieu" name="nghichieu" class="easyui-combobox" style="width:200px" prompt="Hình thức nghỉ"
                       url="chamcong/loainghi" valueField="id" textField="name" />
            </td>
        </tr><tr>
            <td>Ghi chú: </td>
            <td>
              <input id="noidung" name="noidung" class="easyui-textbox" prompt="Lý do" style="width:200px; height:60px;" multiline="true" >
            </td>
        </tr>
    </table>
    </form>
    <div class="dialog-button" style="text-align:center">
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="savenghi()">OK</a>
      <a href="javascript:void(0)" class="easyui-linkbutton" style="width:45%;height:35px" onclick="$('#dlg-baonghi').dialog('close')">Bỏ qua</a>
    </div>
</div>

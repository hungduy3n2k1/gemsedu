<script type="text/javascript" src="js/nhanvien.js"></script>
<table id="dg" style="width: 100%;" url="nhanvien/json" class="easyui-datagrid" toolbar="#toolbar"
    pagination="true" idField="id" rownumbers="true" fitColumns="true" singleSelect="true"
    nowrap="false" data-options="border:false,fit:true,iconCls:'icon-nhaphang'" pageSize="30">
    <thead>
        <tr>
            <th field="name" width="150">Họ tên</th>
            <th field="gioi_tinh" width="100" formatter="gioitinh">Giới tính</th>
            <th field="phongban" width="150">Phòng ban</th>
            <th field="email" width="250">Email</th>
            <th field="dien_thoai" width="120">Điện thoại</th>
            <th field="facebook" width="250">Facebook</th>
            <th field="zalo" width="120">Zalo</th>
            <th field="vanphong" width="120">Văn phòng</th>
            <th field="calv" width="120">Ca làm việc</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <span style="color:blue; font-weight:bold; margin-right:20px">HỒ SƠ NHÂN SỰ</span>
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Tìm kiếm" /> &nbsp;&nbsp;&nbsp;
    <!-- <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button> -->
    <?php
       foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width:1000px; padding:10px" closed="true" buttons="#dlg-buttons" modal="true" data-options="resizable:true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <fieldset><legend>Thông tin cá nhân:</legend>
                <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                    <tr>
                        <td>Họ tên:</td>
                        <td>
                            <input id="name" name="name" class="easyui-textbox" style="width: 200px;height:25px;" required />
                        </td>
                        <td>Giới tính:</td>
                        <td>
                            <input class="easyui-combobox" name="gioi_tinh" id="gioi_tinh" required style="width: 200px;height:25px;"
                              url="common/gioitinh" valueField="id" textField="name" >
                        </td>
                        <td>Ngày sinh:</td>
                        <td>
                            <input id="ngaysinh" name="ngaysinh" class="easyui-datebox" style="width: 200px;height:25px;" />
                        </td>
                    </tr><tr>
                        <td>Địa chỉ:</td>
                        <td>
                            <input name="dia_chi" id="dia_chi" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Quê quán:</td>
                        <td>
                          <input class="easyui-combobox" name="que_quan" id="que_quan" style="width: 200px;height:25px;"
                            url="common/thanhpho" valueField="id" textField="name" >
                        </td>
                        <td>T/t hôn nhân:</td>
                        <td>
                          <input class="easyui-combobox" name="hon_nhan" id="hon_nhan" required style="width: 200px;height:25px;"
                            url="common/honnhan" valueField="id" textField="name" >
                        </td>
                    </tr><tr>
                        <td>Số CMND:</td>
                        <td>
                            <input id="cmnd" name="cmnd" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Ngày cấp:</td>
                        <td>
                            <input id="ngaycap" name="ngaycap" class="easyui-datebox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Nơi cấp:</td>
                        <td>
                            <input id="noi_cap" name="noi_cap" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                    </tr><tr>
                        <td>Thường trú:</td>
                        <td>
                            <input id="thuong_tru" name="thuong_tru" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Điện thoại:</td>
                        <td>
                            <input id="dien_thoai" name="dien_thoai" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Email cá nhân:</td>
                        <td>
                            <input id="email_canhan" name="email_canhan" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                    </tr><tr>
                        <td>Zalo:</td>
                        <td>
                            <input id="zalo" name="zalo" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Facebook:</td>
                        <td>
                            <input id="facebook" name="facebook" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Trình độ:</td>
                        <td>
                          <input class="easyui-combobox" name="trinh_do" id="trinh_do" required style="width: 200px;height:25px;"
                            url="common/trinhdo" valueField="id" textField="name" >
                        </td>
                    </tr><tr>
                        <td>Trường đào tạo:</td>
                        <td>
                            <input id="truong_hoc" name="truong_hoc" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Mã số thuế:</td>
                        <td>
                            <input id="ma_so_thue" name="ma_so_thue" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                        <td>Mã bảo hiểm:</td>
                        <td>
                            <input name="so_bao_hiem" id="so_bao_hiem" class="easyui-textbox" style="width: 200px;height:25px;" />
                        </td>
                    </tr>
                </table>
        </fieldset>
        <br>
        <fieldset><legend>Thông tin công việc:</legend>
            <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
                <tr>
                    <td>Ngày bắt đầu:</td>
                    <td>
                        <input id="ngaydilam" name="ngaydilam" class="easyui-datebox" required style="width: 200px;height:25px;" />
                    </td>
                    <td>Loại hợp đồng:</td>
                    <td>
                        <input class="easyui-combobox" name="tinh_trang" id="tinh_trang" required style="width: 200px;height:25px;"
                          url="common/hopdonglaodong" valueField="id" textField="text" >
                    </td>
                    <td>Ngày kết thúc:</td>
                    <td>
                        <input id="ngayketthuc" name="ngayketthuc" class="easyui-datebox" style="width: 200px;height:25px;" />
                    </td>
                </tr><tr>
                    <td>Lương cơ bản:</td>
                    <td>
                          <input id="luong" name="luong" class="easyui-numberbox" style="width: 200px;height:25px;" groupSeparator=',' />
                    </td>
                    <td>Phụ cấp ăn ca:</td>
                    <td>
                          <input id="an_ca" name="an_ca" class="easyui-numberbox" style="width: 200px;height:25px;" groupSeparator=',' />
                    </td>
                    <td>Phụ cấp gửi xe:</td>
                    <td>
                          <input id="gui_xe" name="gui_xe" class="easyui-numberbox" style="width: 200px;height:25px;" groupSeparator=',' />
                    </td>
                </tr><tr>
                    <td>Số tài khoản:</td>
                    <td>
                        <input id="so_tk" name="so_tk" class="easyui-textbox" style="width: 200px;height:25px;" />
                    </td>
                    <td>Ngân hàng:</td>
                    <td>
                        <input id="ngan_hang" name="ngan_hang" class="easyui-textbox" style="width: 200px;height:25px;" />
                    </td>
                    <td>Chủ TK:</td>
                    <td>
                        <input name="chu_tk" id="chu_tk" class="easyui-textbox" style="width: 200px;height:25px;" />
                    </td>
                 </tr><tr>
                     <td>Văn phòng:</td>
                     <td>
                         <input class="easyui-combobox" name="van_phong" id="van_phong" style="width: 200px;height:25px;"
                           url="common/chinhanh" valueField="id" textField="name" >
                     </td>
                      <td>Phòng ban:</td>
                      <td>
                          <input class="easyui-combobox" name="phong_ban" id="phong_ban" style="width: 200px;height:25px;"
                            url="common/phongban" valueField="id" textField="name" >
                      </td>
                      <td>Ca làm việc:</td>
                      <td>
                          <input class="easyui-combobox" name="ca" id="ca" style="width: 200px;height:25px;"
                            url="common/ca" valueField="id" textField="ca" >
                      </td>
                  </tr><tr>
                        <td>Ghi chú:</td>
                        <td colspan="3">
                            <input name="ghi_chu" id="ghi_chu" class="easyui-textbox" style="width: 500px;height:25px;" />
                        </td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

<div id="dlg-hopdong" class="easyui-dialog" style="width:700px; padding:10px" closed="true" buttons="#dlg-buttons-1" modal="true" data-options="resizable:true">
    <form id="fm-hopdong" method="post" enctype="multipart/form-data">
          <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
              <tr>
                  <td>Họ tên:</td>
                  <td>
                      <input id="name" name="name" class="easyui-textbox" required style="width: 200px;height:25px;" />
                  </td>
                  <td>Ngày đi làm:</td>
                  <td>
                      <input id="ngaydilam" name="ngaydilam" class="easyui-datebox" style="width: 200px;height:25px;" />
                  </td>
              </tr><tr>
                  <td>Hợp đồng lao động:</td>
                  <td>
                    <input class="easyui-combobox" name="tinh_trang" id="tinh_trang" style="width: 200px;height:25px;"
                      url="common/hopdong" valueField="id" textField="name" >
                  </td>
                  <td>Phòng ban:</td>
                  <td>
                    <input class="easyui-combobox" name="phong_ban" id="phong_ban" style="width: 200px;height:25px;"
                      url="common/phongban" valueField="id" textField="name" editable="false" >
                  </td>
              </tr><tr>
                  <td>Email công việc:</td>
                  <td>
                      <input id="email" name="email" class="easyui-textbox" style="width: 200px;height:25px;" />
                  </td>
                  <td>Lương:</td>
                  <td>
                      <input id="luong" name="luong" style="width: 200px;height:25px;" class="easyui-numberbox" data-options="precision:0,groupSeparator:',',width:'100%'">
                  </td>
              </tr><tr>
                  <td>Mức đóng bảo hiểm:</td>
                  <td>
                      <input id="bao_hiem" name="bao_hiem" style="width: 200px;height:25px;" class="easyui-numberbox" data-options="precision:0,groupSeparator:',',width:'100%'">
                  </td>
                  <td>Phụ cấp ăn ca:</td>
                  <td>
                      <input id="an_ca" name="an_ca" style="width: 200px;height:25px;" class="easyui-numberbox" data-options="precision:0,groupSeparator:',',width:'100%'">
                  </td>
              </tr><tr>
                  <td>Phụ cấp gửi xe:</td>
                  <td>
                      <input id="gui_xe" name="gui_xe" style="width: 200px;height:25px;" class="easyui-numberbox" data-options="precision:0,groupSeparator:',',width:'100%'">
                  </td>
                  <td>Mã số thuế:</td>
                  <td>
                      <input id="ma_so_thue" name="ma_so_thue" class="easyui-textbox" style="width: 200px;height:25px;" />
                  </td>
              </tr><tr>
                  <td>Số sổ bảo hiểm:</td>
                  <td>
                      <input id="so_bao_hiem" name="so_bao_hiem" class="easyui-textbox" style="width: 200px;height:25px;" />
                  </td>
                  <td>Số TK ngân hàng:</td>
                  <td>
                      <input id="so_tk" name="so_tk" class="easyui-textbox" style="width: 200px;height:25px;" />
                  </td>
              </tr><tr>
                  <td>Chủ TK:</td>
                  <td>
                      <input id="chu_tk" name="chu_tk" class="easyui-textbox" style="width: 200px;height:25px;" />
                  </td>
                  <td>Tên ngân hàng:</td>
                  <td>
                      <input id="ngan_hang" name="ngan_hang" class="easyui-textbox" style="width: 200px;height:25px;" />
                  </td>
              </tr><tr>
                  <td>Ngày kết thúc:</td>
                  <td>
                      <input id="ngayketthuc" name="ngayketthuc" class="easyui-datebox" style="width: 200px;height:25px;" />
                  </td>
                  <td>Ghi chú:</td>
                  <td>
                      <input id="ghi_chu" name="ghi_chu" class="easyui-textbox" style="width: 200px;height:25px;" />
                  </td>
              </tr>
          </table>
    </form>
</div>
<div id="dlg-buttons-1">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="savehopdong()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-hopdong').dialog('close')">Bỏ qua</button>
</div>

<!-- <script>
    tinymce.init({
                mode: "textareas",
                entity_encoding : "raw",
                plugins: ["advlist autolink lists link image charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen textcolor", "media",
                            "insertdatetime media table contextmenu paste jbimages","fullscreen","moxiemanager"],
                image_advtab: true,
                paste_data_images: true,
                browser_spellcheck : true,
                relative_urls:false,
                remove_script_host : false,
                image_dimensions: false,
                forced_root_block : false,
                force_br_newlines : true,
                force_p_newlines : false,
                toolbar: " undo redo | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media insertfile |  fontsizeselect | forecolor backcolor | fullscreen"
        });
</script> -->

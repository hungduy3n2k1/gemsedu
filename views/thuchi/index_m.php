<script type="text/javascript" src="js/thuchi.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="thuchi/json?taikhoan=<?=$this->taikhoan?>" toolbar="#toolbar" pagination="true"
    idField="id" fitColumns="false" singleSelect="true" nowrap="true" iconCls="icon-dichvu" title="<?=$this->name?>"
    data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
        <tr>
            <th field="ngay" width="100">Ngày giờ</th>
            <th field="khachhang" width="100">Khách hàng</th>
            <th field="dien_giai" width="200">Diễn giải</th>
            <th field="thu" width="100" align="right" formatter="CurrencyFormatted">Thu</th>
            <th field="chi" width="100" align="right" formatter="CurrencyFormatted">Chi</th>
            <th field="so_du" width="100" align="right" formatter="CurrencyFormatted">Số dư</th>
        </tr>
    </thead>
</table>

<div id="dlg-search" class="easyui-dialog" style="width:90%; height: auto; padding: 10px; top:30px" closable="false" closed="true"
    buttons="#dlg-buttons-2" modal="true">
    <form id="fm-search" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tài khoản:</td>
                <td>
                    <input class="easyui-combobox" style="width:200px; height:25px" id="taikhoan" name="taikhoan"
                      valueField="id" textField="name" url="common/taikhoan" value="<?php echo $_SESSION['user']['nhan_vien'] ?>">
                </td>
            </tr><tr>
                <td>Từ ngày:</td>
                <td>
                    <input class="easyui-datebox" style="width:200px; height:25px" id="ngaybd" name="ngaybd"
                        value="<?php echo date(" d/m/Y",strtotime('first day of this month')) ?>" >
                </td>
            </tr><tr>
                <td>Đến ngày:</td>
                <td>
                  <input class="easyui-datebox" style="width:200px; height:25px" id="ngaykt" name="ngaykt"
                      value="<?php echo date(" d/m/Y ") ?>" >
                </td>
            </tr><tr>
                <td>Từ khóa:</td>
                <td>
                    <input class="easyui-textbox" style="width:200px; height:25px" id="tukhoa" name="tukhoa">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons-2">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="timkiem()">OK</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg-search').dialog('close')">Bỏ qua</button>
</div>

<footer>
    <div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="javascript:$('#dlg-search').dialog({closed:false})" style="font-size:14px">
            <img src="libs/jquery/images/search.png" width="32" height="32" />
            <br>Tìm kiếm
        </div>
    </div>

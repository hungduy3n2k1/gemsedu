<script type="text/javascript" src="js/thuchi.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="thuchi/json" toolbar="#toolbar" pagination="true"
    idField="id" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="true" iconCls="icon-dichvu"
    data-options="border:false,fit:true" pageSize="30" showFooter="true">
    <thead>
        <tr>
            <th field="ngaygio" width="100">Ngày giờ</th>
            <th field="sale" width="150">Sale</th>
            <th field="khachhang" width="170">Khách hàng</th>
            <th field="dien_giai" width="340">Diễn giải</th>
            <th field="hachtoan" width="80">Hạch toán</th>
            <th field="thu" width="100" align="right" formatter="CurrencyFormatted">Thu</th>
            <th field="chi" width="100" align="right" formatter="CurrencyFormatted">Chi</th>
            <th field="so_du" width="100" align="right" formatter="CurrencyFormatted">Số dư</th>
            <th field="ghi_chu" width="200">Ghi chú</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <input id="taikhoan" name="taikhoan" class="easyui-combobox" url="common/taikhoan" valueField="id" , textField="name" style="width:150px" />
    Từ ngày: <input id="ngaybd" name="ngaybd" class="easyui-datebox" style="width:120px" value="<?php echo date(" d/m/Y",strtotime('first day of this month')) ?>"/>
    Đến ngày: <input id="ngaykt" name="ngaykt" class="easyui-datebox" style="width:120px" value="<?php echo date(" d/m/Y ") ?>"/>
    <input id="tukhoa" name="tukhoa" class="easyui-textbox" prompt="Từ khóa" style="width:150px" />
    <button class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="timkiem()">Tìm kiếm</button>&nbsp;&nbsp;
    <?php
       foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 670px; height: auto; padding: 10px;" closed="true" buttons="#dlg-buttons" modal="true" iconCls="icon-phieuthu">
    <form id="fm" method="post">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>
                    <div>Loại phiếu :</div>
                </td>
                <td>
                    <input id="loai" name="loai" class="easyui-combobox" style="width: 200px;height:28px;"
                    data-options="valueField:'id',textField:'text',data:[{'id':'0','text':'Phiếu thu'},{'id':'1','text':'Phiếu chi'}] " />
                </td>
                <td>
                    <div>Khách hàng:</div>
                </td>
                <td>
                    <input id="khach_hang"  name="khach_hang" class="easyui-combobox" style="width: 200px;height:28px;"
                    valueField="id" textField="name"/>
                </td>
            </tr><tr>
                <td>
                    <div>Ngày :</div>
                </td>
                <td>
                    <input id="ngaygio" name="ngaygio" class="easyui-datebox" style="width: 200px;height:28px;" />
                </td>
                <td>
                    <div>Tài khoản:</div>
                </td>
                <td>
                    <input id="tai_khoan"  name="tai_khoan" class="easyui-combobox" url="common/taikhoan" style="width: 200px;height:28px;" valueField="id" textField="name" />
                </td>
            </tr>
            <tr>
                <td>
                    <div>Số tiền:</div>
                </td>
                <td>
                    <input id="so_tien" name="so_tien" style="width:200px; height:28px;" class="easyui-numberbox" groupSeparator="," required />
                </td>
                <td>
                    <div>Hạch toán:</div>
                </td>
                <td>
                    <input id="hach_toan"  name="hach_toan" class="easyui-combobox" url="common/hachtoan" style="width: 200px;height:28px;" valueField="id" textField="name" />
                </td>
            </tr>
            <tr>
                <td>
                    <div>Invoice:</div>
                </td>
                <td>
                    <input id="invoice" name="invoice" style="width:200px; height:28px;" editable="false"
                           valueField="id" textField="invname" class="easyui-combobox"/>
                </td>
                <td>
                    <div>Nhân viên sale:</div>
                </td>
                <td>
                    <input id="nhan_vien_sale" name="nhan_vien_sale" style="width:200px; height:28px;" editable="false" url="common/nhanvien"
                           valueField="id" textField="name" class="easyui-combobox"/>
                </td>
            </tr>
            <tr>
                <td>Tạm ứng:</td>
                <td><input class="easyui-radiobutton" name="tinhtranginv" id="tinhtranginv1" value="3" ></td>
                <td>Thanh toán hết:</td>
                <td><input class="easyui-radiobutton" name="tinhtranginv" id="tinhtranginv2" value="4" checked></td>
            </tr>
            <tr>
                <td>
                    <div>Nội dung:</div>
                </td>
                <td>
                    <input id="dien_giai" name="dien_giai" style="width:200px; height:60px;" class="easyui-textbox" multiline="true" />
                </td>
                <td>
                    <div>Ghi chú:</div>
                </td>
                <td>
                    <input id="ghi_chu" name="ghi_chu" style="width:200px; height:60px;"  class="easyui-textbox" multiline="true"  />
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dlg').dialog('close')">Quay lại
    </button>
</div>

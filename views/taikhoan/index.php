<script type="text/javascript" src="js/taikhoan.js"></script>
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="taikhoan/json" toolbar="#toolbar"
pagination="true" idField="id" rownumbers="true" fitColumns="false" singleSelect="true" showFooter="true"
nowrap="true" iconCls="icon-phanloai" data-options="border:false,fit:true" pageSize="30">
    <thead>
        <tr>
            <th field="name" width="150">Tên tài khoản</th>
            <th field="so_tk" width="150">Số tài khoản</th>
            <th field="ngan_hang" width="200">Ngân hàng</th>
            <th field="sodu" width="200" align="right" formatter="CurrencyFormatted">Số dư</th>
        </tr>
    </thead>
</table>
<div id="toolbar" style="padding-top:5px; padding-bottom:5px;">
    <!-- <span style="color:blue; font-weight:bold; margin-right:20px">Các tài khoản kế toán</span> -->
    <?php
        foreach ($this->funs as $item)
              echo '<button class="easyui-linkbutton" iconCls="'.$item['icon'].'" plain="false" onclick="'.$item['link'].'">'.$item['name'].'</button> ';
    ?>
</div>

<div id="dlg" class="easyui-dialog" style="width: 340px; height: auto; padding: 10px;" closable="false" closed="true" buttons="#dlg-buttons" modal="true">
    <form id="fm" method="post" enctype="multipart/form-data">
        <table border="0" width="100%" cellspacing="0" cellpadding="1" class="tblForm">
            <tr>
                <td>Tên tài khoản:</td>
                <td>
                    <input id="name" name="name" class="easyui-validatebox" required="required" style="width: 197px;height:22px;border: 1px solid #6B9CDE; border-radius: 5px;" />
                </td>
            </tr>
            <tr>
                <td>Số tài khoản:</td>
                <td>
                    <input class="easyui-textbox" style="width:200px; height:25px; " id="so_tk" name="so_tk">
                </td>
            </tr>
            <tr>
                <td>Ngân hàng:</td>
                <td>
                    <input class="easyui-textbox" style="width:200px; height:25px; " id="ngan_hang" name="ngan_hang">
                </td>
            </tr>
            <tr>
                <td>Số dư đầu:</td>
                <td>
                    <input class="easyui-numberbox" style="width:200px; height:25px; " id="sodu" name="sodu">
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="dlg-buttons">
    <button class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Câp nhật</button>
    <button class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Bỏ qua</button>
</div>

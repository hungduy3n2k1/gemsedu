<script>
    function linkto(val,row) {
        return '<a href="thuchi?taikhoan='+row.id+'&name='+row.name+'">'+val+'</a>';
    }
</script>
<table id="dg" class="easyui-datagrid" url="taikhoan/json" pagination="false"  style="width: 100%;"
  data-options="header:'#hh',singleSelect:true,border:false,fit:true,fitColumns:true ,scrollbarSize:0">
    <thead>
        <tr>
            <th field="name" width="150" formatter="linkto">Tên tài khoản</th>
            <th field="sodu" width="200" align="right" formatter="CurrencyFormatted">Số dư</th>
        </tr>
    </thead>
</table>
<div id="hh">
    <div class="m-toolbar">
        <div class="m-title" id="title">Số dư tài khoản</div>
    </div>
</div>

<footer>
<div class="easyui-tabs" data-options="tabHeight:60,fit:true,tabPosition:'bottom',border:false,pill:true,narrow:true,justified:true">
    <div style="padding:10px">
        <div class="panel-header tt-inner" onClick="javascript:$('#dlg').dialog({closed:false})" style="font-size:14px">
            <img src='libs/jquery/images/search.png' width="32" height="32" />
            <br>Tìm kiếm
        </div>
    </div>

<!--
<table id="dg" style="width: 100%;" class="easyui-datagrid" url="" toolbar="#toolbar"
idField="id" rownumbers="true" fitColumns="false" singleSelect="true" showFooter="true"
nowrap="true" iconCls="icon-phanloai" data-options="border:false,fit:true" pageSize="30">
    <thead>

    </thead>
</table> -->
